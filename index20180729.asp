<!DOCTYPE html><!-- lang="zh-TW"-->
<html class="no-js">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta charset="utf-8">
<title>超悠旅行社 | 歡迎光臨</title>
<link rel="shortcut icon" type="image/x-icon" href="/eweb_summittour/images/default/favicon.ico">
<link rel="bookmark" type="image/x-icon" href="/eweb_summittour/images/default/favicon.ico">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="description" content=	測試"超悠旅行社股份有限公司,電話:04-23052639,傳真:04-23052659,地址:臺中市西區後龍里向上路一段25號3樓,依發展觀光條例之規定，凡經中央主管機關核准，為旅客設計安排國內外旅程、食宿、領隊人員、導遊人員、代購代售交通客票、代辦出國簽證手續等有關服務及旅遊諮詢…等等"><!--網站描述-->
<meta name="keywords" content="悠遊卡,護照,連假,節日,行李箱,假期,旅展,頂尖,頂尖旅遊,超悠,游遍中國,旅遊,超,悠,旅行社,游遍,中國,中國大陸,東北亞,東南亞,南亞,台灣旅遊,台中出發,郵輪，印度，泰國，越南"><!--關鍵字-->
<meta property="og:image" content="http://www.summittour.com.tw/eweb_summittour/images/default/favicon.ico"><!--FB分享圖示-->
<meta property="og:title" content="超悠旅行社 | 歡迎光臨"><!--FB分享標題-->
<meta property="og:url" content="http://www.summittour.com.tw/"><!--FB分享路徑-->
<meta property="og:description" content="超悠旅行社股份有限公司,電話:04-23052639,傳真:04-23052659,地址:臺中市西區後龍里向上路一段25號3樓,依發展觀光條例之規定，凡經中央主管機關核准，為旅客設計安排國內外旅程、食宿、領隊人員、導遊人員、代購代售交通客票、代辦出國簽證手續等有關服務及旅遊諮詢…等等"><!--FB分享內容敘述-->
<meta name="Robots" content="all" />
<meta name="Googlebot" content="index,follow" />
<meta name="Audience" content="All" />
<meta name="Page-topic" content="Internet" />
<meta name="Rating" content="General" />
<meta name="Resource-Type" content="document" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="windows-Target" content="_top" />
<meta name="Author" content="GTSHAYO" />
<meta name="Copyright" content="Copyright c 2017 www.summittour.com.tw All Rights Reserved." />
<meta name="Generator" content="Visual Studio Code" />
<meta name="Publisher" content="超悠旅行社" />
<meta name="Creation-Date" content="06-23-2017" />
<meta name="WEBCRAWLERS" content="ALL" />
<meta name="SPIDERS" content="ALL" />
<meta name="revisit-after" content="7 days"/>
<meta name="google-site-verification" content="_keCfnTu5RcERww46iY-baiEjI88avmNO3dZ8T5ghI8" />
<link type="text/css" rel="stylesheet" href="/eweb_summittour/css/reset_new.css">
<link type="text/css" rel="stylesheet" href="/eweb_summittour/css/normalize.css">
<link type="text/css" rel="stylesheet" href="/eweb_summittour/css/bootstrap.css">
<link type="text/css" rel="stylesheet" href="/eweb_summittour/css/font-awesome.min.css">
<!--<link rel="stylesheet" type="text/css" href="/eweb_summittour/css/loaders.css"/>-->
<link rel="stylesheet" type="text/css" href="/eweb_summittour/css/main.css">
<link type="text/css" rel="stylesheet" href="/eweb_summittour/css/default.css">

<script type="text/javascript" language="javascript" src="/eweb_summittour/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" language="javascript" src="/eweb_summittour/js/bootstrap.min.js"></script>
<!--<script type="text/javascript" language="javascript" src="/eweb_summittour/js/jquery.nicescroll.min.js"></script>-->
<script type="text/javascript" language="javascript" src="/eweb_summittour/js/modernizr.js"></script>
<script type="text/javascript" language="javascript" src="/eweb_summittour/js/all.js"></script>
<script type="text/javascript" language="javascript" src="/eweb_summittour/js/hammer.min.js"></script>


<link href="/eweb_summittour/css/home_autopage.css" rel="stylesheet" type="text/css">
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-P6LCV9G');</script>
<!-- End Google Tag Manager -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-99295320-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-99295320-1');
</script>
<style type="text/css">
p img {
    margin: 4px;
    -webkit-border-top-left-radius: 10px;
    -webkit-border-bottom-right-radius: 5px;
    -moz-border-radius: 5px;
    -moz-border-radius: 5px;
    border-radius: 5px;
}
</style>

</head>

<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P6LCV9G"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- Loader動畫 -->
<!--<div id="loader-wrapper">
    <div id="loader"></div>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
</div>-->
<!-- Loader動畫 -->

<div class="wrapper">
    <!--上標-->
    <article class="header">
    <!--#include virtual="/eweb_summittour/public/header.asp" -->
    </article>
    <article class="summittour_carousel">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div id="myCarousel" class="carousel slide carousel-fade" data-ride="carousel" data-interval="9000">
                <!--div id="myCarousel" class="carousel slide" data-ride="carousel"-->
                    <!-- 指標 Indicators -->
                    <ol class="carousel-indicators" style="width: 100%;">
                        <li data-target="#myCarousel" data-slide-to="0" class="active">人氣商品</li>
                        <li data-target="#myCarousel" data-slide-to="1" class="">人氣商品</li>
                        <li data-target="#myCarousel" data-slide-to="2" class="">人氣商品</li>
                        <li data-target="#myCarousel" data-slide-to="3" class="">人氣商品</li>
                        <li data-target="#myCarousel" data-slide-to="4" class="">人氣商品</li>
                    </ol>
                    <!-- 幻燈片區塊 Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <a href="/EW/GO/GroupDetail.asp?prodCd=TYO05170708B" target="_blank">
								<div class="img" style="background-image:url('/eweb_miyabi/images/default/slider_01.jpg');cursor: pointer; width: 20%;"></div>
							</a>
						</div>
                        <div class="item">
                            <a href="/EW/GO/GroupDetail.asp?prodCd=TYO05170826A" target="_blank">
								<div class="img" style="background-image:url('/eweb_miyabi/images/default/slider_01.jpg');cursor: pointer; width: 20%;"></div>
                            </a>
                        </div>
                        <div class="item">
                            <a href="/eweb_miyabi/starcruises/index.asp" target="_blank">
								<div class="img" style="background-image:url('/eweb_miyabi/images/default/slider_01.jpg');cursor: pointer; width: 20%;"></div>
                            </a>
                        </div>
                        <div class="item">
                            <a href="/eweb_miyabi/starcruises/index.asp" target="_blank">
								<div class="img" style="background-image:url('/eweb_miyabi/images/default/slider_01.jpg');cursor: pointer; width: 20%;"></div>
                            </a>
                        </div>
                        <div class="item">
                            <a href="/eweb_miyabi/starcruises/index.asp" target="_blank">
								<div class="img" style="background-image:url('/eweb_miyabi/images/default/slider_01.jpg');cursor: pointer; width: 20%;"></div>
                            </a>
                        </div>
                    </div>

                    <!-- Left and right controls -->
                    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </article>
    <article class="container">
        <div class="row">

            <!--麵包屑及頁面標題-->
            <!--ol class="breadcrumb">
                <li>目前位置：</li>
                <li class="active">首頁</li> 
            </ol-->


			
            <div id="search">

                <div class="container" >
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="search_content">
                        <!--#include virtual="/eweb_summittour/module/GroupSearch.asp" -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>
	<article id="module_content" class="container">
		<div class="row">
            <section class="module_content">
			
			
            </section>
		</div>
	</article>
	<!--促銷活動(首頁上方 6圖 )-->
	<article id="module_content_1" class="container">
		<div class="row"  id="module_content_row_1">
            <section class="module_content" id="module_01">
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 tile module_three">
                    <a href="javascript:;" target="_blank">
                        <img src="/eweb_summittour/images/default/index_01.jpg" />
						<div class="text">
                            <h3 class="animate-text">
							<i class="fa fa-link" aria-hidden="true"></i>
							</h3>
                        </div>
                    </a>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 tile module_three">
                    <a href="javascript:;" target="_blank">
                        <img src="/eweb_summittour/images/default/index_01.jpg" />
						<div class="text">
                            <h3 class="animate-text"><i class="fa fa-link" aria-hidden="true"></i></h3>
                        </div>
                    </a>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 tile module_three">
                    <a href="javascript:;" target="_blank">
                        <img src="/eweb_summittour/images/default/index_01.jpg" />
						<div class="text">
                            <h3 class="animate-text"><i class="fa fa-link" aria-hidden="true"></i></h3>
                        </div>
                    </a>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 tile module_three">
                    <a href="javascript:;" target="_blank">
                        <img src="/eweb_summittour/images/default/index_02.jpg" />
						<div class="text">
                            <h3 class="animate-text"><i class="fa fa-link" aria-hidden="true"></i></h3>
                        </div>
                    </a>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 tile module_three">
                    <a href="javascript:;" target="_blank">
                        <img src="/eweb_summittour/images/default/index_03.jpg" />
						<div class="text">
                            <h3 class="animate-text"><i class="fa fa-link" aria-hidden="true"></i></h3>
                        </div>
                    </a>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 tile module_three">
                    <a href="javascript:;" target="_blank">
                        <img src="/eweb_summittour/images/default/index_04.jpg" />
						<div class="text">
                            <h3 class="animate-text"><i class="fa fa-link" aria-hidden="true"></i></h3>
                        </div>
                    </a>
                </div>

            </section>
		</div>
	</article>
	<article id="module_full_01" class="container">
		<div class="row" id="module_full_row_01">
            <section class="module_content">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 tile module_three">
                    <a href="javascript:;" target="_blank">
                        <img src="/eweb_summittour/images/default/index_05.jpg" />
                    </a>
                </div>
            </section>
		</div>
	</article>
	<!--促銷活動(首頁上方 6圖 )-->
	
	<!--季節限定--
	<article id="module_content_3" class="container">
		<div class="row">
            <section class="module_content">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 module_title">
                    <h3>季節限定</h3>
                </div>
				<div class="exercise_box">
					<div class="central_tours">
						<div id="theme_content" class="skin_module">
							<div id="tab8"></div>
						</div>   
					</div>
				</div>
			
            </section>
		</div>
	</article>
	<!--季節限定-->
	
	<!--線上旅展--
	<article id="module_content_3" class="container">
		<div class="row">
            <section class="module_content">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 module_title">
                    <h3>線上旅展</h3>
                </div>
				<div class="exercise_box">
					<div class="central_tours">
						<div id="theme_content" class="skin_module">
							<div id="tab6"></div>
						</div>   
					</div>
				</div>
			
            </section>
		</div>
	</article>
	<!--線上旅展-->

	<!--過年限定優惠--
	<article id="module_content_3" class="container">
		<div class="row">
            <section class="module_content">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 module_title">
                    <h3>過年限定優惠</h3>
                </div>
				<div class="exercise_box">
					<div class="central_tours">
						<div id="theme_content" class="skin_module">
							<div id="tab5"></div>
						</div>   
					</div>
				</div>
			
            </section>
		</div>
	</article>
	<!--過年限定優惠-->
	
	<!--早鳥優惠--
	<article id="module_content_3" class="container"> 
		<div class="row">
            <section class="module_content">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 module_title">
                    <h3>早鳥優惠</h3>
                </div>
				<div class="exercise_box">
					<div class="central_tours">
						<div id="theme_content" class="skin_module">
							<div id="tab9"></div>
						</div>   
					</div>
				</div>
			
            </section>
		</div>
	</article>
	<article id="module_full_02" class="container">
		<div class="row" id="module_full_row_02">
            <section class="module_content">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 tile module_three">
                    <a href="javascript:;" target="_blank">
                        <img src="/eweb_summittour/images/default/index_05.jpg" />
                    </a>
                </div>
            </section>
		</div>
	</article>
	<!--早鳥優惠-->
	
	
	<!--好禮贈送-->
	<article id="module_content_3" class="container">
		<div class="row">
            <section class="module_content">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 module_title">
                    <h3>好禮贈送</h3>
                </div>
				<div class="exercise_box">
					<div class="central_tours">
						<div id="theme_content" class="skin_module">
							<div id="tab3"></div>
						</div>   
					</div>
				</div>
            </section>
		</div>
	</article>
	<!--好禮贈送-->
	<!--促銷行程-->
	<article id="module_content_3" class="container"> 
		<div class="row">
            <section class="module_content">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 module_title">
                    <h3>促銷行程</h3>
                </div>
				<div class="exercise_box">
					<div class="central_tours">
						<div id="theme_content" class="skin_module">
							<div id="tab1"></div>
						</div>   
					</div>
				</div>
			
            </section>
		</div>
	</article>
	<article id="module_full_02" class="container">
		<div class="row" id="module_full_row_02">
            <section class="module_content">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 tile module_three">
                    <a href="javascript:;" target="_blank">
                        <img src="/eweb_summittour/images/default/index_05.jpg" />
                    </a>
                </div>
            </section>
		</div>
	</article>
	<!--促銷行程-->
	
	<!--保證出團-->
	<article id="module_content_3" class="container">
		<div class="row">
            <section class="module_content">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 module_title">
                    <h3>保證出團</h3>
                </div>
				<div class="exercise_box">
					<div class="central_tours">
						<div id="theme_content" class="skin_module">
							<div id="tab2"></div>
						</div>   
					</div>
				</div>
            </section>
		</div>
	</article>
	<article id="module_full_03" class="container">
		<div class="row" id="module_full_row_03">
            <section class="module_content">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 tile module_three">
                    <a href="javascript:;" target="_blank">
                        <img src="/eweb_summittour/images/default/index_05.jpg" />
                    </a>
                </div>
            </section>
		</div>
	</article>
	<!--保證出團-->
	<!--自由行-->
	
	<article id="module_content_3" class="container">
		<div class="row">
            <section class="module_content">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 module_title">
                    <h3>自由行</h3>
                </div>
				<div class="exercise_box">
					<div class="central_tours">
						<div id="theme_content" class="skin_module">
							<div id="tab7"></div>
						</div>   
					</div>
				</div>
			
            </section>
		</div>
	</article>
	<!--自由行-->
	
	

    <article class="footer">
    <!--#include virtual="/eweb_summittour/public/footer.asp" -->
    </article>

</div>

<div class="service_box">
    <ul>
        <li><button class="btn btn-success" onclick="location.href='tel:0423052639'"><i class="fa fa-phone" aria-hidden="true"></i> 手機直撥</button></li>
        <li><button class="btn btn-danger" onclick="location.href='/eweb_summittour/diy/V_diy.asp'"><i class="fa fa-envelope" aria-hidden="true"></i> 聯絡我們</button></li>
    </ul>
</div>
<div class="actGotop"><a href="javascript:;" title="返回頂部"><i class="fa fa-angle-up" aria-hidden="true"></i>TOP</a></div>


<script src="/eweb_summittour/js/share_main.js"></script>

<script type="text/javascript">
$(document).ready( function() {
	getXML(true,'');
});
</script>

<script src="/eweb_summittour/js/jquery.superslides.js" type="text/javascript" charset="utf-8"></script>

<!--自動上架置入-->
<% 
            if returl1="" then returl1="/eWeb/Main/home.asp"
            if returl2="" then returl2="/eWeb/Main/home.asp"
            If (Session("USR_CLS") = "A") Then
        '判斷是否為同業會員
           %>
           <!--如果是同業會員，就秀下面資料-->
          <script>
			$(function(){
				$("#tab1").load("/eweb_summittour/DHTML/AGT/UP000001/index.asp .metropolis",function(){$("html");});
				$("#tab2").load("/eweb_summittour/DHTML/AGT/GRT00001/index.asp .metropolis",function(){$("html");});
				$("#tab3").load("/eweb_summittour/DHTML/AGT/DIS00001/index.asp .metropolis",function(){$("html");});
				$("#tab4").load("/eweb_summittour/DHTML/AGT/TXG00001/index.asp .metropolis",function(){$("html");});
				$("#tab5").load("/eweb_summittour/DHTML/AGT/UP000002/index.asp .metropolis",function(){$("html");});
				$("#tab6").load("/eweb_summittour/DHTML/AGT/OTS00001/index.asp .metropolis",function(){$("html");});
				$("#tab7").load("/eweb_summittour/DHTML/AGT/MWF00001/index.asp .metropolis",function(){$("html");});
				$("#tab8").load("/eweb_summittour/DHTML/AGT/UPR00001/index.asp .metropolis",function(){$("html");});
				$("#tab9").load("/eweb_summittour/DHTML/AGT/EBO00001/index.asp .metropolis",function(){$("html");});
				});
				
			</script>
          <% else%>
          <!--如果不是，就秀下面資料-->
          <script>
			$(function(){
				$("#tab1").load("/eweb_summittour/DHTML/UP000001/index.asp .metropolis",function(){$("html");});
				$("#tab2").load("/eweb_summittour/DHTML/GRT00001/index.asp .metropolis",function(){$("html");});
				$("#tab3").load("/eweb_summittour/DHTML/DIS00001/index.asp .metropolis",function(){$("html");});
				$("#tab4").load("/eweb_summittour/DHTML/TXG00001/index.asp .metropolis",function(){$("html");}); 
				$("#tab5").load("/eweb_summittour/DHTML/UP000002/index.asp .metropolis",function(){$("html");});
				$("#tab6").load("/eweb_summittour/DHTML/OTS00001/index.asp .metropolis",function(){$("html");});
				$("#tab7").load("/eweb_summittour/DHTML/MWF00001/index.asp .metropolis",function(){$("html");});
				$("#tab8").load("/eweb_summittour/DHTML/UPR00001/index.asp .metropolis",function(){$("html");});
				$("#tab9").load("/eweb_summittour/DHTML/EBO00001/index.asp .metropolis",function(){$("html");});
				});
			</script>
          
          <% End If %>


<script type="text/javascript">

    $(function() {
      var $slides = $('#slides');

      Hammer($slides[0]).on("swipeleft", function(e) {
        $slides.data('superslides').animate('next');
      });

      Hammer($slides[0]).on("swiperight", function(e) {
        $slides.data('superslides').animate('prev');
      });

      $slides.superslides({
        hashchange: true,
        play: 8000
      });

      $('#slides').on('mouseenter', function() {
        $(this).superslides('stop');
        console.log('Stopped')
      });
      $('#slides').on('mouseleave', function() {
        $(this).superslides('start');
        console.log('Started')
      });

    });

//分頁頁籤
$('#myTab a').click(function (e) {
  e.preventDefault()
  $(this).tab('show')
})

setTimeout(function(){
            $('.carousel').carousel({
                interval: 8000
            })
        }, 5000);

//回頂點
$(function(){   
    $(window).scroll(function() {       
        if($(window).scrollTop() >= 100){
            $('.actGotop').fadeIn(300); 
        }else{    
            $('.actGotop').fadeOut(300);    
        }  
    });
    $('.actGotop').click(function(){
    $('html,body').animate({scrollTop: '0px'}, 800);}); 
    $('#go_search').click(function(){   $('html,body').animate({scrollTop:$('#search').offset().top}, 900); return false;   })
});

$(function(){   
    var cubuk_seviye = $(document).scrollTop();
    var header_yuksekligi = $('.header').outerHeight();

    $(window).scroll(function() {
        var kaydirma_cubugu = $(document).scrollTop();

        if (kaydirma_cubugu > header_yuksekligi){$('.header').addClass('gizle');} 
        else {$('.header').removeClass('gizle');}

        if (kaydirma_cubugu > cubuk_seviye){$('.header').removeClass('sabit');} 
        else {$('.header').addClass('sabit');}              

        cubuk_seviye = $(document).scrollTop(); 
     });
     
});


$(function(){
    var sideslider = $('[data-toggle=collapse-side]');
    var get_sidebar = sideslider.attr('data-target-sidebar');
    var get_content = sideslider.attr('data-target-content');
    sideslider.click(function(event){
      $(get_sidebar).toggleClass('in');
      $(get_content).toggleClass('out');
   });

});
$(function(){
    var sideslider = $('[data-toggle=collapse-side1]');
    var get_sidebar = sideslider.attr('data-target-sidebar1');
    var get_content = sideslider.attr('data-target-content1');
    sideslider.click(function(event){
      $(get_sidebar).toggleClass('in');
      $(get_content).toggleClass('out');
   });

});

</script>

</body>
</html>
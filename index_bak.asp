﻿<!--#INCLUDE VIRTUAL="/INCLUDE/egrpDSN_binary.inc" -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" >
<meta name="description" content="超悠旅行社股份有限公司,電話:04-23052639,傳真:04-23052659,地址:臺中市西區後龍里向上路一段25號3樓,依發展觀光條例之規定，凡經中央主管機關核准，為旅客設計安排國內外旅程、食宿、領隊人員、導遊人員、代購代售交通客票、代辦出國簽證手續等有關服務及旅遊諮詢…等等">
<meta name="keywords" content="悠遊卡,護照,連假,節日,行李箱,假期,旅展,頂尖,頂尖旅遊,超悠,游遍中國,旅遊,超,悠,旅行社,游遍,中國,中國大陸,東北亞,東南亞,南亞,台灣旅遊,台中出發,郵輪，印度，泰國，越南">
<meta name="author" content="超悠旅行社,頂尖旅遊">
<meta name="copyright" content="Copyright YourCompany - 2017">
<meta name="email" content="summit1@summittour.com.tw">
<title>超悠旅行社 | 歡迎光臨</title>
<link href="/css/summittour.css" rel="stylesheet" type="text/css">
<link href="/eweb_summittour/css/jquery.bxslider.css" rel="stylesheet" type="text/css">

<!– 這一行是給IE看的 –> 
<link rel="SHORTCUT ICON" href="/eweb_summittour/favicon.ico" /> 

<!– 這一行是給Mozilla看的 –> 
<link rel="icon" href="/eweb_summittour/favicon.ico" type="image/ico" /> 



<script type="text/javascript" src="/eweb_summittour/js/jquery.min.js"></script>
<script src="/eweb_summittour/js/jquery.bxslider.min.js"></script>
<script type="text/javascript" src="/eweb_summittour/js/ddaccordion.js"></script>
<script type="text/javascript" src="/eweb_summittour/js/tab.js"></script>
<script type="text/javascript">
ddaccordion.init({
	headerclass: "silverheader", //Shared CSS class name of headers group 標頭組的共享CSS類名稱
	contentclass: "submenu", //Shared CSS class name of contents group 共享CSS類名稱的內容組
	revealtype: "mouseover", //Reveal content when user clicks or onmouseover the header? Valid value: "click", "clickgo", or "mouseover"  顯示用戶點擊或onmouseover標題時的內容？ 有效值：“點擊”，“點擊”或“鼠標懸停”
	mouseoverdelay: 200, //if revealtype="mouseover", set delay in milliseconds before header expands onMouseover 如果revealtype =“mouseover”，請設置延遲（以毫秒為單位），然後標頭在Mouseover上展開
	collapseprev: true, //Collapse previous content (so only one open at any time)? true/false 折疊以前的內容（所以只有一個打開任何時候）？ 真假
	defaultexpanded: [0], //index of content(s) open by default [index1, index2, etc] [] denotes no content 內容索引默認打開[index1，index2，etc] []表示沒有內容
	onemustopen: true, //Specify whether at least one header should be open always (so never all headers closed) 指定是否應始終打開至少一個標題（所以不要關閉所有標題）
	animatedefault: false, //Should contents open by default be animated into view? 內容打開默認情況下是否動畫化？
	persiststate: true, //persist state of opened contents within browser session? 在瀏覽器會話中持續打開內容的狀態？
	toggleclass: ["", "selected"], //Two CSS classes to be applied to the header when it's collapsed and expanded, respectively ["class1", "class2"] 兩個CSS類被分別應用於標題，並分別展開[“class1”，“class2”]
	togglehtml: ["", "", ""], //Additional HTML added to the header when it's collapsed and expanded, respectively  ["position", "html1", "html2"] (see docs)  分頁[“position”，“html1”，“html2”]（見文檔）時，附加的HTML會添加到標題中
	animatespeed: "slow", //speed of animation: integer in milliseconds (ie: 200), or keywords "fast", "normal", or "slow"  動畫速度：以毫秒為單位的整數（即：200），或關鍵字“fast”，“normal”或“slow”
	oninit:function(headers, expandedindices){ //custom code to run when headers have initalized  標頭初始化時運行的自定義代碼
		//do nothing 沒做什麼
	},
	onopenclose:function(header, index, state, isuseractivated){ //custom code to run whenever a header is opened or closed
		//do nothing
	}
})
</script>
<script type="text/javascript">
  $(document).ready(function(){
    
$('.bxslider').bxSlider({
  mode: 'fade',
  captions: true,
  auto: true,
  autoControls: true
});
  });
</script>
<script type="text/javascript">
$(document).ready(function(){
  $('.slider4').bxSlider({
    slideWidth:210,
    minSlides: 2,
    maxSlides: 3,
    moveSlides: 1,
    slideMargin: 10,
	autoControls: true
  });
});
</script>
<script type="text/javascript">
$(document).ready(function(e) {
	$("#rightButton").css("right", "0px");
	
    var button_toggle = true;
	$(".right_ico").live("mouseover", function(){
		var tip_top;
		var show= $(this).attr('show');
		var hide= $(this).attr('hide');
		tip_top = show == 'tel' ?  65 :  -10;
		button_toggle = false;
		$("#right_tip").css("top" , tip_top).show().find(".flag_"+show).show();
		$(".flag_"+hide).hide();
		
	}).live("mouseout", function(){
		button_toggle = true;
		hideRightTip();
	});
	
	
	$("#right_tip").live("mouseover", function(){
		button_toggle = false;
		$(this).show();
	}).live("mouseout", function(){
		button_toggle = true;
		hideRightTip();
	});
	
	function hideRightTip(){
		setTimeout(function(){		
			if( button_toggle ) $("#right_tip").hide();
		}, 500);
	}
	
	$("#backToTop").live("click", function(){
		var _this = $(this);
		$('html,body').animate({ scrollTop: 0 }, 500 ,function(){
			_this.hide();
		});
	});

	$(window).scroll(function(){
		var htmlTop = $(document).scrollTop();
		if( htmlTop > 0){
			$("#backToTop").show();	
		}else{
			$("#backToTop").hide();
		}
	});
});
</script>

<!-- Piwik 分析-->
<script type="text/javascript">
  var _paq = _paq || [];
  /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
  _paq.push(["setDocumentTitle", document.domain + "/" + document.title]);
  _paq.push(["setCookieDomain", "*.www.summittour.com.tw"]);
  _paq.push(["setDomains", ["*.www.summittour.com.tw"]]);
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  (function() {
    var u="//summittour.pe.hu/piwik/";
    _paq.push(['setTrackerUrl', u+'piwik.php']);
    _paq.push(['setSiteId', '2']);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
    g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
  })();
</script>
<img src="http://summittour.pe.hu/piwik/piwik.php?idsite=2&rec=1&action_name=%E8%B6%85%E6%82%A0%E6%97%85%E8%A1%8C%E7%A4%BE" style="border:0" alt="" />
<!-- End Piwik 分析Code -->	

<style type="text/css">
#new_rg .hoticon {
	border-bottom: 1px #CCC dashed;
	line-height: 32px;
	font-weight: bold;
	margin-top: 10px;
	margin-bottom: 20px;
	float: left;
	width: 710px;
	margin-right: 0px;
	margin-left: 0px;
}
.hoticon h1 {
	font-size: 18pt;
	padding-left: 10px;
}
#new_rg .section__title {
	float: left;
	margin-bottom: 20px;
	color: #A1BA6D;
	font-size: 18pt;
	font-weight: bold;
	padding-left: 10px;
	width: 700px;
}
#new_rg .wrap--page {
	float: left;
	width: 230px;
	margin-bottom: 30px;
	margin-left: 2px;
	font-weight: bold;
	border: 1px dotted #999;
}
#new_rg .wrap--page a {
	text-decoration: none;
	color: #000;
}
#new_rg .wrap--page img {
	padding: 5px;
	width: 220px;
	height: 180px;
}
#new_rg .wrap--page .content--brand {
	padding: 5px;
	font-size: 110%;
	font-weight: bold;
	margin-top: 5px;
	margin-right: 5px;
	margin-left: 5px;
	height: 50px;
}
#new_rg .wrap--page .content--brand3 {
	font-size: 110%;
	color: #F00;
	text-align: right;
	float: left;
	padding: 5px;
	margin-right: 5px;
	margin-left: 5px;
	width: 210px;
	font-weight: bold;
	margin-bottom: 5px;
	height: 20px;
	clear: both;
}
</head>
<%
iSITE_CD=Request("SITE_CD")
iMP_ID	=Request("MP_ID")
if Request("edit_iWEB_ID")<>"" then iWEB_ID=Request("edit_iWEB_ID")
%>


<body leftmargin="0" topmargin="0">
 <!--Google Analytics 分析-->
 <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-81282938-2', 'auto');
  ga('send', 'pageview');

</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-99295320-1', 'auto');
  ga('send', 'pageview');
  ga('set', 'userId', {{USER_ID}}); // 使用已登入的 user_id 設定 User-ID。

</script>
<!--Google Analytics 分析-->
   
   
   
  <div id="web_Son_top"> 
      <!--#include virtual="/eweb_summittour/public/inc_top.asp" -->
  </div>
  
    
<div id="web_main">
    
  <div id="main_bigbox">
    <!-- main上半 左右側 (最新消息 大圖輪播 主題旅遊  搜尋引擎) start -->
    <div id="main_lfbox">
      <!-- 最新消息 start -->
          <div class="newsarea">
              <div class="newstitle">最新消息</div>
              <iframe  src="/eWeb/Main/home.asp?SITE_CD=1&MP_ID=MPnew&STYLE_TP=0" allowtransparency="True" frameborder="0" vspace=0 hspace=0 width="600" height="30"  marginheight=0 marginwidth=0 scrolling=no></iframe>
              
          </div>
      <!-- 最新消息 end -->
      <!-- 大圖輪播 start--> 
          <ul class="bxslider">
			<!-- 旅展 -->
			<!--li><a href="/eWeb_summittour/Prefecture/Prefecture04.html"><img src="/eWeb_summittour/Prefecture/images/Banners/travel show.jpg" width="100%" height="270"></a></li-->
			<!-- 過年 -->
			<!--li><a href="/eWeb_summittour/Prefecture/cny.html"><img src="/eweb_summittour/images/movable/S_roation07.jpg"></a></li-->
			<!-- 228 -->
			<!--li><a href="/eWeb_summittour/Prefecture/Prefecture02.html"><img src="/eWeb_summittour/Prefecture/images/228.jpg" width="100%" height="270"></a></li-->
			<!-- 國泰世華優惠 --
			<li><a href="/eWeb_summittour/Prefecture/Prefecture01.asp"><img src="/eWeb_summittour/Prefecture/images/S_roation000.jpg" width="100%"></a></li>
            <!-- 航線促銷 -->
			<li><a href="/eWeb_summittour/page/China-09.asp"><img src="/eweb_summittour/images/movable/S_roation17-2.jpg" width="100%"></a></li>
			<li><a href="/eWeb_summittour/page/South-01.asp"><img src="/eweb_summittour/images/movable/S_roation16.jpg" width="100%"></a></li>
			<li><a href="/eWeb_summittour/page/China-17.asp"><img src="/eweb_summittour/images/movable/S_roation14-2.jpg" width="100%"></a></li>
			<li><a href="/eWeb_summittour/page/South-06.asp"><img src="/eweb_summittour/images/movable/S_roation13.jpg" width="100%"></a></li>
			
          </ul>
      <!-- 大圖輪播 end-->
          <!-- 搜尋引擎 start-->
      <div class="search-box">
      
      <iframe src="/eweb_summittour/Module/M_S.asp" allowtransparency="true" frameborder=no vspace=0 hspace=0 width="490" height="150" marginheight=0 marginwidth=0 scrolling=no></iframe>
      
      </div>
          <!-- 搜尋引擎 end-->
    </div>
    
    
<div style="display: none">        
    <div id="main_rgbox">
            <a href="http://www.summittour.com.tw/eWeb_summittour/page/display.asp"><img src="/eweb_summittour/images/movable/sum_banner04.gif"></a>
            <div class="themetext"></div>
 </div>
 
 
 
 </div>
             <!--保證出團 start-->
            <div id="main_rgbox">
            <div class="themetext">
			<h1>主題專區 <span>Theme Travel</span> </h1>
			</div>
			<!-- 過年 -->
			<!--a href="/eWeb_summittour/Prefecture/cny.html"><img src="/eweb_summittour/images/movable/sum_banner04.gif"></a-->
			<!-- 旅展 -->
			<!--a href="/eWeb_summittour/Prefecture/Prefecture04.html"><img src="/eweb_summittour/images/movable/sum_banner08.jpg" width="210" height="150"></a-->
			<!-- 專案 -->
            <!-- a href="/eWeb_summittour/Prefecture/Prefecture01.asp"><img src="/eweb_summittour/images/movable/moveimg04-1.jpg" width="100%"></a>
			<!-- 保證出團 -->
			<!-- a href="/eWeb/GO/L_GO_List_ASP.asp?SUB_CD=GO&TWFG=N&TVL_DAREA=&TVL_SAREA=&FROM_CD=&AIR_PT=&t_days=&bodjet=&CARR_CD=&SEL_TKT_BDL=2016/10/06&SEL_TKT_EDL=2017/12/31&AllowJoin=1&AllowWait=1&ikeyword=%E4%BF%9D%E8%AD%89%E5%87%BA%E5%9C%98"><img src="/eweb_summittour/images/movable/moveimg03-1.jpg" width="100%"></a>
            <!-- 線上DM -->      
            <a href="http://www.summittour.com.tw/eWeb/GO/L_GO_Type.asp?iMGRUP_CD=LZNB7R&iSUB_CD="><img src="/eweb_summittour/images/movable/moveimg6-3.jpg" width="100%"></a>
            </div>
            <!--保證出團 end-->
			
            <!--主題旅遊 start-->
			<!--<h1>主題專區 <span>Theme Travel</span> </h1>
            <div class="applemenu">
                <div class="silverheader"><a href=" ">線上DM</a></div>
                <div class="submenu">
					<a href="/eWeb_summittour/page/DM.asp"_blank">
						<img src="/eweb_summittour/images/movable/moveimg01.jpg">
					</a>
				</div>
                    
                <div class="silverheader"><a href=" " >北京、天津</a></div>
                <div class="submenu">
					<a href="/eWeb_summittour/page/China-07.asp"_blank"><!--圖片連結區--
						<img src="/eweb_summittour/images/movable/moveimg02.jpg"><!--顯示圖片--
					</a>
				</div>
					
                <!--
                <div class="silverheader"><a href=" ">廣東-珠海-深圳</a></div>
                <div class="submenu">
					<a href="/eWeb_summittour/page/China-16.asp"_blank"><!--圖片連結區--
						<img src="/eweb_summittour/images/movable/moveimg03.jpg">
					</a>
				</div>
                <!--
                <div class="silverheader"><a href=" ">澎湖</a></div>
                <div class="submenu">
					<a href="/eWeb_summittour/page/Taiwan-02.asp"_blank"><!--圖片連結區--
						<img src="/eweb_summittour/images/movable/moveimg04.jpg">
					</a>
				</div>		
            </div>
            
            <!--主題旅遊 end-->
    </div>
        
    <!-- main上半 左右側 (最新消息 大圖輪播 主題旅遊  搜尋引擎) end -->
    <div id="new_lf">
    <div class="lf-bg"> 
        <div class="gofast"><img src="/eweb_summittour/images/movable/gofast.jpg"></div>        
         
        
        <div class="gf-title">東北亞</div>
        <ul>
          <li><a href="/eWeb_summittour/page/North-01.asp">大阪-南紀-四國</a></li>
          <li><a href="/eWeb_summittour/page/North-02.asp">北海道</a></li>
          <li><a href="/eWeb_summittour/page/North-03.asp">東京</a></li>
          <!--li><a href="/eWeb_summittour/page/North-04.asp">立山黑部</a></li-->
          <li><a href="/eWeb_summittour/page/North-05.asp">九州-沖繩</a></li>
          <li><a href="/eWeb_summittour/page/North-06.asp">首爾-濟州島</a></li>
          <!--li><a href="/eWeb_summittour/page/North-07.asp">釜山</a></li-->
        </ul>
        
        <div class="gf-title">東南亞</div>
        <ul>
          <li><a href="/eWeb_summittour/page/South-01.asp">泰國曼谷</a></li><!--  -->
          <li><a href="/eWeb_summittour/page/South-02.asp">馬來西亞</a></li>
          <!--li><a href="/eWeb_summittour/page/South-03.asp">新加坡</a></li-->
          <li><a href="/eWeb_summittour/page/South-04.asp">菲律賓</a></li>
          <!--li><a href="/eWeb_summittour/page/South-05.asp">巴里島</a></li-->
		  <!--li><a href="/eWeb_summittour/page/South-06.asp">中南半島</a></li-->
		  <li><a href="/eWeb_summittour/page/South-07.asp">吳哥窟</a></li>
		  <li><a href="/eWeb_summittour/page/South-08.asp">越南</a></li>
		  
        </ul> 

		<!--div class="gf-title">南亞</div>
        <ul>
          <!--li><a href="/eWeb_summittour/page/Southern-01.asp">印度</a></li-->
          <!--li><a href="/eWeb_summittour/page/Southern-02.asp">斯里蘭卡</a></li-->
        <!--/ul-->
        
        <div class="gf-title">中國大陸</div>
        <ul>
          <!--li><a href="/eWeb_summittour/page/China-01.asp">東北、北朝鮮</a></li-->
          <!--li><a href="/eWeb_summittour/page/China-02.asp">西藏(青藏鐵路)</a></li-->
          <!--li><a href="/eWeb_summittour/page/China-03.asp">南北疆、銀川</a></li-->
		  <li><a href="/eWeb_summittour/page/China-04.asp">山東(青島)</a></li><!--  -->
          <!--li><a href="/eWeb_summittour/page/China-05.asp">山西-內蒙</a></li-->
          <li><a href="/eWeb_summittour/page/China-06.asp">鄭州-西安</a></li><!--  -->
          <!--li><a href="/eWeb_summittour/page/China-07.asp">北京、天津</a></li-->
          <!--li><a href="/eWeb_summittour/page/China-08.asp">雲南、貴州</a></li-->
          <li><a href="/eWeb_summittour/page/China-09.asp">四川-九寨溝</a></li>
          <li><a href="/eWeb_summittour/page/China-10.asp">長沙-張家界</a></li><!--  -->
          <li><a href="/eWeb_summittour/page/China-11.asp">黃山、合肥</a></li><!--  -->
          <!--li><a href="/eWeb_summittour/page/China-12.asp">長江三峽</a></li-->
          <!--li><a href="/eWeb_summittour/page/China-13.asp">江西(廬山婺源)</a></li-->
          <li><a href="/eWeb_summittour/page/China-14.asp">江南-杭州上海</a></li><!--  -->
          <li><a href="/eWeb_summittour/page/China-15.asp">廣西-桂林-南寧</a></li><!--  -->
          <li><a href="/eWeb_summittour/page/China-16.asp">廣東-珠海-深圳</a></li><!--  -->
          <!--li><a href="/eWeb_summittour/page/China-17.asp">海南島</a></li-->
          <!--li><a href="/eWeb_summittour/page/China-18.asp">福建廈門</a></li-->
          <!--li><a href="/eWeb_summittour/page/China-19.asp">港澳地區</a></li-->
          <!--li><a href="/eWeb_summittour/page/China-20.asp">港澳自由行</a></li-->
          <!--li><a href="/eWeb_summittour/page/China-21.asp">外蒙古專區</a></li-->
        </ul>
        
<div style=display:none>                
        <div class="gf-title">歐洲</div>
        <ul>
          <li><a href="/eWeb_summittour/page/Europe-01.asp">歐洲精選行程</a></li>
        </ul>
</div>
     
        <div class="gf-title">台灣</div>
        <ul>
          <!--li><a href="/eWeb_summittour/page/Taiwan-01.asp">金門</a></li-->
          <!--li><a href="/eWeb_summittour/page/Taiwan-02.asp">澎湖</a></li-->
          <li><a href="/eWeb_summittour/page/Taiwan-03.asp">馬祖</a></li><!--  -->
        </ul>
        
<div style=display:none>        
        <div class="gf-title">郵輪館</div>
        <ul>
			<li><a href="/eWeb_summittour/page/Cruises-01.asp">麗娜號</a></li><!--  -->
			<li><a href="/eWeb_summittour/page/Cruises-02.asp">海峽號</a></li><!--   -->          
			<li><a href="/eWeb_summittour/page/Cruises-03.asp">星夢郵輪</a></li><!--   --> 
			<li><a href="/eWeb_summittour/page/Cruises-04.asp">麗星郵輪</a></li><!--   --> 
        </ul>
</div>		
		<iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FsummittourTaiwan%2F&&width=220&height=160&small_header=true&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="220" height="140" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
		
	
    </div>
    <!-- 快速前往 三張圖 --
    <div class="banner-radius">
	
	
        <a href="http://www.summittour.com.tw/eWeb_summittour/page/South-02.asp">
        <img src="/eWeb_summittour/images/movable/lf-banner01.jpg" ></a>
        <a href="http://www.summittour.com.tw/eWeb_summittour/page/China-15.asp">
        <img src="/eWeb_summittour/images/movable/lf-banner02.jpg" ></a>
        <a href="http://www.summittour.com.tw/eWeb_summittour/page/North-05.asp">
        <img src="/eWeb_summittour/images/movable/lf-banner03.jpg" ></a>
    </div>
    <!-- 快速前往三張圖 end -->
    </div>
	
    
    <div id="new_rg">
	
	 <!-- 促銷行程三個選項頁籤 start -->
	
    <div class="change-box">
    
        <ul class="nav" id="myTab">
            <li class="active"><a href="#home" class="chmenu01">促銷行程</a>│</li>
            <li><a href="#profile" class="chmenu02">精選旅行</a>│</li>
            <li><a href="#messages" class="chmenu03">好康優惠</a></li>
        </ul>
    </div>
    <div class="tab-pane active" id="home">
      <div class="rg-outline">
      <iframe  src="/eWeb/Main/home.asp?SITE_CD=1&MP_ID=MPN61&STYLE_TP=0" allowtransparency="true" frameborder="0" vspace=0 hspace=0 width="233" height="230"  marginheight=0 marginwidth=0 scrolling=no ></iframe>
      </div>
      <div class="rg-mainline">
      <iframe  src="/eWeb/Main/home.asp?SITE_CD=1&MP_ID=MPN62&STYLE_TP=0" allowtransparency="true" frameborder="0" vspace=0 hspace=0 width="233" height="230"  marginheight=0 marginwidth=0 scrolling=no></iframe>
      </div>
      <div class="rg-outline">
      <iframe  src="/eWeb/Main/home.asp?SITE_CD=1&MP_ID=MPN63&STYLE_TP=0" allowtransparency="true" frameborder="0" vspace=0 hspace=0 width="233" height="230"  marginheight=0 marginwidth=0 scrolling=no></iframe>
      </div>
    </div>
  
    <div class="tab-pane" id="profile">
      <div class="rg-outline"><iframe  src="/eWeb/Main/home.asp?SITE_CD=1&MP_ID=MPN64&STYLE_TP=0" allowtransparency="true" frameborder="0" vspace=0 hspace=0 width="233" height="230"  marginheight=0 marginwidth=0 scrolling=no ></iframe></div>
      <div class="rg-mainline"><iframe  src="/eWeb/Main/home.asp?SITE_CD=1&MP_ID=MPN65&STYLE_TP=0" allowtransparency="true" frameborder="0" vspace=0 hspace=0 width="233" height="230"  marginheight=0 marginwidth=0 scrolling=no ></iframe></div>
      <div class="rg-outline"><iframe  src="/eWeb/Main/home.asp?SITE_CD=1&MP_ID=MPN66&STYLE_TP=0" allowtransparency="true" frameborder="0" vspace=0 hspace=0 width="233" height="230"  marginheight=0 marginwidth=0 scrolling=no ></iframe></div>
    </div>
   
    <div class="tab-pane" id="messages">
      <div class="rg-outline"><iframe  src="/eWeb/Main/home.asp?SITE_CD=1&MP_ID=MPN67&STYLE_TP=0" allowtransparency="true" frameborder="0" vspace=0 hspace=0 width="233" height="230"  marginheight=0 marginwidth=0 scrolling=no ></iframe></div>
      <div class="rg-mainline"><iframe  src="/eWeb/Main/home.asp?SITE_CD=1&MP_ID=MPN68&STYLE_TP=0" allowtransparency="true" frameborder="0" vspace=0 hspace=0 width="233" height="230"  marginheight=0 marginwidth=0 scrolling=no ></iframe></div>
      <div class="rg-outline"><iframe  src="/eWeb/Main/home.asp?SITE_CD=1&MP_ID=MPN69&STYLE_TP=0" allowtransparency="true" frameborder="0" vspace=0 hspace=0 width="233" height="230"  marginheight=0 marginwidth=0 scrolling=no ></iframe></div>
    </div>
    
    <div style="clear:both;"></div>
    <!-- 促銷行程三個選項頁籤 end -->  
	<!-- 促銷行程 start --
		<div class="recdicon" style="border-bottom: 1px #CCC dashed;
    font-weight: bold;
    margin-top: 10px;
    margin-bottom: 20px;
    width: 710px; font-size: 18pt; padding-left: 45px;">促銷行程 &nbsp;&nbsp;&nbsp;<span style="font-style: normal; font-size: 18px; color: #999;">On Sale</span></div>
	<div class="recdbg">
    <div class="slider4">
      <div class=" view slide">
          <img src="/eweb_summittour/images/movable/recpic01.jpg">
          <div class="mask">
          <a href="http://summittour.voyage.com.tw/eWeb/GO/L_GO_Type.asp?iMGRUP_CD=CTU0813&iSUB_CD=">
           <h2>游遍中國-環線九寨溝牟尼溝樂山峨嵋山熊貓基地八日(豪華版)</h2>
           <p>$19,900</p>
          </a>
          </div>
      </div>
      
      <div class=" view slide">
          <img src="/eweb_summittour/images/movable/recpic02.jpg">
          <div class="mask">
          <a href="http://summittour.voyage.com.tw/eWeb/GO/L_GO_Type.asp?iMGRUP_CD=MFMGEPR4A&iSUB_CD=">
           <h2>復興假期-珠海海洋王國、精彩澳門四天</h2>
           <p>$14,900</p>
          </a>
          </div>
      </div>
      
      <div class=" view slide">
          <img src="/eweb_summittour/images/movable/recpic03.jpg">
          <div class="mask">
          <a href="http://summittour.voyage.com.tw/eWeb/GO/L_GO_Type.asp?iMGRUP_CD=HAKGSPR4A&iSUB_CD=">
           <h2>傳奇海口─博鼇三亞風情四天</h2>
           <p>$11,900</p>
          </a>
          </div>
      </div>
            
      <div class=" view slide">
          <img src="/eweb_summittour/images/movable/recpic04.jpg">
          <div class="mask">
          <a href="http://summittour.voyage.com.tw/eWeb/GO/L_GO_Type.asp?iMGRUP_CD=CGO0802&iSUB_CD=">
           <h2>游遍中國─鄭州少林寺龍門石窟太行山峽谷許昌三國遺產八天</h2>
           <p>$30,800</p>
          </a>
          </div>
      </div>
	 </div>
	</div>
	<!-- 促銷行程 end --> 
	
	<div class="hoticon">
	  <h1>最新行程&nbsp;&nbsp;&nbsp;<span style="font-style: normal; font-size: 18px; color: #999;">固定航班路線、食宿，人數達標即出團。</span></h1>  
	</div>
	<div class="section__title">中國大陸</div>
	<div class="wrap--page"><a href="/eWeb/GO/L_GO_Type.asp?iMGRUP_CD=CTUCAPT8B"><img src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000060/000079/000143/00007727.JPG" width="230
	" >
	  <div class="content--brand">
		<p>藏族羌族文化-新都橋.海螺溝.丹巴.四姑娘山八日</p>
	  </div>
	  <div class="content--brand3">
		<h3>$40,900 起</h3>
	  </div></a>
	</div>
	<div class="wrap--page"><a href="/eWeb/GO/L_GO_Type.asp?iMGRUP_CD=CTU3UPT8A"><img src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000060/000079/000143/00007736.JPG" width="230
	" >
	  <div class="content--brand">
		<p>探秘甘孜-最後的香格里拉成都、稻城、亞丁單飛8日</p>
	  </div>
	  <div class="content--brand3">
		<h3>$50,900 起</h3>
	  </div></a>
	</div>
	<div class="wrap--page"><a href="/eWeb/GO/L_GO_Type.asp?iMGRUP_CD=CTU3UPT10A"><img src="/eWeb_summittour/IMGDB/000060/000079/000957/00007900.JPG" width="230
	" >
	  <div class="content--brand"> 
		<p>探秘甘孜-最後的香格里拉稻城亞丁+四姑娘山+木格措10日</p>
	  </div>
	  <div class="content--brand3">
		<h3>$46,900 起</h3>
	  </div></a>
	</div>
	
	
	
	<div class="section__title">東北亞</div>
	<div class="wrap--page"><a href="/eWeb/GO/L_GO_Type.asp?iMGRUP_CD=OKAIT04A"><img src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000182/000567/000601/00008099.JPG" width="230
	" >
	  <div class="content--brand">
		<p>台灣虎航 時尚沖繩小清新～黑潮探險．美國村紀行四日</p>
	  </div>
	  <div class="content--brand3">
		<h3>$17,900 起</h3>
	  </div></a>
	</div>
	<div class="wrap--page"><a href="/eWeb/GO/L_GO_Type.asp?iMGRUP_CD=KIXITPT5A"><img src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000182/000192/000198/00007780.JPG" width="230
	" >
	  <div class="content--brand">
		<p>精選關西大阪.鳥取.岡山後樂園.姬路城溫泉五日遊</p>
	  </div>
	  <div class="content--brand3">
		<h3>$31,900 起</h3>
	  </div></a>
	</div>
	<div class="wrap--page"><a href="/eWeb/GO/L_GO_Type.asp?iMGRUP_CD=OITAEP5B"><img src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000182/000196/000210/00007773.JPG" width="230
	" >
	  <div class="content--brand">
		<p>九州 湯布院 九重吊橋 柳川遊船 磯庭園5日</p>
	  </div>
	  <div class="content--brand3">
		<h3>$37,900 起</h3>
	  </div></a>
	</div>
	
	
	
	<div class="section__title">東南亞</div>
	<div class="wrap--page"><a href="/eWeb/GO/L_GO_Type.asp?iMGRUP_CD=BKKCIP5B"><img src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000098/000099/000104/00007838.JPG" width="230
	" >
	  <div class="content--brand">
		<p>FUN泰爽 五星酒店帝王蟹吃到飽+頂級SPA五日(6人成行)<br /> 
		</p>
	  </div>
	  <div class="content--brand3">
		<h3>$25,800 起</h3>
	  </div></a>
	</div>
	<div class="wrap--page"><a href="/eWeb/GO/L_GO_Type.asp?iMGRUP_CD=PPSPRT5A"><img src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000098/000113/000114/00007723.JPG" width="230
	" >
	  <div class="content--brand">
		<p>玩瘋了巴拉望精彩五日遊<br />
		</p>
	  </div>
	  <div class="content--brand3">
		<h3>$27,900 起</h3>
	  </div></a>
	</div>
	<div class="wrap--page"><a href="/eWeb/GO/L_GO_Type.asp?iMGRUP_CD=PNHJCP5B"><img src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000098/000481/000482/00007205.JPG" width="230
	" >
	  <div class="content--brand">
		<p>5★熱力雙星-金吳雙城5日(雙城雙飛雙秀1日券)<br />
		</p>
	  </div>
	  <div class="content--brand3">
		<h3>$22,800 起</h3>
	  </div></a>
	</div>
	
	
	
	
	
	
	
	<!--div class="section__title">南亞</div>
	<div class="wrap--page"><a href="/eWeb/GO/L_GO_Type.asp?iMGRUP_CD=COKODPT8A&iSUB_CD="><img src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000098/000493/000495/00006462.JPG" width="230
	" >
	  <div class="content--brand">
		<p>馬印假期─南印度喀拉拉邦迴水遊船阿育吠陀8日</p>
	  </div>
	  <div class="content--brand3">
		<h3>$55,900 起</h3>
	  </div></a>
	</div>
	<div class="wrap--page"><a href="/eWeb/GO/L_GO_Type.asp?iMGRUP_CD=CMBODPT8A&iSUB_CD="><img src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000098/000498/000500/00006475.JPG" width="230
	" >
	  <div class="content--brand">
		<p>馬印假期─斯里蘭卡生態文化吉隆坡雙城8日</p>
	  </div>
	  <div class="content--brand3">
		<h3>$49,900 起</h3>
	  </div></a>
	</div-->
	
	<div class="section__title">台灣</div>
	<div class="wrap--page"><a href="/eWeb/GO/L_GO_Type.asp?iMGRUP_CD=LZNB7R&iSUB_CD="><img src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000183/000366/000367/00005532.JPG" width="230
	" >
	  <div class="content--brand">
		<p>台中包機-馬祖卡蹓趣三日遊(南竿、北竿、藍眼淚大FUN送)</p>
	  </div>
	  <div class="content--brand3">
		<h3>$7,800 起</h3>
	  </div></a>
	</div>
	<div class="section__title"></div-->
	 <!-- 最新行程 start --
	 <div class="hoticon">最新行程 <span>Latest Travel</span></div>
	 <iframe  src="/eWeb/Main/home.asp?SITE_CD=1&MP_ID=MPN61&STYLE_TP=0" allowtransparency="true" frameborder="0" vspace=0 hspace=0 width="233" height="230"  marginheight=0 marginwidth=0 scrolling=no ></iframe>
	 <iframe  src="/eWeb/Main/home.asp?SITE_CD=1&MP_ID=MPN62&STYLE_TP=0" allowtransparency="true" frameborder="0" vspace=0 hspace=0 width="233" height="230"  marginheight=0 marginwidth=0 scrolling=no ></iframe>
	 <iframe  src="/eWeb/Main/home.asp?SITE_CD=1&MP_ID=MPN63&STYLE_TP=0" allowtransparency="true" frameborder="0" vspace=0 hspace=0 width="233" height="230"  marginheight=0 marginwidth=0 scrolling=no ></iframe>
	 <iframe  src="/eWeb/Main/home.asp?SITE_CD=1&MP_ID=MPN64&STYLE_TP=0" allowtransparency="true" frameborder="0" vspace=0 hspace=0 width="233" height="230"  marginheight=0 marginwidth=0 scrolling=no ></iframe>
	 <iframe  src="/eWeb/Main/home.asp?SITE_CD=1&MP_ID=MPN65&STYLE_TP=0" allowtransparency="true" frameborder="0" vspace=0 hspace=0 width="233" height="230"  marginheight=0 marginwidth=0 scrolling=no ></iframe>
	 <iframe  src="/eWeb/Main/home.asp?SITE_CD=1&MP_ID=MPN66&STYLE_TP=0" allowtransparency="true" frameborder="0" vspace=0 hspace=0 width="233" height="230"  marginheight=0 marginwidth=0 scrolling=no ></iframe>
	 <iframe  src="/eWeb/Main/home.asp?SITE_CD=1&MP_ID=MPN67&STYLE_TP=0" allowtransparency="true" frameborder="0" vspace=0 hspace=0 width="233" height="230"  marginheight=0 marginwidth=0 scrolling=no ></iframe>
	 <iframe  src="/eWeb/Main/home.asp?SITE_CD=1&MP_ID=MPN68&STYLE_TP=0" allowtransparency="true" frameborder="0" vspace=0 hspace=0 width="233" height="230"  marginheight=0 marginwidth=0 scrolling=no ></iframe>
	 <iframe  src="/eWeb/Main/home.asp?SITE_CD=1&MP_ID=MPN69&STYLE_TP=0" allowtransparency="true" frameborder="0" vspace=0 hspace=0 width="233" height="230"  marginheight=0 marginwidth=0 scrolling=no ></iframe>
	 <iframe  src="/eWeb/Main/home.asp?SITE_CD=1&MP_ID=MPN70&STYLE_TP=0" allowtransparency="true" frameborder="0" vspace=0 hspace=0 width="233" height="230"  marginheight=0 marginwidth=0 scrolling=no ></iframe>
	 <iframe  src="/eWeb/Main/home.asp?SITE_CD=1&MP_ID=MPN71&STYLE_TP=0" allowtransparency="true" frameborder="0" vspace=0 hspace=0 width="233" height="230"  marginheight=0 marginwidth=0 scrolling=no ></iframe>
	 <iframe  src="/eWeb/Main/home.asp?SITE_CD=1&MP_ID=MPN72&STYLE_TP=0" allowtransparency="true" frameborder="0" vspace=0 hspace=0 width="233" height="230"  marginheight=0 marginwidth=0 scrolling=no ></iframe>
	 
	 <!-- 最新行程 start -->
     
    
   
    <!-- 熱門行程 start --
    <div class="hoticon">熱門行程 <span>Popular Tours</span></div>
    <div class="hotbox01">
   <iframe src="/eWeb/Main/home.asp?SITE_CD=1&MP_ID=MPN81&STYLE_TP=0" allowtransparency="true" frameborder="0" vspace="0" hspace="0" width="228" height="270" marginheight="0" marginwidth="0" scrolling="no"></iframe>
    </div>
    <div class="hotbox01">
    <iframe src="/eWeb/Main/home.asp?SITE_CD=1&MP_ID=MPN82&STYLE_TP=0" allowtransparency="true" frameborder="0" vspace="0" hspace="0" width="228" height="270" marginheight="0" marginwidth="0" scrolling="no"></iframe>
    </div>
    <div class="hotbox01">
    <iframe src="/eWeb/Main/home.asp?SITE_CD=1&MP_ID=MPN83&STYLE_TP=0" allowtransparency="true" frameborder="0" vspace="0" hspace="0" width="228" height="270" marginheight="0" marginwidth="0" scrolling="no"></iframe>
    </div>
    <div class="hotbox01">
    <iframe src="/eWeb/Main/home.asp?SITE_CD=1&MP_ID=MPN84&STYLE_TP=0" allowtransparency="true" frameborder="0" vspace="0" hspace="0" width="228" height="270" marginheight="0" marginwidth="0" scrolling="no"></iframe>
    </div>
    <div class="hotbox01">
    <iframe src="/eWeb/Main/home.asp?SITE_CD=1&MP_ID=MPN85&STYLE_TP=0" allowtransparency="true" frameborder="0" vspace="0" hspace="0" width="228" height="270" marginheight="0" marginwidth="0" scrolling="no"></iframe>
    </div>
    <div class="hotbox01">
    <iframe src="/eWeb/Main/home.asp?SITE_CD=1&MP_ID=MPN86&STYLE_TP=0" allowtransparency="true" frameborder="0" vspace="0" hspace="0" width="228" height="270" marginheight="0" marginwidth="0" scrolling="no"></iframe>
    </div>
    
    <div style="clear:both;"></div>
    <!-- 熱門行程 end -->
    
    <!-- 推薦行程 start --
    <div class="recdicon">推薦行程 <span>Recommend Tours</span></div>
    
    <div class="recdbg">
    <div class="slider4">
      

      
      <div class=" view slide">
          <img src="/eweb_summittour/images/movable/recpic01.jpg">
          <div class="mask">
          <a href="http://summittour.voyage.com.tw/eWeb/GO/L_GO_Type.asp?iMGRUP_CD=CTU0813&iSUB_CD=">
           <h2>游遍中國-環線九寨溝牟尼溝樂山峨嵋山熊貓基地八日(豪華版)</h2>
           <p>$19,900</p>
          </a>
          </div>
      </div>
      
      <div class=" view slide">
          <img src="/eweb_summittour/images/movable/recpic02.jpg">
          <div class="mask">
          <a href="http://summittour.voyage.com.tw/eWeb/GO/L_GO_Type.asp?iMGRUP_CD=MFMGEPR4A&iSUB_CD=">
           <h2>復興假期-珠海海洋王國、精彩澳門四天</h2>
           <p>$14,900</p>
          </a>
          </div>
      </div>
      
      <div class=" view slide">
          <img src="/eweb_summittour/images/movable/recpic03.jpg">
          <div class="mask">
          <a href="http://summittour.voyage.com.tw/eWeb/GO/L_GO_Type.asp?iMGRUP_CD=HAKGSPR4A&iSUB_CD=">
           <h2>傳奇海口─博鼇三亞風情四天</h2>
           <p>$11,900</p>
          </a>
          </div>
      </div>
            
      <div class=" view slide">
          <img src="/eweb_summittour/images/movable/recpic04.jpg">
          <div class="mask">
          <a href="http://summittour.voyage.com.tw/eWeb/GO/L_GO_Type.asp?iMGRUP_CD=CGO0802&iSUB_CD=">
           <h2>游遍中國─鄭州少林寺龍門石窟太行山峽谷許昌三國遺產八天</h2>
           <p>$30,800</p>
          </a>
          </div>
      </div>
      
    
    
    
    
    
    </div></marquee></ul>
    <!-- 推薦行程 end -->
    </div>
  </div>
    </div>
</div>
<div id="web_Son_tail">
<!--#include virtual="/eweb_summittour/public/inc_tail.asp" -->
</div>
    
</body>
</html>
<script language="JavaScript" type="text/JavaScript">
if(parent.document.getElementById("web_Parent_top")){
	if(document.getElementById("web_Son_top")){
		parent.document.getElementById("web_Parent_top").innerHTML=document.getElementById("web_Son_top").innerHTML;
	}
}
if(parent.document.getElementById("web_Parent_left")){
	parent.document.getElementById("web_Parent_left").width=1;
	if(document.getElementById("web_Son_left")){
		parent.document.getElementById("web_Parent_left").innerHTML=document.getElementById("web_Son_left").innerHTML;
	}
}
if(parent.document.getElementById("web_Parent_right")){
	parent.document.getElementById("web_Parent_right").width=1;
	if(document.getElementById("web_Son_right")){
		parent.document.getElementById("web_Parent_right").innerHTML=document.getElementById("web_Son_right").innerHTML;
	}
}
if(parent.document.getElementById("web_Parent_main")){
	parent.document.getElementById("web_Parent_main").width=960;
	if(document.getElementById("web_Son_main")){
		parent.document.getElementById("web_Parent_main").innerHTML=document.getElementById("web_Son_main").innerHTML;
	}
}
if(parent.document.getElementById("web_Parent_tail")){
	if(document.getElementById("web_Son_tail")){
		parent.document.getElementById("web_Parent_tail").innerHTML=document.getElementById("web_Son_tail").innerHTML;
	}
}
var pbody = parent.document.body;          <!--背景圖設定-->                                               
$(pbody).css("background", "url(/eweb_summittour/images/stable/bg.jpg)");     
$(pbody).css("background-repeat","repeat");      
$(pbody).css("background-position","top center"); 
</script>
<script type="text/javascript" charset="utf-8">
  $(document).ready(function(){
	$('#myTab a').click(function (e) {
	  e.preventDefault();
	  $(this).tab('show');
	});
  });
</script>


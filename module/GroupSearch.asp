<!--#include Virtual="/include/egrpDSN_binary.inc" -->
<!--#include Virtual="/include/Func_Dao.asp" -->
<!--#include virtual="/EW/services/FileTools.inc" -->
<!--#include Virtual="/eweb/Public/eWeb_Defense_func.asp" -->
<!--#include Virtual="/EW/Services/ApiConfig.inc" -->
<!--#include Virtual="/EW/Services/SiteAdmAPI.inc" -->
<!--#include Virtual="/EW/Services/json2.asp" -->
<!--#include Virtual="/EW/Services/SECoreAPI.inc" -->
<!--#include Virtual="/EW/Services/JSONTools.inc" -->
<!--#include Virtual="/EW/Services/SECondition.inc" -->
<!--#include Virtual="/EW/Services/PageList.inc" -->
<!--#include Virtual="/EW/Services/SearchList.inc" -->
<!--#include Virtual="/EW/Services/Member.inc" -->
<!--#include Virtual="/EW/Services/SiteSet.inc" -->
<!--#include Virtual="/include/Func_SECore.asp" -->
<!--#include Virtual="/EW/GO/GroupList.inc" -->
<% 
dim f_debug         : f_debug       = Cbool(Request("debug_fg"))
dim searchMode      : searchMode    = "G"		'M:團型 / G:個團'
dim srcCls			: srcCls		= "D"		'D:直客 / A:同業'
if session("SRC_CLS")	<>"" then srcCls 		= session("SRC_CLS")
%>
<!--取得資料-->
<!--#include Virtual="/EW/GO/SearchListResult.inc" -->

<!--搜尋引擎-->
<section class="merge_search">
    <h4><!-- 標題名稱，由 CSS 控制 --></h4>
    <form method="post" id="GO_search_FORM" name="GO_search_FORM" target="_parent" action="/EW/GO/GroupList.asp">
        <input type="hidden" id="displayType" name="displayType" value="<%=searchMode%>"><!--StrUspType-->
		<input type="hidden" id="subCd" name="subCd" value="<%=subCd%>">
        <input type="hidden" id="keyword" name="keyword" value="<%=encodeKeyword%>">
        <input type="hidden" id="orderCd" name="orderCd" value="<%=orderCd%>">
        <input type="hidden" id="pageALL" name="pageALL" value="1">
        <input type="hidden" id="pageGO"  name="pageGO" value="1">
        <input type="hidden" id="pagePGO" name="pagePGO" value="1">
        <input type="hidden" id="waitData" name="waitData" value="false">
        <input type="hidden" id="waitPage" name="waitPage" value="false">
        <input type="hidden" id="mGrupCd" name="mGrupCd" value='<%=Request("mGrupCd")%>'>
        <input type="hidden" id="SrcCls" name="SrcCls" value='<%=Session("SRC_CLS")%>'>

        <ul>
            <!--旅遊區域-->
            <li class="col-xs-12 col-sm-12 col-md-12 col-lg-6 merge_search_area">
                <label><i class="fa fa-asterisk" aria-hidden="true"></i>旅遊區域：</label>
				<select id="regmCd" name="regmCd" class="form-control merge_search_select" onChange="seModule.updOpt(this)"><%=GetOptStr(JSONregm)%></select>
			  <select id="regsCd" name="regsCd" class="form-control merge_search_select" onChange="seModule.updOpt(this)"><%=GetOptStr(JSONregs)%></select>
            </li>
      
            <!--出發期間-->
            <li class="col-xs-12 col-sm-12 col-md-12 col-lg-6 merge_search_date">
                <label><i class="fa fa-asterisk" aria-hidden="true"></i>日期：</label>
                <div class="input-group date form_date" data-date="" data-date-format="yyyy/MM/dd" data-link-field="dtp_input1" data-link-format="yyyy-mm-dd">
                    <input type="text" id="beginDt" name="beginDt" value="<%=defBeginDt%>" class="form-control merge_search_input_date" readonly="readonly">
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                </div>
                <div class="input-group date form_date" data-date="" data-date-format="yyyy/MM/dd" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
                    <input type="text" id="endDt" name="endDt" value="<%=defEndDt%>" class="form-control merge_search_input_date" readonly="readonly">
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                </div>
                <input type="hidden" id="dtp_input1" value=""><!--br-->
                <input type="hidden" id="dtp_input2" value=""><!--br-->
            </li>


            <!--出境機場-->
            <li class="col-xs-12 col-sm-12 col-md-12 col-lg-3 merge_search_airport">
                <label>出境機場：</label>
                <select id="portCd" name="portCd" class="form-control merge_search_select" onChange="seModule.updOpt(this)"><%=GetOptStr(JSONport)%></select>
            </li>

            <!--旅遊天數-->
            <li class="col-xs-12 col-sm-12 col-md-12 col-lg-3 merge_search_days">
                <label>旅遊天數：</label>
				<select id="tdays" name="tdays" class="form-control merge_search_select" onChange="seModule.updOpt(this)"><%=GetOptStr(JSONtday)%></select>
            </li>

            <!--每人預算-->
            <li class="col-xs-12 col-sm-12 col-md-12 col-lg-3 merge_search_budget">
                <label>每人預算：</label>
				<select id="bjt" name="bjt" class="form-control merge_search_select" onChange="seModule.updOpt(this)"><%=GetOptStr(JSONbjt)%></select>
            </li>

            <!--航空公司-->
            <li class="col-xs-12 col-sm-12 col-md-12 col-lg-3 merge_search_airline">
                <label>航空公司：</label>
				<select id="carr" name="carr" class="form-control merge_search_select" onChange="seModule.updOpt(this)"><%=GetOptStr(JSONcarr)%></select>
            </li>
            
            <!--合併其他選項-->

            <!--產品快搜-->
            <li class="col-xs-12 col-sm-12 col-md-12 col-lg-6 merge_search_keyword">
                <label>產品快搜：</label>
				<input type="text" id="ikeyword" name="ikeyword" class="form-control merge_search_input" value="<%=ikeyword%>" placeholder="請輸入關鍵字" onChange="seModule.ConvStr();">
            </li>
            <li class="col-xs-12 col-sm-12 col-md-12 col-lg-6 merge_search_combine">
				<input type="checkbox" id="allowJoin" name="allowJoin" class="css-checkbox" value="1" <%=DefAllowJoin%>>
                <label for="allowJoin" class="css-label">可報名</label>

				<input type="checkbox" id="allowWait" name="allowWait" class="css-checkbox" value="1" <%=DefAllowWait%>>
                <label for="allowWait" class="css-label">可候補</label>
				
				<input type="checkbox" id="promote" name="promote" class="css-checkbox" value="1" <%=Defpromote%>>
				<label for="promote" class="css-label">促銷</label>
				
				<input type="checkbox" id="guarantee" name="guarantee" class="css-checkbox" value="1" <%=Defguarantee%>>
				<label for="guarantee" class="css-label">保證出發</label>
			</li>
            <li>
                <!--搜尋引擎按鈕-->
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 merge_search_btn">
                    <a onClick="document.getElementById('GO_search_FORM').submit();" title="搜尋" class="btn btn-success" id="searchBtn"><i class="fa fa-search" aria-hidden="true"></i>搜尋</a>
                    <a onClick="javascript:document.getElementById('GO_search_FORM').reset();" title="清除" class="btn btn-default"><i class="fa fa-repeat" aria-hidden="true"></i>清除</a>
                </div>	
            </li>
        </ul>

    </form>
</section>



<!--<script type="text/javascript" language="javascript" src="/eweb_<%=iWeb_Id%>/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" language="javascript" src="/eweb_<%=iWeb_Id%>/js/bootstrap.min.js"></script>-->
<script type="text/javascript" language="javascript" src="/eweb_<%=iWeb_Id%>/js/jquery.nicescroll.min.js"></script>
<script type="text/javascript" language="javascript" src="/eweb_<%=iWeb_Id%>/js/modernizr.js"></script>
<script type="text/javascript" language="javascript" src="/eweb_<%=iWeb_Id%>/js/all.js"></script>
<script type="text/javascript" language="javascript" src="/eweb_<%=iWeb_Id%>/js/imagesLoaded.js"></script>
<script type="text/javascript" language="javascript" src="/eweb_<%=iWeb_Id%>/js/masonry.pkgd.min.js"></script>
<script type="text/javascript" language="javascript" src="/eweb_<%=iWeb_Id%>/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script type="text/javascript" language="javascript" src="/eweb_<%=iWeb_Id%>/js/locales/bootstrap-datetimepicker.zh-TW.js" charset="UTF-8"></script>
<script type="text/javascript" language="javascript" src="/eweb_<%=iWeb_Id%>/js/jquery.cookie.js"></script>
<script src="https://d.line-scdn.net/r/web/social-plugin/js/thirdparty/loader.min.js" async="async" defer="defer"></script>

<!--#include Virtual="/EW/Script/Init/SearchResultInit.asp" -->
<script src="/EW/Script/Module/FlightModule.js" type="text/javascript"></script>
<script src="/EW/Script/Module/SEModule.js" type="text/javascript"></script>
<script src="/EW/Script/Module/SearchResultModule.js" type="text/javascript"></script>
<script src="/EW/Script/jquery.tmpl.min.js" type="text/javascript"></script>
<!--#include Virtual="/EW/Script/Template/tmplMenu.asp" -->
<script>
 searchList.init();
</script>

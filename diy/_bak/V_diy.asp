﻿<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<%
'=== Hamburger ^__^||| ===============
'程式名稱：V_diy.asp
'功能描述：量身定做畫面
'使用資料：
'使用元件：
'適用對象：網路使用者
'使用時機：
'包含程式： /INCLUDE/egrpDSN_binary.inc,
'			/eWeb_bobby/Public/G_top.asp,G_Menu.asp,G_tail.asp
'			/eWeb_bobby/diy/N_diy.asp, N_diy_su.asp						
'母程式：  
'加註日期：	2003/06/19
'更新日期：	2003/06/19
'更新說明：
'			2003/06/19	新增共用路徑
'			2004-08-18	[BRIAN]	配合保保修改發送郵件地址	LINE 
'			2004-09-07	[brian]	將預設的樣板代號取消	line 25 
'			2006-03-02	[brian]	調整顯示文件內容函數
'			2006-06-30	[Rebo]	若myG_NATN=CN則介面導向至"N_diy_ESH.asp"
'			2012-06-27  [Kevin] [ew.v1010.222U] Mail發送機制 前台頁面增頁處理
'			2013-04-11	[Aron][ew.v1209.349U]世界eweb/diy移至客製化目錄，並修改code
'			2013-07-08	[Aron][ew.v1209.454.B]修改找不到include檔的錯誤
'=====================================
SITE_CD=Request("SITE_CD")
MP_ID	=Request("MP_ID")
STYLE_TP=Request("STYLE_TP")
'	==========		brian	2006-03-02	將文字檔案取出讀取某一行	==========
Function ReadText(iURL,iLen)
	dim oText
	oText = ""
	Set objFile=Server.CreateObject("Scripting.FileSystemObject")
	FilePath=Server.MapPath(iURL)
	if objFile.FileExists(FilePath) then
		Set ObjStream=objFile.OpenTextfile(FilePath,1,False,False)
		if iLen-1<0 then
			oText=ObjStream.ReadAll
		else
			if iLen -1>0 then
				for x = 1 to iLen-1
					ObjStream.SkipLine
				next
			end if
			oText=ObjStream.ReadLine
		end if		
	else
		oText=""
	end if
	ObjStream.Close
	Set objFile=Nothing		
	ReadText = oText
End Function
'	==========		brian	2006-03-02	將文字檔案取出讀取某一行	==========
%>
<!--#include virtual="/include/myFunc.inc" -->
<!--#INCLUDE VIRTUAL="/INCLUDE/egrpDSN_binary.inc" -->
<!--#include virtual="/eWeb/Style/style_setting.asp" -->
<!--#include virtual="/include/Send_OrderMail.asp" -->
<%'基本參數值'
Sub Main()
str_TITLE=ReadText("/eWeb_"&iWEB_ID&"/Public/DIY_TITLE.txt",1)%>
<a name="top"></a>
<style type="text/css">
.order-all-table-width hr{
	text-align:center;
	margin:10px auto;
	width:940px;
	border:0; height:1px; background-color:#d4d4d4;
	color:#d4d4d4	/* IE6 */
}
h3.order-step-title{
	font-size:16px;
	height:40px;
	line-height:40px;
	text-align:center;
}
#css_table {
	display:table;
	width:940px;
	margin:0 auto;
}
.css_tr {
	display: table-row;
}
.css_td {
	display: table-cell;
}
h5.order-contant-text01{
	margin:10px 10px 10px 25px;
}
h5.order-contant-text02{
	margin:10px 10px 10px 25px;
}
.input_buttom{
	text-align:center;
	height:50px;
}
/*.myButton {
	font-family:"微軟正黑體";
	-moz-box-shadow:inset 0px 1px 0px 0px #bee2f9;
	-webkit-box-shadow:inset 0px 1px 0px 0px #bee2f9;
	box-shadow:inset 0px 1px 0px 0px #bee2f9;
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #63b8ee), color-stop(1, #468ccf));
	background:-moz-linear-gradient(top, #63b8ee 5%, #468ccf 100%);
	background:-webkit-linear-gradient(top, #63b8ee 5%, #468ccf 100%);
	background:-o-linear-gradient(top, #63b8ee 5%, #468ccf 100%);
	background:-ms-linear-gradient(top, #63b8ee 5%, #468ccf 100%);
	background:linear-gradient(to bottom, #63b8ee 5%, #468ccf 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#63b8ee', endColorstr='#468ccf',GradientType=0);
	background-color:#63b8ee;
	-moz-border-radius:6px;
	-webkit-border-radius:6px;
	border-radius:6px;
	border:1px solid #3866a3;
	display:inline-block;
	cursor:pointer;
	color:#14396a;
	font-family:arial;
	font-size:15px;
	font-weight:bold;
	padding:6px 24px;
	text-decoration:none;
	text-shadow:0px 1px 0px #7cacde;
}*/
.myButton {
font-family: "微軟正黑體";
-moz-box-shadow: inset 0px 1px 0px 0px #bee2f9;
-webkit-box-shadow: inset 0px 1px 0px 0px #bee2f9;
box-shadow: inset 0px 1px 0px 0px #bee2f9;
background: -webkit-gradient(linear, left top, left bottom, color-stop(0.05, #097c25), color-stop(1, #8fc31f));
background: -moz-linear-gradient(top, #097c25 5%, #8fc31f 100%);
background: -webkit-linear-gradient(top, #097c25 5%, #8fc31f 100%);
background: -o-linear-gradient(top, #097c25 5%,#8fc31f 100%);
background: -ms-linear-gradient(top, #097c25 5%, #8fc31f 100%);
background: linear-gradient(to bottom, #097c25 5%, #8fc31f 100%);
filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#097c25', endColorstr='#8fc31f',GradientType=0);
background-color: #DFF5B8;
-moz-border-radius: 6px;
-webkit-border-radius: 6px;
border-radius: 6px;
border: 1px solid #097c25;
display: inline-block;
cursor: pointer;
color: #FFFFFF;
font-family: arial;
font-size: 15px;
font-weight: bold;
padding: 6px 24px;
text-decoration: none;
/* text-shadow: 0px 1px 0px #4D4D4D; */
}
.myButton:hover {
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #8fc31f), color-stop(1, #097c25));
	background:-moz-linear-gradient(top, #8fc31f 5%, #097c25 100%);
	background:-webkit-linear-gradient(top, #8fc31f 5%, #097c25 100%);
	background:-o-linear-gradient(top, #8fc31f 5%, #097c25 100%);
	background:-ms-linear-gradient(top, #8fc31f 5%, #097c25 100%);
	background:linear-gradient(to bottom, #8fc31f 5%, #097c25 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#8fc31f', endColorstr='#097c25',GradientType=0);
	background-color:#8fc31f;
}
.myButton:active {
	position:relative;
	top:1px;
}
.order-input-text01 input{height:23px;line-height:23px; vertical-align:middle;}
.order-input-text01 select{height:23px;line-height:23px;}
</style>
<div class="order-all-table-width">
<h3  class="order-step-title"><%= str_TITLE %></h3>
<%						inc=request("inc")
						if inc="" then
							If myG_NATN = "CN" Then%>
								<!--#include virtual="/eweb_summittour/DIY/N_diy_ESH.asp" -->							
							<%Else%> 																
								<!--#include virtual="/eweb_summittour/DIY/N_diy.asp" -->
<%							End If
						end if
						if inc="su" then%> 
							<!--#include virtual="/eweb_summittour/DIY/N_diy_su.asp" -->
<%						end if%>
</div>
<% End Sub %>

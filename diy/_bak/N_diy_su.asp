﻿<%
'	=================================================
'	更新記錄:
'				2005-02-17	[brian]	新增取得郵件條件為主要聯絡人和利用序號排序取得第一筆郵件
'				2007-11-08	[poison] 針對良友與樂遊調整需求單主旨,使其能分辨來源
'				2007-12-07  [poison] 調整網站預設首頁取MPSITEDTL中的EWEB_HOME_PAGE,若為空值則取/eWeb/Main/home.asp
'				2012-06-27  [Kevin] [ew.v1010.222U] Mail發送機制 前台頁面增頁處理
'				2012-11-27  [Glenn][wm.v1012.161.U]調整SQL物件開啟使用ADO方式
'	=================================================
'2007-12-07 poison 將網站預設頁改為iEWEB_HOME_PAGE值
iEWEB_HOME_PAGE = GetSiteDTL("OT","EWEB_HOME_PAGE")
if iEWEB_HOME_PAGE = "" Then
	iEWEB_HOME_PAGE = "/eWeb/Main/home.asp"
end if
set RS = Server.CreateObject("ADODB.Recordset")
set bRS = Server.CreateObject("ADODB.Recordset")
'=== 取得系統日期 =======
toDate=Date
toYear=Year(toDate)
toMonth=Month(toDate)
if len(toMonth)<2 then toMonth = "0" & toMonth
toDay=Day(toDate)
if len(toDay)<2 then toDay = "0" & toDay
'=== 取得表單參數 =======
DIY_TP=request("from")
DIY_PNM=request("DIY_PNM")
DIY_NM=request("DIY_NM")
DIY_PJOB=request("DIY_PJOB") & "(" & request("DIY_PJOB1") & ")"
DIY_EXP=request("DIY_EXP")
DIY_EXPPLACE=request("DIY_EXPPLACE")
DIY_TELNO=request("DIY_TELNO")
DIY_FAXNO=request("DIY_FAXNO")
DIY_MOBILE=request("DIY_MOBILE")
DIY_EMAIL=request("DIY_EMAIL")
NEED_AIRPORT=request("NEED_AIRPORT")
DIY_BUDGET=request("DIY_BUDGET")
TOT_DAYS=request("TOT_DAYS")
MAN_CNT=request("MAN_CNT")
CHD_CNT=request("CHD_CNT")
BABY_CNT=request("BABY_CNT")
DIY_PURPOSE=request("DIY_PURPOSE")
if request("DIY_PURPOSE_e") <> "" then
	DIY_PURPOSE=DIY_PURPOSE&request("DIY_PURPOSE_e")
end if
DIY_MODE=request("DIY_MODE")
if request("DIY_MODE_e") <> "" then
	DIY_MODE=DIY_MODE&request("DIY_MODE_e")
end if
DIY_TOPLACE=request("DIY_TOPLACE")
if request("DIY_TOPLACE_e") <> "" then
	DIY_TOPLACE=DIY_TOPLACE&request("DIY_TOPLACE_e")
end if
DIY_RK=request("DIY_RK")
'=== 日期整理 ===
s_year=request("s_year")
s_month=request("s_month")
s_day=request("s_day")
if s_year="" then
	if s_month="" then
		OUT_BDT=""
	else
		if s_day="" then
			OUT_BDT=toYear&"/"&s_month&"/"&"01"
		else
			OUT_BDT=toYear&"/"&s_month&"/"&s_day
		end if
	end if
else
	if s_year = toYear then
		if s_month < toMonth or s_month = "" then
			s_month = toMonth
			s_day = toDay
		end if
	else
		if s_month = "" then
			s_month = "01"
			s_day="01"
		else
			if s_day = "" then
				s_day = "01"
			end if
		end if		
	end if
	OUT_BDT = s_year&"/"&s_month&"/"&s_day
end if
e_year=request("e_year")
e_month=request("e_month")
e_day=request("e_day")
if e_year="" then
	if e_month="" then
		OUT_EDT=""
	else
		if e_day="" then
			select case e_month
				case "01","03","05","07","08","10","12"
					e_day = "31"
				case "02"
					if (toYear mod 4 = 0 and toYear mod 100 <> 0) or (toYear mod 400 = 0) then
						e_day = "29"
					else
						e_day = "28"
					end if
				case else
					e_day = "30"
			end select
		end if
		OUT_EDT=toYear&"/"&e_month&"/"&e_day
	end if
else
	if e_year = toYear then
		if e_month < toMonth or e_month = "" then
			e_month = toMonth
			e_day = toDay
		end if
	else
		if e_month = "" then
			e_month = "12"
			e_day="31"
		else
			if e_day = "" then
				select case e_month
					case "01","03","05","07","08","10","12"
						e_day = "31"
					case "02"
						if (e_year mod 4 = 0 and e_year mod 100 <> 0) or (e_year mod 400 = 0) then
							e_day = "29"
						else
							e_day = "28"
						end if
					case else
						e_day = "30"
				end select
			end if
		end if		
	end if
	OUT_EDT = e_year&"/"&e_month&"/"&e_day
end if
strSQL = "select * from TRDIYRQFRM "
							strSQL = strSQL & " where DIY_PNM = '" & FixForSQL(DIY_PNM) & "' "
if DIY_PJOB <> "" then		strSQL = strSQL & " and DIY_PJOB = '" & FixForSQL(DIY_PJOB) & "' "
if DIY_EXP <> "" then		strSQL = strSQL & " and DIY_EXP = '" & FixForSQL(DIY_EXP) & "' "
if DIY_EXPPLACE <> "" then	strSQL = strSQL & " and DIY_EXPPLACE = '" & FixForSQL(DIY_EXPPLACE) & "' "
if DIY_TELNO <> "" then		strSQL = strSQL & " and DIY_TELNO = '" & FixForSQL(DIY_TELNO) & "' "
if DIY_FAXNO <> "" then		strSQL = strSQL & " and DIY_FAXNO = '" & FixForSQL(DIY_FAXNO) & "' "
if DIY_MOBILE <> "" then	strSQL = strSQL & " and DIY_MOBILE = '" & FixForSQL(DIY_MOBILE) & "' "
							strSQL = strSQL & " and DIY_EMAIL = '" & FixForSQL(DIY_EMAIL) & "' "
if NEED_AIRPORT <> "" then	strSQL = strSQL & " and NEED_AIRPORT = '" & FixForSQL(NEED_AIRPORT) & "' "
if DIY_BUDGET <> "" then	strSQL = strSQL & " and DIY_BUDGET = '" & FixForSQL(DIY_BUDGET) & "' "
if TOT_DAYS <> "" then		strSQL = strSQL & " and TOT_DAYS = '" & FixForSQL(TOT_DAYS) & "' "
if MAN_CNT <> "" then		strSQL = strSQL & " and MAN_CNT = " & Cint(MAN_CNT)
if CHD_CNT <> "" then		strSQL = strSQL & " and CHD_CNT = " & Cint(CHD_CNT)
if BABY_CNT <> "" then		strSQL = strSQL & " and BABY_CNT = " & Cint(BABY_CNT)
if DIY_PURPOSE <> "" then	strSQL = strSQL & " and DIY_PURPOSE = '" & FixForSQL(DIY_PURPOSE) & "' "
							strSQL = strSQL & " and DIY_MODE = '" & FixForSQL(DIY_MODE) & "' "
							strSQL = strSQL & " and DIY_TOPLACE = '" & FixForSQL(DIY_TOPLACE) & "' "
if DIY_RK <> "" then		strSQL = strSQL & " and DIY_RK like '" & FixForSQL(DIY_RK) & "' "
if OUT_BDT <> "" then		strSQL = strSQL & " and OUT_BDT = '" & OUT_BDT & "' "
if OUT_EDT <> "" then		strSQL = strSQL & " and OUT_EDT = '" & OUT_EDT & "' "
RS.Open strSQL, egrpDSN, 3, 3
if not RS.EOF then
	msg="<br>您的需求已經送出了喔!!"
	mailFG=0
else
	RS.AddNew
		RS("DIY_PNM")=DIY_PNM
if DIY_NM <> "" then        RS("DIY_NM") = DIY_NM  		
if DIY_PJOB <> "" then		RS("DIY_PJOB")=DIY_PJOB
if DIY_EXP <> "" then		RS("DIY_EXP")=DIY_EXP
if DIY_EXPPLACE <> "" then	RS("DIY_EXPPLACE")=DIY_EXPPLACE
if DIY_TELNO <> "" then		RS("DIY_TELNO")=DIY_TELNO
if DIY_FAXNO <> "" then		RS("DIY_FAXNO")=DIY_FAXNO
if DIY_MOBILE <> "" then	RS("DIY_MOBILE")=DIY_MOBILE
if DIY_EMAIL <> "" then		RS("DIY_EMAIL")=DIY_EMAIL
if NEED_AIRPORT <> "" then	RS("NEED_AIRPORT")=NEED_AIRPORT
if DIY_BUDGET <> "" then	RS("DIY_BUDGET")=DIY_BUDGET
if TOT_DAYS <> "" then		RS("TOT_DAYS")=TOT_DAYS
if MAN_CNT <> "" then		RS("MAN_CNT")=MAN_CNT
if CHD_CNT <> "" then		RS("CHD_CNT")=CHD_CNT
if BABY_CNT <> "" then		RS("BABY_CNT")=BABY_CNT
if DIY_PURPOSE <> "" then	RS("DIY_PURPOSE")=DIY_PURPOSE
		RS("DIY_MODE")=DIY_MODE
		RS("DIY_TOPLACE")=DIY_TOPLACE
if DIY_RK <> "" then		RS("DIY_RK")=DIY_RK
if OUT_BDT <> "" then		RS("OUT_BDT")=OUT_BDT
if OUT_EDT <> "" then		RS("OUT_EDT")=OUT_EDT
		RS("DIY_RTN") = 0
		                    RS("DIY_TP") = DIY_TP		
	RS.Update
	msg = ReadText("/eWeb_"&iWEB_ID&"/Public/DIY_TITLE.txt",4)
	mailFG=1
end if
%> 
<table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr> 
		<td align="center"> 
			<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr> 
					<td align="center" class="order-contant-text01"><%= msg %></td>
				</tr>
			</table>
			<%'2007-12-07 poison 將網站預設頁改為iEWEB_HOME_PAGE值%>
		<BUTTON type="button" hidefocus="true" class="member-input-button" style="CURSOR: hand" onclick="window.location.href='<%=iEWEB_HOME_PAGE%>'">      
			<table border="0" align="center" cellpadding="0" cellspacing="0">
				<tr>
					<td ><img src="/eWeb_<%=iWEB_ID%>/images/button_001.gif" width="12" height="21" ></td>
					<td align="center" background="/eWeb_<%=iWEB_ID%>/images/button_002.gif" class="ms-input-text">回首頁</td>
					<td><img src="/eWeb_<%=iWEB_ID%>/images/button_003.gif" width="12" height="21" ></td>
				</tr>
			</table>                        
		</BUTTON>
		</td>
	</tr>
</table>
<%'=== 2003/05/22 寄信到客服信箱 ===
if mailFG=1 then
	mailTitle = "客服人員您好:"
	mailbody1 = "<html><head><meta http-equiv=content-type content=text/html; charset=UTF-8></head><body>"
	mailbody1 = "<div align=""center""><font color=""red"" size=""3"">這是訪客所留的量身訂做需求, 請查照。</font></div>"
	mailbody1 = mailbody1 & "<table width='80%' border='0' cellspacing='1' bgcolor='#CCCCCC' cellpadding='4' style=""font-size: 13px;"" align=""center"">"
	mailbody1 = mailbody1 & "<tr bgcolor=""#FAFAFA""><td>"
 if from = "" then
	mailbody1 = mailbody1 & "姓名："&DIY_PNM&"<br>"
 elseif from="1" or from="3" then
    mailbody1 = mailbody1 & "政府機關："&DIY_NM&"<br>"
 elseif from="2" then
    mailbody1 = mailbody1 & "公司名稱/店別："&DIY_NM&"<br>"
 end if
 if from<>"" then
 	mailbody1 = mailbody1 & "聯絡人："&DIY_PNM&"<br>"
 end if 
 if from<>"" then
    mailbody1 = mailbody1 & "職業(部別職稱)："&DIY_PJOB&"<br>"
 else	
	mailbody1 = mailbody1 & "職業："&DIY_PJOB&"<br>"
 end if	
	mailbody1 = mailbody1 & "出國經驗："&DIY_EXP&""
	if DIY_EXPPLACE <> "" then
		mailbody1 = mailbody1 & "　(去過三次以上的地方："&DIY_EXPPLACE&")"
	end if
	mailbody1 = mailbody1 & "</td></tr>"
	mailbody1 = mailbody1 & "<tr bgcolor=""#FAFAFA""><td>"
	mailbody1 = mailbody1 & "‧電話："&DIY_TELNO
	mailbody1 = mailbody1 & "‧傳真："&DIY_FAXNO
	mailbody1 = mailbody1 & "‧手機："&DIY_MOBILE&"<br>"
	mailbody1 = mailbody1 & "‧E-Mail：<a href=""mailto:"&DIY_EMAIL&""">"&DIY_EMAIL&"</a><br>"
	mailbody1 = mailbody1 & "</td></tr>"
	mailbody1 = mailbody1 & "<tr bgcolor=""#FAFAFA""><td>"
	mailbody1 = mailbody1 & "兩地機場交通接送："
	select case NEED_AIRPORT
		case 1
			mailbody1 = mailbody1 & "需要"
		case 0
			mailbody1 = mailbody1 & "不需要"
	end select
	mailbody1 = mailbody1 & "</td></tr>"
	mailbody1 = mailbody1 & "<tr bgcolor=""#FAFAFA""><td>"
	mailbody1 = mailbody1 & "預算："&DIY_BUDGET&"/人"
	mailbody1 = mailbody1 & "‧旅遊天數："&TOT_DAYS
	mailbody1 = mailbody1 & "</td></tr>"
	mailbody1 = mailbody1 & "<tr bgcolor=""#FAFAFA""><td>"
	mailbody1 = mailbody1 & "人數：<br>"
	mailbody1 = mailbody1 & "‧大人："&MAN_CNT&"位"
	mailbody1 = mailbody1 & "‧小孩："&CHD_CNT&"位(12歲以下)"
	mailbody1 = mailbody1 & "‧嬰兒："&BABY_CNT&"位(2歲以下)"
	mailbody1 = mailbody1 & "</td></tr>"
	mailbody1 = mailbody1 & "<tr bgcolor=""#FAFAFA""><td>"
	mailbody1 = mailbody1 & "旅遊期間："
	if OUT_BDT <> "" then mailbody1 = mailbody1 & "從"&OUT_BDT
	if OUT_EDT <> "" then mailbody1 = mailbody1 & "到"&OUT_EDT
	mailbody1 = mailbody1 & "</td></tr>"
	mailbody1 = mailbody1 & "<tr bgcolor=""#FAFAFA""><td>"
	mailbody1 = mailbody1 & "旅遊目的："&DIY_PURPOSE
	mailbody1 = mailbody1 & "</td></tr>"
	mailbody1 = mailbody1 & "<tr bgcolor=""#FAFAFA""><td>"
	mailbody1 = mailbody1 & "旅遊方式："&DIY_MODE
	mailbody1 = mailbody1 & "</td></tr>"
	mailbody1 = mailbody1 & "<tr bgcolor=""#FAFAFA""><td>"
	mailbody1 = mailbody1 & "計劃的地點："&DIY_TOPLACE
	mailbody1 = mailbody1 & "</td></tr>"
	mailbody1 = mailbody1 & "<tr bgcolor=""#FAFAFA""><td>"
	mailbody1 = mailbody1 & "其他需求："&Replace(DIY_RK,Chr(10),"<br>")&"<br>"
	mailbody1 = mailbody1 & "</td></tr>"
	mailbody1 = mailbody1 &"<tr bgcolor=""#F0F7FF""><td align=""center"">"
	mailbody1 = mailbody1 & "請儘速回覆需求，以爭取相關業務，謝謝。"
	mailbody1 = mailbody1 & "</td></tr></table></body></html>"
	'=== 公司聯絡資訊 ===
	src_cls = matchtype("SRC_CLS",Session("SRC_CLS"))
	if src_cls = "" then
		src_cls = "直客"
	else
		if len(src_cls) > 4 then
			src_cls = left(src_cls,4)
		end if
	end if
	'	=====	brian	2005-02-17	=====
	CNTA_E1 = selCNTA("CNTA_DR","CNTA_CL","S","E1", src_cls, " and MAIL_FG =1 Order By CNTA_SQ")		'EMAIL	
	'	=====	brian	2005-02-17	=====
	mailfrom=CNTA_E1
	mailto = CNTA_E1
	mailbcc = ""
	'2007-11-08 [poison] 針對良友與樂遊調整需求單主旨,使其能分辨來源
	Select Case iWeb_ID
		Case "loyo"
			mailsubject = "樂遊訪客量身訂做需求通知"
		Case "ftstour"
			mailsubject = "良友訪客量身訂做需求通知"
		Case Else
			mailsubject = "訪客量身訂做需求通知"
	end select
	'2012-06-27 [Kevin] Mail發送機制調整
	Set tmpRs = Server.CreateObject("Adodb.Recordset")
	strSQL = "SELECT * FROM TRWORD WHERE TRWORD.CLS_CD = 'Mail_Server';"
	tmpRs.Open strSQL, egrpDSN, 3, 3
		if not tmpRs.EOF then
			oCHIN_WD = trim(tmpRs("CHIN_WD"))
			oENGL_WD = trim(tmpRs("ENGL_WD"))
		end if 
	tmpRs.close
	'2012-11-27 [Glenn]將資料庫物件結束時清空
	set tmpRs = nothing
	
		
		if oCHIN_WD <> "" or not isnull(oCHIN_WD) then
			Call Send_OrderMail(oENGL_WD,mailto,"","",mailsubject,mailbody1)
		else
			Call send_mail(mailfrom,mailto,mailbcc,mailsubject,mailTitle,mailbody1,"")
		end if
		
	end if
'2012-11-27 [Glenn]將資料庫物件結束時清空
RS.close
set RS = nothing
set bRS = nothing%>

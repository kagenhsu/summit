﻿<%
'更新說明：
'			2007-12-07	[wanglijun@cowell.com.cn]	出國經驗(去過3次以上的地方)要拉掉 
'           2007-12-07  [wanglijun@cowell.com.cn]   聯絡方式電話設為必填欄位  	
'			2013-04-11	[Aron][ew.v1209.349U]		世界eweb/diy移至客製化目錄，並修改code
'=====================================
%>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<script src="/script/eWeb_sel.js" type="text/javascript"></script>
<script language="JavaScript" type="text/JavaScript">
//必填欄位檢查
function chkDIY(){
	var diy = document.form_DIY;
	var cnt_DIY_TOPLACE = 0;
	var cnt_DIY_MODE = 0;
	//姓名
	if(diy.DIY_PNM.value==""){
		alert("請填寫您的姓名!!");
		return false;}
	
	//e-Mail
	if(diy.DIY_EMAIL.value==""){
		alert("請填寫您的Email !!");
		return false;}
		
		
//2007-12-07 wanglijun@cowell.com.cn 新增電話必選項的驗證	 
	  if(diy.DIY_TELNO.value=="")
	  {
	  	alert("請填寫您的電話!");
		return false;  
	  }	
	  	
	//旅遊方式
	for(i=0;i<diy.DIY_MODE.length;i++){
		if(diy.DIY_MODE[i].checked){
			cnt_DIY_MODE+=1;}
	}
	if(cnt_DIY_MODE==0){
		alert("請填寫您希望的旅遊方式!!");
		return false;}
	for(i=0;i<diy.DIY_TOPLACE.length;i++){
		if(diy.DIY_TOPLACE[i].checked){
			cnt_DIY_TOPLACE+=1;}
	}
	//計劃地點
	if(cnt_DIY_TOPLACE==0){
		alert("請填寫您的計劃地點!!");
		return false;}
}
function chk_e(obj1, obj2, Index){
	if(obj1[Index].checked){
		obj2.style.display="";
	} else {
		obj2.value="";
		obj2.style.display="none";
	}
}
function chk_EXP(obj1,obj2,Index){
	if(obj1.selectedIndex>=Index){
		obj2.style.display="";
	} else {
		obj2.value="";
		obj2.style.display="none";
	}
}
//2007-12-07 wanglijun@cowell.com.cn  新增函數
function chk_EXP1(obj1,Index){
    var id1=document.getElementById("DIY_EXPPLACE");
	if(obj1.selectedIndex>=Index){
		id1.style.display="";
	} else {
		id1.style.display="none";
	}
}
</script>
<%
'=== 取得系統日期 =======
toDate=Date
toYear=Year(toDate)
toMonth=Month(toDate)
if len(toMonth)<2 then toMonth = "0" & toMonth
toDay=Day(toDate)
if len(toDay)<2 then toDay = "0" & toDay
str_Sub_TITLE=ReadText("/eWeb_"&iWEB_ID&"/Public/DIY_TITLE.txt",2)
str_End_TITLE=ReadText("/eWeb_"&iWEB_ID&"/Public/DIY_TITLE.txt",3)
%>
<link href="/css/<%=iWEB_ID%>.css" rel="stylesheet" type="text/css">
<form action="/eWeb/diy/V_diy.asp" method="post" name="form_DIY" id="form_DIY" onSubmit="return chkDIY()">
<input name="inc" type="hidden" id="inc" value="su">
<h5 class="order-contant-text01"><%= str_Sub_TITLE %></h5>
<table width="92%" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr> 
		<td> 
			<table width="100%" border="0" cellpadding="2" cellspacing="1">
				<tr> 
					<td colspan="2" class="order-td-line-height"> </td>
				</tr>
				<tr> 
					<td width="110" class="order-td-color01"> 姓名 </td>
					<td class="order-input-text01">
						<input name="DIY_PNM" type="text" class="order-input-box" id="DIY_PNM2" size="8">
						女士/先生<font color="#FF0000">*</font>
					</td>
				</tr>
				<tr> 
					<td class="order-td-color01">職業</td>
					<td class="order-input-text01">
						<select name="DIY_PJOB" class="order-input-box" id="select">
							<option>上班族</option>
							<option>學生</option>
							<option>一般員工</option>
							<option>主管階級</option>
							<option>企業老闆</option>
							<option>家庭主婦</option>
							<option>SOHO族</option>
							<option>其他</option>
						</select>
					</td>
				</tr>
				<tr> 
					<td class="order-td-color01">出國經驗</td>
					<td class="order-input-text">
					<!-----2007-12-07 wanglijun@cowell.com.cn 修改前的程式
						<select name="DIY_EXP" class="order-input-box" onChange="chk_EXP(this,document.form_DIY.DIY_EXPPLACE,2)">
							<option value="尚未使用過護照">尚未使用過護照</option>
							<option value="1~3次">1~3次</option>
							<option value="3~6次">3~6次</option>
							<option value="6~10次">6~10次</option>
							<option value="10次以上">10次以上</option>
						</select>
						( 去過三次以上的地方 
						<input type="text" name="DIY_EXPPLACE" size="13" class="order-input-box" style="display:none">)
						--> 
					<!----2007-12-07	[wanglijun@cowell.com.cn]	出國經驗(去過3次以上的地方)要拉掉,新增函數chk_EXP1  begin------------------->
					  <select name="DIY_EXP" class="order-input-box"    onChange="chk_EXP1(this,2)">			
							<option value="尚未使用過護照">尚未使用過護照</option>
							<option value="1~3次">1~3次</option>
							<option value="3~6次">3~6次</option>
							<option value="6~10次">6~10次</option>
							<option value="10次以上">10次以上</option>
						</select>
						<label  id="DIY_EXPPLACE"   style="display:none">
						( 去過三次以上的地方
						<input type="text"  name="DIY_EXPPLACE" size="13" class="order-input-box" >)
						</lable>
				<!-----2007-12-07	[wanglijun@cowell.com.cn]	出國經驗(去過3次以上的地方)要拉掉  end --------------------------------------->		
					</td>
				</tr>
				<tr> 
					<td class="order-td-color01">E-Mail</td>
					<td class="order-input-text01">
						<input name="DIY_EMAIL" type="text" class="order-input-box" id="DIY_EMAIL2" size="40">
						<font color="#FF0000">*</font>
					</td>
				</tr>
				<tr> 
					<td class="order-td-color01">聯絡方式</td>
					<td class="order-input-text01">
					<!--修改前的程式
						電話 <input name="DIY_TELNO" type="text" class="order-input-box" id="DIY_TELNO2" size="15">
						-->
				    <!--------聯絡方式電話設為必填欄位 -------------------------------->  
						電話 <input name="DIY_TELNO" type="text" class="order-input-box" id="DIY_TELNO2" size="15"><font color="#FF0000">*</font>
					<!--------聯絡方式電話設為必填欄位 -------------------------------->
						傳真 <input name="DIY_FAXNO" type="text" class="order-input-box" id="DIY_FAXNO2" size="15">
						<br>手機 <input name="DIY_MOBILE" type="text" class="order-input-box" id="DIY_MOBILE2" size="15"> 
					</td>
				</tr>
				<tr><td colspan="2" class="order-td-line-height" > </td></tr>
				<tr> 
					<td class="order-td-color01"> 您希望的旅遊方式<font color="#FF0000">*</font></td>
					<td class="order-input-text01">
						<table border="0" cellpadding="0" cellspacing="0" class="order-input-text01">
							<tr> 
								<td><input name="DIY_MODE" type="checkbox" value="自由行">自由行</td>
								<td><input name="DIY_MODE" type="checkbox" value="定點旅遊">定點旅遊</td>
  								<td><input name="DIY_MODE" type="checkbox" value="單訂酒店">單訂酒店</td>
  								<td><input name="DIY_MODE" type="checkbox" value="島嶼渡假">島嶼渡假</td>
	  							<td><input name="DIY_MODE" type="checkbox" value="單買機票">單買機票</td>
							</tr>
							<tr>
														  	<td><input name="DIY_MODE" type="checkbox" value="參加團體行程">參加團體行程</td> 
	  							<td colspan="5">
									<input name="DIY_MODE" type="checkbox" value="其他：" onClick="chk_e(document.form_DIY.DIY_MODE,document.form_DIY.DIY_MODE_e,6)">其他 
									<input type="text" name="DIY_MODE_e" class="order-input-box"  size="20" maxlength="24" style="display:none">可復選）
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td colspan="2" class="order-td-line-height" > </td></tr>
				<tr> 
					<td class="order-td-color01"> 計劃的地點<font color="#FF0000">*</font><br> </td>
						<td class="order-input-text01">
							<table border="0" cellpadding="0" cellspacing="0" class="order-input-text01">
								<tr> 
		  							<td><input name="DIY_TOPLACE" type="checkbox" value="中國大陸">中國大陸</td>
  									<td><input name="DIY_TOPLACE" type="checkbox" value="港澳地區">港澳地區</td>
  									<td><input name="DIY_TOPLACE" type="checkbox" value="島嶼旅遊">島嶼旅遊</td>
  									<td><input name="DIY_TOPLACE" type="checkbox" value="俄羅斯">俄羅斯</td>
  									<td><input name="DIY_TOPLACE" type="checkbox" value="東南亞">東南亞</td>
  									<td>&nbsp;</td>
								</tr>
								<tr>
									<td><input name="DIY_TOPLACE" type="checkbox" value="台灣">台灣</td> 
  									<td><input name="DIY_TOPLACE" type="checkbox" value="日本/韓國">日本/韓國</td>
  									<td><input name="DIY_TOPLACE" type="checkbox" value="澳洲/紐西蘭">澳洲/新西蘭</td>
  									<td><input name="DIY_TOPLACE" type="checkbox" value="歐洲">歐洲</td>
  									<td><input name="DIY_TOPLACE" type="checkbox" value="美國/加拿大">美國/加拿大</td>
									<td>&nbsp;</td>
								</tr>
								<tr> 
  									<td colspan="5">
										<input name="DIY_TOPLACE" type="checkbox" value="其他：" onClick="
										if(this.checked == true){
											document.form_DIY.DIY_TOPLACE_e.style.display='';
										} else {
											document.form_DIY.DIY_TOPLACE_e.style.display='none';
										}
										  ">其他 
									<input name="DIY_TOPLACE_e" type="text" class="order-input-box"  size="20" maxlength="24" style="display:none">（可復選）
									</td>
  									<td>&nbsp;</td>
								</tr>
							</table>
						</td>
					</tr>
				
				<tr> 
					<td class="order-td-color01"> 旅遊目的 </td>
					<td class="order-input-text01">
					<select name="DIY_PURPOSE" class="order-input-box" onChange="chk_EXP(this,document.form_DIY.DIY_PURPOSE_e,10)">
							<option value="個人旅遊">個人旅遊</option>
							<option value="商務洽公">商務洽公</option>
							<option value="員工旅遊">員工旅遊</option>
							<option value="畢業旅行">畢業旅行</option>
							<option value="情侶/蜜月">情侶/蜜月</option>
							<option value="廠商招待">廠商招待</option>
							<option value="家族旅遊">家族旅遊</option>
							<option value="參展">參展</option>
							<option value="工商考察">工商考察</option>
							<option value="遊學">遊學</option>
							<option value="其他：">其他</option>
						</select>． 
						<input name="DIY_PURPOSE_e" type="text" class="order-input-box" id="DIY_PURPOSE_e2" size="20" style="display:none"> 
					</td>
				</tr>
				<tr> 
					<td class="order-td-color01"> 預算 </td>
					<td class="order-input-text01"> 
						<select name="DIY_BUDGET" class="order-input-box" id="select3">
							<option value="">不限</option>
							<option value="一百元以下">一百元以下</option>
							<option value="1-2千元">1~2千元</option>
							<option value="2-4千元">2~4千元</option>
							<option value="4-6千元">4~6千元</option>
							<option value="6-8千元">6~8千元</option>
							<option value="8千元以上">8千元以上</option>
						</select>/人
					</td>
				</tr>
				<tr> 
					<td class="order-td-color01">旅遊天數</td>
					<td class="order-input-text01">
						<select name="TOT_DAYS" class="order-input-box" id="select2">
							<option value="">不限</option>
							<option value="3-5天">3~5天</option>
							<option value="6-10天">6~10天</option>
							<option value="11-15天">11~15天</option>
							<option value="16-20天">16~20天</option>
							<option value="20天以上">20天以上</option>
						</select>
					</td>
				</tr>
				<tr> 
					<td class="order-td-color01"> 人數</td>
					<td class="order-input-text01">大人 
						<select name="MAN_CNT" class="order-input-box" id="select4">
<% 						for i = 0 to 20 
							str_i = i
							if i = 20 then
								str_i = str_i & "以上"
							end if%>
							<option value="<%= str_i %>" <% if i = 1 then %>selected<% end if %>><%= str_i %></option>
<% 						next %>
						</select>位　小孩 
						<select name="CHD_CNT" class="order-input-box" id="select5">
<% 						for i = 0 to 20 
							str_i = i
							if i = 20 then
								str_i = str_i & "以上"
							end if%>
							<option value="<%= str_i %>"><%= str_i %></option>
<% 						next %>
						</select>位(12歲以下) 嬰兒 
						<select name="BABY_CNT" class="order-input-box" id="select6">
<% 						for i = 0 to 20 
							str_i = i
							if i = 20 then
								str_i = str_i & "以上"
							end if%>
							<option value="<%= str_i %>"><%= str_i %></option>
<% 						next %>
						</select>位(2歲以下)
					</td>
				</tr>
				<tr> 
					<td class="order-td-color01"> 旅遊期間<br> </td>
					<td class="order-input-text01">
						<select name="s_year" class="order-input-box" onChange="javascript:chkstartDate(document.form_DIY)">
							<option value="">不限</option>
<% 								for i =toYear to toYear+4 %>
							<option value="<%= i %>"><%= i %></option>
<% 								next %>
						</select>年 
						<select name="s_month" class="order-input-box" onChange="javascript:chkstartDate(document.form_DIY)">
							<option value="">不限</option>
<% 								for i = 1 to 9 %>
							<option value="<%= "0"&i %>"><%= "0"&i %></option>
<% 								next %>
<% 								for i = 10 to 12 %>
							<option value="<%= i %>"><%= i %></option>
<% 								next %>
						</select>月 
						<select name="s_day" class="order-input-box" onChange="javascript:chkstartDate(document.form_DIY)">
							<option value="">不限</option>
<% 								for i = 1 to 9 %>
							<option value="<%= "0"&i %>"><%= "0"&i %></option>
<% 								next %>
<% 								for i = 10 to 31 %>
							<option value="<%= i %>"><%= i %></option>
<% 								next %>
						</select>日到 
						<select name="e_year" class="order-input-box" onChange="javascript:chkstartDate(document.form_DIY)">
							<option value="">不限</option>
<% 								for i = toYear to toYear+5 %>
							<option value="<%= i %>"><%= i %></option>
<% 								next %>
						</select>年 
						<select name="e_month" class="order-input-box"  onChange="javascript:chkstartDate(document.form_DIY)">
							<option value="">不限</option>
<% 								for i = 1 to 9 %>
							<option value="<%= "0"&i %>"><%= "0"&i %></option>
<% 								next %>
<% 								for i = 10 to 12 %>
							<option value="<%= i %>"><%= i %></option>
<% 								next %>
						</select>月 
						<select name="e_day" class="order-input-box" onChange="javascript:chkstartDate(document.form_DIY)">
							<option value="">不限</option>
<% 								for i = 1 to 9 %>
							<option value="<%= "0"&i %>"><%= "0"&i %></option>
<% 								next %>
<% 								for i = 10 to 31 %>
							<option value="<%= i %>"><%= i %></option>
<% 								next %>
						</select>日
					</td>
				</tr>
				<tr> 
					<td class="order-td-color01">兩地機場交通接送</td>
					<td class="order-input-text01">
						<input type="radio" name="NEED_AIRPORT" value="1">需要 
						<input type="radio" name="NEED_AIRPORT" value="0">不需要</td>
				</tr>
				<tr><td colspan="2" class="order-td-line-height" > </td></tr>
					<tr> 
						<td class="order-td-color01"> 其他需求<br> </td>
						<td class="order-input-text01">
							<textarea name="DIY_RK" cols="60" rows="6" class="order-input-box" id="textarea"></textarea>
						</td>
					</tr>
					<tr> 
						<td colspan="2" align="center">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr> 
									<td class="order-contant-text02"><%= str_End_TITLE %></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr> 
						<td colspan="2" align="center">							 
							<table border="0" cellspacing="5" cellpadding="0">
								<tr> 
								<td>
									<BUTTON type="submit" hidefocus="true" class="member-input-button" style="CURSOR: hand"> 
										<table border="0" cellspacing="0" cellpadding="0">
	  										<tr> 
												<td><img src="/eWeb_<%=iWEB_ID%>/images/button_001.gif" width="12" height="21" ></td>
												<td align="center" background="/eWeb_<%=iWEB_ID%>/images/button_002.gif" class="ms-input-text">送出需求</td>
												<td><img src="/eWeb_<%=iWEB_ID%>/images/button_003.gif" width="12" height="21" ></td>
	  										</tr>
										</table>
									</BUTTON>
								</td>
  								<td>
									<BUTTON type="reset" hidefocus="true" class="member-input-button" style="CURSOR: hand"> 
										<table border="0" cellspacing="0" cellpadding="0">
	  										<tr> 
												<td><img src="/eWeb_<%=iWEB_ID%>/images/button_001.gif" width="12" height="21" ></td>
												<td align="center" background="/eWeb_<%=iWEB_ID%>/images/button_002.gif" class="ms-input-text">重新填寫</td>
												<td><img src="/eWeb_<%=iWEB_ID%>/images/button_003.gif" width="12" height="21" ></td>
	  										</tr>
										</table>
									</BUTTON>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</form>


<%
'更新說明： 
'			2007-12-07	[wanglijun@cowell.com.cn]	出國經驗(去過3次以上的地方)要拉掉
'           2007-12-07  [wanglijun@cowell.com.cn]   聯絡方式電話設為必填欄位
'           2008-03-28  [ader@cowell.com.tw]        遠見需求,針對機關,公司,員工旅遊資料欄位顯示方式不同,判斷from參數
'			2012-02-09	[art]	[ew.v1010.083U]		量身訂做頁面表單項目調整
'			2012-10-05	[art]	[ew.v1012.118U] 	需吃GF MEM_MS_EMAIL_FG 參數
'			2012-10-17  [art]	[ew.v1012.135U]		EMAIL輸入需判斷@。
'			2013-04-03	[Aron]	[ew.v1029.340B]		修正世邦ie量身定作送出按鈕無法點擊的問題
'			2013-04-11	[Aron]	[ew.v1209.349U]		世界eweb/diy移至客製化目錄，並修改code
'			2014-01-17	[Derrick][EW.1308.260.B]旅遊行程需求單在chrome瀏覽器有異常狀況,相容性調整
'			2014-03-17	[Derrick][EW.1406.24.U]行家需求單頁面要調整內容
'=====================================
MEM_MS_EMAIL_FG		= GetSiteDTL("OT", "MEM_MS_EMAIL_FG")		'參照MPSITEDTL.MEM_MS_EMAIL_FG		決定前台會員專區「E_Mail」	是否為必填
response.write "<!--MEM_MS_EMAIL_FG="&MEM_MS_EMAIL_FG&"-->"
%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<script src="/script/eWeb_sel.js" type="text/javascript"></script>
<script language="JavaScript" type="text/JavaScript">
//必填欄位檢查
function chkDIY(){
	//var diy = document.form_DIY;
	var cnt_DIY_TOPLACE = 0;
	var cnt_DIY_MODE = 0;
	//姓名
	//2012-10-05 [art] fix error
	var OBJ_PNM = document.getElementById("DIY_PNM");
	if (OBJ_PNM) {
		if(document.getElementById('DIY_PNM').value=="" && document.getElementById('from').value==""){
			alert("請填寫您的姓名!!");
			return false;}
	}
	var OBJ_NM = document.getElementById("DIY_NM");
	if (OBJ_NM) {
		if(document.getElementById('DIY_NM').value=="" && document.getElementById('from').value=="1"){
		alert("請填寫您的政府機關!!");
		return false;}	
		if(document.getElementById('DIY_NM').value=="" && document.getElementById('from').value=="3"){
		alert("請填寫您的政府機關!!");
		return false;}	
		if(document.getElementById('DIY_NM').value=="" && document.getElementById('from').value=="2"){
		alert("請填寫您的公司名稱/店別!!");
		return false;}
	}
	//2012-10-05 [art] [ew.v1012.118U] 需吃GF MEM_MS_EMAIL_FG 參數
	<%if MEM_MS_EMAIL_FG = "1" then%>
	<%'2012-10-17 [art][ew.v1012.135U] 加上判斷@的邏輯
	%>
	var TxtEMail = document.getElementById("DIY_EMAIL").value;
	if(TxtEMail.value==""){
		alert("請填寫您的Email !!");
		return false;
	}
	if(TxtEMail.indexOf("@")==-1){ 
		alert("E-mail格式不正確，請重新填寫。謝謝!");
		return false;
	}
	
	<%end if%>
	//2013-04-03 [Aron] 修正世邦ie量身定作送出按鈕無法點擊的問題
	//return false;
	 //2007-11-07 wanglijun 增加電話必選項的判斷
	  if(document.getElementById('DIY_TELNO').value=="")
	  {
	  	alert("請填寫您的電話!");
		return false;  
	  }		
	//旅遊方式
	for(i=0;i<document.getElementsByName('DIY_MODE').length;i++){
		if(document.getElementsByName('DIY_MODE')[i].checked){
			cnt_DIY_MODE+=1;}
	}
	if(cnt_DIY_MODE==0){
		alert("請填寫您希望的旅遊方式!!");
		return false;}
	for(i=0;i<document.getElementsByName('DIY_TOPLACE').length;i++){
		if(document.getElementsByName('DIY_TOPLACE')[i].checked){
			cnt_DIY_TOPLACE+=1;}
	}
	//計劃地點
	if(cnt_DIY_TOPLACE==0){
		alert("請填寫您的計劃地點!!");
		return false;}
	document.forms['form_DIY'].submit();
}

function chk_e(obj1, obj2, Index){
	if(obj1[Index].checked){
		obj2.style.display="";
	} else {
		obj2.value="";
		obj2.style.display="none";
	}
}
function chk_EXP(obj1,obj2,Index){
	if(obj1.selectedIndex>=Index){
		obj2.style.display="";
	} else {
		obj2.value="";
		obj2.style.display="none";
	}
}
//----2007-12-07	[wanglijun@cowell.com.cn]	出國經驗(去過3次以上的地方)要拉掉,並增加chk_EXP1函數 
function chk_EXP1(obj1,Index){
    var id1=document.getElementById("DIY_EXPPLACE");
	if(obj1.selectedIndex>=Index){
		id1.style.display="";
	} else {
		id1.style.display="none";
	}
}
</script>

<%
'=== 取得系統日期 =======
toDate=Date
toYear=Year(toDate)
toMonth=Month(toDate)
if len(toMonth)<2 then toMonth = "0" & toMonth
toDay=Day(toDate)
if len(toDay)<2 then toDay = "0" & toDay
str_Sub_TITLE=ReadText("/eWeb_"&iWEB_ID&"/Public/DIY_TITLE.txt",2)
str_End_TITLE=ReadText("/eWeb_"&iWEB_ID&"/Public/DIY_TITLE.txt",3)
%>
<%
   from = trim(request("from"))
%>
<link href="/css/<%=iWEB_ID%>.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="/eWeb_miyabi/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="/eWeb_miyabi/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="/eWeb_miyabi/css/default.css">
<div class="order-all-table-width">
<form action="/eWeb_miyabi/diy/V_diy.asp" method="post" name="form_DIY" id="form_DIY" onSubmit="return chkDIY()">
<input type="hidden" name="from" id="from" value="<%=from %>" />	
<input name="inc" type="hidden" id="inc" value="su">
<h5 class="order-contant-text01"><%= str_Sub_TITLE %></h5>
<hr />
<div id="css_table">
	<div class="css_tr">
    <% If from = "" Then %>
    	<div class="css_td order-td-color01">姓名</div>
        <div class="css_td order-input-text01">
        	<input name="DIY_PNM" type="text" class="order-input-box" id="DIY_PNM" size="8">女士/先生 <font color="#FF0000">*</font>
        </div>
	<% elseif from="1" or from="3" then %>
    	<div class="css_td order-td-color01">政府機關</div>
        <div class="css_td order-input-text01">
            <input name="DIY_NM" type="text" class="order-input-box" id="DIY_NM" size="8"> <font color="#FF0000">*</font>
        </div>
	<% elseif from="2" then %>
    	<div class="css_td order-td-color01">公司名稱/店別</div>
        <div class="css_td order-input-text01">
            <input name="DIY_NM" type="text" class="order-input-box" id="DIY_NM" size="8"> <font color="#FF0000">*</font>
        </div>
	<% end if %>
    </div>
    <div class="css_tr">
    	<div class="css_td order-td-color01">職業</div>
        <div class="css_td order-input-text01">
            <select name="DIY_PJOB" id="DIY_PJOB" class="order-input-box" id="select">
				<option>上班族</option>
				<option>學生</option>
				<option>一般員工</option>
				<option>主管階級</option>
				<option>企業老闆</option>
				<option>家庭主婦</option>
				<option>SOHO族</option>
				<option>其他</option>
			</select>
        </div>
    </div>
	<%if from<>"" then %>
    <div class="css_tr">  
    	<div class="css_td order-td-color01">部門職稱</div>
        <div class="css_td order-input-text01">
            <input name="DIY_PJOB1" type="text" class="order-input-box" id="DIY_PJOB1" size="8">
        </div>
    </div>
	<% end if %>
    <%if from<>"" then %>
    <div class="css_tr">
    	<div class="css_td order-td-color01">聯絡人</div>
        <div class="css_td order-input-text01">
            <input name="DIY_PNM" type="text" class="order-input-box" id="DIY_PNM" size="8">
        </div>
	</div>
	<% end if %>
    <% if from="" Then %>	
	<%' 2014-03-17 Derrick 行家不顯示
		if iweb_id<>"protour" then
	%>
    <!--<div class="css_tr">
    	<div class="css_td order-td-color01">出國經驗</div>
        <div class="css_td order-input-text01">
            <!----修改前的程式
			<select name="DIY_EXP" id="DIY_EXP" class="order-input-box" onChange="chk_EXP(this,document.getElementById('DIY_EXPPLACE'),2)">
				<option value="尚未使用過護照">尚未使用過護照</option>
				<option value="1~3次">1~3次</option>
				<option value="3~6次">3~6次</option>
				<option value="6~10次">6~10次</option>
				<option value="10次以上">10次以上</option>
			</select>
			( 去過三次以上的地方 
			<input type="text" name="DIY_EXPPLACE" id="DIY_EXPPLACE" size="13" class="order-input-box" style="display:none">) 
			-->
			<!----2007-12-07	[wanglijun@cowell.com.cn]	出國經驗(去過3次以上的地方)要拉掉,並增加chk_EXP1函數  begin------------------->
			<!--<select name="DIY_EXP" id="DIY_EXP" class="order-input-box"    onChange="chk_EXP1(this,2)">
				<option value="尚未使用過護照">尚未使用過護照</option>
				<option value="1~3次">1~3次</option>
				<option value="3~6次">3~6次</option>
				<option value="6~10次">6~10次</option>
				<option value="10次以上">10次以上</option>
			</select>
			<label  id="DIY_EXPPLACE"   style="display:none">
			( 去過三次以上的地方
			<input type="text"  name="DIY_EXPPLACE" id="DIY_EXPPLACE" size="13" class="order-input-box" >)
			</lable>

				<!-----2007-12-07	[wanglijun@cowell.com.cn]	出國經驗(去過3次以上的地方)要拉掉,並增加chk_EXP1函數  end --------------------------------------->	
        <!--</div>
    </div>-->
	<%end if%>
    <% end if %>
    <div class="css_tr">
    	<div class="css_td order-td-color01">E-Mail</div>
        <div class="css_td order-input-text01">
            <input name="DIY_EMAIL" type="text" class="order-input-box" id="DIY_EMAIL" size="40">
			<%'2012-10-05 [art] 盈達 需吃GF MEM_MS_EMAIL_FG 參數
				if MEM_MS_EMAIL_FG = "1" then%> <font color="#FF0000">*</font>　
			<%end if%>
        </div>
	</div>
    <div class="css_tr">
    	<div class="css_td order-td-color01">聯絡方式</div>
        <div class="css_td order-input-text01">
            <!--修改前的程式
				電話 <input name="DIY_TELNO" type="text" class="order-input-box" id="DIY_TELNO2" size="15">
			-->	
			<!-------- 2007-12-07  [wanglijun@cowell.com.cn]聯絡方式電話設為必填欄位 begin-------------------------------->
				電話 <input name="DIY_TELNO" type="text" class="order-input-box" id="DIY_TELNO" size="15"> <font color="#FF0000">*</font>　
			<!-------- 2007-12-07  [wanglijun@cowell.com.cn]聯絡方式電話設為必填欄位 end -------------------------------->
				傳真 <input name="DIY_FAXNO" type="text" class="order-input-box" id="DIY_FAXNO" size="15">　
				手機 <input name="DIY_MOBILE" type="text" class="order-input-box" id="DIY_MOBILE" size="15">
        </div>
	</div>
</div>
<hr />
<div id="css_table">
    <!--<div class="css_tr">
    	<div class="css_td order-td-color01">兩地機場交通接送</div>
        <div class="css_td order-input-text01">
            <input type="radio" name="NEED_AIRPORT" value="1">需要　
			<input type="radio" name="NEED_AIRPORT" value="0">不需要
        </div>
	</div>-->
    <div class="css_tr">
    	<div class="css_td order-td-color01">預算</div>
        <div class="css_td order-input-text01">
            <select name="DIY_BUDGET" class="order-input-box" id="DIY_BUDGET">
				<option value="">不限</option>
				<option value="一萬元以下">一萬元以下</option>
				<option value="1-2萬元">1~2萬元</option>
				<option value="2-4萬元">2~4萬元</option>
				<option value="4-6萬">4~6萬</option>
				<option value="6-8萬">6~8萬</option>
				<option value="8萬元以上">8萬元以上</option>
			</select> /人
        </div>
	</div>
    <div class="css_tr">
    	<div class="css_td order-td-color01">旅遊天數</div>
        <div class="css_td order-input-text01">
            <select name="TOT_DAYS" class="order-input-box" id="TOT_DAYS">
				<option value="">不限</option>
				<option value="3-5天">3~5天</option>
				<option value="6-10天">6~10天</option>
				<option value="11-15天">11~15天</option>
				<option value="16-20天">16~20天</option>
				<option value="20天以上">20天以上</option>
			</select>
        </div>
	</div>
	<% if from="" Then %>
    <div class="css_tr">
    	<div class="css_td order-td-color01">人數</div>
        <div class="css_td order-input-text01">
            大人 
			<select name="MAN_CNT" class="order-input-box" id="MAN_CNT">
			<% 	for i = 0 to 20 
				str_i = i
				if i = 20 then
				str_i = str_i & "以上"
			end if%>
				<option value="<%= str_i %>" <% if i = 1 then %>selected<% end if %>><%= str_i %></option>
			<% 	next %>
			</select> 位　小孩 
			<select name="CHD_CNT" class="order-input-box" id="CHD_CNT">
			<% 	for i = 0 to 20 
				str_i = i
				if i = 20 then
				str_i = str_i & "以上"
			end if%>
			<option value="<%= str_i %>"><%= str_i %></option>
			<% 	next %>
			</select> 位(12歲以下)　嬰兒 
			<select name="BABY_CNT" class="order-input-box" id="BABY_CNT">
			<% 	for i = 0 to 20 
				str_i = i
				if i = 20 then
				str_i = str_i & "以上"
			end if%>
			<option value="<%= str_i %>"><%= str_i %></option>
			<% 	next %>
			</select> 位(2歲以下)
        </div>
	</div>
	<% else %>
    <div class="css_tr">
    	<div class="css_td order-td-color01"> </div>
        <div class="css_td order-input-text01">
            大人<input name="MAN_CNT" type="text" class="order-input-box" id="MAN_CNT" size="5" value="1"> 位　
            小孩<input name="CHD_CNT" type="text" class="order-input-box" id="CHD_CNT" size="5" value="0"> 位(12歲以下)　
            嬰兒<input name="BABY_CNT" type="text" class="order-input-box" id="BABY_CNT" size="5" value="0"> 位(2歲以下)
        </div>
	</div>
	<% end if %>
    <div class="css_tr">
    	<div class="css_td order-td-color01">旅遊期間</div>
        <div class="css_td order-input-text01">
            <select name="s_year" class="order-input-box" onChange="javascript:chkstartDate(document.form_DIY)">
				<option value="">不限</option>
				<% 	for i =toYear to toYear+4 %>
				<option value="<%= i %>"><%= i %></option>
				<% 	next %>
			</select> 年  
			<select name="s_month" class="order-input-box" onChange="javascript:chkstartDate(document.form_DIY)">
				<option value="">不限</option>
				<% 	for i = 1 to 9 %>
				<option value="<%= "0"&i %>"><%= "0"&i %></option>
				<% 	next %>
				<%	for i = 10 to 12 %>
				<option value="<%= i %>"><%= i %></option>
				<% 	next %>
			</select> 月 
			<select name="s_day" class="order-input-box" onChange="javascript:chkstartDate(document.form_DIY)">
				<option value="">不限</option>
				<%	for i = 1 to 9 %>
				<option value="<%= "0"&i %>"><%= "0"&i %></option>
				<% 	next %>
				<% 	for i = 10 to 31 %>
				<option value="<%= i %>"><%= i %></option>
				<% 	next %>
			</select> 日　到　 
			<select name="e_year" class="order-input-box" onChange="javascript:chkstartDate(document.form_DIY)">
				<option value="">不限</option>
				<% 	for i = toYear to toYear+5 %>
				<option value="<%= i %>"><%= i %></option>
				<% 	next %>
			</select> 年 
			<select name="e_month" class="order-input-box"  onChange="javascript:chkstartDate(document.form_DIY)">
				<option value="">不限</option>
				<%	for i = 1 to 9 %>
				<option value="<%= "0"&i %>"><%= "0"&i %></option>
				<% 	next %>
				<% 	for i = 10 to 12 %>
				<option value="<%= i %>"><%= i %></option>
				<% 	next %>
			</select> 月 
			<select name="e_day" class="order-input-box" onChange="javascript:chkstartDate(document.form_DIY)">
				<option value="">不限</option>
				<%	for i = 1 to 9 %>
				<option value="<%= "0"&i %>"><%= "0"&i %></option>
				<%	next %>
				<% 	for i = 10 to 31 %>
				<option value="<%= i %>"><%= i %></option>
				<% 	next %>
			</select> 日
        </div>
	</div>
	<% if from="" Then %>
    <div class="css_tr">
    	<div class="css_td order-td-color01">旅遊目的</div>
        <div class="css_td order-input-text01">
            <select name="DIY_PURPOSE" id="DIY_PURPOSE" class="order-input-box" onChange="chk_EXP(this,document.getElementById('DIY_PURPOSE_e'),10)">
				<option value="個人旅遊">個人旅遊</option>
				<option value="商務洽公">商務洽公</option>
				<option value="員工旅遊">員工旅遊</option>
				<option value="畢業旅行">畢業旅行</option>
				<option value="情侶/蜜月">情侶/蜜月</option>
				<option value="廠商招待">廠商招待</option>
				<option value="家族旅遊">家族旅遊</option>
				<option value="參展">參展</option>
				<option value="工商考察">工商考察</option>
				<option value="遊學">遊學</option>
				<option value="其他：">其他</option>
			</select> 
			<input name="DIY_PURPOSE_e" type="text" class="order-input-box" id="DIY_PURPOSE_e" size="20" style="display:none">
        </div>
	</div>
	<% end if %>
</div>
<hr />
<div id="css_table">
    <div class="css_tr">
    	<div class="css_td order-td-color01">您希望的旅遊方式 <font color="#FF0000">*</font></div>
        <div class="css_td order-input-text01">
            <input name="DIY_MODE" type="checkbox" value="自由行"> 自由行　
            <input name="DIY_MODE" type="checkbox" value="Mini Tour"> Mini Tour　
            <input name="DIY_MODE" type="checkbox" value="定點旅遊"> 定點旅遊　
            <%' 2014-03-17 Derrick 行家不顯示
				if iweb_id<>"protour" then
			%>
            <input name="DIY_MODE" type="checkbox" value="Shopping"> Shopping　
            <%end if%>
            <input name="DIY_MODE" type="checkbox" value="島嶼渡假"> 島嶼渡假　
            <%' 2014-03-17 Derrick 行家不顯示
				if iweb_id<>"protour" then
			%>
            <input name="DIY_MODE" type="checkbox" value="單買機票"> 單買機票　
            <%end if%>
            <input name="DIY_MODE" type="checkbox" value="參加團體行程"> 參加團體行程　<br />
            <!--2012-02-09 [art] [ew.v1010.083U] 高爾夫假期-->
	  		<input name="DIY_MODE" type="checkbox" value="高爾夫假期"> 高爾夫假期　
			<input name="DIY_MODE" type="checkbox" value="其他：" onClick="chk_e(document.getElementsByName('DIY_MODE'),document.getElementById('DIY_MODE_e'),6)"> 其他　
			<input type="text" name="DIY_MODE_e" id="DIY_MODE_e" class="order-input-box"  size="20" maxlength="24" style="display:none"> (可複選）
        </div>
	</div>
</div>
<hr />
<div id="css_table">
    <div class="css_tr">
    	<div class="css_td order-td-color01">計劃的地點 <font color="#FF0000">*</font></div>
        <div class="css_td order-input-text01">
            <!--2014-03-17 Derrick 以下調整行家某些欄位不顯示-->
		  	<input name="DIY_TOPLACE" type="checkbox" value="台灣/北部"> 台灣<%if iweb_id<>"protour" then%>/北部<%end if%>　
			<%if iweb_id<>"protour" then%>
  			<input name="DIY_TOPLACE" type="checkbox" value="台灣/中部"> 台灣/中部　
  			<input name="DIY_TOPLACE" type="checkbox" value="台灣/南部"> 台灣/南部　
  			<input name="DIY_TOPLACE" type="checkbox" value="台灣/東部"> 台灣/東部　
			<input name="DIY_TOPLACE" type="checkbox" value="台灣/離島"> 台灣/離島　
			<%end if%>
            <input name="DIY_TOPLACE" type="checkbox" value="<%if iweb_id<>"protour" then%>香港/<%end if%>中國大陸"> 香港/中國大陸　
  			<input name="DIY_TOPLACE" type="checkbox" value="日本/韓國"> 日本/韓國　
  			<input name="DIY_TOPLACE" type="checkbox" value="東南亞"> 東南亞　<br />
  			<input name="DIY_TOPLACE" type="checkbox" value="紐西蘭/澳洲"> 紐西蘭/澳洲　
  			<input name="DIY_TOPLACE" type="checkbox" value="歐洲"> 歐洲　
            <input name="DIY_TOPLACE" type="checkbox" value="美國/加拿大"> 美國/加拿大　
  			<input name="DIY_TOPLACE" type="checkbox" value="遊輪"> 遊輪　
  			<input name="DIY_TOPLACE" type="checkbox" value="其他：" onClick="
				if(this.checked == true){
					document.getElementById('DIY_TOPLACE_e').style.display='';
				} else {
					document.getElementById('DIY_TOPLACE_e').style.display='none';
				}"> 其他　
			<input name="DIY_TOPLACE_e" id="DIY_TOPLACE_e" type="text" class="order-input-box"  size="20" maxlength="24" style="display:none">（可複選）
        </div>
	</div>
    <div class="css_tr">
    	<div class="css_td order-td-color01">其他需求</div>
        <div class="css_td order-input-text01">
            <textarea name="DIY_RK" id="DIY_RK" cols="60" rows="6" class="order-input-box" id="textarea"></textarea>
        </div>
    </div>
</div>
<hr />
    <h5 class="order-contant-text02"><%= str_End_TITLE %></h5>
<div class="input_buttom">
    <input type="submit" hidefocus="true" class="btn btn-danger" value="送出需求" style="CURSOR: hand" onClick="chkDIY();">
    <input type="reset" hidefocus="true" class="btn btn-danger" value="重新填寫" style="CURSOR: hand">
</div>
</div>
</form>
//加入template
CKEDITOR.addTemplates(
'default',
{
    //圖片資料夾路徑，放在同目錄的images資料夾內
    imagesPath: CKEDITOR.getUrl('/eweb_summittour/ckeditor/templates/images/'),
	
    templates: [
	//銷售特色標題
    {
        //標題
        title: '行程特色標題',
		//範本圖
        image: 'Title_1_1.PNG',
        description: '銷售特色標題', //樣板描述
        //自訂樣板內容
        html: '<div class="Title_1">'+
			  '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">'+
			 ' <h2 style="font-size:26px; text-align:center">銷售特色</h2>'+
			 ' <hr style="border-top:2px solid #83b53c; margin-bottom:3px; margin-top:5px; width:100%" /></div>'+
			 ' </div>'

    },
    //景點介紹
    {
        //標題
		title: '行程特色標題',
		//範本圖
        image: 'Title_1_2.PNG',
		//樣板描述
        description: '景點介紹',
		//自訂樣板內容
        html: '<div class="Title_1">'+
			  '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">'+
			  '<h2 style="font-size:26px; text-align:center">景點介紹</h2>'+
			  '<hr style="border-top:2px solid #83b53c; margin-bottom:3px; margin-top:5px; width:100%" /></div>'+
			  '</div>'
    },
	//特別安排
	{
        //標題
		title: '行程特色標題',
		//範本圖
        image: 'Title_1_3.PNG',
		//樣板描述
        description: '特別安排',
		//自訂樣板內容
        html: 	'<div class="Title_1">'+
				'<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">'+
				'<h2 style="font-size:26px; text-align:center">特別安排</h2>'+
				'<hr style="border-top:2px solid #83b53c; margin-bottom:3px; margin-top:5px; width:100%" /></div>'+
				'</div>'
    },
	//銷售特色內文特別贈送
	{
        //標題
		title: '銷售特色內文',
		//範本圖
        image: 'Title_2_1.PNG',
		//樣板描述
        description: '特別贈送',
		//自訂樣板內容
        html: 	'<div class="Title_2">'+
				'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 item0" >'+
				'<h2 style="font-size: 20px;text-align: center; font-weight: bold;">特別贈送</h2>'+
				'<hr style="margin-top: 2px;margin-bottom: 3px;border-top: 2px solid #3333;width: 100%;"/>'+
				'<p style="font-size: 18px;text-align: center;color: #FF0000;">這裡打上特別贈送說明</p>'+
				'</div>'+
				'</div>'
    },
	//銷售特色內文購物景點
	{
        //標題
		title: '銷售特色內文',
		//範本圖
        image: 'Title_2_2.PNG',
		//樣板描述
        description: '購物景點',
		//自訂樣板內容
        html: 	'<div class="Title_2">'+
				'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 item0" >'+
				'<h2 style="font-size: 20px;text-align: center; font-weight: bold;">購物景點</h2>'+
				'<hr style="margin-top: 2px;margin-bottom: 3px;border-top: 2px solid #3333;width: 100%;"/>'+
				'<p style="font-size: 18px;text-align: center;color: #FF0000;">這裡打上購物景點說明</p>'+
				'</div>'+
				'</div>'
    },
	//特別安排內文安排行程
	{
        //標題
		title: '特別安排內文',
		//範本圖
        image: 'Title_2_3.PNG',
		//樣板描述
        description: '安排行程',
		//自訂樣板內容
        html: 	'<div class="Title_2">'+
				'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 item0" >'+
				'<h2 style="font-size: 20px;text-align: center; font-weight: bold;">安排行程</h2>'+
				'<hr style="margin-top: 2px;margin-bottom: 3px;border-top: 2px solid #3333;width: 100%;"/>'+
				'<p style="font-size: 18px;">這裡打上安排行程說明</p>'+
				'</div>'+
				'</div>'
    },
	//特別安排內文表演節目安排
	{
        //標題
		title: '特別安排內文',
		//範本圖
        image: 'Title_2_4.PNG',
		//樣板描述
        description: '表演節目安排',
		//自訂樣板內容
        html: 	'<div class="Title_2">'+
				'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 item0" >'+
				'<h2 style="font-size: 20px;text-align: center; font-weight: bold;">表演節目安排</h2>'+
				'<hr style="margin-top: 2px;margin-bottom: 3px;border-top: 2px solid #3333;width: 100%;"/>'+
				'<p style="font-size: 18px;">這裡打上表演節目安排說明</p>'+
				'</div>'+
				'</div>'
    },
	//特別安排內文美食安排
	{
        //標題
		title: '特別安排內文',
		//範本圖
        image: 'Title_2_5.PNG',
		//樣板描述
        description: '美食安排',
		//自訂樣板內容
        html: 	'<div class="Title_2">'+
				'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 item0" >'+
				'<h2 style="font-size: 20px;text-align: center; font-weight: bold;">美食安排</h2>'+
				'<hr style="margin-top: 2px;margin-bottom: 3px;border-top: 2px solid #3333;width: 100%;"/>'+
				'<p style="font-size: 18px;">這裡打上美食安排說明</p>'+
				'</div>'+
				'</div>'
    },
	//特別安排內文住宿安排
	{
        //標題
		title: '特別安排內文',
		//範本圖
        image: 'Title_2_6.PNG',
		//樣板描述
        description: '住宿安排',
		//自訂樣板內容
        html: 	'<div class="Title_2">'+
				'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 item0" >'+
				'<h2 style="font-size: 20px;text-align: center; font-weight: bold;">住宿安排</h2>'+
				'<hr style="margin-top: 2px;margin-bottom: 3px;border-top: 2px solid #3333;width: 100%;"/>'+
				'<p style="font-size: 18px;">這裡打上住宿安排說明</p>'+
				'</div>'+
				'</div>'
    },
	//特別安排內文自費項目
	{
        //標題
		title: '特別安排內文',
		//範本圖
        image: 'Title_2_7.PNG',
		//樣板描述
        description: '自費項目',
		//自訂樣板內容
        html: 	'<div class="Title_2">'+
				'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 item0" >'+
				'<h2 style="font-size: 20px;text-align: center; font-weight: bold;">自費項目</h2>'+
				'<hr style="margin-top: 2px;margin-bottom: 3px;border-top: 2px solid #3333;width: 100%;"/>'+
				'<p style="font-size: 18px;">這裡打上自費項目說明</p>'+
				'</div>'+
				'</div>'
    },
	//行程特色內文備註貼心提醒
	{
        //標題
		title: '行程特色內文備註',
		//範本圖
        image: 'note_1.png',
		//樣板描述
        description: '貼心提醒',
		//自訂樣板內容
        html: 	'<div class="note_1">'+
				'<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 item0">'+
				'<h2 style="color:#ff0000; font-size:20px; text-align:center">貼心提醒</h2>'+
				'<hr style="border-top:2px solid #3333; margin-bottom:3px; margin-top:2px; width:100%" />'+
				'<p style="color:#ff0000; font-size:16px; text-align:center">備註:以下所有景點圖片本公司景點圖片僅供參考，因景點季節不同景色稍許會改變 敬請見諒!</p>'+
				'</div>'+
				'</div>'
    },
	//一張圖片景點介紹
	{
        //標題
		title: '行程特色一張圖片景點介紹',
		//範本圖
        image: 'day_style_1.png',
		//樣板描述
        description: '一張圖片景點介紹',
		//自訂樣板內容
        html: 	'<div class="day_style_1">'+
				'<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 item">'+
				'<h1 style="bottom:-5px; color:#ffffff; font-size:22px; font-weight:bold; padding:5px 20px; position:relative; right:-5px; text-shadow:black 0.1em 0.1em 0.2em">景點名稱</h1>'+
				'<img class="img-thumbnail" src="/eWeb_summittour/images/svg/style_demo_img.gif" style="margin-top:-50px" /></div>'+
				'<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 item">'+
				'<h2>景點名稱</h2>'+
				'<p>請輸入圖片文字介紹或敘述</p>'+
				'</div>'+
				'</div>'
    },
	//二張圖片景點介紹
	{
        //標題
		title: '行程特色二張圖片景點介紹',
		//範本圖
        image: 'day_style_2.png',
		//樣板描述
        description: '二張圖片景點介紹',
		//自訂樣板內容
        html: 	'<div class="col-lg-12 col-md-12 day_style_2 item">'+
				'<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 item">'+
				'<div class="img_box">'+
				'<h1 style="bottom:-5px; color:#ffffff; font-size:22px; font-weight:bold; padding:5px 20px; position:relative; right:-5px; text-shadow:black 0.1em 0.1em 0.2em">景點名稱</h1>'+
				'<img class="img-thumbnail" src="/eWeb_summittour/images/svg/style_demo_img.gif" style="margin-top:-50px" /></div>'+
				'<h2>景點名稱</h2>'+
				'<p>請輸入圖片文字介紹或敘述。</p>'+
				'</div>'+
				'<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 item">'+
				'<div class="img_box">'+
				'<h1 style="bottom:-5px; color:#ffffff; font-size:22px; font-weight:bold; padding:5px 20px; position:relative; right:-5px; text-shadow:black 0.1em 0.1em 0.2em">景點名稱</h1>'+
				'<img class="img-thumbnail" src="/eWeb_summittour/images/svg/style_demo_img.gif" style="margin-top:-50px" /></div>'+
				'<h2>景點名稱</h2>'+
				'<p>請輸入圖片文字介紹或敘述。</p>'+
				'</div>'+
				'</div>'
    },
	//三張圖片景點介紹
	{
        //標題
		title: '行程特色三張圖片景點介紹',
		//範本圖
        image: 'day_style_3.png',
		//樣板描述
        description: '三張圖片景點介紹',
		//自訂樣板內容
        html: 	'<div class="day_style_3">'+
				'<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 item">'+
				'<div class="img_box">'+
				'<h1 style="bottom:5px; color:#ffffff; font-size:22px; font-weight:bold; padding:5px 20px; position:relative; right:5px; text-shadow:black 0.1em 0.1em 0.2em">景點名稱</h1>'+
				'<img class="img-thumbnail" src="/eWeb_summittour/images/style_demo_img.gif" /></div>'+
				'<h2>景點名稱</h2>'+
				'<p>請輸入圖片文字介紹或敘述。</p>'+
				'</div>'+
				'<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 item">'+
				'<div class="img_box">'+
				'<h1 style="bottom:5px; color:#ffffff; font-size:22px; font-weight:bold; padding:5px 20px; position:relative; right:5px; text-shadow:black 0.1em 0.1em 0.2em">景點名稱</h1>'+
				'<img class="img-thumbnail" src="/eWeb_summittour/images/style_demo_img.gif" /></div>'+
				'<h2>景點名稱</h2>'+
				'<p>請輸入圖片文字介紹或敘述。</p>'+
				'</div>'+
				'<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 item">'+
				'<div class="img_box">'+
				'<h1 style="bottom:5px; color:#ffffff; font-size:22px; font-weight:bold; padding:5px 20px; position:relative; right:5px; text-shadow:black 0.1em 0.1em 0.2em">景點名稱</h1>'+
				'<img class="img-thumbnail" src="/eWeb_summittour/images/style_demo_img.gif" /></div>'+
				'<h2>景點名稱</h2>'+
				'<p>請輸入圖片文字介紹或敘述。</p>'+
				'</div>'+
				'</div>'
    },
	//飯店介紹
	{
        //標題
		title: '行程特色飯店介紹',
		//範本圖
        image: 'day_style_4.png',
		//樣板描述
        description: '飯店介紹',
		//自訂樣板內容
        html: 	'<div class="day_style_7">'+
				'<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 item"><img class="golo" src="/eWeb_summittour/images/style_demo_img.gif" style="display:block; margin:auto; padding-bottom:10px; width:50%" />'+
				'<h2 style="font-size:28px; text-align:center">飯店地理名稱</h2>'+
				'<hr style="border-top:2px solid #999999; margin:0 auto; padding-bottom:6px; width:40%" />'+
				'<h2 style="font-size:24px; text-align:center">飯店名稱</h2>'+
				'<h2 style="font-size:18px; margin-top:-20px; text-align:center">飯店英文名稱</h2>'+
				'<h2 style="font-size:18px; margin-top:-20px; text-align:center">幾星級飯店、國際幾星級飯店</h2>'+
				'</div>'+
				'<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 item">'+
				'<p style="font-size:16px">請輸入飯店文字介紹或敘述</p>'+
				'<div class="hotel" style="border:rgba(0,0,0,0.5) 1px solid; cursor:pointer; font-size:14px; left:90%; margin-bottom:0px; margin-left:0px; margin-right:0px; margin-top:0px; padding-left:4px; position:relative; top:10px; width:68px"><a href="飯店網址" style="text-decoration:none; color: #000" target="_blank">飯店官網 </a></div>'+
				'</div>'+
				'<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 item" style="margin-top:10px;">'+
				'<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 item"><img class="img-thumbnail" src="/eWeb_summittour/images/style_demo_img.gif" /></div>'+
				'<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 item"><img class="img-thumbnail" src="/eWeb_summittour/images/style_demo_img.gif"/></div>'+
				'<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 item"><img class="img-thumbnail" src="/eWeb_summittour/images/style_demo_img.gif" /></div>'+
				'</div>'+
				'<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 item" style="margin-bottom:10px; margin-left:-20px; margin-right:-20px; margin-top:10px">'+
				'<hr style="border-top:2px solid #3333; margin-bottom:3px; margin-top:2px; width:100%" /></div>'+
				'</div>'
    },
	//單張圖片
	{
        //標題
		title: '行程特色單張圖片',
		//範本圖
        image: 'day_style_5.png',
		//樣板描述
        description: '單張圖片',
		//自訂樣板內容
        html: 	'<div class="day_style_9">'+
				'<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 item"><img src="/eWeb_summittour/images/svg/style_demo_img.gif" /></div>'+
				'</div>'
    },
	//橫幅圖片+內文
	{
        //標題
		title: '行程特色橫幅圖片+內文',
		//範本圖
        image: 'day_style_6.png',
		//樣板描述
        description: '橫幅圖片+內文',
		//自訂樣板內容
        html: 	'<div class="day_style_10">'+
				'<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 item"><img src="/eWeb_summittour/images/svg/style_demo_img.gif" /></div>'+
				'<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 item">'+
				'<h2>圖片標題文字</h2>'+
				'<p>請輸入圖片文字介紹或敘述</p>'+
				'</div>'+
				'</div>'
    },
	//每日行程內文
	{
        //標題
		title: '每日行程內文',
		//範本圖
        image: 'day_style_7.png',
		//樣板描述
        description: '景點介紹或敘述',
		//自訂樣板內容
        html: 	'<div class="day_style_1">'+
				'<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 item">'+
				'<h2>景點名稱</h2>'+
				'<p>景點介紹或敘述</p>'+
				'</div>'+
				'</div>'
    },
	//每日行程內文備註特別說明
	{
        //標題
		title: '每日行程內文備註',
		//範本圖
        image: 'day_style_8_1.png',
		//樣板描述
        description: '特別說明',
		//自訂樣板內容
        html: 	'<div class="daily_box_note2 col-lg-12 col-md-12 col-sm-12 col-xs-12 item">'+
				'<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 item">'+
				'<h2 style="color:red;">特別說明</h2>'+
				'<ul style="margin-bottom: 15px;">'+
				'<li style="color:red;">敘述文字</li>'+
				'<li style="color:red;">敘述文字</li>'+
				'<li style="color:red;">敘述文字</li>'+
				'</ul>'+
				'<hr style="border-top:2px solid #3333; margin-bottom:3px; margin-top:2px; width:100%">'+
				'<h2 style="color:red;">貼心提醒</h2>'+
				'<ul>'+
				'<li style="color:red;">敘述文字</li>'+
				'<li style="color:red;">敘述文字</li>'+
				'<li style="color:red;">敘述文字</li>'+
				'</ul>'+
				'</div>'+
				'</div>'

    },	
	//放置YouTube影片
	{
        //標題
		title: '放置YouTube影片',
		//範本圖
        image: 'day_style_13.png',
		//樣板描述
        description: '放置YouTube影片',
		//自訂樣板內容
        html: 	'<div class="day_style_12">'+
				'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 item">'+
				'<iframe style="margin-top:10px;" width="100%" height="620px" src="https://www.youtube-nocookie.com/embed/【影片ID】?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>'+
				'</div>'+
				'</div>'
    },
	//放置表格
	{
        //標題
		title: '放置表格',
		//範本圖
        image: 'day_style_12.png',
		//樣板描述
        description: '放置表格',
		//自訂樣板內容
        html: 	'<div class="day_style_12">'+
				'<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">'+
				'<table align="center" class="blueTable" style="width:100%">'+
				'<thead>'+
				'<tr>'+
				'<th scope="col"><strong>標題</strong></th>'+
				'<th scope="col"><strong>標題</strong></th>'+
				'<th scope="col"><strong>標題</strong></th>'+
				'<th scope="col"><strong>標題</strong></th>'+
				'</tr>'+
				'</thead>'+
				'<tbody>'+
				'<tr>'+
				'<td style="text-align:center; vertical-align:middle">內文</td>'+
				'<td style="text-align:center; vertical-align:middle">內文</td>'+
				'<td style="text-align:center; vertical-align:middle">內文</td>'+
				'<td style="text-align:center; vertical-align:middle">內文</td>'+
				'</tr>'+
				'<tr>'+
				'<td style="text-align:center; vertical-align:middle">內文</td>'+
				'<td style="text-align:center; vertical-align:middle">內文</td>'+
				'<td style="text-align:center; vertical-align:middle">內文</td>'+
				'<td style="text-align:center; vertical-align:middle">內文</td>'+
				'</tr>'+
				'<tr>'+
				'<td style="text-align:center; vertical-align:middle">內文</td>'+
				'<td style="text-align:center; vertical-align:middle">內文</td>'+
				'<td style="text-align:center; vertical-align:middle">內文</td>'+
				'<td style="text-align:center; vertical-align:middle">內文</td>'+
				'</tr>'+
				'</tbody>'+
				'</table>'+
				'</div>'+
				'</div>'
    }
    ]
});
function getXML(autoPlayFG,artUrl,USR_CLS) {
  $.ajax({
    url: "/eweb_summittour/DXML/GCModule.xml",
    type: "GET",
    dataType: "xml",
    timeout: 1E3,
    error: function(b) {
      getXML(autoPlayFG,artUrl,USR_CLS);
    },
    success: function(b) {

	//主題頁-首頁
	if($("#myCarousel").length > 0 ){
        var cnt = 0; cntto = -1
        $("#myCarousel").html("");
                
				$("<ol/>", {
                    "class": "carousel-indicators"
				}).appendTo("#myCarousel");  

				$("<div/>", {
					"role": "listbox",
                    "class": "carousel-inner"
				}).appendTo("#myCarousel");	
                

				$("<a/>", {
                    "id": "left_btn",
                    "class": "left carousel-control",
					"href": "#myCarousel",
                    "role": "button",
                    "data-slide": "prev"
				}).appendTo("#myCarousel");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-left"
                    }).appendTo("#myCarousel #left_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Previous"
                    }).appendTo("#myCarousel #left_btn");

				$("<a/>", {
                    "id": "right_btn",
                    "class": "right carousel-control",
					"href": "#myCarousel",
                    "role": "button",
                    "data-slide": "next"
				}).appendTo("#myCarousel");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-right"
                    }).appendTo("#myCarousel #right_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Next"
                    }).appendTo("#myCarousel #right_btn");

	  
        $(b).find("Site[SITE_CD='1']").children("Page[MP_ID='MPIDX']").children("Module[DTL_ID='5']").children("Items").each(function(a) {          
		  cnt++;
          cntto++;
                
		var MPIDX_IMG_URL = $(this).children("IMG_URL").text();
        var MPIDX_LINK_URL = $(this).children("LINK_URL").text();
        var MPIDX_LINK_NM = $(this).children("LINK_NM").text();

		
				$("<li/>", {
                    "id": "slide_to_"+cnt,
                    "class": " ",
                    "data-target": "#myCarousel",
                    "data-slide-to": cntto,
					"html" : MPIDX_LINK_NM
				}).appendTo("#myCarousel .carousel-indicators");

				$("<div/>", {
					"id": "listbox_li_"+cnt,
                    "class": "item"
				}).appendTo("#myCarousel .carousel-inner");
				
					$("<a/>", {
						"id": "listbox_li_a_"+cnt,
						"href" : MPIDX_LINK_URL
					}).appendTo("div#listbox_li_"+cnt);
                
						$("<div/>", {
							"id": "listbox_li_a_img"+cnt,
							"class": "img"
						}).appendTo("a#listbox_li_a_"+cnt);
												
							$("<img/>", {
								"src": MPIDX_IMG_URL
							}).appendTo("#listbox_li_a_img"+cnt);


				$("<div/>", {
					"id": "listbox_li_a_div"+cnt,
                    "class" : "carousel-caption",
				}).appendTo("#listbox_li_"+cnt+" #listbox_li_a_"+cnt);
				
				
      });
	  
		//首頁-大圖輪播JS
		if (autoPlayFG){
			$("#listbox_li_1").addClass("active");
            $("#slide_to_1").addClass("active");
                $("#slide_to_1").click(function(){
                    $("#slide_to_1").addClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_2").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").addClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_3").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").addClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_4").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").addClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_5").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").addClass("active");
                });
                

		}
		
	  }  
	  
		//推薦01 圖+文
		if($("#module_content_1").length > 0 ){
		var cnt = 0
		$("#module_content_1 #module_content_row_1").html("")
		  
		$(b).find("Site[SITE_CD='1']").children("Page[MP_ID='MPIDX']").children("Module[DTL_ID='1']").each(function(a) {        
		cnt++;
			var MPIDX_DTL_TITLE = $(this).children("DTL_TITLE").text();
			var MPIDX_DTL_RK = $(this).children("DTL_RK").text();
			var MPIDX_DTL_IMG_LINK = $(this).children("DTL_IMG_LINK").text();
			
		  $("<div/>", {
				"id" : "module_content_row_1",
				"class" : "row"
			}).appendTo("#module_content_1");		
			
		  $("<section/>", {
				"id" : "module_1",
				"class" : "module_content"
			}).appendTo("#module_content_row_1");		
			
					var lcnt = 0

					$($(this)).children("Items").each(function(a) {

					  var MPIDX_LINK_NM = $(this).children("LINK_NM").text();
					  var MPIDX_LINK_URL = $(this).children("LINK_URL").text();
					  var MPIDX_IMG_DR = $(this).children("IMG_DR").text();
					  var MPIDX_IMG_URL = $(this).children("IMG_URL").text();
					  var MPIDX_LINK_NAMT = $(this).children("LINK_NAMT").text();
					lcnt++;
					  
						$("<div/>", {
							"id" : "module_div"+lcnt,
							"class" : "col-xs-12 col-sm-4 col-md-4 col-lg-4 tile module_three"
						}).appendTo("section#module_1");
						
							$("<a/>", {
								"id" : "module_div_a"+lcnt,
								"target" : "_blank",
								"href" : MPIDX_LINK_URL
							}).appendTo("div#module_div"+lcnt);
							
								$("<img/>", {
									"id" : "module_div_a_img"+lcnt,
									"src" : MPIDX_IMG_URL
								}).appendTo("a#module_div_a"+lcnt);

										
					});			
				
			})
		}
		//推薦01 圖+文
		if($("#module_full_01").length > 0 ){
		var cnt = 0
		$("#module_full_01 #module_full_row_01").html("")
		  
		$(b).find("Site[SITE_CD='1']").children("Page[MP_ID='MPIDX']").children("Module[DTL_ID='2']").each(function(a) {        
		cnt++;
			var MPIDX_DTL_TITLE = $(this).children("DTL_TITLE").text();
			var MPIDX_DTL_RK = $(this).children("DTL_RK").text();
			var MPIDX_DTL_IMG_LINK = $(this).children("DTL_IMG_LINK").text();
			
		  $("<div/>", {
				"id" : "module_full_row_01",
				"class" : "row"
			}).appendTo("#module_full_01");		
			
			  $("<section/>", {
					"id" : "module_full_content_01",
					"class" : "module_content"
				}).appendTo("#module_full_row_01");		
			
					var lcnt = 0

					$($(this)).children("Items").each(function(a) {

					  var MPIDX_LINK_NM = $(this).children("LINK_NM").text();
					  var MPIDX_LINK_URL = $(this).children("LINK_URL").text();
					  var MPIDX_IMG_DR = $(this).children("IMG_DR").text();
					  var MPIDX_IMG_URL = $(this).children("IMG_URL").text();
					  var MPIDX_LINK_NAMT = $(this).children("LINK_NAMT").text();
					lcnt++;
					  
						$("<div/>", {
							"id" : "module_full_01_div"+lcnt,
							"class" : "col-xs-12 col-sm-12 col-md-12 col-lg-12 tile module_three"
						}).appendTo("section#module_full_content_01");
						
							$("<a/>", {
								"id" : "module_full_01_div_a"+lcnt,
								"target" : "_blank",
								"href" : MPIDX_LINK_URL
							}).appendTo("div#module_full_01_div"+lcnt);
							
								$("<img/>", {
									"id" : "module_div_a_img"+lcnt,
									"src" : MPIDX_IMG_URL
								}).appendTo("a#module_full_01_div_a"+lcnt);

										
					});			
				
			})
		}
		//推薦01 圖+文
		if($("#module_full_02").length > 0 ){
		var cnt = 0
		$("#module_full_02 #module_full_row_02").html("")
		  
		$(b).find("Site[SITE_CD='1']").children("Page[MP_ID='MPIDX']").children("Module[DTL_ID='3']").each(function(a) {        
		cnt++;
			var MPIDX_DTL_TITLE = $(this).children("DTL_TITLE").text();
			var MPIDX_DTL_RK = $(this).children("DTL_RK").text();
			var MPIDX_DTL_IMG_LINK = $(this).children("DTL_IMG_LINK").text();
			
		  $("<div/>", {
				"id" : "module_full_row_02",
				"class" : "row"
			}).appendTo("#module_full_02");		
			
			  $("<section/>", {
					"id" : "module_full_content_02",
					"class" : "module_content"
				}).appendTo("#module_full_row_02");		
			
					var lcnt = 0

					$($(this)).children("Items").each(function(a) {

					  var MPIDX_LINK_NM = $(this).children("LINK_NM").text();
					  var MPIDX_LINK_URL = $(this).children("LINK_URL").text();
					  var MPIDX_IMG_DR = $(this).children("IMG_DR").text();
					  var MPIDX_IMG_URL = $(this).children("IMG_URL").text();
					  var MPIDX_LINK_NAMT = $(this).children("LINK_NAMT").text();
					lcnt++;
					  
						$("<div/>", {
							"id" : "module_full_02_div"+lcnt,
							"class" : "col-xs-12 col-sm-12 col-md-12 col-lg-12 tile module_three"
						}).appendTo("section#module_full_content_02");
						
							$("<a/>", {
								"id" : "module_full_02_div_a"+lcnt,
								"target" : "_blank",
								"href" : MPIDX_LINK_URL
							}).appendTo("div#module_full_02_div"+lcnt);
							
								$("<img/>", {
									"id" : "module_div_a_img"+lcnt,
									"src" : MPIDX_IMG_URL
								}).appendTo("a#module_full_02_div_a"+lcnt);

										
					});			
				
			})
		}
		//推薦01 圖+文
		if($("#module_full_03").length > 0 ){
		var cnt = 0
		$("#module_full_03 #module_full_row_03").html("")
		  
		$(b).find("Site[SITE_CD='1']").children("Page[MP_ID='MPIDX']").children("Module[DTL_ID='4']").each(function(a) {        
		cnt++;
			var MPIDX_DTL_TITLE = $(this).children("DTL_TITLE").text();
			var MPIDX_DTL_RK = $(this).children("DTL_RK").text();
			var MPIDX_DTL_IMG_LINK = $(this).children("DTL_IMG_LINK").text();
			
		  $("<div/>", {
				"id" : "module_full_row_03",
				"class" : "row"
			}).appendTo("#module_full_03");		
			
			  $("<section/>", {
					"id" : "module_full_content_03",
					"class" : "module_content"
				}).appendTo("#module_full_row_03");		
			
					var lcnt = 0

					$($(this)).children("Items").each(function(a) {

					  var MPIDX_LINK_NM = $(this).children("LINK_NM").text();
					  var MPIDX_LINK_URL = $(this).children("LINK_URL").text();
					  var MPIDX_IMG_DR = $(this).children("IMG_DR").text();
					  var MPIDX_IMG_URL = $(this).children("IMG_URL").text();
					  var MPIDX_LINK_NAMT = $(this).children("LINK_NAMT").text();
					lcnt++;
					  
						$("<div/>", {
							"id" : "module_full_03_div"+lcnt,
							"class" : "col-xs-12 col-sm-12 col-md-12 col-lg-12 tile module_three"
						}).appendTo("section#module_full_content_03");
						
							$("<a/>", {
								"id" : "module_full_03_div_a"+lcnt,
								"target" : "_blank",
								"href" : MPIDX_LINK_URL
							}).appendTo("div#module_full_03_div"+lcnt);
							
								$("<img/>", {
									"id" : "module_div_a_img"+lcnt,
									"src" : MPIDX_IMG_URL
								}).appendTo("a#module_full_03_div_a"+lcnt);

										
					});			
				
			})
		}

	//主題頁-港澳大陸	
	if($("#myCarousel_01").length > 0 ){
        var cnt = 0; cntto = -1
        $("#myCarousel_01").html("");
                
				$("<ol/>", {
                    "class": "carousel-indicators"
				}).appendTo("#myCarousel_01");  

				$("<div/>", {
					"role": "listbox",
                    "class": "carousel-inner"
				}).appendTo("#myCarousel_01");	
                

				$("<a/>", {
                    "id": "left_btn",
                    "class": "left carousel-control",
					"href": "#myCarousel_01",
                    "role": "button",
                    "data-slide": "prev"
				}).appendTo("#myCarousel_01");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-left"
                    }).appendTo("#myCarousel_01 #left_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Previous"
                    }).appendTo("#myCarousel_01 #left_btn");

				$("<a/>", {
                    "id": "right_btn",
                    "class": "right carousel-control",
					"href": "#myCarousel_01",
                    "role": "button",
                    "data-slide": "next"
				}).appendTo("#myCarousel_01");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-right"
                    }).appendTo("#myCarousel_01 #right_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Next"
                    }).appendTo("#myCarousel_01 #right_btn");

	  
        $(b).find("Site[SITE_CD='1']").children("Page[MP_ID='MPI01']").children("Module[DTL_ID='1']").children("Items").each(function(a) {          
		  cnt++;
          cntto++;
                
		var MPI01_IMG_URL = $(this).children("IMG_URL").text();
        var MPI01_LINK_URL = $(this).children("LINK_URL").text();
        var MPI01_LINK_NM = $(this).children("LINK_NM").text();

		
				$("<li/>", {
                    "id": "slide_to_"+cnt,
                    "class": " ",
                    "data-target": "#myCarousel_01",
                    "data-slide-to": cntto,
					"html" : MPI01_LINK_NM
				}).appendTo("#myCarousel_01 .carousel-indicators");

				$("<div/>", {
					"id": "listbox_li_"+cnt,
                    "class": "item"
				}).appendTo("#myCarousel_01 .carousel-inner");
				
					$("<a/>", {
						"id": "listbox_li_a_"+cnt,
						"href" : MPI01_LINK_URL
					}).appendTo("#listbox_li_"+cnt);
                
						$("<div/>", {
							"id": "listbox_li_a_img"+cnt,
							"class": "img"
						}).appendTo("a#listbox_li_a_"+cnt);
												
							$("<img/>", {
								"src": MPI01_IMG_URL
							}).appendTo("#listbox_li_a_img"+cnt);

				$("<div/>", {
					"id": "listbox_li_a_div"+cnt,
                    "class" : "carousel-caption",
				}).appendTo("#listbox_li_"+cnt+" #listbox_li_a_"+cnt);
				
				
      });
	  
		//首頁-大圖輪播JS
		if (autoPlayFG){
			$("#listbox_li_1").addClass("active");
            $("#slide_to_1").addClass("active");
                $("#slide_to_1").click(function(){
                    $("#slide_to_1").addClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_2").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").addClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_3").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").addClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_4").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").addClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_5").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").addClass("active");
                });
                

		}
		
	  }
	  
	  	//小圖
		if($("#module_content_page01").length > 0 ){
		var cnt = 0
		$("#module_content_page01 #module_content_row_page01").html("")
		  
		$(b).find("Site[SITE_CD='1']").children("Page[MP_ID='MPI01']").children("Module[DTL_ID='2']").each(function(a) {        
		cnt++;
			var MPI01_DTL_TITLE = $(this).children("DTL_TITLE").text();
			var MPI01_DTL_RK = $(this).children("DTL_RK").text();
			var MPI01_DTL_IMG_LINK = $(this).children("DTL_IMG_LINK").text();
			
		  $("<div/>", {
				"id" : "module_content_row_page01",
				"class" : "row"
			}).appendTo("#module_content_page01");		
			
		  $("<section/>", {
				"id" : "module_1",
				"class" : "module_content"
			}).appendTo("#module_content_row_page01");		
			
					var lcnt = 0

					$($(this)).children("Items").each(function(a) {

					  var MPI01_LINK_NM = $(this).children("LINK_NM").text();
					  var MPI01_LINK_URL = $(this).children("LINK_URL").text();
					  var MPI01_IMG_DR = $(this).children("IMG_DR").text();
					  var MPI01_IMG_URL = $(this).children("IMG_URL").text();
					  var MPI01_LINK_NAMT = $(this).children("LINK_NAMT").text();
					lcnt++;
					  
						$("<div/>", {
							"id" : "module_div"+lcnt,
							"class" : "col-xs-12 col-sm-4 col-md-4 col-lg-4 tile module_three"
						}).appendTo("section#module_1");
						
							$("<a/>", {
								"id" : "module_div_a"+lcnt,
								"target" : "_blank",
								"href" : MPI01_LINK_URL
							}).appendTo("div#module_div"+lcnt);
							
								$("<img/>", {
									"id" : "module_div_a_img"+lcnt,
									"src" : MPI01_IMG_URL
								}).appendTo("a#module_div_a"+lcnt);

										
					});			
				
			})
		}
		
	//主題頁-東南亞	
	if($("#myCarousel_02").length > 0 ){
        var cnt = 0; cntto = -1
        $("#myCarousel_02").html("");
                
				$("<ol/>", {
                    "class": "carousel-indicators"
				}).appendTo("#myCarousel_02");  

				$("<div/>", {
					"role": "listbox",
                    "class": "carousel-inner"
				}).appendTo("#myCarousel_02");	
                

				$("<a/>", {
                    "id": "left_btn",
                    "class": "left carousel-control",
					"href": "#myCarousel_02",
                    "role": "button",
                    "data-slide": "prev"
				}).appendTo("#myCarousel_02");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-left"
                    }).appendTo("#myCarousel_02 #left_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Previous"
                    }).appendTo("#myCarousel_02 #left_btn");

				$("<a/>", {
                    "id": "right_btn",
                    "class": "right carousel-control",
					"href": "#myCarousel_02",
                    "role": "button",
                    "data-slide": "next"
				}).appendTo("#myCarousel_02");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-right"
                    }).appendTo("#myCarousel_02 #right_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Next"
                    }).appendTo("#myCarousel_02 #right_btn");

	  
        $(b).find("Site[SITE_CD='1']").children("Page[MP_ID='MPI02']").children("Module[DTL_ID='1']").children("Items").each(function(a) {          
		  cnt++;
          cntto++;
                
		var MPI02_IMG_URL = $(this).children("IMG_URL").text();
        var MPI02_LINK_URL = $(this).children("LINK_URL").text();
        var MPI02_LINK_NM = $(this).children("LINK_NM").text();

		
				$("<li/>", {
                    "id": "slide_to_"+cnt,
                    "class": " ",
                    "data-target": "#myCarousel_02",
                    "data-slide-to": cntto,
					"html" : MPI02_LINK_NM
				}).appendTo("#myCarousel_02 .carousel-indicators");

				$("<div/>", {
					"id": "listbox_li_"+cnt,
                    "class": "item"
				}).appendTo("#myCarousel_02 .carousel-inner");
				
					$("<a/>", {
						"id": "listbox_li_a_"+cnt,
						"href" : MPI02_LINK_URL
					}).appendTo("#listbox_li_"+cnt);
                
						$("<div/>", {
							"id": "listbox_li_a_img"+cnt,
							"class": "img"
						}).appendTo("a#listbox_li_a_"+cnt);
												
							$("<img/>", {
								"src": MPI02_IMG_URL
							}).appendTo("#listbox_li_a_img"+cnt);

				$("<div/>", {
					"id": "listbox_li_a_div"+cnt,
                    "class" : "carousel-caption",
				}).appendTo("#listbox_li_"+cnt+" #listbox_li_a_"+cnt);
				
				
      });
	  
		//首頁-大圖輪播JS
		if (autoPlayFG){
			$("#listbox_li_1").addClass("active");
            $("#slide_to_1").addClass("active");
                $("#slide_to_1").click(function(){
                    $("#slide_to_1").addClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_2").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").addClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_3").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").addClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_4").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").addClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_5").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").addClass("active");
                });
                

		}
		
	  }
	  
	  	//小圖
		if($("#module_content_page02").length > 0 ){
		var cnt = 0
		$("#module_content_page02 #module_content_row_page02").html("")
		  
		$(b).find("Site[SITE_CD='1']").children("Page[MP_ID='MPI02']").children("Module[DTL_ID='2']").each(function(a) {        
		cnt++;
			var MPI02_DTL_TITLE = $(this).children("DTL_TITLE").text();
			var MPI02_DTL_RK = $(this).children("DTL_RK").text();
			var MPI02_DTL_IMG_LINK = $(this).children("DTL_IMG_LINK").text();
			
		  $("<div/>", {
				"id" : "module_content_row_page02",
				"class" : "row"
			}).appendTo("#module_content_page02");		
			
		  $("<section/>", {
				"id" : "module_1",
				"class" : "module_content"
			}).appendTo("#module_content_row_page02");		
			
					var lcnt = 0

					$($(this)).children("Items").each(function(a) {

					  var MPI02_LINK_NM = $(this).children("LINK_NM").text();
					  var MPI02_LINK_URL = $(this).children("LINK_URL").text();
					  var MPI02_IMG_DR = $(this).children("IMG_DR").text();
					  var MPI02_IMG_URL = $(this).children("IMG_URL").text();
					  var MPI02_LINK_NAMT = $(this).children("LINK_NAMT").text();
					lcnt++;
					  
						$("<div/>", {
							"id" : "module_div"+lcnt,
							"class" : "col-xs-12 col-sm-4 col-md-4 col-lg-4 tile module_three"
						}).appendTo("section#module_1");
						
							$("<a/>", {
								"id" : "module_div_a"+lcnt,
								"target" : "_blank",
								"href" : MPI02_LINK_URL
							}).appendTo("div#module_div"+lcnt);
							
								$("<img/>", {
									"id" : "module_div_a_img"+lcnt,
									"src" : MPI02_IMG_URL
								}).appendTo("a#module_div_a"+lcnt);

										
					});			
				
			})
		}
		
	//主題頁-東北亞	
	if($("#myCarousel_03").length > 0 ){
        var cnt = 0; cntto = -1
        $("#myCarousel_03").html("");
                
				$("<ol/>", {
                    "class": "carousel-indicators"
				}).appendTo("#myCarousel_03");  

				$("<div/>", {
					"role": "listbox",
                    "class": "carousel-inner"
				}).appendTo("#myCarousel_03");	
                

				$("<a/>", {
                    "id": "left_btn",
                    "class": "left carousel-control",
					"href": "#myCarousel_03",
                    "role": "button",
                    "data-slide": "prev"
				}).appendTo("#myCarousel_03");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-left"
                    }).appendTo("#myCarousel_03 #left_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Previous"
                    }).appendTo("#myCarousel_03 #left_btn");

				$("<a/>", {
                    "id": "right_btn",
                    "class": "right carousel-control",
					"href": "#myCarousel_03",
                    "role": "button",
                    "data-slide": "next"
				}).appendTo("#myCarousel_03");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-right"
                    }).appendTo("#myCarousel_03 #right_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Next"
                    }).appendTo("#myCarousel_03 #right_btn");

	  
        $(b).find("Site[SITE_CD='1']").children("Page[MP_ID='MPI03']").children("Module[DTL_ID='1']").children("Items").each(function(a) {          
		  cnt++;
          cntto++;
                
		var MPI03_IMG_URL = $(this).children("IMG_URL").text();
        var MPI03_LINK_URL = $(this).children("LINK_URL").text();
        var MPI03_LINK_NM = $(this).children("LINK_NM").text();

		
				$("<li/>", {
                    "id": "slide_to_"+cnt,
                    "class": " ",
                    "data-target": "#myCarousel_03",
                    "data-slide-to": cntto,
					"html" : MPI03_LINK_NM
				}).appendTo("#myCarousel_03 .carousel-indicators");

				$("<div/>", {
					"id": "listbox_li_"+cnt,
                    "class": "item"
				}).appendTo("#myCarousel_03 .carousel-inner");
				
					$("<a/>", {
						"id": "listbox_li_a_"+cnt,
						"href" : MPI03_LINK_URL
					}).appendTo("#listbox_li_"+cnt);
                
						$("<div/>", {
							"id": "listbox_li_a_img"+cnt,
							"class": "img"
						}).appendTo("a#listbox_li_a_"+cnt);
												
							$("<img/>", {
								"src": MPI03_IMG_URL
							}).appendTo("#listbox_li_a_img"+cnt);

				$("<div/>", {
					"id": "listbox_li_a_div"+cnt,
                    "class" : "carousel-caption",
				}).appendTo("#listbox_li_"+cnt+" #listbox_li_a_"+cnt);
				
				
      });
	  
		//首頁-大圖輪播JS
		if (autoPlayFG){
			$("#listbox_li_1").addClass("active");
            $("#slide_to_1").addClass("active");
                $("#slide_to_1").click(function(){
                    $("#slide_to_1").addClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_2").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").addClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_3").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").addClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_4").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").addClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_5").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").addClass("active");
                });
                

		}
		
	  }
	  
	  	//小圖
		if($("#module_content_page03").length > 0 ){
		var cnt = 0
		$("#module_content_page03 #module_content_row_page03").html("")
		  
		$(b).find("Site[SITE_CD='1']").children("Page[MP_ID='MPI03']").children("Module[DTL_ID='2']").each(function(a) {        
		cnt++;
			var MPI03_DTL_TITLE = $(this).children("DTL_TITLE").text();
			var MPI03_DTL_RK = $(this).children("DTL_RK").text();
			var MPI03_DTL_IMG_LINK = $(this).children("DTL_IMG_LINK").text();
			
		  $("<div/>", {
				"id" : "module_content_row_page03",
				"class" : "row"
			}).appendTo("#module_content_page03");		
			
		  $("<section/>", {
				"id" : "module_1",
				"class" : "module_content"
			}).appendTo("#module_content_row_page03");		
			
					var lcnt = 0

					$($(this)).children("Items").each(function(a) {

					  var MPI03_LINK_NM = $(this).children("LINK_NM").text();
					  var MPI03_LINK_URL = $(this).children("LINK_URL").text();
					  var MPI03_IMG_DR = $(this).children("IMG_DR").text();
					  var MPI03_IMG_URL = $(this).children("IMG_URL").text();
					  var MPI03_LINK_NAMT = $(this).children("LINK_NAMT").text();
					lcnt++;
					  
						$("<div/>", {
							"id" : "module_div"+lcnt,
							"class" : "col-xs-12 col-sm-4 col-md-4 col-lg-4 tile module_three"
						}).appendTo("section#module_1");
						
							$("<a/>", {
								"id" : "module_div_a"+lcnt,
								"target" : "_blank",
								"href" : MPI03_LINK_URL
							}).appendTo("div#module_div"+lcnt);
							
								$("<img/>", {
									"id" : "module_div_a_img"+lcnt,
									"src" : MPI03_IMG_URL
								}).appendTo("a#module_div_a"+lcnt);

										
					});			
				
			})
		}

	//主題頁-南亞、中亞非	
	if($("#myCarousel_04").length > 0 ){
        var cnt = 0; cntto = -1
        $("#myCarousel_04").html("");
                
				$("<ol/>", {
                    "class": "carousel-indicators"
				}).appendTo("#myCarousel_04");  

				$("<div/>", {
					"role": "listbox",
                    "class": "carousel-inner"
				}).appendTo("#myCarousel_04");	
                

				$("<a/>", {
                    "id": "left_btn",
                    "class": "left carousel-control",
					"href": "#myCarousel_04",
                    "role": "button",
                    "data-slide": "prev"
				}).appendTo("#myCarousel_04");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-left"
                    }).appendTo("#myCarousel_04 #left_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Previous"
                    }).appendTo("#myCarousel_04 #left_btn");

				$("<a/>", {
                    "id": "right_btn",
                    "class": "right carousel-control",
					"href": "#myCarousel_04",
                    "role": "button",
                    "data-slide": "next"
				}).appendTo("#myCarousel_04");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-right"
                    }).appendTo("#myCarousel_04 #right_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Next"
                    }).appendTo("#myCarousel_04 #right_btn");

	  
        $(b).find("Site[SITE_CD='1']").children("Page[MP_ID='MPI04']").children("Module[DTL_ID='1']").children("Items").each(function(a) {          
		  cnt++;
          cntto++;
                
		var MPI04_IMG_URL = $(this).children("IMG_URL").text();
        var MPI04_LINK_URL = $(this).children("LINK_URL").text();
        var MPI04_LINK_NM = $(this).children("LINK_NM").text();

		
				$("<li/>", {
                    "id": "slide_to_"+cnt,
                    "class": " ",
                    "data-target": "#myCarousel_04",
                    "data-slide-to": cntto,
					"html" : MPI04_LINK_NM
				}).appendTo("#myCarousel_04 .carousel-indicators");

				$("<div/>", {
					"id": "listbox_li_"+cnt,
                    "class": "item"
				}).appendTo("#myCarousel_04 .carousel-inner");
				
					$("<a/>", {
						"id": "listbox_li_a_"+cnt,
						"href" : MPI04_LINK_URL
					}).appendTo("#listbox_li_"+cnt);
                
						$("<div/>", {
							"id": "listbox_li_a_img"+cnt,
							"class": "img"
						}).appendTo("a#listbox_li_a_"+cnt);
												
							$("<img/>", {
								"src": MPI04_IMG_URL
							}).appendTo("#listbox_li_a_img"+cnt);

				$("<div/>", {
					"id": "listbox_li_a_div"+cnt,
                    "class" : "carousel-caption",
				}).appendTo("#listbox_li_"+cnt+" #listbox_li_a_"+cnt);
				
				
      });
	  
		//首頁-大圖輪播JS
		if (autoPlayFG){
			$("#listbox_li_1").addClass("active");
            $("#slide_to_1").addClass("active");
                $("#slide_to_1").click(function(){
                    $("#slide_to_1").addClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_2").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").addClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_3").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").addClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_4").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").addClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_5").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").addClass("active");
                });
                

		}
		
	  }
	  
	  	//小圖
		if($("#module_content_page04").length > 0 ){
		var cnt = 0
		$("#module_content_page04 #module_content_row_page04").html("")
		  
		$(b).find("Site[SITE_CD='1']").children("Page[MP_ID='MPI04']").children("Module[DTL_ID='2']").each(function(a) {        
		cnt++;
			var MPI04_DTL_TITLE = $(this).children("DTL_TITLE").text();
			var MPI04_DTL_RK = $(this).children("DTL_RK").text();
			var MPI04_DTL_IMG_LINK = $(this).children("DTL_IMG_LINK").text();
			
		  $("<div/>", {
				"id" : "module_content_row_page04",
				"class" : "row"
			}).appendTo("#module_content_page04");		
			
		  $("<section/>", {
				"id" : "module_1",
				"class" : "module_content"
			}).appendTo("#module_content_row_page04");		
			
					var lcnt = 0

					$($(this)).children("Items").each(function(a) {

					  var MPI04_LINK_NM = $(this).children("LINK_NM").text();
					  var MPI04_LINK_URL = $(this).children("LINK_URL").text();
					  var MPI04_IMG_DR = $(this).children("IMG_DR").text();
					  var MPI04_IMG_URL = $(this).children("IMG_URL").text();
					  var MPI04_LINK_NAMT = $(this).children("LINK_NAMT").text();
					lcnt++;
					  
						$("<div/>", {
							"id" : "module_div"+lcnt,
							"class" : "col-xs-12 col-sm-4 col-md-4 col-lg-4 tile module_three"
						}).appendTo("section#module_1");
						
							$("<a/>", {
								"id" : "module_div_a"+lcnt,
								"target" : "_blank",
								"href" : MPI04_LINK_URL
							}).appendTo("div#module_div"+lcnt);
							
								$("<img/>", {
									"id" : "module_div_a_img"+lcnt,
									"src" : MPI04_IMG_URL
								}).appendTo("a#module_div_a"+lcnt);

										
					});			
				
			})
		}
		
	//主題頁-歐洲	
	if($("#myCarousel_05").length > 0 ){
        var cnt = 0; cntto = -1
        $("#myCarousel_05").html("");
                
				$("<ol/>", {
                    "class": "carousel-indicators"
				}).appendTo("#myCarousel_05");  

				$("<div/>", {
					"role": "listbox",
                    "class": "carousel-inner"
				}).appendTo("#myCarousel_05");	
                

				$("<a/>", {
                    "id": "left_btn",
                    "class": "left carousel-control",
					"href": "#myCarousel_05",
                    "role": "button",
                    "data-slide": "prev"
				}).appendTo("#myCarousel_05");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-left"
                    }).appendTo("#myCarousel_05 #left_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Previous"
                    }).appendTo("#myCarousel_05 #left_btn");

				$("<a/>", {
                    "id": "right_btn",
                    "class": "right carousel-control",
					"href": "#myCarousel_05",
                    "role": "button",
                    "data-slide": "next"
				}).appendTo("#myCarousel_05");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-right"
                    }).appendTo("#myCarousel_05 #right_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Next"
                    }).appendTo("#myCarousel_05 #right_btn");

	  
        $(b).find("Site[SITE_CD='1']").children("Page[MP_ID='MPI05']").children("Module[DTL_ID='1']").children("Items").each(function(a) {          
		  cnt++;
          cntto++;
                
		var MPI05_IMG_URL = $(this).children("IMG_URL").text();
        var MPI05_LINK_URL = $(this).children("LINK_URL").text();
        var MPI05_LINK_NM = $(this).children("LINK_NM").text();

		
				$("<li/>", {
                    "id": "slide_to_"+cnt,
                    "class": " ",
                    "data-target": "#myCarousel_05",
                    "data-slide-to": cntto,
					"html" : MPI05_LINK_NM
				}).appendTo("#myCarousel_05 .carousel-indicators");

				$("<div/>", {
					"id": "listbox_li_"+cnt,
                    "class": "item"
				}).appendTo("#myCarousel_05 .carousel-inner");
				
					$("<a/>", {
						"id": "listbox_li_a_"+cnt,
						"href" : MPI05_LINK_URL
					}).appendTo("#listbox_li_"+cnt);
                
						$("<div/>", {
							"id": "listbox_li_a_img"+cnt,
							"class": "img"
						}).appendTo("a#listbox_li_a_"+cnt);
												
							$("<img/>", {
								"src": MPI05_IMG_URL
							}).appendTo("#listbox_li_a_img"+cnt);

				$("<div/>", {
					"id": "listbox_li_a_div"+cnt,
                    "class" : "carousel-caption",
				}).appendTo("#listbox_li_"+cnt+" #listbox_li_a_"+cnt);
				
				
      });
	  
		//首頁-大圖輪播JS
		if (autoPlayFG){
			$("#listbox_li_1").addClass("active");
            $("#slide_to_1").addClass("active");
                $("#slide_to_1").click(function(){
                    $("#slide_to_1").addClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_2").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").addClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_3").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").addClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_4").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").addClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_5").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").addClass("active");
                });
                

		}
		
	  }
	  
	  	//小圖
		if($("#module_content_page05").length > 0 ){
		var cnt = 0
		$("#module_content_page05 #module_content_row_page05").html("")
		  
		$(b).find("Site[SITE_CD='1']").children("Page[MP_ID='MPI05']").children("Module[DTL_ID='2']").each(function(a) {        
		cnt++;
			var MPI05_DTL_TITLE = $(this).children("DTL_TITLE").text();
			var MPI05_DTL_RK = $(this).children("DTL_RK").text();
			var MPI05_DTL_IMG_LINK = $(this).children("DTL_IMG_LINK").text();
			
		  $("<div/>", {
				"id" : "module_content_row_page05",
				"class" : "row"
			}).appendTo("#module_content_page05");		
			
		  $("<section/>", {
				"id" : "module_1",
				"class" : "module_content"
			}).appendTo("#module_content_row_page05");		
			
					var lcnt = 0

					$($(this)).children("Items").each(function(a) {

					  var MPI05_LINK_NM = $(this).children("LINK_NM").text();
					  var MPI05_LINK_URL = $(this).children("LINK_URL").text();
					  var MPI05_IMG_DR = $(this).children("IMG_DR").text();
					  var MPI05_IMG_URL = $(this).children("IMG_URL").text();
					  var MPI05_LINK_NAMT = $(this).children("LINK_NAMT").text();
					lcnt++;
					  
						$("<div/>", {
							"id" : "module_div"+lcnt,
							"class" : "col-xs-12 col-sm-4 col-md-4 col-lg-4 tile module_three"
						}).appendTo("section#module_1");
						
							$("<a/>", {
								"id" : "module_div_a"+lcnt,
								"target" : "_blank",
								"href" : MPI05_LINK_URL
							}).appendTo("div#module_div"+lcnt);
							
								$("<img/>", {
									"id" : "module_div_a_img"+lcnt,
									"src" : MPI05_IMG_URL
								}).appendTo("a#module_div_a"+lcnt);

										
					});			
				
			})
		}
		
	//主題頁-紐澳	
	if($("#myCarousel_06").length > 0 ){
        var cnt = 0; cntto = -1
        $("#myCarousel_06").html("");
                
				$("<ol/>", {
                    "class": "carousel-indicators"
				}).appendTo("#myCarousel_06");  

				$("<div/>", {
					"role": "listbox",
                    "class": "carousel-inner"
				}).appendTo("#myCarousel_06");	
                

				$("<a/>", {
                    "id": "left_btn",
                    "class": "left carousel-control",
					"href": "#myCarousel_06",
                    "role": "button",
                    "data-slide": "prev"
				}).appendTo("#myCarousel_06");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-left"
                    }).appendTo("#myCarousel_06 #left_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Previous"
                    }).appendTo("#myCarousel_06 #left_btn");

				$("<a/>", {
                    "id": "right_btn",
                    "class": "right carousel-control",
					"href": "#myCarousel_06",
                    "role": "button",
                    "data-slide": "next"
				}).appendTo("#myCarousel_06");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-right"
                    }).appendTo("#myCarousel_06 #right_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Next"
                    }).appendTo("#myCarousel_06 #right_btn");

	  
        $(b).find("Site[SITE_CD='1']").children("Page[MP_ID='MPI06']").children("Module[DTL_ID='1']").children("Items").each(function(a) {          
		  cnt++;
          cntto++;
                
		var MPI06_IMG_URL = $(this).children("IMG_URL").text();
        var MPI06_LINK_URL = $(this).children("LINK_URL").text();
        var MPI06_LINK_NM = $(this).children("LINK_NM").text();

		
				$("<li/>", {
                    "id": "slide_to_"+cnt,
                    "class": " ",
                    "data-target": "#myCarousel_06",
                    "data-slide-to": cntto,
					"html" : MPI06_LINK_NM
				}).appendTo("#myCarousel_06 .carousel-indicators");

				$("<div/>", {
					"id": "listbox_li_"+cnt,
                    "class": "item"
				}).appendTo("#myCarousel_06 .carousel-inner");
				
					$("<a/>", {
						"id": "listbox_li_a_"+cnt,
						"href" : MPI06_LINK_URL
					}).appendTo("#listbox_li_"+cnt);
                
						$("<div/>", {
							"id": "listbox_li_a_img"+cnt,
							"class": "img"
						}).appendTo("a#listbox_li_a_"+cnt);
												
							$("<img/>", {
								"src": MPI06_IMG_URL
							}).appendTo("#listbox_li_a_img"+cnt);

				$("<div/>", {
					"id": "listbox_li_a_div"+cnt,
                    "class" : "carousel-caption",
				}).appendTo("#listbox_li_"+cnt+" #listbox_li_a_"+cnt);
				
				
      });
	  
		//首頁-大圖輪播JS
		if (autoPlayFG){
			$("#listbox_li_1").addClass("active");
            $("#slide_to_1").addClass("active");
                $("#slide_to_1").click(function(){
                    $("#slide_to_1").addClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_2").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").addClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_3").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").addClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_4").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").addClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_5").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").addClass("active");
                });
                

		}
		
	  }
	  
	  	//小圖
		if($("#module_content_page06").length > 0 ){
		var cnt = 0
		$("#module_content_page06 #module_content_row_page06").html("")
		  
		$(b).find("Site[SITE_CD='1']").children("Page[MP_ID='MPI06']").children("Module[DTL_ID='2']").each(function(a) {        
		cnt++;
			var MPI06_DTL_TITLE = $(this).children("DTL_TITLE").text();
			var MPI06_DTL_RK = $(this).children("DTL_RK").text();
			var MPI06_DTL_IMG_LINK = $(this).children("DTL_IMG_LINK").text();
			
		  $("<div/>", {
				"id" : "module_content_row_page06",
				"class" : "row"
			}).appendTo("#module_content_page06");		
			
		  $("<section/>", {
				"id" : "module_1",
				"class" : "module_content"
			}).appendTo("#module_content_row_page06");		
			
					var lcnt = 0

					$($(this)).children("Items").each(function(a) {

					  var MPI06_LINK_NM = $(this).children("LINK_NM").text();
					  var MPI06_LINK_URL = $(this).children("LINK_URL").text();
					  var MPI06_IMG_DR = $(this).children("IMG_DR").text();
					  var MPI06_IMG_URL = $(this).children("IMG_URL").text();
					  var MPI06_LINK_NAMT = $(this).children("LINK_NAMT").text();
					lcnt++;
					  
						$("<div/>", {
							"id" : "module_div"+lcnt,
							"class" : "col-xs-12 col-sm-4 col-md-4 col-lg-4 tile module_three"
						}).appendTo("section#module_1");
						
							$("<a/>", {
								"id" : "module_div_a"+lcnt,
								"target" : "_blank",
								"href" : MPI06_LINK_URL
							}).appendTo("div#module_div"+lcnt);
							
								$("<img/>", {
									"id" : "module_div_a_img"+lcnt,
									"src" : MPI06_IMG_URL
								}).appendTo("a#module_div_a"+lcnt);

										
					});			
				
			})
		}		
		
	//主題頁-美加	
	if($("#myCarousel_07").length > 0 ){
        var cnt = 0; cntto = -1
        $("#myCarousel_07").html("");
                
				$("<ol/>", {
                    "class": "carousel-indicators"
				}).appendTo("#myCarousel_07");  

				$("<div/>", {
					"role": "listbox",
                    "class": "carousel-inner"
				}).appendTo("#myCarousel_07");	
                

				$("<a/>", {
                    "id": "left_btn",
                    "class": "left carousel-control",
					"href": "#myCarousel_07",
                    "role": "button",
                    "data-slide": "prev"
				}).appendTo("#myCarousel_07");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-left"
                    }).appendTo("#myCarousel_07 #left_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Previous"
                    }).appendTo("#myCarousel_07 #left_btn");

				$("<a/>", {
                    "id": "right_btn",
                    "class": "right carousel-control",
					"href": "#myCarousel_07",
                    "role": "button",
                    "data-slide": "next"
				}).appendTo("#myCarousel_07");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-right"
                    }).appendTo("#myCarousel_07 #right_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Next"
                    }).appendTo("#myCarousel_07 #right_btn");

	  
        $(b).find("Site[SITE_CD='1']").children("Page[MP_ID='MPI07']").children("Module[DTL_ID='1']").children("Items").each(function(a) {          
		  cnt++;
          cntto++;
                
		var MPI07_IMG_URL = $(this).children("IMG_URL").text();
        var MPI07_LINK_URL = $(this).children("LINK_URL").text();
        var MPI07_LINK_NM = $(this).children("LINK_NM").text();

		
				$("<li/>", {
                    "id": "slide_to_"+cnt,
                    "class": " ",
                    "data-target": "#myCarousel_07",
                    "data-slide-to": cntto,
					"html" : MPI07_LINK_NM
				}).appendTo("#myCarousel_07 .carousel-indicators");

				$("<div/>", {
					"id": "listbox_li_"+cnt,
                    "class": "item"
				}).appendTo("#myCarousel_07 .carousel-inner");
				
					$("<a/>", {
						"id": "listbox_li_a_"+cnt,
						"href" : MPI07_LINK_URL
					}).appendTo("#listbox_li_"+cnt);
                
						$("<div/>", {
							"id": "listbox_li_a_img"+cnt,
							"class": "img"
						}).appendTo("a#listbox_li_a_"+cnt);
												
							$("<img/>", {
								"src": MPI07_IMG_URL
							}).appendTo("#listbox_li_a_img"+cnt);

				$("<div/>", {
					"id": "listbox_li_a_div"+cnt,
                    "class" : "carousel-caption",
				}).appendTo("#listbox_li_"+cnt+" #listbox_li_a_"+cnt);
				
				
      });
	  
		//首頁-大圖輪播JS
		if (autoPlayFG){
			$("#listbox_li_1").addClass("active");
            $("#slide_to_1").addClass("active");
                $("#slide_to_1").click(function(){
                    $("#slide_to_1").addClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_2").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").addClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_3").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").addClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_4").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").addClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_5").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").addClass("active");
                });
                

		}
		
	  }
	  
	  	//小圖
		if($("#module_content_page07").length > 0 ){
		var cnt = 0
		$("#module_content_page07 #module_content_row_page07").html("")
		  
		$(b).find("Site[SITE_CD='1']").children("Page[MP_ID='MPI07']").children("Module[DTL_ID='2']").each(function(a) {        
		cnt++;
			var MPI07_DTL_TITLE = $(this).children("DTL_TITLE").text();
			var MPI07_DTL_RK = $(this).children("DTL_RK").text();
			var MPI07_DTL_IMG_LINK = $(this).children("DTL_IMG_LINK").text();
			
		  $("<div/>", {
				"id" : "module_content_row_page07",
				"class" : "row"
			}).appendTo("#module_content_page07");		
			
		  $("<section/>", {
				"id" : "module_1",
				"class" : "module_content"
			}).appendTo("#module_content_row_page07");		
			
					var lcnt = 0

					$($(this)).children("Items").each(function(a) {

					  var MPI07_LINK_NM = $(this).children("LINK_NM").text();
					  var MPI07_LINK_URL = $(this).children("LINK_URL").text();
					  var MPI07_IMG_DR = $(this).children("IMG_DR").text();
					  var MPI07_IMG_URL = $(this).children("IMG_URL").text();
					  var MPI07_LINK_NAMT = $(this).children("LINK_NAMT").text();
					lcnt++;
					  
						$("<div/>", {
							"id" : "module_div"+lcnt,
							"class" : "col-xs-12 col-sm-4 col-md-4 col-lg-4 tile module_three"
						}).appendTo("section#module_1");
						
							$("<a/>", {
								"id" : "module_div_a"+lcnt,
								"target" : "_blank",
								"href" : MPI07_LINK_URL
							}).appendTo("div#module_div"+lcnt);
							
								$("<img/>", {
									"id" : "module_div_a_img"+lcnt,
									"src" : MPI07_IMG_URL
								}).appendTo("a#module_div_a"+lcnt);

										
					});			
				
			})
		}		
		
	//主題頁-海島	
	if($("#myCarousel_08").length > 0 ){
        var cnt = 0; cntto = -1
        $("#myCarousel_08").html("");
                
				$("<ol/>", {
                    "class": "carousel-indicators"
				}).appendTo("#myCarousel_08");  

				$("<div/>", {
					"role": "listbox",
                    "class": "carousel-inner"
				}).appendTo("#myCarousel_08");	
                

				$("<a/>", {
                    "id": "left_btn",
                    "class": "left carousel-control",
					"href": "#myCarousel_08",
                    "role": "button",
                    "data-slide": "prev"
				}).appendTo("#myCarousel_08");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-left"
                    }).appendTo("#myCarousel_08 #left_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Previous"
                    }).appendTo("#myCarousel_08 #left_btn");

				$("<a/>", {
                    "id": "right_btn",
                    "class": "right carousel-control",
					"href": "#myCarousel_08",
                    "role": "button",
                    "data-slide": "next"
				}).appendTo("#myCarousel_08");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-right"
                    }).appendTo("#myCarousel_08 #right_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Next"
                    }).appendTo("#myCarousel_08 #right_btn");

	  
        $(b).find("Site[SITE_CD='1']").children("Page[MP_ID='MPI08']").children("Module[DTL_ID='1']").children("Items").each(function(a) {          
		  cnt++;
          cntto++;
                
		var MPI08_IMG_URL = $(this).children("IMG_URL").text();
        var MPI08_LINK_URL = $(this).children("LINK_URL").text();
        var MPI08_LINK_NM = $(this).children("LINK_NM").text();

		
				$("<li/>", {
                    "id": "slide_to_"+cnt,
                    "class": " ",
                    "data-target": "#myCarousel_08",
                    "data-slide-to": cntto,
					"html" : MPI08_LINK_NM
				}).appendTo("#myCarousel_08 .carousel-indicators");

				$("<div/>", {
					"id": "listbox_li_"+cnt,
                    "class": "item"
				}).appendTo("#myCarousel_08 .carousel-inner");
				
					$("<a/>", {
						"id": "listbox_li_a_"+cnt,
						"href" : MPI08_LINK_URL
					}).appendTo("#listbox_li_"+cnt);
                
						$("<div/>", {
							"id": "listbox_li_a_img"+cnt,
							"class": "img"
						}).appendTo("a#listbox_li_a_"+cnt);
												
							$("<img/>", {
								"src": MPI08_IMG_URL
							}).appendTo("#listbox_li_a_img"+cnt);

				$("<div/>", {
					"id": "listbox_li_a_div"+cnt,
                    "class" : "carousel-caption",
				}).appendTo("#listbox_li_"+cnt+" #listbox_li_a_"+cnt);
				
				
      });
	  
		//首頁-大圖輪播JS
		if (autoPlayFG){
			$("#listbox_li_1").addClass("active");
            $("#slide_to_1").addClass("active");
                $("#slide_to_1").click(function(){
                    $("#slide_to_1").addClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_2").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").addClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_3").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").addClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_4").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").addClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_5").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").addClass("active");
                });
                

		}
		
	  }
	  
	  	//小圖
		if($("#module_content_page08").length > 0 ){
		var cnt = 0
		$("#module_content_page08 #module_content_row_page08").html("")
		  
		$(b).find("Site[SITE_CD='1']").children("Page[MP_ID='MPI08']").children("Module[DTL_ID='2']").each(function(a) {        
		cnt++;
			var MPI08_DTL_TITLE = $(this).children("DTL_TITLE").text();
			var MPI08_DTL_RK = $(this).children("DTL_RK").text();
			var MPI08_DTL_IMG_LINK = $(this).children("DTL_IMG_LINK").text();
			
		  $("<div/>", {
				"id" : "module_content_row_page08",
				"class" : "row"
			}).appendTo("#module_content_page08");		
			
		  $("<section/>", {
				"id" : "module_1",
				"class" : "module_content"
			}).appendTo("#module_content_row_page08");		
			
					var lcnt = 0

					$($(this)).children("Items").each(function(a) {

					  var MPI08_LINK_NM = $(this).children("LINK_NM").text();
					  var MPI08_LINK_URL = $(this).children("LINK_URL").text();
					  var MPI08_IMG_DR = $(this).children("IMG_DR").text();
					  var MPI08_IMG_URL = $(this).children("IMG_URL").text();
					  var MPI08_LINK_NAMT = $(this).children("LINK_NAMT").text();
					lcnt++;
					  
						$("<div/>", {
							"id" : "module_div"+lcnt,
							"class" : "col-xs-12 col-sm-4 col-md-4 col-lg-4 tile module_three"
						}).appendTo("section#module_1");
						
							$("<a/>", {
								"id" : "module_div_a"+lcnt,
								"target" : "_blank",
								"href" : MPI08_LINK_URL
							}).appendTo("div#module_div"+lcnt);
							
								$("<img/>", {
									"id" : "module_div_a_img"+lcnt,
									"src" : MPI08_IMG_URL
								}).appendTo("a#module_div_a"+lcnt);

										
					});			
				
			})
		}		
		
	//主題頁-中南半島	
	if($("#myCarousel_09").length > 0 ){
        var cnt = 0; cntto = -1
        $("#myCarousel_09").html("");
                
				$("<ol/>", {
                    "class": "carousel-indicators"
				}).appendTo("#myCarousel_09");  

				$("<div/>", {
					"role": "listbox",
                    "class": "carousel-inner"
				}).appendTo("#myCarousel_09");	
                

				$("<a/>", {
                    "id": "left_btn",
                    "class": "left carousel-control",
					"href": "#myCarousel_09",
                    "role": "button",
                    "data-slide": "prev"
				}).appendTo("#myCarousel_09");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-left"
                    }).appendTo("#myCarousel_09 #left_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Previous"
                    }).appendTo("#myCarousel_09 #left_btn");

				$("<a/>", {
                    "id": "right_btn",
                    "class": "right carousel-control",
					"href": "#myCarousel_09",
                    "role": "button",
                    "data-slide": "next"
				}).appendTo("#myCarousel_09");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-right"
                    }).appendTo("#myCarousel_09 #right_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Next"
                    }).appendTo("#myCarousel_09 #right_btn");

	  
        $(b).find("Site[SITE_CD='1']").children("Page[MP_ID='MPI09']").children("Module[DTL_ID='1']").children("Items").each(function(a) {          
		  cnt++;
          cntto++;
                
		var MPI09_IMG_URL = $(this).children("IMG_URL").text();
        var MPI09_LINK_URL = $(this).children("LINK_URL").text();
        var MPI09_LINK_NM = $(this).children("LINK_NM").text();

		
				$("<li/>", {
                    "id": "slide_to_"+cnt,
                    "class": " ",
                    "data-target": "#myCarousel_09",
                    "data-slide-to": cntto,
					"html" : MPI09_LINK_NM
				}).appendTo("#myCarousel_09 .carousel-indicators");

				$("<div/>", {
					"id": "listbox_li_"+cnt,
                    "class": "item"
				}).appendTo("#myCarousel_09 .carousel-inner");
				
					$("<a/>", {
						"id": "listbox_li_a_"+cnt,
						"href" : MPI09_LINK_URL
					}).appendTo("#listbox_li_"+cnt);
                
						$("<div/>", {
							"id": "listbox_li_a_img"+cnt,
							"class": "img"
						}).appendTo("a#listbox_li_a_"+cnt);
												
							$("<img/>", {
								"src": MPI09_IMG_URL
							}).appendTo("#listbox_li_a_img"+cnt);

				$("<div/>", {
					"id": "listbox_li_a_div"+cnt,
                    "class" : "carousel-caption",
				}).appendTo("#listbox_li_"+cnt+" #listbox_li_a_"+cnt);
				
				
      });
	  
	//首頁-大圖輪播JS
	if (autoPlayFG){
			$("#listbox_li_1").addClass("active");
            $("#slide_to_1").addClass("active");
                $("#slide_to_1").click(function(){
                    $("#slide_to_1").addClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_2").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").addClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_3").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").addClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_4").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").addClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_5").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").addClass("active");
                });
                

		}
		
	  }
	  
	  	//小圖
		if($("#module_content_page09").length > 0 ){
		var cnt = 0
		$("#module_content_page09 #module_content_row_page09").html("")
		  
		$(b).find("Site[SITE_CD='1']").children("Page[MP_ID='MPI09']").children("Module[DTL_ID='2']").each(function(a) {        
		cnt++;
			var MPI09_DTL_TITLE = $(this).children("DTL_TITLE").text();
			var MPI09_DTL_RK = $(this).children("DTL_RK").text();
			var MPI09_DTL_IMG_LINK = $(this).children("DTL_IMG_LINK").text();
			
		  $("<div/>", {
				"id" : "module_content_row_page09",
				"class" : "row"
			}).appendTo("#module_content_page09");		
			
		  $("<section/>", {
				"id" : "module_1",
				"class" : "module_content"
			}).appendTo("#module_content_row_page09");		
			
					var lcnt = 0

					$($(this)).children("Items").each(function(a) {

					  var MPI09_LINK_NM = $(this).children("LINK_NM").text();
					  var MPI09_LINK_URL = $(this).children("LINK_URL").text();
					  var MPI09_IMG_DR = $(this).children("IMG_DR").text();
					  var MPI09_IMG_URL = $(this).children("IMG_URL").text();
					  var MPI09_LINK_NAMT = $(this).children("LINK_NAMT").text();
					lcnt++;
					  
						$("<div/>", {
							"id" : "module_div"+lcnt,
							"class" : "col-xs-12 col-sm-4 col-md-4 col-lg-4 tile module_three"
						}).appendTo("section#module_1");
						
							$("<a/>", {
								"id" : "module_div_a"+lcnt,
								"target" : "_blank",
								"href" : MPI09_LINK_URL
							}).appendTo("div#module_div"+lcnt);
							
								$("<img/>", {
									"id" : "module_div_a_img"+lcnt,
									"src" : MPI09_IMG_URL
								}).appendTo("a#module_div_a"+lcnt);

										
					});			
				
			})
		}		
		
	//主題頁-國內離島	
	if($("#myCarousel_10").length > 0 ){
        var cnt = 0; cntto = -1
        $("#myCarousel_10").html("");
                
				$("<ol/>", {
                    "class": "carousel-indicators"
				}).appendTo("#myCarousel_10");  

				$("<div/>", {
					"role": "listbox",
                    "class": "carousel-inner"
				}).appendTo("#myCarousel_10");	
                

				$("<a/>", {
                    "id": "left_btn",
                    "class": "left carousel-control",
					"href": "#myCarousel_10",
                    "role": "button",
                    "data-slide": "prev"
				}).appendTo("#myCarousel_10");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-left"
                    }).appendTo("#myCarousel_10 #left_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Previous"
                    }).appendTo("#myCarousel_10 #left_btn");

				$("<a/>", {
                    "id": "right_btn",
                    "class": "right carousel-control",
					"href": "#myCarousel_10",
                    "role": "button",
                    "data-slide": "next"
				}).appendTo("#myCarousel_10");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-right"
                    }).appendTo("#myCarousel_10 #right_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Next"
                    }).appendTo("#myCarousel_10 #right_btn");

	  
        $(b).find("Site[SITE_CD='1']").children("Page[MP_ID='MPI10']").children("Module[DTL_ID='1']").children("Items").each(function(a) {          
		  cnt++;
          cntto++;
                
		var MPI10_IMG_URL = $(this).children("IMG_URL").text();
        var MPI10_LINK_URL = $(this).children("LINK_URL").text();
        var MPI10_LINK_NM = $(this).children("LINK_NM").text();

		
				$("<li/>", {
                    "id": "slide_to_"+cnt,
                    "class": " ",
                    "data-target": "#myCarousel_10",
                    "data-slide-to": cntto,
					"html" : MPI10_LINK_NM
				}).appendTo("#myCarousel_10 .carousel-indicators");

				$("<div/>", {
					"id": "listbox_li_"+cnt,
                    "class": "item"
				}).appendTo("#myCarousel_10 .carousel-inner");
				
					$("<a/>", {
						"id": "listbox_li_a_"+cnt,
						"href" : MPI10_LINK_URL
					}).appendTo("#listbox_li_"+cnt);
                
						$("<div/>", {
							"id": "listbox_li_a_img"+cnt,
							"class": "img"
						}).appendTo("a#listbox_li_a_"+cnt);
												
							$("<img/>", {
								"src": MPI10_IMG_URL
							}).appendTo("#listbox_li_a_img"+cnt);

				$("<div/>", {
					"id": "listbox_li_a_div"+cnt,
                    "class" : "carousel-caption",
				}).appendTo("#listbox_li_"+cnt+" #listbox_li_a_"+cnt);
				
				
      });
	  
		//首頁-大圖輪播JS
		if (autoPlayFG){
			$("#listbox_li_1").addClass("active");
            $("#slide_to_1").addClass("active");
                $("#slide_to_1").click(function(){
                    $("#slide_to_1").addClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_2").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").addClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_3").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").addClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_4").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").addClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_5").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").addClass("active");
                });
                

		}
		
	  }
	  
	  	//小圖
		if($("#module_content_page10").length > 0 ){
		var cnt = 0
		$("#module_content_page10 #module_content_row_page10").html("")
		  
		$(b).find("Site[SITE_CD='1']").children("Page[MP_ID='MPI10']").children("Module[DTL_ID='2']").each(function(a) {        
		cnt++;
			var MPI10_DTL_TITLE = $(this).children("DTL_TITLE").text();
			var MPI10_DTL_RK = $(this).children("DTL_RK").text();
			var MPI10_DTL_IMG_LINK = $(this).children("DTL_IMG_LINK").text();
			
		  $("<div/>", {
				"id" : "module_content_row_page10",
				"class" : "row"
			}).appendTo("#module_content_page10");		
			
		  $("<section/>", {
				"id" : "module_1",
				"class" : "module_content"
			}).appendTo("#module_content_row_page10");		
			
					var lcnt = 0

					$($(this)).children("Items").each(function(a) {

					  var MPI10_LINK_NM = $(this).children("LINK_NM").text();
					  var MPI10_LINK_URL = $(this).children("LINK_URL").text();
					  var MPI10_IMG_DR = $(this).children("IMG_DR").text();
					  var MPI10_IMG_URL = $(this).children("IMG_URL").text();
					  var MPI10_LINK_NAMT = $(this).children("LINK_NAMT").text();
					lcnt++;
					  
						$("<div/>", {
							"id" : "module_div"+lcnt,
							"class" : "col-xs-12 col-sm-4 col-md-4 col-lg-4 tile module_three"
						}).appendTo("section#module_1");
						
							$("<a/>", {
								"id" : "module_div_a"+lcnt,
								"target" : "_blank",
								"href" : MPI10_LINK_URL
							}).appendTo("div#module_div"+lcnt);
							
								$("<img/>", {
									"id" : "module_div_a_img"+lcnt,
									"src" : MPI10_IMG_URL
								}).appendTo("a#module_div_a"+lcnt);

										
					});			
				
			})
		}	
		
	//主題頁-郵輪	
	if($("#myCarousel_11").length > 0 ){
        var cnt = 0; cntto = -1
        $("#myCarousel_11").html("");
                
				$("<ol/>", {
                    "class": "carousel-indicators"
				}).appendTo("#myCarousel_11");  

				$("<div/>", {
					"role": "listbox",
                    "class": "carousel-inner"
				}).appendTo("#myCarousel_11");	
                

				$("<a/>", {
                    "id": "left_btn",
                    "class": "left carousel-control",
					"href": "#myCarousel_11",
                    "role": "button",
                    "data-slide": "prev"
				}).appendTo("#myCarousel_11");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-left"
                    }).appendTo("#myCarousel_11 #left_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Previous"
                    }).appendTo("#myCarousel_11 #left_btn");

				$("<a/>", {
                    "id": "right_btn",
                    "class": "right carousel-control",
					"href": "#myCarousel_11",
                    "role": "button",
                    "data-slide": "next"
				}).appendTo("#myCarousel_11");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-right"
                    }).appendTo("#myCarousel_11 #right_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Next"
                    }).appendTo("#myCarousel_11 #right_btn");

	  
        $(b).find("Site[SITE_CD='1']").children("Page[MP_ID='MPI11']").children("Module[DTL_ID='1']").children("Items").each(function(a) {          
		  cnt++;
          cntto++;
                
		var MPI11_IMG_URL = $(this).children("IMG_URL").text();
        var MPI11_LINK_URL = $(this).children("LINK_URL").text();
        var MPI11_LINK_NM = $(this).children("LINK_NM").text();

		
				$("<li/>", {
                    "id": "slide_to_"+cnt,
                    "class": " ",
                    "data-target": "#myCarousel_11",
                    "data-slide-to": cntto,
					"html" : MPI11_LINK_NM
				}).appendTo("#myCarousel_11 .carousel-indicators");

				$("<div/>", {
					"id": "listbox_li_"+cnt,
                    "class": "item"
				}).appendTo("#myCarousel_11 .carousel-inner");
				
					$("<a/>", {
						"id": "listbox_li_a_"+cnt,
						"href" : MPI11_LINK_URL
					}).appendTo("#listbox_li_"+cnt);
                
						$("<div/>", {
							"id": "listbox_li_a_img"+cnt,
							"class": "img"
						}).appendTo("a#listbox_li_a_"+cnt);
												
							$("<img/>", {
								"src": MPI11_IMG_URL
							}).appendTo("#listbox_li_a_img"+cnt);

				$("<div/>", {
					"id": "listbox_li_a_div"+cnt,
                    "class" : "carousel-caption",
				}).appendTo("#listbox_li_"+cnt+" #listbox_li_a_"+cnt);
				
				
      });
	  
		//首頁-大圖輪播JS
		if (autoPlayFG){
			$("#listbox_li_1").addClass("active");
            $("#slide_to_1").addClass("active");
                $("#slide_to_1").click(function(){
                    $("#slide_to_1").addClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_2").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").addClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_3").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").addClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_4").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").addClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_5").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").addClass("active");
                });
                

		}
		
	  }
	  
	  	//小圖
		if($("#module_content_page11").length > 0 ){
		var cnt = 0
		$("#module_content_page11 #module_content_row_page11").html("")
		  
		$(b).find("Site[SITE_CD='1']").children("Page[MP_ID='MPI11']").children("Module[DTL_ID='2']").each(function(a) {        
		cnt++;
			var MPI11_DTL_TITLE = $(this).children("DTL_TITLE").text();
			var MPI11_DTL_RK = $(this).children("DTL_RK").text();
			var MPI11_DTL_IMG_LINK = $(this).children("DTL_IMG_LINK").text();
			
		  $("<div/>", {
				"id" : "module_content_row_page11",
				"class" : "row"
			}).appendTo("#module_content_page11");		
			
		  $("<section/>", {
				"id" : "module_1",
				"class" : "module_content"
			}).appendTo("#module_content_row_page11");		
			
					var lcnt = 0

					$($(this)).children("Items").each(function(a) {

					  var MPI11_LINK_NM = $(this).children("LINK_NM").text();
					  var MPI11_LINK_URL = $(this).children("LINK_URL").text();
					  var MPI11_IMG_DR = $(this).children("IMG_DR").text();
					  var MPI11_IMG_URL = $(this).children("IMG_URL").text();
					  var MPI11_LINK_NAMT = $(this).children("LINK_NAMT").text();
					lcnt++;
					  
						$("<div/>", {
							"id" : "module_div"+lcnt,
							"class" : "col-xs-12 col-sm-4 col-md-4 col-lg-4 tile module_three"
						}).appendTo("section#module_1");
						
							$("<a/>", {
								"id" : "module_div_a"+lcnt,
								"target" : "_blank",
								"href" : MPI11_LINK_URL
							}).appendTo("div#module_div"+lcnt);
							
								$("<img/>", {
									"id" : "module_div_a_img"+lcnt,
									"src" : MPI11_IMG_URL
								}).appendTo("a#module_div_a"+lcnt);

										
					});			
				
			})
		}		

	//主題頁-台中出發	
	if($("#myCarousel_12").length > 0 ){
        var cnt = 0; cntto = -1
        $("#myCarousel_12").html("");
                
				$("<ol/>", {
                    "class": "carousel-indicators"
				}).appendTo("#myCarousel_12");  

				$("<div/>", {
					"role": "listbox",
                    "class": "carousel-inner"
				}).appendTo("#myCarousel_12");	
                

				$("<a/>", {
                    "id": "left_btn",
                    "class": "left carousel-control",
					"href": "#myCarousel_12",
                    "role": "button",
                    "data-slide": "prev"
				}).appendTo("#myCarousel_12");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-left"
                    }).appendTo("#myCarousel_12 #left_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Previous"
                    }).appendTo("#myCarousel_12 #left_btn");

				$("<a/>", {
                    "id": "right_btn",
                    "class": "right carousel-control",
					"href": "#myCarousel_12",
                    "role": "button",
                    "data-slide": "next"
				}).appendTo("#myCarousel_12");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-right"
                    }).appendTo("#myCarousel_12 #right_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Next"
                    }).appendTo("#myCarousel_12 #right_btn");

	  
        $(b).find("Site[SITE_CD='1']").children("Page[MP_ID='MPI12']").children("Module[DTL_ID='1']").children("Items").each(function(a) {          
		  cnt++;
          cntto++;
                
		var MPI12_IMG_URL = $(this).children("IMG_URL").text();
        var MPI12_LINK_URL = $(this).children("LINK_URL").text();
        var MPI12_LINK_NM = $(this).children("LINK_NM").text();

		
				$("<li/>", {
                    "id": "slide_to_"+cnt,
                    "class": " ",
                    "data-target": "#myCarousel_12",
                    "data-slide-to": cntto,
					"html" : MPI12_LINK_NM
				}).appendTo("#myCarousel_12 .carousel-indicators");

				$("<div/>", {
					"id": "listbox_li_"+cnt,
                    "class": "item"
				}).appendTo("#myCarousel_12 .carousel-inner");
				
					$("<a/>", {
						"id": "listbox_li_a_"+cnt,
						"href" : MPI12_LINK_URL
					}).appendTo("#listbox_li_"+cnt);
                
						$("<div/>", {
							"id": "listbox_li_a_img"+cnt,
							"class": "img"
						}).appendTo("a#listbox_li_a_"+cnt);
												
							$("<img/>", {
								"src": MPI12_IMG_URL
							}).appendTo("#listbox_li_a_img"+cnt);

				$("<div/>", {
					"id": "listbox_li_a_div"+cnt,
                    "class" : "carousel-caption",
				}).appendTo("#listbox_li_"+cnt+" #listbox_li_a_"+cnt);
				
				
      });
	  
		//首頁-大圖輪播JS
		if (autoPlayFG){
			$("#listbox_li_1").addClass("active");
            $("#slide_to_1").addClass("active");
                $("#slide_to_1").click(function(){
                    $("#slide_to_1").addClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_2").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").addClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_3").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").addClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_4").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").addClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_5").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").addClass("active");
                });
                

		}
		
	  }
	  
	  	//小圖
		if($("#module_content_page12").length > 0 ){
		var cnt = 0
		$("#module_content_page12 #module_content_row_page12").html("")
		  
		$(b).find("Site[SITE_CD='1']").children("Page[MP_ID='MPI12']").children("Module[DTL_ID='2']").each(function(a) {        
		cnt++;
			var MPI12_DTL_TITLE = $(this).children("DTL_TITLE").text();
			var MPI12_DTL_RK = $(this).children("DTL_RK").text();
			var MPI12_DTL_IMG_LINK = $(this).children("DTL_IMG_LINK").text();
			
		  $("<div/>", {
				"id" : "module_content_row_page12",
				"class" : "row"
			}).appendTo("#module_content_page12");		
			
		  $("<section/>", {
				"id" : "module_12",
				"class" : "module_content"
			}).appendTo("#module_content_row_page12");		
			
					var lcnt = 0

					$($(this)).children("Items").each(function(a) {

					  var MPI12_LINK_NM = $(this).children("LINK_NM").text();
					  var MPI12_LINK_URL = $(this).children("LINK_URL").text();
					  var MPI12_IMG_DR = $(this).children("IMG_DR").text();
					  var MPI12_IMG_URL = $(this).children("IMG_URL").text();
					  var MPI12_LINK_NAMT = $(this).children("LINK_NAMT").text();
					lcnt++;
					  
						$("<div/>", {
							"id" : "module_div"+lcnt,
							"class" : "col-xs-12 col-sm-4 col-md-4 col-lg-4 tile module_three"
						}).appendTo("section#module_1");
						
							$("<a/>", {
								"id" : "module_div_a"+lcnt,
								"target" : "_blank",
								"href" : MPI12_LINK_URL
							}).appendTo("div#module_div"+lcnt);
							
								$("<img/>", {
									"id" : "module_div_a_img"+lcnt,
									"src" : MPI12_IMG_URL
								}).appendTo("a#module_div_a"+lcnt);

										
					});			
				
			})
		}		
		
	//主題頁-線上旅展	
	if($("#myCarousel_13").length > 0 ){
        var cnt = 0; cntto = -1
        $("#myCarousel_13").html("");
                
				$("<ol/>", {
                    "class": "carousel-indicators"
				}).appendTo("#myCarousel_13");  

				$("<div/>", {
					"role": "listbox",
                    "class": "carousel-inner"
				}).appendTo("#myCarousel_13");	
                

				$("<a/>", {
                    "id": "left_btn",
                    "class": "left carousel-control",
					"href": "#myCarousel_13",
                    "role": "button",
                    "data-slide": "prev"
				}).appendTo("#myCarousel_13");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-left"
                    }).appendTo("#myCarousel_13 #left_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Previous"
                    }).appendTo("#myCarousel_13 #left_btn");

				$("<a/>", {
                    "id": "right_btn",
                    "class": "right carousel-control",
					"href": "#myCarousel_13",
                    "role": "button",
                    "data-slide": "next"
				}).appendTo("#myCarousel_13");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-right"
                    }).appendTo("#myCarousel_13 #right_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Next"
                    }).appendTo("#myCarousel_13 #right_btn");

	  
        $(b).find("Site[SITE_CD='1']").children("Page[MP_ID='MPi13']").children("Module[DTL_ID='1']").children("Items").each(function(a) {          
		  cnt++;
          cntto++;
                
		var MPi13_IMG_URL = $(this).children("IMG_URL").text();
        var MPi13_LINK_URL = $(this).children("LINK_URL").text();
        var MPi13_LINK_NM = $(this).children("LINK_NM").text();

		
				$("<li/>", {
                    "id": "slide_to_"+cnt,
                    "class": " ",
                    "data-target": "#myCarousel_13",
                    "data-slide-to": cntto,
					"html" : MPi13_LINK_NM
				}).appendTo("#myCarousel_13 .carousel-indicators");

				$("<div/>", {
					"id": "listbox_li_"+cnt,
                    "class": "item"
				}).appendTo("#myCarousel_13 .carousel-inner");
				
					$("<a/>", {
						"id": "listbox_li_a_"+cnt,
						"href" : MPi13_LINK_URL
					}).appendTo("#listbox_li_"+cnt);
                
						$("<div/>", {
							"id": "listbox_li_a_img"+cnt,
							"class": "img"
						}).appendTo("a#listbox_li_a_"+cnt);
												
							$("<img/>", {
								"src": MPi13_IMG_URL
							}).appendTo("#listbox_li_a_img"+cnt);

				$("<div/>", {
					"id": "listbox_li_a_div"+cnt,
                    "class" : "carousel-caption",
				}).appendTo("#listbox_li_"+cnt+" #listbox_li_a_"+cnt);
				
				
      });
	  
		//首頁-大圖輪播JS
		if (autoPlayFG){
			$("#listbox_li_1").addClass("active");
            $("#slide_to_1").addClass("active");
                $("#slide_to_1").click(function(){
                    $("#slide_to_1").addClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_2").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").addClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_3").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").addClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_4").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").addClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_5").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").addClass("active");
                });
                

		}
		
	  }
	  
	  	//小圖
		if($("#module_content_page13").length > 0 ){
		var cnt = 0
		$("#module_content_page13 #module_content_row_page13").html("")
		  
		$(b).find("Site[SITE_CD='1']").children("Page[MP_ID='MPi13']").children("Module[DTL_ID='2']").each(function(a) {        
		cnt++;
			var MPi13_DTL_TITLE = $(this).children("DTL_TITLE").text();
			var MPi13_DTL_RK = $(this).children("DTL_RK").text();
			var MPi13_DTL_IMG_LINK = $(this).children("DTL_IMG_LINK").text();
			
		  $("<div/>", {
				"id" : "module_content_row_page13",
				"class" : "row"
			}).appendTo("#module_content_page13");		
			
		  $("<section/>", {
				"id" : "module_13",
				"class" : "module_content"
			}).appendTo("#module_content_row_page13");		
			
					var lcnt = 0

					$($(this)).children("Items").each(function(a) {

					  var MPi13_LINK_NM = $(this).children("LINK_NM").text();
					  var MPi13_LINK_URL = $(this).children("LINK_URL").text();
					  var MPi13_IMG_DR = $(this).children("IMG_DR").text();
					  var MPi13_IMG_URL = $(this).children("IMG_URL").text();
					  var MPi13_LINK_NAMT = $(this).children("LINK_NAMT").text();
					lcnt++;
					  
						$("<div/>", {
							"id" : "module_div"+lcnt,
							"class" : "col-xs-12 col-sm-4 col-md-4 col-lg-4 tile module_three"
						}).appendTo("section#module_13");
						
							$("<a/>", {
								"id" : "module_div_a"+lcnt,
								"target" : "_blank",
								"href" : MPi13_LINK_URL
							}).appendTo("div#module_div"+lcnt);
							
								$("<img/>", {
									"id" : "module_div_a_img"+lcnt,
									"src" : MPi13_IMG_URL
								}).appendTo("a#module_div_a"+lcnt);

										
					});			
				
			})
		}		
		
	//主題頁-關於超悠旅遊	
	if($("#myCarousel_14").length > 0 ){
        var cnt = 0; cntto = -1
        $("#myCarousel_14").html("");
                
				$("<ol/>", {
                    "class": "carousel-indicators"
				}).appendTo("#myCarousel_14");  

				$("<div/>", {
					"role": "listbox",
                    "class": "carousel-inner"
				}).appendTo("#myCarousel_14");	
                

				$("<a/>", {
                    "id": "left_btn",
                    "class": "left carousel-control",
					"href": "#myCarousel_14",
                    "role": "button",
                    "data-slide": "prev"
				}).appendTo("#myCarousel_14");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-left"
                    }).appendTo("#myCarousel_14 #left_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Previous"
                    }).appendTo("#myCarousel_14 #left_btn");

				$("<a/>", {
                    "id": "right_btn",
                    "class": "right carousel-control",
					"href": "#myCarousel_14",
                    "role": "button",
                    "data-slide": "next"
				}).appendTo("#myCarousel_14");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-right"
                    }).appendTo("#myCarousel_14 #right_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Next"
                    }).appendTo("#myCarousel_14 #right_btn");

	  
        $(b).find("Site[SITE_CD='1']").children("Page[MP_ID='MPI14']").children("Module[DTL_ID='1']").children("Items").each(function(a) {          
		  cnt++;
          cntto++;
                
		var MPI14_IMG_URL = $(this).children("IMG_URL").text();
        var MPI14_LINK_URL = $(this).children("LINK_URL").text();
        var MPI14_LINK_NM = $(this).children("LINK_NM").text();

		
				$("<li/>", {
                    "id": "slide_to_"+cnt,
                    "class": " ",
                    "data-target": "#myCarousel_14",
                    "data-slide-to": cntto,
					"html" : MPI14_LINK_NM
				}).appendTo("#myCarousel_14 .carousel-indicators");

				$("<div/>", {
					"id": "listbox_li_"+cnt,
                    "class": "item"
				}).appendTo("#myCarousel_14 .carousel-inner");
				
					$("<a/>", {
						"id": "listbox_li_a_"+cnt,
						"href" : MPI14_LINK_URL
					}).appendTo("#listbox_li_"+cnt);
                
						$("<div/>", {
							"id": "listbox_li_a_img"+cnt,
							"class": "img"
						}).appendTo("a#listbox_li_a_"+cnt);
												
							$("<img/>", {
								"src": MPI14_IMG_URL
							}).appendTo("#listbox_li_a_img"+cnt);

				$("<div/>", {
					"id": "listbox_li_a_div"+cnt,
                    "class" : "carousel-caption",
				}).appendTo("#listbox_li_"+cnt+" #listbox_li_a_"+cnt);
				
				
      });
	  
		//首頁-大圖輪播JS
		if (autoPlayFG){
			$("#listbox_li_1").addClass("active");
            $("#slide_to_1").addClass("active");
                $("#slide_to_1").click(function(){
                    $("#slide_to_1").addClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_2").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").addClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_3").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").addClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_4").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").addClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_5").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").addClass("active");
                });
                

		}
		
	  }
	  
	  	//小圖
		if($("#module_content_14").length > 0 ){
		var cnt = 0
		$("#module_content_14 #module_content_row_14").html("")
		  
		$(b).find("Site[SITE_CD='1']").children("Page[MP_ID='MPI14']").children("Module[DTL_ID='2']").each(function(a) {        
		cnt++;
			var MPI14_DTL_TITLE = $(this).children("DTL_TITLE").text();
			var MPI14_DTL_RK = $(this).children("DTL_RK").text();
			var MPI14_DTL_IMG_LINK = $(this).children("DTL_IMG_LINK").text();
			
		  $("<div/>", {
				"id" : "module_content_row_14",
				"class" : "row"
			}).appendTo("#module_content_14");		
			
		  $("<section/>", {
				"id" : "module_14",
				"class" : "module_content"
			}).appendTo("#module_content_row_14");		
			
					var lcnt = 0

					$($(this)).children("Items").each(function(a) {

					  var MPI14_LINK_NM = $(this).children("LINK_NM").text();
					  var MPI14_LINK_URL = $(this).children("LINK_URL").text();
					  var MPI14_IMG_DR = $(this).children("IMG_DR").text();
					  var MPI14_IMG_URL = $(this).children("IMG_URL").text();
					  var MPI14_LINK_NAMT = $(this).children("LINK_NAMT").text();
					lcnt++;
					  
						$("<div/>", {
							"id" : "module_div"+lcnt,
							"class" : "col-xs-4 col-sm-3 col-md-3 col-lg-2 tile module_three"
						}).appendTo("section#module_14");
						
							$("<a/>", {
								"id" : "module_div_a"+lcnt,
								"target" : "_blank",
								"href" : MPI14_LINK_URL
							}).appendTo("div#module_div"+lcnt);
							
								$("<img/>", {
									"id" : "module_div_a_img"+lcnt,
									"src" : MPI14_IMG_URL
								}).appendTo("a#module_div_a"+lcnt);

										
					});			
				
			})
		}
		
	//主題頁-過年限時優惠	
	if($("#myCarousel_15").length > 0 ){
        var cnt = 0; cntto = -1
        $("#myCarousel_15").html("");
                
				$("<ol/>", {
                    "class": "carousel-indicators"
				}).appendTo("#myCarousel_15");  

				$("<div/>", {
					"role": "listbox",
                    "class": "carousel-inner"
				}).appendTo("#myCarousel_15");	
                

				$("<a/>", {
                    "id": "left_btn",
                    "class": "left carousel-control",
					"href": "#myCarousel_15",
                    "role": "button",
                    "data-slide": "prev"
				}).appendTo("#myCarousel_15");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-left"
                    }).appendTo("#myCarousel_15 #left_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Previous"
                    }).appendTo("#myCarousel_15 #left_btn");

				$("<a/>", {
                    "id": "right_btn",
                    "class": "right carousel-control",
					"href": "#myCarousel_15",
                    "role": "button",
                    "data-slide": "next"
				}).appendTo("#myCarousel_15");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-right"
                    }).appendTo("#myCarousel_15 #right_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Next"
                    }).appendTo("#myCarousel_15 #right_btn");

	  
        $(b).find("Site[SITE_CD='1']").children("Page[MP_ID='MPI15']").children("Module[DTL_ID='1']").children("Items").each(function(a) {          
		  cnt++;
          cntto++;
                
		var MPI15_IMG_URL = $(this).children("IMG_URL").text();
        var MPI15_LINK_URL = $(this).children("LINK_URL").text();
        var MPI15_LINK_NM = $(this).children("LINK_NM").text();

		
				$("<li/>", {
                    "id": "slide_to_"+cnt,
                    "class": " ",
                    "data-target": "#myCarousel_15",
                    "data-slide-to": cntto,
					"html" : MPI15_LINK_NM
				}).appendTo("#myCarousel_15 .carousel-indicators");

				$("<div/>", {
					"id": "listbox_li_"+cnt,
                    "class": "item"
				}).appendTo("#myCarousel_15 .carousel-inner");
				
					$("<a/>", {
						"id": "listbox_li_a_"+cnt,
						"href" : MPI15_LINK_URL
					}).appendTo("#listbox_li_"+cnt);
                
						$("<div/>", {
							"id": "listbox_li_a_img"+cnt,
							"class": "img"
						}).appendTo("a#listbox_li_a_"+cnt);
												
							$("<img/>", {
								"src": MPI15_IMG_URL
							}).appendTo("#listbox_li_a_img"+cnt);

				$("<div/>", {
					"id": "listbox_li_a_div"+cnt,
                    "class" : "carousel-caption",
				}).appendTo("#listbox_li_"+cnt+" #listbox_li_a_"+cnt);
				
				
      });
	  
		//首頁-大圖輪播JS
		if (autoPlayFG){
			$("#listbox_li_1").addClass("active");
            $("#slide_to_1").addClass("active");
                $("#slide_to_1").click(function(){
                    $("#slide_to_1").addClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_2").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").addClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_3").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").addClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_4").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").addClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_5").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").addClass("active");
                });
                

		}
		
	  }
	  
	//主題頁-季節限定限時優惠	
	if($("#myCarousel_16").length > 0 ){
        var cnt = 0; cntto = -1
        $("#myCarousel_16").html("");
                
				$("<ol/>", {
                    "class": "carousel-indicators"
				}).appendTo("#myCarousel_16");  

				$("<div/>", {
					"role": "listbox",
                    "class": "carousel-inner"
				}).appendTo("#myCarousel_16");	
                

				$("<a/>", {
                    "id": "left_btn",
                    "class": "left carousel-control",
					"href": "#myCarousel_16",
                    "role": "button",
                    "data-slide": "prev"
				}).appendTo("#myCarousel_16");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-left"
                    }).appendTo("#myCarousel_16 #left_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Previous"
                    }).appendTo("#myCarousel_16 #left_btn");

				$("<a/>", {
                    "id": "right_btn",
                    "class": "right carousel-control",
					"href": "#myCarousel_16",
                    "role": "button",
                    "data-slide": "next"
				}).appendTo("#myCarousel_16");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-right"
                    }).appendTo("#myCarousel_16 #right_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Next"
                    }).appendTo("#myCarousel_16 #right_btn");

	  
        $(b).find("Site[SITE_CD='1']").children("Page[MP_ID='MPI16']").children("Module[DTL_ID='1']").children("Items").each(function(a) {          
		  cnt++;
          cntto++;
                
		var MPI16_IMG_URL = $(this).children("IMG_URL").text();
        var MPI16_LINK_URL = $(this).children("LINK_URL").text();
        var MPI16_LINK_NM = $(this).children("LINK_NM").text();

		
				$("<li/>", {
                    "id": "slide_to_"+cnt,
                    "class": " ",
                    "data-target": "#myCarousel_16",
                    "data-slide-to": cntto,
					"html" : MPI16_LINK_NM
				}).appendTo("#myCarousel_16 .carousel-indicators");

				$("<div/>", {
					"id": "listbox_li_"+cnt,
                    "class": "item"
				}).appendTo("#myCarousel_16 .carousel-inner");
				
					$("<a/>", {
						"id": "listbox_li_a_"+cnt,
						"href" : MPI16_LINK_URL
					}).appendTo("#listbox_li_"+cnt);
                
						$("<div/>", {
							"id": "listbox_li_a_img"+cnt,
							"class": "img"
						}).appendTo("a#listbox_li_a_"+cnt);
												
							$("<img/>", {
								"src": MPI16_IMG_URL
							}).appendTo("#listbox_li_a_img"+cnt);

				$("<div/>", {
					"id": "listbox_li_a_div"+cnt,
                    "class" : "carousel-caption",
				}).appendTo("#listbox_li_"+cnt+" #listbox_li_a_"+cnt);
				
				
      });
	  
		//首頁-大圖輪播JS
		if (autoPlayFG){
			$("#listbox_li_1").addClass("active");
            $("#slide_to_1").addClass("active");
                $("#slide_to_1").click(function(){
                    $("#slide_to_1").addClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_2").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").addClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_3").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").addClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_4").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").addClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_5").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").addClass("active");
                });
                

		}
		
	  }
	  
	//主題頁-航空資訊
	if($("#myCarousel_17").length > 0 ){
        var cnt = 0; cntto = -1
        $("#myCarousel_17").html("");
                
				$("<ol/>", {
                    "class": "carousel-indicators"
				}).appendTo("#myCarousel_17");  

				$("<div/>", {
					"role": "listbox",
                    "class": "carousel-inner"
				}).appendTo("#myCarousel_17");	
                

				$("<a/>", {
                    "id": "left_btn",
                    "class": "left carousel-control",
					"href": "#myCarousel_17",
                    "role": "button",
                    "data-slide": "prev"
				}).appendTo("#myCarousel_17");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-left"
                    }).appendTo("#myCarousel_17 #left_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Previous"
                    }).appendTo("#myCarousel_17 #left_btn");

				$("<a/>", {
                    "id": "right_btn",
                    "class": "right carousel-control",
					"href": "#myCarousel_17",
                    "role": "button",
                    "data-slide": "next"
				}).appendTo("#myCarousel_17");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-right"
                    }).appendTo("#myCarousel_17 #right_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Next"
                    }).appendTo("#myCarousel_17 #right_btn");

	  
        $(b).find("Site[SITE_CD='1']").children("Page[MP_ID='MPI17']").children("Module[DTL_ID='1']").children("Items").each(function(a) {          
		  cnt++;
          cntto++;
                
		var MPI17_IMG_URL = $(this).children("IMG_URL").text();
        var MPI17_LINK_URL = $(this).children("LINK_URL").text();
        var MPI17_LINK_NM = $(this).children("LINK_NM").text();

		
				$("<li/>", {
                    "id": "slide_to_"+cnt,
                    "class": " ",
                    "data-target": "#myCarousel_17",
                    "data-slide-to": cntto,
					"html" : MPI17_LINK_NM
				}).appendTo("#myCarousel_17 .carousel-indicators");

				$("<div/>", {
					"id": "listbox_li_"+cnt,
                    "class": "item"
				}).appendTo("#myCarousel_17 .carousel-inner");
				
					$("<a/>", {
						"id": "listbox_li_a_"+cnt,
						"href" : MPI17_LINK_URL
					}).appendTo("#listbox_li_"+cnt);
                
						$("<div/>", {
							"id": "listbox_li_a_img"+cnt,
							"class": "img"
						}).appendTo("a#listbox_li_a_"+cnt);
												
							$("<img/>", {
								"src": MPI17_IMG_URL
							}).appendTo("#listbox_li_a_img"+cnt);

				$("<div/>", {
					"id": "listbox_li_a_div"+cnt,
                    "class" : "carousel-caption",
				}).appendTo("#listbox_li_"+cnt+" #listbox_li_a_"+cnt);
				
				
      });
	  
		//首頁-大圖輪播JS
		if (autoPlayFG){
			$("#listbox_li_1").addClass("active");
            $("#slide_to_1").addClass("active");
                $("#slide_to_1").click(function(){
                    $("#slide_to_1").addClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_2").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").addClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_3").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").addClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_4").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").addClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_5").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").addClass("active");
                });
                

		}
		
	  }
	  
	//主題頁-簽證資訊
	if($("#myCarousel_18").length > 0 ){
        var cnt = 0; cntto = -1
        $("#myCarousel_18").html("");
                
				$("<ol/>", {
                    "class": "carousel-indicators"
				}).appendTo("#myCarousel_18");  

				$("<div/>", {
					"role": "listbox",
                    "class": "carousel-inner"
				}).appendTo("#myCarousel_18");	
                

				$("<a/>", {
                    "id": "left_btn",
                    "class": "left carousel-control",
					"href": "#myCarousel_18",
                    "role": "button",
                    "data-slide": "prev"
				}).appendTo("#myCarousel_18");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-left"
                    }).appendTo("#myCarousel_18 #left_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Previous"
                    }).appendTo("#myCarousel_18 #left_btn");

				$("<a/>", {
                    "id": "right_btn",
                    "class": "right carousel-control",
					"href": "#myCarousel_18",
                    "role": "button",
                    "data-slide": "next"
				}).appendTo("#myCarousel_18");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-right"
                    }).appendTo("#myCarousel_18 #right_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Next"
                    }).appendTo("#myCarousel_18 #right_btn");

	  
        $(b).find("Site[SITE_CD='1']").children("Page[MP_ID='MPI18']").children("Module[DTL_ID='1']").children("Items").each(function(a) {          
		  cnt++;
          cntto++;
                
		var MPI18_IMG_URL = $(this).children("IMG_URL").text();
        var MPI18_LINK_URL = $(this).children("LINK_URL").text();
        var MPI18_LINK_NM = $(this).children("LINK_NM").text();

		
				$("<li/>", {
                    "id": "slide_to_"+cnt,
                    "class": " ",
                    "data-target": "#myCarousel_18",
                    "data-slide-to": cntto,
					"html" : MPI18_LINK_NM
				}).appendTo("#myCarousel_18 .carousel-indicators");

				$("<div/>", {
					"id": "listbox_li_"+cnt,
                    "class": "item"
				}).appendTo("#myCarousel_18 .carousel-inner");
				
					$("<a/>", {
						"id": "listbox_li_a_"+cnt,
						"href" : MPI18_LINK_URL
					}).appendTo("#listbox_li_"+cnt);
                
						$("<div/>", {
							"id": "listbox_li_a_img"+cnt,
							"class": "img"
						}).appendTo("a#listbox_li_a_"+cnt);
												
							$("<img/>", {
								"src": MPI18_IMG_URL
							}).appendTo("#listbox_li_a_img"+cnt);

				$("<div/>", {
					"id": "listbox_li_a_div"+cnt,
                    "class" : "carousel-caption",
				}).appendTo("#listbox_li_"+cnt+" #listbox_li_a_"+cnt);
				
				
      });
	  
		//首頁-大圖輪播JS
		if (autoPlayFG){
			$("#listbox_li_1").addClass("active");
            $("#slide_to_1").addClass("active");
                $("#slide_to_1").click(function(){
                    $("#slide_to_1").addClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_2").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").addClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_3").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").addClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_4").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").addClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_5").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").addClass("active");
                });
                

		}
		
	  }
	
	//主題頁-蒙古
    if($("#myCarousel_19").length > 0 ){
        var cnt = 0; cntto = -1
        $("#myCarousel_19").html("");
                
				$("<ol/>", {
                    "class": "carousel-indicators"
				}).appendTo("#myCarousel_19");  

				$("<div/>", {
					"role": "listbox",
                    "class": "carousel-inner"
				}).appendTo("#myCarousel_19");	
                

				$("<a/>", {
                    "id": "left_btn",
                    "class": "left carousel-control",
					"href": "#myCarousel_19",
                    "role": "button",
                    "data-slide": "prev"
				}).appendTo("#myCarousel_19");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-left"
                    }).appendTo("#myCarousel_19 #left_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Previous"
                    }).appendTo("#myCarousel_19 #left_btn");

				$("<a/>", {
                    "id": "right_btn",
                    "class": "right carousel-control",
					"href": "#myCarousel_19",
                    "role": "button",
                    "data-slide": "next"
				}).appendTo("#myCarousel_19");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-right"
                    }).appendTo("#myCarousel_19 #right_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Next"
                    }).appendTo("#myCarousel_19 #right_btn");

	  
        $(b).find("Site[SITE_CD='1']").children("Page[MP_ID='MPI19']").children("Module[DTL_ID='1']").children("Items").each(function(a) {          
		  cnt++;
          cntto++;
                
		var MPI19_IMG_URL = $(this).children("IMG_URL").text();
        var MPI19_LINK_URL = $(this).children("LINK_URL").text();
        var MPI19_LINK_NM = $(this).children("LINK_NM").text();

		
				$("<li/>", {
                    "id": "slide_to_"+cnt,
                    "class": " ",
                    "data-target": "#myCarousel_19",
                    "data-slide-to": cntto,
					"html" : MPI19_LINK_NM
				}).appendTo("#myCarousel_19 .carousel-indicators");

				$("<div/>", {
					"id": "listbox_li_"+cnt,
                    "class": "item"
				}).appendTo("#myCarousel_19 .carousel-inner");
				
					$("<a/>", {
						"id": "listbox_li_a_"+cnt,
						"href" : MPI19_LINK_URL
					}).appendTo("#listbox_li_"+cnt);
                
						$("<div/>", {
							"id": "listbox_li_a_img"+cnt,
							"class": "img"
						}).appendTo("a#listbox_li_a_"+cnt);
												
							$("<img/>", {
								"src": MPI19_IMG_URL
							}).appendTo("#listbox_li_a_img"+cnt);

				$("<div/>", {
					"id": "listbox_li_a_div"+cnt,
                    "class" : "carousel-caption",
				}).appendTo("#listbox_li_"+cnt+" #listbox_li_a_"+cnt);
				
				
      });
	  
		//首頁-大圖輪播JS
		if (autoPlayFG){
			$("#listbox_li_1").addClass("active");
            $("#slide_to_1").addClass("active");
                $("#slide_to_1").click(function(){
                    $("#slide_to_1").addClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_2").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").addClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_3").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").addClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_4").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").addClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_5").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").addClass("active");
                });
                

		}
		//小圖
		if($("#module_content_page19").length > 0 ){
		var cnt = 0
		$("#module_content_page19 #module_content_row_page19").html("")
		  
		$(b).find("Site[SITE_CD='1']").children("Page[MP_ID='MPI19']").children("Module[DTL_ID='2']").each(function(a) {        
		cnt++;
			var MPI19_DTL_TITLE = $(this).children("DTL_TITLE").text();
			var MPI19_DTL_RK = $(this).children("DTL_RK").text();
			var MPI19_DTL_IMG_LINK = $(this).children("DTL_IMG_LINK").text();
			
		  $("<div/>", {
				"id" : "module_content_row_page19",
				"class" : "row"
			}).appendTo("#module_content_page19");		
			
		  $("<section/>", {
				"id" : "module_12",
				"class" : "module_content"
			}).appendTo("#module_content_row_page19");		
			
					var lcnt = 0

					$($(this)).children("Items").each(function(a) {

					  var MPI19_LINK_NM = $(this).children("LINK_NM").text();
					  var MPI19_LINK_URL = $(this).children("LINK_URL").text();
					  var MPI19_IMG_DR = $(this).children("IMG_DR").text();
					  var MPI19_IMG_URL = $(this).children("IMG_URL").text();
					  var MPI19_LINK_NAMT = $(this).children("LINK_NAMT").text();
					lcnt++;
					  
						$("<div/>", {
							"id" : "module_div"+lcnt,
							"class" : "col-xs-12 col-sm-4 col-md-4 col-lg-4 tile module_three"
						}).appendTo("section#module_1");
						
							$("<a/>", {
								"id" : "module_div_a"+lcnt,
								"target" : "_blank",
								"href" : MPI19_LINK_URL
							}).appendTo("div#module_div"+lcnt);
							
								$("<img/>", {
									"id" : "module_div_a_img"+lcnt,
									"src" : MPI19_IMG_URL
								}).appendTo("a#module_div_a"+lcnt);

										
					});			
				
			})
		}
	  }
		
	//主題頁-歐來歐去專案
	if($("#myCarousel_20").length > 0 ){
        var cnt = 0; cntto = -1
        $("#myCarousel_20").html("");
                
				$("<ol/>", {
                    "class": "carousel-indicators"
				}).appendTo("#myCarousel_20");  

				$("<div/>", {
					"role": "listbox",
                    "class": "carousel-inner"
				}).appendTo("#myCarousel_20");	
                

				$("<a/>", {
                    "id": "left_btn",
                    "class": "left carousel-control",
					"href": "#myCarousel_20",
                    "role": "button",
                    "data-slide": "prev"
				}).appendTo("#myCarousel_20");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-left"
                    }).appendTo("#myCarousel_20 #left_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Previous"
                    }).appendTo("#myCarousel_20 #left_btn");

				$("<a/>", {
                    "id": "right_btn",
                    "class": "right carousel-control",
					"href": "#myCarousel_20",
                    "role": "button",
                    "data-slide": "next"
				}).appendTo("#myCarousel_20");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-right"
                    }).appendTo("#myCarousel_20 #right_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Next"
                    }).appendTo("#myCarousel_20 #right_btn");

	  
        $(b).find("Site[SITE_CD='1']").children("Page[MP_ID='MPI20']").children("Module[DTL_ID='1']").children("Items").each(function(a) {          
		  cnt++;
          cntto++;
                
		var MPI20_IMG_URL = $(this).children("IMG_URL").text();
        var MPI20_LINK_URL = $(this).children("LINK_URL").text();
        var MPI20_LINK_NM = $(this).children("LINK_NM").text();

		
				$("<li/>", {
                    "id": "slide_to_"+cnt,
                    "class": " ",
                    "data-target": "#myCarousel_20",
                    "data-slide-to": cntto,
					"html" : MPI20_LINK_NM
				}).appendTo("#myCarousel_20 .carousel-indicators");

				$("<div/>", {
					"id": "listbox_li_"+cnt,
                    "class": "item"
				}).appendTo("#myCarousel_20 .carousel-inner");
				
					$("<a/>", {
						"id": "listbox_li_a_"+cnt,
						"href" : MPI20_LINK_URL
					}).appendTo("#listbox_li_"+cnt);
                
						$("<div/>", {
							"id": "listbox_li_a_img"+cnt,
							"class": "img"
						}).appendTo("a#listbox_li_a_"+cnt);
												
							$("<img/>", {
								"src": MPI20_IMG_URL
							}).appendTo("#listbox_li_a_img"+cnt);

				$("<div/>", {
					"id": "listbox_li_a_div"+cnt,
                    "class" : "carousel-caption",
				}).appendTo("#listbox_li_"+cnt+" #listbox_li_a_"+cnt);
				
				
      });
	  
		//首頁-大圖輪播JS
		if (autoPlayFG){
			$("#listbox_li_1").addClass("active");
            $("#slide_to_1").addClass("active");
                $("#slide_to_1").click(function(){
                    $("#slide_to_1").addClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_2").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").addClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_3").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").addClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_4").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").addClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_5").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").addClass("active");
                });
                

		}
		//小圖
		if($("#module_content_page19").length > 0 ){
		var cnt = 0
		$("#module_content_page19 #module_content_row_page19").html("")
		  
		$(b).find("Site[SITE_CD='1']").children("Page[MP_ID='MPI20']").children("Module[DTL_ID='2']").each(function(a) {        
		cnt++;
			var MPI20_DTL_TITLE = $(this).children("DTL_TITLE").text();
			var MPI20_DTL_RK = $(this).children("DTL_RK").text();
			var MPI20_DTL_IMG_LINK = $(this).children("DTL_IMG_LINK").text();
			
		  $("<div/>", {
				"id" : "module_content_row_page19",
				"class" : "row"
			}).appendTo("#module_content_page19");		
			
		  $("<section/>", {
				"id" : "module_12",
				"class" : "module_content"
			}).appendTo("#module_content_row_page19");		
			
					var lcnt = 0

					$($(this)).children("Items").each(function(a) {

					  var MPI20_LINK_NM = $(this).children("LINK_NM").text();
					  var MPI20_LINK_URL = $(this).children("LINK_URL").text();
					  var MPI20_IMG_DR = $(this).children("IMG_DR").text();
					  var MPI20_IMG_URL = $(this).children("IMG_URL").text();
					  var MPI20_LINK_NAMT = $(this).children("LINK_NAMT").text();
					lcnt++;
					  
						$("<div/>", {
							"id" : "module_div"+lcnt,
							"class" : "col-xs-12 col-sm-4 col-md-4 col-lg-4 tile module_three"
						}).appendTo("section#module_1");
						
							$("<a/>", {
								"id" : "module_div_a"+lcnt,
								"target" : "_blank",
								"href" : MPI20_LINK_URL
							}).appendTo("div#module_div"+lcnt);
							
								$("<img/>", {
									"id" : "module_div_a_img"+lcnt,
									"src" : MPI20_IMG_URL
								}).appendTo("a#module_div_a"+lcnt);
 
										
					});			
				
			})
		}
	  }	
	  
	//主題頁-賞花季節
	if($("#myCarousel_21").length > 0 ){
        var cnt = 0; cntto = -1
        $("#myCarousel_21").html("");
                
				$("<ol/>", {
                    "class": "carousel-indicators"
				}).appendTo("#myCarousel_21");  

				$("<div/>", {
					"role": "listbox",
                    "class": "carousel-inner"
				}).appendTo("#myCarousel_21");	
                

				$("<a/>", {
                    "id": "left_btn",
                    "class": "left carousel-control",
					"href": "#myCarousel_21",
                    "role": "button",
                    "data-slide": "prev"
				}).appendTo("#myCarousel_21");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-left"
                    }).appendTo("#myCarousel_21 #left_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Previous"
                    }).appendTo("#myCarousel_21 #left_btn");

				$("<a/>", {
                    "id": "right_btn",
                    "class": "right carousel-control",
					"href": "#myCarousel_21",
                    "role": "button",
                    "data-slide": "next"
				}).appendTo("#myCarousel_21");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-right"
                    }).appendTo("#myCarousel_21 #right_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Next"
                    }).appendTo("#myCarousel_21 #right_btn");

	  
        $(b).find("Site[SITE_CD='1']").children("Page[MP_ID='MPU01']").children("Module[DTL_ID='1']").children("Items").each(function(a) {          
		  cnt++;
          cntto++;
                
		var MPU01_IMG_URL = $(this).children("IMG_URL").text();
        var MPU01_LINK_URL = $(this).children("LINK_URL").text();
        var MPU01_LINK_NM = $(this).children("LINK_NM").text();

		
				$("<li/>", {
                    "id": "slide_to_"+cnt,
                    "class": " ",
                    "data-target": "#myCarousel_21",
                    "data-slide-to": cntto,
					"html" : MPU01_LINK_NM
				}).appendTo("#myCarousel_21 .carousel-indicators");

				$("<div/>", {
					"id": "listbox_li_"+cnt,
                    "class": "item"
				}).appendTo("#myCarousel_21 .carousel-inner");
				
					$("<a/>", {
						"id": "listbox_li_a_"+cnt,
						"href" : MPU01_LINK_URL
					}).appendTo("#listbox_li_"+cnt);
                
						$("<div/>", {
							"id": "listbox_li_a_img"+cnt,
							"class": "img"
						}).appendTo("a#listbox_li_a_"+cnt);
												
							$("<img/>", {
								"src": MPU01_IMG_URL
							}).appendTo("#listbox_li_a_img"+cnt);

				$("<div/>", {
					"id": "listbox_li_a_div"+cnt,
                    "class" : "carousel-caption",
				}).appendTo("#listbox_li_"+cnt+" #listbox_li_a_"+cnt);
				
				
      });
	  
		//首頁-大圖輪播JS
		if (autoPlayFG){
			$("#listbox_li_1").addClass("active");
            $("#slide_to_1").addClass("active");
                $("#slide_to_1").click(function(){
                    $("#slide_to_1").addClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_2").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").addClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_3").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").addClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_4").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").addClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_5").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").addClass("active");
                });
                

		}
	  }  
	
	//主題頁-端午優惠期間
	if($("#myCarousel_22").length > 0 ){
        var cnt = 0; cntto = -1
        $("#myCarousel_22").html("");
                
				$("<ol/>", {
                    "class": "carousel-indicators"
				}).appendTo("#myCarousel_22");  

				$("<div/>", {
					"role": "listbox",
                    "class": "carousel-inner"
				}).appendTo("#myCarousel_22");	
                

				$("<a/>", {
                    "id": "left_btn",
                    "class": "left carousel-control",
					"href": "#myCarousel_22",
                    "role": "button",
                    "data-slide": "prev"
				}).appendTo("#myCarousel_22");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-left"
                    }).appendTo("#myCarousel_22 #left_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Previous"
                    }).appendTo("#myCarousel_22 #left_btn");

				$("<a/>", {
                    "id": "right_btn",
                    "class": "right carousel-control",
					"href": "#myCarousel_22",
                    "role": "button",
                    "data-slide": "next"
				}).appendTo("#myCarousel_22");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-right"
                    }).appendTo("#myCarousel_22 #right_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Next"
                    }).appendTo("#myCarousel_22 #right_btn");

	  
        $(b).find("Site[SITE_CD='1']").children("Page[MP_ID='MPU02']").children("Module[DTL_ID='1']").children("Items").each(function(a) {          
		  cnt++;
          cntto++;
                
		var MPU02_IMG_URL = $(this).children("IMG_URL").text();
        var MPU02_LINK_URL = $(this).children("LINK_URL").text();
        var MPU02_LINK_NM = $(this).children("LINK_NM").text();

		
				$("<li/>", {
                    "id": "slide_to_"+cnt,
                    "class": " ",
                    "data-target": "#myCarousel_22",
                    "data-slide-to": cntto,
					"html" : MPU02_LINK_NM
				}).appendTo("#myCarousel_22 .carousel-indicators");

				$("<div/>", {
					"id": "listbox_li_"+cnt,
                    "class": "item"
				}).appendTo("#myCarousel_22 .carousel-inner");
				
					$("<a/>", {
						"id": "listbox_li_a_"+cnt,
						"href" : MPU02_LINK_URL
					}).appendTo("#listbox_li_"+cnt);
                
						$("<div/>", {
							"id": "listbox_li_a_img"+cnt,
							"class": "img"
						}).appendTo("a#listbox_li_a_"+cnt);
												
							$("<img/>", {
								"src": MPU02_IMG_URL
							}).appendTo("#listbox_li_a_img"+cnt);

				$("<div/>", {
					"id": "listbox_li_a_div"+cnt,
                    "class" : "carousel-caption",
				}).appendTo("#listbox_li_"+cnt+" #listbox_li_a_"+cnt);
				
				
      });
	  
		//首頁-大圖輪播JS
		if (autoPlayFG){
			$("#listbox_li_1").addClass("active");
            $("#slide_to_1").addClass("active");
                $("#slide_to_1").click(function(){
                    $("#slide_to_1").addClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_2").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").addClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_3").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").addClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_4").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").addClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_5").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").addClass("active");
                });
                

		}
	  }
	  
	//主題頁-中秋假期
	if($("#myCarousel_23").length > 0 ){
        var cnt = 0; cntto = -1
        $("#myCarousel_23").html("");
                
				$("<ol/>", {
                    "class": "carousel-indicators"
				}).appendTo("#myCarousel_23");  

				$("<div/>", {
					"role": "listbox",
                    "class": "carousel-inner"
				}).appendTo("#myCarousel_23");	
                

				$("<a/>", {
                    "id": "left_btn",
                    "class": "left carousel-control",
					"href": "#myCarousel_23",
                    "role": "button",
                    "data-slide": "prev"
				}).appendTo("#myCarousel_23");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-left"
                    }).appendTo("#myCarousel_23 #left_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Previous"
                    }).appendTo("#myCarousel_23 #left_btn");

				$("<a/>", {
                    "id": "right_btn",
                    "class": "right carousel-control",
					"href": "#myCarousel_23",
                    "role": "button",
                    "data-slide": "next"
				}).appendTo("#myCarousel_23");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-right"
                    }).appendTo("#myCarousel_23 #right_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Next"
                    }).appendTo("#myCarousel_23 #right_btn");

	  
        $(b).find("Site[SITE_CD='1']").children("Page[MP_ID='MPU03']").children("Module[DTL_ID='1']").children("Items").each(function(a) {          
		  cnt++;
          cntto++;
                
		var MPU03_IMG_URL = $(this).children("IMG_URL").text();
        var MPU03_LINK_URL = $(this).children("LINK_URL").text();
        var MPU03_LINK_NM = $(this).children("LINK_NM").text();

		
				$("<li/>", {
                    "id": "slide_to_"+cnt,
                    "class": " ",
                    "data-target": "#myCarousel_23",
                    "data-slide-to": cntto,
					"html" : MPU03_LINK_NM
				}).appendTo("#myCarousel_23 .carousel-indicators");

				$("<div/>", {
					"id": "listbox_li_"+cnt,
                    "class": "item"
				}).appendTo("#myCarousel_23 .carousel-inner");
				
					$("<a/>", {
						"id": "listbox_li_a_"+cnt,
						"href" : MPU03_LINK_URL
					}).appendTo("#listbox_li_"+cnt);
                
						$("<div/>", {
							"id": "listbox_li_a_img"+cnt,
							"class": "img"
						}).appendTo("a#listbox_li_a_"+cnt);
												
							$("<img/>", {
								"src": MPU03_IMG_URL
							}).appendTo("#listbox_li_a_img"+cnt);

				$("<div/>", {
					"id": "listbox_li_a_div"+cnt,
                    "class" : "carousel-caption",
				}).appendTo("#listbox_li_"+cnt+" #listbox_li_a_"+cnt);
				
				
      });
	  
		//首頁-大圖輪播JS
		if (autoPlayFG){
			$("#listbox_li_1").addClass("active");
            $("#slide_to_1").addClass("active");
                $("#slide_to_1").click(function(){
                    $("#slide_to_1").addClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_2").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").addClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_3").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").addClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_4").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").addClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_5").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").addClass("active");
                });
                

		}
	  }
	  
	//主題頁-直航優惠
	if($("#myCarousel_24").length > 0 ){
        var cnt = 0; cntto = -1
        $("#myCarousel_24").html("");
                
				$("<ol/>", {
                    "class": "carousel-indicators"
				}).appendTo("#myCarousel_24");  

				$("<div/>", {
					"role": "listbox",
                    "class": "carousel-inner"
				}).appendTo("#myCarousel_24");	
                

				$("<a/>", {
                    "id": "left_btn",
                    "class": "left carousel-control",
					"href": "#myCarousel_24",
                    "role": "button",
                    "data-slide": "prev"
				}).appendTo("#myCarousel_24");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-left"
                    }).appendTo("#myCarousel_24 #left_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Previous"
                    }).appendTo("#myCarousel_24 #left_btn");

				$("<a/>", {
                    "id": "right_btn",
                    "class": "right carousel-control",
					"href": "#myCarousel_24",
                    "role": "button",
                    "data-slide": "next"
				}).appendTo("#myCarousel_24");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-right"
                    }).appendTo("#myCarousel_24 #right_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Next"
                    }).appendTo("#myCarousel_24 #right_btn");

	  
        $(b).find("Site[SITE_CD='1']").children("Page[MP_ID='MPU04']").children("Module[DTL_ID='1']").children("Items").each(function(a) {          
		  cnt++;
          cntto++;
                
		var MPU04_IMG_URL = $(this).children("IMG_URL").text();
        var MPU04_LINK_URL = $(this).children("LINK_URL").text();
        var MPU04_LINK_NM = $(this).children("LINK_NM").text();

		
				$("<li/>", {
                    "id": "slide_to_"+cnt,
                    "class": " ",
                    "data-target": "#myCarousel_24",
                    "data-slide-to": cntto,
					"html" : MPU04_LINK_NM
				}).appendTo("#myCarousel_24 .carousel-indicators");

				$("<div/>", {
					"id": "listbox_li_"+cnt,
                    "class": "item"
				}).appendTo("#myCarousel_24 .carousel-inner");
				
					$("<a/>", {
						"id": "listbox_li_a_"+cnt,
						"href" : MPU04_LINK_URL
					}).appendTo("#listbox_li_"+cnt);
                
						$("<div/>", {
							"id": "listbox_li_a_img"+cnt,
							"class": "img"
						}).appendTo("a#listbox_li_a_"+cnt);
												
							$("<img/>", {
								"src": MPU04_IMG_URL
							}).appendTo("#listbox_li_a_img"+cnt);

				$("<div/>", {
					"id": "listbox_li_a_div"+cnt,
                    "class" : "carousel-caption",
				}).appendTo("#listbox_li_"+cnt+" #listbox_li_a_"+cnt);
				
				
      });
	  
		//首頁-大圖輪播JS
		if (autoPlayFG){
			$("#listbox_li_1").addClass("active");
            $("#slide_to_1").addClass("active");
                $("#slide_to_1").click(function(){
                    $("#slide_to_1").addClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_2").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").addClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_3").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").addClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_4").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").addClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_5").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").addClass("active");
                });
                

		}
	  }
	  
	//主題頁-婦幼清明
	if($("#myCarousel_25").length > 0 ){
        var cnt = 0; cntto = -1
        $("#myCarousel_25").html("");
                
				$("<ol/>", {
                    "class": "carousel-indicators"
				}).appendTo("#myCarousel_25");  

				$("<div/>", {
					"role": "listbox",
                    "class": "carousel-inner"
				}).appendTo("#myCarousel_25");	
                

				$("<a/>", {
                    "id": "left_btn",
                    "class": "left carousel-control",
					"href": "#myCarousel_25",
                    "role": "button",
                    "data-slide": "prev"
				}).appendTo("#myCarousel_25");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-left"
                    }).appendTo("#myCarousel_25 #left_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Previous"
                    }).appendTo("#myCarousel_25 #left_btn");

				$("<a/>", {
                    "id": "right_btn",
                    "class": "right carousel-control",
					"href": "#myCarousel_25",
                    "role": "button",
                    "data-slide": "next"
				}).appendTo("#myCarousel_25");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-right"
                    }).appendTo("#myCarousel_25 #right_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Next"
                    }).appendTo("#myCarousel_25 #right_btn");

	  
        $(b).find("Site[SITE_CD='1']").children("Page[MP_ID='MPU05']").children("Module[DTL_ID='1']").children("Items").each(function(a) {          
		  cnt++;
          cntto++;
                
		var MPU05_IMG_URL = $(this).children("IMG_URL").text();
        var MPU05_LINK_URL = $(this).children("LINK_URL").text();
        var MPU05_LINK_NM = $(this).children("LINK_NM").text();

		
				$("<li/>", {
                    "id": "slide_to_"+cnt,
                    "class": " ",
                    "data-target": "#myCarousel_25",
                    "data-slide-to": cntto,
					"html" : MPU05_LINK_NM
				}).appendTo("#myCarousel_25 .carousel-indicators");

				$("<div/>", {
					"id": "listbox_li_"+cnt,
                    "class": "item"
				}).appendTo("#myCarousel_25 .carousel-inner");
				
					$("<a/>", {
						"id": "listbox_li_a_"+cnt,
						"href" : MPU05_LINK_URL
					}).appendTo("#listbox_li_"+cnt);
                
						$("<div/>", {
							"id": "listbox_li_a_img"+cnt,
							"class": "img"
						}).appendTo("a#listbox_li_a_"+cnt);
												
							$("<img/>", {
								"src": MPU05_IMG_URL
							}).appendTo("#listbox_li_a_img"+cnt);

				$("<div/>", {
					"id": "listbox_li_a_div"+cnt,
                    "class" : "carousel-caption",
				}).appendTo("#listbox_li_"+cnt+" #listbox_li_a_"+cnt);
				
				
      });
	  
		//首頁-大圖輪播JS
		if (autoPlayFG){
			$("#listbox_li_1").addClass("active");
            $("#slide_to_1").addClass("active");
                $("#slide_to_1").click(function(){
                    $("#slide_to_1").addClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_2").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").addClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_3").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").addClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_4").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").addClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_5").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").addClass("active");
                });
                

		}
	  }
	  
	//主題頁-清艙促銷
	if($("#myCarousel_26").length > 0 ){
        var cnt = 0; cntto = -1
        $("#myCarousel_26").html("");
                
				$("<ol/>", {
                    "class": "carousel-indicators"
				}).appendTo("#myCarousel_26");  

				$("<div/>", {
					"role": "listbox",
                    "class": "carousel-inner"
				}).appendTo("#myCarousel_26");	
                

				$("<a/>", {
                    "id": "left_btn",
                    "class": "left carousel-control",
					"href": "#myCarousel_26",
                    "role": "button",
                    "data-slide": "prev"
				}).appendTo("#myCarousel_26");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-left"
                    }).appendTo("#myCarousel_26 #left_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Previous"
                    }).appendTo("#myCarousel_26 #left_btn");

				$("<a/>", {
                    "id": "right_btn",
                    "class": "right carousel-control",
					"href": "#myCarousel_26",
                    "role": "button",
                    "data-slide": "next"
				}).appendTo("#myCarousel_26");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-right"
                    }).appendTo("#myCarousel_26 #right_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Next"
                    }).appendTo("#myCarousel_26 #right_btn");

	  
        $(b).find("Site[SITE_CD='1']").children("Page[MP_ID='MPU06']").children("Module[DTL_ID='1']").children("Items").each(function(a) {          
		  cnt++;
          cntto++;
                
		var MPU06_IMG_URL = $(this).children("IMG_URL").text();
        var MPU06_LINK_URL = $(this).children("LINK_URL").text();
        var MPU06_LINK_NM = $(this).children("LINK_NM").text();

		
				$("<li/>", {
                    "id": "slide_to_"+cnt,
                    "class": " ",
                    "data-target": "#myCarousel_26",
                    "data-slide-to": cntto,
					"html" : MPU06_LINK_NM
				}).appendTo("#myCarousel_26 .carousel-indicators");

				$("<div/>", {
					"id": "listbox_li_"+cnt,
                    "class": "item"
				}).appendTo("#myCarousel_26 .carousel-inner");
				
					$("<a/>", {
						"id": "listbox_li_a_"+cnt,
						"href" : MPU06_LINK_URL
					}).appendTo("#listbox_li_"+cnt);
                
						$("<div/>", {
							"id": "listbox_li_a_img"+cnt,
							"class": "img"
						}).appendTo("a#listbox_li_a_"+cnt);
												
							$("<img/>", {
								"src": MPU06_IMG_URL
							}).appendTo("#listbox_li_a_img"+cnt);

				$("<div/>", {
					"id": "listbox_li_a_div"+cnt,
                    "class" : "carousel-caption",
				}).appendTo("#listbox_li_"+cnt+" #listbox_li_a_"+cnt);
				
				
      });
	  
		//首頁-大圖輪播JS
		if (autoPlayFG){
			$("#listbox_li_1").addClass("active");
            $("#slide_to_1").addClass("active");
                $("#slide_to_1").click(function(){
                    $("#slide_to_1").addClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_2").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").addClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_3").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").addClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_4").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").addClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_5").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").addClass("active");
                });
                

		}
	  }
	  
	//主題頁-聖誕&跨年
	if($("#myCarousel_27").length > 0 ){
        var cnt = 0; cntto = -1
        $("#myCarousel_27").html("");
                
				$("<ol/>", {
                    "class": "carousel-indicators"
				}).appendTo("#myCarousel_27");  

				$("<div/>", {
					"role": "listbox",
                    "class": "carousel-inner"
				}).appendTo("#myCarousel_27");	
                

				$("<a/>", {
                    "id": "left_btn",
                    "class": "left carousel-control",
					"href": "#myCarousel_27",
                    "role": "button",
                    "data-slide": "prev"
				}).appendTo("#myCarousel_27");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-left"
                    }).appendTo("#myCarousel_27 #left_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Previous"
                    }).appendTo("#myCarousel_27 #left_btn");

				$("<a/>", {
                    "id": "right_btn",
                    "class": "right carousel-control",
					"href": "#myCarousel_27",
                    "role": "button",
                    "data-slide": "next"
				}).appendTo("#myCarousel_27");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-right"
                    }).appendTo("#myCarousel_27 #right_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Next"
                    }).appendTo("#myCarousel_27 #right_btn");

	  
        $(b).find("Site[SITE_CD='1']").children("Page[MP_ID='MPU07']").children("Module[DTL_ID='1']").children("Items").each(function(a) {          
		  cnt++;
          cntto++;
                
		var MPU07_IMG_URL = $(this).children("IMG_URL").text();
        var MPU07_LINK_URL = $(this).children("LINK_URL").text();
        var MPU07_LINK_NM = $(this).children("LINK_NM").text();

		
				$("<li/>", {
                    "id": "slide_to_"+cnt,
                    "class": " ",
                    "data-target": "#myCarousel_27",
                    "data-slide-to": cntto,
					"html" : MPU07_LINK_NM
				}).appendTo("#myCarousel_27 .carousel-indicators");

				$("<div/>", {
					"id": "listbox_li_"+cnt,
                    "class": "item"
				}).appendTo("#myCarousel_27 .carousel-inner");
				
					$("<a/>", {
						"id": "listbox_li_a_"+cnt,
						"href" : MPU07_LINK_URL
					}).appendTo("#listbox_li_"+cnt);
                
						$("<div/>", {
							"id": "listbox_li_a_img"+cnt,
							"class": "img"
						}).appendTo("a#listbox_li_a_"+cnt);
												
							$("<img/>", {
								"src": MPU07_IMG_URL
							}).appendTo("#listbox_li_a_img"+cnt);

				$("<div/>", {
					"id": "listbox_li_a_div"+cnt,
                    "class" : "carousel-caption",
				}).appendTo("#listbox_li_"+cnt+" #listbox_li_a_"+cnt);
				
				
      });
	  
		//首頁-大圖輪播JS
		if (autoPlayFG){
			$("#listbox_li_1").addClass("active");
            $("#slide_to_1").addClass("active");
                $("#slide_to_1").click(function(){
                    $("#slide_to_1").addClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_2").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").addClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_3").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").addClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_4").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").addClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_5").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").addClass("active");
                });
                

		}
	  } 
	
	//主題頁-達人推薦
	if($("#myCarousel_28").length > 0 ){
        var cnt = 0; cntto = -1
        $("#myCarousel_28").html("");
                
				$("<ol/>", {
                    "class": "carousel-indicators"
				}).appendTo("#myCarousel_28");  

				$("<div/>", {
					"role": "listbox",
                    "class": "carousel-inner"
				}).appendTo("#myCarousel_28");	
                

				$("<a/>", {
                    "id": "left_btn",
                    "class": "left carousel-control",
					"href": "#myCarousel_28",
                    "role": "button",
                    "data-slide": "prev"
				}).appendTo("#myCarousel_28");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-left"
                    }).appendTo("#myCarousel_28 #left_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Previous"
                    }).appendTo("#myCarousel_28 #left_btn");

				$("<a/>", {
                    "id": "right_btn",
                    "class": "right carousel-control",
					"href": "#myCarousel_28",
                    "role": "button",
                    "data-slide": "next"
				}).appendTo("#myCarousel_28");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-right"
                    }).appendTo("#myCarousel_28 #right_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Next"
                    }).appendTo("#myCarousel_28 #right_btn");

	  
        $(b).find("Site[SITE_CD='1']").children("Page[MP_ID='MPU08']").children("Module[DTL_ID='1']").children("Items").each(function(a) {          
		  cnt++;
          cntto++;
                
		var MPU08_IMG_URL = $(this).children("IMG_URL").text();
        var MPU08_LINK_URL = $(this).children("LINK_URL").text();
        var MPU08_LINK_NM = $(this).children("LINK_NM").text();

		
				$("<li/>", {
                    "id": "slide_to_"+cnt,
                    "class": " ",
                    "data-target": "#myCarousel_28",
                    "data-slide-to": cntto,
					"html" : MPU08_LINK_NM
				}).appendTo("#myCarousel_28 .carousel-indicators");

				$("<div/>", {
					"id": "listbox_li_"+cnt,
                    "class": "item"
				}).appendTo("#myCarousel_28 .carousel-inner");
				
					$("<a/>", {
						"id": "listbox_li_a_"+cnt,
						"href" : MPU08_LINK_URL
					}).appendTo("#listbox_li_"+cnt);
                
						$("<div/>", {
							"id": "listbox_li_a_img"+cnt,
							"class": "img"
						}).appendTo("a#listbox_li_a_"+cnt);
												
							$("<img/>", {
								"src": MPU08_IMG_URL
							}).appendTo("#listbox_li_a_img"+cnt);

				$("<div/>", {
					"id": "listbox_li_a_div"+cnt,
                    "class" : "carousel-caption",
				}).appendTo("#listbox_li_"+cnt+" #listbox_li_a_"+cnt);
				
				
      });
	  
		//首頁-大圖輪播JS
		if (autoPlayFG){
			$("#listbox_li_1").addClass("active");
            $("#slide_to_1").addClass("active");
                $("#slide_to_1").click(function(){
                    $("#slide_to_1").addClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_2").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").addClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_3").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").addClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_4").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").addClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_5").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").addClass("active");
                });
                

		}
	  } 
	
	//主題頁-228優惠
	if($("#myCarousel_29").length > 0 ){
        var cnt = 0; cntto = -1
        $("#myCarousel_29").html("");
                
				$("<ol/>", {
                    "class": "carousel-indicators"
				}).appendTo("#myCarousel_29");  

				$("<div/>", {
					"role": "listbox",
                    "class": "carousel-inner"
				}).appendTo("#myCarousel_29");	
                

				$("<a/>", {
                    "id": "left_btn",
                    "class": "left carousel-control",
					"href": "#myCarousel_29",
                    "role": "button",
                    "data-slide": "prev"
				}).appendTo("#myCarousel_29");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-left"
                    }).appendTo("#myCarousel_29 #left_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Previous"
                    }).appendTo("#myCarousel_29 #left_btn");

				$("<a/>", {
                    "id": "right_btn",
                    "class": "right carousel-control",
					"href": "#myCarousel_29",
                    "role": "button",
                    "data-slide": "next"
				}).appendTo("#myCarousel_29");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-right"
                    }).appendTo("#myCarousel_29 #right_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Next"
                    }).appendTo("#myCarousel_29 #right_btn");

	  
        $(b).find("Site[SITE_CD='1']").children("Page[MP_ID='MPU09']").children("Module[DTL_ID='1']").children("Items").each(function(a) {          
		  cnt++;
          cntto++;
                
		var MPU09_IMG_URL = $(this).children("IMG_URL").text();
        var MPU09_LINK_URL = $(this).children("LINK_URL").text();
        var MPU09_LINK_NM = $(this).children("LINK_NM").text();

		
				$("<li/>", {
                    "id": "slide_to_"+cnt,
                    "class": " ",
                    "data-target": "#myCarousel_29",
                    "data-slide-to": cntto,
					"html" : MPU09_LINK_NM
				}).appendTo("#myCarousel_29 .carousel-indicators");

				$("<div/>", {
					"id": "listbox_li_"+cnt,
                    "class": "item"
				}).appendTo("#myCarousel_29 .carousel-inner");
				
					$("<a/>", {
						"id": "listbox_li_a_"+cnt,
						"href" : MPU09_LINK_URL
					}).appendTo("#listbox_li_"+cnt);
                
						$("<div/>", {
							"id": "listbox_li_a_img"+cnt,
							"class": "img"
						}).appendTo("a#listbox_li_a_"+cnt);
												
							$("<img/>", {
								"src": MPU09_IMG_URL
							}).appendTo("#listbox_li_a_img"+cnt);

				$("<div/>", {
					"id": "listbox_li_a_div"+cnt,
                    "class" : "carousel-caption",
				}).appendTo("#listbox_li_"+cnt+" #listbox_li_a_"+cnt);
				
				
      });
	  
		//首頁-大圖輪播JS
		if (autoPlayFG){
			$("#listbox_li_1").addClass("active");
            $("#slide_to_1").addClass("active");
                $("#slide_to_1").click(function(){
                    $("#slide_to_1").addClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_2").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").addClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_3").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").addClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_4").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").addClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_5").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").addClass("active");
                });
                

		}
	  } 
	
	//主題頁-軍人優惠
	if($("#myCarousel_30").length > 0 ){
        var cnt = 0; cntto = -1
        $("#myCarousel_30").html("");
                
				$("<ol/>", {
                    "class": "carousel-indicators"
				}).appendTo("#myCarousel_30");  

				$("<div/>", {
					"role": "listbox",
                    "class": "carousel-inner"
				}).appendTo("#myCarousel_30");	
                

				$("<a/>", {
                    "id": "left_btn",
                    "class": "left carousel-control",
					"href": "#myCarousel_30",
                    "role": "button",
                    "data-slide": "prev"
				}).appendTo("#myCarousel_30");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-left"
                    }).appendTo("#myCarousel_30 #left_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Previous"
                    }).appendTo("#myCarousel_30 #left_btn");

				$("<a/>", {
                    "id": "right_btn",
                    "class": "right carousel-control",
					"href": "#myCarousel_30",
                    "role": "button",
                    "data-slide": "next"
				}).appendTo("#myCarousel_30");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-right"
                    }).appendTo("#myCarousel_30 #right_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Next"
                    }).appendTo("#myCarousel_30 #right_btn");

	  
        $(b).find("Site[SITE_CD='1']").children("Page[MP_ID='MPU10']").children("Module[DTL_ID='1']").children("Items").each(function(a) {          
		  cnt++;
          cntto++;
                
		var MPU10_IMG_URL = $(this).children("IMG_URL").text();
        var MPU10_LINK_URL = $(this).children("LINK_URL").text();
        var MPU10_LINK_NM = $(this).children("LINK_NM").text();

		
				$("<li/>", {
                    "id": "slide_to_"+cnt,
                    "class": " ",
                    "data-target": "#myCarousel_30",
                    "data-slide-to": cntto,
					"html" : MPU10_LINK_NM
				}).appendTo("#myCarousel_30 .carousel-indicators");

				$("<div/>", {
					"id": "listbox_li_"+cnt,
                    "class": "item"
				}).appendTo("#myCarousel_30 .carousel-inner");
				
					$("<a/>", {
						"id": "listbox_li_a_"+cnt,
						"href" : MPU10_LINK_URL
					}).appendTo("#listbox_li_"+cnt);
                
						$("<div/>", {
							"id": "listbox_li_a_img"+cnt,
							"class": "img"
						}).appendTo("a#listbox_li_a_"+cnt);
												
							$("<img/>", {
								"src": MPU10_IMG_URL
							}).appendTo("#listbox_li_a_img"+cnt);

				$("<div/>", {
					"id": "listbox_li_a_div"+cnt,
                    "class" : "carousel-caption",
				}).appendTo("#listbox_li_"+cnt+" #listbox_li_a_"+cnt);
				
				
      });
	  
		//首頁-大圖輪播JS
		if (autoPlayFG){
			$("#listbox_li_1").addClass("active");
            $("#slide_to_1").addClass("active");
                $("#slide_to_1").click(function(){
                    $("#slide_to_1").addClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_2").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").addClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_3").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").addClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_4").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").addClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_5").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").addClass("active");
                });
                

		}
	  } 
	
	//主題頁-賞楓之旅
	if($("#myCarousel_31").length > 0 ){
        var cnt = 0; cntto = -1
        $("#myCarousel_31").html("");
                
				$("<ol/>", {
                    "class": "carousel-indicators"
				}).appendTo("#myCarousel_31");  

				$("<div/>", {
					"role": "listbox",
                    "class": "carousel-inner"
				}).appendTo("#myCarousel_31");	
                

				$("<a/>", {
                    "id": "left_btn",
                    "class": "left carousel-control",
					"href": "#myCarousel_31",
                    "role": "button",
                    "data-slide": "prev"
				}).appendTo("#myCarousel_31");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-left"
                    }).appendTo("#myCarousel_31 #left_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Previous"
                    }).appendTo("#myCarousel_31 #left_btn");

				$("<a/>", {
                    "id": "right_btn",
                    "class": "right carousel-control",
					"href": "#myCarousel_31",
                    "role": "button",
                    "data-slide": "next"
				}).appendTo("#myCarousel_31");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-right"
                    }).appendTo("#myCarousel_31 #right_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Next"
                    }).appendTo("#myCarousel_31 #right_btn");

	  
        $(b).find("Site[SITE_CD='1']").children("Page[MP_ID='MPU11']").children("Module[DTL_ID='1']").children("Items").each(function(a) {          
		  cnt++;
          cntto++;
                
		var MPU11_IMG_URL = $(this).children("IMG_URL").text();
        var MPU11_LINK_URL = $(this).children("LINK_URL").text();
        var MPU11_LINK_NM = $(this).children("LINK_NM").text();

		
				$("<li/>", {
                    "id": "slide_to_"+cnt,
                    "class": " ",
                    "data-target": "#myCarousel_31",
                    "data-slide-to": cntto,
					"html" : MPU11_LINK_NM
				}).appendTo("#myCarousel_31 .carousel-indicators");

				$("<div/>", {
					"id": "listbox_li_"+cnt,
                    "class": "item"
				}).appendTo("#myCarousel_31 .carousel-inner");
				
					$("<a/>", {
						"id": "listbox_li_a_"+cnt,
						"href" : MPU11_LINK_URL
					}).appendTo("#listbox_li_"+cnt);
                
						$("<div/>", {
							"id": "listbox_li_a_img"+cnt,
							"class": "img"
						}).appendTo("a#listbox_li_a_"+cnt);
												
							$("<img/>", {
								"src": MPU11_IMG_URL
							}).appendTo("#listbox_li_a_img"+cnt);

				$("<div/>", {
					"id": "listbox_li_a_div"+cnt,
                    "class" : "carousel-caption",
				}).appendTo("#listbox_li_"+cnt+" #listbox_li_a_"+cnt);
				
				
      });
	  
		//首頁-大圖輪播JS
		if (autoPlayFG){
			$("#listbox_li_1").addClass("active");
            $("#slide_to_1").addClass("active");
                $("#slide_to_1").click(function(){
                    $("#slide_to_1").addClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_2").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").addClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_3").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").addClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_4").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").addClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_5").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").addClass("active");
                });
                

		}
	  }
	
	//主題頁-機+酒優惠
	if($("#myCarousel_32").length > 0 ){
        var cnt = 0; cntto = -1
        $("#myCarousel_32").html("");
                
				$("<ol/>", {
                    "class": "carousel-indicators"
				}).appendTo("#myCarousel_32");  

				$("<div/>", {
					"role": "listbox",
                    "class": "carousel-inner"
				}).appendTo("#myCarousel_32");	
                

				$("<a/>", {
                    "id": "left_btn",
                    "class": "left carousel-control",
					"href": "#myCarousel_32",
                    "role": "button",
                    "data-slide": "prev"
				}).appendTo("#myCarousel_32");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-left"
                    }).appendTo("#myCarousel_32 #left_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Previous"
                    }).appendTo("#myCarousel_32 #left_btn");

				$("<a/>", {
                    "id": "right_btn",
                    "class": "right carousel-control",
					"href": "#myCarousel_32",
                    "role": "button",
                    "data-slide": "next"
				}).appendTo("#myCarousel_32");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-right"
                    }).appendTo("#myCarousel_32 #right_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Next"
                    }).appendTo("#myCarousel_32 #right_btn");

	  
        $(b).find("Site[SITE_CD='1']").children("Page[MP_ID='MPU12']").children("Module[DTL_ID='1']").children("Items").each(function(a) {          
		  cnt++;
          cntto++;
                
		var MPU12_IMG_URL = $(this).children("IMG_URL").text();
        var MPU12_LINK_URL = $(this).children("LINK_URL").text();
        var MPU12_LINK_NM = $(this).children("LINK_NM").text();

		
				$("<li/>", {
                    "id": "slide_to_"+cnt,
                    "class": " ",
                    "data-target": "#myCarousel_32",
                    "data-slide-to": cntto,
					"html" : MPU12_LINK_NM
				}).appendTo("#myCarousel_32 .carousel-indicators");

				$("<div/>", {
					"id": "listbox_li_"+cnt,
                    "class": "item"
				}).appendTo("#myCarousel_32 .carousel-inner");
				
					$("<a/>", {
						"id": "listbox_li_a_"+cnt,
						"href" : MPU12_LINK_URL
					}).appendTo("#listbox_li_"+cnt);
                
						$("<div/>", {
							"id": "listbox_li_a_img"+cnt,
							"class": "img"
						}).appendTo("a#listbox_li_a_"+cnt);
												
							$("<img/>", {
								"src": MPU12_IMG_URL
							}).appendTo("#listbox_li_a_img"+cnt);

				$("<div/>", {
					"id": "listbox_li_a_div"+cnt,
                    "class" : "carousel-caption",
				}).appendTo("#listbox_li_"+cnt+" #listbox_li_a_"+cnt);
				
				
      });
	  
		//首頁-大圖輪播JS
		if (autoPlayFG){
			$("#listbox_li_1").addClass("active");
            $("#slide_to_1").addClass("active");
                $("#slide_to_1").click(function(){
                    $("#slide_to_1").addClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_2").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").addClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_3").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").addClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_4").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").addClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_5").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").addClass("active");
                });
                

		}
	  }
	
	//主題頁-週年慶特惠
	if($("#myCarousel_33").length > 0 ){
        var cnt = 0; cntto = -1
        $("#myCarousel_33").html("");
                
				$("<ol/>", {
                    "class": "carousel-indicators"
				}).appendTo("#myCarousel_33");  

				$("<div/>", {
					"role": "listbox",
                    "class": "carousel-inner"
				}).appendTo("#myCarousel_33");	
                

				$("<a/>", {
                    "id": "left_btn",
                    "class": "left carousel-control",
					"href": "#myCarousel_33",
                    "role": "button",
                    "data-slide": "prev"
				}).appendTo("#myCarousel_33");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-left"
                    }).appendTo("#myCarousel_33 #left_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Previous"
                    }).appendTo("#myCarousel_33 #left_btn");

				$("<a/>", {
                    "id": "right_btn",
                    "class": "right carousel-control",
					"href": "#myCarousel_33",
                    "role": "button",
                    "data-slide": "next"
				}).appendTo("#myCarousel_33");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-right"
                    }).appendTo("#myCarousel_33 #right_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Next"
                    }).appendTo("#myCarousel_33 #right_btn");

	  
        $(b).find("Site[SITE_CD='1']").children("Page[MP_ID='MPU13']").children("Module[DTL_ID='1']").children("Items").each(function(a) {          
		  cnt++;
          cntto++;
                
		var MPU13_IMG_URL = $(this).children("IMG_URL").text();
        var MPU13_LINK_URL = $(this).children("LINK_URL").text();
        var MPU13_LINK_NM = $(this).children("LINK_NM").text();

		
				$("<li/>", {
                    "id": "slide_to_"+cnt,
                    "class": " ",
                    "data-target": "#myCarousel_33",
                    "data-slide-to": cntto,
					"html" : MPU13_LINK_NM
				}).appendTo("#myCarousel_33 .carousel-indicators");

				$("<div/>", {
					"id": "listbox_li_"+cnt,
                    "class": "item"
				}).appendTo("#myCarousel_33 .carousel-inner");
				
					$("<a/>", {
						"id": "listbox_li_a_"+cnt,
						"href" : MPU13_LINK_URL
					}).appendTo("#listbox_li_"+cnt);
                
						$("<div/>", {
							"id": "listbox_li_a_img"+cnt,
							"class": "img"
						}).appendTo("a#listbox_li_a_"+cnt);
												
							$("<img/>", {
								"src": MPU13_IMG_URL
							}).appendTo("#listbox_li_a_img"+cnt);

				$("<div/>", {
					"id": "listbox_li_a_div"+cnt,
                    "class" : "carousel-caption",
				}).appendTo("#listbox_li_"+cnt+" #listbox_li_a_"+cnt);
				
				
      });
	  
		//首頁-大圖輪播JS
		if (autoPlayFG){
			$("#listbox_li_1").addClass("active");
            $("#slide_to_1").addClass("active");
                $("#slide_to_1").click(function(){
                    $("#slide_to_1").addClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_2").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").addClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_3").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").addClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_4").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").addClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_5").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").addClass("active");
                });
                

		}
	  }
	
	//主題頁-暑假優惠
	if($("#myCarousel_34").length > 0 ){
        var cnt = 0; cntto = -1
        $("#myCarousel_34").html("");
                
				$("<ol/>", {
                    "class": "carousel-indicators"
				}).appendTo("#myCarousel_34");  

				$("<div/>", {
					"role": "listbox",
                    "class": "carousel-inner"
				}).appendTo("#myCarousel_34");	
                

				$("<a/>", {
                    "id": "left_btn",
                    "class": "left carousel-control",
					"href": "#myCarousel_34",
                    "role": "button",
                    "data-slide": "prev"
				}).appendTo("#myCarousel_34");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-left"
                    }).appendTo("#myCarousel_34 #left_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Previous"
                    }).appendTo("#myCarousel_34 #left_btn");

				$("<a/>", {
                    "id": "right_btn",
                    "class": "right carousel-control",
					"href": "#myCarousel_34",
                    "role": "button",
                    "data-slide": "next"
				}).appendTo("#myCarousel_34");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-right"
                    }).appendTo("#myCarousel_34 #right_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Next"
                    }).appendTo("#myCarousel_34 #right_btn");

	  
        $(b).find("Site[SITE_CD='1']").children("Page[MP_ID='MPU16']").children("Module[DTL_ID='1']").children("Items").each(function(a) {          
		  cnt++;
          cntto++;
                
		var MPU16_IMG_URL = $(this).children("IMG_URL").text();
        var MPU16_LINK_URL = $(this).children("LINK_URL").text(); 
        var MPU16_LINK_NM = $(this).children("LINK_NM").text();

		
				$("<li/>", {
                    "id": "slide_to_"+cnt,
                    "class": " ",
                    "data-target": "#myCarousel_34",
                    "data-slide-to": cntto,
					"html" : MPU16_LINK_NM
				}).appendTo("#myCarousel_34 .carousel-indicators");

				$("<div/>", {
					"id": "listbox_li_"+cnt,
                    "class": "item"
				}).appendTo("#myCarousel_34 .carousel-inner");
				
					$("<a/>", {
						"id": "listbox_li_a_"+cnt,
						"href" : MPU16_LINK_URL
					}).appendTo("#listbox_li_"+cnt);
                
						$("<div/>", {
							"id": "listbox_li_a_img"+cnt,
							"class": "img"
						}).appendTo("a#listbox_li_a_"+cnt);
												
							$("<img/>", {
								"src": MPU16_IMG_URL
							}).appendTo("#listbox_li_a_img"+cnt);

				$("<div/>", {
					"id": "listbox_li_a_div"+cnt,
                    "class" : "carousel-caption",
				}).appendTo("#listbox_li_"+cnt+" #listbox_li_a_"+cnt);
				
				
      });
	  
		//首頁-大圖輪播JS
		if (autoPlayFG){
			$("#listbox_li_1").addClass("active");
            $("#slide_to_1").addClass("active");
                $("#slide_to_1").click(function(){
                    $("#slide_to_1").addClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_2").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").addClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_3").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").addClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_4").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").addClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_5").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").addClass("active");
                });
                

		}
	  }
	
	//主題頁-寒假優惠
	if($("#myCarousel_35").length > 0 ){
        var cnt = 0; cntto = -1
        $("#myCarousel_35").html("");
                
				$("<ol/>", {
                    "class": "carousel-indicators"
				}).appendTo("#myCarousel_35");  

				$("<div/>", {
					"role": "listbox",
                    "class": "carousel-inner"
				}).appendTo("#myCarousel_35");	
                

				$("<a/>", {
                    "id": "left_btn",
                    "class": "left carousel-control",
					"href": "#myCarousel_35",
                    "role": "button",
                    "data-slide": "prev"
				}).appendTo("#myCarousel_35");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-left"
                    }).appendTo("#myCarousel_35 #left_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Previous"
                    }).appendTo("#myCarousel_35 #left_btn");

				$("<a/>", {
                    "id": "right_btn",
                    "class": "right carousel-control",
					"href": "#myCarousel_35",
                    "role": "button",
                    "data-slide": "next"
				}).appendTo("#myCarousel_35");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-right"
                    }).appendTo("#myCarousel_35 #right_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Next"
                    }).appendTo("#myCarousel_35 #right_btn");

	  
        $(b).find("Site[SITE_CD='1']").children("Page[MP_ID='MPU15']").children("Module[DTL_ID='1']").children("Items").each(function(a) {          
		  cnt++;
          cntto++;
                
		var MPU15_IMG_URL = $(this).children("IMG_URL").text();
        var MPU15_LINK_URL = $(this).children("LINK_URL").text();
        var MPU15_LINK_NM = $(this).children("LINK_NM").text();

		
				$("<li/>", {
                    "id": "slide_to_"+cnt,
                    "class": " ",
                    "data-target": "#myCarousel_35",
                    "data-slide-to": cntto,
					"html" : MPU15_LINK_NM
				}).appendTo("#myCarousel_35 .carousel-indicators");

				$("<div/>", {
					"id": "listbox_li_"+cnt,
                    "class": "item"
				}).appendTo("#myCarousel_35 .carousel-inner");
				
					$("<a/>", {
						"id": "listbox_li_a_"+cnt,
						"href" : MPU15_LINK_URL
					}).appendTo("#listbox_li_"+cnt);
                
						$("<div/>", {
							"id": "listbox_li_a_img"+cnt,
							"class": "img"
						}).appendTo("a#listbox_li_a_"+cnt);
												
							$("<img/>", {
								"src": MPU15_IMG_URL
							}).appendTo("#listbox_li_a_img"+cnt);

				$("<div/>", {
					"id": "listbox_li_a_div"+cnt,
                    "class" : "carousel-caption",
				}).appendTo("#listbox_li_"+cnt+" #listbox_li_a_"+cnt);
				
				
      });
	  
		//首頁-大圖輪播JS
		if (autoPlayFG){
			$("#listbox_li_1").addClass("active");
            $("#slide_to_1").addClass("active");
                $("#slide_to_1").click(function(){
                    $("#slide_to_1").addClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_2").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").addClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_3").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").addClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_4").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").addClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_5").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").addClass("active");
                });
                

		}
	  }
	
	//主題頁-雙十特惠
	if($("#myCarousel_36").length > 0 ){
        var cnt = 0; cntto = -1
        $("#myCarousel_36").html("");
                
				$("<ol/>", {
                    "class": "carousel-indicators"
				}).appendTo("#myCarousel_36");  

				$("<div/>", {
					"role": "listbox",
                    "class": "carousel-inner"
				}).appendTo("#myCarousel_36");	
                

				$("<a/>", {
                    "id": "left_btn",
                    "class": "left carousel-control",
					"href": "#myCarousel_36",
                    "role": "button",
                    "data-slide": "prev"
				}).appendTo("#myCarousel_36");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-left"
                    }).appendTo("#myCarousel_36 #left_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Previous"
                    }).appendTo("#myCarousel_36 #left_btn");

				$("<a/>", {
                    "id": "right_btn",
                    "class": "right carousel-control",
					"href": "#myCarousel_36",
                    "role": "button",
                    "data-slide": "next"
				}).appendTo("#myCarousel_36");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-right"
                    }).appendTo("#myCarousel_36 #right_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Next"
                    }).appendTo("#myCarousel_36 #right_btn");

	  
        $(b).find("Site[SITE_CD='1']").children("Page[MP_ID='MPU16']").children("Module[DTL_ID='1']").children("Items").each(function(a) {          
		  cnt++;
          cntto++;
                
		var MPU16_IMG_URL = $(this).children("IMG_URL").text();
        var MPU16_LINK_URL = $(this).children("LINK_URL").text();
        var MPU16_LINK_NM = $(this).children("LINK_NM").text();

		
				$("<li/>", {
                    "id": "slide_to_"+cnt,
                    "class": " ",
                    "data-target": "#myCarousel_36",
                    "data-slide-to": cntto,
					"html" : MPU16_LINK_NM
				}).appendTo("#myCarousel_36 .carousel-indicators");

				$("<div/>", {
					"id": "listbox_li_"+cnt,
                    "class": "item"
				}).appendTo("#myCarousel_36 .carousel-inner");
				
					$("<a/>", {
						"id": "listbox_li_a_"+cnt,
						"href" : MPU16_LINK_URL
					}).appendTo("#listbox_li_"+cnt);
                
						$("<div/>", {
							"id": "listbox_li_a_img"+cnt,
							"class": "img"
						}).appendTo("a#listbox_li_a_"+cnt);
												
							$("<img/>", {
								"src": MPU16_IMG_URL
							}).appendTo("#listbox_li_a_img"+cnt);

				$("<div/>", {
					"id": "listbox_li_a_div"+cnt,
                    "class" : "carousel-caption",
				}).appendTo("#listbox_li_"+cnt+" #listbox_li_a_"+cnt);
				
				
      });
	  
		//首頁-大圖輪播JS
		if (autoPlayFG){
			$("#listbox_li_1").addClass("active");
            $("#slide_to_1").addClass("active");
                $("#slide_to_1").click(function(){
                    $("#slide_to_1").addClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_2").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").addClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_3").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").addClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_4").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").addClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_5").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").addClass("active");
                });
                

		}
	  }
	
	//主題頁-畢業特惠
	if($("#myCarousel_37").length > 0 ){
        var cnt = 0; cntto = -1
        $("#myCarousel_37").html("");
                
				$("<ol/>", {
                    "class": "carousel-indicators"
				}).appendTo("#myCarousel_37");  

				$("<div/>", {
					"role": "listbox",
                    "class": "carousel-inner"
				}).appendTo("#myCarousel_37");	
                

				$("<a/>", {
                    "id": "left_btn",
                    "class": "left carousel-control",
					"href": "#myCarousel_37",
                    "role": "button",
                    "data-slide": "prev"
				}).appendTo("#myCarousel_37");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-left"
                    }).appendTo("#myCarousel_37 #left_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Previous"
                    }).appendTo("#myCarousel_37 #left_btn");

				$("<a/>", {
                    "id": "right_btn",
                    "class": "right carousel-control",
					"href": "#myCarousel_37",
                    "role": "button",
                    "data-slide": "next"
				}).appendTo("#myCarousel_37");

                    $("<span/>", {
                        "class": "glyphicon glyphicon-chevron-right"
                    }).appendTo("#myCarousel_37 #right_btn");

                    $("<span/>", {
                        "class": "sr-only",
                        "html": "Next"
                    }).appendTo("#myCarousel_37 #right_btn");

	  
        $(b).find("Site[SITE_CD='1']").children("Page[MP_ID='MPU17']").children("Module[DTL_ID='1']").children("Items").each(function(a) {          
		  cnt++;
          cntto++;
                
		var MPU17_IMG_URL = $(this).children("IMG_URL").text();
        var MPU17_LINK_URL = $(this).children("LINK_URL").text();
        var MPU17_LINK_NM = $(this).children("LINK_NM").text();

		
				$("<li/>", {
                    "id": "slide_to_"+cnt,
                    "class": " ",
                    "data-target": "#myCarousel_37",
                    "data-slide-to": cntto,
					"html" : MPU17_LINK_NM
				}).appendTo("#myCarousel_37 .carousel-indicators");

				$("<div/>", {
					"id": "listbox_li_"+cnt,
                    "class": "item"
				}).appendTo("#myCarousel_37 .carousel-inner");
				
					$("<a/>", {
						"id": "listbox_li_a_"+cnt,
						"href" : MPU17_LINK_URL
					}).appendTo("#listbox_li_"+cnt);
                
						$("<div/>", {
							"id": "listbox_li_a_img"+cnt,
							"class": "img"
						}).appendTo("a#listbox_li_a_"+cnt);
												
							$("<img/>", {
								"src": MPU17_IMG_URL
							}).appendTo("#listbox_li_a_img"+cnt);

				$("<div/>", {
					"id": "listbox_li_a_div"+cnt,
                    "class" : "carousel-caption",
				}).appendTo("#listbox_li_"+cnt+" #listbox_li_a_"+cnt);
				
				
      });
	  
		//首頁-大圖輪播JS
		if (autoPlayFG){
			$("#listbox_li_1").addClass("active");
            $("#slide_to_1").addClass("active");
                $("#slide_to_1").click(function(){
                    $("#slide_to_1").addClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_2").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").addClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_3").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").addClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_4").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").addClass("active");
                    $("#slide_to_5").removeClass("active");
                });
                $("#slide_to_5").click(function(){
                    $("#slide_to_1").removeClass("active");
                    $("#slide_to_2").removeClass("active");
                    $("#slide_to_3").removeClass("active");
                    $("#slide_to_4").removeClass("active");
                    $("#slide_to_5").addClass("active");
                });
                

		}
	  }
	
    }
	
	 
  })

}



var fixed_menu = true;
window.jQuery = window.$ = jQuery;

/*-----------------------------------------------------------------------------------*/
/*	NICESCROLL
/*-----------------------------------------------------------------------------------*/
jQuery(document).ready(function() {
	jQuery("body").niceScroll({
		cursorcolor:"#333",
		cursorborder:"0px",
		cursorwidth :"8px",
		zindex:"9999"
	});
});	
/*-----------------------------------------------------------------------------------*/
/*	PRELOADER
/*-----------------------------------------------------------------------------------*/
$(document).ready(function() {
	
	setTimeout(function(){
		$('body').addClass('loaded');
		//$('h1').css('color','#ff0040');
	}, 3000);
	
});

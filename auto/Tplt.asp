﻿<!DOCTYPE html><!-- lang="zh-TW"-->
<html class="no-js">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta charset="utf-8">
<title>直航優惠</title>
<!-- for Facebook -->          
<meta property="og:url"                content="http://www.summittour.com.tw/eWeb_summittour/auto/Central_tours_UP07.asp" />
<meta property="og:type"               content="website" />
<meta property="og:locale"             content="zh_TW" />
<meta property="og:title"              content="頂尖旅遊限定優惠" />
<meta property="og:description"        content="直航優惠" />
<meta property="og:image"              content="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001328/001329/001336/00015739.JPG	" />
<!-- for Facebook -->  
<link rel="shortcut icon" type="image/x-icon" href="/eweb_summittour/images/default/favicon.ico">
<link rel="bookmark" type="image/x-icon" href="/eweb_summittour/images/default/favicon.ico">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="description" content="直航優惠"><!--網站描述-->
<meta name="keywords" content="直航優惠"><!--關鍵字-->
<meta name="Robots" content="all" />
<meta name="Googlebot" content="index,follow" />
<meta name="Audience" content="All" />
<meta name="Page-topic" content="Internet" />
<meta name="Rating" content="General" />
<meta name="Resource-Type" content="document" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="windows-Target" content="_top" />
<meta name="Author" content="GTSHAYO" />
<meta name="Copyright" content="Copyright c 2017 www.summittour.com.tw All Rights Reserved." />
<meta name="Generator" content="Visual Studio Code" />
<meta name="Publisher" content="超悠旅行社" />
<meta name="Creation-Date" content="06-23-2017" />
<meta name="WEBCRAWLERS" content="ALL" />
<meta name="SPIDERS" content="ALL" />
<meta name="revisit-after" content="7 days"/>

<link type="text/css" rel="stylesheet" href="/eweb_summittour/css/reset.css">
<link type="text/css" rel="stylesheet" href="/eweb_summittour/css/normalize.css">
<link type="text/css" rel="stylesheet" href="/eweb_summittour/css/bootstrap.css">
<link type="text/css" rel="stylesheet" href="/eweb_summittour/css/bootstrap-datetimepicker.min.css" media="screen">
<link type="text/css" rel="stylesheet" href="/eweb_summittour/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="/eweb_summittour/css/main.css">
<link type="text/css" rel="stylesheet" href="/eweb_summittour/css/default.css">

<script type="text/javascript" language="javascript" src="/eweb_summittour/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" language="javascript" src="/eweb_summittour/js/bootstrap.min.js"></script>
<!--<script type="text/javascript" language="javascript" src="/eweb_summittour/js/jquery.nicescroll.min.js"></script>-->
<script type="text/javascript" language="javascript" src="/eweb_summittour/js/modernizr.js"></script>
<script type="text/javascript" language="javascript" src="/eweb_summittour/js/all.js"></script>
<script type="text/javascript" language="javascript" src="/eweb_summittour/js/hammer.min.js"></script> 




<link href="/eweb_summittour/auto/autopage.css" rel="stylesheet" type="text/css">
<style type="text/css">
#skin {display: table; float: left;}
#skin li {float: left; font-size:  20px; color: #999999; padding: 0 5px; cursor: pointer;}
#skin li i {padding: 0 5px;}
#skin li:hover {color: #74a73a;}
#skin li.selected {color: #74a73a; cursor: default;}
</style>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-99295320-1"></script>
<script>

  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-99295320-1');
</script>


<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="UTF-8">
<title>海棠風情─潑墨黃山臥虎藏龍宏村屯溪老街徽州古城五日-超悠旅行社-頂尖旅遊</title>
<link rel="shortcut icon" type="image/x-icon" href="/eweb_summittour/images/rwd_icon.png">
<link rel="bookmark" type="image/x-icon" href="/eweb_summittour/images/rwd_icon.png">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="Robots" content="all">
<meta name="Googlebot" content="index,follow">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="windows-Target" content="_top">
<meta name="revisit-after" content="7 days">
<meta name="description" content="">
<meta name="keywords" content="">
<meta property="og:image" content="/eWeb_summittour/IMGDB/000060/000258/000259/00015965.JPG">
<meta property="og:title" content="海棠風情─潑墨黃山臥虎藏龍宏村屯溪老街徽州古城五日">
<meta property="og:url" content="http://www.summittour.com.tw/ew/go/mgroupdetail.asp?prodcd=txnmut5a">
<meta property="og:description" content="">
<meta property="fb:app_id" content="">

<link type="text/css" rel="stylesheet" href="/eweb_summittour/css/reset.css">
<link type="text/css" rel="stylesheet" href="/eweb_summittour/css/normalize.css">
<link type="text/css" rel="stylesheet" href="/eweb_summittour/css/bootstrap.css">
<link type="text/css" rel="stylesheet" href="/eweb_summittour/css/bootstrap-datetimepicker.min.css" media="screen">
<link type="text/css" rel="stylesheet" href="/eweb_summittour/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="/eweb_summittour/css/main.css">
<link type="text/css" rel="stylesheet" href="/eweb_summittour/css/default.css">
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn’t work if you view the page via file:// -->
    
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

	<link type="text/css" rel="stylesheet" href="/eweb_summittour/css/animations.css">
	<link rel="stylesheet" type="text/css" href="/eweb_summittour/css/loaders.css">
	
<script type="text/javascript" async="" src="https://apis.google.com/js/platform.js" gapi_processed="true"></script><script id="facebook-jssdk" src="//connect.facebook.net/zh_TW/sdk.js#xfbml=1&amp;version=v2.8&amp;appId=116456551778811"></script><script async="" src="//www.google-analytics.com/analytics.js"></script><script type="text/javascript">
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	ga('create', 'UA-81282938-2', 'auto');
ga('send', 'pageview');
</script>

<style type="text/css">.backpack.dropzone {
  font-family: 'SF UI Display', 'Segoe UI';
  font-size: 15px;
  text-align: center;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 250px;
  height: 150px;
  font-weight: lighter;
  color: white;
  will-change: right;
  z-index: 2147483647;
  bottom: 20%;
  background: #333;
  position: fixed;
  user-select: none;
  transition: left .5s, right .5s;
  right: 0px; }
  .backpack.dropzone .animation {
    height: 80px;
    width: 250px;
    background: url("chrome-extension://lifbcibllhkdhoafpjfnlhfpfgnpldfl/assets/backpack/dropzone/hoverstate.png") left center; }
  .backpack.dropzone .title::before {
    content: 'Save to'; }
  .backpack.dropzone.closed {
    right: -250px; }
  .backpack.dropzone.hover .animation {
    animation: sxt-play-anim-hover 0.91s steps(21);
    animation-fill-mode: forwards;
    background: url("chrome-extension://lifbcibllhkdhoafpjfnlhfpfgnpldfl/assets/backpack/dropzone/hoverstate.png") left center; }

@keyframes sxt-play-anim-hover {
  from {
    background-position: 0px; }
  to {
    background-position: -5250px; } }
  .backpack.dropzone.saving .title::before {
    content: 'Saving to'; }
  .backpack.dropzone.saving .animation {
    background: url("chrome-extension://lifbcibllhkdhoafpjfnlhfpfgnpldfl/assets/backpack/dropzone/saving_loop.png") left center;
    animation: sxt-play-anim-saving steps(59) 2.46s infinite; }

@keyframes sxt-play-anim-saving {
  100% {
    background-position: -14750px; } }
  .backpack.dropzone.saved .title::before {
    content: 'Saved to'; }
  .backpack.dropzone.saved .animation {
    background: url("chrome-extension://lifbcibllhkdhoafpjfnlhfpfgnpldfl/assets/backpack/dropzone/saved.png") left center;
    animation: sxt-play-anim-saved steps(20) 0.83s forwards; }

@keyframes sxt-play-anim-saved {
  100% {
    background-position: -5000px; } }
</style><script src="chrome-extension://odamfmfcmgcaghpmeppfiaaafahcnfbc/js/synofficeExt.js"></script><style type="text/css">.fb_hidden{position:absolute;top:-10000px;z-index:10001}.fb_reposition{overflow:hidden;position:relative}.fb_invisible{display:none}.fb_reset{background:none;border:0;border-spacing:0;color:#000;cursor:auto;direction:ltr;font-family:"lucida grande", tahoma, verdana, arial, sans-serif;font-size:12px;font-style:normal;font-variant:normal;font-weight:normal;letter-spacing:normal;line-height:1;margin:0;overflow:visible;padding:0;text-align:left;text-decoration:none;text-indent:0;text-shadow:none;text-transform:none;visibility:visible;white-space:normal;word-spacing:normal}.fb_reset>div{overflow:hidden}.fb_link img{border:none}@keyframes fb_transform{from{opacity:0;transform:scale(.95)}to{opacity:1;transform:scale(1)}}.fb_animate{animation:fb_transform .3s forwards}
.fb_dialog{background:rgba(82, 82, 82, .7);position:absolute;top:-10000px;z-index:10001}.fb_reset .fb_dialog_legacy{overflow:visible}.fb_dialog_advanced{padding:10px;border-radius:8px}.fb_dialog_content{background:#fff;color:#333}.fb_dialog_close_icon{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/yq/r/IE9JII6Z1Ys.png) no-repeat scroll 0 0 transparent;cursor:pointer;display:block;height:15px;position:absolute;right:18px;top:17px;width:15px}.fb_dialog_mobile .fb_dialog_close_icon{top:5px;left:5px;right:auto}.fb_dialog_padding{background-color:transparent;position:absolute;width:1px;z-index:-1}.fb_dialog_close_icon:hover{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/yq/r/IE9JII6Z1Ys.png) no-repeat scroll 0 -15px transparent}.fb_dialog_close_icon:active{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/yq/r/IE9JII6Z1Ys.png) no-repeat scroll 0 -30px transparent}.fb_dialog_loader{background-color:#f6f7f9;border:1px solid #606060;font-size:25px;padding:20px}.fb_dialog_top_left,.fb_dialog_top_right,.fb_dialog_bottom_left,.fb_dialog_bottom_right{height:10px;width:10px;overflow:hidden;position:absolute}.fb_dialog_top_left{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/ye/r/8YeTNIlTZjm.png) no-repeat 0 0;left:-10px;top:-10px}.fb_dialog_top_right{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/ye/r/8YeTNIlTZjm.png) no-repeat 0 -10px;right:-10px;top:-10px}.fb_dialog_bottom_left{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/ye/r/8YeTNIlTZjm.png) no-repeat 0 -20px;bottom:-10px;left:-10px}.fb_dialog_bottom_right{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/ye/r/8YeTNIlTZjm.png) no-repeat 0 -30px;right:-10px;bottom:-10px}.fb_dialog_vert_left,.fb_dialog_vert_right,.fb_dialog_horiz_top,.fb_dialog_horiz_bottom{position:absolute;background:#525252;filter:alpha(opacity=70);opacity:.7}.fb_dialog_vert_left,.fb_dialog_vert_right{width:10px;height:100%}.fb_dialog_vert_left{margin-left:-10px}.fb_dialog_vert_right{right:0;margin-right:-10px}.fb_dialog_horiz_top,.fb_dialog_horiz_bottom{width:100%;height:10px}.fb_dialog_horiz_top{margin-top:-10px}.fb_dialog_horiz_bottom{bottom:0;margin-bottom:-10px}.fb_dialog_iframe{line-height:0}.fb_dialog_content .dialog_title{background:#6d84b4;border:1px solid #365899;color:#fff;font-size:15px;font-weight:bold;margin:0}.fb_dialog_content .dialog_title>span{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/yd/r/Cou7n-nqK52.gif) no-repeat 5px 50%;float:left;padding:5px 0 7px 26px}body.fb_hidden{-webkit-transform:none;height:100%;margin:0;overflow:visible;position:absolute;top:-10000px;left:0;width:100%}.fb_dialog.fb_dialog_mobile.loading{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/ya/r/3rhSv5V8j3o.gif) white no-repeat 50% 50%;min-height:100%;min-width:100%;overflow:hidden;position:absolute;top:0;z-index:10001}.fb_dialog.fb_dialog_mobile.loading.centered{width:auto;height:auto;min-height:initial;min-width:initial;background:none}.fb_dialog.fb_dialog_mobile.loading.centered #fb_dialog_loader_spinner{width:100%}.fb_dialog.fb_dialog_mobile.loading.centered .fb_dialog_content{background:none}.loading.centered #fb_dialog_loader_close{color:#fff;display:block;padding-top:20px;clear:both;font-size:19px}#fb-root #fb_dialog_ipad_overlay{background:rgba(0, 0, 0, .45);position:absolute;bottom:0;left:0;right:0;top:0;width:100%;min-height:100%;z-index:10000}#fb-root #fb_dialog_ipad_overlay.hidden{display:none}.fb_dialog.fb_dialog_mobile.loading iframe{visibility:hidden}.fb_dialog_content .dialog_header{-webkit-box-shadow:white 0 1px 1px -1px inset;background:-webkit-gradient(linear, 0% 0%, 0% 100%, from(#738ABA), to(#2C4987));border-bottom:1px solid;border-color:#1d4088;color:#fff;font:14px Helvetica, sans-serif;font-weight:bold;text-overflow:ellipsis;text-shadow:rgba(0, 30, 84, .296875) 0 -1px 0;vertical-align:middle;white-space:nowrap}.fb_dialog_content .dialog_header table{-webkit-font-smoothing:subpixel-antialiased;height:43px;width:100%}.fb_dialog_content .dialog_header td.header_left{font-size:13px;padding-left:5px;vertical-align:middle;width:60px}.fb_dialog_content .dialog_header td.header_right{font-size:13px;padding-right:5px;vertical-align:middle;width:60px}.fb_dialog_content .touchable_button{background:-webkit-gradient(linear, 0% 0%, 0% 100%, from(#4966A6), color-stop(.5, #355492), to(#2A4887));border:1px solid #29487d;-webkit-background-clip:padding-box;-webkit-border-radius:3px;-webkit-box-shadow:rgba(0, 0, 0, .117188) 0 1px 1px inset, rgba(255, 255, 255, .167969) 0 1px 0;display:inline-block;margin-top:3px;max-width:85px;line-height:18px;padding:4px 12px;position:relative}.fb_dialog_content .dialog_header .touchable_button input{border:none;background:none;color:#fff;font:12px Helvetica, sans-serif;font-weight:bold;margin:2px -12px;padding:2px 6px 3px 6px;text-shadow:rgba(0, 30, 84, .296875) 0 -1px 0}.fb_dialog_content .dialog_header .header_center{color:#fff;font-size:17px;font-weight:bold;line-height:18px;text-align:center;vertical-align:middle}.fb_dialog_content .dialog_content{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/y9/r/jKEcVPZFk-2.gif) no-repeat 50% 50%;border:1px solid #555;border-bottom:0;border-top:0;height:150px}.fb_dialog_content .dialog_footer{background:#f6f7f9;border:1px solid #555;border-top-color:#ccc;height:40px}#fb_dialog_loader_close{float:left}.fb_dialog.fb_dialog_mobile .fb_dialog_close_button{text-shadow:rgba(0, 30, 84, .296875) 0 -1px 0}.fb_dialog.fb_dialog_mobile .fb_dialog_close_icon{visibility:hidden}#fb_dialog_loader_spinner{animation:rotateSpinner 1.2s linear infinite;background-color:transparent;background-image:url(https://static.xx.fbcdn.net/rsrc.php/v3/yD/r/t-wz8gw1xG1.png);background-repeat:no-repeat;background-position:50% 50%;height:24px;width:24px}@keyframes rotateSpinner{0%{transform:rotate(0deg)}100%{transform:rotate(360deg)}}
.fb_iframe_widget{display:inline-block;position:relative}.fb_iframe_widget span{display:inline-block;position:relative;text-align:justify}.fb_iframe_widget iframe{position:absolute}.fb_iframe_widget_fluid_desktop,.fb_iframe_widget_fluid_desktop span,.fb_iframe_widget_fluid_desktop iframe{max-width:100%}.fb_iframe_widget_fluid_desktop iframe{min-width:220px;position:relative}.fb_iframe_widget_lift{z-index:1}.fb_hide_iframes iframe{position:relative;left:-10000px}.fb_iframe_widget_loader{position:relative;display:inline-block}.fb_iframe_widget_fluid{display:inline}.fb_iframe_widget_fluid span{width:100%}.fb_iframe_widget_loader iframe{min-height:32px;z-index:2;zoom:1}.fb_iframe_widget_loader .FB_Loader{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/y9/r/jKEcVPZFk-2.gif) no-repeat;height:32px;width:32px;margin-left:-16px;position:absolute;left:50%;z-index:4}
.fb_customer_chat_bounce_in_v1{animation-duration:250ms;animation-name:fb_bounce_in_v1}.fb_customer_chat_bounce_out_v1{animation-duration:250ms;animation-name:fb_bounce_out_v1}.fb_customer_chat_bounce_in_v2{animation-duration:300ms;animation-name:fb_bounce_in_v2;transition-timing-function:ease-in}.fb_customer_chat_bounce_out_v2{animation-duration:300ms;animation-name:fb_bounce_out_v2;transition-timing-function:ease-in}.fb_customer_chat_bounce_in_v2_mobile_chat_started{animation-duration:300ms;animation-name:fb_bounce_in_v2_mobile_chat_started;transition-timing-function:ease-in}.fb_customer_chat_bounce_out_v2_mobile_chat_started{animation-duration:300ms;animation-name:fb_bounce_out_v2_mobile_chat_started;transition-timing-function:ease-in}.fb_customer_chat_bubble_pop_in{animation-duration:250ms;animation-name:fb_customer_chat_bubble_bounce_in_animation}.fb_customer_chat_bubble_animated_no_badge{box-shadow:0 3px 12px rgba(0, 0, 0, .15);transition:box-shadow 150ms linear}.fb_customer_chat_bubble_animated_no_badge:hover{box-shadow:0 5px 24px rgba(0, 0, 0, .3)}.fb_customer_chat_bubble_animated_with_badge{box-shadow:-5px 4px 14px rgba(0, 0, 0, .15);transition:box-shadow 150ms linear}.fb_customer_chat_bubble_animated_with_badge:hover{box-shadow:-5px 8px 24px rgba(0, 0, 0, .2)}.fb_invisible_flow{display:inherit;height:0;overflow-x:hidden;width:0}.fb_mobile_overlay_active{background-color:#fff;height:100%;overflow:hidden;position:fixed;visibility:hidden;width:100%}@keyframes fb_bounce_in_v1{0%{opacity:0;transform:scale(.8, .8);transform-origin:bottom right}80%{opacity:.8;transform:scale(1.03, 1.03)}100%{opacity:1;transform:scale(1, 1)}}@keyframes fb_bounce_in_v2{0%{opacity:0;transform:scale(0, 0);transform-origin:bottom right}50%{transform:scale(1.03, 1.03);transform-origin:bottom right}100%{opacity:1;transform:scale(1, 1);transform-origin:bottom right}}@keyframes fb_bounce_in_v2_mobile_chat_started{0%{opacity:0;top:20px}100%{opacity:1;top:0}}@keyframes fb_bounce_out_v1{from{opacity:1}to{opacity:0}}@keyframes fb_bounce_out_v2{0%{opacity:1;transform:scale(1, 1);transform-origin:bottom right}100%{opacity:0;transform:scale(0, 0);transform-origin:bottom right}}@keyframes fb_bounce_out_v2_mobile_chat_started{0%{opacity:1;top:0}100%{opacity:0;top:20px}}@keyframes fb_customer_chat_bubble_bounce_in_animation{0%{bottom:6pt;opacity:0;transform:scale(0, 0);transform-origin:center}70%{bottom:18pt;opacity:1;transform:scale(1.2, 1.2)}100%{transform:scale(1, 1)}}</style></head>
</head>
<!--網頁背景顏色或圖片設定--
<style type="text/css">
.wrapper {
    background: #ff46433d;
	clip: rect(0px,0px,0px,0px);
	
}
</style>
<!--網頁背景顏色或圖片設定-->
.central_tours {
	background: #fff;
}
<body>
<body data-target="#navbar-spy" data-spy="scroll" style="overflow: hidden;" class="loaded">


<div id="fb-root" class=" fb_reset"><div style="position: absolute; top: -10000px; height: 0px; width: 0px;"><div></div></div><div style="position: absolute; top: -10000px; height: 0px; width: 0px;"><div><iframe name="fb_xdm_frame_http" frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" allow="encrypted-media" id="fb_xdm_frame_http" aria-hidden="true" title="Facebook Cross Domain Communication Frame" tabindex="-1" src="http://staticxx.facebook.com/connect/xd_arbiter/r/mAiQUwlReIP.js?version=42#channel=f24063f4badc648&amp;origin=http%3A%2F%2Fwww.summittour.com.tw" style="border: none;"></iframe><iframe name="fb_xdm_frame_https" frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" allow="encrypted-media" id="fb_xdm_frame_https" aria-hidden="true" title="Facebook Cross Domain Communication Frame" tabindex="-1" src="https://staticxx.facebook.com/connect/xd_arbiter/r/mAiQUwlReIP.js?version=42#channel=f24063f4badc648&amp;origin=http%3A%2F%2Fwww.summittour.com.tw" style="border: none;"></iframe></div></div></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/zh_TW/sdk.js#xfbml=1&version=v2.8&appId=116456551778811";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!-- Loader動畫
<div id="loader-wrapper">
    <div id="loader"></div>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
</div> -->
<!-- Loader動畫 -->
<div class="wrapper">   
  <!--上標-->
  <article class="header gizle">
  


<nav id="top" class="navbar navbar-fixed-top navbar-inverse">
    <div class="container">
        <div class="row">
		
            <div class="navbar-header">
                <button data-toggle="collapse-side" data-target-sidebar=".side-collapse-left" data-target-content=".side-collapse-container-left" type="button" class="navbar-toggle pull-left">
                    <a class="nav-btn" id="nav-open-btn" href="#nav"></a>
                </button>
                <h2 class="block-title">
                    <a href="http://www.summittour.com.tw"><img src="/eWeb_summittour/images/default/logo.png"></a>
                    <!--<a class="navbar-brand" href="#">Design</a>-->
                </h2>
				<div class="navbar-inverse1 side-collapse-right in">
					<nav id="nav1" role="navigation" class="navbar-collapse1">
						<ul class="nav navbar-nav1 navbar-right ">
							<li class="">
								<a href="https://www.facebook.com/summittourTaiwan" target="_blank"><p>粉絲專頁</p></a>
							</li>
							<li class="">
								<a href="/eWeb_summittour/Prefecture/Prefecture01.asp"><p>Line@</p></a>
							</li>
							<li class="">
								<a href="https://line.me/R/ti/p/%40uaj2428d"><p>線上客服</p></a>
							</li>
							<li class="">
								<a href="/eWeb_summittour/page/member.asp"><p>會員專區</p></a>
							</li>
							<li class="">
								<a href="javascript:;"><p>訂閱電子報</p></a>
							</li>
							<li class="">
								<a href="/eweb_summittour/diy/V_diy.asp"><p>客製化旅遊</p></a>
							</li>
							<li class="">
								<a href="/eWeb_summittour/index.asp"><p>回首頁</p></a>
							</li>
							<!--會員登入-->
							
								
							<!--會員登入-END-->
							
						</ul>
					</nav>
				</div>
				
				<div class="navbar-inverse side-collapse-left in">

					<nav id="nav" role="navigation" class="navbar-collapse">
						<ul class="nav navbar-nav navbar-left">
							<!--li><a href="javascript:;">港澳大陸</a></li-->
							<li class="dropdown" onmouseover="dropdown-toggle">
								<a href="/eWeb_summittour/auto/Central_tours_CN.asp" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true">港澳大陸</a>
								<ul class="dropdown-menu" role="menu">	
								  <li><a href="/eWeb_summittour/auto/Central_tours_CN.asp#tab1">東北(大連、瀋陽、哈爾濱、長春、長白山)</a></li>
								  <li><a href="/eWeb_summittour/auto/Central_tours_CN.asp#tab2">山東(濟南、青島、威海、煙台、徐州）</a></li>
								  <li><a href="/eWeb_summittour/auto/Central_tours_CN.asp#tab3">北京、天津、承德</a></li>
								  <li><a href="/eWeb_summittour/auto/Central_tours_CN.asp#tab4">山西(太原)、內蒙(呼和浩特)、銀川	</a></li>
								  <li><a href="/eWeb_summittour/auto/Central_tours_CN.asp#tab5">鄭州、西安</a></li>
								  <li><a href="/eWeb_summittour/auto/Central_tours_CN.asp#tab6">九寨溝、稻城亞丁</a></li>
								  <li><a href="/eWeb_summittour/auto/Central_tours_CN.asp#tab7">長沙、張家界</a></li>
								  <li><a href="/eWeb_summittour/auto/Central_tours_CN.asp#tab8">重慶、武漢、長江三峽</a></li>
								  <li><a href="/eWeb_summittour/auto/Central_tours_CN.asp#tab9">江南、黃山、江西(南昌)</a></li>
								  <li><a href="/eWeb_summittour/auto/Central_tours_CN.asp#tab10">福建(廈門、福州、武夷山)</a></li>
								  <li><a href="/eWeb_summittour/auto/Central_tours_CN.asp#tab11">桂林、南寧</a></li>
								  <li><a href="/eWeb_summittour/auto/Central_tours_CN.asp#tab12">雲南(昆明、大理、麗江、中甸)、貴陽</a></li>
								  <li><a href="/eWeb_summittour/auto/Central_tours_CN.asp#tab13">澳門、珠海、深圳、廣州</a></li>
								  <li><a href="/eWeb_summittour/auto/Central_tours_CN.asp#tab14">海南(海口、三亞)</a></li>
								  <li><a href="/eWeb_summittour/auto/Central_tours_CN.asp#tab15">南北疆、絲路</a></li>
								  <!--li><a href="/eWeb_summittour/auto/Central_tours_CN.asp#tab16">西藏(拉薩、青藏鐵路)</a></li-->
								  <!--li><a href="/eWeb_summittour/auto/Central_tours_CN.asp#tab17">港澳地區</a></li-->
								  <!--li><a href="/eWeb_summittour/auto/Central_tours_CN.asp#tab18">港澳大陸自由行</a></li-->
								</ul>
							</li>
							<li class="dropdown"> 
								<a href="/eWeb_summittour/auto/Central_tours_MY.asp" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true">蒙古</a>
								<ul class="dropdown-menu" role="menu">	
								  <li><a href="/eWeb_summittour/auto/Central_tours_MY.asp#tab1">蒙古國+西伯利亞</a></li>
								  
								</ul>
							</li>
							<li class="dropdown"> 
								<a href="/eWeb_summittour/auto/Central_tours_NSA.asp" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true">東南亞</a>
								<ul class="dropdown-menu" role="menu">	
								  <li><a href="/EW/GO/GroupList.asp?isWm=1&amp;regmCd=0003&amp;regsCd=0003-0001&amp;beginDt=2017/08/22&amp;endDt=2018/12/31&amp;allowJoin=1">泰國(曼谷、清邁、華欣、蘇梅島、普吉島)</a></li>
								  <li><a href="/eWeb_summittour/auto/Central_tours_NSA.asp#tab2">印尼(雅加達、峇里島、日惹、龍目島)</a></li>
								  <li><a href="/eWeb_summittour/auto/Central_tours_NSA.asp#tab3">西馬來西亞(吉隆坡、檳城)、新加坡</a></li>
								  <li><a href="/eWeb_summittour/auto/Central_tours_NSA.asp#tab4">東馬來西亞(沙巴、納閩、沙勞越)、汶萊</a></li>
								  <li><a href="/eWeb_summittour/auto/Central_tours_NSA.asp#tab5">菲律賓(長灘島、宿霧、巴拉望、科隆)</a></li>
								  <!--li><a href="/eWeb_summittour/auto/Central_tours_NSA.asp#tab6">東南亞自由行</a></li-->
								</ul>
							</li>							
							<li class="dropdown">
								<a href="/eWeb_summittour/auto/Central_tours_NEA.asp" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true">東北亞</a>
								<ul class="dropdown-menu" role="menu">	
								  <li><a href="/eWeb_summittour/auto/Central_tours_NEA.asp#tab1">東京(迪士尼、箱根、輕井澤)</a></li>
								  <li><a href="/eWeb_summittour/auto/Central_tours_NEA.asp#tab11">關東(茨城、栃木、群馬、埼玉、千葉、東京都、神奈川)</a></li>
								  <li><a href="/eWeb_summittour/auto/Central_tours_NEA.asp#tab2">關西(大阪、京都、神戶、奈良、南紀)</a></li>
								  <li><a href="/eWeb_summittour/auto/Central_tours_NEA.asp#tab3">四國、中國(道後溫泉、小豆島、廣島、岡山)</a></li>
								  <li><a href="/eWeb_summittour/auto/Central_tours_NEA.asp#tab4">北陸黑部立山(名古屋、富山、小松)</a></li>
								  <li><a href="/eWeb_summittour/auto/Central_tours_NEA.asp#tab5">東北(花卷、仙台、青森、秋田、新潟)</a></li>
								  <li><a href="/eWeb_summittour/auto/Central_tours_NEA.asp#tab6">沖繩(石垣、宮古、與論、琉球、久米島)	</a></li>
								  <li><a href="/eWeb_summittour/auto/Central_tours_NEA.asp#tab7">北海道(札幌、函館、旭川)</a></li>
								  <li><a href="/eWeb_summittour/auto/Central_tours_NEA.asp#tab8">九州(大分、福岡、宮崎、鹿兒島、熊本)</a></li>
								  <li><a href="/EW/GO/GroupList.asp?isWm=1&amp;regmCd=0002&amp;regsCd=0002-0029&amp;beginDt=2017/08/22&amp;endDt=2018/12/31&amp;allowJoin=1">韓國(首爾.釜山.濟州島)</a></li>
								  <!--li><a href="/eWeb_summittour/auto/Central_tours_NEA.asp#tab10">東北亞自由行</a></li-->
								</ul>
							</li>							
							<li class="dropdown">
								<a href="/eWeb_summittour/auto/Central_tours_SA-CA.asp" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true">南亞、中亞非</a>
								<ul class="dropdown-menu" role="menu">	
								  <li><a href="/EW/GO/GroupList.asp?isWm=1&amp;regmCd=0011&amp;regsCd=0011-0002&amp;beginDt=2017/08/22&amp;endDt=2018/12/31&amp;allowJoin=1">印度</a></li>
								  <li><a href="/eWeb_summittour/auto/Central_tours_SA-CA.asp#tab2">尼泊爾、斯里蘭卡</a></li>
								  <!--li><a href="/eWeb_summittour/auto/Central_tours_SA-CA.asp#tab3">土耳其</a></li-->
								  <li><a href="/EW/GO/GroupList.asp?isWm=1&amp;regmCd=0011&amp;regsCd=0011-0004&amp;beginDt=2017/08/22&amp;endDt=2018/12/31&amp;allowJoin=1">杜拜</a></li>
								  <li><a href="/EW/GO/GroupList.asp?isWm=1&amp;regmCd=0011&amp;regsCd=0011-0005&amp;beginDt=2017/08/22&amp;endDt=2018/12/31&amp;allowJoin=1">伊朗、以色列</a></li>
								  <li><a href="/EW/GO/GroupList.asp?isWm=1&amp;regmCd=0011&amp;regsCd=0011-0006&amp;beginDt=2017/08/22&amp;endDt=2018/12/31&amp;allowJoin=1">埃及</a></li>
								  <!--li><a href="/eWeb_summittour/auto/Central_tours_SA-CA.asp#tab7">南非</a></li-->
								  <!--li><a href="/eWeb_summittour/auto/Central_tours_SA-CA.asp#tab8">肯亞</a></li-->
								  <!--li><a href="/eWeb_summittour/auto/Central_tours_SA-CA.asp#tab9">南亞、中亞非自由行</a></li-->
								</ul>
							</li>							
							<li class="dropdown">
								<a href="/eWeb_summittour/auto/Central_tours_EU.asp" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true">歐洲</a>
								<ul class="dropdown-menu" role="menu">
								<li><a href="/EW/GO/GroupList.asp?isWm=1&amp;regmCd=0012&amp;regsCd=0012-0001&amp;beginDt=2018/05/16&amp;endDt=2099/12/31&amp;allowJoin=1">英國</a></li>
								<li><a href="/EW/GO/GroupList.asp?isWm=1&amp;regmCd=0012&amp;regsCd=0012-0002&amp;beginDt=2018/05/16&amp;endDt=2099/12/31&amp;allowJoin=1">法國</a></li>
								<li><a href="/EW/GO/GroupList.asp?regmCd=0012&amp;regsCd=0012-0009&amp;beginDt=2017/08/15&amp;endDt=2099/12/31&amp;allowJoin=1">瑞士</a></li>
								<li><a href="/EW/GO/GroupList.asp?regmCd=0012&amp;regsCd=0012-0006&amp;beginDt=2017/08/15&amp;endDt=2099/12/31&amp;allowJoin=1">德國</a></li>
								<li><a href="/EW/GO/GroupList.asp?isWm=1&amp;regmCd=0012&amp;regsCd=0012-0029&amp;beginDt=2018/05/16&amp;endDt=2099/12/31&amp;allowJoin=1">荷蘭</a></li>
								<li><a href="/EW/GO/GroupList.asp?regmCd=0012&amp;regsCd=0012-0004&amp;beginDt=2017/08/15&amp;endDt=2099/12/31&amp;allowJoin=1">西班牙</a></li>
								<li><a href="/EW/GO/GroupList.asp?regmCd=0012&amp;regsCd=0012-0016&amp;beginDt=2017/08/15&amp;endDt=2099/12/31&amp;allowJoin=1">冰島</a></li> 
								<li><a href="/EW/GO/GroupList.asp?isWm=1&amp;regmCd=0012&amp;regsCd=0012-0022&amp;beginDt=2018/05/16&amp;endDt=2099/12/31&amp;allowJoin=1">義大利</a></li> 
								<li><a href="/EW/GO/GroupList.asp?regmCd=0012&amp;regsCd=0012-0026&amp;beginDt=2017/08/15&amp;endDt=2099/12/31&amp;allowJoin=1">俄羅斯</a></li>
								<li><a href="/EW/GO/GroupList.asp?isWm=1&amp;regmCd=0012&amp;regsCd=0012-0023&amp;beginDt=2018/05/16&amp;endDt=2099/12/31&amp;allowJoin=1">北歐多國(丹麥、芬蘭、冰島、挪威、瑞典)</a></li>
								<li><a href="/EW/GO/GroupList.asp?regmCd=0012&amp;regsCd=0012-0024&amp;beginDt=2017/08/15&amp;endDt=2099/12/31&amp;allowJoin=1">中西歐多國(英.法.荷.比.盧.德.瑞)</a></li>
								<li><a href="/EW/GO/GroupList.asp?isWm=1&amp;regmCd=0012&amp;regsCd=0012-0025&amp;beginDt=2018/05/16&amp;endDt=2099/12/31&amp;allowJoin=1">東歐多國((奧地利.捷克.匈牙利.波蘭)</a></li>
								<li><a href="/EW/GO/GroupList.asp?regmCd=0012&amp;regsCd=0012-0027&amp;beginDt=2017/08/15&amp;endDt=2099/12/31&amp;allowJoin=1">南歐(義大利.西班牙.葡萄牙.南歐.希臘)</a></li>
								<li><a href="/EW/GO/GroupList.asp?isWm=1&amp;regmCd=0012&amp;regsCd=0012-0031&amp;beginDt=2018/05/16&amp;endDt=2099/12/31&amp;allowJoin=1">歐洲巴士循環團</a></li>
								<li><a href="/EW/GO/GroupList.asp?isWm=1&amp;regmCd=0012&amp;regsCd=0012-0032&amp;beginDt=2018/05/16&amp;endDt=2099/12/31&amp;allowJoin=1">歐洲城市周邊一天遊</a></li>
								</ul>
							</li>
							
							<li>
								<a href="/eWeb_summittour/auto/Central_tours_NZ-AU.asp" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true">紐澳</a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="/EW/GO/GroupList.asp?isWm=1&amp;regmCd=0013&amp;regsCd=0013-0001&amp;beginDt=2018/01/09&amp;endDt=2018/12/31&amp;allowJoin=1">雪梨</a></li>
									<li><a href="/EW/GO/GroupList.asp?isWm=1&amp;regmCd=0013&amp;regsCd=0013-0003&amp;beginDt=2018/01/11&amp;endDt=2018/12/31&amp;allowJoin=1">布里斯班</a></li>
									<li><a href="/EW/GO/GroupList.asp?isWm=1&amp;regmCd=0013&amp;regsCd=0013-0009&amp;beginDt=2018/01/11&amp;endDt=2018/12/31&amp;allowJoin=1">紐西蘭南北島</a></li>
									<li><a href="/EW/GO/GroupList.asp?isWm=1&amp;regmCd=0013&amp;regsCd=0013-0010&amp;beginDt=2018/01/11&amp;endDt=2018/12/31&amp;allowJoin=1">紐西蘭南島</a></li>
								</ul>
							</li>
							<li class="dropdown">
								<a href="/eWeb_summittour/auto/Central_tours_US.asp" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true">美加</a>
									<ul class="dropdown-menu" role="menu">	
									<!--li><a href="/eWeb_summittour/auto/Central_tours_US.asp#tab1">洛杉磯</a></li-->
									<!--li><a href="/eWeb_summittour/auto/Central_tours_US.asp#tab2">舊金山</a></li-->
									<li><a href="/EW/GO/GroupList.asp?isWm=1&amp;regmCd=0017&amp;regsCd=0017-0003&amp;beginDt=2018/03/08&amp;endDt=2018/12/31&amp;allowJoin=1&amp;ikeyword=%E5%A4%8F%E5%A8%81%E5%A4%B7">夏威夷</a></li>
									<!--li><a href="/eWeb_summittour/auto/Central_tours_US.asp#tab4">西雅圖</a></li-->
									<!--li><a href="/eWeb_summittour/auto/Central_tours_US.asp#tab5">阿拉斯加</a></li-->
									<!--li><a href="/eWeb_summittour/auto/Central_tours_US.asp#tab6">芝加哥</a></li-->
									<!--li><a href="/eWeb_summittour/auto/Central_tours_US.asp#tab7">溫哥華</a></li-->
									<!--li><a href="/eWeb_summittour/auto/Central_tours_US.asp#tab8">紐約</a></li-->
									<!--li><a href="/eWeb_summittour/auto/Central_tours_US.asp#tab9">多倫多</a></li-->
									<!--li><a href="/eWeb_summittour/auto/Central_tours_US.asp#tab10">休士頓</a></li-->
									<li><a href="/EW/GO/GroupList.asp?regmCd=0017&amp;regsCd=0017-0010&amp;beginDt=2017/08/15&amp;endDt=2099/12/30&amp;allowJoin=1">加拿大(落磯山)</a></li>
									<li><a href="/eWeb_summittour/auto/Central_tours_US.asp#tab12">古巴</a></li>
									<!--li><a href="/eWeb_summittour/auto/Central_tours_US.asp#tab13">北美洲</a></li-->
									<!--li><a href="/eWeb_summittour/auto/Central_tours_US.asp#tab14">中美洲</a></li-->
									<!--li><a href="/eWeb_summittour/auto/Central_tours_US.asp#tab15">南美洲</a></li-->
									<li><a href="/eWeb_summittour/auto/Central_tours_US.asp#tab16">洛杉磯/舊金山</a></li>
									<!--li><a href="/eWeb_summittour/auto/Central_tours_US.asp#tab17">美洲自由行</a></li-->
									</ul>
							</li>
							<!--li class="dropdown">
								<a href="/eWeb_summittour/auto/Central_tours_Islands.asp" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true">海島</a>
								<ul class="dropdown-menu" role="menu">	
								  <!--li><a href="/eWeb_summittour/auto/Central_tours_Islands.asp#tab1">馬爾地夫</a></li-->
								  <!--li><a href="/eWeb_summittour/auto/Central_tours_Islands.asp#tab2">帛琉</a></li>								   
								  <!--li><a href="/eWeb_summittour/auto/Central_tours_Islands.asp#tab3">關島</a></li-->
								  <!--li><a href="/eWeb_summittour/auto/Central_tours_Islands.asp#tab4">海島自由行</a></li-->
								<!--/ul--> 
							<!--/li-->							
							<li class="dropdown">
							<a href="/eWeb_summittour/auto/Central_tours_Indochina.asp" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true">中南半島</a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="/eWeb_summittour/auto/Central_tours_Indochina.asp#tab1">越南(河內、峴港、胡志明、富國島)</a></li>
									<li><a href="/eWeb_summittour/auto/Central_tours_Indochina.asp#tab2">柬埔寨(吳哥窟、金邊、西哈努克)</a></li>
									<li><a href="/eWeb_summittour/auto/Central_tours_Indochina.asp#tab3">寮國</a></li>
									<li><a href="/eWeb_summittour/auto/Central_tours_Indochina.asp#tab4">緬甸(仰光)</a></li>
									<li><a href="/EW/GO/GroupList.asp?regmCd=0010&amp;regsCd=0010-0005&amp;beginDt=2017/08/15&amp;endDt=2018/12/31&amp;allowJoin=1">雙國(越南+高棉)</a></li>
									<!--li><a href="/eWeb_summittour/auto/Central_tours_Indochina.asp#tab6">雙國(北越+廣西)</a></li-->
									<!--li><a href="/eWeb_summittour/auto/Central_tours_Indochina.asp#tab7">中南半島自由行</a></li-->
								</ul>
							</li>
							<li class="dropdown">
								<a href="/eWeb_summittour/auto/Central_tours_TW.asp" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true">國內離島</a>
								<ul class="dropdown-menu" role="menu">	
								  <li><a href="/eWeb_summittour/auto/Central_tours_TW.asp#tab1">金門</a></li>
								  <li><a href="/eWeb_summittour/auto/Central_tours_TW.asp#tab2">澎湖</a></li>
								  <li><a href="/eWeb_summittour/auto/Central_tours_TW.asp#tab3">馬祖</a></li>
								</ul>
							</li>							
							<li>
								<a href="/eWeb_summittour/auto/Central_tours_Cruise.asp" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true">遊輪</a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="/EW/GO/GroupList.asp?isWm=1&amp;regmCd=0019&amp;regsCd=0019-0001&amp;beginDt=2018/01/11&amp;endDt=2018/12/31&amp;allowJoin=1">嘉年華遊輪</a></li>
									<li><a href="/EW/GO/GroupList.asp?isWm=1&amp;regmCd=0019&amp;regsCd=0019-0012&amp;beginDt=2018/01/11&amp;endDt=2018/12/31&amp;allowJoin=1">基隆港出發系列</a></li>
									<li><a href="/EW/GO/GroupList.asp?isWm=1&amp;regmCd=0019&amp;regsCd=0019-0013&amp;beginDt=2018/01/11&amp;endDt=2018/12/31&amp;allowJoin=1">加勒比海‧墨西哥‧新英格蘭</a></li>
									<li><a href="/EW/GO/GroupList.asp?isWm=1&amp;regmCd=0019&amp;regsCd=0019-0014&amp;beginDt=2018/01/11&amp;endDt=2018/12/31&amp;allowJoin=1">阿拉斯加冰河灣</a></li>
									<li><a href="/EW/GO/GroupList.asp?isWm=1&amp;regmCd=0019&amp;regsCd=0019-0015&amp;beginDt=2018/01/11&amp;endDt=2018/12/31&amp;allowJoin=1">地中海．希臘</a></li>
									<li><a href="/EW/GO/GroupList.asp?isWm=1&amp;regmCd=0019&amp;regsCd=0019-0016&amp;beginDt=2018/01/11&amp;endDt=2018/12/31&amp;allowJoin=1">地中海短天數</a></li>
									<li><a href="/EW/GO/GroupList.asp?isWm=1&amp;regmCd=0019&amp;regsCd=0019-0017&amp;beginDt=2018/01/11&amp;endDt=2018/12/31&amp;allowJoin=1">北歐．英國列島</a></li>
									<li><a href="/EW/GO/GroupList.asp?isWm=1&amp;regmCd=0019&amp;regsCd=0019-0018&amp;beginDt=2018/01/11&amp;endDt=2018/12/31&amp;allowJoin=1">東北亞．庫頁島（中．韓．日．俄）</a></li>
									<li><a href="/EW/GO/GroupList.asp?isWm=1&amp;regmCd=0019&amp;regsCd=0019-0020&amp;beginDt=2018/01/11&amp;endDt=2018/12/31&amp;allowJoin=1">美洲遊輪（墨西哥．加勒比海）</a></li>
									<li><a href="/EW/GO/GroupList.asp?isWm=1&amp;regmCd=0019&amp;regsCd=0019-0021&amp;beginDt=2018/01/11&amp;endDt=2018/12/31&amp;allowJoin=1">加勒比海‧美墨遊輪</a></li>
									<li><a href="/EW/GO/GroupList.asp?isWm=1&amp;regmCd=0019&amp;regsCd=0019-0022&amp;beginDt=2018/01/11&amp;endDt=2018/12/31&amp;allowJoin=1">2018《太陽公主號》夏季航次</a></li>
									<li><a href="/EW/GO/GroupList.asp?isWm=1&amp;regmCd=0019&amp;regsCd=0019-0023&amp;beginDt=2018/01/11&amp;endDt=2018/12/31&amp;allowJoin=1">2018公主號遊輪基隆港出發</a></li>
									<li><a href="/EW/GO/GroupList.asp?isWm=1&amp;regmCd=0019&amp;regsCd=0019-0024&amp;beginDt=2018/01/11&amp;endDt=2018/12/31&amp;allowJoin=1">杜拜、阿曼王國</a></li>
									<li><a href="/EW/GO/GroupList.asp?isWm=1&amp;regmCd=0019&amp;regsCd=0019-0025&amp;beginDt=2018/01/11&amp;endDt=2018/12/31&amp;allowJoin=1">2018《盛世公主號》基隆港系列</a></li>
									<li><a href="/EW/GO/GroupList.asp?isWm=1&amp;regmCd=0019&amp;regsCd=0019-0026&amp;beginDt=2018/01/11&amp;endDt=2018/12/31&amp;allowJoin=1">2018《太陽公主號》夏季基隆港出發</a></li>
									<li><a href="/EW/GO/GroupList.asp?isWm=1&amp;regmCd=0021&amp;regsCd=0021-0002&amp;beginDt=2018/01/11&amp;endDt=2018/12/31&amp;allowJoin=1">美洲遊輪（墨西哥．加勒比海）</a></li>
									<!--li><a href="/EW/GO/GroupList.asp?regmCd=0005&regsCd=0005-0005&beginDt=2017/08/15&endDt=2018/12/31&allowJoin=1">公主遊輪</a></li-->
									<!--li><a href="/eWeb_summittour/auto/Central_tours_Cruise.asp#tab11">歌斯達郵輪</a></li-->
									<!--li><a href="/EW/GO/GroupList.asp?regmCd=0005&regsCd=0005-0007&beginDt=2017/08/17&endDt=2018/12/31&allowJoin=1">美洲遊輪</a></li-->								
								</ul>
							</li>
							
							<li>
								<a href="/eWeb_summittour/auto/Central_tours_RMQ.asp"><!--class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true"-->台中出發</a>
								<!--ul class="dropdown-menu" role="menu">
									<li><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0016&portCd=RMQ&allowJoin=1">港澳大陸</a></li>
									<!--li><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0003&portCd=RMQ&allowJoin=1">東南亞</a></li>
									<li><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0002&portCd=RMQ&allowJoin=1">東北亞</a></li>
									<!--li><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0011&portCd=RMQ&allowJoin=1">南亞、中亞非</a></li-->
									<!--li><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0012&portCd=RMQ&allowJoin=1">歐洲</a></li-->
									<!--li><a href="/eWeb_summittour/auto/Central_tours_RMQ.asp#tab6">紐澳</a></li-->
									<!--li><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0017&portCd=RMQ&allowJoin=1">美加</a></li-->
									<!--li><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0018&portCd=RMQ&allowJoin=1">海島</a></li-->
									<!--li><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0010&portCd=RMQ&allowJoin=1">中南半島</a></li--> 
									<!--li><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0004&portCd=RMQ&allowJoin=1">國內離島</a></li-->
									<!--li><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0005&portCd=RMQ&allowJoin=1">郵輪</a></li-->
								<!--/ul-->
							</li>
							
							<li class="hidden-lg">
								<a href="https://www.facebook.com/summittourTaiwan" target="_blank"><p>粉絲專頁</p></a>
							</li>
							<li class="hidden-lg">
								<a href="/eWeb_summittour/Prefecture/Prefecture01.asp"><p>Line@</p></a>
							</li>
							<li class="hidden-lg">
								<a href="https://line.me/R/ti/p/%40uaj2428d"><p>線上客服</p></a>
							</li>
							<li class="hidden-lg">
								<a href="/eWeb_summittour/page/member.asp"><p>會員專區</p></a>
							</li>
							<li class="hidden-lg">
								<a href="javascript:;"><p>訂閱電子報</p></a>
							</li>
							<li class="hidden-lg">
								<a href="/eweb_summittour/diy/V_diy.asp"><p>客製化旅遊</p></a>
							</li>
							<li class="hidden-lg">
								<a href="/eWeb_summittour/index.asp"><p>回首頁</p></a>
							</li>
							<!--會員登入-->
							
							<!--會員登入-END-->
													
							
						</ul>
					</nav>
				</div>
				
				<!--button data-toggle="collapse-side1" data-target-sidebar1=".side-collapse-right" data-target-content1=".side-collapse-container-right" type="button" class="navbar-toggle1 pull-right">
					<a class="nav-btn" id="nav-open-btn" href="#nav1"></a>
				</button-->
          </div>
        </div>
      </div>
</nav>
    
<!-- Modal -->
<div class="modal fade bs-example-modal-sm" id="myMember" tabindex="-1" role="dialog" aria-labelledby="myMemberLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        <h4 class="modal-title" id="myMemberLabel">會員中心</h4>
      </div>
      <div class="modal-body">
        <!--  20050721 JULIA : 增加當USR_ID為CFCB時, 需顯示合作契約文字及鏈結
 //-->
<meta http-equiv="content-type" content="text/html; charset=UTF-8">

<link href="/css/fly.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
body {
	margin-top: 0px;
}
-->
</style>
     <table width="231" height="129" border="0" cellspacing="0" cellpadding="0" background="/eWeb_fly/images/top_images/left-member.jpg">
       <tbody><tr align="center">
         <td height="30" colspan="2" align="center" class="left-input-text01"> </td>
       </tr>
       
       <form name="login" method="post" action="/eWeb/Main/O_chkLogin.asp" target="_top"></form>
       <tr> 
         <td colspan="3" align="center" class="left-input-text01">登入帳號　<input name="login" type="text" class="left-input-box" size="12" maxlength="12"></td>
       </tr>
       <tr> 
         <td colspan="3" align="center" class="left-input-text01">登入密碼　<input name="pwd" type="password" class="left-input-box" size="12" maxlength="10"></td>
       </tr>
       <tr>
        <td>
         <table width="140" border="0" cellspacing="0" cellpadding="0">
          <tbody><tr>
            <td align="right" colspan="6" class="right_login02"><a href="/eWeb/Member/Member.asp?inc=regist" target="_top">&nbsp;加入會員</a></td>
          </tr>
          <tr>
            <td align="right" colspan="6" class="right_login02"><a href="/eWeb/Member/Member.asp?inc=pwrequest" target="_top">&nbsp;忘記密碼</a></td>
          </tr>
         </tbody></table>
        </td>
        <td align="left">
          <table width="65" border="0" cellspacing="0" cellpadding="0">
           <tbody><tr>
              <td valign="bottom"><a href="/eWeb/Member/Member.asp?inc=regist" target="_top"></a>
                <input name="returl1" type="hidden" value="/eWeb/main/home.asp">
                <input name="returl2" type="hidden" value="/eWeb/main/home.asp">
                <button name="Submit" type="submit" hidefocus="true" class="left-input-button" style="CURSOR: hand"> 
                 <table width="44" height="27" border="0" align="right" cellpadding="0" cellspacing="0" background="/eWeb_fly/images/top_images/member-btn.png">
                   <tbody><tr> 
                    <td width="44"> </td>
                   </tr>
                 </tbody></table>
               </button></td>
             </tr>
           </tbody></table>
        </td>
       </tr>
       <tr> 
         <td align="right"> </td>
       </tr>
       
     </tbody></table>
     
      </div>
    </div>
  </div>
</div>


  </article>
  
    <article class="container Type">
        <div class="row">
		
            <!--麵包屑及頁面標題-->
            <ol class="breadcrumb">
                <li>目前位置：</li>
                <li><a href="/">首頁</a></li>
                <li><a href="/EW/GO/GroupList.asp?regmCd=0016">港澳大陸</a></li>
                <li class="active"><a href="/EW/GO/GroupList.asp?regmCd=0016&amp;regsCd=0016-0042">江南、黃山、江西(南昌)</a></li>
            </ol>
            <!-- 個團明細頁 START -->
            <section id="Main" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 product_types">
                <div class="row">
                    <!-- 產品名稱 START -->
                    <h3>海棠風情─潑墨黃山臥虎藏龍宏村屯溪老街徽州古城五日</h3>
                    <!-- 產品名稱 END -->
					
                    <!-- 產品基本資料  START-->
					<div class="product_basic_info animatedParent article">
                        <ul class="animated fadeInUpShort slow">
							
                            <li class="col-xs-12 col-sm-12 col-md-1 col-lg-1 basic_info_title"><i class="fa fa-usd" aria-hidden="true"></i>售價：</li>
                            <li class="col-xs-12 col-sm-12 col-md-2 col-lg-2 price_content">NT$23,500<q>起</q></li>
                            
							<li class="col-xs-12 col-sm-12 col-md-1 col-lg-1 basic_info_title">
                                <label><i class="fa fa-clock-o" aria-hidden="true"></i>旅遊天數：</label>
                            </li>
                            <li class="col-xs-12 col-sm-12 col-md-2 col-lg-2 return_date">
                                5天4夜
                            </li>
							<li class="col-xs-12 col-sm-12 col-md-6 col-lg-6 sign_up_group">
                                <a href="/EW/GO/GroupList.asp?mGrupCd=TXNMUT5A&amp;beginDt=2018/07/17" class="btn btn-primary btn-lg other_date"><i class="fa fa-calendar" aria-hidden="true"></i>選擇出發日期</a>
                            </li>
                        </ul>
                        <ul class="animated fadeInUpShort slow">
							
                        </ul>
						
                        <ul class="animated fadeInUpShort slow">
                            <li class="col-xs-12 col-sm-12 col-md-1 col-lg-1 basic_info_title"><i class="fa fa-check-square" aria-hidden="true"></i>包含項目：</li>
                            <li class="col-xs-12 col-sm-12 col-md-11 col-lg-11">含團險,含國內外機場稅,含港口稅</li>
                            <li class="col-xs-12 col-sm-12 col-md-1 col-lg-1 basic_info_title"><i class="fa fa-window-close-o" aria-hidden="true"></i>不含項目：</li>
                            <li class="col-xs-12 col-sm-12 col-md-11 col-lg-11">不含小費,不含行李小費,不含接送費,不含簽證費用</li>
                        </ul>
                    </div>
					
                    <!-- 產品內容選項 BAR START -->
                    <div id="navbar-spy" class="nav navbar-default onnavbar">
                        <div id="cart" class="container-fluid" style="position: fixed; top: 56px;">
                            <div class="collapse navbar-collapse" id="myNavbar">
                                <div class="row">
                                <ul class="nav navbar-nav">
										<li class="active"><a href="#TravelCharacteristics" id="GOTravelCharacteristics">行程特色</a></li>							<li><a href="#ReferenceFlights" id="GOReferenceFlights">航班參考</a></li>									<li><a href="#DailyItinerary" id="GODailyItinerary">每日行程</a></li>									<li><a href="#OtherInstructions" id="GOOtherInstructions">其他說明</a></li>			
                                    <!--<li><a href="#TipsAndMore">小費及其他</a></li>-->
                                </ul>
                                <ul class="navbar-other">
                                    <li>
                                        <a title="分享到FB" target="_blank" class="btn-floating fb_icon" href="http://www.facebook.com/share.php?u=http://www.summittour.com.tw/ew/go/mgroupdetail.asp?prodcd=txnmut5a&amp;t=海棠風情─潑墨黃山臥虎藏龍宏村屯溪老街徽州古城五日">
                                            <i class="fa fa-facebook-square" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                    <li>
										<a title="分享到LINE好友" class="btn-floating line_icon">
											<iframe data-lang="zh_Hant" data-type="share-c" data-url="http://www.summittour.com.tw/ew/go/mgroupdetail.asp?prodcd=txnmut5a" data-line-it-id="0" scrolling="no" frameborder="0" allowtransparency="true" class="line-it-button" src="https://social-plugins.line.me/widget/share?url=http%3A%2F%2Fwww.summittour.com.tw%2Few%2Fgo%2Fmgroupdetail.asp%3Fprodcd%3Dtxnmut5a&amp;buttonType=share-c&amp;lang=zh_Hant&amp;type=share&amp;id=0&amp;origin=http%3A%2F%2Fwww.summittour.com.tw%2FEW%2FGO%2FMGroupDetail.asp%3FprodCd%3DTXNMUT5A&amp;title=%E6%B5%B7%E6%A3%A0%E9%A2%A8%E6%83%85%E2%94%80%E6%BD%91%E5%A2%A8%E9%BB%83%E5%B1%B1%E8%87%A5%E8%99%8E%E8%97%8F%E9%BE%8D%E5%AE%8F%E6%9D%91%E5%B1%AF%E6%BA%AA%E8%80%81%E8%A1%97%E5%BE%BD%E5%B7%9E%E5%8F%A4%E5%9F%8E%E4%BA%94%E6%97%A5-%E8%B6%85%E6%82%A0%E6%97%85%E8%A1%8C%E7%A4%BE-%E9%A0%82%E5%B0%96%E6%97%85%E9%81%8A" style="width: 31px; height: 30px; visibility: visible; position: static;" title="可將此頁面的資訊分享至LINE。"></iframe>
										</a>
                                    </li>
									
                                    <li>
                                        <a href="" class="btn-floating download_info" title="個團資料列印、下載、轉寄" data-toggle="modal" data-target="#mydownload">
                                            <i class="fa fa-download" aria-hidden="true"></i>
                                        </a>
                                    </li>
									
                                    <li>
                                        <a href="" class="btn btn-default customer_service" title="洽詢客服人員" data-toggle="modal" data-target="#mycontact">
                                            <span class="glyphicon glyphicon-headphones" aria-hidden="true"></span>洽詢
                                        </a>
                                    </li>
                                    <li>
                                        <a href="" class=""></a>
                                    </li>
                                </ul>
                                </div>
                            </div>
                        </div>
                    </div>   
                    <!-- 產品內容選項 BAR END -->
					
                    <!-- 產品大圖輪播 START [目前沒有輪播功能] -->
                    <div id="carousel-example-generic" class="carousel slide animatedParent article" data-ride="carousel">
                        <!-- 張數按鈕，可點選按鈕，隨張數增加 -->
                        <ol class="carousel-indicators animated fadeInUpShort slow">
                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                            <!--li data-target="#carousel-example-generic" data-slide-to="1"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="2"></li--->
                        </ol>
                        <!-- 圖片架構，含文字敘述 -->
                        <div class="carousel-inner animated fadeInUpShort slow" role="listbox">
							
                            <div class="item active">
                                <img src="/eWeb_summittour/IMGDB/000060/000258/000259/00015965.JPG" alt="">
                                <div class="carousel-caption"><!--圖片文字說明--></div>
                            </div>
                            <!--div class="item">
                                <img src="/eweb_summittour/images/VGD_img_02.jpg" alt="...">
                                <div class="carousel-caption">
                                    抓中台輸入文字的欄位
                                </div>
                            </div>
                            <div class="item">
                                <img src="/eweb_summittour/images/VGD_img_03.jpg" alt="...">
                                <div class="carousel-caption">
                                    抓中台輸入文字的欄位
                                </div>
                            </div-->
							
                        </div>
                        <!-- 左右箭頭按鈕 -->
                        <!--a class="left carousel-control animated fadeInUpShort slow" href="#carousel-example-generic" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control animated fadeInUpShort slow" href="#carousel-example-generic" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                            <span class="sr-only">Next</span>
                        </a-->
                    </div>
                    <!-- 產品大圖輪播 END -->
                    <!-- 產品內容 STRAT -->
                    <div class="product_content">
						
                        <!-- 行程說明、特別安排 START -->
                        <div id="TravelCharacteristics" class="TravelCharacteristics animatedParent article">
                            <h4 class="animated fadeInUpShort slow go"><i class="fa fa-star" aria-hidden="true"></i>行程特色</h4>
                            <div class="features_content animated fadeInUpShort slow go">
								<style type="text/css">
								</style>
								<div class="Title_1">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 item0">
								<h2 style="font-size:36px;text-align:center;">銷售特色</h2>
								
								</div>
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 item0">
								<hr style="margin-top: 5px;margin-bottom: 3px;border-top: 2px solid #83b53c;width: 100%;" /></div>
								</div>
								<style type="text/css">
								</style>
								<div class="Title_2">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 item0">
								<h2 style="font-size:26px;text-align:center;">特別贈送</h2>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 item0">
								<hr style="margin-top: 2px;margin-bottom: 3px;border-top: 2px solid #3333;width: 100%;" /></div>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 item"><h2><span style="color:#FF0000;">
								每人每天一瓶礦泉水</span></h2></div>
								<br>
								<style type="text/css">
								.Title_1 h2{font-size:36px;text-align:center;}
								.Title_1 hr{margin-top: 5px;margin-bottom: 3px;border-top: 2px solid #83b53c;width: 100%;}
								</style>
								<div class="Title_1">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 item0">
								<h2 style="font-size:36px;text-align:center;">景點介紹</h2>
								</div>

								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 item0">
								<hr style="margin-top: 5px;margin-bottom: 3px;border-top: 2px solid #83b53c;width: 100%;"/></div>
								</div>
<style>
/*添加淡入淡出的css*/
.carousel-indicators{display:none;}
.carousel-fade .carousel-inner .item{ opacity:0; -webkit-transition-property:opacity;-moz-transition-property:opacity ; -ms-transition-property:opacity;-o-transition-property:opacity;transition-property:opacity ;}
.carousel-fade .carousel-inner .active{ opacity: 1;}
.carousel-fade .carousel-inner .active.left,.carousel-fade .carousel-inner .active.right{left: 0;opacity: 0;}
.carousel-fade .carousel-inner .next.left,
.carousel-fade .carousel-inner .prev.right {opacity: 1;}
</style>
<div class="day_style_9">
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 item">
<div id="myCarousel" class="carousel slide carousel-fade" data-ride="carousel" data-interval="5000">
<!--div id="myCarousel" class="carousel slide" data-ride="carousel"--> 
<!-- Indicators -->
<ol class="carousel-indicators" style="width: 100%;">
<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
<li data-target="#myCarousel" data-slide-to="1" class=""></li>
<li data-target="#myCarousel" data-slide-to="2" class=""></li>
<li data-target="#myCarousel" data-slide-to="3" class=""></li>
<li data-target="#myCarousel" data-slide-to="4" class=""></li>
</ol>
<!-- Wrapper for slides -->
<div class="carousel-inner" role="listbox">
<div class="item active">
<img  src="https://www.bwt.com.tw/eWeb_bestway/IMGDB/001165/002734/00062238.jpg"  width="100%"/>
</div>
<div class="item">
<img  src="https://www.bwt.com.tw/eWeb_bestway/IMGDB/001165/002734/00062240.jpg"  width="100%"/>
</div>
<div class="item">
<img  src="https://www.bwt.com.tw/eWeb_bestway/IMGDB/001165/002734/00062239.jpg"  width="100%"/>
</div>
<div class="item">
<img  src="https://www.bwt.com.tw/eWeb_bestway/IMGDB/001165/002734/00062241.jpg"  width="100%"/>
</div>
</div>
</div>
</div>
</div>
<style type="text/css">
#day_style_2 ul li{ width:100%; overflow:hidden; float:left; margin:0px 10px 10px 0px; position:relative;}
#day_style_2 ul li .text{ width:100%; height:initial; overflow:hidden; position:absolute; left:0; bottom:5px; background:rgba(0,0,0,0.8); font-size:15px; color:#fff;}
#day_style_2 ul li .text p{text-align:left; color:#fff; line-height:180%; padding:5px 10px; clear:both}
#day_style_2 ul li .text h2{font-size:26px; display:block; padding:10px 10px; display:block; float:left; margin-bottom:5px;}
</style>
<div id="day_style_2">
<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 daily_box">
<div class="img_box">
<ul>
<li>
<h1 style="top: -10px;">湖邊古村落</h1>
<a href=" "><img class="img-thumbnail" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000060/000258/001154/00014977.JPG" style="margin-top:-60px;" width="650" /></a>
<div class="text" style="height: 0%;">
<h2>湖邊古村落</h2>
<p>具有濃鬱徽州傳統特色的湖邊古村落，環境的幽靜秀美。體現了"綠樹村邊合，青山郭外斜"的典雅意境，再現徽州古村落的完美形態。</p>
</div>
</li>
</ul>
</div>
</div>



<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 daily_box">
<div class="img_box">
<ul>
<li>
<h1 style="top: -10px;">宏村</h1>
<a href=" "><img class="img-thumbnail" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000060/000258/001154/00013643.JPG" style="margin-top:-60px;" width="650" /></a>
<div class="text" style="height: 0%;">
<h2>宏村</h2>
<p>是電影《臥虎藏龍》外景拍攝場地。自然景觀與人文內涵交相輝映，是宏村區別于其他古民居建築佈局的特色，品味極高，全村現保存完好的明清古民居有140餘幢，被譽為當今世界歷史《文化遺產》的一大奇跡。</p>
</div>
</li>
</ul>
</div>
</div>
</div>
<script src="/eWeb_summittour/js/jquery.mask.js"></script><script>$(function(){$('#day_style_2 li').hover(function(){$('.text',this).stop().animate({height:'96%'});},function(){$('.text',this).stop().animate({height:'0px'});});});</script>

<style type="text/css">
#day_style_3 ul li{ width:100%; overflow:hidden; float:left; margin:0px 10px 10px 0px; position:relative;}
#day_style_3 ul li .text{ width:100%; height:initial; overflow:hidden; position:absolute; left:0; bottom:0px; background:rgba(0,0,0,0.8); font-size:15px; color:#fff;}
#day_style_3 ul li .text p{text-align:left; color:#fff; line-height:180%; padding:5px 10px; clear:both}
#day_style_3 ul li .text h2{font-size:26px; display:block; padding:10px 10px; display:block; float:left; margin-bottom:5px;} 
</style>
<div id="day_style_3">
<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 daily_box">
<div class="img_box">
<ul>
<li>
<h1 style="top: -10px;">屯溪老街</h1>
<a href=" "><img class="img-thumbnail" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000060/000258/001154/00013637.JPG" style="margin-top:-60px;" width="100%" height="270px" /></a>
<div class="text" style="height: 0%;">
<h2>屯溪老街</h2>
<p>起源于宋代，距今有千餘年歷史。老街全長約一公里，兩旁有大量保存完好的明、清時代的徽派雕花建築。屯溪老街有古玩店、玉器店、字畫齋、文房四寶鋪、徽派餐廳、食品店，古意盎然。</p>
</div>
</li>
</ul>
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 daily_box">
<div class="img_box">
<ul>
<li>
<h1 style="top: -10px;">漁梁壩</h1>
<a href=" "><img class="img-thumbnail" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000060/000258/001154/00015954.JPG" style="margin-top:-60px;" width="100%"  height="270px"/></a>
<div class="text" style="height: 0%;">
<h2>漁梁壩</h2>
<p>是新安江上游最古老、規模最大的古代攔河壩，有《東南都江堰》之稱的宋代水利工程。</p>
</div> 
</li>
</ul>
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 daily_box">
<div class="img_box">
<ul> 
<li>
<h1 style="top: -10px;">黃山</h1>
<a href=" "><img class="img-thumbnail" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000060/000258/001154/00014816.JPG" style="margin-top:-60px;" width="100%" height="270px" /></a>
<div class="text" style="height: 0%;">
<h2>黃山</h2>
<p>是著名的世界文化與自然遺產，已被列入《世界遺產名錄》，素有《五嶽歸來不看山，黃山歸來不看嶽》，《天下第一奇山》之稱。並與長江、長城、黃河並稱為中華民族的象徵之一。於一塊平坦岩石上，另人驚嘆不已，遊人站在平臺邊緣上憑欄覽勝對面的《雙剪峰》及《雙筍峰》，就像一幅潑墨的山水畫。</p>
</div>
</li>
</ul>
</div>
</div>
</div>
<script src="/eWeb_summittour/js/jquery.mask.js"></script><script>$(function(){$('#day_style_3 li').hover(function(){$('.text',this).stop().animate({height:'96%'});},function(){$('.text',this).stop().animate({height:'0px'});});});</script>
<div class="day_style_1">
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 item">
<h2 style="color: #FF0000;">特別說明</h2>
<p>請輸入圖片文字介紹或敘述</p>
<h2 style="color: #FF0000;">貼心提醒</h2>
<p style="color: #FF0000;">請輸入圖片文字介紹或敘述</p>
<p style="color: #FF0000;">備註:以下所有景點圖片本公司景點圖片僅供參考，因景點季節不同景色稍許會改變 敬請見諒!</p>
</div>
</div>
							</div>
                        </div>
                        <!-- 行程說明、特別安排 END -->
						
                        <!-- 航班參考 START -->
                        <div id="ReferenceFlights" class="ReferenceFlights animatedParent article">
                            <h4 class="animated fadeInUpShort slow"><i class="fa fa-plane" aria-hidden="true"></i>航班參考</h4>
                           <div>* 以下為本行程預訂的航空班機及飛航路線，實際航班以團體確認的航班編號與飛行時間為準。</div>
<div>* 因應國際油價波動，航空公司隨機票所增收燃油附加費用，會隨國際油價而有所調整。</div>

                            <div class="flight_box animated fadeInUpShort slow">
                                
                                <ul class="flight_title">
                                    <li>天數</li>
                                    <li>航空公司</li>
                                    <li>航班</li>
                                    <li>出發地</li>
                                    <li>起飛時間</li>
                                    <li>目的地</li>
                                    <li>抵達時間</li>
                                </ul>
								
                                <ul class="flight_content">
                                    <li>1</li>
                                    <li>中國東方航空</li>
                                    <li>MU2096</li>
                                    <li>台灣桃園國際機場</li>
                                    <li> 12:10</li>
                                    <li>黃山市 ~機場</li>
                                    <li> 14:05</li>
                                </ul>
								
                                <ul class="flight_content">
                                    <li>5</li>
                                    <li>中國東方航空</li>
                                    <li>MU2095</li>
                                    <li>黃山市 ~機場</li>
                                    <li> 15:20</li>
                                    <li>台灣桃園國際機場</li>
                                    <li> 17:20</li>
                                </ul>
								
                            </div>
                        </div>
                        <!-- 航班參考 END -->
						
                        <!-- 每日行程 START -->
                        <div id="DailyItinerary" class="DailyItinerary animatedParent article">
                            <h4 class="animated fadeInUpShort slow">
                                <i class="fa fa-map-o" aria-hidden="true"></i>每日行程
								
                                <!-- 全部展開/收合按鈕 -->
                                <a class="btn btn-default dark_pile_btn active"></a>
                            </h4>
                            <div class="panel-group animated fadeInUpShort slowest" id="DailyItinerary_accordion" role="tablist" aria-multiselectable="true">
                                
								<div id="day1" class="panel panel-default every_day">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 panel-heading day_title" role="tab" id="DailyItinerary_heading_1">
                                        <div class="panel-title">
                                            <a role="button" data-toggle="collapse" data-parent="#DailyItinerary_accordion" href="#DailyItinerary_collapse_1" aria-expanded="true" aria-controls="DailyItinerary_collapse_1">
                                                <h4 class="col-xs-12 col-sm-12 col-md-1 col-lg-1 day_title_left"><span class="en_day">D1</span><span class="tw_day">第1天</span></h4>
                                                <h4 class="col-xs-12 col-sm-12 col-md-11 col-lg-11 day_title_right">桃園/屯溪【黎陽IN巷、文峰橋】</h4>
                                            </a>
                                        </div>
                                    </div>
                                    
                                    <div id="DailyItinerary_collapse_1" class="panel-collapse collapse dark_pile in" role="tabpanel" aria-labelledby="DailyItinerary_heading_1" aria-expanded="true">
                                        <div class="panel-body">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 day_content">
                                                
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 day_meal">
                                                <i class="fa fa-cutlery" aria-hidden="true"></i>
                                                <div class="meal_content">
                                                    <dl class="dl-horizontal">
                                                        <dt>早餐：</dt>
                                                        <dd>-- 敬請自理</dd>
                                                        <dt>午餐：</dt>
                                                        <dd>-- 機上餐點</dd>
                                                        <dt>晚餐：</dt>
                                                        <dd>-- 味道食府50</dd>
                                                    </dl>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 day_hotel">
                                                
													<i class="fa fa-bed" aria-hidden="true"></i>
													<p>天都國際酒店 或同級</p>
												
                                            </div>
                                        </div>
                                    </div>
                                </div>
								
								<div id="day2" class="panel panel-default every_day">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 panel-heading day_title" role="tab" id="DailyItinerary_heading_2">
                                        <div class="panel-title">
                                            <a role="button" data-toggle="collapse" data-parent="#DailyItinerary_accordion" href="#DailyItinerary_collapse_2" aria-expanded="false" aria-controls="DailyItinerary_collapse_2" class="collapsed">
                                                <h4 class="col-xs-12 col-sm-12 col-md-1 col-lg-1 day_title_left"><span class="en_day">D2</span><span class="tw_day">第2天</span></h4>
                                                <h4 class="col-xs-12 col-sm-12 col-md-11 col-lg-11 day_title_right">屯溪→黟縣【宏村、賽金花故居】→黃山【雲谷纜車上山、始信峰風景區、北海景區、夢筆生花、清凉台、猴子觀海】</h4>
                                            </a>
                                        </div>
                                    </div>
                                    
                                    <div id="DailyItinerary_collapse_2" class="panel-collapse collapse dark_pile in" role="tabpanel" aria-labelledby="DailyItinerary_heading_2" aria-expanded="false">
                                        <div class="panel-body">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 day_content">
                                                
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 day_meal">
                                                <i class="fa fa-cutlery" aria-hidden="true"></i>
                                                <div class="meal_content">
                                                    <dl class="dl-horizontal">
                                                        <dt>早餐：</dt>
                                                        <dd>-- 酒店內享用</dd>
                                                        <dt>午餐：</dt>
                                                        <dd>-- 農家宴50</dd>
                                                        <dt>晚餐：</dt>
                                                        <dd>-- 酒店合菜90</dd>
                                                    </dl>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 day_hotel">
                                                
													<i class="fa fa-bed" aria-hidden="true"></i>
													<p>獅林大酒店 或同級</p>
												
                                            </div>
                                        </div>
                                    </div>
                                </div>
								
								<div id="day3" class="panel panel-default every_day">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 panel-heading day_title" role="tab" id="DailyItinerary_heading_3">
                                        <div class="panel-title">
                                            <a role="button" data-toggle="collapse" data-parent="#DailyItinerary_accordion" href="#DailyItinerary_collapse_3" aria-expanded="false" aria-controls="DailyItinerary_collapse_3" class="collapsed">
                                                <h4 class="col-xs-12 col-sm-12 col-md-1 col-lg-1 day_title_left"><span class="en_day">D3</span><span class="tw_day">第3天</span></h4>
                                                <h4 class="col-xs-12 col-sm-12 col-md-11 col-lg-11 day_title_right">黃山【觀賞日出(視當天天氣狀況)、西海景區、排雲亭、西海大峡谷(單程地軌纜車)、光明頂、飛来石、雲穀纜車下山】→屯溪</h4>
                                            </a>
                                        </div>
                                    </div>
                                    
                                    <div id="DailyItinerary_collapse_3" class="panel-collapse collapse dark_pile in" role="tabpanel" aria-labelledby="DailyItinerary_heading_3" aria-expanded="false">
                                        <div class="panel-body">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 day_content">
                                                
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 day_meal">
                                                <i class="fa fa-cutlery" aria-hidden="true"></i>
                                                <div class="meal_content">
                                                    <dl class="dl-horizontal">
                                                        <dt>早餐：</dt>
                                                        <dd>-- 酒店內享用</dd>
                                                        <dt>午餐：</dt>
                                                        <dd>-- 酒店合菜70</dd>
                                                        <dt>晚餐：</dt>
                                                        <dd>-- 中式合菜50</dd>
                                                    </dl>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 day_hotel">
                                                
													<i class="fa fa-bed" aria-hidden="true"></i>
													<p>天都國際酒店 或同級</p>
												
                                            </div>
                                        </div>
                                    </div>
                                </div>
								
								<div id="day4" class="panel panel-default every_day">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 panel-heading day_title" role="tab" id="DailyItinerary_heading_4">
                                        <div class="panel-title">
                                            <a role="button" data-toggle="collapse" data-parent="#DailyItinerary_accordion" href="#DailyItinerary_collapse_4" aria-expanded="false" aria-controls="DailyItinerary_collapse_4" class="collapsed">
                                                <h4 class="col-xs-12 col-sm-12 col-md-1 col-lg-1 day_title_left"><span class="en_day">D4</span><span class="tw_day">第4天</span></h4>
                                                <h4 class="col-xs-12 col-sm-12 col-md-11 col-lg-11 day_title_right">屯溪→歙縣【徽州古城、斗山街、府衙、渔梁垻】→屯溪【老街、徽韻秀】</h4>
                                            </a>
                                        </div>
                                    </div>
                                    
                                    <div id="DailyItinerary_collapse_4" class="panel-collapse collapse dark_pile in" role="tabpanel" aria-labelledby="DailyItinerary_heading_4" aria-expanded="false">
                                        <div class="panel-body">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 day_content">
                                                
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 day_meal">
                                                <i class="fa fa-cutlery" aria-hidden="true"></i>
                                                <div class="meal_content">
                                                    <dl class="dl-horizontal">
                                                        <dt>早餐：</dt>
                                                        <dd>-- 酒店內享用</dd>
                                                        <dt>午餐：</dt>
                                                        <dd>-- 中式合菜50</dd>
                                                        <dt>晚餐：</dt>
                                                        <dd>-- 徽菜風味50</dd>
                                                    </dl>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 day_hotel">
                                                
													<i class="fa fa-bed" aria-hidden="true"></i>
													<p>天都國際酒店 或同級</p>
												
                                            </div>
                                        </div>
                                    </div>
                                </div>
								
								<div id="day5" class="panel panel-default every_day">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 panel-heading day_title" role="tab" id="DailyItinerary_heading_5">
                                        <div class="panel-title">
                                            <a role="button" data-toggle="collapse" data-parent="#DailyItinerary_accordion" href="#DailyItinerary_collapse_5" aria-expanded="false" aria-controls="DailyItinerary_collapse_5" class="collapsed">
                                                <h4 class="col-xs-12 col-sm-12 col-md-1 col-lg-1 day_title_left"><span class="en_day">D5</span><span class="tw_day">第5天</span></h4>
                                                <h4 class="col-xs-12 col-sm-12 col-md-11 col-lg-11 day_title_right">屯溪【湖邊古村落、徽州照壁、徽州文化藝術館】/桃園</h4>
                                            </a>
                                        </div>
                                    </div>
                                    
                                    <div id="DailyItinerary_collapse_5" class="panel-collapse collapse dark_pile in" role="tabpanel" aria-labelledby="DailyItinerary_heading_5" aria-expanded="false">
                                        <div class="panel-body">
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 day_content">
                                                
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 day_meal">
                                                <i class="fa fa-cutlery" aria-hidden="true"></i>
                                                <div class="meal_content">
                                                    <dl class="dl-horizontal">
                                                        <dt>早餐：</dt>
                                                        <dd>-- 酒店內享用</dd>
                                                        <dt>午餐：</dt>
                                                        <dd>-- 環翠堂50</dd>
                                                        <dt>晚餐：</dt>
                                                        <dd>-- 敬請自理</dd>
                                                    </dl>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 day_hotel">
                                                
													<i class="fa fa-bed" aria-hidden="true"></i>
													<p>溫暖的家 或同級</p>
												
                                            </div>
                                        </div>
                                    </div>
                                </div>
								
                            </div>
                        </div>
                        <!-- 每日行程 END -->
						
                        <!-- 附加項目 START -->
                        <div id="Additional" class="Additional animatedParent article">
                            <h4 class="animated fadeInUpShort slow"><i class="fa fa-clipboard animated fadeInUpShort slow" aria-hidden="true"></i>附加項目</h4>
                            <div class="panel-group animated fadeInUpShort slow" id="Additional_accordion" role="tablist" aria-multiselectable="true">
                                
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="Additional_heading_2">
                                        <h4 class="panel-title">
                                            台胞證5年多次
											<span class="label_group">
												
											</span>
											
                                            <div><p>費用：<span>2000</span></p></div>
                                            
                                        </h4>
                                    </div>
									
                                </div>
								
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="Additional_heading_2">
                                        <h4 class="panel-title">
                                            中華民國護照
											<span class="label_group">
												
											</span>
											
                                            <div><p>費用：<span>1600</span></p></div>
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#Additional_accordion" href="#Additional_collapse_2" aria-expanded="false" aria-controls="Additional_collapse_2"></a>
                                        </h4>
                                    </div>
									
                                    <div id="Additional_collapse_2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="Additional_heading_2">
                                        <div class="panel-body">
                                            <ul class="list-inline">
                                                <li>說明:<p>1.護照照片規格：<br>照片大小直4.5橫3.5，頭大小3.2~3.6,五官清晰需露眉、露耳，不得露齒，不得配戴粗框及有色眼鏡，客人拍照請特別跟照相館指定是申請護照用的照片。<br>2.未滿20歲者，附父或母或監護人身分證正本<br>*父母離婚或其中一人已歿者，請附戶籍謄本(三個月內)證明，並附監護人身分證正本<br>*若父母離婚，由父母共同監護者，附其中一人身分證正本及戶籍謄本(三個月內)<br>3.已退伍者，附退伍令(役男19歲～36歲)<br>*若前次申辦護照已註銷過兵役身分者，僅需附舊護照。 <br>*37歲以上已達除役年齡，不需再附退伍令。<br>4.初次辦理護照需附『人別確認書』以證實身分，人別確認須親自赴外交部或至戶籍所在地之戶政事務所作人別確認。</p></li><li>備註:<p>1.身分證正本，並影印清晰 <br>2.半年內彩色白底兩吋照片2張，背面需填姓名(請詳閱以下備註1)<br>3.附舊護照 <br>4.未滿20歲者，附父或母或監護人身分證正本（例外者以下備註2） <br>5.未滿14歲無身分證者，附戶口名簿正本 或 戶籍謄本(三個月內)<br>6.國軍人員附軍人身分證影本(不可用傳真版本)<br>7.已退伍者，附退伍令(以下備註3)<br>8.遺失補辦者，另附遺失證明（警察局報案證明） <br>9.改名者，不必再附戶謄,唯退伍令為舊名者,仍需附三個月內戶謄<br>10.未滿20歲，父母離婚者，附戶籍謄本(三個月內)，附監護人的身分證正本。若共同監護，附其一即可</p></li>
                                            </ul>
                                        </div>
                                    </div>
									
                                </div>
								
                            </div>
                        </div>
                        <!-- 附加項目 END -->
						
                        <!-- 護照及簽證 START -->
                        <div id="Passport_Visa" class="Passport_Visa animatedParent article">
                            <h4 class="animated fadeInUpShort slow"><i class="fa fa-id-card-o" aria-hidden="true"></i>護照及簽證</h4>
                            <div class="Passport_Visa_box animated fadeInUpShort slow">
                                <ul class="Passport_Visa_title">
                                    <li>項目</li>
                                    <li>辦證工作日</li>
                                    <li>護照效期</li>
                                    <li>可停留當地天數</li>
                                    <li>說明</li>
                                </ul>
								
                                <ul class="Passport_Visa_content">
                                    <li>台胞證5年多次</li>
                                    <li>7天</li>
                                    <li>
									5年-1日
									</li>
                                    <li>--</li>
                                    <li></li>
                                </ul>
								
                                <ul class="Passport_Visa_content">
                                    <li>中華民國護照</li>
                                    <li>5天</li>
                                    <li>
									10年
									</li>
                                    <li>--</li>
                                    <li><a href="#" title="說明" data-toggle="modal" data-target="#myPassportVisa_2"><i class="fa fa-info-circle" aria-hidden="true"></i></a></li>
                                </ul>
								
                            </div>
                        </div>
                        <!-- 護照及簽證 END -->
						
                        <!-- 其他說明 START -->
                        <div id="OtherInstructions" class="OtherInstructions animatedParent article">
                            <h4 class="animated fadeInUpShort slow"><i class="fa fa-tag" aria-hidden="true"></i>其他說明</h4>
                            <div class="other_content animated fadeInUpShort slow">
                                <ul>
								
									<li><h4>團票說明</h4><p></p><div class="day_style_1"><div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 item"><h2>包含項目</h2><p>機場稅.安檢費.履約責任險.燃油稅。</p></div></div><div class="day_style_1"><div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 item"><h2>不含項目</h2><p>台胞證、機場接送、(領隊+導遊+司機 小費每天NT 200)**</p></div></div><p></p></li>
								
                                    <li>
                                        <h4>出團備註</h4>
                                        <p></p><div class="day_style_1"><div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 item"><p style="text-align: center;">【本行程之各項內容及價格因季節、氣候等其他因素而有所變動，請依出發前說明會資料為主，不另行通知】</p><h2>注意事項</h2><p>1、團體旅遊中各團價格、航班及行程內容可能會因不同出發日期而有所變動，請先依您所需的出團日期查詢。</p><p>2、航空公司於票價中需增收燃油附加費，但因各航空公司徵收的費用不同，以致相同機場之機場稅也會有所差異。</p><p>3、本站所提供之各項旅遊產品是否需額外加收燃油附加費，請詳閱各項產品之說明。</p><p>4、請務必詳閱各團的各項說明以確保您的權益。</p><p>5、因綠色環保各地酒店取消一次性用品，請自行攜帶牙膏、牙刷及洗漱用品。</p><p>6、以上報價已分攤60歲以上的老人及小童優惠門票價格，如產生優惠票，恕不退回。</p><p>7、前往大陸地區，行動電源有清楚明顯標示30000 mAh以下，可放隨身行李攜帶，若超過中國海關一律沒收。若您的行動電源無詳細的標示mAh容量，請攜帶商品說明書或包裝盒說明，否則海關將視同於超過30000 mAh，予以沒收！</p><p>8、根據中國民用航空局有關規定，旅客不得隨身攜帶或在手提行李、托運行李中運輸打火機、火柴，如旅客隨身攜帶或在手提行李、托運行李中有打火機火柴的，請取出並自行處置。否則，將可能面臨中國公安機關5000元人民幣以下罰款、拘留等行政處罰</p></div></div><p></p>
                                    </li>
									
                                    <li>
                                        <h4>天氣</h4>
                                        <p>請點選<a href="http://www.cwb.gov.tw/V7/forecast/world/world_aa.htm" target="_blank">天氣參考網址</a></p>
                                    </li>
									
								</ul>
                            </div>
                        </div>
                        <!-- 其他說明 END -->
						
                    </div>
                    <!-- 產品內容 END -->
                </div>
            </section>
            <!-- 個團明細頁 END -->
		</div>
	</article>
	
	<!-- 個團資料列印、下載、轉寄 Modal -->
	<div class="modal fade bs-example-modal-lg" id="mydownload" tabindex="-1" role="dialog" aria-labelledby="mydownloadLabel">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
					<h4 class="modal-title" id="mydownloadLabel">個團資料下載、列印</h4>
				</div>
				<div class="modal-body">
					<div class="Other_features_box">
						<div class="Function_Buttons">
							<ul>
								<li><a href="http://api.travel.net.tw/pdf/File?ProdTp=MGroup&amp;prodCd=TXNMUT5A&amp;srcCls=D&amp;srcId=&amp;stamp=201806141703&amp;hash=6daeb5bcbdab83494b158c1c473aab268da9c3423af91482ef453dea19dc78a9&amp;webHost=http://www.summittour.com.tw" target="_blank" class="btn btn-danger" title="下載PDF"><i class="fa fa-file-pdf-o" aria-hidden="true"></i><br>下載</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- 洽詢客服人員 -->
	<div class="modal fade bs-example-modal-lg" id="mycontact" tabindex="-1" role="dialog" aria-labelledby="mycontactLabel">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
					<h4 class="modal-title" id="mycontactLabel">洽詢客服人員</h4>
				</div>
				<form name="ContactFrom" id="ContactFrom" method="post" action="GroupContactPro.asp">
					<input type="hidden" name="contGName" id="contGName" value="海棠風情─潑墨黃山臥虎藏龍宏村屯溪老街徽州古城五日">
					<input type="hidden" name="contGCode" id="contGCode" value="TXNMUT5A">
					<div class="modal-body">
						<p>我對貴公司一般團 <span>海棠風情─潑墨黃山臥虎藏龍宏村屯溪老街徽州古城五日</span> 有興趣，請客服人員與我聯絡！</p>
						<ul class="contact_content">
							<li class="col-xs-12 col-sm-12 col-md-2 col-lg-2"><i class="fa fa-asterisk" aria-hidden="true"></i>姓名：</li>
							<li class="col-xs-12 col-sm-12 col-md-2 col-lg-2"><input type="text" class="form-control Sender" name="contUsrName" id="contUsrName" value="" placeholder="請輸入您的姓名"></li>
							<li class="col-xs-12 col-sm-12 col-md-2 col-lg-2"><i class="fa fa-asterisk" aria-hidden="true"></i>EMAIL：</li>
							<li class="col-xs-12 col-sm-12 col-md-6 col-lg-6"><input type="text" class="form-control Email_Address" name="contUsrMail" id="contUsrMail" value="" placeholder="請輸入您的Email地址"></li>
							<li class="col-xs-12 col-sm-12 col-md-2 col-lg-2"><i class="fa fa-asterisk" aria-hidden="true"></i>聯絡資訊：</li>
							<li class="col-xs-12 col-sm-12 col-md-3 col-lg-4"><input type="text" class="form-control Sender" name="contUsrTel" id="contUsrTel" value="" onblur="chkContNumber('contUsrTel');" placeholder="請輸入您的電話"></li>
							<li class="col-xs-12 col-sm-12 col-md-3 col-lg-2"><input type="text" class="form-control Sender" name="contUsrEtn" id="contUsrEtn" value="" placeholder="請輸入您的分機"></li>
							<li class="col-xs-12 col-sm-12 col-md-4 col-lg-4"><input type="text" class="form-control Email_Address" name="contUsrMob" id="contUsrMob" value="" onblur="chkContNumber('contUsrMob');" placeholder="請輸入您的手機號碼"></li>
						</ul>
								
						<ul class="message">
							<li class="col-xs-12 col-sm-12 col-md-2 col-lg-2">留言：</li>
							<li class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
								<textarea class="form-control" name="contUsrMsg" id="contUsrMsg" ols="" rows="5"></textarea>
							</li>
						</ul>
						<p>以上資料請務必詳實填寫，洽詢通知送出後，本公司人員將於近日內與您聯繫。</p>
					</div>
					
					<div class="modal-footer">
						<ul>
							<li class="col-xs-12 col-sm-12 col-md-2 col-lg-2">驗證碼：</li>
							<li class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
								<input name="contVer" id="contVer" class="form-control Captcha" type="text">
							</li>
							<li class="col-xs-12 col-sm-12 col-md-1 col-lg-1">
								<img src="/eweb/public/CREATE_VID.asp?NAME=contVer" alt="點擊更換驗證碼" onclick="this.src='/eweb/public/CREATE_VID.asp?NAME=contVer&amp;randon='+Math.random();" style="CURSOR:hand;">
							</li>
							<li class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
								<button type="button" class="btn btn-primary" onclick="ContactSubmit();">確認寄送</button>
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							</li>
						</ul>
					</div>
				</form>
			</div>
		</div>
	</div>	
	
	
	<div class="modal fade bs-example-modal" id="myPassportVisa_2" tabindex="-1" role="dialog" aria-labelledby="myPassportVisaLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="myPassportVisaLabel">中華民國護照</h4>
            </div>
            <div class="modal-body">
                <div class="panel panel-default">
                    <div class="panel-heading">說明</div>
                    <div class="panel-body"><p>1.身分證正本，並影印清晰 <br>2.半年內彩色白底兩吋照片2張，背面需填姓名(請詳閱以下備註1)<br>3.附舊護照 <br>4.未滿20歲者，附父或母或監護人身分證正本（例外者以下備註2） <br>5.未滿14歲無身分證者，附戶口名簿正本 或 戶籍謄本(三個月內)<br>6.國軍人員附軍人身分證影本(不可用傳真版本)<br>7.已退伍者，附退伍令(以下備註3)<br>8.遺失補辦者，另附遺失證明（警察局報案證明） <br>9.改名者，不必再附戶謄,唯退伍令為舊名者,仍需附三個月內戶謄<br>10.未滿20歲，父母離婚者，附戶籍謄本(三個月內)，附監護人的身分證正本。若共同監護，附其一即可</p></div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">其他備註</div>
                    <div class="panel-body"><p>1.護照照片規格：<br>照片大小直4.5橫3.5，頭大小3.2~3.6,五官清晰需露眉、露耳，不得露齒，不得配戴粗框及有色眼鏡，客人拍照請特別跟照相館指定是申請護照用的照片。<br>2.未滿20歲者，附父或母或監護人身分證正本<br>*父母離婚或其中一人已歿者，請附戶籍謄本(三個月內)證明，並附監護人身分證正本<br>*若父母離婚，由父母共同監護者，附其中一人身分證正本及戶籍謄本(三個月內)<br>3.已退伍者，附退伍令(役男19歲～36歲)<br>*若前次申辦護照已註銷過兵役身分者，僅需附舊護照。 <br>*37歲以上已達除役年齡，不需再附退伍令。<br>4.初次辦理護照需附『人別確認書』以證實身分，人別確認須親自赴外交部或至戶籍所在地之戶政事務所作人別確認。</p></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
  <!--下標-->
  <footer role="contentinfo">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 footer_nav">
		<ul>
			<li><a href="https://www.facebook.com/summittourTaiwan" target="_blank">FB粉專頁</a></li>
			<li><a href="/eWeb_summittour/auto/about.asp">關於超悠旅遊</a></li>
			<li><a href="/eweb_summittour/images/movable/超悠旅行社新版刷卡單.pdf" target="_blank">信用卡授權書下載</a></li>
			<li><a href="https://tw.money.yahoo.com/currency-converter" target="_blank">匯率查詢</a></li>
			<li><a href="http://www.cwb.gov.tw/V7/index.htm" target="_blank">全球氣候</a></li>
			<li><a href="/eWeb_summittour/page/airline_info.asp">航空資訊</a></li>
			<li><a href="/eWeb_summittour/page/visa1.asp">簽證資訊</a></li>
		</ul>
	</div>
    <div class="container footer_info">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 info_content">
                <h3>超悠旅行社股份有限公司</h3>
                <h4>CH’AO TRAVEL AGENCY CO,LTD</h4>
                <p>甲種旅行社 交觀甲字第7398號 品保中0346</p>
                <p>代表人：林瑞恩 聯絡人：陳力嘉</p>
                <p>E-Mail：summit1@summittour.com.tw</p>
                <p>Copyright c summitt our.com All Rights R eserved.</p>
                <p>版權所有，禁止未經授權轉貼節錄</p>
                <p>各項產品及服務為超悠旅行社股份有限公司提供</p>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 info_content">
                <h4>台中總公司</h4>
                <ul>
                    <li>403 台中市西區向上路ㄧ段25號3樓</li>
                    <li>電話：04-23052639、04-22208155</li>
                    <li>傳真：04-23052659、04-22209306</li>
                </ul>
                <h4>台北分公司</h4>
                <ul>
                    <li>104 台北市中山區長安東路2段163號2樓</li>
                    <li>電話：02-27112387 傳真：02-27112360</li>
                </ul>
				<h4>新竹分公司</h4>
                <ul>
                    <li>302 新竹縣竹北市三民路536號2樓</li>
                    <li>電話：03-5520969 傳真：03-5520881</li>
                </ul>
				
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 footer_mark">
                <img src="/eWeb_summittour/images/default/mark.png">
                <ul>
                    <li>為確保您能獲得更為穩定的瀏覽品質與使用體驗</li>
                    <li>建議您使用 Chr ome 或 IE10 以上瀏覽器瀏覽本網站</li>
                </ul>
            </div>
        </div>
    </div>
</footer>

<link type="text/css" rel="stylesheet" href="http://www.bwt.com.tw/eWeb/GO/type_screen.css?0913">
    
  
</div>

<div class="actGotop" style="display: block;"><a href="javascript:;" title="返回頂部"><i class="fa fa-angle-up" aria-hidden="true"></i>TOP</a></div>
<script type="text/javascript" language="javascript" src="/eweb_summittour/js/jquery-3.1.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.8/js/materialize.min.js"></script>
<script type="text/javascript" language="javascript" src="/eweb_summittour/js/bootstrap.min.js"></script>
<script type="text/javascript" language="javascript" src="/eweb_summittour/js/jquery.nicescroll.min.js"></script>
<script type="text/javascript" language="javascript" src="/eweb_summittour/js/modernizr.js"></script>
<script type="text/javascript" language="javascript" src="/eweb_summittour/js/all.js"></script>
<script type="text/javascript">
$(function(){
    var sideslider = $('[data-toggle=collapse-side]');
    var get_sidebar = sideslider.attr('data-target-sidebar');
    var get_content = sideslider.attr('data-target-content');
    sideslider.click(function(event){
      $(get_sidebar).toggleClass('in');
      $(get_content).toggleClass('out');
   });
});
/** 回頂點 **/
$(function(){   
    $(window).scroll(function() {       
        if($(window).scrollTop() >= 100){
            $('.actGotop').fadeIn(300); 
        }else{    
            $('.actGotop').fadeOut(300);    
        }  
    });
    $('.actGotop').click(function(){
    $('html,body').animate({scrollTop: '0px'}, 800);}); 
});
/** HEADER回縮SubNvaq判斷Class **/
$(function(){   
    var cubuk_seviye = $(document).scrollTop();
    var header_yuksekligi = $('.header').outerHeight();
    $(window).scroll(function() {
        var kaydirma_cubugu = $(document).scrollTop();
        if (kaydirma_cubugu > header_yuksekligi){$('.header').addClass('gizle');} 
        else {$('.header').removeClass('gizle');}
        if (kaydirma_cubugu > cubuk_seviye){$('.header').removeClass('sabit');} 
        else {$('.header').addClass('sabit');}              
        cubuk_seviye = $(document).scrollTop(); 
     });
});
$('.carousel').carousel({
  interval: 8000
})
</script>
<script type="text/javascript" language="javascript" src="/eweb_summittour/js/main.js"></script>
<!-- Google+ 在最後一個「分享」標記之後放置這個標記。 -->
<script type="text/javascript">
  window.___gcfg = {lang: 'zh-TW'};
  (function() {
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/platform.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
  })();
</script>

	<script type="text/javascript" language="javascript" src="/eweb_summittour/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
	<script type="text/javascript" language="javascript" src="/eweb_summittour/js/locales/bootstrap-datetimepicker.zh-TW.js" charset="UTF-8"></script>
	<script type="text/javascript" language="javascript" src="/eweb_summittour/js/css3-animate-it.js"></script>
	<script type="text/javascript" language="javascript" src="/eweb_summittour/js/imagesLoaded.js"></script>
	<script type="text/javascript" language="javascript" src="/eweb_summittour/js/masonry.pkgd.min.js"></script>
	<script language="javascript" type="text/javascript" src="/eweb_summittour/js/jquery.cookie.js"></script>
	<script src="https://d.line-scdn.net/r/web/social-plugin/js/thirdparty/loader.min.js" async="async" defer="defer"></script>
	<script language="javascript" type="text/javascript" src="/EW/Script/GroupDetailJs.js"></script>



<div id="syno-notification-is-installed"></div><div class="hiddendiv common"></div><div id="ascrail2000" class="nicescroll-rails" style="width: 8px; z-index: 9999; cursor: default; position: fixed; top: 0px; height: 100%; right: 0px; opacity: 0;"><div style="position: relative; top: 56px; float: right; width: 8px; height: 32px; background-color: rgb(51, 51, 51); border: 0px; background-clip: padding-box; border-radius: 0px;"></div></div><div id="ascrail2000-hr" class="nicescroll-rails" style="height: 8px; z-index: 9999; position: fixed; left: 0px; width: 100%; bottom: 0px; cursor: default; opacity: 0;"><div style="position: relative; top: 0px; height: 8px; width: 1232px; background-color: rgb(51, 51, 51); border: 0px; background-clip: padding-box; border-radius: 0px; left: 40px;"></div></div><button id="lineItTool" style="position: absolute; margin: 0px; padding: 0px; border: none; font-size: 0px; border-radius: 3px; width: 64px; height: 20px; background-size: 64px 20px; background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAAAoCAMAAAABrwJ6AAABZVBMVEUAAAAAuDMAuDMAuDMAuDMAuDMAuDMAuDMAuDMAuDMAuDMAuDMAuDMAuDMAuDMAuDMAuDMAuDMAuDMAuDMAuDMAuDMAuDMAuDMAuDMAuDMAuDMAuDP///932ZJEy2m77MkAtCYAsyQAuDEAtSkAty8iwU4Ati3u+/L7/vzc9uT2/fnm+ezz/Paq57s4xVnY9eFm1IUowk7v+/TM8dax6cCn57qh5rSO4KUzxlwRvUEBtSgAsiD9/v7i9+i27Ma568aa46+D3Zxs1IJV0XgjwEoOuTQHuTP4/vro+e7G79Ot6byI3qB22I9l1IBc0XlEymUvxFQRvDsYujnq+vDV9N5g04BY0HdVznFKy2oyxVcuwEcWvkIgvDwlvDsKtSrY9ePN8tfB7s2T4KeI3Zt/25h42pRx14xr1odp1YVa0HZazW8dv0QKuzkTuDMety8Arxrd9+WN36GB2ZJh0XtTznI7wk8ArRNd+fPuAAAAG3RSTlMA6/zmpn0gAt2FYA75y8S+cO6Oi3RnQj8tKROS0r6+AAADaklEQVRYw8WYCXOaQBSAsU1633f7HrjLIggYj8ZqPKOJMUlzNemRq/d93+3v7wsOVAliO634zQi7vBW+2fcYZlfa49D4yVgMIiUWO3n0itTh8MWDMBLOHj3uPP8ojIyxayRwDEbIucPSoSPgQ9U0FSLiwCXpDHSjCs6ZEIxzEY3EmHQafqNxLbcpFxuNoryZow4Mn5h0EFx0tr4ybVvoYNWmV9bZ8BVOSODC5rIZ7OHOyhyDYeMJ8FYe95FvcRgungBLZzCAzDaDYeIJiJyJgZg5AcPDE9DbcexDva27YxVZTrjthCwrzpVSdx9kD3egbHaa3rBEkAB/lcQ+JF9z716IiieDKDtXJrr7gB7uQLfpDVMCBPT1RUQ0DDrM28kUnZP2fMpesG9XERfX9TABzPYKGPEO7sAlXKJjHDFMgCnziKnNrSTi9E79Resx1nfyz+6/20g/QrTWWKiAkegRiMM+BgqobBkRr9/KkcAEL9z9mbZussYMn11dtinwkqlhAjgZKpCVZcq8iUjZ7ysginsC6VskcPPhjSyvPI9/KMzM3Zh+YlFgSWhhNUCNMIE4IgUdlH4CWq/A3d21udkPhZcV8WnjDgWKImwGlDj9wgUGz0C56QhsVzsCCTNXaXdmIEWB5+VQgZKBZmnfWyB3CwyugVVENLb11bdfblQK995jXPtUWKl8m/m6QIF7LFQAZikP/yYA5VwNMfXmwf2Pb/M79eUpC5sP8s+o+yOD+LlVhlABWELMegKTikPirwQ00UCs2hkzY6dMa6FWxaSZMjLmo1oS8SlTBwjAJBrZkBoYLADs1m3sQ60lYJDAlIHGQIHwjxF7hcEk7zEYJOCc/1FAFU0M5IVQuwVkxaHkE4AJ9NWA0itA8VkgSo2JqUAB0MWyhftpCg26BFwUv0DJTYFHr0DW+Zf79QoSAJ1tTaKPebmswx8JgBIuADLdfIqqhWaijwCo7P3rxevYxe1VpsN/JuHLAAn8RuPtrWb+sZuKxTRXYehI0I3OubYxk0KiWtzgEAGSr69qlXc2It75LgREAS1MfPA1C68/3WUaRMEJWpr54G8W6mtU/dEQo8WpD2033WYqRMRYwPJcYzpExYHLtEFxAEbH+b0tmvHRGYwdl/Y4dgRGQmycnu9w6MKpyLfpDp4avyoRvwAYMlAfzeXmRQAAAABJRU5ErkJggg==&quot;); z-index: 90000; display: none; cursor: pointer;"></button></body>
</body>
</html>

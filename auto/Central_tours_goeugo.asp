﻿<!DOCTYPE html><!-- lang="zh-TW"-->
<html class="no-js">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta charset="utf-8">
<title>歐洲一日遊&循環團優惠</title>
<!-- for Facebook -->          
<meta property="og:url"                content="http://www.summittour.com.tw/eWeb_summittour/auto/Central_tours_goeugo.asp" />
<meta property="og:type"               content="website" />
<meta property="og:locale"             content="zh_TW" />
<meta property="og:title"              content="頂尖旅遊限定優惠" />
<meta property="og:description"        content="歐洲首創以巴士循環方式來主辦旅行團" />
<meta property="og:image"              content="http://www.summittour.com.tw/eWeb_summittour/IMGDB/001273/001274/001351/00015735.JPG	" />
<!-- for Facebook -->  
<link rel="shortcut icon" type="image/x-icon" href="/eweb_summittour/images/default/favicon.ico">
<link rel="bookmark" type="image/x-icon" href="/eweb_summittour/images/default/favicon.ico">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="description" content="歐洲首創以巴士循環方式來主辦旅行團"><!--網站描述-->
<meta name="keywords" content="歐洲一日遊&循環團優惠"><!--關鍵字-->
<meta name="Robots" content="all" />
<meta name="Googlebot" content="index,follow" />
<meta name="Audience" content="All" />
<meta name="Page-topic" content="Internet" />
<meta name="Rating" content="General" />
<meta name="Resource-Type" content="document" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="windows-Target" content="_top" />
<meta name="Author" content="GTSHAYO" />
<meta name="Copyright" content="Copyright c 2017 www.summittour.com.tw All Rights Reserved." />
<meta name="Generator" content="Visual Studio Code" />
<meta name="Publisher" content="超悠旅行社" />
<meta name="Creation-Date" content="06-23-2017" />
<meta name="WEBCRAWLERS" content="ALL" />
<meta name="SPIDERS" content="ALL" />
<meta name="revisit-after" content="7 days"/>

<link type="text/css" rel="stylesheet" href="/eweb_summittour/css/reset_new.css">
<link type="text/css" rel="stylesheet" href="/eweb_summittour/css/normalize.css">
<link type="text/css" rel="stylesheet" href="/eweb_summittour/css/bootstrap.css">
<link type="text/css" rel="stylesheet" href="/eweb_summittour/css/font-awesome.min.css">
<!--<link rel="stylesheet" type="text/css" href="/eweb_summittour/css/loaders.css"/>-->
<link rel="stylesheet" type="text/css" href="/eweb_summittour/css/main.css">
<link type="text/css" rel="stylesheet" href="/eweb_summittour/css/defaultUP2.css">

<script type="text/javascript" language="javascript" src="/eweb_summittour/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" language="javascript" src="/eweb_summittour/js/bootstrap.min.js"></script>
<!--<script type="text/javascript" language="javascript" src="/eweb_summittour/js/jquery.nicescroll.min.js"></script>-->
<script type="text/javascript" language="javascript" src="/eweb_summittour/js/modernizr.js"></script>
<script type="text/javascript" language="javascript" src="/eweb_summittour/js/all.js"></script>
<script type="text/javascript" language="javascript" src="/eweb_summittour/js/hammer.min.js"></script>


<link href="/eweb_summittour/auto/autopage.css" rel="stylesheet" type="text/css">
<style type="text/css">
#skin {display: table; float: left;}
#skin li {float: left; font-size:  20px; color: #999999; padding: 0 5px; cursor: pointer;}
#skin li i {padding: 0 5px;}
#skin li:hover {color: #74a73a;}
#skin li.selected {color: #74a73a; cursor: default;}
</style>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-99295320-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-99295320-1');
</script>
<style type="text/css">
.product_box .product_name img.flag {width: 20%;}
</style>
<script type="text/javascript" language="javascript" src="/eweb_summittour/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script language="javascript" type="text/javascript" src="/EW/Script/GroupDetailJs.js"></script>
</head>
<!--style type="text/css">
.wrapper2 {
    background: #c414d80d;
}
</style-->
<body>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5be3ef390e6b3311cb786257/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->

<!-- Loader動畫 -->
<!--<div id="loader-wrapper">
    <div id="loader"></div>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
</div>-->
<!-- Loader動畫 -->
<div class="wrapper">
	 
	<!--HEADER START-->
	<article class="header"> 
		<!--以參數 fbhtml_url作為存取本視窗網址字串參數。-->
			<script>
			var fbhtml_url=window.location.toString();
			</script>

			<nav id="top" class="navbar navbar-fixed-top navbar-inverse">
				<div class="container">
					<div class="row">
					
						<div class="navbar-header">
							<button data-toggle="collapse-side" data-target-sidebar=".side-collapse-left" data-target-content=".side-collapse-container-left" type="button" class="navbar-toggle pull-left">
								<a class="nav-btn" id="nav-open-btn" href="#nav"></a>
							</button>
							<h2 class="block-title">
								<a href="http://www.summittour.com.tw"><img src="/eWeb_summittour/images/default/logo.png"/></a>
								<!--<a class="navbar-brand" href="#">Design</a>-->
							</h2>
							<div class="navbar-inverse1 side-collapse-right in">
								<nav id="nav1" role="navigation" class="navbar-collapse1">
									<ul class="nav navbar-nav1 navbar-right ">
										<li class="">
											<a href="/eWeb_summittour/page/member.asp"title="加入會員"><p>加入會員</p></a>
										</li>
										<li class="through">
											<a href="/eWeb_summittour/index.asp"title="回頂尖首頁"><p>回頂尖首頁</p></a>
										</li>
										<li class="through">
											<!--FB分享按鈕(純HTML語法)-->
											<a target="_blank" href="http://www.facebook.com/share.php?u=http://28371.psee.io/7H2VV">
											<img title="分享到FB！" src="/eWeb_summittour/images/default/icon_top_FB-02.png" border="0" width="30"></a>
										</li>
										<li class="through">
											<!--line分享按鈕(純HTML語法)-->
											<a target="_blank" href="https://social-plugins.line.me/lineit/share?url=http://28371.psee.io/7H2VV">
											<img title="分享到LINE！" src="/eWeb_summittour/images/default/icon_top_line-02.png" border="0" width="30"></a>
										</li>
										<li class="through">
											<!--Twitter分享按鈕(純HTML語法)-->
											<a target="_blank" href="http://twitter.com/home/?status=http://28371.psee.io/7H2VV">
											<img title="分享到維特！" src="/eWeb_summittour/images/default/icon_top_Twitter-02.png" border="0" width="30"></a>				
										</li>
										<li class="through">
											<!--Google+分享按鈕(純HTML語法)-->
											<a target="_blank" href="https://plus.google.com/share?url=http://28371.psee.io/7H2VV">
											<img title="分享到Google+！" src="/eWeb_summittour/images/default/icon_top_G-02.png" border="0" width="30"></a>
										</li>
										<!--會員登入-->
										
											<%If (Session("USR_ID") <> "") Then%>
												<li class="top_member hidden-md">
													<span style="color: #690;">HI， &nbsp;<%=Session("USR_CNM")%> 歡迎您！ &nbsp;</span>
												</li>
												<li class="top_member hidden-md">
													<span><a href="/eweb/Main/O_Logout.asp" target="_top"><span>登出&nbsp;</span></a></span>
												</li>
												</li>
												
												
											<%elseif Session("COMP_CD") <> "" then%>
												<li class="top_member hidden-md">
													<span><a href="/eWeb_summittour/page/member.asp"></a></span>
												</li>
												<li class="top_member hidden-md">
													<span style="color: #690;">>HI， &nbsp;<%=COMP_ANM%> 歡迎您！ &nbsp;</span>
												</li>
												<li class="top_member hidden-md">
													<span><a href="/eweb/Main/O_Logout.asp" target="_top"><span>登出&nbsp;</span></a></span>
												
												
											<%End If %>
										<!--會員登入-END-->
										
									</ul>
								</nav>
							</div>
							
							<div class="navbar-inverse side-collapse-left in">

								<nav id="nav" role="navigation" class="navbar-collapse">
									<ul class="nav navbar-nav navbar-left">
										<li><a href="#tab3">一日遊</a></li>
										<li><a href="#tab2">循環團</a></li>
										<li class="hidden-lg">
											<a href="/eWeb_summittour/page/member.asp"><p>加入會員</p></a>
										</li>
										<li class="hidden-lg">
											<a href="/eWeb_summittour/index.asp"><p>回首頁</p></a>
										</li>
										<li class="hidden-lg">
											<!--FB分享按鈕(純HTML語法)-->
											<a target="_blank" href="http://www.facebook.com/share.php?u=http://28371.psee.io/7H2VV">
											<p><img title="分享到FB！" src="/eWeb_summittour/images/default/icon_top_FB-02.png" border="0" width="20"style="margin-right: 5px;">FB分享</p></a>
										</li>
										<li class="hidden-lg">
											<!--line分享按鈕(純HTML語法)-->
											<a target="_blank" href="https://social-plugins.line.me/lineit/share?url=http://28371.psee.io/7H2VV">
											<p><img title="分享到LINE！" src="/eWeb_summittour/images/default/icon_top_line-02.png" border="0" width="20"style="margin-right: 5px;">LINE分享</p></a>
										</li>
										<li class="hidden-lg">
											<!--Twitter分享按鈕(純HTML語法)-->
											<a target="_blank" href="http://twitter.com/home/?status=http://28371.psee.io/7H2VV">
											<p><img title="分享到維特！" src="/eWeb_summittour/images/default/icon_top_Twitter-02.png" border="0" width="20"style="margin-right: 5px;">維特分享</p></a>				
										</li>
										<li class="hidden-lg">
											<!--Google+分享按鈕(純HTML語法)-->
											<a target="_blank" href="https://plus.google.com/share?url=http://28371.psee.io/7H2VV">
											<p><img title="分享到Google+！" src="/eWeb_summittour/images/default/icon_top_G-02.png" border="0" width="20"style="margin-right: 5px;">Google+分享</p></a>
										</li>
										<!--會員登入-->
										<%If (Session("USR_ID") <> "") Then%>
										<li class="hidden-lg hidden-md">
										<span style="color: #690;">親愛的 &nbsp;<%=Session("USR_CNM")%> 您好~ &nbsp;</span>
											<a href="/eweb/Main/O_Logout.asp" target="_top"><span style="color: #690;">登出</span></a>
										</li>
										<%elseif Session("COMP_CD") <> "" then%>
										<li class="hidden-lg hidden-md">
											<a href="/eWeb_summittour/page/member.asp"></a>
										<span style="color: #690;">親愛的 &nbsp;<%=COMP_ANM%> 您好~ &nbsp;</span>
											<a href="/eweb/Main/O_Logout.asp" target="_top"><span style="color: #690;">登出</span></a>
										</li>
										<%End If %>
										<!--會員登入-END-->
																
										
									</ul>
								</nav>
							</div>
							
							<!--button data-toggle="collapse-side1" data-target-sidebar1=".side-collapse-right" data-target-content1=".side-collapse-container-right" type="button" class="navbar-toggle1 pull-right">
								<a class="nav-btn" id="nav-open-btn" href="#nav1"></a>
							</button-->
					  </div>
					</div>
				  </div>
			</nav>
				
			<!-- Modal -->
			<div class="modal fade bs-example-modal-sm" id="myMember" tabindex="-1" role="dialog" aria-labelledby="myMemberLabel">
			  <div class="modal-dialog modal-sm" role="document">
				<div class="modal-content">
				  <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myMemberLabel">會員中心</h4>
				  </div>
				  <div class="modal-body">
					<!--#include virtual="/eWeb_summittour/public/login.asp"-->
				  </div>
				</div>
			  </div>
			</div>
	</article>
    <article class="summittour_carousel">
        <div class="row">
			<!--活動標題動畫-->
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<img src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/001273/001274/001275/00015612.JPG" style="width:100%">
            </div>
			<!--活動標題動畫-->
        </div>
    </article>
	<article class="container">
        <!--麵包屑及頁面標題--
		<div class="row">
            <ol class="breadcrumb" id="auto_breadcrumb">
                <li>目前位置：</li>
                <li class="active"><a href="/eWeb_summittour/home.asp">首頁</a></li>
                <li class="active">過年限時優惠</li>
            </ol>			
		</div>
		<!--麵包屑及頁面標題-->
	</article>
    <article class="exercise_min container product_types">
	
		<section>
				<div class="central_tours row">
					<div class="day_style_1 product_basic_info animatedParent article">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<img src="/eWeb_summittour/IMGDB/001273/001274/001277/00015614.PNG" width="100%" />
						</div> 
					</div>
					<div class="day_style_1 product_basic_info animatedParent article">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<img src="/eWeb_summittour/IMGDB/001273/001274/001277/00015615.PNG" width="100%" />
						</div> 
					</div>
					<div class="day_style_1 product_basic_info animatedParent article">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<h2>[團票說明]</h2> 
							<p>．成人團費:兩成人同房、佔半房、單人床</p>
							<p>．小童團費:與兩成人同房、不佔床，如小童需要佔床，則按成人團費計算</p>
							<p>．單人報名者如需獨住一房需付附加費 : 每晚€50歐元（注：粉線每晚€75歐元），如願拼房則無附加費用</p>
							<p>．團費以「天數」計，行程最後一晚是不包括酒店住宿(即七天六晚、四天三晚、一天無晚)</p>
						</div> 
					</div>
					<div class="day_style_2 product_basic_info animatedParent article">
						<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 item">
								<h2>[團費包括]</h2>

								<p>•全程旅遊巴士接送，車內提供免費Wi-Fi</p>
								<p>•中文導遊,服務殷勤 (國語及粵語講解)</p>
								<p>•3星級酒店住宿及歐陸早餐</p>
							</div>

							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 item">
								<h2>[團費不包括]</h2>

								<p>•參團前後的交通費</p>
								<p>•全程景點門劵、午餐及晚餐 </p>
								<p>•小費（每天€7歐元,成人及小童同價）</p>
								<p>•額外或行程以外要求安排之自費節目</p>
								<p>•各類保險、旅遊證件、簽證費用<br />（如有需要，本公司可提供相關數據）</p>
								<p>•在非本公司所能控制之情況下，如罷工及交通延誤，而引發之額外支出</p>
								<p>•一切不在「團費包括」所列 </p><br />
							</div> 
					</div>
					
					<div id="navbar-spy" class="nav navbar-default onnavbar">
                        <div id="cart" class="container-fluid" style="position: absolute; top: 0px;">
                            <div class="collapse navbar-collapse" id="myNavbar">
                                <div class="row">
                                <ul class="nav navbar-nav">
									<li class="active"><a href="#tab3">一日遊</a></li>						
									<li class=""><a href="#tab2">循環團</a></li>
                                </ul>
                                </div>
                            </div>
                        </div>
                    </div>
					<div class="product_content"> 		
						
						<div id="theme_content" class="skin_list">
							<div id="tab3">
							<div class="metropolis">
							<div class="title_box">
								<div class="left_box" > 
									<img src="/eweb_summittour/images/metropolis_title_icon_00.png">
									<h1>一日遊</h1>
								</div>
							</div>
							<div class="product_all"> 
								<div class="product_box">
									<div class="product_img"><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0012&regsCd=0012-0032&beginDt=2018/05/24&endDt=2099/12/31&allowJoin=1&ikeyword=%E5%B7%B4%E9%BB%8E%E5%87%BA%E7%99%BC" target="_top"><img src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/001273/001274/001278/001279/00015620.JPG"></a></div>
									<div class="product_name"><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0012&regsCd=0012-0032&beginDt=2018/05/24&endDt=2099/12/31&allowJoin=1&ikeyword=%E5%B7%B4%E9%BB%8E%E5%87%BA%E7%99%BC" target="_top"><img src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/001273/001274/001278/001279/00015617.PNG" class="flag" alt="Paris">巴黎出發Paris</a></div>
								</div> 
								<div class="product_box">
									<div class="product_img"><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0012&regsCd=0012-0032&beginDt=2018/05/24&endDt=2099/12/31&allowJoin=1&ikeyword=%E5%80%AB%E6%95%A6%E5%87%BA%E7%99%BC" target="_top"><img src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/001273/001274/001278/001279/00015623.JPG"></a></div>
									<div class="product_name"><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0012&regsCd=0012-0032&beginDt=2018/05/24&endDt=2099/12/31&allowJoin=1&ikeyword=%E5%80%AB%E6%95%A6%E5%87%BA%E7%99%BC" target="_top"><img src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/001273/001274/001278/001279/00015616.PNG" class="flag" alt="Paris">倫敦出發London</a></div>
								</div>
								<div class="product_box">
									<div class="product_img"><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0012&regsCd=0012-0032&beginDt=2018/05/24&endDt=2099/12/31&allowJoin=1&ikeyword=%E7%BE%85%E9%A6%AC%E5%87%BA%E7%99%BC" target="_top"><img src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/001273/001274/001278/001279/00015622.JPG"></a></div>
									<div class="product_name"><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0012&regsCd=0012-0032&beginDt=2018/05/24&endDt=2099/12/31&allowJoin=1&ikeyword=%E7%BE%85%E9%A6%AC%E5%87%BA%E7%99%BC" target="_top"><img src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/001273/001274/001278/001279/00015618.PNG" class="flag" alt="Paris">羅馬出發Rome</a></div>
								</div>
								<div class="product_box">
									<div class="product_img"><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0012&regsCd=0012-0032&beginDt=2018/05/24&endDt=2099/12/31&allowJoin=1&ikeyword=%E9%98%BF%E5%A7%86%E6%96%AF%E7%89%B9%E4%B8%B9%E5%87%BA%E7%99%BC" target="_top"><img src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/001273/001274/001278/001279/00015621.JPG"></a></div>
									<div class="product_name"><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0012&regsCd=0012-0032&beginDt=2018/05/24&endDt=2099/12/31&allowJoin=1&ikeyword=%E9%98%BF%E5%A7%86%E6%96%AF%E7%89%B9%E4%B8%B9%E5%87%BA%E7%99%BC" target="_top"><img src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/001273/001274/001278/001279/00015619.PNG" class="flag" alt="Paris">阿姆斯特丹出發Amsterdam</a></div>
								</div>
							</div>
							</div>
							</div>
							<div id="tab2">
							<div class="metropolis">
							<div class="title_box">
								<div class="left_box" > 
									<img src="/eweb_summittour/images/metropolis_title_icon_00.png">
									<h1>循環團</h1>
								</div>
							</div>
							<div class="product_all"> 
								<div class="product_box">
									<div class="product_img"><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0012&regsCd=0012-0031&beginDt=2018/05/24&endDt=2099/12/31&allowJoin=1&ikeyword=%E7%B4%85%E7%B7%9A%E3%80%8C%E8%A5%BF%E6%AD%90%E7%B6%93%E5%85%B8%E3%80%8D" target="_top"><img src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/001273/001274/001278/001279/00015630.JPG"></a></div>
									<div class="product_name"><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0012&regsCd=0012-0031&beginDt=2018/05/24&endDt=2099/12/31&allowJoin=1&ikeyword=%E7%B4%85%E7%B7%9A%E3%80%8C%E8%A5%BF%E6%AD%90%E7%B6%93%E5%85%B8%E3%80%8D" target="_top">紅線「西歐經典」<br/>Western Europe</a></div>
								</div>
								<div class="product_box">
									<div class="product_img"><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0012&regsCd=0012-0031&beginDt=2018/05/24&endDt=2099/12/31&allowJoin=1&ikeyword=%E7%B6%A0%E7%B7%9A%E3%80%8C%E5%8D%97%E6%B3%95%E6%84%8F%E5%BC%8F%E3%80%8D" target="_top"><img src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/001273/001274/001278/001279/00015629.JPG"></a></div>
									<div class="product_name"><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0012&regsCd=0012-0031&beginDt=2018/05/24&endDt=2099/12/31&allowJoin=1&ikeyword=%E7%B6%A0%E7%B7%9A%E3%80%8C%E5%8D%97%E6%B3%95%E6%84%8F%E5%BC%8F%E3%80%8D" target="_top">綠線「南法意式」<br/>Southern France and Italian</a></div>
								</div>
								<div class="product_box">
									<div class="product_img"><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0012&regsCd=0012-0031&beginDt=2018/05/24&endDt=2099/12/31&allowJoin=1&ikeyword=%E8%97%8D%E7%B7%9A%E3%80%8C%E6%9D%B1%E6%AD%90%E7%92%80%E7%92%A8%E3%80%8D" target="_top"><img src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/001273/001274/001278/001279/00015628.JPG"></a></div>
									<div class="product_name"><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0012&regsCd=0012-0031&beginDt=2018/05/24&endDt=2099/12/31&allowJoin=1&ikeyword=%E8%97%8D%E7%B7%9A%E3%80%8C%E6%9D%B1%E6%AD%90%E7%92%80%E7%92%A8%E3%80%8D" target="_top">藍線「東歐璀璨」<br/>Eastern Europe</a></div>
								</div>
								<div class="product_box">
									<div class="product_img"><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0012&regsCd=0012-0031&beginDt=2018/05/24&endDt=2099/12/31&allowJoin=1&ikeyword=%E7%B4%AB%E7%B7%9A%E3%80%8C%E8%8B%B1%E5%80%AB%E8%B2%B4%E6%97%8F%E3%80%8D" target="_top"><img src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/001273/001274/001278/001279/00015627.JPG"></a></div>
									<div class="product_name"><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0012&regsCd=0012-0031&beginDt=2018/05/24&endDt=2099/12/31&allowJoin=1&ikeyword=%E7%B4%AB%E7%B7%9A%E3%80%8C%E8%8B%B1%E5%80%AB%E8%B2%B4%E6%97%8F%E3%80%8D" target="_top">紫線「英倫貴族」<br/>England</a></div>
								</div>
								<!--div class="product_box">
									<div class="product_img"><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0012&regsCd=0012-0031&beginDt=2018/05/24&endDt=2099/12/31&allowJoin=1&ikeyword=%E9%BB%83%E7%B7%9A%E3%80%8C%E7%86%B1%E6%83%85%E8%A5%BF%E8%91%A1%E3%80%8D" target="_top"><img src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/001273/001274/001278/001279/00015626.JPG	"></a></div>
									<div class="product_name"><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0012&regsCd=0012-0031&beginDt=2018/05/24&endDt=2099/12/31&allowJoin=1&ikeyword=%E9%BB%83%E7%B7%9A%E3%80%8C%E7%86%B1%E6%83%85%E8%A5%BF%E8%91%A1%E3%80%8D" target="_top">黃線「熱情西葡」<br/>West Portugal</a></div>
								</div-->
								<div class="product_box">
									<div class="product_img"><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0012&regsCd=0012-0031&beginDt=2018/05/24&endDt=2099/12/31&allowJoin=1&ikeyword=%E9%BB%83%E7%B7%9A%E3%80%8C%E7%86%B1%E6%83%85%E8%A5%BF%E8%91%A1%E3%80%8D" target="_top"><img src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/001273/001274/001278/001279/00015625.JPG"></a></div>
									<div class="product_name"><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0012&regsCd=0012-0031&beginDt=2018/05/24&endDt=2099/12/31&allowJoin=1&ikeyword=%E9%BB%83%E7%B7%9A%E3%80%8C%E7%86%B1%E6%83%85%E8%A5%BF%E8%91%A1%E3%80%8D" target="_top">橙線「普羅旺斯」<br/>Provence</a></div>
								</div>
								<div class="product_box">
									<div class="product_img"><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0012&regsCd=0012-0031&beginDt=2018/05/24&endDt=2099/12/31&allowJoin=1&ikeyword=%E7%B2%89%E7%B7%9A%E3%80%8C%E5%86%B0%E5%B7%9D%E5%B3%BD%E7%81%A3%E3%80%8D" target="_top"><img src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/001273/001274/001278/001279/00015624.JPG"></a></div>
									<div class="product_name"><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0012&regsCd=0012-0031&beginDt=2018/05/24&endDt=2099/12/31&allowJoin=1&ikeyword=%E7%B2%89%E7%B7%9A%E3%80%8C%E5%86%B0%E5%B7%9D%E5%B3%BD%E7%81%A3%E3%80%8D" target="_top">粉線「冰川峽灣」<br/>Nordic</a></div>
								</div>
							</div>
							</div>
							</div>
							</div>
							</div>
						</div>
                    </div>    
                        
							
						
						 
				</div>			
	
        <!--MAIN-->
		
		</section>
		
	</article>
    
    
	
    
    <article class="footer">
    <!--#include virtual="/eweb_summittour/public/footerUP2.asp" -->
    </article>
</div>	


<div class="service_box">
    <ul>
        <li><button class="btn btn-success" onclick="location.href='tel:0423052639'"><i class="fa fa-phone" aria-hidden="true"></i> 手機直撥</button></li>
        <li><button class="btn btn-danger" onclick="location.href='/eweb_summittour/diy/V_diy.asp'"><i class="fa fa-envelope" aria-hidden="true"></i> 聯絡我們</button></li>
    </ul>
</div>
<div class="actGotop"><a href="javascript:;" title="返回頂部"><i class="fa fa-angle-up" aria-hidden="true"></i>TOP</a></div>

<script src="/eweb_summittour/js/share_main.js"></script>

<script type="text/javascript">
$(document).ready( function() {
	getXML(true,'');
});
</script>
<!--自動上架置入-->
<% 
            if returl1="" then returl1="/eWeb/Main/home.asp"
            if returl2="" then returl2="/eWeb/Main/home.asp"
            If (Session("USR_CLS") = "A") Then
        '判斷是否為同業會員
           %>
           <!--如果是同業會員，就秀下面資料-->
         <script>
			$(function(){
				$("#tab1").load("/eweb_summittour/DHTML/AGT/UPR00001/index.asp .metropolis",function(){$("html");});
				
				});
			</script>
          <% else%>
          <!--如果不是，就秀下面資料-->
          <script>
			$(function(){
				$("#tab1").load("/eweb_summittour/DHTML/UPR00001/index.asp .metropolis",function(){$("html");});
				});
			</script>
			
          
          <% End If %>
<!--Include flickerplate-->
<script language="javascript" type="text/javascript" src="/eweb_summittour/js/jquery.cookie.js"></script>
<!--圖文/條列式切換-->
<script type="text/javascript">
//<![CDATA[
    $(function(){
        var $li =$("#skin li");
        $li.click(function(){
            switchSkin( this.id );
        });
        var cookie_skin = $.cookie( "MyCssSkin");
        if (cookie_skin) {
            switchSkin( cookie_skin );
        }
    });
    
    function switchSkin(skinName){
        $("#"+skinName).addClass("selected") //當前<li>元素選中
        .siblings().removeClass("selected"); //去掉其它同輩<li>元素的選中
        $("#theme_content").attr("class", "" + skinName + "");//設置不同classname
        $.cookie( "MyCssSkin" , skinName , { path: '/', expires: 30 });
    }
//]]>
</script>
<script src="/eweb_summittour/js/jquery.superslides.js" type="text/javascript" charset="utf-8"></script>

<script type="text/javascript">

    $(function() {
      var $slides = $('#slides');

      Hammer($slides[0]).on("swipeleft", function(e) {
        $slides.data('superslides').animate('next');
      });

      Hammer($slides[0]).on("swiperight", function(e) {
        $slides.data('superslides').animate('prev');
      });

      $slides.superslides({
        hashchange: true,
        play: 8000
      });

      $('#slides').on('mouseenter', function() {
        $(this).superslides('stop');
        console.log('Stopped')
      });
      $('#slides').on('mouseleave', function() {
        $(this).superslides('start');
        console.log('Started')
      });

    });

//分頁頁籤
$('#myTab a').click(function (e) {
  e.preventDefault()
  $(this).tab('show')
})

setTimeout(function(){
            $('.carousel').carousel({
                interval: 8000
            })
        }, 5000);
//回頂點
$(function(){   
    $(window).scroll(function() {       
        if($(window).scrollTop() >= 100){
            $('.actGotop').fadeIn(300); 
        }else{    
            $('.actGotop').fadeOut(300);    
        }  
    });
    $('.actGotop').click(function(){
    $('html,body').animate({scrollTop: '0px'}, 800);}); 
    $('#go_search').click(function(){   $('html,body').animate({scrollTop:$('#search').offset().top}, 900); return false;   })
});

$(function(){   
    var cubuk_seviye = $(document).scrollTop();
    var header_yuksekligi = $('.header').outerHeight();

    $(window).scroll(function() {
        var kaydirma_cubugu = $(document).scrollTop();

        if (kaydirma_cubugu > header_yuksekligi){$('.header').addClass('gizle');} 
        else {$('.header').removeClass('gizle');}

        if (kaydirma_cubugu > cubuk_seviye){$('.header').removeClass('sabit');} 
        else {$('.header').addClass('sabit');}              

        cubuk_seviye = $(document).scrollTop(); 
     });
     
});


$(function(){
    var sideslider = $('[data-toggle=collapse-side]');
    var get_sidebar = sideslider.attr('data-target-sidebar');
    var get_content = sideslider.attr('data-target-content');
    sideslider.click(function(event){
      $(get_sidebar).toggleClass('in');
      $(get_content).toggleClass('out');
   });

});
$(function(){
    var sideslider = $('[data-toggle=collapse-side1]');
    var get_sidebar = sideslider.attr('data-target-sidebar1');
    var get_content = sideslider.attr('data-target-content1');
    sideslider.click(function(event){
      $(get_sidebar).toggleClass('in');
      $(get_content).toggleClass('out');
   });

});

</script>
</body>
</html>

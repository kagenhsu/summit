﻿<!DOCTYPE html><!-- lang="zh-TW"-->
<html class="no-js">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta charset="utf-8">
<title>聖誕&跨年連假優惠</title>
<!-- for Facebook -->          
<meta property="og:url"                content="http://www.summittour.com.tw/eWeb_summittour/auto/Central_tours_UP10.asp" />
<meta property="og:type"               content="website" />
<meta property="og:locale"             content="zh_TW" />
<meta property="og:title"              content="頂尖旅遊限定優惠" />
<meta property="og:description"        content="聖誕&跨年連假優惠" />
<meta property="og:image"              content="縮圖的連結位置" />
<!-- for Facebook -->  
<link rel="shortcut icon" type="image/x-icon" href="/eweb_summittour/images/default/favicon.ico">
<link rel="bookmark" type="image/x-icon" href="/eweb_summittour/images/default/favicon.ico">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="description" content="聖誕&跨年連假優惠"><!--網站描述-->
<meta name="keywords" content="聖誕&跨年連假優惠"><!--關鍵字-->
<meta name="Robots" content="all" />
<meta name="Googlebot" content="index,follow" />
<meta name="Audience" content="All" />
<meta name="Page-topic" content="Internet" />
<meta name="Rating" content="General" />
<meta name="Resource-Type" content="document" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="windows-Target" content="_top" />
<meta name="Author" content="GTSHAYO" />
<meta name="Copyright" content="Copyright c 2017 www.summittour.com.tw All Rights Reserved." />
<meta name="Generator" content="Visual Studio Code" />
<meta name="Publisher" content="超悠旅行社" />
<meta name="Creation-Date" content="06-23-2017" />
<meta name="WEBCRAWLERS" content="ALL" />
<meta name="SPIDERS" content="ALL" />
<meta name="revisit-after" content="7 days"/>

<link type="text/css" rel="stylesheet" href="/eweb_summittour/css/reset_new.css">
<link type="text/css" rel="stylesheet" href="/eweb_summittour/css/normalize.css">
<link type="text/css" rel="stylesheet" href="/eweb_summittour/css/bootstrap.css">
<link type="text/css" rel="stylesheet" href="/eweb_summittour/css/font-awesome.min.css">
<!--<link rel="stylesheet" type="text/css" href="/eweb_summittour/css/loaders.css"/>-->
<link rel="stylesheet" type="text/css" href="/eweb_summittour/css/main.css">
<link type="text/css" rel="stylesheet" href="/eweb_summittour/css/defaultUP2.css">


<script type="text/javascript" language="javascript" src="/eweb_summittour/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" language="javascript" src="/eweb_summittour/js/bootstrap.min.js"></script>
<!--<script type="text/javascript" language="javascript" src="/eweb_summittour/js/jquery.nicescroll.min.js"></script>-->
<script type="text/javascript" language="javascript" src="/eweb_summittour/js/modernizr.js"></script>
<script type="text/javascript" language="javascript" src="/eweb_summittour/js/all.js"></script>
<script type="text/javascript" language="javascript" src="/eweb_summittour/js/hammer.min.js"></script>




<link href="/eweb_summittour/auto/autopage.css" rel="stylesheet" type="text/css">
<style type="text/css">
#skin {display: table; float: left;}
#skin li {float: left; font-size:  20px; color: #999999; padding: 0 5px; cursor: pointer;}
#skin li i {padding: 0 5px;}
#skin li:hover {color: #74a73a;}
#skin li.selected {color: #74a73a; cursor: default;}
</style>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-99295320-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-99295320-1');
</script>

<script type="text/javascript" language="javascript" src="/eweb_summittour/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script language="javascript" type="text/javascript" src="/EW/Script/GroupDetailJs.js"></script>
</head>
<!--網頁背景顏色或圖片設定--
<style type="text/css">
.wrapper {
    background: #ff46433d;
	clip: rect(0px,0px,0px,0px);
	
}
</style>
<!--網頁背景顏色或圖片設定-->

<body>


<!-- Loader動畫 -->
<!--<div id="loader-wrapper">
    <div id="loader"></div>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
</div>-->
<!-- Loader動畫 -->
<div class="wrapper">
	 
	<!--HEADER START--> 
	<article class="header"> 
		<!--以參數 fbhtml_url作為存取本視窗網址字串參數。-->
			<script>
			var fbhtml_url=window.location.toString();
			</script>

			<nav id="top" class="navbar navbar-fixed-top navbar-inverse">
				<div class="container">
					<div class="row">
					
						<div class="navbar-header">
							<button data-toggle="collapse-side" data-target-sidebar=".side-collapse-left" data-target-content=".side-collapse-container-left" type="button" class="navbar-toggle pull-left">
								<a class="nav-btn" id="nav-open-btn" href="#nav"></a>
							</button>
							<h2 class="block-title">
								<a href="http://www.summittour.com.tw"><img src="/eWeb_summittour/images/default/logo.png"/></a>
								<!--<a class="navbar-brand" href="#">Design</a>-->
							</h2>
							<div class="navbar-inverse1 side-collapse-right in">
								<nav id="nav1" role="navigation" class="navbar-collapse1">
									<ul class="nav navbar-nav1 navbar-right ">
										<li class="">
											<a href="/eWeb_summittour/page/member.asp"title="加入會員"><p>加入會員</p></a>
										</li>
										<li class="through">
											<a href="/eWeb_summittour/index.asp"title="回頂尖首頁"><p>回頂尖首頁</p></a>
										</li>
										<li class="through">
											<!--FB分享按鈕(純HTML語法)-->
											<a target="_blank" href="http://www.facebook.com/share.php?u=http://28371.psee.io/7H2VV">
											<img title="分享到FB！" src="/eWeb_summittour/images/default/icon_top_FB-02.png" border="0" width="30"></a>
										</li>
										<li class="through">
											<!--line分享按鈕(純HTML語法)-->
											<a target="_blank" href="https://social-plugins.line.me/lineit/share?url=http://28371.psee.io/7H2VV">
											<img title="分享到LINE！" src="/eWeb_summittour/images/default/icon_top_line-02.png" border="0" width="30"></a>
										</li>
										<li class="through">
											<!--Twitter分享按鈕(純HTML語法)-->
											<a target="_blank" href="http://twitter.com/home/?status=http://28371.psee.io/7H2VV">
											<img title="分享到維特！" src="/eWeb_summittour/images/default/icon_top_Twitter-02.png" border="0" width="30"></a>				
										</li>
										<li class="through">
											<!--Google+分享按鈕(純HTML語法)-->
											<a target="_blank" href="https://plus.google.com/share?url=http://28371.psee.io/7H2VV">
											<img title="分享到Google+！" src="/eWeb_summittour/images/default/icon_top_G-02.png" border="0" width="30"></a>
										</li>
										<!--會員登入-->
										
											<%If (Session("USR_ID") <> "") Then%>
												<li class="top_member hidden-md">
													<span style="color: #690;">HI， &nbsp;<%=Session("USR_CNM")%> 歡迎您！ &nbsp;</span>
												</li>
												<li class="top_member hidden-md">
													<span><a href="/eweb/Main/O_Logout.asp" target="_top"><span>登出&nbsp;</span></a></span>
												</li>
												</li>
												
												
											<%elseif Session("COMP_CD") <> "" then%>
												<li class="top_member hidden-md">
													<span><a href="/eWeb_summittour/page/member.asp"></a></span>
												</li>
												<li class="top_member hidden-md">
													<span style="color: #690;">>HI， &nbsp;<%=COMP_ANM%> 歡迎您！ &nbsp;</span>
												</li>
												<li class="top_member hidden-md">
													<span><a href="/eweb/Main/O_Logout.asp" target="_top"><span>登出&nbsp;</span></a></span>
												
												
											<%End If %>
										<!--會員登入-END-->
										
									</ul>
								</nav>
							</div>
							
							<div class="navbar-inverse side-collapse-left in">

								<nav id="nav" role="navigation" class="navbar-collapse">
									<ul class="nav navbar-nav navbar-left">
										<li><a href="#DAREA00001">港澳大陸</a></li>
										<li><a href="#DAREA00017">蒙古</a></li>
										<li><a href="#DAREA00004">東南亞</a></li>
										<li><a href="#DAREA00002">東北亞</a></li>
										<li><a href="#DAREA00009">南亞、中亞非</a></li>
										<li><a href="#DAREA00010">歐洲</a></li>
										<li><a href="#DAREA00011">紐澳</a></li>
										<li><a href="#DAREA00014">美加</a></li>
										<li><a href="#DAREA00015">海島</a></li>
										<li><a href="#DAREA00016">中南半島</a></li>
										<li><a href="#DAREA00007">國內離島</a></li>
										<li><a href="#DAREA00008">遊輪</a></li>
										<ul id="skin" class="align-self-end" style="margin-top: 10px;">
											<li id="skin_module" title="區塊式" class="selected hidden-lg" style="color:#fff;text-align:center;">
												<i class="fa fa-th-large" aria-hidden="true" style="color:#fff;left: -50%;"></i>圖片模式
											</li>
											<li id="skin_list" title="條列式" class="hidden-lg" style="color:#fff;text-align:center;">
												<i class="fa fa-th-list" aria-hidden="true" style="color:#fff;left: -50%;"></i>列表模式
											</li>
										</ul>
										<li class="hidden-lg">
											<a href="/eWeb_summittour/page/member.asp"><p>加入會員</p></a>
										</li>
										<li class="hidden-lg">
											<a href="/eWeb_summittour/index.asp"><p>回首頁</p></a>
										</li>
										<li class="hidden-lg">
											<!--FB分享按鈕(純HTML語法)-->
											<a target="_blank" href="http://www.facebook.com/share.php?u=http://28371.psee.io/7H2VV">
											<p><img title="分享到FB！" src="/eWeb_summittour/images/default/icon_top_FB-02.png" border="0" width="20"style="margin-right: 5px;">FB分享</p></a>
										</li>
										<li class="hidden-lg">
											<!--line分享按鈕(純HTML語法)-->
											<a target="_blank" href="https://social-plugins.line.me/lineit/share?url=http://28371.psee.io/7H2VV">
											<p><img title="分享到LINE！" src="/eWeb_summittour/images/default/icon_top_line-02.png" border="0" width="20"style="margin-right: 5px;">LINE分享</p></a>
										</li>
										<li class="hidden-lg">
											<!--Twitter分享按鈕(純HTML語法)-->
											<a target="_blank" href="http://twitter.com/home/?status=http://28371.psee.io/7H2VV">
											<p><img title="分享到維特！" src="/eWeb_summittour/images/default/icon_top_Twitter-02.png" border="0" width="20"style="margin-right: 5px;">維特分享</p></a>				
										</li>
										<li class="hidden-lg">
											<!--Google+分享按鈕(純HTML語法)-->
											<a target="_blank" href="https://plus.google.com/share?url=http://28371.psee.io/7H2VV">
											<p><img title="分享到Google+！" src="/eWeb_summittour/images/default/icon_top_G-02.png" border="0" width="20"style="margin-right: 5px;">Google+分享</p></a>
										</li>
										<!--會員登入-->
										<%If (Session("USR_ID") <> "") Then%>
										<li class="hidden-lg hidden-md">
										<span style="color: #690;">親愛的 &nbsp;<%=Session("USR_CNM")%> 您好~ &nbsp;</span>
											<a href="/eweb/Main/O_Logout.asp" target="_top"><span style="color: #690;">登出</span></a>
										</li>
										<%elseif Session("COMP_CD") <> "" then%>
										<li class="hidden-lg hidden-md">
											<a href="/eWeb_summittour/page/member.asp"></a>
										<span style="color: #690;">親愛的 &nbsp;<%=COMP_ANM%> 您好~ &nbsp;</span>
											<a href="/eweb/Main/O_Logout.asp" target="_top"><span style="color: #690;">登出</span></a>
										</li>
										<%End If %>
										<!--會員登入-END-->
																
										
									</ul>
								</nav>
							</div>
							
							<!--button data-toggle="collapse-side1" data-target-sidebar1=".side-collapse-right" data-target-content1=".side-collapse-container-right" type="button" class="navbar-toggle1 pull-right">
								<a class="nav-btn" id="nav-open-btn" href="#nav1"></a>
							</button-->
					  </div>
					</div>
				  </div>
			</nav>
				
			<!-- Modal -->
			<div class="modal fade bs-example-modal-sm" id="myMember" tabindex="-1" role="dialog" aria-labelledby="myMemberLabel">
			  <div class="modal-dialog modal-sm" role="document">
				<div class="modal-content">
				  <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myMemberLabel">會員中心</h4>
				  </div>
				  <div class="modal-body">
					<!--#include virtual="/eWeb_summittour/public/login.asp"-->
				  </div>
				</div>
			  </div>
			</div>
	</article>
    <article class="summittour_carousel">
        <div class="row">
			<!--活動標題動畫-->
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<img src="/eWeb_summittour/IMGDB/000000/001064/001067/00011367.JPG" style="width:100%">
            </div>
			<!--活動標題動畫-->
        </div>
    </article>
	<article class="container">
        <!--麵包屑及頁面標題--
		<div class="row">
            <ol class="breadcrumb" id="auto_breadcrumb">
                <li>目前位置：</li>
                <li class="active"><a href="/eWeb_summittour/home.asp">首頁</a></li>
                <li class="active">過年限時優惠</li>
            </ol>			
		</div>
		<!--麵包屑及頁面標題-->
	</article>
    <article class="exercise_min container product_types">
	
		<section>
				<div class="central_tours row">
					<div class="day_style_1 product_basic_info animatedParent article">
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<img src="/eWeb_summittour/IMGDB/000000/001301/001302/001306/00015973.PNG" style="width:100%">
						</div> 
					</div>				
					
					<div id="navbar-spy" class="nav navbar-default onnavbar">
                        <div id="cart" class="container-fluid" style="position: absolute; top: 0px;">
                            <div class="collapse navbar-collapse" id="myNavbar">
                                <div class="row">
                                <ul class="nav navbar-nav">
									<li class="active"><a href="#DAREA00001">港澳大陸</a></li>						
									<li class=""><a href="#DAREA00017">蒙古</a></li>
									<li class=""><a href="#DAREA00004">東南亞</a></li>
									<li class=""><a href="#DAREA00002">東北亞</a></li>									
									<li class=""><a href="#DAREA00009">南亞、中亞非</a></li>
									<li class=""><a href="#DAREA00010">歐洲</a></li>
									<li class=""><a href="#DAREA00011">紐澳</a></li>
									<li class=""><a href="#DAREA00014">美加</a></li>
									<li class=""><a href="#DAREA00015">海島</a></li>
									<li class=""><a href="#DAREA00016">中南半島</a></li>
									<li class=""><a href="#DAREA00007">國內離島</a></li>
									<li class=""><a href="#DAREA00008">遊輪</a></li>
                                </ul>
								<ul id="skin" class="align-self-end" style="margin-top: 10px;">
									<li id="skin_module" title="區塊式" class="selected"">
										<i class="fa fa-th-large" aria-hidden="true"></i>圖片模式
									</li>
									<li id="skin_list" title="條列式">
										<i class="fa fa-th-list" aria-hidden="true"></i>列表模式
									</li>
								</ul>  
                                
                                </div>
                            </div>
                        </div>
                    </div>
					<div class="product_content"> 		
						
						<div id="theme_content" class="skin_list">
							<div id="tab1"></div>
							<div id="tab2"></div>
							<div id="tab3"></div>
							<div id="tab4"></div>
							<div id="tab5"></div>
							<div id="tab6"></div>
							<div id="tab7"></div>
							<div id="tab8"></div>
							<div id="tab9"></div>
							<div id="tab10"></div>
							<div id="tab12"></div>
							<div id="tab13"></div>
							<div id="tab14"></div>
							<div id="tab15"></div>
							<div id="tab16"></div>
							<div id="tab17"></div>
							<div id="tab18"></div>
							<div id="tab19"></div>
							<div id="tab20"></div>
						</div>
                    </div>    
                        
							
						
						 
				</div>			
	
        <!--MAIN-->
		
		</section>
		
	</article>
    
    
	
    
    <article class="footer">
    <!--#include virtual="/eweb_summittour/public/footerUP2.asp" -->
    </article>
</div>	


<div class="service_box">
    <ul>
        <li><button class="btn btn-success" onclick="location.href='tel:0423052639'"><i class="fa fa-phone" aria-hidden="true"></i> 手機直撥</button></li>
        <li><button class="btn btn-danger" onclick="location.href='/eweb_summittour/diy/V_diy.asp'"><i class="fa fa-envelope" aria-hidden="true"></i> 聯絡我們</button></li>
    </ul>
</div>
<div class="actGotop"><a href="javascript:;" title="返回頂部"><i class="fa fa-angle-up" aria-hidden="true"></i>TOP</a></div>

<script src="/eweb_summittour/js/share_main.js"></script>

<script type="text/javascript">
$(document).ready( function() {
	getXML(true,'');
});
</script>
<!--自動上架置入-->
<% 
            if returl1="" then returl1="/eWeb/Main/home.asp"
            if returl2="" then returl2="/eWeb/Main/home.asp"
            If (Session("USR_CLS") = "A") Then
        '判斷是否為同業會員
           %>
           <!--如果是同業會員，就秀下面資料-->
         <script>
			$(function(){
				$("#tab1").load("/eweb_summittour/DHTML/AGT/UP000010/index.asp .metropolis",function(){$("html");});
				
				});
			</script>
          <% else%>
          <!--如果不是，就秀下面資料-->
          <script>
			$(function(){
				$("#tab1").load("/eweb_summittour/DHTML/UP000010/index.asp .metropolis",function(){$("html");});
				});
			</script>
			
          
          <% End If %>
<!--Include flickerplate-->
<script language="javascript" type="text/javascript" src="/eweb_summittour/js/jquery.cookie.js"></script>
<!--圖文/條列式切換-->
<script type="text/javascript">
//<![CDATA[
    $(function(){
        var $li =$("#skin li");
        $li.click(function(){
            switchSkin( this.id );
        });
        var cookie_skin = $.cookie( "MyCssSkin");
        if (cookie_skin) {
            switchSkin( cookie_skin );
        }
    });
    
    function switchSkin(skinName){
        $("#"+skinName).addClass("selected") //當前<li>元素選中
        .siblings().removeClass("selected"); //去掉其它同輩<li>元素的選中
        $("#theme_content").attr("class", "" + skinName + "");//設置不同classname
        $.cookie( "MyCssSkin" , skinName , { path: '/', expires: 30 });
    }
//]]>
</script>
<script src="/eweb_summittour/js/jquery.superslides.js" type="text/javascript" charset="utf-8"></script>

<script type="text/javascript">

    $(function() {
      var $slides = $('#slides');

      Hammer($slides[0]).on("swipeleft", function(e) {
        $slides.data('superslides').animate('next');
      });

      Hammer($slides[0]).on("swiperight", function(e) {
        $slides.data('superslides').animate('prev');
      });

      $slides.superslides({
        hashchange: true,
        play: 8000
      });

      $('#slides').on('mouseenter', function() {
        $(this).superslides('stop');
        console.log('Stopped')
      });
      $('#slides').on('mouseleave', function() {
        $(this).superslides('start');
        console.log('Started')
      });

    });

//分頁頁籤
$('#myTab a').click(function (e) {
  e.preventDefault()
  $(this).tab('show')
})

setTimeout(function(){
            $('.carousel').carousel({
                interval: 8000
            })
        }, 5000);
//回頂點
$(function(){   
    $(window).scroll(function() {       
        if($(window).scrollTop() >= 100){
            $('.actGotop').fadeIn(300); 
        }else{    
            $('.actGotop').fadeOut(300);    
        }  
    });
    $('.actGotop').click(function(){
    $('html,body').animate({scrollTop: '0px'}, 800);}); 
    $('#go_search').click(function(){   $('html,body').animate({scrollTop:$('#search').offset().top}, 900); return false;   })
});

$(function(){   
    var cubuk_seviye = $(document).scrollTop();
    var header_yuksekligi = $('.header').outerHeight();

    $(window).scroll(function() {
        var kaydirma_cubugu = $(document).scrollTop();

        if (kaydirma_cubugu > header_yuksekligi){$('.header').addClass('gizle');} 
        else {$('.header').removeClass('gizle');}

        if (kaydirma_cubugu > cubuk_seviye){$('.header').removeClass('sabit');} 
        else {$('.header').addClass('sabit');}              

        cubuk_seviye = $(document).scrollTop(); 
     });
     
});


$(function(){
    var sideslider = $('[data-toggle=collapse-side]');
    var get_sidebar = sideslider.attr('data-target-sidebar');
    var get_content = sideslider.attr('data-target-content');
    sideslider.click(function(event){
      $(get_sidebar).toggleClass('in');
      $(get_content).toggleClass('out');
   });

});
$(function(){
    var sideslider = $('[data-toggle=collapse-side1]');
    var get_sidebar = sideslider.attr('data-target-sidebar1');
    var get_content = sideslider.attr('data-target-content1');
    sideslider.click(function(event){
      $(get_sidebar).toggleClass('in');
      $(get_content).toggleClass('out');
   });

});

</script>
</body>
</html>

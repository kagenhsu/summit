﻿<!DOCTYPE html><!-- lang="zh-TW"-->
<html class="no-js">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta charset="utf-8">
<title>線上旅展優惠</title>
<!-- for Facebook -->          
<meta property="og:url"                content="http://www.summittour.com.tw/eWeb_summittour/auto/Central_tours_OTS.asp" />
<meta property="og:type"               content="website" />
<meta property="og:locale"             content="zh_TW" />
<meta property="og:title"              content="線上旅展優惠" />
<meta property="og:description"        content="今年旅展限定優惠" />
<meta property="og:image"              content="縮圖的連結位置" />
<!-- for Facebook -->  
<link rel="shortcut icon" type="image/x-icon" href="/eweb_summittour/images/default/favicon.ico">
<link rel="bookmark" type="image/x-icon" href="/eweb_summittour/images/default/favicon.ico">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="description" content="今年旅展限定優惠"><!--網站描述-->
<meta name="keywords" content="線上旅展優惠"><!--關鍵字-->
<meta name="Robots" content="all" />
<meta name="Googlebot" content="index,follow" />
<meta name="Audience" content="All" />
<meta name="Page-topic" content="Internet" />
<meta name="Rating" content="General" />
<meta name="Resource-Type" content="document" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="windows-Target" content="_top" />
<meta name="Author" content="GTSHAYO" />
<meta name="Copyright" content="Copyright c 2017 www.summittour.com.tw All Rights Reserved." />
<meta name="Generator" content="Visual Studio Code" />
<meta name="Publisher" content="超悠旅行社" />
<meta name="Creation-Date" content="06-23-2017" />
<meta name="WEBCRAWLERS" content="ALL" />
<meta name="SPIDERS" content="ALL" />
<meta name="revisit-after" content="7 days"/>

<link type="text/css" rel="stylesheet" href="/eweb_summittour/css/reset_new.css">
<link type="text/css" rel="stylesheet" href="/eweb_summittour/css/normalize.css">
<link type="text/css" rel="stylesheet" href="/eweb_summittour/css/bootstrap.css">
<link type="text/css" rel="stylesheet" href="/eweb_summittour/css/font-awesome.min.css">
<!--<link rel="stylesheet" type="text/css" href="/eweb_summittour/css/loaders.css"/>-->
<link rel="stylesheet" type="text/css" href="/eweb_summittour/css/main.css">
<link type="text/css" rel="stylesheet" href="/eweb_summittour/css/defaultUP.css">

<script type="text/javascript" language="javascript" src="/eweb_summittour/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" language="javascript" src="/eweb_summittour/js/bootstrap.min.js"></script>
<!--<script type="text/javascript" language="javascript" src="/eweb_summittour/js/jquery.nicescroll.min.js"></script>-->
<script type="text/javascript" language="javascript" src="/eweb_summittour/js/modernizr.js"></script>
<script type="text/javascript" language="javascript" src="/eweb_summittour/js/all.js"></script>
<script type="text/javascript" language="javascript" src="/eweb_summittour/js/hammer.min.js"></script>


<link href="/eweb_summittour/auto/autopage.css" rel="stylesheet" type="text/css">
<style type="text/css">
#skin {display: table; float: left;}
#skin li {float: left; font-size:  20px; color: #999999; padding: 0 5px; cursor: pointer;}
#skin li i {padding: 0 5px;}
#skin li:hover {color: #74a73a;}
#skin li.selected {color: #74a73a; cursor: default;}
</style>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-99295320-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-99295320-1');
</script>


</head>
<body>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5be3ef390e6b3311cb786257/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->

<!-- Loader動畫 -->
<!--<div id="loader-wrapper">
    <div id="loader"></div>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
</div>-->
<!-- Loader動畫 -->
<div class="wrapper2">
	 
	<!--HEADER START-->
	<article class="header"> 
		<!--#include virtual="/eweb_summittour/public/headerUP.asp"--> 
	</article>
    <article class="summittour_carousel">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div id="myCarousel_13" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators" style="width: 100%;">
                        <li data-target="#myCarousel_13" data-slide-to="0" class="active">人氣商品</li>
                        <li data-target="#myCarousel_13" data-slide-to="1" class="">人氣商品</li>
                        <li data-target="#myCarousel_13" data-slide-to="2" class="">人氣商品</li>
                        <li data-target="#myCarousel_13" data-slide-to="3" class="">人氣商品</li>
                        <li data-target="#myCarousel_13" data-slide-to="4" class="">人氣商品</li>
                    </ol>
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <a href="/EW/GO/GroupDetail.asp?prodCd=TYO05170708B" target="_blank">
								<div class="img" style="background-image:url('/eweb_miyabi/images/default/slider_01.jpg');cursor: pointer; width: 20%;"></div>
							</a>
						</div>
                        <div class="item">
                            <a href="/EW/GO/GroupDetail.asp?prodCd=TYO05170826A" target="_blank">
								<div class="img" style="background-image:url('/eweb_miyabi/images/default/slider_01.jpg');cursor: pointer; width: 20%;"></div>
                            </a>
                        </div>
                        <div class="item">
                            <a href="/eweb_miyabi/starcruises/index.asp" target="_blank">
								<div class="img" style="background-image:url('/eweb_miyabi/images/default/slider_01.jpg');cursor: pointer; width: 20%;"></div>
                            </a>
                        </div>
                        <div class="item">
                            <a href="/eweb_miyabi/starcruises/index.asp" target="_blank">
								<div class="img" style="background-image:url('/eweb_miyabi/images/default/slider_01.jpg');cursor: pointer; width: 20%;"></div>
                            </a>
                        </div>
                        <div class="item">
                            <a href="/eweb_miyabi/starcruises/index.asp" target="_blank">
								<div class="img" style="background-image:url('/eweb_miyabi/images/default/slider_01.jpg');cursor: pointer; width: 20%;"></div>
                            </a>
                        </div>
                    </div>

                    <!-- Left and right controls -->
                    <a class="left carousel-control" href="#myCarousel_15" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#myCarousel_15" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </article>
	<article class="container">
        <div class="row">
			<!--麵包屑及頁面標題-->
            <ol class="breadcrumb" id="auto_breadcrumb">
                <li>目前位置：</li>
                <li class="active"><a href="/eWeb_summittour/home.asp">首頁</a></li>
                <li class="active">線上旅展</li>
            </ol>
			<ul class="navbar-other">
				<li>
					<!--FB分享按鈕(純HTML語法)-->
					<a target="_blank" href="http://www.facebook.com/share.php?u=http://www.summittour.com.tw/eWeb_summittour/auto/Central_tours_OTS.asp">
					<img title="分享到FB！" src="/eWeb_summittour/images/default/icon_top_FB-02.png" border="40" width="40" /></a>
                </li>
                <li>
					<!--line分享按鈕(純HTML語法)-->
					<a target="_blank" href="https://social-plugins.line.me/lineit/share?url=http://www.summittour.com.tw/eWeb_summittour/auto/Central_tours_OTS.asp">
					<img title="分享到LINE！" src="/eWeb_summittour/images/default/icon_top_line-02.png" border="40" width="40" /></a>
                </li>
				<li>
					<!--Twitter分享按鈕(純HTML語法)-->
					<a target="_blank" href="http://twitter.com/home/?status=http://www.summittour.com.tw/eWeb_summittour/auto/Central_tours_OTS.asp">
					<img title="分享到維特！" src="/eWeb_summittour/images/default/icon_top_Twitter-02.png" border="40" width="40" /></a>				
				</li>
				<li>
					<!--Google+分享按鈕(純HTML語法)-->
					<a target="_blank" href="https://plus.google.com/share?url=http://www.summittour.com.tw/eWeb_summittour/auto/Central_tours_OTS.asp">
					<img title="分享到Google+！" src="/eWeb_summittour/images/default/icon_top_G-02.png" border="40" width="40" /></a>
				</li>
           </ul>
		</div>
	</article>
	
	
	
    <article class="exercise_min container">
	
		
		<section>
		
		
         
					
				<div class="central_tours">
                          <ul id="skin">
                                <li id="skin_module" title="區塊式" class="selected">
                                	<i class="fa fa-th-large" aria-hidden="true"></i>圖片模式
                                </li>
                                <li id="skin_list" title="條列式">
                                	<i class="fa fa-th-list" aria-hidden="true"></i>列表模式
                                </li>
                            </ul>  
                         
						<div id="theme_content" class="skin_list">
								<div id="tab1"></div>
								<div id="tab2"></div>
								<div id="tab3"></div>
								<div id="tab4"></div>
								<div id="tab5"></div>
								<div id="tab6"></div>
								<div id="tab7"></div>
								<div id="tab8"></div>
								<div id="tab9"></div>
								<div id="tab10"></div>
								<div id="tab12"></div>
								<div id="tab13"></div>
								<div id="tab14"></div>
								<div id="tab15"></div>
								<div id="tab16"></div>
								<div id="tab17"></div>
								<div id="tab18"></div>
								<div id="tab19"></div>
								<div id="tab20"></div>
                         </div>
                         
                        
							
						
						 
				</div>
			</div>
	
        <!--MAIN-->
		
		</section>
		
	</article>
    
    
	
    
    <article class="footer">
    <!--#include virtual="/eweb_summittour/public/footer.asp" -->
    </article>
</div>	


<div class="service_box">
    <ul>
        <li><button class="btn btn-success" onclick="location.href='tel:0423052639'"><i class="fa fa-phone" aria-hidden="true"></i> 手機直撥</button></li>
        <li><button class="btn btn-danger" onclick="location.href='/eweb_summittour/diy/V_diy.asp'"><i class="fa fa-envelope" aria-hidden="true"></i> 聯絡我們</button></li>
    </ul>
</div>
<div class="actGotop"><a href="javascript:;" title="返回頂部"><i class="fa fa-angle-up" aria-hidden="true"></i>TOP</a></div>

<script src="/eweb_summittour/js/share_main.js"></script>

<script type="text/javascript">
$(document).ready( function() {
	getXML(true,'');
});
</script>
<!--自動上架置入-->
<% 
            if returl1="" then returl1="/eWeb/Main/home.asp"
            if returl2="" then returl2="/eWeb/Main/home.asp"
            If (Session("USR_CLS") = "A") Then
        '判斷是否為同業會員
           %>
           <!--如果是同業會員，就秀下面資料-->
         <script>
			$(function(){
				$("#tab1").load("/eweb_summittour/DHTML/AGT/OTS00001/index.asp .metropolis",function(){$("html");});
				
				});
			</script>
          <% else%>
          <!--如果不是，就秀下面資料-->
          <script>
			$(function(){
				$("#tab1").load("/eweb_summittour/DHTML/OTS00001/index.asp .metropolis",function(){$("html");});
				});
			</script>
			
          
          <% End If %>
<!--Include flickerplate-->
<script language="javascript" type="text/javascript" src="/eweb_summittour/js/jquery.cookie.js"></script>
<!--圖文/條列式切換-->
<script type="text/javascript">
//<![CDATA[
    $(function(){
        var $li =$("#skin li");
        $li.click(function(){
            switchSkin( this.id );
        });
        var cookie_skin = $.cookie( "MyCssSkin");
        if (cookie_skin) {
            switchSkin( cookie_skin );
        }
    });
    
    function switchSkin(skinName){
        $("#"+skinName).addClass("selected") //當前<li>元素選中
        .siblings().removeClass("selected"); //去掉其它同輩<li>元素的選中
        $("#theme_content").attr("class", "" + skinName + "");//設置不同classname
        $.cookie( "MyCssSkin" , skinName , { path: '/', expires: 30 });
    }
//]]>
</script>
<script src="/eweb_summittour/js/jquery.superslides.js" type="text/javascript" charset="utf-8"></script>

<script type="text/javascript">

    $(function() {
      var $slides = $('#slides');

      Hammer($slides[0]).on("swipeleft", function(e) {
        $slides.data('superslides').animate('next');
      });

      Hammer($slides[0]).on("swiperight", function(e) {
        $slides.data('superslides').animate('prev');
      });

      $slides.superslides({
        hashchange: true,
        play: 8000
      });

      $('#slides').on('mouseenter', function() {
        $(this).superslides('stop');
        console.log('Stopped')
      });
      $('#slides').on('mouseleave', function() {
        $(this).superslides('start');
        console.log('Started')
      });

    });

//分頁頁籤
$('#myTab a').click(function (e) {
  e.preventDefault()
  $(this).tab('show')
})

setTimeout(function(){
            $('.carousel').carousel({
                interval: 8000
            })
        }, 5000);
//回頂點
$(function(){   
    $(window).scroll(function() {       
        if($(window).scrollTop() >= 100){
            $('.actGotop').fadeIn(300); 
        }else{    
            $('.actGotop').fadeOut(300);    
        }  
    });
    $('.actGotop').click(function(){
    $('html,body').animate({scrollTop: '0px'}, 800);}); 
    $('#go_search').click(function(){   $('html,body').animate({scrollTop:$('#search').offset().top}, 900); return false;   })
});

$(function(){   
    var cubuk_seviye = $(document).scrollTop();
    var header_yuksekligi = $('.header').outerHeight();

    $(window).scroll(function() {
        var kaydirma_cubugu = $(document).scrollTop();

        if (kaydirma_cubugu > header_yuksekligi){$('.header').addClass('gizle');} 
        else {$('.header').removeClass('gizle');}

        if (kaydirma_cubugu > cubuk_seviye){$('.header').removeClass('sabit');} 
        else {$('.header').addClass('sabit');}              

        cubuk_seviye = $(document).scrollTop(); 
     });
     
});


$(function(){
    var sideslider = $('[data-toggle=collapse-side]');
    var get_sidebar = sideslider.attr('data-target-sidebar');
    var get_content = sideslider.attr('data-target-content');
    sideslider.click(function(event){
      $(get_sidebar).toggleClass('in');
      $(get_content).toggleClass('out');
   });

});
$(function(){
    var sideslider = $('[data-toggle=collapse-side1]');
    var get_sidebar = sideslider.attr('data-target-sidebar1');
    var get_content = sideslider.attr('data-target-content1');
    sideslider.click(function(event){
      $(get_sidebar).toggleClass('in');
      $(get_content).toggleClass('out');
   });

});

</script>
</body>
</html>

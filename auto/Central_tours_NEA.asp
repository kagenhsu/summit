﻿<!DOCTYPE html><!-- lang="zh-TW"-->
<html class="no-js">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta charset="utf-8">
<title>超悠旅行社 | 歡迎光臨</title>
<link rel="shortcut icon" type="image/x-icon" href="/eweb_summittour/images/default/favicon.ico">
<link rel="bookmark" type="image/x-icon" href="/eweb_summittour/images/default/favicon.ico">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="description" content="超悠旅行社股份有限公司,電話:04-23052639,傳真:04-23052659,地址:臺中市西區後龍里向上路一段25號3樓,依發展觀光條例之規定，凡經中央主管機關核准，為旅客設計安排國內外旅程、食宿、領隊人員、導遊人員、代購代售交通客票、代辦出國簽證手續等有關服務及旅遊諮詢…等等"><!--網站描述-->
<meta name="keywords" content="悠遊卡,護照,連假,節日,行李箱,假期,旅展,頂尖,頂尖旅遊,超悠,游遍中國,旅遊,超,悠,旅行社,游遍,中國,中國大陸,東北亞,東南亞,南亞,台灣旅遊,台中出發,郵輪，印度，泰國，越南"><!--關鍵字-->
<meta property="og:image" content="http://www.summittour.com.tw/eweb_summittour/images/default/favicon.ico"><!--FB分享圖示-->
<meta property="og:title" content="超悠旅行社 | 歡迎光臨"><!--FB分享標題-->
<meta property="og:url" content="http://www.summittour.com.tw/"><!--FB分享路徑-->
<meta property="og:description" content="超悠旅行社股份有限公司,電話:04-23052639,傳真:04-23052659,地址:臺中市西區後龍里向上路一段25號3樓,依發展觀光條例之規定，凡經中央主管機關核准，為旅客設計安排國內外旅程、食宿、領隊人員、導遊人員、代購代售交通客票、代辦出國簽證手續等有關服務及旅遊諮詢…等等"><!--FB分享內容敘述-->
<meta name="Robots" content="all" />
<meta name="Googlebot" content="index,follow" />
<meta name="Audience" content="All" />
<meta name="Page-topic" content="Internet" />
<meta name="Rating" content="General" />
<meta name="Resource-Type" content="document" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="windows-Target" content="_top" />
<meta name="Author" content="GTSHAYO" />
<meta name="Copyright" content="Copyright c 2017 www.summittour.com.tw All Rights Reserved." />
<meta name="Generator" content="Visual Studio Code" />
<meta name="Publisher" content="超悠旅行社" />
<meta name="Creation-Date" content="06-23-2017" />
<meta name="WEBCRAWLERS" content="ALL" />
<meta name="SPIDERS" content="ALL" />
<meta name="revisit-after" content="7 days"/>

<link type="text/css" rel="stylesheet" href="/eweb_summittour/css/reset_new.css">
<link type="text/css" rel="stylesheet" href="/eweb_summittour/css/normalize.css">
<link type="text/css" rel="stylesheet" href="/eweb_summittour/css/bootstrap.css">
<link type="text/css" rel="stylesheet" href="/eweb_summittour/css/font-awesome.min.css">
<!--<link rel="stylesheet" type="text/css" href="/eweb_summittour/css/loaders.css"/>-->
<link rel="stylesheet" type="text/css" href="/eweb_summittour/css/main.css">
<link type="text/css" rel="stylesheet" href="/eweb_summittour/css/default.css">

<script type="text/javascript" language="javascript" src="/eweb_summittour/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" language="javascript" src="/eweb_summittour/js/bootstrap.min.js"></script>
<!--<script type="text/javascript" language="javascript" src="/eweb_summittour/js/jquery.nicescroll.min.js"></script>-->
<script type="text/javascript" language="javascript" src="/eweb_summittour/js/modernizr.js"></script>
<script type="text/javascript" language="javascript" src="/eweb_summittour/js/all.js"></script>
<script type="text/javascript" language="javascript" src="/eweb_summittour/js/hammer.min.js"></script>


<link href="/eweb_summittour/auto/autopage.css" rel="stylesheet" type="text/css">


<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-99295320-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-99295320-1');
</script>
</head>

<body>


<!-- Loader動畫 -->
<!--<div id="loader-wrapper">
    <div id="loader"></div>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
</div>-->
<!-- Loader動畫 -->
<div class="wrapper">
	 
	<!--HEADER START-->
	<article class="header"> 
		<!--#include virtual="/eweb_summittour/public/header.asp"--> 
	</article>
    <article class="summittour_carousel">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div id="myCarousel" class="carousel slide" data-ride="carousel"><!--myCarousel_03-->
                    <!-- Indicators -->
                    <ol class="carousel-indicators" style="width: 100%;">
                        <li data-target="#myCarousel" data-slide-to="0" class="active">人氣商品</li>
                        <li data-target="#myCarousel" data-slide-to="1" class="">人氣商品</li>
                        <li data-target="#myCarousel" data-slide-to="2" class="">人氣商品</li>
                        <li data-target="#myCarousel" data-slide-to="3" class="">人氣商品</li>
                        <li data-target="#myCarousel" data-slide-to="4" class="">人氣商品</li>
                    </ol>
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <a href="/EW/GO/GroupDetail.asp?prodCd=TYO05170708B" target="_blank">
								<div class="img" style="background-image:url('/eweb_miyabi/images/default/slider_01.jpg');cursor: pointer; width: 20%;"></div>
							</a>
						</div>
                        <div class="item">
                            <a href="/EW/GO/GroupDetail.asp?prodCd=TYO05170826A" target="_blank">
								<div class="img" style="background-image:url('/eweb_miyabi/images/default/slider_01.jpg');cursor: pointer; width: 20%;"></div>
                            </a>
                        </div>
                        <div class="item">
                            <a href="/eweb_miyabi/starcruises/index.asp" target="_blank">
								<div class="img" style="background-image:url('/eweb_miyabi/images/default/slider_01.jpg');cursor: pointer; width: 20%;"></div>
                            </a>
                        </div>
                        <div class="item">
                            <a href="/eweb_miyabi/starcruises/index.asp" target="_blank">
								<div class="img" style="background-image:url('/eweb_miyabi/images/default/slider_01.jpg');cursor: pointer; width: 20%;"></div>
                            </a>
                        </div>
                        <div class="item">
                            <a href="/eweb_miyabi/starcruises/index.asp" target="_blank">
								<div class="img" style="background-image:url('/eweb_miyabi/images/default/slider_01.jpg');cursor: pointer; width: 20%;"></div>
                            </a>
                        </div>
                    </div>

                    <!-- Left and right controls -->
                    <a class="left carousel-control" href="#myCarousel_03" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#myCarousel_03" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
				<h1 style="width: 100%;margin-top: -90px;display: block;font-size: 5em;color: #fff;text-shadow: 2px 2px 1px #000, 1px 1px 3px #000;opacity: .8;filter: alpha(opacity=80);text-align: center;">東北亞</h1>
            </div>
        </div>
    </article>
	<article class="container">
        <div class="row">
			<!--麵包屑及頁面標題-->
            <ol class="breadcrumb" id="auto_breadcrumb">
                <li>目前位置：</li>
                <li class="active"><a href="/eWeb_summittour/home.asp">首頁</a></li>
                <li class="active">東北亞</li>
            </ol>
		</div>
		<ul id="theme_tabs" class="nav nav-tabs" role="tablist">
			<li><a href="javascript:;" name="#tab1" id="area1">東京(迪士尼、箱根、輕井澤)</a></li>
			<li><a href="javascript:;" name="#tab2" id="area2">關東地區(茨城、栃木、群馬、埼玉、千葉、東京都、神奈川)</a></li>
			<li><a href="javascript:;" name="#tab3" id="area3">關西(大阪、京都、神戶、奈良、南紀)</a></li>
			<!--li><a href="javascript:;" name="#tab4" id="area4">四國（道後溫泉‧小豆島）</a></li-->
			<li><a href="javascript:;" name="#tab5" id="area5">北陸黑部立山(名古屋、富山、小松、靜岡)</a></li>
			<li><a href="javascript:;" name="#tab6" id="area6">東北(花卷、仙台、青森、秋田、新潟)</a></li>
			<li><a href="javascript:;" name="#tab7" id="area7">沖繩(石垣、宮古、與論、琉球、久米島)</a></li>
			<li><a href="javascript:;" name="#tab8" id="area8">北海道(札幌、函館、旭川)</a></li>
			<li><a href="javascript:;" name="#tab9" id="area9">九州(大分、福岡、宮崎、鹿兒島、熊本)</a></li>
			<li><a href="javascript:;" name="#tab10" id="area10">韓國(首爾.釜山.濟州島)</a></li>
			<li><a href="javascript:;" name="#tab12" id="area11">韓國(首爾)</a></li>
			<li><a href="javascript:;" name="#tab13" id="area12">韓國(釜山、大邱、慶州)</a></li>
			<li><a href="javascript:;" name="#tab14" id="area13">韓國(濟州島)</a></li>
			<!--li><a href="javascript:;" name="#tab11" id="area11">東北亞自由行</a></li-->
		</ul>
	</article>
	<article id="module_content_page03" class="container">
		<div class="row"  id="module_content_row_page03">
            <section class="module_content" id="module_01">
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 tile module_three">
                    <a href="javascript:;" target="_blank">
                        <img src="/eweb_summittour/images/default/index_01.jpg" />
						<div class="text">
                            <h3 class="animate-text">
							<i class="fa fa-link" aria-hidden="true"></i>
							</h3>
                        </div>
                    </a>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 tile module_three">
                    <a href="javascript:;" target="_blank">
                        <img src="/eweb_summittour/images/default/index_01.jpg" />
						<div class="text">
                            <h3 class="animate-text"><i class="fa fa-link" aria-hidden="true"></i></h3>
                        </div>
                    </a>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 tile module_three">
                    <a href="javascript:;" target="_blank">
                        <img src="/eweb_summittour/images/default/index_01.jpg" />
						<div class="text">
                            <h3 class="animate-text"><i class="fa fa-link" aria-hidden="true"></i></h3>
                        </div>
                    </a>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 tile module_three">
                    <a href="javascript:;" target="_blank">
                        <img src="/eweb_summittour/images/default/index_02.jpg" />
						<div class="text">
                            <h3 class="animate-text"><i class="fa fa-link" aria-hidden="true"></i></h3>
                        </div>
                    </a>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 tile module_three">
                    <a href="javascript:;" target="_blank">
                        <img src="/eweb_summittour/images/default/index_03.jpg" />
						<div class="text">
                            <h3 class="animate-text"><i class="fa fa-link" aria-hidden="true"></i></h3>
                        </div>
                    </a>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 tile module_three">
                    <a href="javascript:;" target="_blank">
                        <img src="/eweb_summittour/images/default/index_04.jpg" />
						<div class="text">
                            <h3 class="animate-text"><i class="fa fa-link" aria-hidden="true"></i></h3>
                        </div>
                    </a>
                </div>

            </section>
		</div>
	</article>
	
    <article class="exercise_min container">
		<section>
		
		
         <!--搜尋引擎-->
            <!--div id="module_search_hotel">
            	<img src="images/top_images/hotel_search_img.png" />
           </div-->
            <!--搜尋引擎-->
			<div class="exercise_box">
				<div class="central_tours">
                          <ul id="skin">
                                <li id="skin_module" title="區塊式" class="selected">
                                	<i class="fa fa-th-large" aria-hidden="true"></i>圖片模式
                                </li>
                                <li id="skin_list" title="條列式">
                                	<i class="fa fa-th-list" aria-hidden="true"></i>列表模式
                                </li>
                            </ul>  
                         
                         
                         
                         <div id="theme_content" class="skin_list">
								<div id="tab1"></div>
								<div id="tab2"></div>
								<div id="tab3"></div>
								<div id="tab4"></div>
								<div id="tab5"></div>
								<div id="tab6"></div>
								<div id="tab7"></div>
								<div id="tab8"></div>
								<div id="tab9"></div>
								<div id="tab10"></div>
								<div id="tab11"></div>
								<div id="tab12"></div>
								<div id="tab13"></div>
								<div id="tab14"></div>
								<div id="tab15"></div>
								<div id="tab16"></div>
								<div id="tab17"></div>
								<div id="tab18"></div>
								<div id="tab19"></div>
								<div id="tab20"></div>
                         </div>   
				</div>
			</div>
	
        <!--MAIN-->
		
		</section>
		
	</article>
    
    
	
    
    <article class="footer">
    <!--#include virtual="/eweb_summittour/public/footer.asp" -->
    </article>
</div>	


<div class="service_box">
    <ul>
        <li><button class="btn btn-success" onclick="location.href='tel:0423052639'"><i class="fa fa-phone" aria-hidden="true"></i> 手機直撥</button></li>
        <li><button class="btn btn-danger" onclick="location.href='/eweb_summittour/diy/V_diy.asp'"><i class="fa fa-envelope" aria-hidden="true"></i> 聯絡我們</button></li>
    </ul>
</div>
<div class="actGotop"><a href="javascript:;" title="返回頂部"><i class="fa fa-angle-up" aria-hidden="true"></i>TOP</a></div>

<script src="/eweb_summittour/js/share_main.js"></script>

<script type="text/javascript">
$(document).ready( function() {
	getXML(true,'');
});
</script>
<!--自動上架置入-->
<% 
            if returl1="" then returl1="/eWeb/Main/home.asp"
            if returl2="" then returl2="/eWeb/Main/home.asp"
            If (Session("USR_CLS") = "A") Then
        '判斷是否為同業會員
           %>
           <!--如果是同業會員，就秀下面資料-->
          <script>
			$(function(){
				$("#tab1").load("/eweb_summittour/DHTML/AGT/MPG0003-SAREA00021/index.asp .metropolis",function(){$("html");});<!--關東(東京、伊豆、日光)-->
				$("#tab2").load("/eweb_summittour/DHTML/AGT/MPG0003-SAREA00297/index.asp .metropolis",function(){$("html");});<!--關東地區(茨城、栃木、群馬、埼玉、千葉、東京都、神奈川)-->
				$("#tab3").load("/eweb_summittour/DHTML/AGT/MPG0003-SAREA00003/index.asp .metropolis",function(){$("html");});<!--大阪、南紀、岡山-->
				$("#tab4").load("/eweb_summittour/DHTML/AGT/MPG0003-SAREA00049/index.asp .metropolis",function(){$("html");});<!--四國（道後溫泉‧小豆島）-->
				$("#tab5").load("/eweb_summittour/DHTML/AGT/MPG0003-SAREA00022/index.asp .metropolis",function(){$("html");});<!--北陸(名古屋、立山黑部)-->
				$("#tab6").load("/eweb_summittour/DHTML/AGT/MPG0003-SAREA00104/index.asp .metropolis",function(){$("html");});<!--東北(仙台、花卷)-->
				$("#tab7").load("/eweb_summittour/DHTML/AGT/MPG0003-SAREA00112/index.asp .metropolis",function(){$("html");});<!--沖繩(石垣、宮古、與論)-->
				$("#tab8").load("/eweb_summittour/DHTML/AGT/MPG0003-SAREA00020/index.asp .metropolis",function(){$("html");});<!--北海道(札幌、函館、旭川)-->
				$("#tab9").load("/eweb_summittour/DHTML/AGT/MPG0003-SAREA00023/index.asp .metropolis",function(){$("html");});<!--九州(大分、福岡、宮崎、鹿兒島)-->
				<!--$("#tab10").load("/eweb_summittour/DHTML/AGT/MPG0003-SAREA00048/index.asp .metropolis",function(){$("html");});<!--韓國(首爾.釜山.濟州島)-->
				$("#tab11").load("/eweb_summittour/DHTML/AGT/MPG0003-SAREA00113/index.asp .metropolis",function(){$("html");});<!--東北亞自由行-->
				$("#tab12").load("/eweb_summittour/DHTML/AGT/MPG0003-SAREA00303/index.asp .metropolis",function(){$("html");});<!--韓國(首爾)-->
				$("#tab13").load("/eweb_summittour/DHTML/AGT/MPG0003-SAREA00304/index.asp .metropolis",function(){$("html");});<!--韓國(釜山、大邱、慶州)-->
				$("#tab14").load("/eweb_summittour/DHTML/AGT/MPG0003-SAREA00305/index.asp .metropolis",function(){$("html");});<!--韓國(濟州島)-->
				});
			</script>
          <% else%>
          <!--如果不是，就秀下面資料-->
          <script>
			$(function(){
				$("#tab1").load("/eweb_summittour/DHTML/MPG0003-SAREA00021/index.asp .metropolis",function(){$("html");});<!--關東(東京、伊豆、日光)-->
				$("#tab2").load("/eweb_summittour/DHTML/MPG0003-SAREA00297/index.asp .metropolis",function(){$("html");});<!--關東地區(茨城、栃木、群馬、埼玉、千葉、東京都、神奈川)-->
				$("#tab3").load("/eweb_summittour/DHTML/MPG0003-SAREA00003/index.asp .metropolis",function(){$("html");});<!--大阪、南紀、岡山-->
				$("#tab4").load("/eweb_summittour/DHTML/MPG0003-SAREA00049/index.asp .metropolis",function(){$("html");});<!--四國（道後溫泉‧小豆島）-->
				$("#tab5").load("/eweb_summittour/DHTML/MPG0003-SAREA00022/index.asp .metropolis",function(){$("html");});<!--北陸(名古屋、立山黑部)-->
				$("#tab6").load("/eweb_summittour/DHTML/MPG0003-SAREA00104/index.asp .metropolis",function(){$("html");});<!--東北(仙台、花卷)-->
				$("#tab7").load("/eweb_summittour/DHTML/MPG0003-SAREA00112/index.asp .metropolis",function(){$("html");});<!--沖繩(石垣、宮古、與論)-->
				$("#tab8").load("/eweb_summittour/DHTML/MPG0003-SAREA00020/index.asp .metropolis",function(){$("html");});<!--北海道(札幌、函館、旭川)-->
				$("#tab9").load("/eweb_summittour/DHTML/MPG0003-SAREA00023/index.asp .metropolis",function(){$("html");});<!--九州(大分、福岡、宮崎、鹿兒島)-->
				<!--$("#tab10").load("/eweb_summittour/DHTML/MPG0003-SAREA00048/index.asp .metropolis",function(){$("html");});<!--韓國(首爾.釜山.濟州島)-->
				$("#tab11").load("/eweb_summittour/DHTML/MPG0003-SAREA00113/index.asp .metropolis",function(){$("html");});<!--東北亞自由行-->
				$("#tab12").load("/eweb_summittour/DHTML/MPG0003-SAREA00303/index.asp .metropolis",function(){$("html");});<!--韓國(首爾)-->
				$("#tab13").load("/eweb_summittour/DHTML/MPG0003-SAREA00304/index.asp .metropolis",function(){$("html");});<!--韓國(釜山、大邱、慶州)-->
				$("#tab14").load("/eweb_summittour/DHTML/MPG0003-SAREA00305/index.asp .metropolis",function(){$("html");});<!--韓國(濟州島)-->
				});
			</script>
          
          <% End If %>
<!--Include flickerplate-->
<script language="javascript" type="text/javascript" src="/eweb_summittour/js/jquery.cookie.js"></script>
<!--圖文/條列式切換-->
<script type="text/javascript">
//<![CDATA[
    $(function(){
        var $li =$("#skin li");
        $li.click(function(){
            switchSkin( this.id );
        });
        var cookie_skin = $.cookie( "MyCssSkin");
        if (cookie_skin) {
            switchSkin( cookie_skin );
        }
    });
    
    function switchSkin(skinName){
        $("#"+skinName).addClass("selected") //當前<li>元素選中
        .siblings().removeClass("selected"); //去掉其它同輩<li>元素的選中
        $("#theme_content").attr("class", "" + skinName + "");//設置不同classname
        $.cookie( "MyCssSkin" , skinName , { path: '/', expires: 30 });
    }
//]]>
</script>
<script src="/eweb_summittour/js/jquery.superslides.js" type="text/javascript" charset="utf-8"></script>

<script type="text/javascript">

    $(function() {
      var $slides = $('#slides');

      Hammer($slides[0]).on("swipeleft", function(e) {
        $slides.data('superslides').animate('next');
      });

      Hammer($slides[0]).on("swiperight", function(e) {
        $slides.data('superslides').animate('prev');
      });

      $slides.superslides({
        hashchange: true,
        play: 8000
      });

      $('#slides').on('mouseenter', function() {
        $(this).superslides('stop');
        console.log('Stopped')
      });
      $('#slides').on('mouseleave', function() {
        $(this).superslides('start');
        console.log('Started')
      });

    });

//分頁頁籤
$('#myTab a').click(function (e) {
  e.preventDefault()
  $(this).tab('show')
})

setTimeout(function(){
            $('.carousel').carousel({
                interval: 8000
            })
        }, 5000);
//回頂點
$(function(){   
    $(window).scroll(function() {       
        if($(window).scrollTop() >= 100){
            $('.actGotop').fadeIn(300); 
        }else{    
            $('.actGotop').fadeOut(300);    
        }  
    });
    $('.actGotop').click(function(){
    $('html,body').animate({scrollTop: '0px'}, 800);}); 
    $('#go_search').click(function(){   $('html,body').animate({scrollTop:$('#search').offset().top}, 900); return false;   })
});

$(function(){   
    var cubuk_seviye = $(document).scrollTop();
    var header_yuksekligi = $('.header').outerHeight();

    $(window).scroll(function() {
        var kaydirma_cubugu = $(document).scrollTop();

        if (kaydirma_cubugu > header_yuksekligi){$('.header').addClass('gizle');} 
        else {$('.header').removeClass('gizle');}

        if (kaydirma_cubugu > cubuk_seviye){$('.header').removeClass('sabit');} 
        else {$('.header').addClass('sabit');}              

        cubuk_seviye = $(document).scrollTop(); 
     });
     
});


$(function(){
    var sideslider = $('[data-toggle=collapse-side]');
    var get_sidebar = sideslider.attr('data-target-sidebar');
    var get_content = sideslider.attr('data-target-content');
    sideslider.click(function(event){
      $(get_sidebar).toggleClass('in');
      $(get_content).toggleClass('out');
   });

});
$(function(){
    var sideslider = $('[data-toggle=collapse-side1]');
    var get_sidebar = sideslider.attr('data-target-sidebar1');
    var get_content = sideslider.attr('data-target-content1');
    sideslider.click(function(event){
      $(get_sidebar).toggleClass('in');
      $(get_content).toggleClass('out');
   });

});

</script>
<script type="text/javascript">
	$('#area1').click(function(){
		$('html,body').animate({scrollTop:$('#tab1').offset().top}, 1000);
	});
	$('#area2').click(function(){
		$('html,body').animate({scrollTop:$('#tab2').offset().top}, 1000);
	});
	$('#area3').click(function(){
		$('html,body').animate({scrollTop:$('#tab3').offset().top}, 1000);
	});
	$('#area4').click(function(){
		$('html,body').animate({scrollTop:$('#tab4').offset().top}, 1000);
	});
	$('#area5').click(function(){
		$('html,body').animate({scrollTop:$('#tab5').offset().top}, 1000);
	});
	$('#area6').click(function(){
		$('html,body').animate({scrollTop:$('#tab6').offset().top}, 1000);
	});
	$('#area7').click(function(){
		$('html,body').animate({scrollTop:$('#tab7').offset().top}, 1000);
	});
	$('#area8').click(function(){
		$('html,body').animate({scrollTop:$('#tab8').offset().top}, 1000);
	});
	$('#area9').click(function(){
		$('html,body').animate({scrollTop:$('#tab9').offset().top}, 1000);
	});
	$('#area10').click(function(){
		$('html,body').animate({scrollTop:$('#tab10').offset().top}, 1000);
	});
</script>
</body>
</html>

﻿<%
'//===========================================================================================
'//						SubTicket.asp
'//-------------------------------------------------------------------------------------------
'//		description		: 機票次首頁
'//		table			: 
'//		include			: 
'//		version			: 
'//		begin			: 2010-12-21
'//		copyright		: (C) 2010 demo
'//		author			: Gary
'//		2010-12-23		: [Gary] [ew.v902.054U] 機票次首頁
'//-------------------------------------------------------------------------------------------
%>
<!--#include virtual="/include/egrpDSN_binary.inc" -->
<!--include virtual="/eWeb/public/eWeb_utility.asp" -->
<!--include virtual="/include/myFunc.inc" -->
<!--#include virtual="/eWeb/Style/style_setting.asp" -->
<%
'call MAIN()
Sub MAIN()
	Dim aMYAGT_CD,aTN_ADM_URL,strSQL
	strSQL = ""
	aMYAGT_CD = ""	
	aTN_ADM_URL = ""
	
	'取得上游供應商ACCT_NO(下游向哪一家上游引用商品資料)
	Call GetMYAGT(RS)	
	IF Not RS.EOF Then 
	   aMYAGT_CD = RS("MYAGT_CD")
	   aTN_ADM_URL = RS("TN_ADM_URL")
	End if
	RS.Close()
	
	'response.write("MYAGT_CD=" & aMYAGT_CD & "<BR>" ) 
	'response.write("TN_ADM_URL=" & aTN_ADM_URL & "<BR>" ) 
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<title>機票次首頁</title>
<link href="../../css/demo_ticket.css" rel="stylesheet" type="text/css" />
<style>
</style>
<script src="../../script/jquery-1.4.2.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
	initPage();
})
function initPage()
{	
	//取得機票廣告資料
	aURL = "getTktBanner.asp?ACCT_NO=" + "<%=aMYAGT_CD%>" + "&LINK=" + "<%=aTN_ADM_URL%>";        
	var http_request = false;
    if (window.XMLHttpRequest) { // Mozilla, Safari,...
        http_request = new XMLHttpRequest();
    } else if (window.ActiveXObject) { // IE
      try {
           http_request = new ActiveXObject("Msxml2.XMLHTTP");
      } catch (e) {
        try {
             http_request = new ActiveXObject("Microsoft.XMLHTTP");
        } catch (e) {}
      }
    }
    if (!http_request) {
       alert('Cannot create an XMLHTTP instance');
       return false;
    }
    // 定義事件處理函數為 alterContents()
    http_request.onreadystatechange = function() { 
    	                                  alertContents(http_request); 
									  };
    http_request.open('GET', aURL, true);//非同步
    //http_request.open('GET', aURL, false);
	http_request.send(null);
	function alertContents(http_request) {
		if (http_request.readyState == 4) {
		  if (http_request.status == 200) {
			//var xmldoc = http_request.responseXML;		
			var xmldoc = http_request.responseText;	
			//回傳回來的資料會部份資料會被改變，在這裡做轉換動作
			templateXml = HTMLDecode(xmldoc);				
					
			
			//JQUERY在處理XML時必須有xml Tag			
			templateXml = "<xml>" + templateXml + "</xml>";		
				
			//建立一個只有Root空瞉的DOM	
			var rtndom;
		    var rtnXml = "<?xml version=\"1.0\"?>";
			rtnXml = rtnXml + "<Root></Root>";
			if (window.ActiveXObject) {
				rtndom = new ActiveXObject('Microsoft.XMLDOM');
				rtndom.loadXML(rtnXml);
			}
			// code for Mozilla, Firefox, Opera, etc.
			else if (document.implementation && document.implementation.createDocument) {
				rtndom = document.implementation.createDocument("", "", null);
				rtndom.async = false;
				rtndom.loadXML(rtnXml);
			}
			//過濾掉WS傳進來的值，取得我們所須的資料Root節點下的資料(Copy)
			var rootNode = $(templateXml).find("Root").clone();
			//將所需資料，新增至空的DOM資料中
			$(rtndom).find("Root").append(rootNode);
			
		
			//顯示畫面
			ShowData($(rtndom));				
			
		  } else {
				alert('There was a problem with the request.');
		  }
		}
	 }
			//金額三位一撇     
    function moneyFormat(str) { 
     	if(str.length<=3) return str; 
        else return moneyFormat(str.substr(0,str.length-3))+","+(str.substr(str.length-3)); 
    }
	
	//顯示畫面
	function ShowData(rtndom)
	{	
		var htmlTag;
		var i = 0;
		htmlTag = "";
		htmlTag = htmlTag + "<div>"
		htmlTag = htmlTag + "	<table class=\"\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">"
		htmlTag = htmlTag + "	 <tr>"
		
	
		var iAreaItemCount = rtndom.find("Root AreaItem").size();
		//跑洲別迴圈
		rtndom.find("Root AreaItem").each(function(e){
			var area_cnm = $(this).find("AREA_NM").eq(0).text();	
			var area_cd = $(this).find("AREA_CD").eq(0).text();
			
				htmlTag = htmlTag + "	  <td width=\"10\">"
				htmlTag = htmlTag + "	  &nbsp;</td>"
				htmlTag = htmlTag + "	  <td valign=\"top\">"
				htmlTag = htmlTag + "		<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">"
				htmlTag = htmlTag + "		  <tr>"
				htmlTag = htmlTag + "			<td align=\"center\"><a href=\"#\"><img class=\"main-pic\" src=\"../images/CarrImg/" + area_cd + ".gif\"  border=\"0\" /></a></td>"
				htmlTag = htmlTag + "		  </tr>"
				htmlTag = htmlTag + "		  <tr>"
				htmlTag = htmlTag + "			<td>"			
			
				
				htmlTag = htmlTag + "	  <table class=\"main-table\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">"
				htmlTag = htmlTag + "        <tr>"
		
			var j=0;
			var iCityItemCount = $(this).find("CityItem CITY").size();
			//跑城市迴圈
			$(this).find("CityItem CITY").each(function(f){		
				var city_cd = $(this).find("CITY_CD").eq(0).text();
				var city_nm = $(this).find("CITY_NM").eq(0).text();
				var adt_sale_am = $(this).find("ADT_SALE_AM").eq(0).text();	
				
				htmlTag = htmlTag + "            <td height=\"5\"></td>"
				htmlTag = htmlTag + "        </tr>"
				htmlTag = htmlTag + "        <tr>"
				htmlTag = htmlTag + "          <td>"
				htmlTag = htmlTag + "            <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">"
				htmlTag = htmlTag + "              <tr>"
				htmlTag = htmlTag + "                 <td class=\"main-td-width1 main-text\" align=\"left\"><img class=\"main-pic002-1\" src=\"../images/CarrImg/main-pic" + area_cd + ".png\" border=\"0\" align=\"top\" />&nbsp;&nbsp;<a href=\"/eweb/tkt/flt_result.asp?tvl_fg=1&SEND_FORM_FG=1&SEL_TKT_ROUT_TP=2&SEL_DPT_ARP_CD=TPE&SEL_DST_CTY_CD1=" + city_cd + "\">" + city_nm + "</a></th>"
				htmlTag = htmlTag + "                 <td class=\"main-td-width2 main-text\" align=\"right\"><a href=\"/eweb/tkt/flt_result.asp?tvl_fg=1&SEND_FORM_FG=1&SEL_TKT_ROUT_TP=2&SEL_DPT_ARP_CD=TPE&SEL_DST_CTY_CD1=" + city_cd + "\">" + moneyFormat(adt_sale_am) + " 元起</a></td>"
				htmlTag = htmlTag + "              </tr>"
				htmlTag = htmlTag + "            </table>"				
				htmlTag = htmlTag + "          </td>"
				htmlTag = htmlTag + "        </tr>"
				//htmlTag = htmlTag + "        <tr>"
				//htmlTag = htmlTag + "          <td><img class=\"main-line\" src=\"../images/CarrImg/main_line.png\" border=\"0\"/></td>"
				//htmlTag = htmlTag + "        </tr>"
				
				if (j==iCityItemCount - 1)
				{                            
				htmlTag = htmlTag + "    	 <tr>"
				htmlTag = htmlTag + "          <td><img class=\"main-pic-down\"src=\"../images/CarrImg/main-pic_down1.png\" border=\"0\"/></td>"
				htmlTag = htmlTag + "        </tr>"
				}                            
	
				j++;
			});
				htmlTag = htmlTag + "     </table>"
				
				
				
				htmlTag = htmlTag + "			</td>"
				htmlTag = htmlTag + "		   </tr>"
				htmlTag = htmlTag + "		</table>"
				htmlTag = htmlTag + "	  </td>"		
				//三個洲別換一行
				if((i+1)%3 ==0)
				{
					htmlTag = htmlTag + "	  <tr><td height=\"10px\"></td></tr>"
				}		
				i++;
		});
				htmlTag = htmlTag + "	 </tr>"
				htmlTag = htmlTag + "	</table>"
				htmlTag = htmlTag + "</div>"
		document.getElementById("AREA_BANNER").innerHTML = htmlTag;
	}
	
	
   //回傳回來的資料會部份資料會被改變，在這裡做轉換動作
   function HTMLDecode(str)
   {   
         var s = "";
         if(str.length == 0)   return "";
         s    =    str.replace(/&amp;/g,"&");
         s    =    s.replace(/&lt;/g,"<");
         s    =    s.replace(/&gt;/g,">");
         s    =    s.replace(/&nbsp;/g," ");
         s    =    s.replace(/&#39;/g,"\'");
         s    =    s.replace(/&quot;/g,"\"");
		 
         s    =    s.replace(/<br>/g,"");
		 s    =    s.replace(/<I>/g,"");
		 s    =    s.replace(/<\/I>/g,"");
         return   s;   
   }
}
</script>
</head>
<body>
<!--<div class="wrapper">-->
<!--搜尋引擎-->
<div class="top">
<iframe src="/eWeb/Module/m_s_tkt_simple.asp" frameborder="0" vspace="0" hspace="0" align="center" width="100%" height="160px" marginheight="0" marginwidth="0" scrolling="no" ></iframe>
<!--  <table>
    <tr>
      <td width="960" height="180"><iframe src="/eWeb/Module/M_S_TKT_SIMPLE.asp" frameborder="0" vspace="0" hspace="0" align="center" width="100%" height="100%" marginheight="0" marginwidth="0" scrolling="no" ></iframe></td>
     </tr>
  </table>-->
</div>
<!--搜尋引擎下方-->
<div class="main">
  <div class="main-left">
    <table border="0" cellpadding="0" cellspacing="0" class="left-bg">
      <tr>
         <td>
           <table border="0" cellpadding="0" cellspacing="0" class="left-main">
            <tr>
             <td align="left" class="left-title">快速前往   </td>
            </tr>
              <td class="left01-table">
                <table border="0" cellpadding="0" cellspacing="0">
                 <tr>
                   <td valign="bottom" align="left" class="left01-1"><a href="/eweb/tkt/flt_result.asp?tvl_fg=1&SEND_FORM_FG=1&SEL_TKT_ROUT_TP=2&SEL_DPT_ARP_CD=TPE&SEL_DST_CTY_CD1=HKG">香港</a>．<a href="/eweb/tkt/flt_result.asp?tvl_fg=1&SEND_FORM_FG=1&SEL_TKT_ROUT_TP=2&SEL_DPT_ARP_CD=TPE&SEL_DST_CTY_CD1=MFM">澳門</a>．<a href="/eweb/tkt/flt_result.asp?tvl_fg=1&SEND_FORM_FG=1&SEL_TKT_ROUT_TP=2&SEL_DPT_ARP_CD=TPE&SEL_DST_CTY_CD1=FOC">福州</a>．<a href="/eweb/tkt/flt_result.asp?tvl_fg=1&SEND_FORM_FG=1&SEL_TKT_ROUT_TP=2&SEL_DPT_ARP_CD=TPE&SEL_DST_CTY_CD1=CAN">廣州</a></td>
                 </tr>
                 <tr>
                   <td><img class="left-dot" src="../images/CarrImg/left-dotline.png"></td>
                 </tr>
                 <tr>
                   <td valign="bottom" align="left" class="left01-1"><a href="/eweb/tkt/flt_result.asp?tvl_fg=1&SEND_FORM_FG=1&SEL_TKT_ROUT_TP=2&SEL_DPT_ARP_CD=TPE&SEL_DST_CTY_CD1=PEK">北京</a>．<a href="/eweb/tkt/flt_result.asp?tvl_fg=1&SEND_FORM_FG=1&SEL_TKT_ROUT_TP=2&SEL_DPT_ARP_CD=TPE&SEL_DST_CTY_CD1=SHA">上海</a>．<a href="/eweb/tkt/flt_result.asp?tvl_fg=1&SEND_FORM_FG=1&SEL_TKT_ROUT_TP=2&SEL_DPT_ARP_CD=TPE&SEL_DST_CTY_CD1=SZX">深圳</a></td>
                 </tr>
                 <tr>
                   <td><img class="left-dot" src="../images/CarrImg/left-dotline.png" width="248" height="2"></td>
                 </tr>
                 <tr>
                   <td valign="bottom" align="left" class="left01-1"><a href="/eweb/tkt/flt_result.asp?tvl_fg=1&SEND_FORM_FG=1&SEL_TKT_ROUT_TP=2&SEL_DPT_ARP_CD=TPE&SEL_DST_CTY_CD1=SEL">首爾</a>．<a href="/eweb/tkt/flt_result.asp?tvl_fg=1&SEND_FORM_FG=1&SEL_TKT_ROUT_TP=2&SEL_DPT_ARP_CD=TPE&SEL_DST_CTY_CD1=TYO">東京</a>．<a href="OSA">大阪</a>．<a href="/eweb/tkt/flt_result.asp?tvl_fg=1&SEND_FORM_FG=1&SEL_TKT_ROUT_TP=2&SEL_DPT_ARP_CD=TPE&SEL_DST_CTY_CD1=NGO">名古屋</a></td>
                 </tr>
                 <tr>
                   <td><img class="left-dot" src="../images/CarrImg/left-dotline.png" width="248" height="2"></td>
                 </tr>
                 <tr>
                   <td valign="bottom" align="left" class="left01-1"><a href="/eweb/tkt/flt_result.asp?tvl_fg=1&SEND_FORM_FG=1&SEL_TKT_ROUT_TP=2&SEL_DPT_ARP_CD=TPE&SEL_DST_CTY_CD1=BKK">曼谷</a>．<a href="/eweb/tkt/flt_result.asp?tvl_fg=1&SEND_FORM_FG=1&SEL_TKT_ROUT_TP=2&SEL_DPT_ARP_CD=TPE&SEL_DST_CTY_CD1=SGN">胡志明市</a>．<a href="/eweb/tkt/flt_result.asp?tvl_fg=1&SEND_FORM_FG=1&SEL_TKT_ROUT_TP=2&SEL_DPT_ARP_CD=TPE&SEL_DST_CTY_CD1=SIN">新加坡</a></td>
                 </tr>
                 <tr>
                   <td><img class="left-dot" src="../images/CarrImg/left-dotline.png" width="248" height="2"></td>
                 </tr>
                 <tr>
                   <td valign="bottom" align="left" class="left01-1"><a href="/eweb/tkt/flt_result.asp?tvl_fg=1&SEND_FORM_FG=1&SEL_TKT_ROUT_TP=2&SEL_DPT_ARP_CD=TPE&SEL_DST_CTY_CD1=LAX">洛杉磯</a>．<a href="/eweb/tkt/flt_result.asp?tvl_fg=1&SEND_FORM_FG=1&SEL_TKT_ROUT_TP=2&SEL_DPT_ARP_CD=TPE&SEL_DST_CTY_CD1=SFO">舊金山</a>．<a href="/eweb/tkt/flt_result.asp?tvl_fg=1&SEND_FORM_FG=1&SEL_TKT_ROUT_TP=2&SEL_DPT_ARP_CD=TPE&SEL_DST_CTY_CD1=YYR">溫哥華</a></td>
                 </tr>
                 <tr>
                   <td><img class="left-dot" src="../images/CarrImg/left-dotline.png" width="248" height="2"></td>
                 </tr>
                 <tr>
                   <td valign="bottom" align="left" class="left01-1"><a href="/eweb/tkt/flt_result.asp?tvl_fg=1&SEND_FORM_FG=1&SEL_TKT_ROUT_TP=2&SEL_DPT_ARP_CD=TPE&SEL_DST_CTY_CD1=PAR">巴黎</a>．<a href="/eweb/tkt/flt_result.asp?tvl_fg=1&SEND_FORM_FG=1&SEL_TKT_ROUT_TP=2&SEL_DPT_ARP_CD=TPE&SEL_DST_CTY_CD1=LON">倫敦</a>．<a href="/eweb/tkt/flt_result.asp?tvl_fg=1&SEND_FORM_FG=1&SEL_TKT_ROUT_TP=2&SEL_DPT_ARP_CD=TPE&SEL_DST_CTY_CD1=FRA">法蘭克福</a></td>
                 </tr>
                 <tr>
                   <td><img class="left-dot" src="../images/CarrImg/left-dotline.png" width="248" height="2"></td>
                 </tr>
               </table>   
              </td>
            </tr>
            <tr>
              <td align="left" class="left-title">旅遊資訊   </td>
            </tr>
            <tr>
              <td class="left02-table">
                <table align="center" class="left02-main" border="0" cellpadding="0" cellspacing="0">
                 <tr>
                  <td class="left02-1"><a href="http://tw.weather.yahoo.com/world.html" target="_blank"></a></td>
                  <td class="left02-2"><a href="http://time.artjoey.com" target="_blank"></a></td>
                  <td class="left02-3"><a href="http://rate.bot.com.tw/Pages/Static/UIP003.zh-TW.htm" target="_blank"></a></td>
                 </tr>
                 <tr>
                  <td class="left02-4"><a href="http://www.tourcenter.com.tw/2info/Electric/Electric.htm#eu" target="_blank"></a></td>
                  <td class="left02-5"><a href="http://www.caa.gov.tw/big5/content/index.asp?sno=76" target="_blank"></a></td>
                  <td class="left02-6"><a href="http://www.boca.gov.tw/lp.asp?ctNode=509&CtUnit=10&BaseDSD=7&mp=1" target="_blank"></a></td>
                 </tr>
                </table>   
              </td>
            </tr>
            <tr>
              <td>
                <table class="left03-table" border="0" cellpadding="0" cellspacing="0">
                 <tr>
                   <td align="center" class="left03-1"><a href="http://www.tsa.gov.tw/" target="_blank"></a></td>
                 </tr>
                 <tr>
                   <td align="center" class="left03-2"><a href="http://www.taoyuan-airport.com/chinese/index.jsp" target="_blank"></a></td>
                 </tr>
                 <tr>
                   <td align="center" class="left03-3"><a href="http://www.kia.gov.tw/" target="_blank"></a></td>
                 <tr>
                   <td align="center" class="left03-4"><a href="http://www.tca.gov.tw/" target="_blank"></a></td>
                 </tr>
                 <tr>
                   <td align="center" class="left03-5"></td>
                 </tr>
                 <tr>
                   <td align="center" class="left-banner1"><a href="http://www.boca.gov.tw/mp?mp=1" target="_blank"></a></td>
                 </tr>
                 <tr>
                   <td align="center" class="left-banner2"><a href="http://www.immigration.gov.tw/welcome.htm" target="_blank"></a></td>
                 </tr>
                 <tr>
                   <td align="center" class="left-banner3"><a href="http://admin.taiwan.net.tw/indexc.asp" target="_blank"></a></td>
                 </tr>
                </table>   
               </td>
             </tr>
           </table>
         </td>
      </tr>
    </table>
  </div>
  <div class="main-right" >
     <div class="main-right-top" id="AREA_BANNER" >
     <!--廣告-->
     </div>
     <div class="main-right-down">   
       <table class="airline-table" height=100% border="0" align="left" cellpadding="0" cellspacing="0">
       <!---以下為第一行--->
        <tr>
            <td colspan="4" align="left" class="airline-title">航空公司專區</td>
         </tr>
         <tr>
            <td class="airline-td-width" align="center" valign="middle"><a href="/eWeb/TKT/V_TK_byFlight.asp?CARR_CD=CI" target"_top"><img src="../images/CarrImg/CI.jpg"></a></td>
            <td class="airline-td-width" align="center" valign="middle"><a href="/eWeb/TKT/V_TK_byFlight.asp?CARR_CD=BR" target"_top"><img src="../images/CarrImg/BR.jpg"></a></td>
            <td class="airline-td-width" align="center" valign="middle"><a href="/eWeb/TKT/V_TK_byFlight.asp?CARR_CD=CX" target"_top"><img src="../images/CarrImg/CX.jpg"></a></td>
            <td class="airline-td-width" align="center" valign="middle"><a href="/eWeb/TKT/V_TK_byFlight.asp?CARR_CD=KA" target"_top"><img src="../images/CarrImg/KA.jpg"></a></td>
         </tr>
         <tr>
            <td align="center" class="airline-text"><a href="/eWeb/TKT/V_TK_byFlight.asp?CARR_CD=CI" target"_top">中華航空</a></td>
            <td align="center" class="airline-text"><a href="/eWeb/TKT/V_TK_byFlight.asp?CARR_CD=BR" target"_top">長榮航空</a></td>
            <td align="center" class="airline-text"><a href="/eWeb/TKT/V_TK_byFlight.asp?CARR_CD=CX" target"_top">國泰航空</a></td>
            <td align="center" class="airline-text"><a href="/eWeb/TKT/V_TK_byFlight.asp?CARR_CD=KA" target"_top">港龍航空</a></td>
         </tr>
       <!---以下為第二行--->
        <tr>
            <td class="airline-td-width" align="center" valign="middle"><a href="/eWeb/TKT/V_TK_byFlight.asp?CARR_CD=SQ" target"_top"><img src="../images/CarrImg/SQ.jpg"></a></td>
            <td class="airline-td-width" align="center" valign="middle"><a href="/eWeb/TKT/V_TK_byFlight.asp?CARR_CD=MH" target"_top"><img src="../images/CarrImg/MH.jpg"></a></td>
            <td class="airline-td-width" align="center" valign="middle"><a href="/eWeb/TKT/V_TK_byFlight.asp?CARR_CD=TG" target"_top"><img src="../images/CarrImg/TG.jpg"></a></td>
            <td class="airline-td-width" align="center" valign="middle"><a href="/eWeb/TKT/V_TK_byFlight.asp?CARR_CD=NX" target"_top"><img src="../images/CarrImg/NX.jpg"></a></td>
         </tr>
         <tr>
            <td align="center" class="airline-text"><a href="/eWeb/TKT/V_TK_byFlight.asp?CARR_CD=SQ" target"_top">新加坡航空</a></td>
            <td align="center" class="airline-text"><a href="/eWeb/TKT/V_TK_byFlight.asp?CARR_CD=MH" target"_top">馬來西亞航空</a></td>
            <td align="center" class="airline-text"><a href="/eWeb/TKT/V_TK_byFlight.asp?CARR_CD=TG" target"_top">泰國航空</a></td>
            <td align="center" class="airline-text"><a href="/eWeb/TKT/V_TK_byFlight.asp?CARR_CD=NX" target"_top">澳門航空</a></td>
         </tr>
       <!---以下為第三行--->
         <tr>
            <td class="airline-td-width" align="center" valign="middle"><a href="/eWeb/TKT/V_TK_byFlight.asp?CARR_CD=KL" target"_top"><img src="../images/CarrImg/KL.jpg"></a></td>
            <td class="airline-td-width" align="center" valign="middle"><a href="/eWeb/TKT/V_TK_byFlight.asp?CARR_CD=KE" target"_top"><img src="../images/CarrImg/KE.jpg"></a></td>
            <td class="airline-td-width" align="center" valign="middle"><a href="/eWeb/TKT/V_TK_byFlight.asp?CARR_CD=JL" target"_top"><img src="../images/CarrImg/JL.jpg"></a></td>
            <td class="airline-td-width" align="center" valign="middle"><a href="/eWeb/TKT/V_TK_byFlight.asp?CARR_CD=DL" target"_top"><img src="../images/CarrImg/DL.jpg"></a></td>
         </tr>
         <tr>
            <td align="center" class="airline-text"><a href="/eWeb/TKT/V_TK_byFlight.asp?CARR_CD=KL" target"_top">荷蘭航空</a></td>
            <td align="center" class="airline-text"><a href="/eWeb/TKT/V_TK_byFlight.asp?CARR_CD=KE" target"_top">大韓航空</a></td>
            <td align="center" class="airline-text"><a href="/eWeb/TKT/V_TK_byFlight.asp?CARR_CD=JL" target"_top">日本航空</a></td>
            <td align="center" class="airline-text"><a href="/eWeb/TKT/V_TK_byFlight.asp?CARR_CD=DL" target"_top">達美航空</a></td>
         </tr>
       <!---以下為第四行--->
         <tr>
            <td class="airline-td-width" align="center" valign="middle"><a href="/eWeb/TKT/V_TK_byFlight.asp?CARR_CD=CA" target"_top"><img src="../images/CarrImg/CA.jpg" /></a></td>
            <td class="airline-td-width" align="center" valign="middle"><a href="/eWeb/TKT/V_TK_byFlight.asp?CARR_CD=MU" target"_top"><img src="../images/CarrImg/MU.jpg"></a></td>
            <td class="airline-td-width" align="center" valign="middle"><a href="/eWeb/TKT/V_TK_byFlight.asp?CARR_CD=MF" target"_top"><img src="../images/CarrImg/MF.jpg"></a></td>
            <td class="airline-td-width" align="center" valign="middle"><a href="/eWeb/TKT/V_TK_byFlight.asp?CARR_CD=xx" target"_top"><img src="../images/CarrImg/xx.jpg"></a></td>
         </tr>
         <tr>
            <td align="center" valign="center" class="airline-text"><a href="/eWeb/TKT/V_TK_byFlight.asp?CARR_CD=CA" target"_top">中國國際航空</a></td>
            <td align="center" valign="center" class="airline-text"><a href="/eWeb/TKT/V_TK_byFlight.asp?CARR_CD=MU" target"_top">中國東方航空</a></td>
            <td align="center" valign="center" class="airline-text"><a href="/eWeb/TKT/V_TK_byFlight.asp?CARR_CD=MF" target"_top">廈門航空</a></td>
            <td align="center" valign="center" class="airline-text"><a href="/eWeb/TKT/V_TK_byFlight.asp?CARR_CD=xx" target"_top"></a></td>
         </tr>
       </table>
    </div>
  </div>
</div>
<!--</div>-->
</body>
</html>
<%
End Sub
	'取得上游供應商ACCT_NO(下游向哪一家上游引用商品資料)
	function MYAGT_CD()
		Set rsTmp = Server.CreateObject("Adodb.Recordset")
		strSQL = "SELECT MYAGT_CD FROM TRCHNLACCT WHERE CHNL_ID = 'TRAVEL.NET' "
		rsTmp.open strSQL,egrpDSN,1,1		
		IF Not rsTmp.EOF Then 
		   aMYAGT_CD = rsTmp.Fields(0).Value
		End if   
		rsTmp.Close 
		MYAGT_CD =  aMYAGT_CD
	end function 
	
	Sub GetMYAGT(RS)
		strSQL = "SELECT MYAGT_CD,TN_ADM_URL  FROM TRCHNLACCT (NOLOCK) WHERE CHNL_ID = 'TRAVEL.NET' "		
		Set RS = Server.CreateObject("Adodb.Recordset")
		RS.Open strSQL, egrpDSN, 1, 1	
	End Sub
	
	'取得所設定的洲別資料
	Sub GetArea(RS)
		strSQL = "SELECT AREA_CD,AREA_SORT FROM TRTKTBANNER (NOLOCK) "
		strSQL = strSQL & "GROUP BY AREA_CD,AREA_SORT "
		strSQL = strSQL & "ORDER BY AREA_CD,AREA_SORT "			
		Set RS = Server.CreateObject("Adodb.Recordset")
		RS.Open strSQL, egrpDSN, 1, 1	
	End Sub
	
	'取得此洲別下的城市資料
	Sub GetBanner(pMYAGT_CD,pAREA_CD,RS)
		strSQL = "SELECT A.*,B.CITY_CNM,B.CITY_ENM FROM TRTKTBANNER A (NOLOCK) "
	    strSQL = strSQL & "JOIN TRCITY B (NOLOCK) ON A.CITY_CD = B.CITY_CD WHERE A.ACCT_NO = '" & pMYAGT_CD & "'"
	    strSQL = strSQL & "AND A.AREA_CD ='" & pAREA_CD & "'"
	    strSQL = strSQL & "ORDER BY A.AREA_SORT,A.CITY_SORT "
		Set RS = Server.CreateObject("Adodb.Recordset")
		RS.Open strSQL, egrpDSN, 1, 1		
	End Sub
%>

﻿<!--#INCLUDE VIRTUAL="/INCLUDE/egrpDSN_binary.inc" -->
<html><head><meta http-equiv="content-type" content="text/html; charset=UTF-8"><title>簽證資訊</title>
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
function NewOpenWindow() {
	opener.location.href="/eweb_demo/html/Html_Main.asp?html_fg=visadetail&VISA_CD=" + document.FORM1.ITN_NATN.value + "";
}
//此函數代表選洲別之後選擇國家
function SelArea(AreaCD)
{
var Area=document.FORM1;
	if (AreaCD != ""){
	<%	'=============== 2009-02-18 Ziv 只顯示已建簽證商品的洲別及國家(起) ==================	%>
		<!--strarea=showModalDialog("/eweb_demo/public/G_MS_Sel.asp?kind=1&DEP_CD="+AreaCD,"","dialogWidth=0pt;dialogHeight=0pt");-->
		strarea=showModalDialog("/eweb_demo/public/G_MS_Sel.asp?kind=3&DEP_CD="+AreaCD,"","dialogWidth=0pt;dialogHeight=0pt");
	<%	'=============== 2009-02-18 Ziv 只顯示已建簽證商品的洲別及國家(迄) ==================	%>	
		if(strarea){
			var ary_city = strarea.split(",");
				Area.ITN_NATN.length = ary_city.length;
			for(i=0;i<ary_city.length;i++){
				var sub_ary=ary_city[i].split("^^");
				Area.ITN_NATN.options[i]=new Option(sub_ary[1],sub_ary[0], i);}
		} else {
			Area.ITN_NATN.length = 1;
			Area.ITN_NATN.options[0]=new Option("","請選擇", 0);
			Area.ITN_NATN.defaultselectedIndex=0;}
	} else {
			Area.ITN_NATN.length = 1;
			Area.ITN_NATN.options[0]=new Option("","請選擇", 0);
			Area.ITN_NATN.defaultselectedIndex=0;		
  }
}
//-->
</script>
<link href="/css/demo.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.style1 {
	color: #0066CC;
	font-weight: bold;
}
-->
</style>
</head>
<body leftmargin="0" topmargin="0" onLoad="MM_preloadImages('/eWeb_<%=iWEB_ID%>/images/top_menu/top_menu001_on.gif')">
<form action="" method="post" name="FORM1" id="FORM1">
<table width="100%" height="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr> 
      <td height="21" valign="top"> <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td width="145" height="30" bgcolor="#CEECFF" class="text2">&nbsp;</td>
            <td width="30" bgcolor="#86CEFF">&nbsp; </td>
            <td width="49" bgcolor="#0099FF">&nbsp; </td>
            <td width="71" bgcolor="#0073BF">&nbsp; </td>
          </tr>
        </table>
        <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#666666">
        <tr> 
          <td height="1"> </td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td align="center" valign="top" class="visa-bg"><br>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="30"><br>
          </td>
          <td height="26" class="moneytitle"><img src="/eWeb_<%=iWEB_ID%>/images/visa_icon.gif" align="absmiddle"> 
            各國簽證：</td>
        </tr>
      </table>
      <br>
      <table width="100%" border="0" cellpadding="2" cellspacing="1">
        <tr > 
          <td align="center"> <span class="style1">（ 請選擇欲查詢簽證之洲別國家 ） 
            </span></td>
        </tr>
        <tr> 
          <td align="center" ><table border="0" cellpadding="0" cellspacing="0">
              <tr> 
                <td height="50" class="top-menu-link02"><img src="/eWeb_<%=iWEB_ID%>/images/right_icon.gif" align="absmiddle">洲別 
                  		  <select name="ITN_AREA" class="box" id="ITN_AREA" onChange="javascript:SelArea(this.value)">
		      	<option value="">--</option>
<%	
'	**********************************************************************
		'=============== 2009-02-26 Ziv 只顯示有上簽證商品的洲別(起) ==================
		'query01="select * from TRAREA"
		
		'query01 = "SELECT DISTINCT TRAREA.AREA_CD, TRAREA.AREA_CNM FROM TRAREA INNER JOIN TRNATN ON TRAREA.AREA_CD = TRNATN.AREA_CD WHERE TRNATN.NATN_CD IN (SELECT DISTINCT NATN_CD FROM TRVISA)"
		query01 = "SELECT DISTINCT TRAREA.AREA_CD, TRAREA.AREA_CNM FROM TRAREA INNER JOIN TRNATN ON TRAREA.AREA_CD = TRNATN.AREA_CD WHERE TRNATN.NATN_CD IN (SELECT DISTINCT NATN_CD FROM TRVISA LEFT JOIN TRSUBD ON TRVISA.VISA_CD=TRSUBD.VISA_CD WHERE WEB_PD='1')"
		'=============== 2009-02-26 Ziv 只顯示有上簽證商品的洲別(迄) ==================
		
			set RS1=Server.CreateObject("Adodb.Recordset")
			RS1.open query01, egrpDSN,1,1
			Do While Not RS1.Eof%>		
<%				If RS1("AREA_CD") = AreaCD then%>					  
				<option value="<%=RS1("AREA_CD")%>" selected><%=RS1("AREA_CNM")%></option>
<%				Else%>				
				<option value="<%=RS1("AREA_CD")%>"><%=RS1("AREA_CNM")%></option>	
<%				End If%>
<%			RS1.movenext
			loop
			RS1.close
			set RS1=nothing
'	**********************************************************************			%>	
          </select>
                </td>
              </tr>
              <tr> 
                <td height="50" class="top-menu-link02"><img src="/eWeb_<%=iWEB_ID%>/images/right_icon.gif" align="absmiddle">國家 
            <select name="ITN_NATN" class="box" id="ITN_NATN">
              		<option value="0" >--</option>
<%			If AreaCD<> "" then%>					
<%				query02="select * from TRNATN where AREA_CD='"&AreaCD&"'"				
				Set RS2=Server.CreateObject("Adodb.recordset")
				RS2.open query02, egrpDSN,1,1
				Do While Not RS2.eof
'					If country <> "" then
						If RS2("natn_cd")=ITN_NATN then%>			  
					<option value="<%=RS2("natn_cd")%>" selected><%=Trim(RS2("natn_cnm"))%></option>
<%						Else%>						
					<option value="<%=RS2("natn_cd")%>"><%=Trim(RS2("natn_cnm"))%></option>
<%						End If%>			
<%'					End If
				RS2.movenext
				loop
				RS2.close
				set RS2=nothing%>
<%			End If%>								
            </select> </td>
              </tr>
            </table></td>
        </tr>
      </table> 
      <BUTTON type="button"  hidefocus="true" class="order-input-button" style="CURSOR: hand" onClick="javascript:NewOpenWindow();window.close();" >
      <table border="0" align="center" cellpadding="0" cellspacing="0">
              <tr> 
               <td ><img src="/eWeb_<%=iWEB_ID%>/images/button_001.gif" ></td>
                
          <td align="center" background="/eWeb_<%=iWEB_ID%>/images/button_002.gif" class="top-menu-link02"><font color="#1B3661">確　定</font></td>
                <td><img src="/eWeb_<%=iWEB_ID%>/images/button_003.gif" ></td>
              </tr>
        </table>
            
      </BUTTON></td>
  </tr>
  <tr>
    <td height="21" valign="top"> 
      <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#666666">
        <tr> 
          <td height="1"> </td>
        </tr>
      </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr> 
            <td height="30" bgcolor="#CEECFF" class="text2">&nbsp;</td>
            <td bgcolor="#86CEFF">&nbsp; </td>
            <td bgcolor="#0099FF">&nbsp; </td>
            <td bgcolor="#0073BF">&nbsp; </td>
          </tr>
        </table></td>
  </tr>
</table></form></body></html>

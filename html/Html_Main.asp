﻿<%
Response.CacheControl="no-cache"
SITE_CD	= Request("SITE_CD")
MP_ID	= Request("MP_ID")
STYLE_TP= Request("STYLE_TP")
%>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<!--#INCLUDE VIRTUAL="/INCLUDE/egrpDSN_binary.inc" -->
<!--#include virtual="/include/myFunc.inc" -->
<!--#include virtual="/eWeb/Style/style_setting.asp" -->
<!--#include virtual="/include/eWeb_utility.asp" -->
<%
Sub Main()
'=== 會員專區主程式 ===
eWeb_URL = "/eWeb_"&iWEB_ID&"/"
'=== Hamburger ^__^||| ===============
'程式名稱：	Html_Main.asp
'功能描述：	會員專區主體
'使用資料：	TRWORD
'使用元件：
'適用對象：	會員
'使用時機：	進入會員專區
'包含程式：
'加註日期：	2004/04/29
'更新日期：	2004/04/29 
'更新說明：
'			2004-09-03	[brian]	修改相關頁面連結
'=====================================
%>
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="member-all-table-width">
  <tr>
    <td>
	  <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td align="center">
			<%
			html_fg=request.QueryString("html_fg")
			select case html_fg
			case "company"              	'科威簡介		
			%><!--#include virtual="/eweb_demo/html/pages/company.asp" --><%
			case "fax"                      '刷卡單			
			%><!--#include virtual="/eweb_demo/html/pages/fax.html" --><%
			case "info"                  	'旅遊資訊			
			%><!--#include virtual="/eweb_demo/html/pages/info.html" --><%															
			end select%> 
		  </td>
        </tr>
      </table>
	</td>
  </tr>
</table>
<% End Sub %>

﻿$(function(){
	var timer;
	var ImgStep = -1;
	var speed = 8 * 1000;  // 5 秒
	
	var myImages = $(".AdSrc a");	
	$("#AdShow").slideDown();
	
	// 建出上方 tabs
	$(".AdTab").append("<ul />");
	for(var i=0;i<myImages.length;i++){
		var _ImgBlock = myImages.eq(i);
		var adtitle=_ImgBlock.find("img").attr("title");
		$(".AdTab ul").append("<li>"+adtitle+"</li>");
	}
	$(".AdTab li").click(
		function(){
			var idx = $(".AdTab li").index(this);		
			var _ImgBlock = myImages.eq(idx);
			var adlink=_ImgBlock.attr("href");
			window.open(adlink);
		}
	);
	//滑鼠移入頁籤切換
	$(".AdTab li").mouseover(
		function(){
			// 找出是要換成那張圖
			var idx = $(".AdTab li").index(this);	
			// 記住目前是到那張圖
			ImgStep = idx;	
			var _ImgBlock = myImages.eq(idx);
			
			//取得連結、圖片src
			var adsrc=_ImgBlock.find("img").attr("src");
			var adlink=_ImgBlock.attr("href");
					
			// 換連結
			$(".AdImg").find("a").attr({
				href: adlink
			});
			// 換圖 
			$(".AdImg").find("img").attr("src",adsrc);
			
			// 改變 tab 的狀態
			$(this).removeClass("off").addClass("on")
				.siblings().removeClass("on").addClass("off");		
		}
		
	);
	$(".AdShow").hover(
		function(){
			clearTimeout(timer);
		}, 
		function(){
			timer = setTimeout(autoShow, speed);
		}
	);
	
	// 設定輪播 (模擬 mouseover 動作)
	function autoShow(){
		ImgStep = (ImgStep+1<myImages.length) ? ImgStep+1 : 0;
		$(".AdTab li").eq(ImgStep).mouseover();
		timer = setTimeout(autoShow, speed);
	}
	autoShow();
	
});

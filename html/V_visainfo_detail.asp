﻿<!--INCLUDE VIRTUAL="/INCLUDE/egrpDSN_binary.inc" -->
<%'基本參數
'=== 更新紀錄 ===
'2005/12/20	Hamburger	參數名稱/來源調整
'2005/12/20	Hamburger	重新取得辦證價格
'2005-12-20 Hamburger	調整辦證價格取得順序
'================
NATN_CD=request("VISA_CD")
Set visaRS = Server.CreateObject("ADODB.Recordset")
'查詢簽證資訊
strSQL="select natn.NATN_CNM AS NATN_CNM, visa.VISA_NM, visa.VISA_RK, visa.VISA_RK2,visa.* "
strSQL=strSQL& " from TRVISA visa "
strSQL=strSQL& " INNER JOIN TRNATN natn ON visa.NATN_CD = natn.NATN_CD "
strSQL=strSQL& " where visa.NATN_CD='"&NATN_CD&"'"
visaRS.Open strSQL, egrpDSN, 1, 1
if not visaRS.EOF then
	'SQLPR = "SELECT CURR_CD,CURR_AM FROM TRSUBD WHERE VISA_CD='" & TRIM(visaRS("VISA_CD")) & "' AND SUB_CD='V'"
	'Response.Write(SQLPR&"<br>")
	'SET RSPR = SERVER.CREATEOBJECT("ADODB.RECORDSET")
	'RSPR.OPEN SQLPR,egrpDSN,1,1
	'IF NOT RSPR.EOF THEN
	'	oCURR_CD = TRIM(RSPR("CURR_CD"))
	'	oCURR_AM = TRIM(RSPR("CURR_AM"))
	'	if oCURR_AM<>"" then oCURR_AM=Format_AM(Cdbl(oCURR_AM),0)
	'ELSE
	'	oCURR_CD = ""
	'	oCURR_AM = ""
	'END IF
%>
<link href="/css/demo.css" rel="stylesheet" type="text/css">
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr> 
    <td align="center" valign="top" > 
      <table width="98%" border="0" cellpadding="0" cellspacing="0">
        <tr> 
          <td width="20" height="19" bgcolor="#0073BF">&nbsp; </td>
          <td width="387" bgcolor="#CEECFF" class="text2">　簽證資訊</td>
          <td width="40" bgcolor="#86CEFF">&nbsp; </td>
          <td width="48" bgcolor="#0099FF">&nbsp;</td>
          <td width="65" bgcolor="#0073BF">&nbsp; </td>
        </tr>
		<%
			do until visaRS.eof
			'=== 2005-12-20 調整辦證價格取得順序 ===
			SQLPR = "SELECT CURR_CD,CURR_AM FROM TRSUBD WHERE VISA_CD='" & TRIM(visaRS("VISA_CD")) & "' AND SUB_CD='V'"
			SET RSPR = SERVER.CREATEOBJECT("ADODB.RECORDSET")
			RSPR.OPEN SQLPR,egrpDSN,1,1
			IF NOT RSPR.EOF THEN
				oCURR_CD = TRIM(RSPR("CURR_CD"))
				oCURR_AM = TRIM(RSPR("CURR_AM"))
				if oCURR_AM<>"" then oCURR_AM=Format_AM(Cdbl(oCURR_AM),0)
			ELSE
				oCURR_CD = ""
				oCURR_AM = ""
			END IF
			'=== 2005-12-20重新取得辦證價格 ===
'			oCURR_AM=Format_AM(visaRS("VISA_FE"),0)
			'==================================
			%>
        <tr align="center" bgcolor="#666666"> 
          <td height="1" colspan="5"> </td>
        </tr>
        <tr align="center"> 
          <td colspan="5"><br> 
            <table width="100%" border="0" cellpadding="5" cellspacing="1" bgcolor="#B2D5FE">
              <tr bgcolor="#FFFFFF"> 
                <td width="100" bgcolor="#DDF7FF" class="text4">‧證照名稱 </td>
                <td colspan="3" class="link04"><%= visaRS("VISA_NM") %></td>
              </tr>
              <tr bgcolor="#FFFFFF"> 
                <td bgcolor="#DDF7FF" class="text4">‧證照效期</td>
                <td class="link04"><%=visaRS("VLDE_YY")%>年<%=visaRS("VLDE_MM")%>月</td>
                <td width="100" bgcolor="#DDF7FF" class="text4">‧辦件工作日</td>
                <td class="link04"><%=visaRS("APPL_DY")%>&nbsp;天</td>
              </tr>
              <tr bgcolor="#FFFFFF"> 
                <td bgcolor="#DDF7FF" class="text4">‧當地停留時間</td>
                <td class="link04"><%'=visaRS("VLDE_DT")%><%=visaRS("BEDATE")%>&nbsp;天</td>
                <td bgcolor="#DDF7FF" class="text4">‧參考價格</td>
                <td class="link04"><%= oCURR_CD %>&nbsp;<%'= oCURR_AM0 %><%= oCURR_AM %></td>
              </tr>
              <tr bgcolor="#F6FEF6"> 
                <td colspan="4" bgcolor="#DDF7FF" class="text4">‧辦證須知</td>
              </tr>
              <tr bgcolor="#FFFFFF"> 
                <td colspan="4" class="link04"><%=visaRS("VISA_RK2")%></td>
              </tr>
              <tr bgcolor="#F6FEF6"> 
                <td colspan="4" bgcolor="#DDF7FF" class="text4">‧證照備註</td>
              </tr>
              <tr bgcolor="#FFFFFF"> 
                <td colspan="4" class="link04"><%=visaRS("VISA_RK")%></td>
              </tr>
            </table></td>
        </tr>
		<%		visaRS.movenext
				loop%>
      </table>
      <br> </td>
  </tr>
</table>
<% else %>
<table width="325" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center" class="link">很抱歉，找不到相關資料!!</td>
  </tr>
</table>
<% 	end if 
	visaRS.Close%>

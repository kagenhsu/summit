﻿<%
'//===========================================================================================
'//						getTktBanner.asp
'//-------------------------------------------------------------------------------------------
'//		description		: 機票次首頁:取得DataCenter上的洲別城市機票廣告資料
'//		table			: 
'//		include			: 
'//		version			: 
'//		begin			: 2010-12-21
'//		copyright		: (C) 2010 COWELL
'//		author			: Gary
'//		2010-12-23		: [Gary] [ew.v902.054U] 機票次首頁
'//-------------------------------------------------------------------------------------------
	
	'Web Service預設回傳是UTF-8，我們前中台是Big5所以在此宣告
	Response.cachecontrol = "no-cache"
	
	'連結至提供WS的IP
	iTN_ADM_URL = request("LINK")	
	'上流統編
	iACCT_NO = request("ACCT_NO")
	
	'設定預定單明細WS所需參數
	FuncName = "GetTicketBanner"
	NameSpace = "http://tempuri.org/"
	RequestXml = "<pACCT_NO>" & iACCT_NO & "</pACCT_NO>"
	url = iTN_ADM_URL & "/TN/DPC/WebService/WSTicketManager.asmx"
	'url = "http://10.10.1.17:8000/TN/DPC/WebService/WSTicketManager.asmx"		
	'定義soap
	strxml = "<?xml version = ""1.0"" encoding = ""UTF-8""?>" 
	strxml = strxml & "<soap:Envelope xmlns:xsi = ""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd = ""http://www.w3.org/2001/XMLSchema"" xmlns:soap = ""http://schemas.xmlsoap.org/soap/envelope/"">" 
	strxml = strxml & "<soap:Body> " 	
	strxml = strxml & "<" & FuncName & " xmlns = """ & NameSpace & """>" 
	strxml = strxml & RequestXml
	strxml = strxml & "</" & FuncName & ">"
	strxml = strxml & "</soap:Body>" 
	strxml = strxml & "</soap:Envelope>" 
	Set XmlHttp = createobject( "Microsoft.XMLHTTP") 	
	XmlHttp.open "POST", url, False 
	'XmlHttp.open "POST", url, True '非同步
	XmlHttp.setRequestHeader "Content-Type", "text/xml; charset=utf-8" 
	XmlHttp.setRequestHeader "Content-Length",LEN(strxml) 
	SOAPAction = ""
	if right(NameSpace,1)="/" then
		SOAPAction= NameSpace&FuncName 
	else
		SOAPAction= NameSpace & "/" & FuncName
	end if
	XmlHttp.setRequestHeader "SOAPAction", SOAPAction
	XmlHttp.send (strxml)
	
	'顯示返回的XML信息
	If XmlHttp.Status = 200 Then 
	   '因為WS回傳為XML字串格式，所以會部份資料會被改變
	   'Ws回傳回來的資料會部份資料會被改變，在這裡做轉換動作
	   strXml = dvHTMLEncode(XmlHttp.responseText)								
	Else 
	   strXml = "Err"
	End if 
	Set XmlHttp = Nothing
	
	
	Response.Write(strXml)
	Response.End()
	'Ws回傳回來的資料會部份資料會被改變，在這裡做轉換動作
	Function dvHTMLEncode(fString)
		If Not IsNull(fString) Then
			fString = replace(fString, ">", "&gt;")
			fString = replace(fString, "<", "&lt;")
			fString = Replace(fString, CHR(32), "<I></I>&nbsp;")
			fString = Replace(fString, CHR(9), "&nbsp;")
			fString = Replace(fString, CHR(34), "&quot;")
			fString = Replace(fString, CHR(39), "&#39;")
			fString = Replace(fString, CHR(13), "")
			fString = Replace(fString, CHR(10) & CHR(10), "</P><P> ")
			fString = Replace(fString, CHR(10), "<br> ")			
			dvHTMLEncode = fString
		End If
	End Function
%>

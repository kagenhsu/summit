
<!--以參數 fbhtml_url作為存取本視窗網址字串參數。-->
<script>
var fbhtml_url=window.location.toString();
</script>

<nav id="top" class="navbar navbar-fixed-top navbar-inverse">
    <div class="container">
        <div class="row">
		
            <div class="navbar-header">
                <button data-toggle="collapse-side" data-target-sidebar=".side-collapse-left" data-target-content=".side-collapse-container-left" type="button" class="navbar-toggle pull-left">
                    <a class="nav-btn" id="nav-open-btn" href="#nav"></a>
                </button>
                <h2 class="block-title">
                    <a href="http://www.summittour.com.tw"><img src="/eWeb_summittour/images/default/logo02-new.png"/></a>
                    <!--<a class="navbar-brand" href="#">Design</a>-->
                </h2>
				<div class="navbar-inverse1 side-collapse-right in">
					<nav id="nav1" role="navigation" class="navbar-collapse1">
						<ul class="nav navbar-nav1 navbar-right ">
							<li class="">
								<a href="/eWeb_summittour/page/member.asp"><p>加入會員</p></a>
							</li>
							<li class="">
								<a href="/eWeb_summittour/index.asp"><p>回頂尖首頁</p></a>
							</li>
							<!--會員登入-->
							
								<%If (Session("USR_ID") <> "") Then%>
									<li class="top_member hidden-md">
										<span style="color: #690;">HI， &nbsp;<%=Session("USR_CNM")%> 歡迎您！ &nbsp;</span>
									</li>
									<li class="top_member hidden-md">
										<span><a href="/eweb/Main/O_Logout.asp" target="_top"><span>登出&nbsp;</span></a></span>
									</li>
									</li>
									
									
								<%elseif Session("COMP_CD") <> "" then%>
									<li class="top_member hidden-md">
										<span><a href="/eWeb_summittour/page/member.asp"></a></span>
									</li>
									<li class="top_member hidden-md">
										<span style="color: #690;">>HI， &nbsp;<%=COMP_ANM%> 歡迎您！ &nbsp;</span>
									</li>
									<li class="top_member hidden-md">
										<span><a href="/eweb/Main/O_Logout.asp" target="_top"><span>登出&nbsp;</span></a></span>
									
									
								<%End If %>
							<!--會員登入-END-->
							
						</ul>
					</nav>
				</div>
				
				<div class="navbar-inverse side-collapse-left in">

					<nav id="nav" role="navigation" class="navbar-collapse">
						<ul class="nav navbar-nav navbar-left">
							<li><a href="#DAREA00001">港澳大陸</a></li>
							<li><a href="#DAREA00017">蒙古</a></li>
							<li><a href="#DAREA00004">東南亞</a></li>
							<li><a href="#DAREA00002">東北亞</a></li>
							<li><a href="#DAREA00009">南亞、中亞非</a></li>
							<li><a href="#DAREA00010">歐洲</a></li>
							<li><a href="#DAREA00011">紐澳</a></li>
							<li><a href="#DAREA00014">美加</a></li>
							<li><a href="#DAREA00015">海島</a></li>
							<li><a href="#DAREA00016">中南半島</a></li>
							<li><a href="#DAREA00007">國內離島</a></li>
							<li><a href="#DAREA00008">遊輪</a></li>
							<li class="hidden-lg"> 
								<a href="javascript: void(window.open('http://www.facebook.com/share.php?u='.concat(encodeURIComponent(location.href)) ));" target="_blank"><p>fb分享</p></a>
							</li>
							<li class="hidden-lg">
								<a href="/eWeb_summittour/Prefecture/Prefecture01.asp"><p>Line@</p></a>
							</li>
							<li class="hidden-lg">
								<a href="/eWeb_summittour/page/member.asp"><p>加入會員</p></a>
							</li>
							<!--li class="hidden-lg">
								<a href="javascript:;"><p>訂閱電子報</p></a>
							</li-->
							<!--li class="hidden-lg">
								<a href="/eweb_summittour/diy/V_diy.asp"><p>客製化旅遊</p></a>
							</li-->
							<li class="hidden-lg">
								<a href="/eWeb_summittour/index.asp"><p>回首頁</p></a>
							</li>
							<!--會員登入-->
							<%If (Session("USR_ID") <> "") Then%>
							<li class="hidden-lg hidden-md">
							<span style="color: #690;">親愛的 &nbsp;<%=Session("USR_CNM")%> 您好~ &nbsp;</span>
								<a href="/eweb/Main/O_Logout.asp" target="_top"><span style="color: #690;">登出</span></a>
							</li>
							<%elseif Session("COMP_CD") <> "" then%>
							<li class="hidden-lg hidden-md">
								<a href="/eWeb_summittour/page/member.asp"></a>
							<span style="color: #690;">親愛的 &nbsp;<%=COMP_ANM%> 您好~ &nbsp;</span>
								<a href="/eweb/Main/O_Logout.asp" target="_top"><span style="color: #690;">登出</span></a>
							</li>
							<%End If %>
							<!--會員登入-END-->
													
							
						</ul>
					</nav>
				</div>
				
				<!--button data-toggle="collapse-side1" data-target-sidebar1=".side-collapse-right" data-target-content1=".side-collapse-container-right" type="button" class="navbar-toggle1 pull-right">
					<a class="nav-btn" id="nav-open-btn" href="#nav1"></a>
				</button-->
          </div>
        </div>
      </div>
</nav>
    
<!-- Modal -->
<div class="modal fade bs-example-modal-sm" id="myMember" tabindex="-1" role="dialog" aria-labelledby="myMemberLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myMemberLabel">會員中心</h4>
      </div>
      <div class="modal-body">
        <!--#include virtual="/eWeb_summittour/public/login.asp"-->
      </div>
    </div>
  </div>
</div>


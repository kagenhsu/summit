<footer role="contentinfo">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 footer_nav">
		<ul>
			<li><a href="https://www.facebook.com/summittourTaiwan" target="_blank">FB粉專頁</a></li>
			<li><a href="/eWeb_summittour/auto/about.asp">關於超悠旅遊</a></li>
			<li><a href="/eweb_summittour/images/movable/超悠旅行社新版刷卡單.pdf" target="_blank">信用卡授權書下載</a></li>
			<li><a href="https://tw.money.yahoo.com/currency-converter" target="_blank">匯率查詢</a></li>
			<li><a href="http://www.cwb.gov.tw/V7/index.htm" target="_blank">全球氣候</a></li>
			<li><a href="/eWeb_summittour/page/airline_info.asp">航空資訊</a></li>
			<li><a href="/eWeb_summittour/page/visa.asp">簽證資訊</a></li>
		</ul>
	</div>
    <div class="container footer_info">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 info_content">
                <a href="http://www.summittour.com.tw"><img src="/eWeb_summittour/images/default/logo-new.png" style="width: 75%;" /></a>
				<h3>超悠旅行社股份有限公司</h3>
                <h4>CH’AO TRAVEL AGENCY CO,LTD</h4>
                <p>甲種旅行社 交觀甲字第7398號 品保中0346</p>
                <p>代表人：林瑞恩 聯絡人：陳力嘉</p>
                <p>E-Mail：summit1@summittour.com.tw</p>
                <p>Copyright c summitt our.com All Rights R eserved.</p>
                <p>版權所有，禁止未經授權轉貼節錄</p>
                <p>各項產品及服務為超悠旅行社股份有限公司提供</p>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 info_content">
                <h4>台中總公司</h4>
                <ul>
                    <li>403 台中市西區向上路ㄧ段25號3樓</li>
                    <li>電話：04-23052639、04-22208155</li>
                    <li>傳真：04-23052659、04-22209306</li>
                </ul>
                <h4>台北分公司</h4>
                <ul>
                    <li>104 台北市中山區長安東路2段163號2樓</li>
                    <li>電話：02-27112387 傳真：02-27112360</li>
                </ul>
				<h4>新竹分公司</h4>
                <ul>
                    <li>302 新竹縣竹北市三民路536號2樓</li>
                    <li>電話：03-5520969 傳真：03-5520881</li>
                </ul>
				
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 footer_mark">
                <img src="/eWeb_summittour/images/default/mark.png">
                <ul>
                    <li>為確保您能獲得更為穩定的瀏覽品質與使用體驗</li>
                    <li>建議您使用 Chr ome 或 IE10 以上瀏覽器瀏覽本網站</li>
                </ul>
            </div>
        </div>
    </div>
</footer>

<link type="text/css" rel="stylesheet" href="http://www.bwt.com.tw/eWeb/GO/type_screen.css?0913">
    
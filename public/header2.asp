



<nav id="top" class="navbar navbar-fixed-top navbar-inverse">
    <div class="container">
        <div class="row">
		
            <div class="navbar-header">
                <button data-toggle="collapse-side" data-target-sidebar=".side-collapse-left" data-target-content=".side-collapse-container-left" type="button" class="navbar-toggle pull-left">
                    <a class="nav-btn" id="nav-open-btn" href="#nav"></a>
                </button>
                <h2 class="block-title">
                    <a href="http://www.summittour.com.tw"><img src="/eWeb_summittour/images/default/logo02-new.png" /></a>
                    <!--<a class="navbar-brand" href="#">Design</a>-->
                </h2>
				<div class="navbar-inverse1 side-collapse-right in">
					<nav id="nav1" role="navigation" class="navbar-collapse1">
						<ul class="nav navbar-nav1 navbar-right ">
							<li class="">
								<a href="https://www.facebook.com/summittourTaiwan" target="_blank"><p>粉絲專頁</p></a>
							</li>
							<li class="">
								<a href="https://line.me/R/ti/p/%40uaj2428d"><p>Line@</p></a>
							</li>
							<li class="">
								<a href="https://line.me/R/ti/p/%40uaj2428d"><p>線上客服</p></a>
							</li>
							<li class="">
								<a href="/eWeb_summittour/page/member.asp"><p>會員專區</p></a>
							</li>
							<li class="">
								<a href="javascript:;"><p>訂閱電子報</p></a>
							</li>
							<li class="">
								<a href="/eweb_summittour/diy/V_diy.asp"><p>客製化旅遊</p></a>
							</li>
							<li class="">
								<a href="/eWeb_summittour/index.asp"><p>回首頁</p></a>
							</li>
							<!--會員登入-->
							
								
							
						</ul>
					</nav>
				</div>
				
				<div class="navbar-inverse side-collapse-left in">

					<nav id="nav" role="navigation" class="navbar-collapse">
						<ul class="nav navbar-nav navbar-left">
							<!--li><a href="javascript:;">港澳大陸</a></li-->
							<li class="dropdown" onmouseover="dropdown-toggle">
								<a href="/eWeb_summittour/auto/Central_tours_CN.asp" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true">港澳大陸</a>
								<ul class="dropdown-menu" role="menu">	
								  <li><a href="/eWeb_summittour/auto/Central_tours_CN.asp#tab1">東北(大連、瀋陽、哈爾濱、長春、長白山)</a></li>
								  <li><a href="/eWeb_summittour/auto/Central_tours_CN.asp#tab2">山東(濟南、青島、威海、煙台、徐州）</a></li>
								  <li><a href="/eWeb_summittour/auto/Central_tours_CN.asp#tab3">北京、天津、承德</a></li>
								  <li><a href="/eWeb_summittour/auto/Central_tours_CN.asp#tab4">山西(太原)、內蒙(呼和浩特)、銀川	</a></li>
								  <li><a href="/eWeb_summittour/auto/Central_tours_CN.asp#tab5">鄭州、西安</a></li>
								  <li><a href="/eWeb_summittour/auto/Central_tours_CN.asp#tab6">九寨溝、稻城亞丁</a></li>
								  <li><a href="/eWeb_summittour/auto/Central_tours_CN.asp#tab7">長沙、張家界</a></li>
								  <li><a href="/eWeb_summittour/auto/Central_tours_CN.asp#tab8">重慶、武漢、長江三峽</a></li>
								  <li><a href="/eWeb_summittour/auto/Central_tours_CN.asp#tab9">江南、黃山、江西(南昌)</a></li>
								  <li><a href="/eWeb_summittour/auto/Central_tours_CN.asp#tab10">福建(廈門、福州、武夷山)</a></li>
								  <li><a href="/eWeb_summittour/auto/Central_tours_CN.asp#tab11">桂林、南寧</a></li>
								  <li><a href="/eWeb_summittour/auto/Central_tours_CN.asp#tab12">雲南(昆明、大理、麗江、中甸)、貴陽</a></li>
								  <li><a href="/eWeb_summittour/auto/Central_tours_CN.asp#tab13">澳門、珠海、深圳、廣州</a></li>
								  <li><a href="/eWeb_summittour/auto/Central_tours_CN.asp#tab14">海南(海口、三亞)</a></li>
								  <li><a href="/eWeb_summittour/auto/Central_tours_CN.asp#tab15">南北疆、絲路</a></li>
								  <li><a href="/eWeb_summittour/auto/Central_tours_CN.asp#tab16">西藏(拉薩、青藏鐵路)</a></li>
								  <!--li><a href="/eWeb_summittour/auto/Central_tours_CN.asp#tab17">港澳地區</a></li-->
								  <!--li><a href="/eWeb_summittour/auto/Central_tours_CN.asp#tab18">港澳大陸自由行</a></li-->
								</ul>
							</li>
							<li class="dropdown"> 
								<a href="/eWeb_summittour/auto/Central_tours_MY.asp" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true">東亞</a>
								<ul class="dropdown-menu" role="menu">	
								  <li><a href="/eWeb_summittour/auto/Central_tours_MY.asp#tab1">蒙古國</a></li>
								  <li><a href="/eWeb_summittour/auto/Central_tours_MY.asp#tab2">蒙古國+西伯利亞</a></li>
								</ul>
							</li>
							<li class="dropdown"> 
								<a href="/eWeb_summittour/auto/Central_tours_NSA.asp" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true">東南亞</a>
								<ul class="dropdown-menu" role="menu">	
								  <li><a href="/eWeb_summittour/auto/Central_tours_NSA.asp#tab1">泰國(曼谷、清邁、華欣、蘇梅島、普吉島)</a></li>
								  <li><a href="/eWeb_summittour/auto/Central_tours_NSA.asp#tab2">印尼(雅加達、峇里島、日惹、龍目島)</a></li>
								  <li><a href="/eWeb_summittour/auto/Central_tours_NSA.asp#tab3">西馬來西亞(吉隆坡、檳城)、新加坡</a></li>
								  <li><a href="/eWeb_summittour/auto/Central_tours_NSA.asp#tab4">東馬來西亞(沙巴、納閩、沙勞越)、汶萊</a></li>
								  <li><a href="/eWeb_summittour/auto/Central_tours_NSA.asp#tab5">菲律賓(長灘島、宿霧、巴拉望、科隆)</a></li>
								  <!--li><a href="/eWeb_summittour/auto/Central_tours_NSA.asp#tab6">東南亞自由行</a></li-->
								</ul>
							</li>							
							<li class="dropdown">
								<a href="/eWeb_summittour/auto/Central_tours_NEA.asp" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true">東北亞</a>
								<ul class="dropdown-menu" role="menu">	
								  <li><a href="/eWeb_summittour/auto/Central_tours_NEA.asp#tab1">東京(迪士尼、箱根、輕井澤)</a></li>
								  <li><a href="/eWeb_summittour/auto/Central_tours_NEA.asp#tab2">關東(茨城、栃木、群馬、埼玉、千葉、東京都、神奈川)</a></li>
								  <li><a href="/eWeb_summittour/auto/Central_tours_NEA.asp#tab3">關西(大阪、京都、神戶、奈良、南紀)</a></li>
								  <li><a href="/eWeb_summittour/auto/Central_tours_NEA.asp#tab4">四國、中國(道後溫泉、小豆島、廣島、岡山)</a></li>
								  <li><a href="/eWeb_summittour/auto/Central_tours_NEA.asp#tab5">北陸黑部立山(名古屋、富山、小松)</a></li>
								  <li><a href="/eWeb_summittour/auto/Central_tours_NEA.asp#tab6">東北(花卷、仙台、青森、秋田、新潟)</a></li>
								  <li><a href="/eWeb_summittour/auto/Central_tours_NEA.asp#tab7">沖繩(石垣、宮古、與論、琉球、久米島)	</a></li>
								  <li><a href="/eWeb_summittour/auto/Central_tours_NEA.asp#tab8">北海道(札幌、函館、旭川)</a></li>
								  <li><a href="/eWeb_summittour/auto/Central_tours_NEA.asp#tab9">九州(大分、福岡、宮崎、鹿兒島、熊本)</a></li>
								  <li><a href="/eWeb_summittour/auto/Central_tours_NEA.asp#tab10">韓國(首爾.釜山.濟州島)</a></li>
								  <!--li><a href="/eWeb_summittour/auto/Central_tours_NEA.asp#tab11">東北亞自由行</a></li-->
								</ul>
							</li>							
							<li class="dropdown">
								<a href="/eWeb_summittour/auto/Central_tours_SA-CA.asp" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true">南亞、中亞非</a>
								<ul class="dropdown-menu" role="menu">	
								  <li><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0011&regsCd=0011-0002&beginDt=2017/08/22&endDt=2018/12/31&allowJoin=1">印度</a></li>
								  <li><a href="/eWeb_summittour/auto/Central_tours_SA-CA.asp#tab2">尼泊爾、斯里蘭卡</a></li>
								  <!--li><a href="/eWeb_summittour/auto/Central_tours_SA-CA.asp#tab3">土耳其</a></li-->
								  <li><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0011&regsCd=0011-0004&beginDt=2017/08/22&endDt=2018/12/31&allowJoin=1">杜拜</a></li>
								  <li><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0011&regsCd=0011-0005&beginDt=2017/08/22&endDt=2018/12/31&allowJoin=1">伊朗、以色列</a></li>
								  <li><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0011&regsCd=0011-0006&beginDt=2017/08/22&endDt=2018/12/31&allowJoin=1">埃及</a></li>
								  <!--li><a href="/eWeb_summittour/auto/Central_tours_SA-CA.asp#tab7">南非</a></li-->
								  <!--li><a href="/eWeb_summittour/auto/Central_tours_SA-CA.asp#tab8">肯亞</a></li-->
								  <!--li><a href="/eWeb_summittour/auto/Central_tours_SA-CA.asp#tab9">南亞、中亞非自由行</a></li-->
								</ul>
							</li>							
							<li class="dropdown">
								<a href="/eWeb_summittour/auto/Central_tours_EU.asp" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true">歐洲</a>
								<ul class="dropdown-menu" role="menu">
								<li><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0012&regsCd=0012-0001&beginDt=2018/05/16&endDt=2099/12/31&allowJoin=1">英國</a></li>
								<li><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0012&regsCd=0012-0002&beginDt=2018/05/16&endDt=2099/12/31&allowJoin=1">法國</a></li>
								<li><a href="/EW/GO/GroupList.asp?regmCd=0012&regsCd=0012-0009&beginDt=2017/08/15&endDt=2099/12/31&allowJoin=1">瑞士</a></li>
								<li><a href="/EW/GO/GroupList.asp?regmCd=0012&regsCd=0012-0006&beginDt=2017/08/15&endDt=2099/12/31&allowJoin=1">德國</a></li>
								<li><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0012&regsCd=0012-0029&beginDt=2018/05/16&endDt=2099/12/31&allowJoin=1">荷蘭</a></li>
								<li><a href="/EW/GO/GroupList.asp?regmCd=0012&regsCd=0012-0004&beginDt=2017/08/15&endDt=2099/12/31&allowJoin=1">西班牙</a></li>
								<li><a href="/EW/GO/GroupList.asp?regmCd=0012&regsCd=0012-0016&beginDt=2017/08/15&endDt=2099/12/31&allowJoin=1">冰島</a></li> 
								<li><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0012&regsCd=0012-0022&beginDt=2018/05/16&endDt=2099/12/31&allowJoin=1">義大利</a></li> 
								<li><a href="/EW/GO/GroupList.asp?regmCd=0012&regsCd=0012-0026&beginDt=2017/08/15&endDt=2099/12/31&allowJoin=1">俄羅斯</a></li>
								<li><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0012&regsCd=0012-0023&beginDt=2018/05/16&endDt=2099/12/31&allowJoin=1">北歐多國(丹麥、芬蘭、冰島、挪威、瑞典)</a></li>
								<li><a href="/EW/GO/GroupList.asp?regmCd=0012&regsCd=0012-0024&beginDt=2017/08/15&endDt=2099/12/31&allowJoin=1">中西歐多國(英.法.荷.比.盧.德.瑞)</a></li>
								<li><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0012&regsCd=0012-0025&beginDt=2018/05/16&endDt=2099/12/31&allowJoin=1">東歐多國((奧地利.捷克.匈牙利.波蘭)</a></li>
								<li><a href="/EW/GO/GroupList.asp?regmCd=0012&regsCd=0012-0027&beginDt=2017/08/15&endDt=2099/12/31&allowJoin=1">南歐(義大利.西班牙.葡萄牙.南歐.希臘)</a></li>
								<li><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0012&regsCd=0012-0031&beginDt=2018/05/16&endDt=2099/12/31&allowJoin=1">歐洲巴士循環團</a></li>
								<li><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0012&regsCd=0012-0032&beginDt=2018/05/16&endDt=2099/12/31&allowJoin=1">歐洲城市周邊一天遊</a></li>
								<li><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0012&regsCd=0012-0033&beginDt=2018/06/29&endDt=2099/12/31&allowJoin=1">克斯蒙（克羅埃西亞‧斯洛維尼亞‧蒙地內哥)</a></li>
								<li><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0012&regsCd=0012-0034&beginDt=2018/06/29&endDt=2099/12/31&allowJoin=1">魅力歐洲(聯營團體)</a></li>
								<li><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0012&regsCd=0012-0035&beginDt=2018/06/29&endDt=2099/12/31&allowJoin=1">中西歐單國</a></li>
								<li><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0012&regsCd=0012-0036&beginDt=2018/06/29&endDt=2099/12/31&allowJoin=1">東歐+巴爾幹</a></li>
								</ul>
							</li>
							
							<li>
								<a href="/eWeb_summittour/auto/Central_tours_NZ-AU.asp" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true">紐澳</a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0013&regsCd=0013-0001&beginDt=2018/01/09&endDt=2099/12/31&allowJoin=1">雪梨</a></li>
									<li><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0013&regsCd=0013-0003&beginDt=2018/01/11&endDt=2099/12/31&allowJoin=1">布里斯班</a></li>
									<li><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0013&regsCd=0013-0009&beginDt=2018/01/11&endDt=2099/12/31&allowJoin=1">紐西蘭南北島</a></li>
									<li><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0013&regsCd=0013-0010&beginDt=2018/01/11&endDt=2099/12/31&allowJoin=1">紐西蘭南島</a></li>
								</ul>
							</li>
							<li class="dropdown">
								<a href="/eWeb_summittour/auto/Central_tours_US.asp" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true">美加</a>
									<ul class="dropdown-menu" role="menu">	
									<li><a href="/eWeb_summittour/auto/Central_tours_US.asp#tab1">洛杉磯</a></li>
									<li><a href="/eWeb_summittour/auto/Central_tours_US.asp#tab2">舊金山</a></li>
									<li><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0017&regsCd=0017-0003&beginDt=2018/03/08&endDt=2099/12/31&allowJoin=1&ikeyword=%E5%A4%8F%E5%A8%81%E5%A4%B7">夏威夷</a></li>
									<!--li><a href="/eWeb_summittour/auto/Central_tours_US.asp#tab4">西雅圖</a></li-->
									<li><a href="/eWeb_summittour/auto/Central_tours_US.asp#tab5">阿拉斯加</a></li>
									<!--li><a href="/eWeb_summittour/auto/Central_tours_US.asp#tab6">芝加哥</a></li-->
									<!--li><a href="/eWeb_summittour/auto/Central_tours_US.asp#tab7">溫哥華</a></li-->
									<li><a href="/eWeb_summittour/auto/Central_tours_US.asp#tab8">紐約</a></li>
									<!--li><a href="/eWeb_summittour/auto/Central_tours_US.asp#tab9">多倫多</a></li-->
									<!--li><a href="/eWeb_summittour/auto/Central_tours_US.asp#tab10">休士頓</a></li-->
									<li><a href="/EW/GO/GroupList.asp?regmCd=0017&regsCd=0017-0010&beginDt=2017/08/15&endDt=2099/12/30&allowJoin=1">加拿大(落磯山)</a></li>
									<li><a href="/eWeb_summittour/auto/Central_tours_US.asp#tab12">古巴</a></li>
									<!--li><a href="/eWeb_summittour/auto/Central_tours_US.asp#tab13">北美洲</a></li-->
									<!--li><a href="/eWeb_summittour/auto/Central_tours_US.asp#tab14">中美洲</a></li-->
									<!--li><a href="/eWeb_summittour/auto/Central_tours_US.asp#tab15">南美洲</a></li-->
									<li><a href="/eWeb_summittour/auto/Central_tours_US.asp#tab16">洛杉磯/舊金山</a></li>
									<!--li><a href="/eWeb_summittour/auto/Central_tours_US.asp#tab17">美洲自由行</a></li-->
									</ul>
							</li>
							<li class="dropdown">
								<a href="/eWeb_summittour/auto/Central_tours_Islands.asp" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true">海島</a>
								<ul class="dropdown-menu" role="menu">	
								  <li><a target="_blank" href="http://www.designtravel.com.tw/travel-hot.aspx?seq=5&b_seq=5938">馬爾地夫</a></li>
								  <li><a target="_blank" href="http://www.designtravel.com.tw/travel-hot.aspx?seq=3&b_seq=5938">帛琉</a></li>  
								  <li><a target="_blank" href="http://www.designtravel.com.tw/travel-hot.aspx?seq=2&b_seq=5938">關島</a></li>
								  <li><a target="_blank" href="http://www.designtravel.com.tw/travel-hot.aspx?seq=7&b_seq=5938">巴里島</a></li>
								  <li><a target="_blank" href="http://www.designtravel.com.tw/travel-hot.aspx?seq=6&b_seq=5938">蘇美島</a></li>
								  <!--li><a href="/eWeb_summittour/auto/Central_tours_Islands.asp#tab4">海島自由行</a></li-->
								</ul> 
							</li>							
							<li class="dropdown">
							<a href="/eWeb_summittour/auto/Central_tours_Indochina.asp" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true">中南半島</a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="/eWeb_summittour/auto/Central_tours_Indochina.asp#tab1">越南(河內、峴港、胡志明、富國島)</a></li>
									<li><a href="/eWeb_summittour/auto/Central_tours_Indochina.asp#tab2">柬埔寨(吳哥窟、金邊、西哈努克)</a></li>
									<li><a href="/eWeb_summittour/auto/Central_tours_Indochina.asp#tab3">寮國</a></li>
									<li><a href="/eWeb_summittour/auto/Central_tours_Indochina.asp#tab4">緬甸(仰光)</a></li>
									<li><a href="/EW/GO/GroupList.asp?regmCd=0010&regsCd=0010-0005&beginDt=2017/08/15&endDt=2099/12/31&allowJoin=1">雙國(越南+高棉)</a></li>
									<!--li><a href="/eWeb_summittour/auto/Central_tours_Indochina.asp#tab6">雙國(北越+廣西)</a></li-->
									<!--li><a href="/eWeb_summittour/auto/Central_tours_Indochina.asp#tab7">中南半島自由行</a></li-->
								</ul>
							</li>
							<!--
							<li class="dropdown">
								<a href="/eWeb_summittour/auto/Central_tours_TW.asp" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true">國內離島</a>
								<ul class="dropdown-menu" role="menu">	
								  <li><a href="/eWeb_summittour/auto/Central_tours_TW.asp#tab1">金門</a></li>
								  <li><a href="/eWeb_summittour/auto/Central_tours_TW.asp#tab2">澎湖</a></li>
								  <li><a href="/eWeb_summittour/auto/Central_tours_TW.asp#tab3">馬祖</a></li>
								</ul>
							</li>	
								-->
							<li>
								<a href="/eWeb_summittour/auto/Central_tours_Cruise.asp" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true">遊輪</a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0019&regsCd=0019-0025&beginDt=2018/09/13&endDt=2099/12/31&allowJoin=1">2019《盛世公主號》基隆港出發</a></li>
									<li><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0019&regsCd=0019-0026&beginDt=2018/09/13&endDt=2099/12/31&allowJoin=1">2019《太陽公主號》基隆港出發</a></li>
									<li><a href=" ">2019《歌詩達郵輪》基隆港出發</a></li>
									<li><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0019&regsCd=0019-0018&beginDt=2018/09/13&endDt=2099/12/31&allowJoin=1">《日本四大名城》基隆港出發</a></li>
									<li><a href=" ">阿拉斯加─冰河灣</a></li>
									<li><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0019&regsCd=0019-0013&beginDt=2018/09/13&endDt=2099/12/31&allowJoin=1">加勒比海─墨西哥─新英格蘭</a></li>
									<li><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0019&regsCd=0019-0023&beginDt=2018/09/13&endDt=2099/12/31&allowJoin=1">《公主郵輪》東北亞─俄羅斯</a></li>
									<li><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0019&regsCd=0019-0015&beginDt=2018/09/13&endDt=2099/12/31&allowJoin=1">《公主郵輪》地中海─希臘</a></li>
									<li><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0019&regsCd=0019-0028&beginDt=2018/09/13&endDt=2099/12/31&allowJoin=1">《歌詩達郵輪》日本─韓國</a></li>
									<li><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0019&regsCd=0019-0029&beginDt=2018/09/13&endDt=2099/12/31&allowJoin=1">《歌詩達郵輪》地中海─愛琴海</a></li>
									<li><a href=" ">《盛世公主號》紐西蘭─澳洲─大洋洲</a></li>
								</ul>
							</li>
							
							<li>
								<a href="/eWeb_summittour/auto/Central_tours_RMQ.asp" ><!--class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true"-->台中出發</a>
								<!--ul class="dropdown-menu" role="menu">
									<li><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0016&portCd=RMQ&allowJoin=1">港澳大陸</a></li>
									<!--li><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0003&portCd=RMQ&allowJoin=1">東南亞</a></li>
									<li><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0002&portCd=RMQ&allowJoin=1">東北亞</a></li>
									<!--li><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0011&portCd=RMQ&allowJoin=1">南亞、中亞非</a></li-->
									<!--li><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0012&portCd=RMQ&allowJoin=1">歐洲</a></li-->
									<!--li><a href="/eWeb_summittour/auto/Central_tours_RMQ.asp#tab6">紐澳</a></li-->
									<!--li><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0017&portCd=RMQ&allowJoin=1">美加</a></li-->
									<!--li><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0018&portCd=RMQ&allowJoin=1">海島</a></li-->
									<!--li><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0010&portCd=RMQ&allowJoin=1">中南半島</a></li--> 
									<!--li><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0004&portCd=RMQ&allowJoin=1">國內離島</a></li-->
									<!--li><a href="/EW/GO/GroupList.asp?isWm=1&regmCd=0005&portCd=RMQ&allowJoin=1">郵輪</a></li-->
								<!--/ul-->
							</li>
							<li>
								<a href="http://cs.sabretn.com.tw/summittour/index2" ><!--class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true"-->機票最低價有位</a>
							</li>
							<li class="hidden-lg">
								<a href="https://www.facebook.com/summittourTaiwan" target="_blank"><p>粉絲專頁</p></a>
							</li>
							<li class="hidden-lg">
								<a href="https://line.me/R/ti/p/%40uaj2428d"><p>Line@</p></a>
							</li>
							<li class="hidden-lg">
								<a href="https://line.me/R/ti/p/%40uaj2428d"><p>線上客服</p></a>
							</li>
							<li class="hidden-lg">
								<a href="/eWeb_summittour/page/member.asp"><p>會員專區</p></a>
							</li>
							<li class="hidden-lg">
								<a href="javascript:;"><p>訂閱電子報</p></a>
							</li>
							<li class="hidden-lg">
								<a href="/eweb_summittour/diy/V_diy.asp"><p>客製化旅遊</p></a>
							</li>
							<li class="hidden-lg">
								<a href="/eWeb_summittour/index.asp"><p>回首頁</p></a>
							</li>

													
							
						</ul>
					</nav>
				</div>
				
				<!--button data-toggle="collapse-side1" data-target-sidebar1=".side-collapse-right" data-target-content1=".side-collapse-container-right" type="button" class="navbar-toggle1 pull-right">
					<a class="nav-btn" id="nav-open-btn" href="#nav1"></a>
				</button-->
          </div>
        </div>
      </div>
</nav>
    
<!-- Modal -->
<div class="modal fade bs-example-modal-sm" id="myMember" tabindex="-1" role="dialog" aria-labelledby="myMemberLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myMemberLabel">會員中心</h4>
      </div>
      <div class="modal-body">
        <!--#include virtual="/eWeb_summittour/public/login.asp"-->
      </div>
    </div>
  </div>
</div>


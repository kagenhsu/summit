﻿<!DOCTYPE html><!-- lang="zh-TW"-->
<html class="no-js">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/> 
<meta charset="utf-8">
<title>內頁排版設計</title>
<link rel="shortcut icon" type="image/x-icon" href="/eweb_summittour/images/default/favicon.ico">
<link rel="bookmark" type="image/x-icon" href="/eweb_summittour/images/default/favicon.ico">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="Robots" content="all" />
<meta name="Googlebot" content="index,follow" />
<meta name="Audience" content="All" />
<meta name="Page-topic" content="Internet" />
<meta name="Rating" content="General" />
<meta name="Resource-Type" content="document" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="windows-Target" content="_top" />
<meta name="Author" content="GTSHAYO" />
<meta name="Copyright" content="Copyright c 2017 www.summittour.com.tw All Rights Reserved." />
<meta name="Generator" content="Visual Studio Code" />
<meta name="Publisher" content="超悠旅行社" />
<meta name="Creation-Date" content="06-23-2017" />
<meta name="WEBCRAWLERS" content="ALL" />
<meta name="SPIDERS" content="ALL" />
<meta name="revisit-after" content="7 days"/>

<link type="text/css" rel="stylesheet" href="/eweb_summittour/css/reset_new.css">
<link type="text/css" rel="stylesheet" href="/eweb_summittour/css/normalize.css">
<link type="text/css" rel="stylesheet" href="/eweb_summittour/css/bootstrap.css">
<link type="text/css" rel="stylesheet" href="/eweb_summittour/css/font-awesome.min.css">
<!--<link rel="stylesheet" type="text/css" href="/eweb_summittour/css/loaders.css"/>-->
<link rel="stylesheet" type="text/css" href="/eweb_summittour/css/main.css">
<link type="text/css" rel="stylesheet" href="/eweb_summittour/css/defaultUP2.css">
<link type="text/css" rel="stylesheet" href="/eweb_summittour/css/EditorTemplate.css">

<script type="text/javascript" language="javascript" src="/eweb_summittour/js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" language="javascript" src="/eweb_summittour/js/bootstrap.min.js"></script>
<!--<script type="text/javascript" language="javascript" src="/eweb_summittour/js/jquery.nicescroll.min.js"></script>-->
<script type="text/javascript" language="javascript" src="/eweb_summittour/js/modernizr.js"></script>
<script type="text/javascript" language="javascript" src="/eweb_summittour/js/all.js"></script>
<script type="text/javascript" language="javascript" src="/eweb_summittour/js/hammer.min.js"></script>

<link href="/eweb_summittour/auto/autopage.css" rel="stylesheet" type="text/css">

<script src="/eWeb_summittour/ckeditor/ckeditor.js"></script>

</head>

<body>

</style>

<!-- Loader動畫 -->
<!--<div id="loader-wrapper">
    <div id="loader"></div>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
</div>-->
<!-- Loader動畫 --> 
<div class="wrapper">
	 
	<!--上標位置-->
	<article class="header"> 
		<!--#include virtual="/eweb_summittour/public/headerUP2.asp"--> 
	</article>
    <article class="summittour_carousel">
        <div class="row" style="height:85px;">
           
        </div>
    </article>
	<div class="container">
        <div class="row">
			<!--麵包屑及頁面標題-->
            <ol class="breadcrumb" id="auto_breadcrumb">
                <li>目前位置：</li>
                <li class="active"><a href="/eWeb_summittour/home.asp">首頁</a></li>
                <li class="active">內頁排版設計</li>
            </ol>
		</div>
		<ul id="theme_tabs" class="nav nav-tabs" role="tablist">
			<li><a style="font-size:18px;" href="#tab1">上架編輯測試</a></li>
			<li><a style="font-size:18px;" href="#tab2">優惠網頁連結(設計版)</a></li>
			<li><a style="font-size:18px;" href="#tab3">開團型模組應用</a></li>
			<li><a style="font-size:18px;" href="#tab4">動畫特效排版</a></li>
		</ul>
	
		<div class="exercise_min container">
		
			<div class="Title_1">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 item0">
					<h2 id="tab1" style="font-size:36px;text-align:center;margin-top: 30px;">上架編輯測試</h2>
					<hr style="margin-top: 5px;margin-bottom: 3px;border-top: 2px solid #83b53c;width: 100%;"/>
				</div>
			</div>
			<div class="day_style_12">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<table align="center" class="blueTable" style="width:100%">
			<thead>
			<tr>
			<th scope="col" style="text-align:center;"><strong>活動主題名稱</strong></th>
			<th scope="col" style="text-align:center;"><strong>網頁檔名</strong></th>
			<th scope="col" style="text-align:center;"><strong>活動主題名稱</strong></th>
			<th scope="col" style="text-align:center;"><strong>網頁檔名</strong></th>
			</tr>
			</thead>
			<tbody>
			<tr>
			<td style="text-align:center; vertical-align:middle"><a href="editor.asp" target="_blank">科威中台上架說明</a></td>
			<td style="text-align:center; vertical-align:middle">editor.asp</td>
			<td style="text-align:center; vertical-align:middle"><a href="editor3.asp" target="_blank">科威前台網頁管理</a></td>
			<td style="text-align:center; vertical-align:middle">editor3.asp</td>
			</tr>
			<tr>
			<td style="text-align:center; vertical-align:middle"><a href="Central_tours.asp" target="_blank">活動頁測試版</a></td>
			<td style="text-align:center; vertical-align:middle">Central_tours.asp</td>
			<td style="text-align:center; vertical-align:middle"><a href="MGroupDetail.asp" target="_blank">團型內容頁測試版</a></td>
			<td style="text-align:center; vertical-align:middle">MGroupDetail.asp</td>
			</tr>
			<tr>
			<td style="text-align:center; vertical-align:middle"><a href="GroupDetail.asp" target="_blank">個型內容頁測試版</a></td>
			<td style="text-align:center; vertical-align:middle">GroupDetail.asp</td>
			<td style="text-align:center; vertical-align:middle"><a href="GroupList.asp" target="_blank">收尋內容頁測試版</a></td>
			<td style="text-align:center; vertical-align:middle">GroupList.asp</td>
			</tr>
			</tbody>
			</table>
			</div>
			</div>
		
			<div class="Title_1">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 item0">
					<h2 id="tab2" style="font-size:36px;text-align:center;margin-top: 30px;">優惠網頁連結(設計版)</h2>
					<hr style="margin-top: 5px;margin-bottom: 3px;border-top: 2px solid #83b53c;width: 100%;"/>
				</div>
			</div>
			<div class="day_style_12">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<table align="center" class="blueTable" style="width:100%">
						<thead>
						<tr>
						<th scope="col" style="text-align:center;"><strong>活動主題名稱</strong></th>
						<th scope="col" style="text-align:center;"><strong>網頁檔名</strong></th>
						<th scope="col" style="text-align:center;"><strong>活動主題名稱</strong></th>
						<th scope="col" style="text-align:center;"><strong>網頁檔名</strong></th>
						</tr>
						</thead>
						<tbody>
						<tr>
						<td style="text-align:center; vertical-align:middle"><a href="/eweb_summittour/auto/Central_tours_UP02.asp" target="_blank">過年限時優惠</a></td>
						<td style="text-align:center; vertical-align:middle">Central_tours_UP02.asp</td>
						<td style="text-align:center; vertical-align:middle"><a href="/eweb_summittour/auto/Central_tours_UP03.asp" target="_blank">賞花季節</a></td>
						<td style="text-align:center; vertical-align:middle">Central_tours_UP03.asp</td>
						</tr>
						<tr>
						<td style="text-align:center; vertical-align:middle"><a href="/eweb_summittour/auto/Central_tours_UP04.asp" target="_blank">端午優惠期間</a></td>
						<td style="text-align:center; vertical-align:middle">Central_tours_UP04.asp</td>
						<td style="text-align:center; vertical-align:middle"><a href="/eweb_summittour/auto/Central_tours_UP05.asp" target="_blank">228假期</a></td>
						<td style="text-align:center; vertical-align:middle">Central_tours_UP05.asp</td>
						</tr>
						<tr>
						<td style="text-align:center; vertical-align:middle"><a href="/eweb_summittour/auto/Central_tours_UP06.asp" target="_blank">中秋假期</a></td>
						<td style="text-align:center; vertical-align:middle">Central_tours_UP06.asp</td>
						<td style="text-align:center; vertical-align:middle"><a href="/eweb_summittour/auto/Central_tours_UP07.asp" target="_blank">直航優惠</a></td>
						<td style="text-align:center; vertical-align:middle">Central_tours_UP07.asp</td>
						</tr>
						<tr>
						  <td style="text-align:center; vertical-align:middle"><a href="/eweb_summittour/auto/Central_tours_UP08.asp" target="_blank">婦幼清明</a></td>
						  <td style="text-align:center; vertical-align:middle">Central_tours_UP08.asp</td>
						  <td style="text-align:center; vertical-align:middle"><a href="/eweb_summittour/auto/Central_tours_UP09.asp" target="_blank">清艙促銷</a></td>
						  <td style="text-align:center; vertical-align:middle">Central_tours_UP09.asp</td>
						</tr>
						<tr>
						  <td style="text-align:center; vertical-align:middle"><a href="/eweb_summittour/auto/Central_tours_UP10.asp" target="_blank">聖誕&amp;跨年</a></td>
						  <td style="text-align:center; vertical-align:middle">Central_tours_UP10.asp</td>
						  <td style="text-align:center; vertical-align:middle"><a href="/eweb_summittour/auto/Central_tours_OTS.asp" target="_blank">線上旅展</a></td>
						  <td style="text-align:center; vertical-align:middle">Central_tours_OTS.asp</td>
						</tr>
						<tr>
						  <td style="text-align:center; vertical-align:middle"><a href="/eweb_summittour/auto/Central_tours_UPR01.asp" target="_blank">季節限定優惠</a></td>
						  <td style="text-align:center; vertical-align:middle">Central_tours_UPR01.asp</td>
						  <td style="text-align:center; vertical-align:middle"><a href="/eweb_summittour/auto/Central_tours_UP11.asp" target="_blank">達人推薦</a></td>
						  <td style="text-align:center; vertical-align:middle">Central_tours_UP11.asp</td>
						</tr>
						<tr>
						  <td style="text-align:center; vertical-align:middle"><a href="/eweb_summittour/auto/Central_tours_UP12.asp" target="_blank">軍人優惠</a></td>
						  <td style="text-align:center; vertical-align:middle">Central_tours_UP12.asp</td>
						  <td style="text-align:center; vertical-align:middle"><a href="/eweb_summittour/auto/Central_tours_UP13.asp" target="_blank">賞楓之旅</a></td>
						  <td style="text-align:center; vertical-align:middle">Central_tours_UP13.asp</td>
						</tr>
						<tr>
						  <td style="text-align:center; vertical-align:middle"><a href="/eweb_summittour/auto/Central_tours_UP14.asp" target="_blank">機+酒優惠</a></td>
						  <td style="text-align:center; vertical-align:middle">Central_tours_UP14.asp</td>
						  <td style="text-align:center; vertical-align:middle"><a href="/eweb_summittour/auto/Central_tours_UP15.asp" target="_blank">週年慶特惠</a></td>
						  <td style="text-align:center; vertical-align:middle">Central_tours_UP15.asp</td>
						</tr>
						<tr>
						  <td style="text-align:center; vertical-align:middle"><a href="/eweb_summittour/auto/Central_tours_UP16.asp" target="_blank">暑假優惠</a></td>
						  <td style="text-align:center; vertical-align:middle">Central_tours_UP16.asp</td>
						  <td style="text-align:center; vertical-align:middle"><a href="/eweb_summittour/auto/Central_tours_UP17.asp" target="_blank">寒假優惠</a></td>
						  <td style="text-align:center; vertical-align:middle">Central_tours_UP17.asp</td>
						</tr>
						<tr>
						  <td style="text-align:center; vertical-align:middle"><a href="/eweb_summittour/auto/Central_tours_UP18.asp" target="_blank">雙十特惠</a></td>
						  <td style="text-align:center; vertical-align:middle">Central_tours_UP18.asp</td>
						  <td style="text-align:center; vertical-align:middle"><a href="/eweb_summittour/auto/Central_tours_UP19.asp" target="_blank">畢業特惠</a></td>
						  <td style="text-align:center; vertical-align:middle">Central_tours_UP19.asp</td>
						</tr>
						<tr>
						  <td style="text-align:center; vertical-align:middle"><a href="/eweb_summittour/auto/Central_tours_RMQ.asp" target="_blank">台中出發</a></td>
						  <td style="text-align:center; vertical-align:middle">Central_tours_RMQ.asp</td>
						  <td style="text-align:center; vertical-align:middle"><a href="/EW/GO/GroupList.asp?isWm=1&amp;beginDt=2017/10/23&amp;endDt=2018/12/31&amp;allowJoin=1&amp;promote=1" target="_blank">每週限時優惠行程</a></td>
						  <td style="text-align:center; vertical-align:middle">GroupList.asp 科威系統自動產生網址</td>
						</tr>
						</tbody>
					</table>
				</div>
				</div>
			<div class="Title_1">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 item0">
					<h2 id="tab3" style="font-size:36px;text-align:center;margin-top: 30px;">開團型模組應用</h2>
					<hr style="margin-top: 5px;margin-bottom: 3px;border-top: 2px solid #83b53c;width: 100%;"/>
					<p style="font-size:28px;text-align:center;color: #FF0000;">以下是放在開團引用HTML語法</p>
				</div>
			</div> 
			<div class="Title_2">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 item0">
					<h2 style="font-size:26px;text-align:center;margin-top: 30px;">靜態排版</h2>
					<hr style="margin-top: 2px;margin-bottom: 3px;border-top: 2px solid #3333;width: 100%;" />
					<p style="font-size:18px;text-align:center;color: #FF0000;margin-bottom: 10px;">修改網頁樣式位置/eweb_summittour/EditorTemplate</p>
					<div class="day_style_12">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<table align="center" class="blueTable" style="width:100%">
							<thead>
								<tr>
									<th scope="col" style="text-align:center;"><strong>編號</strong></th>
									<th scope="col" style="text-align:center;"><strong>範本</strong></th>
									<th scope="col" style="text-align:center;"><strong>名稱</strong></th>
									<th scope="col" style="text-align:center;"><strong>檔名</strong></th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td style="text-align:center; vertical-align:middle">01</td>
									<td style="text-align:center; vertical-align:middle"><a href="001.asp"><img src="http://www.summittour.com.tw:8000/eWeb_summittour/EditorTemplate/Title_1_1_0.gif" width="114" height="58" /></a></td>
									<td style="text-align:center; vertical-align:middle"><a href="001.asp">銷售特色</a></td>
									<td style="text-align:center; vertical-align:middle">Title_1_1_0.txt</td>
								</tr>
								<tr>
									<td style="text-align:center; vertical-align:middle">02</td>
									<td style="text-align:center; vertical-align:middle"><a href="002.asp"><img src="http://www.summittour.com.tw:8000/eWeb_summittour/EditorTemplate/Title_1_2_0.gif" width="114" height="58" /></a></td>
									<td style="text-align:center; vertical-align:middle"><a href="002.asp">景點介紹</a></td>
									<td style="text-align:center; vertical-align:middle">Title_1_2_0.txt</td>
								</tr>
								<tr>
									<td style="text-align:center; vertical-align:middle">03</td>
									<td style="text-align:center; vertical-align:middle"><a href="003.asp"><img src="http://www.summittour.com.tw:8000/eWeb_summittour/EditorTemplate/Title_1_3_0.gif" width="114" height="58" /></a></td>
									<td style="text-align:center; vertical-align:middle"><a href="003.asp">特別安排</a></td>
									<td style="text-align:center; vertical-align:middle">Title_1_3_0.txt</td>
								</tr>
								<tr>
									<td style="text-align:center; vertical-align:middle">04</td>
									<td style="text-align:center; vertical-align:middle"><a href="004.asp"><img src="http://www.summittour.com.tw:8000/eWeb_summittour/EditorTemplate/Title_1_4_0.gif" width="114" height="58" /></a></td>
									<td style="text-align:center; vertical-align:middle"><a href="004.asp">團票說明</a></td>
									<td style="text-align:center; vertical-align:middle">Title_1_4_0.txt</td>
								</tr>
								<tr>
									<td style="text-align:center; vertical-align:middle">05</td>
									<td style="text-align:center; vertical-align:middle"><a href="005.asp"><img src="http://www.summittour.com.tw:8000/eWeb_summittour/EditorTemplate/Title_1_5_0.gif" width="114" height="58" /></a></td>
									<td style="text-align:center; vertical-align:middle"><a href="005.asp">出團備註</a></td>
									<td style="text-align:center; vertical-align:middle">Title_1_5_0.txt</td>
								</tr>
								<tr>
									<td style="text-align:center; vertical-align:middle">06</td>
									<td style="text-align:center; vertical-align:middle"><a href="006.asp"><img src="http://www.summittour.com.tw:8000/eWeb_summittour/EditorTemplate/Title_1_6_0.gif" width="114" height="58" /></a></td>
									<td style="text-align:center; vertical-align:middle"><a href="006.asp">行程內容</a></td>
									<td style="text-align:center; vertical-align:middle">Title_1_6_0.txt</td>
								</tr>
								<tr>
									<td style="text-align:center; vertical-align:middle">07</td>
									<td style="text-align:center; vertical-align:middle"><a href="007.asp"><img src="http://www.summittour.com.tw:8000/eWeb_summittour/EditorTemplate/Note_1.gif" width="114" height="58" /></a></td>
									<td style="text-align:center; vertical-align:middle"><a href="007.asp">	貼心提醒等備註文字敘述</a></td>
									<td style="text-align:center; vertical-align:middle">Note_1.txt</td>
								</tr>
								<tr>
									<td style="text-align:center; vertical-align:middle">08</td>
									<td style="text-align:center; vertical-align:middle"><a href="008.asp"><img src="http://www.summittour.com.tw:8000/eWeb_summittour/EditorTemplate/day_style_7.gif" width="114" height="58" /></a></td>
									<td style="text-align:center; vertical-align:middle"><a href="008.asp">文字敘述</a></td>
									<td style="text-align:center; vertical-align:middle">day_style_7.txt</td>
								</tr>
								<tr>
									<td style="text-align:center; vertical-align:middle">09</td>
									<td style="text-align:center; vertical-align:middle"><a href="009.asp"><img src="http://www.summittour.com.tw:8000/eWeb_summittour/EditorTemplate/day_style_1.gif" width="114" height="58" /></a></td>
									<td style="text-align:center; vertical-align:middle"><a href="009.asp">單張圖片景點介紹</a></td>
									<td style="text-align:center; vertical-align:middle">day_style_1.txt</td>
								</tr>
								<tr>
									<td style="text-align:center; vertical-align:middle">10</td>
									<td style="text-align:center; vertical-align:middle"><a href="010.asp"><img src="http://www.summittour.com.tw:8000/eWeb_summittour/EditorTemplate/day_style_2.gif" width="114" height="58" /></a></td>
									<td style="text-align:center; vertical-align:middle"><a href="010.asp">兩張圖片景點介紹</a></td>
									<td style="text-align:center; vertical-align:middle">day_style_2.txt</td>
								</tr>
								<tr>
									<td style="text-align:center; vertical-align:middle">11</td>
									<td style="text-align:center; vertical-align:middle"><a href="011.asp"><img src="	http://www.summittour.com.tw:8000/eWeb_summittour/EditorTemplate/day_style_3.gif" width="114" height="58" /></a></td>
									<td style="text-align:center; vertical-align:middle"><a href="011.asp">三張圖片景點介紹</a></td>
									<td style="text-align:center; vertical-align:middle">day_style_3.txt</td>
								</tr>
								<tr>
									<td style="text-align:center; vertical-align:middle">12</td>
									<td style="text-align:center; vertical-align:middle"><a href="012.asp"><img src="http://www.summittour.com.tw:8000/eWeb_summittour/EditorTemplate/day_style_4.gif" width="114" height="58" /></a></td>
									<td style="text-align:center; vertical-align:middle"><a href="012.asp">飯店介紹</a></td>
									<td style="text-align:center; vertical-align:middle">day_style_4.txt</td>
								</tr>
								<tr>
									<td style="text-align:center; vertical-align:middle">13</td>
									<td style="text-align:center; vertical-align:middle"><a href="013.asp"><img src="http://www.summittour.com.tw:8000/eWeb_summittour/EditorTemplate/day_style_5.gif" width="114" height="58" /></a></td>
									<td style="text-align:center; vertical-align:middle"><a href="013.asp">橫幅圖片</a></td>
									<td style="text-align:center; vertical-align:middle">day_style_5.txt</td>
								</tr>
								<tr>
									<td style="text-align:center; vertical-align:middle">14</td>
									<td style="text-align:center; vertical-align:middle"><a href="014.asp"><img src="http://www.summittour.com.tw:8000/eWeb_summittour/EditorTemplate/day_style_6.gif" width="114" height="58" /></a></td>
									<td style="text-align:center; vertical-align:middle"><a href="014.asp">橫幅圖片文字敘述	</a></td>
									<td style="text-align:center; vertical-align:middle">day_style_6.txt</td>
								</tr>
								<tr>
									<td style="text-align:center; vertical-align:middle">15</td>
									<td style="text-align:center; vertical-align:middle"><a href="015.asp"><img src="http://www.summittour.com.tw:8000/eWeb_summittour/EditorTemplate/day_style_12.gif" width="114" height="58" /></a></td>
									<td style="text-align:center; vertical-align:middle"><a href="015.asp">表格</a></td>
									<td style="text-align:center; vertical-align:middle">day_style_12.txt</td>
								</tr>
								<tr>
									<td style="text-align:center; vertical-align:middle">16</td>
									<td style="text-align:center; vertical-align:middle"><a href="YT_js.asp"><img src="http://www.summittour.com.tw:8000/eWeb_summittour/EditorTemplate/day_style_13.gif" width="114" height="58" /></a></td>
									<td style="text-align:center; vertical-align:middle"><a href="YT_js.asp">崁入YouTube影片</a></td>
									<td style="text-align:center; vertical-align:middle">day_style_13.txt</td>
								</tr>
							</tbody>
						</table>
						</div>
						</div> 
				</div>
			</div>
			
			<div class="Title_2">
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 item0">
					<h2 id="tab4" style="font-size:26px;text-align:center;margin-top: 30px;">動畫特效排版</h2>
					<hr style="margin-top: 2px;margin-bottom: 3px;border-top: 2px solid #3333;width: 100%;" />
					<p style="font-size:18px;text-align:center;color: #FF0000;">後續會在開發動畫</p>
											<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<table align="center" class="blueTable" style="width:100%">
							<thead>
								<tr>
									<th scope="col" style="text-align:center;"><strong>編號</strong></th>
									<th scope="col" style="text-align:center;"><strong>範本</strong></th>
									<th scope="col" style="text-align:center;"><strong>名稱</strong></th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td style="text-align:center; vertical-align:middle">01</td>
									<td style="text-align:center; vertical-align:middle"><a href="001_js.asp"><img src="http://www.summittour.com.tw:8000/eWeb_summittour/EditorTemplate/day_style_1.gif" width="114" height="58" /></a></td>
									<td style="text-align:center; vertical-align:middle"><a href="001_js.asp">一張圖片淡入淡效果景點介紹</a></td>									
								</tr>
								<tr>
									<td style="text-align:center; vertical-align:middle">02</td>
									<td style="text-align:center; vertical-align:middle"><a href="002_js.asp"><img src="http://www.summittour.com.tw:8000/eWeb_summittour/EditorTemplate/day_style_2.gif" width="114" height="58" /></a></td>
									<td style="text-align:center; vertical-align:middle"><a href="002_js.asp">兩張圖片滑鼠滑過出現景點介紹</a></td>									
								</tr>
								<tr>
									<td style="text-align:center; vertical-align:middle">03</td>
									<td style="text-align:center; vertical-align:middle"><a href="003_js.asp"><img src="http://www.summittour.com.tw:8000/eWeb_summittour/EditorTemplate/day_style_3.gif" width="114" height="58" /></a></td>
									<td style="text-align:center; vertical-align:middle"><a href="003_js.asp">三張圖片滑鼠滑過效果景點介紹</a></td>									
								</tr>
								<tr>
									<td style="text-align:center; vertical-align:middle">04</td>
									<td style="text-align:center; vertical-align:middle"><a href="004_js.asp"><img src="http://www.summittour.com.tw:8000/eWeb_summittour/EditorTemplate/day_style_14.gif" width="114" height="58" /></a></td>
									<td style="text-align:center; vertical-align:middle"><a href="004_js.asp">飯店介紹圖片淡入淡出效果</a></td>
								</tr>
								<tr>
									<td style="text-align:center; vertical-align:middle">05</td>
									<td style="text-align:center; vertical-align:middle"><a href="009_js.asp"><img src="http://www.summittour.com.tw:8000/eWeb_summittour/EditorTemplate/day_style_5.gif" width="114" height="58" /></a></td>
									<td style="text-align:center; vertical-align:middle"><a href="009_js.asp">單張圖片淡入淡出效果</a></td>
								</tr>
								
							</tbody>
						</table>
						</div>
						</div> 
				</div>
			</div>
			

		</div>
				
	</div> 
	</div>
	
	
    
    
    
	
    
    <article class="footer">
    
    </article>
</div>	
<div class="actGotop" style="display: block;"><a href="javascript:;" title="返回頂部"><i class="fa fa-angle-up" aria-hidden="true"></i>TOP</a></div>
<script src="/eweb_summittour/js/share_main.js"></script>
<script type="text/javascript">$(document).ready( function() {getXML(true,'');});</script>

<script type="text/javascript">
//回頂點
$(function(){   
    $(window).scroll(function() {       
        if($(window).scrollTop() >= 100){
            $('.actGotop').fadeIn(300); 
        }else{    
            $('.actGotop').fadeOut(300);    
        }  
    });
    $('.actGotop').click(function(){
    $('html,body').animate({scrollTop: '0px'}, 800);}); 
    $('#go_search').click(function(){   $('html,body').animate({scrollTop:$('#search').offset().top}, 900); return false;   })
});

$(function(){   
    var cubuk_seviye = $(document).scrollTop();
    var header_yuksekligi = $('.header').outerHeight();

    $(window).scroll(function() {
        var kaydirma_cubugu = $(document).scrollTop();

        if (kaydirma_cubugu > header_yuksekligi){$('.header').addClass('gizle');} 
        else {$('.header').removeClass('gizle');}

        if (kaydirma_cubugu > cubuk_seviye){$('.header').removeClass('sabit');} 
        else {$('.header').addClass('sabit');}              

        cubuk_seviye = $(document).scrollTop(); 
     });
     
});


$(function(){
    var sideslider = $('[data-toggle=collapse-side]');
    var get_sidebar = sideslider.attr('data-target-sidebar');
    var get_content = sideslider.attr('data-target-content');
    sideslider.click(function(event){
      $(get_sidebar).toggleClass('in');
      $(get_content).toggleClass('out');
   });

});
$(function(){
    var sideslider = $('[data-toggle=collapse-side1]');
    var get_sidebar = sideslider.attr('data-target-sidebar1');
    var get_content = sideslider.attr('data-target-content1');
    sideslider.click(function(event){
      $(get_sidebar).toggleClass('in');
      $(get_content).toggleClass('out');
   });

});
</script>

</body>
</html>

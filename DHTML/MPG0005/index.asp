﻿<%
'//===========================================================================================
'//						index.asp
'//-------------------------------------------------------------------------------------------
'//		description		: 此頁面係由自動產生因此不用納入版本
'//-------------------------------------------------------------------------------------------
Response.CacheControl = "no-cache" 
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="robots" content="noindex">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
	<title>國外團體旅遊：海島旅遊行程 - 超悠</title>
	<!--產出時間:2017/8/7 下午 02:02:24 / WEB:summittour-->
	<link href='/eweb_summittour/css/reset.css' rel='stylesheet' type='text/css'>
	<link href='/eweb_summittour/css/autopage.css' rel='stylesheet' type='text/css'>
	<script languege="javascript" src="/script/jquery.js"></script>
	<script languege="javascript" src="/eweb_summittour/JS/autopage.js"></script>
</head>
<body>
	<div class="metropolis">
		<div class="title_box" >
			<div class="left_box" id="SAREA00250">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>帛琉</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/eWeb/GO/L_GO_Type.asp?iSUB_CD=GO&STYLE_TP=2&TWFG=N&iMGRUP_CD=RORT0401" target="_top"><img src="/eWeb_summittour/IMGDB/000578/000733/000960/00008083.JPG" /></a></div>
				<div class="product_name"><a href="/eWeb/GO/L_GO_Type.asp?iSUB_CD=GO&STYLE_TP=2&TWFG=N&iMGRUP_CD=RORT0401" target="_top">帛琉水世界星光夜釣4日-豪華版</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=RORT0401&GRUP_CD=RORT4D7906&SUB_CD=GO&JOIN_TP=' target='_top'>9/6</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=RORT0401&GRUP_CD=RORT4D7913&SUB_CD=GO&JOIN_TP=' target='_top'>9/13</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=RORT0401&GRUP_CD=RORT4D7920&SUB_CD=GO&JOIN_TP=' target='_top'>9/20</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=RORT0401&GRUP_CD=RORT4D7927&SUB_CD=GO&JOIN_TP=' target='_top'>9/27</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=RORT0401&GRUP_CD=RORT4D71004&SUB_CD=GO&JOIN_TP=' target='_top'>10/4</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=RORT0401&GRUP_CD=RORT4D71011&SUB_CD=GO&JOIN_TP=' target='_top'>10/11</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=RORT0401&GRUP_CD=RORT4D71018&SUB_CD=GO&JOIN_TP=' target='_top'>10/18</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=RORT0401&GRUP_CD=RORT4D71025&SUB_CD=GO&JOIN_TP=' target='_top'>10/25</a>
					<a href="/eWeb/GO/L_GO_Type.asp?iSUB_CD=GO&STYLE_TP=2&TWFG=N&iMGRUP_CD=RORT0401" target="_top">...more</a></div>
				<div class="product_price"><span>$30,500起</span></div>
   				<div class="product_offer"><p><span style="color:#FFFFFF;"><span style="font-family:微軟正黑體,sans-serif;"><span style="font-size:12.0pt;"><span style="background-color:#FF8C00;">桃園出發</span></span></span></span></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/eWeb/GO/L_GO_Type.asp?iSUB_CD=GO&STYLE_TP=2&TWFG=N&iMGRUP_CD=RORT05A" target="_top"><img src="/eWeb_summittour/IMGDB/000578/000733/000960/00008078.JPG" /></a></div>
				<div class="product_name"><a href="/eWeb/GO/L_GO_Type.asp?iSUB_CD=GO&STYLE_TP=2&TWFG=N&iMGRUP_CD=RORT05A" target="_top">帛琉水世界星光夜釣5日-經典版</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=RORT05A&GRUP_CD=ROR5DT7902A&SUB_CD=GO&JOIN_TP=' target='_top'>9/2</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=RORT05A&GRUP_CD=ROR5DT7909A&SUB_CD=GO&JOIN_TP=' target='_top'>9/9</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=RORT05A&GRUP_CD=ROR5DT7916A&SUB_CD=GO&JOIN_TP=' target='_top'>9/16</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=RORT05A&GRUP_CD=ROR5DT7923A&SUB_CD=GO&JOIN_TP=' target='_top'>9/23</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=RORT05A&GRUP_CD=ROR5DT7930A&SUB_CD=GO&JOIN_TP=' target='_top'>9/30</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=RORT05A&GRUP_CD=ROR5DT71014A&SUB_CD=GO&JOIN_TP=' target='_top'>10/14</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=RORT05A&GRUP_CD=ROR5DT71021A&SUB_CD=GO&JOIN_TP=' target='_top'>10/21</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=RORT05A&GRUP_CD=ROR5DT71028A&SUB_CD=GO&JOIN_TP=' target='_top'>10/28</a>
					<a href="/eWeb/GO/L_GO_Type.asp?iSUB_CD=GO&STYLE_TP=2&TWFG=N&iMGRUP_CD=RORT05A" target="_top">...more</a></div>
				<div class="product_price"><span>$39,000起</span></div>
   				<div class="product_offer"><p><span style="color:#FFFFFF;"><span style="font-family:微軟正黑體,sans-serif;"><span style="font-size:12.0pt;"><span style="background-color:#FF8C00;">桃園出發</span></span></span></span></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/eWeb/GO/L_GO_Type.asp?iSUB_CD=GO&STYLE_TP=2&TWFG=N&iMGRUP_CD=RORT05B" target="_top"><img src="/eWeb_summittour/IMGDB/000578/000733/000960/00008081.JPG" /></a></div>
				<div class="product_name"><a href="/eWeb/GO/L_GO_Type.asp?iSUB_CD=GO&STYLE_TP=2&TWFG=N&iMGRUP_CD=RORT05B" target="_top">帛琉北島叢林探險5日-豪華版</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=RORT05B&GRUP_CD=ROR5DT71014B&SUB_CD=GO&JOIN_TP=' target='_top'>10/14</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=RORT05B&GRUP_CD=ROR5DT71021B&SUB_CD=GO&JOIN_TP=' target='_top'>10/21</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=RORT05B&GRUP_CD=ROR5DT71028B&SUB_CD=GO&JOIN_TP=' target='_top'>10/28</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=RORT05B&GRUP_CD=ROR5DT71104B&SUB_CD=GO&JOIN_TP=' target='_top'>11/4</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=RORT05B&GRUP_CD=ROR5DT71111B&SUB_CD=GO&JOIN_TP=' target='_top'>11/11</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=RORT05B&GRUP_CD=ROR5DT71118B&SUB_CD=GO&JOIN_TP=' target='_top'>11/18</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=RORT05B&GRUP_CD=ROR5DT71125B&SUB_CD=GO&JOIN_TP=' target='_top'>11/25</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=RORT05B&GRUP_CD=ROR5DT71202B&SUB_CD=GO&JOIN_TP=' target='_top'>12/2</a>
					<a href="/eWeb/GO/L_GO_Type.asp?iSUB_CD=GO&STYLE_TP=2&TWFG=N&iMGRUP_CD=RORT05B" target="_top">...more</a></div>
				<div class="product_price"><span>$40,000起</span></div>
   				<div class="product_offer"><p><span style="color:#FFFFFF;"><span style="font-family:微軟正黑體,sans-serif;"><span style="font-size:12.0pt;"><span style="background-color:#FF8C00;">桃園出發</span></span></span></span></p></div>
			</div>
		</div>
	</div>
</body>
</html>

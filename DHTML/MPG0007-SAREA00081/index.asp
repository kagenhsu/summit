﻿<%
'//===========================================================================================
'//						index.asp
'//-------------------------------------------------------------------------------------------
'//		description		: 此頁面係由自動產生因此不用納入版本
'//-------------------------------------------------------------------------------------------
Response.CacheControl = "no-cache" 
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="robots" content="noindex">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
	<title>國外團體旅遊：洛杉磯/舊金山旅遊行程 - 超悠</title>
	<!--產出時間:2018/12/4 下午 11:01:38 / WEB:summittour-->
	<link href='/eweb_summittour/css/reset.css' rel='stylesheet' type='text/css'>
	<link href='/eweb_summittour/css/autopage.css' rel='stylesheet' type='text/css'>
	<script languege="javascript" src="/script/jquery.js"></script>
	<script languege="javascript" src="/eweb_summittour/JS/autopage.js"></script>
</head>
<body>
	<div class="metropolis">
		<div class="title_box" >
			<div class="left_box" id="SAREA00081">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>洛杉磯/舊金山</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SFOLAXHXP10A" target="_top"><img src="/eWeb_summittour/IMGDB/000577/000764/000949/00013077.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SFOLAXHXP10A" target="_top">愛玩美西五城~17哩海岸.西峽谷.環球影城10天SFO進</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=SFOLA190101A' target='_top'>1/1</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOLA190107A' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOLA190212A' target='_top'>2/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOLA190218A' target='_top'>2/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOLA190225A' target='_top'>2/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOLA190311A' target='_top'>3/11</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOLA190325A' target='_top'>3/25</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=SFOLAXHXP10A" target="_top">...more</a></div>
				<div class="product_price"><span>$54,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SFOLAXHXP10B" target="_top"><img src="/eWeb_summittour/IMGDB/000577/000764/000949/00015177.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SFOLAXHXP10B" target="_top">享玩美西五城~大峽谷.17哩海岸.環球影城.空軍一號10天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=SFOLA190101B' target='_top'>1/1</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOLA190107B' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOLA190212B' target='_top'>2/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOLA190218B' target='_top'>2/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOLA190225B' target='_top'>2/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOLA190311B' target='_top'>3/11</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOLA190325B' target='_top'>3/25</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=SFOLAXHXP10B" target="_top">...more</a></div>
				<div class="product_price"><span>$51,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
		</div>
	</div>
</body>
</html>

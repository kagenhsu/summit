﻿<%
'//===========================================================================================
'//						index.asp
'//-------------------------------------------------------------------------------------------
'//		description		: 此頁面係由自動產生因此不用納入版本
'//-------------------------------------------------------------------------------------------
Response.CacheControl = "no-cache" 
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="robots" content="noindex">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
	<title>國外團體旅遊：澳門、珠海、深圳、廣州旅遊行程 - 超悠</title>
	<!--產出時間:2018/12/4 下午 11:01:21 / WEB:summittour-->
	<link href='/eweb_summittour/css/reset.css' rel='stylesheet' type='text/css'>
	<link href='/eweb_summittour/css/autopage.css' rel='stylesheet' type='text/css'>
	<script languege="javascript" src="/script/jquery.js"></script>
	<script languege="javascript" src="/eweb_summittour/JS/autopage.js"></script>
</head>
<body>
	<div class="metropolis">
		<div class="title_box" >
			<div class="left_box" id="SAREA00012">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>澳門、珠海、深圳、廣州</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=MFM0402" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000087/001006/00008187.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=MFM0402" target="_top">游遍中國~澳珠長隆海洋王國歡樂四日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=MFM04181209A' target='_top'>12/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=MFM04190106A' target='_top'>1/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=MFM04190113A' target='_top'>1/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=MFM04190210A' target='_top'>2/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=MFM04190217A' target='_top'>2/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=MFM04190303A' target='_top'>3/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=MFM04190310A' target='_top'>3/17</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=MFM0402" target="_top">...more</a></div>
				<div class="product_price"><span>$17,500起</span></div>
   				<div class="product_offer"><p><span style="color:#FFFFFF;"><span style="background-color:#FF8C00;">台中出發</span></span></p></div>
			</div>
		</div>
	</div>
</body>
</html>

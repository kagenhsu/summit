﻿<%
'//===========================================================================================
'//						index.asp
'//-------------------------------------------------------------------------------------------
'//		description		: 此頁面係由自動產生因此不用納入版本
'//-------------------------------------------------------------------------------------------
Response.CacheControl = "no-cache" 
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="robots" content="noindex">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
	<title>國外團體旅遊：東南亞旅遊行程 - 超悠</title>
	<!--產出時間:2018/12/4 下午 11:00:36 / WEB:summittour-->
	<link href='/eweb_summittour/css/reset.css' rel='stylesheet' type='text/css'>
	<link href='/eweb_summittour/css/autopage.css' rel='stylesheet' type='text/css'>
	<script languege="javascript" src="/script/jquery.js"></script>
	<script languege="javascript" src="/eweb_summittour/JS/autopage.js"></script>
</head>
<body>
	<div class="metropolis">
		<div class="title_box" >
			<div class="left_box" id="SAREA00061">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>西馬來西亞(吉隆坡、檳城)、新加坡</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=KULCIP05C" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000619/000641/00014437.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=KULCIP05C" target="_top">《五大保證》旗艦馬來西亞～全程五星酒店、大紅花海上VILLA、熱石SPA療程、英式下午茶5日(無購物)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=KULCI181222A' target='_top'>12/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULCI190104A' target='_top'>1/4</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULCI190107A' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULCI190109A' target='_top'>1/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULCI190111A' target='_top'>1/11</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULCI190114A' target='_top'>1/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULCI190116A' target='_top'>1/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULCI190118A' target='_top'>1/18</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=KULCIP05C" target="_top">...more</a></div>
				<div class="product_price"><span>$28,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=KULD7P05B" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000619/000641/00017545.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=KULD7P05B" target="_top">《春節預購減3千》翻轉馬來西亞～大紅花海上VILLA(一房一泳池)、馬六甲遊蹤5日(升等五星酒店1晚ｘ飯店自助晚餐)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=KULD7190201A' target='_top'>2/1</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULD7190204A' target='_top'>2/4</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULD7190206A' target='_top'>2/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULD7190207A' target='_top'>2/7</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=KULD7P05B" target="_top">...more</a></div>
				<div class="product_price"><span>$33,900起</span></div>
   				<div class="product_offer"><p><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=KULODP05C" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000619/000641/00017450.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=KULODP05C" target="_top">【馬印假期】鍾愛馬來-怡保山城、雙威溫泉樂園、五星悅榕庄五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190107A' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190110A' target='_top'>1/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190114A' target='_top'>1/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190117A' target='_top'>1/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190121A' target='_top'>1/21</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190124A' target='_top'>1/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190218A' target='_top'>2/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190225A' target='_top'>2/25</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=KULODP05C" target="_top">...more</a></div>
				<div class="product_price"><span>$16,900起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008844.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=KULODP05D" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000619/000641/00018038.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=KULODP05D" target="_top">【馬印假期】愛去馬來-馬六甲、法國村、雲頂小資五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190107B' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190110B' target='_top'>1/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190114B' target='_top'>1/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190117B' target='_top'>1/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190121B' target='_top'>1/21</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190124B' target='_top'>1/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190218B' target='_top'>2/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190225B' target='_top'>2/25</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=KULODP05D" target="_top">...more</a></div>
				<div class="product_price"><span>$14,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=KULODP05E" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000619/000641/00018047.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=KULODP05E" target="_top">【馬印假期】最愛馬來-AVANI棕櫚渡假村海上VILLA、馬六甲古城五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190107C' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190110C' target='_top'>1/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190114C' target='_top'>1/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190117C' target='_top'>1/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190121C' target='_top'>1/21</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190124C' target='_top'>1/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190218C' target='_top'>2/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190225C' target='_top'>2/25</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=KULODP05E" target="_top">...more</a></div>
				<div class="product_price"><span>$16,800起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=KULODP06B" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000619/000641/00014411.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=KULODP06B" target="_top">【豬年迎馬新】璀璨馬新大紅花海上Villa.環球影城.樂高樂園.漫步雲端.濱海灣無購物 6 日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190202A' target='_top'>2/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190204A' target='_top'>2/4</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190207A' target='_top'>2/7</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=KULODP06B" target="_top">...more</a></div>
				<div class="product_price"><span>$43,900起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011383.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PENCIP05B" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000619/000641/00017550.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PENCIP05B" target="_top">《經典》檳城蘭卡威～三離島之旅(孕婦島濕米島水晶島)、蟲鳴大地、魚吻足SPA5日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PENCI181217A' target='_top'>12/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PENCI181219A' target='_top'>12/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PENCI181221A' target='_top'>12/21</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PENCI181223A' target='_top'>12/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PENCI181224A' target='_top'>12/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PENCI181225A' target='_top'>12/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PENCI181230A' target='_top'>12/30</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PENCI181231A' target='_top'>12/31</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=PENCIP05B" target="_top">...more</a></div>
				<div class="product_price"><span>$25,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img alt="" height="21" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png" width="61" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SINBRP04A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000619/000641/00017580.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SINBRP04A" target="_top">甜蜜遊獅城～五星聖淘沙、環球影城、海洋生物園、金沙濱海灣四日(含稅)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=SINBR181214A' target='_top'>12/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SINBR181216A' target='_top'>12/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SINBR181218A' target='_top'>12/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SINBR181219A' target='_top'>12/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SINBR181220B' target='_top'>12/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SINBR181221A' target='_top'>12/21</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SINBR181222A' target='_top'>12/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SINBR181223B' target='_top'>12/23</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=SINBRP04A" target="_top">...more</a></div>
				<div class="product_price"><span>$26,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SINBRP05A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000619/000641/00014437.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SINBRP05A" target="_top">經典新馬大紅花海上VILLA五日～精油熱石SPA‧三輪車‧KYTOWER玻璃走廊‧3D藝術博物館‧高塔‧金沙(無購物)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=SINBR181220A' target='_top'>12/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SINBR181223A' target='_top'>12/23</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=SINBR181229A' target='_top'>12/29</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SINBR190103A' target='_top'>1/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SINBR190108A' target='_top'>1/8</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SINBR190110A' target='_top'>1/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SINBR190112A' target='_top'>1/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SINBR190115A' target='_top'>1/15</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=SINBRP05A" target="_top">...more</a></div>
				<div class="product_price"><span>$25,999起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img alt="" height="21" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png" width="61" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SINTRP05A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000619/000641/00017489.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SINTRP05A" target="_top">【豬年迎新春~2/6保證出團】新馬雙國獨家卡通星球.環球影城.樂高樂園.柔佛螢河遊.阿凡達燈光秀5日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=SINTR190206A' target='_top'>2/6</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=SINTRP05A" target="_top">...more</a></div>
				<div class="product_price"><span>$39,999起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img alt="" height="21" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png" width="61" /><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011383.png"/></p></div>
			</div>
		</div>
		<div class="title_box" >
			<div class="left_box" id="SAREA00060">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>東馬來西亞(沙巴、納閩、沙勞越)、汶萊</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=BKIMH05E" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000620/000646/00008439.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=BKIMH05E" target="_top">東南亞馬爾地夫~沙巴淘夢島5日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190102A' target='_top'>1/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190107A' target='_top'>1/7</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190109A' target='_top'>1/9</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190114A' target='_top'>1/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190116A' target='_top'>1/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190121A' target='_top'>1/21</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190123A' target='_top'>1/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190128A' target='_top'>1/28</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=BKIMH05E" target="_top">...more</a></div>
				<div class="product_price"><span>$16,800起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=BKIMH5A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000620/000646/00011771.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=BKIMH5A" target="_top">沙巴沙很大5日~海洋公園逍遙遊、生態雨林長鼻猴、精油SPA</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190104A' target='_top'>1/4</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190106A' target='_top'>1/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190111A' target='_top'>1/11</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190113A' target='_top'>1/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190118A' target='_top'>1/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190120A' target='_top'>1/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190125A' target='_top'>1/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190127A' target='_top'>1/27</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=BKIMH5A" target="_top">...more</a></div>
				<div class="product_price"><span>$19,300起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=BKIMH5B" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000620/000646/00014171.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=BKIMH5B" target="_top">情定沙巴生態雨林、泛舟、長鼻猴、精油SPA五日遊</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190101A' target='_top'>1/1</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190105A' target='_top'>1/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190108A' target='_top'>1/8</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190112A' target='_top'>1/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190115A' target='_top'>1/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190119A' target='_top'>1/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190122A' target='_top'>1/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190126A' target='_top'>1/26</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=BKIMH5B" target="_top">...more</a></div>
				<div class="product_price"><span>$23,500起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=BKIMH5C" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000620/000646/00014239.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=BKIMH5C" target="_top">探索沙巴、龍尾灣、瑪莉島逍遙5日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190103A' target='_top'>1/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190110A' target='_top'>1/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190117A' target='_top'>1/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190124A' target='_top'>1/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190131A' target='_top'>1/31</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190214A' target='_top'>2/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190221A' target='_top'>2/21</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190228A' target='_top'>2/28</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=BKIMH5C" target="_top">...more</a></div>
				<div class="product_price"><span>$20,800起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=BKIMHP05A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000620/000646/00017439.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=BKIMHP05A" target="_top">2019新春沙巴_麥哲倫海洋奇緣5日【豪華五星絲綢灣麥哲倫飯店+龍尾灣海角樂園七合一+淘夢島全日遊+長鼻猴&螢河遊蹤】</a></div>
				<div class="departure_date">
					出發日期：
					<s><a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190203A' target='_top'>2/3</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190205A' target='_top'>2/5</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=BKIMHP05A" target="_top">...more</a></div>
				<div class="product_price"><span>$55,100起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img alt="" height="21" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png" width="61" /><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011383.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=BKIMHP05B" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000620/000646/00008438.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=BKIMHP05B" target="_top">2019新春沙巴_海洋樂翻天4+1日【泛舟&高空滑索+婆羅洲文化村+淘夢島全日遊+長鼻猴&螢河遊蹤+飯店晚宴&開運撈生】</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190206A' target='_top'>2/6</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=BKIMHP05B" target="_top">...more</a></div>
				<div class="product_price"><span>$41,100起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img alt="" height="21" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png" width="61" /><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011383.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=BKIMHP5A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000620/000646/00010266.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=BKIMHP5A" target="_top">響亮璀璨沙巴五日(馬來西亞航空)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190107B' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190114B' target='_top'>1/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190121B' target='_top'>1/21</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190128B' target='_top'>1/28</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190211B' target='_top'>2/11</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190218B' target='_top'>2/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190225B' target='_top'>2/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190304B' target='_top'>3/4</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=BKIMHP5A" target="_top">...more</a></div>
				<div class="product_price"><span>$18,300起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
		</div>
		<div class="title_box" >
			<div class="left_box" id="SAREA00028">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>泰國(曼谷、清邁、華欣、蘇梅島、普吉島)</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=BKKCIP5A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000617/000631/00017293.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=BKKCIP5A" target="_top">【泰輕鬆】曼谷芭達雅、四方水上市場、時尚千萬遊艇、冰雪世界、海鮮燒烤+帝王蟹吃到飽5日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=BKKCI190111A' target='_top'>1/11</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKKCI190118A' target='_top'>1/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKKCI190215A' target='_top'>2/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKKCI190222A' target='_top'>2/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKKCI190308A' target='_top'>3/8</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKKCI190315A' target='_top'>3/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKKCI190322A' target='_top'>3/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKKCI190327A' target='_top'>3/27</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=BKKCIP5A" target="_top">...more</a></div>
				<div class="product_price"><span>$22,300起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=BKKCIP5B" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000617/000631/00017310.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=BKKCIP5B" target="_top">【泰國樂】曼谷五星(保證入住一房一廳)+Pool Villa 2晚+海豚灣+夢幻世界+雙按摩5日(無購物)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=BKKCI181218A' target='_top'>12/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKKCI181220A' target='_top'>12/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKKCI190108A' target='_top'>1/8</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKKCI190110A' target='_top'>1/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKKCI190115A' target='_top'>1/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKKCI190117A' target='_top'>1/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKKCI190212A' target='_top'>2/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKKCI190214A' target='_top'>2/14</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=BKKCIP5B" target="_top">...more</a></div>
				<div class="product_price"><span>$27,600起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=BKKCIP6A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000617/000631/00017300.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=BKKCIP6A" target="_top">【泰風情】曼谷芭達雅、暹邏水陸雙主題樂園、機器人餐廳、水流蝦燒烤吃到飽6日(無購物)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=BKKCI190106A' target='_top'>1/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKKCI190107A' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKKCI190113A' target='_top'>1/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKKCI190114A' target='_top'>1/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKKCI190210A' target='_top'>2/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKKCI190211A' target='_top'>2/11</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKKCI190217A' target='_top'>2/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKKCI190218A' target='_top'>2/18</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=BKKCIP6A" target="_top">...more</a></div>
				<div class="product_price"><span>$26,300起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CNXBRP5A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000617/000631/00017958.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CNXBRP5A" target="_top">BR【泰北玩家】清邁、清萊美麗境界5日(五星一房一廳+小木屋.長頸族.黑廟.白廟.金三角.寮國島.泰式按摩)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CNXBR190101A' target='_top'>1/1</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CNXBR190102A' target='_top'>1/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CNXBR190103A' target='_top'>1/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CNXBR190106A' target='_top'>1/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CNXBR190107A' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CNXBR190109A' target='_top'>1/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CNXBR190110A' target='_top'>1/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CNXBR190115A' target='_top'>1/15</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=CNXBRP5A" target="_top">...more</a></div>
				<div class="product_price"><span>$23,000起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CNXBRP5B" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000617/000631/00017981.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CNXBRP5B" target="_top">BR【泰北玩家】清邁、清萊美麗境界5日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CNXBR181224A' target='_top'>12/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CNXBR181225A' target='_top'>12/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CNXBR181227A' target='_top'>12/27</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CNXBR181231A' target='_top'>12/31</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CNXBRP5B" target="_top">...more</a></div>
				<div class="product_price"><span>$24,500起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CNXFDP5A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000617/000631/00018084.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CNXFDP5A" target="_top">FD【亞航假期】泰北雙城遊記叢林騎象超值5+1日遊</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CNXFD190105A' target='_top'>1/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CNXFD190112A' target='_top'>1/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CNXFD190119A' target='_top'>1/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CNXFD190123A' target='_top'>1/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CNXFD190302A' target='_top'>3/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CNXFD190306A' target='_top'>3/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CNXFD190309A' target='_top'>3/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CNXFD190313A' target='_top'>3/13</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=CNXFDP5A" target="_top">...more</a></div>
				<div class="product_price"><span>$18,200起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CNXFDP5B" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000617/000631/00018100.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CNXFDP5B" target="_top">FD【亞航假期】清邁泰好玩叢林騎象帕耀湖大象便便紙DIY5+1日遊</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CNXFD190105B' target='_top'>1/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CNXFD190112B' target='_top'>1/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CNXFD190119B' target='_top'>1/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CNXFD190123B' target='_top'>1/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CNXFD190302B' target='_top'>3/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CNXFD190306B' target='_top'>3/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CNXFD190309B' target='_top'>3/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CNXFD190313B' target='_top'>3/13</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=CNXFDP5B" target="_top">...more</a></div>
				<div class="product_price"><span>$20,500起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CNXFDP6A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000617/000631/00018116.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CNXFDP6A" target="_top">FD【亞航假期】泰北玩透透南邦帕耀湖蘭納古城6+1日遊</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CNXFD190117A' target='_top'>1/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CNXFD190124A' target='_top'>1/24</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CNXFDP6A" target="_top">...more</a></div>
				<div class="product_price"><span>$21,500起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
		</div>
		<div class="title_box" >
			<div class="left_box" id="SAREA00031">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>菲律賓(長灘島、宿霧、巴拉望、科隆)</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEB05A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00017052.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEB05A" target="_top">衝一波宿霧吧！奇幻島、二日自由活動、自選飯店5日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEB05190102A' target='_top'>1/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEB05190105A' target='_top'>1/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEB05190109A' target='_top'>1/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEB05190112A' target='_top'>1/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEB05190116A' target='_top'>1/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEB05190119A' target='_top'>1/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEB05190123A' target='_top'>1/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEB05190126A' target='_top'>1/26</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=CEB05A" target="_top">...more</a></div>
				<div class="product_price"><span>$9,500起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEB05B" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00015892.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEB05B" target="_top">虎航版～衝一波宿霧吧！奇幻島、二日自由活動、自選飯店5+1日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEB05190101A' target='_top'>1/1</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEB05190108A' target='_top'>1/8</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEB05190115A' target='_top'>1/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEB05190122A' target='_top'>1/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEB05190129A' target='_top'>1/29</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEB05190212A' target='_top'>2/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEB05190219A' target='_top'>2/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEB05190305A' target='_top'>3/5</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=CEB05B" target="_top">...more</a></div>
				<div class="product_price"><span>$9,500起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBBR0502" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00010562.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBBR0502" target="_top">鯨菲昔比-歐斯陸與鯨鯊共舞精緻五日【豪華版】</a></div>
				<div class="departure_date">
					出發日期：
					<s><a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181214A' target='_top'>12/14</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181217A' target='_top'>12/17</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CEBBR0502" target="_top">...more</a></div>
				<div class="product_price"><span>$32,500起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBBR1805B" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00015885.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBBR1805B" target="_top">宿霧雙島合一：南方夢幻海域一島一酒店五日遊</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181219A' target='_top'>12/19</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CEBBR1805B" target="_top">...more</a></div>
				<div class="product_price"><span>$28,500起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBBR1805C" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00015892.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBBR1805C" target="_top">宿霧雙島合一：享樂生活一島一酒店五日遊</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181215C' target='_top'>12/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181216B' target='_top'>12/16</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CEBBR1805C" target="_top">...more</a></div>
				<div class="product_price"><span>$27,500起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBBR1805D" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00009949.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBBR1805D" target="_top">宿霧小蜜月~兩天自由行五日遊</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181218B' target='_top'>12/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181220A' target='_top'>12/20</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CEBBR1805D" target="_top">...more</a></div>
				<div class="product_price"><span>$21,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBBRP05B" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00015892.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBBRP05B" target="_top">2人成行～新鯨豔宿霧5日精采版</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181219B' target='_top'>12/19</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181222A' target='_top'>12/22</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR190123A' target='_top'>1/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR190227A' target='_top'>2/27</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CEBBRP05B" target="_top">...more</a></div>
				<div class="product_price"><span>$29,500起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBBRP05C" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00012586.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBBRP05C" target="_top">春節限定～新鯨奇無比宿霧5日特惠版</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR190201A' target='_top'>2/1</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR190203A' target='_top'>2/3</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CEBBRP05C" target="_top">...more</a></div>
				<div class="product_price"><span>$38,500起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011383.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBBRP05D" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00017330.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBBRP05D" target="_top">春節限定～新鯨豔宿霧5日繽紛版(長榮加班包機)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR190202A' target='_top'>2/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR190206A' target='_top'>2/6</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CEBBRP05D" target="_top">...more</a></div>
				<div class="product_price"><span>$43,500起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img alt="" height="21" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png" width="61" /><img alt="" height="21" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011383.png" width="61" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBBRP05E" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00009949.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBBRP05E" target="_top">雙宿雙菲~宿霧薄荷島雙島五日繽紛版</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181215D' target='_top'>12/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181217B' target='_top'>12/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181220C' target='_top'>12/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181223A' target='_top'>12/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181224A' target='_top'>12/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181225A' target='_top'>12/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181226A' target='_top'>12/26</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR190306A' target='_top'>3/6</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CEBBRP05E" target="_top">...more</a></div>
				<div class="product_price"><span>$27,500起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBBRP05F" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00017678.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBBRP05F" target="_top">新鯨奇無比宿霧5日特惠版</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181215E' target='_top'>12/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181217C' target='_top'>12/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181220D' target='_top'>12/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181223B' target='_top'>12/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181224B' target='_top'>12/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181225B' target='_top'>12/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181226B' target='_top'>12/26</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR190123B' target='_top'>1/23</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=CEBBRP05F" target="_top">...more</a></div>
				<div class="product_price"><span>$25,500起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBITP05A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00009968.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBITP05A" target="_top">虎虎生風~宿霧資生堂跳島4+1日【輕鬆版】</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190124A' target='_top'>1/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190126A' target='_top'>1/26</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190131A' target='_top'>1/31</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190202A' target='_top'>2/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190207A' target='_top'>2/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190209A' target='_top'>2/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190214A' target='_top'>2/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190216A' target='_top'>2/16</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=CEBITP05A" target="_top">...more</a></div>
				<div class="product_price"><span>$13,900起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img alt="" height="21" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png" width="61" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBITP05B" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00017678.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBITP05B" target="_top">宿霧薄荷島資生堂雙跳島4+1日【超值版】</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190124B' target='_top'>1/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190126B' target='_top'>1/26</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190131B' target='_top'>1/31</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190202B' target='_top'>2/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190207B' target='_top'>2/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190209B' target='_top'>2/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190214B' target='_top'>2/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190216B' target='_top'>2/16</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=CEBITP05B" target="_top">...more</a></div>
				<div class="product_price"><span>$15,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBITP05C" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00012586.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBITP05C" target="_top">虎航宿霧薄荷島與鯨共舞全覽4+1日【全覽版】</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190124C' target='_top'>1/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190126C' target='_top'>1/26</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190131C' target='_top'>1/31</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190202C' target='_top'>2/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190207C' target='_top'>2/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190209C' target='_top'>2/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190214C' target='_top'>2/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190216C' target='_top'>2/16</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=CEBITP05C" target="_top">...more</a></div>
				<div class="product_price"><span>$20,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBITP05D" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00010562.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBITP05D" target="_top">0119聖嬰節限定～宿霧薄荷島資生堂雙跳島4+1日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190119A' target='_top'>1/19</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CEBITP05D" target="_top">...more</a></div>
				<div class="product_price"><span>$17,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBITP06A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00009949.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBITP06A" target="_top">虎虎生風~宿霧資生堂跳島5+1日【輕鬆版】</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190120A' target='_top'>1/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190122A' target='_top'>1/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190127A' target='_top'>1/27</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190129A' target='_top'>1/29</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190203A' target='_top'>2/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190205A' target='_top'>2/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190210A' target='_top'>2/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190212A' target='_top'>2/12</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=CEBITP06A" target="_top">...more</a></div>
				<div class="product_price"><span>$14,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBITP06B" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00012376.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBITP06B" target="_top">虎航宿霧薄荷島與鯨鯊共游5+1日【超值版】</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190120B' target='_top'>1/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190122B' target='_top'>1/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190127B' target='_top'>1/27</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190129B' target='_top'>1/29</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190203B' target='_top'>2/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190205B' target='_top'>2/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190210B' target='_top'>2/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190212B' target='_top'>2/12</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=CEBITP06B" target="_top">...more</a></div>
				<div class="product_price"><span>$20,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBITP06C" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00015904.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBITP06C" target="_top">虎航宿霧薄荷島與鯨共舞全覽5+1日【全覽版】</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190120C' target='_top'>1/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190122C' target='_top'>1/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190127C' target='_top'>1/27</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190129C' target='_top'>1/29</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190203C' target='_top'>2/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190210C' target='_top'>2/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190212C' target='_top'>2/12</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=CEBITP06C" target="_top">...more</a></div>
				<div class="product_price"><span>$24,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBZ2P04A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00017046.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBZ2P04A" target="_top">宿霧玩很大玩家4日遊特別版早去晚回(亞航)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEBZ2190228A' target='_top'>2/28</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CEBZ2P04A" target="_top">...more</a></div>
				<div class="product_price"><span>$23,500起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011377.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBZ2P05A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00012586.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBZ2P05A" target="_top">宿霧薄荷島海濱渡假五日精采版(保證入住薄荷島海濱渡假村)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEBZ2190227A' target='_top'>2/27</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CEBZ2P05A" target="_top">...more</a></div>
				<div class="product_price"><span>$30,500起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img alt="" height="21" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png" width="61" /><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008845.png" width="60" /><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011377.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBZ2P05B" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00012376.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBZ2P05B" target="_top">宿霧玩很大~歐斯陸與鯨鯊共舞玩家五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEBZ2190227B' target='_top'>2/27</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CEBZ2P05B" target="_top">...more</a></div>
				<div class="product_price"><span>$27,500起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img alt="" height="21" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png" width="61" /><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008845.png" width="60" /><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011377.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBZ2P06A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00017056.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBZ2P06A" target="_top">宿霧薄荷島精采六日(早去早回)</a></div>
				<div class="departure_date">
					出發日期：
					<s><a href='/EW/GO/GroupDetail.asp?prodCd=CEBZ2181227C' target='_top'>12/27</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CEBZ2P06A" target="_top">...more</a></div>
				<div class="product_price"><span>$36,900起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011381.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBZ2P06B" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00010562.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBZ2P06B" target="_top">宿霧玩很大~歐斯陸與鯨鯊共舞玩家6日(早去早回)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEBZ2181227D' target='_top'>12/27</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CEBZ2P06B" target="_top">...more</a></div>
				<div class="product_price"><span>$30,500起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=KLO5JP05A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00016897.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=KLO5JP05A" target="_top">最美長灘島回來了~風帆、香蕉船、沙灘按摩、環島遊、悠閒五日【直飛】</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=KLO5J190126A' target='_top'>1/26</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KLO5J190131A' target='_top'>1/31</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KLO5J190205A' target='_top'>2/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KLO5J190210A' target='_top'>2/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KLO5J190215A' target='_top'>2/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KLO5J190220A' target='_top'>2/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KLO5J190225A' target='_top'>2/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KLO5J190302A' target='_top'>3/2</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=KLO5JP05A" target="_top">...more</a></div>
				<div class="product_price"><span>$22,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=MNLCIP05A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00017322.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=MNLCIP05A" target="_top">【主題旅遊．愛冒險．全包式】菲律賓．EL_NIDO愛妮島．APULIT阿普莉渡假村5日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=MNLCI181214A' target='_top'>12/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=MNLCI181215A' target='_top'>12/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=MNLCI181216A' target='_top'>12/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=MNLCI181217A' target='_top'>12/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=MNLCI181218A' target='_top'>12/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=MNLCI181219A' target='_top'>12/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=MNLCI190101A' target='_top'>1/1</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=MNLCI190102A' target='_top'>1/2</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=MNLCIP05A" target="_top">...more</a></div>
				<div class="product_price"><span>$59,500起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=MPH5JP05A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00016897.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=MPH5JP05A" target="_top">春節限定5J轉機～HIGH翻長灘島悠閒五日</a></div>
 				<div class="product_description">春節旅遊~10人以上加派領隊</div> 
				<div class="departure_date">
					出發日期：
					<s><a href='/EW/GO/GroupDetail.asp?prodCd=MPH5J190202A' target='_top'>2/2</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=MPH5J190204A' target='_top'>2/4</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=MPH5J190206A' target='_top'>2/6</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=MPH5JP05A" target="_top">...more</a></div>
				<div class="product_price"><span>$25,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PPS5JP05A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00015348.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PPS5JP05A" target="_top">春節限定～愛上巴拉望一島一飯店跳島渡假5日(保證入住一島一飯店)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PPS5J190202B' target='_top'>2/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PPS5J190203B' target='_top'>2/3</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=PPS5JP05A" target="_top">...more</a></div>
				<div class="product_price"><span>$33,500起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PPS5JP05B" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00016479.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PPS5JP05B" target="_top">228限定～愛上巴拉望一島一飯店跳島渡假5日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PPS5J190227A' target='_top'>2/27</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PPS5J190228A' target='_top'>2/28</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=PPS5JP05B" target="_top">...more</a></div>
				<div class="product_price"><span>$30,500起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PPS5JP05C" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00016479.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PPS5JP05C" target="_top">春節限定5J轉機～玩瘋了巴拉望超級玩家、地底河流、本田灣五天</a></div>
 				<div class="product_description">春節旅遊~10人以上加派領隊</div> 
				<div class="departure_date">
					出發日期：
					<s><a href='/EW/GO/GroupDetail.asp?prodCd=PPS5J190202A' target='_top'>2/2</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PPS5J190203A' target='_top'>2/3</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=PPS5J190204A' target='_top'>2/4</a></s> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=PPS5J190205A' target='_top'>2/5</a></s> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=PPS5J190206A' target='_top'>2/6</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=PPS5JP05C" target="_top">...more</a></div>
				<div class="product_price"><span>$28,800起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011383.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PPSFEP05A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00015348.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PPSFEP05A" target="_top">2人成行～新看見巴拉望渡假5日(保證入住一島一飯店)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PPSFE190119A' target='_top'>1/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PPSFE190123A' target='_top'>1/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PPSFE190127A' target='_top'>1/27</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PPSFE190212A' target='_top'>2/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PPSFE190216A' target='_top'>2/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PPSFE190220A' target='_top'>2/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PPSFE190224A' target='_top'>2/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PPSFE190228A' target='_top'>2/28</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=PPSFEP05A" target="_top">...more</a></div>
				<div class="product_price"><span>$28,500起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PPSFEP05C" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00015904.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PPSFEP05C" target="_top">春節限定～2人成行新看見巴拉望渡假5日(保證入住一島一飯店)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PPSFE190131A' target='_top'>1/31</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PPSFE190204A' target='_top'>2/4</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=PPSFE190208A' target='_top'>2/8</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=PPSFEP05C" target="_top">...more</a></div>
				<div class="product_price"><span>$28,500起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
		</div>
	</div>
</body>
</html>

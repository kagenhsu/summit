﻿<%
'//===========================================================================================
'//						index.asp
'//-------------------------------------------------------------------------------------------
'//		description		: 此頁面係由自動產生因此不用納入版本
'//-------------------------------------------------------------------------------------------
Response.CacheControl = "no-cache" 
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="robots" content="noindex">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
	<title>國外團體旅遊：港澳大陸旅遊行程 - 超悠</title>
	<!--產出時間:2018/12/4 下午 11:01:03 / WEB:summittour-->
	<link href='/eweb_summittour/css/reset.css' rel='stylesheet' type='text/css'>
	<link href='/eweb_summittour/css/autopage.css' rel='stylesheet' type='text/css'>
	<script languege="javascript" src="/script/jquery.js"></script>
	<script languege="javascript" src="/eweb_summittour/JS/autopage.js"></script>
</head>
<body>
	<div class="metropolis">
		<div class="title_box" >
			<div class="left_box" id="SAREA00270">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>南北疆、絲路</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=URCMFPT12A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000065/001160/00013773.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=URCMFPT12A" target="_top">南疆風情.帕米爾高原.卡拉庫力湖.溫宿大峽谷12天(廈門)</a></div>
 				<div class="product_description">贈送每人一頂維吾爾小花帽</div> 
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=URCMF181217A' target='_top'>12/17</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=URCMFPT12A" target="_top">...more</a></div>
				<div class="product_price"><span>$47,900起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img height="21" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011521.png"/></p></div>
			</div>
		</div>
		<div class="title_box" >
			<div class="left_box" id="SAREA00005">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>山東(濟南、青島、威海、煙台、徐州）</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=TAO0803" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000073/000982/00018167.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=TAO0803" target="_top">游遍中國─歐陸青島天鵝湖童話屋泉城泰山祈福8日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=TAOCIT89206A' target='_top'>2/6</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=TAO0803" target="_top">...more</a></div>
				<div class="product_price"><span>$32,900起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=TAOCIT8A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000073/000982/00011962.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=TAOCIT8A" target="_top">華信假期─山東青島琴島之眼濟南泰山封禪秀臺兒莊八天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=TAOCIP89109A' target='_top'>1/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=TAOCIP89130A' target='_top'>1/30</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=TAOCIP89227A' target='_top'>2/27</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=TAOCIP89313A' target='_top'>3/13</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=TAOCIT8A" target="_top">...more</a></div>
				<div class="product_price"><span>$25,800起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /></p></div>
			</div>
		</div>
		<div class="title_box" >
			<div class="left_box" id="SAREA00013">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>山西(太原)、內蒙(呼和浩特)、銀川</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=INCFEPT8A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000074/000985/00014927.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=INCFEPT8A" target="_top">游遍中國─寧夏沙坡頭108塔東方泰姬瑪哈陵溫泉8日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=INCMUT89122A' target='_top'>1/22</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=INCMUT89226A' target='_top'>2/26</a></s> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=INCMUT89326A' target='_top'>3/26</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=INCFEPT8A" target="_top">...more</a></div>
				<div class="product_price"><span>$21,500起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=INCMUPT8A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000074/000985/00014927.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=INCMUPT8A" target="_top">游遍中國─寧夏沙坡頭108塔東方泰姬瑪哈陵沙湖8日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=INCMUT89427A' target='_top'>4/27</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=INCMUT89525A' target='_top'>5/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=INCMUT89622A' target='_top'>6/22</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=INCMUPT8A" target="_top">...more</a></div>
				<div class="product_price"><span>$24,500起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=INCMUT8A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000074/000985/00017865.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=INCMUT8A" target="_top">游遍中國─塞上寧夏金沙島黃河石林西夏王朝八日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=INCMUT89427B' target='_top'>4/27</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=INCMUT89525B' target='_top'>5/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=INCMUT89622B' target='_top'>6/22</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=INCMUT8A" target="_top">...more</a></div>
				<div class="product_price"><span>$27,500起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /></p></div>
			</div>
		</div>
		<div class="title_box" >
			<div class="left_box" id="SAREA00007">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>鄭州、西安</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CGOAEPR8B" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000076/000987/00010411.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CGOAEPR8B" target="_top">華信假期─賀新年龍門石窟雲臺山萬仙山郭亮村八日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CGOAEP89206A' target='_top'>2/6</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CGOAEPR8B" target="_top">...more</a></div>
				<div class="product_price"><span>$32,800起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CGOAEPT8A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000076/000987/00010411.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CGOAEPT8A" target="_top">華信假期─太行大峽谷龍門石窟雲台山頂級八日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CGOAEP88D28A' target='_top'>12/28</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CGOAEPT8A" target="_top">...more</a></div>
				<div class="product_price"><span>$31,800起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CGOAEPT8D" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000076/000987/00016483.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CGOAEPT8D" target="_top">華信假期─鄭州之旅登封少林寺龍門石窟雲臺山萬仙山郭亮村八日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CGOAEP88D28B' target='_top'>12/28</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=CGOAEP89102B' target='_top'>1/2</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CGOAEP89116B' target='_top'>1/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CGOAEP89220B' target='_top'>2/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CGOAEP89313B' target='_top'>3/13</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CGOAEPT8D" target="_top">...more</a></div>
				<div class="product_price"><span>$23,800起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /></p></div>
			</div>
		</div>
		<div class="title_box" >
			<div class="left_box" id="SAREA00002">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>九寨溝、稻城亞丁</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTU08TP" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000079/000549/00012385.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTU08TP" target="_top">華信假期─冰川奇景~海螺溝、四姑娘山八天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCIP88D18A' target='_top'>12/18</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CTU08TP" target="_top">...more</a></div>
				<div class="product_price"><span>$24,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTUCA08" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000079/001180/00017746.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTUCA08" target="_top">五星希爾頓四姑娘山新都橋木格錯8日(無購物、無自費)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190103A' target='_top'>1/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190115A' target='_top'>1/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190122A' target='_top'>1/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190212A' target='_top'>2/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190223A' target='_top'>2/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190302A' target='_top'>3/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190312A' target='_top'>3/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190319A' target='_top'>3/19</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CTUCA08" target="_top">...more</a></div>
				<div class="product_price"><span>$21,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTUCA08B" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000079/001180/00011696.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTUCA08B" target="_top">五星希爾頓四姑娘山蒸汽小車羅城古鎮8日(無購物、無自費)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190103B' target='_top'>1/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190115B' target='_top'>1/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190122B' target='_top'>1/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190212B' target='_top'>2/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190223B' target='_top'>2/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190302B' target='_top'>3/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190312B' target='_top'>3/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190319B' target='_top'>3/19</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CTUCA08B" target="_top">...more</a></div>
				<div class="product_price"><span>$19,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTUCAPT8A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000079/000549/00007741.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTUCAPT8A" target="_top">稻城.亞丁.新都橋.丹巴.四姑娘山八日</a></div>
 				<div class="product_description">贈送紅景天口服液一盒</div> 
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA181217A' target='_top'>12/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA181231A' target='_top'>12/31</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190107A' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190312C' target='_top'>3/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190323A' target='_top'>3/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190326A' target='_top'>3/26</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CTUCAPT8A" target="_top">...more</a></div>
				<div class="product_price"><span>$25,900起</span></div>
   				<div class="product_offer"><p><img alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" style="height: 22px; width: 60px" /><img height="21" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011521.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTUCAPT8B" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000079/000549/00007728.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTUCAPT8B" target="_top">四姑娘山、甲居藏寨、新都橋、海螺溝8日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA181218A' target='_top'>12/18</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA181225A' target='_top'>12/25</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190101A' target='_top'>1/1</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190115C' target='_top'>1/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190129A' target='_top'>1/29</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CTUCAPT8B" target="_top">...more</a></div>
				<div class="product_price"><span>$21,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
		</div>
		<div class="title_box" >
			<div class="left_box" id="SAREA00001">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>長沙、張家界</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CSXAEPT8A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000080/000993/00018270.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CSXAEPT8A" target="_top">華信假期─湘江風光張家界鳳凰天空玻璃橋尊爵8日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CSXAEP89321A' target='_top'>3/21</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CSXAEPT8A" target="_top">...more</a></div>
				<div class="product_price"><span>$26,800起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CSXAEPT8B" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000080/000993/00008166.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CSXAEPT8B" target="_top">華信假期─湘媚風光張家界鳳凰天空玻璃橋豪華8天</a></div>
				<div class="departure_date">
					出發日期：
					<s><a href='/EW/GO/GroupDetail.asp?prodCd=CSXAEP89207A' target='_top'>2/7</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CSXAEPT8B" target="_top">...more</a></div>
				<div class="product_price"><span>$28,800起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CSXAEPT8C" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000080/000993/00016859.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CSXAEPT8C" target="_top">華信假期─湘西風光張家界鳳凰天空玻璃橋豪華8天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CSXAEP88D20A' target='_top'>12/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CSXAEP89103A' target='_top'>1/3</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=CSXAEP89110A' target='_top'>1/10</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CSXAEP89117A' target='_top'>1/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CSXAEP89228A' target='_top'>2/28</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=CSXAEP89307A' target='_top'>3/7</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CSXAEPT8C" target="_top">...more</a></div>
				<div class="product_price"><span>$19,800起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
		</div>
		<div class="title_box" >
			<div class="left_box" id="SAREA00040">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>江南、黃山、江西(南昌)</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=HGH0502" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000258/000995/00017924.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=HGH0502" target="_top">華信假期─杭州海寧上海楓涇古鎮尊貴五天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=HGHAEP59206A' target='_top'>2/6</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=HGH0502" target="_top">...more</a></div>
				<div class="product_price"><span>$29,000起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=HGHAEPR5A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000258/000995/00008171.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=HGHAEPR5A" target="_top">華信假期-黃山奇景千島湖西遞村杭州西湖五日</a></div>
				<div class="departure_date">
					出發日期：
					<s><a href='/EW/GO/GroupDetail.asp?prodCd=HGHAEP59109A' target='_top'>1/9</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=HGHAEPR5A" target="_top">...more</a></div>
				<div class="product_price"><span>$20,500起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.PNG	" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=NKGAET8A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000258/000995/00015205.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=NKGAET8A" target="_top">東方神起─南京揚州鹽城徐州句容尊爵逍遙八天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=NKGMUP89202A' target='_top'>2/2</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=NKGAET8A" target="_top">...more</a></div>
				<div class="product_price"><span>$29,999起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=NKGCIT8A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000258/000995/00015205.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=NKGCIT8A" target="_top">東方神起─南京無錫嘉興紹興橫店義烏烏鎮海寧句容超值八天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=NKGMUP88D22B' target='_top'>12/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NKGMUP89105B' target='_top'>1/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NKGMUP89119B' target='_top'>1/19</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=NKGMUP89302B' target='_top'>3/2</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=NKGCIT8A" target="_top">...more</a></div>
				<div class="product_price"><span>$19,900起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=NKGMUT8B" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000258/000995/00015205.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=NKGMUT8B" target="_top">東方航空─南京揚州瘦西湖鹽城丹頂鶴徐州句容尊爵八天</a></div>
				<div class="departure_date">
					出發日期：
					<s><a href='/EW/GO/GroupDetail.asp?prodCd=NKGMUP88D22A' target='_top'>12/22</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NKGMUP89112A' target='_top'>1/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NKGMUP89223A' target='_top'>2/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NKGMUP89309A' target='_top'>3/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NKGMUP89316A' target='_top'>3/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NKGMUP89323A' target='_top'>3/23</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=NKGMUT8B" target="_top">...more</a></div>
				<div class="product_price"><span>$22,000起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.PNG	" width="60" /><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008861.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=WUXAEPR5A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000258/000995/00017367.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=WUXAEPR5A" target="_top">華信假期─無錫泰州鹽城蘇北精華尊貴五天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=WUXAEP59107A' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=WUXAEP59218A' target='_top'>2/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=WUXAEP59304A' target='_top'>3/4</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=WUXAEP59318A' target='_top'>3/18</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=WUXAEPR5A" target="_top">...more</a></div>
				<div class="product_price"><span>$18,000起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=WUXAEPR5B" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000258/000995/00011499.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=WUXAEPR5B" target="_top">游遍中國─無錫杭州嘉興上海五天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=WUXAER59204A' target='_top'>2/4</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=WUXAEPR5B" target="_top">...more</a></div>
				<div class="product_price"><span>$26,500起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.png" width="60" /><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011383.png"/></p></div>
			</div>
		</div>
		<div class="title_box" >
			<div class="left_box" id="SAREA00057">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>重慶、武漢、長江三峽</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=WUHCIP8B" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000081/000997/00011822.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=WUHCIP8B" target="_top">華信假期─天下第一仙山武當山兩壩一峽８日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=WUHCIP89124A' target='_top'>1/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=WUHCIP89131A' target='_top'>1/31</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=WUHCIP89214A' target='_top'>2/14</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=WUHCIP89314A' target='_top'>3/14</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=WUHCIP8B" target="_top">...more</a></div>
				<div class="product_price"><span>$29,800起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=WUHCIT8C" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000081/000997/00010002.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=WUHCIT8C" target="_top">華信假期─四季浪歌瀑布絕壁龍洞恩施大峽谷三峽人家八天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=WUHCIP89124B' target='_top'>1/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=WUHCIP89131B' target='_top'>1/31</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=WUHCIP89214B' target='_top'>2/14</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=WUHCIP89314B' target='_top'>3/14</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=WUHCIT8C" target="_top">...more</a></div>
				<div class="product_price"><span>$27,800起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
		</div>
		<div class="title_box" >
			<div class="left_box" id="SAREA00012">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>澳門、珠海、深圳、廣州</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=MFM0402" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000087/001006/00008187.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=MFM0402" target="_top">游遍中國~澳珠長隆海洋王國歡樂四日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=MFM04181209A' target='_top'>12/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=MFM04190106A' target='_top'>1/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=MFM04190113A' target='_top'>1/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=MFM04190210A' target='_top'>2/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=MFM04190217A' target='_top'>2/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=MFM04190303A' target='_top'>3/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=MFM04190310A' target='_top'>3/17</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=MFM0402" target="_top">...more</a></div>
				<div class="product_price"><span>$17,500起</span></div>
   				<div class="product_offer"><p><span style="color:#FFFFFF;"><span style="background-color:#FF8C00;">台中出發</span></span></p></div>
			</div>
		</div>
		<div class="title_box" >
			<div class="left_box" id="SAREA00015">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>福建(廈門、福州、武夷山)</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=XMN04" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000086/001155/00017747.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=XMN04" target="_top">廈門靈玲馬戲城平和土樓4日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=XMN04190228A' target='_top'>2/28</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=XMN04" target="_top">...more</a></div>
				<div class="product_price"><span>$17,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=XMNMFPT5A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000086/001004/00011358.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=XMNMFPT5A" target="_top">《遨遊天廈》廈門武夷山雙飛、閩南傳奇秀五日之旅</a></div>
 				<div class="product_description">贈送每人每天1瓶礦泉水</div> 
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=XMNMF190105A' target='_top'>1/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=XMNMF190112A' target='_top'>1/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=XMNMF190223A' target='_top'>2/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=XMNMF190302A' target='_top'>3/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=XMNMF190309A' target='_top'>3/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=XMNMF190316A' target='_top'>3/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=XMNMF190323A' target='_top'>3/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=XMNMF190330A' target='_top'>3/30</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=XMNMFPT5A" target="_top">...more</a></div>
				<div class="product_price"><span>$17,900起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img height="21" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011521.png"/></p></div>
			</div>
		</div>
		<div class="title_box" >
			<div class="left_box" id="SAREA00041">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>海南(海口、三亞)</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR4A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000085/000547/00014888.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR4A" target="_top">海南傳奇~三亞Club Med玫瑰谷海棠廣場椰田古寨精彩四天</a></div>
				<div class="departure_date">
					出發日期：
					<s><a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP48D23C' target='_top'>12/23</a></s> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP48D30C' target='_top'>12/30</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49106C' target='_top'>1/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49113C' target='_top'>1/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49120C' target='_top'>1/20</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49127C' target='_top'>1/27</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49217C' target='_top'>2/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49224C' target='_top'>2/24</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=SYX3UPR4A" target="_top">...more</a></div>
				<div class="product_price"><span>$23,500起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.PNG	" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR4C" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000085/000547/00010269.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR4C" target="_top">海南傳奇─三亞臨春嶺森林公園陵水超值四天(買一送一)</a></div>
 				<div class="product_description">稅外$2500/人.買一送一</div> 
				<div class="departure_date">
					出發日期：
					<s><a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP48D23S' target='_top'>12/23</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=SYX3UPR4C" target="_top">...more</a></div>
				<div class="product_price"><span>$4,900起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR4D" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000085/000547/00017947.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR4D" target="_top">海南傳奇─玩翻三亞水上活動浪漫風帆精彩搖滾之旅四天</a></div>
 				<div class="product_description">送網卡</div> 
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49106R' target='_top'>1/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49113R' target='_top'>1/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49120R' target='_top'>1/20</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49127R' target='_top'>1/27</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49217R' target='_top'>2/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49224R' target='_top'>2/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49303R' target='_top'>3/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49310R' target='_top'>3/10</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=SYX3UPR4D" target="_top">...more</a></div>
				<div class="product_price"><span>$14,999起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR4E" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000085/000547/00017978.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR4E" target="_top">海南傳奇─三亞全程五星希爾頓興隆泡湯趣四日(稅外2500)</a></div>
 				<div class="product_description">稅外$2500/人</div> 
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49106H' target='_top'>1/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49113H' target='_top'>1/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49120H' target='_top'>1/20</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49127H' target='_top'>1/27</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49217H' target='_top'>2/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49224H' target='_top'>2/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49303H' target='_top'>3/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49310H' target='_top'>3/10</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=SYX3UPR4E" target="_top">...more</a></div>
				<div class="product_price"><span>$6,900起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR5B" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000085/000547/00014852.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR5B" target="_top">海南傳奇─三亞海口馮小剛電影公社興隆陵水低調奢華五天</a></div>
				<div class="departure_date">
					出發日期：
					<s><a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59206A' target='_top'>2/6</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=SYX3UPR5B" target="_top">...more</a></div>
				<div class="product_price"><span>$29,500起</span></div>
   				<div class="product_offer"><p><span style="color:#FFFFFF;"><span style="background-color:#FF8C00;">台中出發</span></span></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR5C" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000085/000547/00014852.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR5C" target="_top">海南傳奇─三亞陵水特選優值五天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59130A' target='_top'>1/30</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=SYX3UPR5C" target="_top">...more</a></div>
				<div class="product_price"><span>$19,800起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR5D" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000085/000547/00017929.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR5D" target="_top">海南傳奇─三亞Club Med陵水奢華五天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59102C' target='_top'>1/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59109C' target='_top'>1/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59116C' target='_top'>1/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59130C' target='_top'>1/30</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59213C' target='_top'>2/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59220C' target='_top'>2/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59306C' target='_top'>3/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59313C' target='_top'>3/13</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=SYX3UPR5D" target="_top">...more</a></div>
				<div class="product_price"><span>$28,300起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR5E" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000085/000547/00017949.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR5E" target="_top">海南傳奇─玩翻三亞大東海浪漫風帆萬寧石梅灣五日</a></div>
 				<div class="product_description">送網卡</div> 
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59102R' target='_top'>1/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59109R' target='_top'>1/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59116R' target='_top'>1/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59130R' target='_top'>1/30</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59213R' target='_top'>2/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59220R' target='_top'>2/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59306R' target='_top'>3/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59313R' target='_top'>3/13</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=SYX3UPR5E" target="_top">...more</a></div>
				<div class="product_price"><span>$19,999起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR5F" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000085/000547/00017977.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR5F" target="_top">海南傳奇─三亞全程五星希爾頓興隆泡湯趣五天(稅外2500)</a></div>
 				<div class="product_description">稅外$2500/人</div> 
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP58D19H' target='_top'>12/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59102H' target='_top'>1/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59109H' target='_top'>1/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59116H' target='_top'>1/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59130H' target='_top'>1/30</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59213H' target='_top'>2/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59220H' target='_top'>2/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59306H' target='_top'>3/6</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=SYX3UPR5F" target="_top">...more</a></div>
				<div class="product_price"><span>$8,400起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYXJDPR4A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000085/000547/00017789.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYXJDPR4A" target="_top">海南傳奇─三亞鳳凰嶺陵水椰田古寨特選優值四天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49203A' target='_top'>2/3</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=SYXJDPR4A" target="_top">...more</a></div>
				<div class="product_price"><span>$19,900起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.PNG	" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYXJDPR4B" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000085/000547/00014851.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYXJDPR4B" target="_top">海南傳奇─三亞蘭花大世界南天生態植物園鳳凰嶺精選優值四天</a></div>
				<div class="departure_date">
					出發日期：
					<s><a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP48D23A' target='_top'>12/23</a></s> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP48D30A' target='_top'>12/30</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49106A' target='_top'>1/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49113A' target='_top'>1/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49120A' target='_top'>1/20</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49127A' target='_top'>1/27</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49217A' target='_top'>2/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49224A' target='_top'>2/24</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=SYXJDPR4B" target="_top">...more</a></div>
				<div class="product_price"><span>$11,999起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.PNG	" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYXJDPR5A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000085/000547/00014879.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYXJDPR5A" target="_top">海南傳奇─三亞保亭陵水椰田古寨石梅灣奢華饗宴五天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP58D19A' target='_top'>12/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP58D26A' target='_top'>12/26</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59102A' target='_top'>1/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59109A' target='_top'>1/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59116A' target='_top'>1/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59123A' target='_top'>1/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59213A' target='_top'>2/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59220A' target='_top'>2/20</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=SYXJDPR5A" target="_top">...more</a></div>
				<div class="product_price"><span>$21,000起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.PNG	" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYXJDPR5B" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000085/000547/00008345.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYXJDPR5B" target="_top">海南傳奇─三亞Club Med宋城千古情陵水奢華五天</a></div>
				<div class="departure_date">
					出發日期：
					<s><a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59206B' target='_top'>2/6</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=SYXJDPR5B" target="_top">...more</a></div>
				<div class="product_price"><span>$40,500起</span></div>
   				<div class="product_offer"><p><span style="color:#FFFFFF;"><span style="background-color:#FF8C00;">台中出發</span></span></p></div>
			</div>
		</div>
	</div>
</body>
</html>

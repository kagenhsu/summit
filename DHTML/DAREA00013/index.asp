﻿<%
'//===========================================================================================
'//						index.asp
'//-------------------------------------------------------------------------------------------
'//		description		: 此頁面係由自動產生因此不用納入版本
'//-------------------------------------------------------------------------------------------
Response.CacheControl = "no-cache" 
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="robots" content="noindex">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
	<title>國外團體旅遊：促銷行程旅遊行程 - 超悠</title>
	<!--產出時間:2018/12/4 下午 11:00:49 / WEB:summittour-->
	<link href='/eweb_summittour/css/reset.css' rel='stylesheet' type='text/css'>
	<link href='/eweb_summittour/css/autopage.css' rel='stylesheet' type='text/css'>
	<script languege="javascript" src="/script/jquery.js"></script>
	<script languege="javascript" src="/eweb_summittour/JS/autopage.js"></script>
</head>
<body>
	<div class="metropolis">
		<div class="title_box" >
			<div class="left_box" id="SAREA00065">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>超低價</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PEKCA05A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000077/001149/00017792.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PEKCA05A" target="_top">皇朝古都北京司馬台長城古北水鎮頤和園五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PEKCA190102B' target='_top'>1/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PEKCA190109B' target='_top'>1/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PEKCA190116B' target='_top'>1/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PEKCA190123B' target='_top'>1/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PEKCA190213B' target='_top'>2/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PEKCA190220B' target='_top'>2/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PEKCA190306B' target='_top'>3/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PEKCA190313B' target='_top'>3/13</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=PEKCA05A" target="_top">...more</a></div>
				<div class="product_price"><span>$20,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PEKCA08A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000077/001149/00013297.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PEKCA08A" target="_top">北京四大文化遺產超值五日(無購物)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PEKCA190102A' target='_top'>1/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PEKCA190109A' target='_top'>1/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PEKCA190116A' target='_top'>1/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PEKCA190123A' target='_top'>1/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PEKCA190213A' target='_top'>2/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PEKCA190220A' target='_top'>2/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PEKCA190306A' target='_top'>3/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PEKCA190313A' target='_top'>3/13</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=PEKCA08A" target="_top">...more</a></div>
				<div class="product_price"><span>$16,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=WNZCA05" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000258/001154/00017856.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=WNZCA05" target="_top">溫州雁蕩山、神仙居、古堰畫鄉、土豪宴五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=WNZCA190105A' target='_top'>1/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=WNZCA190110A' target='_top'>1/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=WNZCA190112A' target='_top'>1/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=WNZCA190117A' target='_top'>1/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=WNZCA190119A' target='_top'>1/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=WNZCA190124A' target='_top'>1/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=WNZCA190126A' target='_top'>1/26</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=WNZCA190214A' target='_top'>2/14</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=WNZCA05" target="_top">...more</a></div>
				<div class="product_price"><span>$18,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
		</div>
	</div>
</body>
</html>

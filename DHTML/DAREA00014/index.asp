﻿<%
'//===========================================================================================
'//						index.asp
'//-------------------------------------------------------------------------------------------
'//		description		: 此頁面係由自動產生因此不用納入版本
'//-------------------------------------------------------------------------------------------
Response.CacheControl = "no-cache" 
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="robots" content="noindex">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
	<title>國外團體旅遊：美加旅遊行程 - 超悠</title>
	<!--產出時間:2018/12/4 下午 11:00:54 / WEB:summittour-->
	<link href='/eweb_summittour/css/reset.css' rel='stylesheet' type='text/css'>
	<link href='/eweb_summittour/css/autopage.css' rel='stylesheet' type='text/css'>
	<script languege="javascript" src="/script/jquery.js"></script>
	<script languege="javascript" src="/eweb_summittour/JS/autopage.js"></script>
</head>
<body>
	<div class="metropolis">
		<div class="title_box" >
			<div class="left_box" id="SAREA00080">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>古巴</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=HAVCAP13A" target="_top"><img src="/eWeb_summittour/IMGDB/000577/001055/001058/00008316.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=HAVCAP13A" target="_top">國航出任務～古巴大冒險～發現新北京13天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=HAVCA190110A' target='_top'>1/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=HAVCA190411A' target='_top'>4/11</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=HAVCAP13A" target="_top">...more</a></div>
				<div class="product_price"><span>$128,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=LAXBRP11B" target="_top"><img src="/eWeb_summittour/IMGDB/000577/001055/001058/00017899.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=LAXBRP11B" target="_top">情迷醉人烏托邦～古巴深度11天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=LAXBR190111A' target='_top'>1/11</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXBR190215A' target='_top'>2/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXBR190315A' target='_top'>3/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXBR190412A' target='_top'>4/12</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=LAXBRP11B" target="_top">...more</a></div>
				<div class="product_price"><span>$128,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=LAXMIAHXP10A" target="_top"><img src="/eWeb_summittour/IMGDB/000577/001055/001058/00012986.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=LAXMIAHXP10A" target="_top">神祕古巴華麗的冒險10天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=LAXMI190111B' target='_top'>1/11</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXMI190215B' target='_top'>2/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXMI190315B' target='_top'>3/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXMI190412B' target='_top'>4/12</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=LAXMIAHXP10A" target="_top">...more</a></div>
				<div class="product_price"><span>$99,900起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.PNG	" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=LAXMIAHXP12A" target="_top"><img src="/eWeb_summittour/IMGDB/000577/001055/001058/00015018.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=LAXMIAHXP12A" target="_top">情迷醉人烏托邦~古巴深度12天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=LAXMI190111A' target='_top'>1/11</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXMI190215A' target='_top'>2/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXMI190315A' target='_top'>3/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXMI190412A' target='_top'>4/12</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=LAXMIAHXP12A" target="_top">...more</a></div>
				<div class="product_price"><span>$139,900起</span></div>
   				<div class="product_offer"><p><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00015107.png"/><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.PNG	" width="60" /></p></div>
			</div>
		</div>
		<div class="title_box" >
			<div class="left_box" id="SAREA00070">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>阿拉斯加</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=FAIBRP08A" target="_top">阿拉斯加璀璨極光、珍娜溫泉8天</a></div>
 				<div class="product_description">保證兩晚入住極光星球屋</div> 
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=FAIBR190114A' target='_top'>1/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=FAIBR190407A' target='_top'>4/7</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=FAIBRP08A" target="_top">...more</a></div>
				<div class="product_price"><span>$94,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=YVRHXP12A" target="_top"><img src="/eWeb_summittour/IMGDB/000577/000768/001193/00012761.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=YVRHXP12A" target="_top">阿拉斯加極光、洛磯山脈雪景、奇幻之旅12天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=YVRHX190220A' target='_top'>2/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=YVRHX190225A' target='_top'>2/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=YVRHX190313A' target='_top'>3/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=YVRHX190327A' target='_top'>3/27</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=YVRHXP12A" target="_top">...more</a></div>
				<div class="product_price"><span>$109,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
		</div>
		<div class="title_box" >
			<div class="left_box" id="SAREA00066">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>洛杉磯</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=LAXHXP08A" target="_top"><img src="/eWeb_summittour/IMGDB/000577/000764/000949/00013080.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=LAXHXP08A" target="_top">美西雙國家公園～大峽谷+錫安、羚羊峽谷+馬蹄灣8天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190106A' target='_top'>1/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190107C' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190120A' target='_top'>1/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190121A' target='_top'>1/21</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190217A' target='_top'>2/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190218C' target='_top'>2/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190224A' target='_top'>2/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190225C' target='_top'>2/25</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=LAXHXP08A" target="_top">...more</a></div>
				<div class="product_price"><span>$44,900起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008861.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=LAXHXP08B" target="_top"><img src="/eWeb_summittour/IMGDB/000577/000764/000949/00015383.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=LAXHXP08B" target="_top">美西四城~大峽谷.洛杉磯.拉斯維加斯.拉芙琳.聖地牙哥空軍一號8天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190101B' target='_top'>1/1</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190107B' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190109B' target='_top'>1/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190212B' target='_top'>2/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190218B' target='_top'>2/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190220B' target='_top'>2/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190225A' target='_top'>2/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190227C' target='_top'>2/27</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=LAXHXP08B" target="_top">...more</a></div>
				<div class="product_price"><span>$36,900起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=LAXHXP08D" target="_top"><img src="/eWeb_summittour/IMGDB/000577/000764/000949/00013080.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=LAXHXP08D" target="_top">美西三城~壯麗奇景大峽谷國家公園.洛杉磯.拉斯維加斯.拉芙琳.環球影城8天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190101A' target='_top'>1/1</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190107A' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190109A' target='_top'>1/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190212A' target='_top'>2/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190218A' target='_top'>2/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190220A' target='_top'>2/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190225B' target='_top'>2/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190227B' target='_top'>2/27</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=LAXHXP08D" target="_top">...more</a></div>
				<div class="product_price"><span>$39,900起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=LAXHXP08E" target="_top"><img src="/eWeb_summittour/IMGDB/000577/000764/000949/00010380.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=LAXHXP08E" target="_top">期間限定～洛杉磯、拉斯維加斯、聖地牙哥海景火車、藍瓶咖啡8天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190115A' target='_top'>1/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190304C' target='_top'>3/4</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=LAXHXP08E" target="_top">...more</a></div>
				<div class="product_price"><span>$28,888起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008846.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=LAXHXP10B" target="_top"><img src="/eWeb_summittour/IMGDB/000577/000764/000949/00014533.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=LAXHXP10B" target="_top">4321走一回!!四大國家公園~羚羊峽谷+馬蹄灣10天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190102A' target='_top'>1/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190116A' target='_top'>1/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190213A' target='_top'>2/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190227A' target='_top'>2/27</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190313A' target='_top'>3/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190327A' target='_top'>3/27</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=LAXHXP10B" target="_top">...more</a></div>
				<div class="product_price"><span>$56,900起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=LAXHXP14A" target="_top"><img src="/eWeb_summittour/IMGDB/000577/000764/000949/00014610.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=LAXHXP14A" target="_top">LUCKY7!!七大國家公園~羚羊峽谷+馬蹄灣14天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190513A' target='_top'>5/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190527A' target='_top'>5/27</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190603A' target='_top'>6/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190610A' target='_top'>6/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190624A' target='_top'>6/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190708A' target='_top'>7/8</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190722A' target='_top'>7/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190805A' target='_top'>8/5</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=LAXHXP14A" target="_top">...more</a></div>
				<div class="product_price"><span>$99,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
		</div>
		<div class="title_box" >
			<div class="left_box" id="SAREA00081">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>洛杉磯/舊金山</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SFOLAXHXP10A" target="_top"><img src="/eWeb_summittour/IMGDB/000577/000764/000949/00013077.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SFOLAXHXP10A" target="_top">愛玩美西五城~17哩海岸.西峽谷.環球影城10天SFO進</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=SFOLA190101A' target='_top'>1/1</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOLA190107A' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOLA190212A' target='_top'>2/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOLA190218A' target='_top'>2/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOLA190225A' target='_top'>2/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOLA190311A' target='_top'>3/11</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOLA190325A' target='_top'>3/25</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=SFOLAXHXP10A" target="_top">...more</a></div>
				<div class="product_price"><span>$54,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SFOLAXHXP10B" target="_top"><img src="/eWeb_summittour/IMGDB/000577/000764/000949/00015177.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SFOLAXHXP10B" target="_top">享玩美西五城~大峽谷.17哩海岸.環球影城.空軍一號10天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=SFOLA190101B' target='_top'>1/1</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOLA190107B' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOLA190212B' target='_top'>2/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOLA190218B' target='_top'>2/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOLA190225B' target='_top'>2/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOLA190311B' target='_top'>3/11</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOLA190325B' target='_top'>3/25</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=SFOLAXHXP10B" target="_top">...more</a></div>
				<div class="product_price"><span>$51,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
		</div>
		<div class="title_box" >
			<div class="left_box" id="SAREA00073">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>溫哥華</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=YVRACP08B" target="_top"><img src="/eWeb_summittour/IMGDB/000577/000770/001214/00017600.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=YVRACP08B" target="_top">溫哥華、洛磯山脈8日～雙飛+雙城堡酒店</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=YVRAC190112A' target='_top'>1/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=YVRAC190216A' target='_top'>2/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=YVRAC190309A' target='_top'>3/9</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=YVRACP08B" target="_top">...more</a></div>
				<div class="product_price"><span>$59,800起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img alt="" height="21" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png" width="61" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=YVRACP08C" target="_top"><img src="/eWeb_summittour/IMGDB/000577/000770/001214/00017720.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=YVRACP08C" target="_top">【探索極光】冬季加拿大黃刀鎮8日~3晚黃刀鎮賞極光、狗拉雪橇</a></div>
 				<div class="product_description">★兩人同行第二人減6000元★</div> 
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=YVRAC190119A' target='_top'>1/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=YVRAC190223A' target='_top'>2/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=YVRAC190316A' target='_top'>3/16</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=YVRACP08C" target="_top">...more</a></div>
				<div class="product_price"><span>$82,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
		</div>
		<div class="title_box" >
			<div class="left_box" id="SAREA00067">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>舊金山</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SFOHXP08B" target="_top"><img src="/eWeb_summittour/IMGDB/000577/000765/001230/00015117.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SFOHXP08B" target="_top">漫步北加州8天-入住舊金山市中心2晚  納帕品酒.太浩湖美景.沙加緬度老城.賭城雷諾.APPLE PARK</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=SFOHX190102B' target='_top'>1/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOHX190116B' target='_top'>1/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOHX190213B' target='_top'>2/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOHX190227B' target='_top'>2/27</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOHX190313B' target='_top'>3/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOHX190327B' target='_top'>3/27</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=SFOHXP08B" target="_top">...more</a></div>
				<div class="product_price"><span>$47,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008851.png"/><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SFOHXP08C" target="_top"><img src="/eWeb_summittour/IMGDB/000577/000765/001230/00013060.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SFOHXP08C" target="_top">遇見北加州8天-舊金山經典市區全覽(缪爾紅木公園.蘇莎利托.蝴蝶鎮.17哩.APPLE PARK)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=SFOHX190102A' target='_top'>1/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOHX190116A' target='_top'>1/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOHX190213A' target='_top'>2/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOHX190227A' target='_top'>2/27</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOHX190313A' target='_top'>3/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOHX190327A' target='_top'>3/27</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=SFOHXP08C" target="_top">...more</a></div>
				<div class="product_price"><span>$45,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008851.png"/><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /></p></div>
			</div>
		</div>
	</div>
</body>
</html>

﻿<%
'//===========================================================================================
'//						index.asp
'//-------------------------------------------------------------------------------------------
'//		description		: 此頁面係由自動產生因此不用納入版本
'//-------------------------------------------------------------------------------------------
Response.CacheControl = "no-cache" 
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="robots" content="noindex">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
	<title>國外團體旅遊：北海道(札幌、函館、旭川)旅遊行程 - 超悠</title>
	<!--產出時間:2018/12/4 下午 11:01:26 / WEB:summittour-->
	<link href='/eweb_summittour/css/reset.css' rel='stylesheet' type='text/css'>
	<link href='/eweb_summittour/css/autopage.css' rel='stylesheet' type='text/css'>
	<script languege="javascript" src="/script/jquery.js"></script>
	<script languege="javascript" src="/eweb_summittour/JS/autopage.js"></script>
</head>
<body>
	<div class="metropolis">
		<div class="title_box" >
			<div class="left_box" id="SAREA00020">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>北海道(札幌、函館、旭川)</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTSCIP5D" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000557/000559/00007942.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTSCIP5D" target="_top">【浪漫北海道】百萬夜景 小丑漢堡 浪漫小樽物語 五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CTSCI181220A' target='_top'>12/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTSCI190106A' target='_top'>1/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTSCI190110A' target='_top'>1/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTSCI190113A' target='_top'>1/13</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CTSCIP5D" target="_top">...more</a></div>
				<div class="product_price"><span>$29,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTSCIP5E" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000557/000559/00010753.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTSCIP5E" target="_top">【銀色北海道】流冰奇航 雪上活動 螃蟹美食五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CTSCI190120A' target='_top'>1/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTSCI190124A' target='_top'>1/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTSCI190127A' target='_top'>1/27</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTSCI190131A' target='_top'>1/31</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTSCI190214A' target='_top'>2/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTSCI190217A' target='_top'>2/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTSCI190221A' target='_top'>2/21</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTSCI190224A' target='_top'>2/24</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CTSCIP5E" target="_top">...more</a></div>
				<div class="product_price"><span>$38,800起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTSCIP5F" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000557/000559/00017783.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTSCIP5F" target="_top">【新春北海道】流冰奇航 雪上活動 溫泉饗宴五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CTSCI190205A' target='_top'>2/5</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CTSCIP5F" target="_top">...more</a></div>
				<div class="product_price"><span>$64,800起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011383.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTSMMP5A" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000557/000559/00015057.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTSMMP5A" target="_top">【悠閒時光】北海道雪國祭典~道東阿寒湖、層雲峽、破冰船、玩雪四合一、三溫泉5日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CTSMM190206A' target='_top'>2/6</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CTSMMP5A" target="_top">...more</a></div>
				<div class="product_price"><span>$61,800起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011383.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTSMMP6A" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000557/000559/00017850.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTSMMP6A" target="_top">【悠閒時光】北海道雪國祭典~道東阿寒湖、層雲峽、破冰船、玩雪四合一、三溫泉6日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CTSMM190203A' target='_top'>2/3</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CTSMMP6A" target="_top">...more</a></div>
				<div class="product_price"><span>$61,800起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img alt="" height="21" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011383.png" width="61" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=HKDBRP05B" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000557/001166/00015776.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=HKDBRP05B" target="_top">北海道雪在燒5日(百萬夜景.北極星觀景列車.雪樂園.摩天輪.雙纜車.三大螃蟹)-函館/千歲</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=HKDBR181214A' target='_top'>12/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=HKDBR181217A' target='_top'>12/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=HKDBR181221A' target='_top'>12/21</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=HKDBRP05B" target="_top">...more</a></div>
				<div class="product_price"><span>$32,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
		</div>
	</div>
</body>
</html>

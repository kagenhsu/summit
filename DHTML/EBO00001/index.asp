﻿<%
'//===========================================================================================
'//						index.asp
'//-------------------------------------------------------------------------------------------
'//		description		: 此頁面係由自動產生因此不用納入版本
'//-------------------------------------------------------------------------------------------
Response.CacheControl = "no-cache" 
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="robots" content="noindex">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
	<title>國外團體旅遊：中南半島旅遊行程 - 超悠</title>
	<!--產出時間:2018/12/4 下午 11:01:48 / WEB:summittour-->
	<link href='/eweb_summittour/css/reset.css' rel='stylesheet' type='text/css'>
	<link href='/eweb_summittour/css/autopage.css' rel='stylesheet' type='text/css'>
	<script languege="javascript" src="/script/jquery.js"></script>
	<script languege="javascript" src="/eweb_summittour/JS/autopage.js"></script>
</head>
<body>
	<div class="metropolis">
		<div class="title_box" >
			<div class="left_box" id="DAREA00010">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>歐洲</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SVOSUP10A" target="_top"><img src="/eWeb_summittour/IMGDB/000432/001367/001370/00016939.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SVOSUP10A" target="_top">俄羅斯雙都、莫曼斯克四晚極光圈十日</a></div>
 				<div class="product_description">★早鳥優惠2000元★</div> 
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=SVOSU181222A' target='_top'>12/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SVOSU181229A' target='_top'>12/29</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SVOSU190105A' target='_top'>1/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SVOSU190112A' target='_top'>1/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SVOSU190118A' target='_top'>1/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SVOSU190126A' target='_top'>1/26</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SVOSU190202A' target='_top'>2/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SVOSU190208A' target='_top'>2/8</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=SVOSUP10A" target="_top">...more</a></div>
				<div class="product_price"><span>$110,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
		</div>
		<div class="title_box" >
			<div class="left_box" id="DAREA00014">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>美加</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=LAXMIAHXP10A" target="_top"><img src="/eWeb_summittour/IMGDB/000577/001055/001058/00012986.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=LAXMIAHXP10A" target="_top">神祕古巴華麗的冒險10天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=LAXMI190111B' target='_top'>1/11</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXMI190215B' target='_top'>2/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXMI190315B' target='_top'>3/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXMI190412B' target='_top'>4/12</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=LAXMIAHXP10A" target="_top">...more</a></div>
				<div class="product_price"><span>$99,900起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.PNG	" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=LAXMIAHXP12A" target="_top"><img src="/eWeb_summittour/IMGDB/000577/001055/001058/00015018.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=LAXMIAHXP12A" target="_top">情迷醉人烏托邦~古巴深度12天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=LAXMI190111A' target='_top'>1/11</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXMI190215A' target='_top'>2/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXMI190315A' target='_top'>3/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXMI190412A' target='_top'>4/12</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=LAXMIAHXP12A" target="_top">...more</a></div>
				<div class="product_price"><span>$139,900起</span></div>
   				<div class="product_offer"><p><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00015107.png"/><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.PNG	" width="60" /></p></div>
			</div>
		</div>
	</div>
</body>
</html>

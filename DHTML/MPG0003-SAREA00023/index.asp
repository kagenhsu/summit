﻿<%
'//===========================================================================================
'//						index.asp
'//-------------------------------------------------------------------------------------------
'//		description		: 此頁面係由自動產生因此不用納入版本
'//-------------------------------------------------------------------------------------------
Response.CacheControl = "no-cache" 
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="robots" content="noindex">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
	<title>國外團體旅遊：九州(大分、福岡、宮崎、鹿兒島、熊本)旅遊行程 - 超悠</title>
	<!--產出時間:2018/12/4 下午 11:01:25 / WEB:summittour-->
	<link href='/eweb_summittour/css/reset.css' rel='stylesheet' type='text/css'>
	<link href='/eweb_summittour/css/autopage.css' rel='stylesheet' type='text/css'>
	<script languege="javascript" src="/script/jquery.js"></script>
	<script languege="javascript" src="/eweb_summittour/JS/autopage.js"></script>
</head>
<body>
	<div class="metropolis">
		<div class="title_box" >
			<div class="left_box" id="SAREA00023">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>九州(大分、福岡、宮崎、鹿兒島、熊本)</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=FUKBRP05A" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000569/000603/00015059.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=FUKBRP05A" target="_top">輕遊九州鐵道雙溫泉五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=FUKBR181223A' target='_top'>12/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=FUKBR190107A' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=FUKBR190129A' target='_top'>1/29</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=FUKBR190215A' target='_top'>2/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=FUKBR190218A' target='_top'>2/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=FUKBR190222A' target='_top'>2/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=FUKBR190304A' target='_top'>3/4</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=FUKBR190318A' target='_top'>3/18</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=FUKBRP05A" target="_top">...more</a></div>
				<div class="product_price"><span>$27,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=FUKCIP5B" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000569/000603/00017346.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=FUKCIP5B" target="_top">【戲雪九州】旅人電車 和服體驗 長崎纜車五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=FUKCI181218A' target='_top'>12/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=FUKCI181225A' target='_top'>12/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=FUKCI190103A' target='_top'>1/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=FUKCI190108A' target='_top'>1/8</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=FUKCI190110A' target='_top'>1/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=FUKCI190115A' target='_top'>1/15</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=FUKCIP5B" target="_top">...more</a></div>
				<div class="product_price"><span>$27,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=FUKCIP5C" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000569/000603/00015775.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=FUKCIP5C" target="_top">【FUN寒假】豪斯登堡 旅人電車 和服體驗 五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=FUKCI190118A' target='_top'>1/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=FUKCI190122A' target='_top'>1/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=FUKCI190125A' target='_top'>1/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=FUKCI190129A' target='_top'>1/29</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=FUKCIP5C" target="_top">...more</a></div>
				<div class="product_price"><span>$34,800起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
		</div>
	</div>
</body>
</html>

﻿<%
'//===========================================================================================
'//						index.asp
'//-------------------------------------------------------------------------------------------
'//		description		: 此頁面係由自動產生因此不用納入版本
'//-------------------------------------------------------------------------------------------
Response.CacheControl = "no-cache" 
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="robots" content="noindex">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
	<title>國外團體旅遊：北陸黑部立山(名古屋、富山、小松、靜岡)旅遊行程 - 超悠</title>
	<!--產出時間:2018/12/4 下午 11:01:26 / WEB:summittour-->
	<link href='/eweb_summittour/css/reset.css' rel='stylesheet' type='text/css'>
	<link href='/eweb_summittour/css/autopage.css' rel='stylesheet' type='text/css'>
	<script languege="javascript" src="/script/jquery.js"></script>
	<script languege="javascript" src="/eweb_summittour/JS/autopage.js"></script>
</head>
<body>
	<div class="metropolis">
		<div class="title_box" >
			<div class="left_box" id="SAREA00022">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>北陸黑部立山(名古屋、富山、小松、靜岡)</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=NGOCIP5D" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000565/000590/00017337.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=NGOCIP5D" target="_top">【戲雪名古屋】穗高纜車 童話合掌村 玩雪去 五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=NGOCI190104A' target='_top'>1/4</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NGOCI190106A' target='_top'>1/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NGOCI190111A' target='_top'>1/11</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NGOCI190113A' target='_top'>1/13</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=NGOCIP5D" target="_top">...more</a></div>
				<div class="product_price"><span>$28,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=NGOCIP5E" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000565/000590/00015906.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=NGOCIP5E" target="_top">【FUN寒假】樂高樂園 童話合掌村 玩雪趣 五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=NGOCI190116A' target='_top'>1/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NGOCI190123A' target='_top'>1/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NGOCI190130A' target='_top'>1/30</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=NGOCIP5E" target="_top">...more</a></div>
				<div class="product_price"><span>$36,800起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=NGOCXP5A" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000565/000590/00017337.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=NGOCXP5A" target="_top">【悠閒時光】雪白北陸童話村~白川鄉合掌村、絕景星空、兼六園、戲雪樂、三溫泉5日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=NGOCX190129A' target='_top'>1/29</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=NGOCXP5A" target="_top">...more</a></div>
				<div class="product_price"><span>$36,800起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=NRTCIP5B" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000565/000590/00017804.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=NRTCIP5B" target="_top">【浪漫花語】繽紛舞曲~足立藤花 粉蝶花海 粉紅芝櫻五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=NRTCI190416A' target='_top'>4/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NRTCI190418A' target='_top'>4/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NRTCI190420A' target='_top'>4/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NRTCI190422A' target='_top'>4/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NRTCI190511A' target='_top'>5/11</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NRTCI190513A' target='_top'>5/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NRTCI190515A' target='_top'>5/15</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=NRTCIP5B" target="_top">...more</a></div>
				<div class="product_price"><span>$36,900起</span></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=NRTCIP6A" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000565/000590/00014218.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=NRTCIP6A" target="_top">【雪壁絕景~立山黑部】童話合掌村 藤花伴芝櫻六日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=NRTCI190415A' target='_top'>4/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NRTCI190417A' target='_top'>4/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NRTCI190419A' target='_top'>4/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NRTCI190421A' target='_top'>4/21</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NRTCI190510A' target='_top'>5/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NRTCI190512A' target='_top'>5/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NRTCI190514A' target='_top'>5/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NRTCI190516A' target='_top'>5/16</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=NRTCIP6A" target="_top">...more</a></div>
				<div class="product_price"><span>$39,800起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
		</div>
	</div>
</body>
</html>

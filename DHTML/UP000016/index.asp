﻿<%
'//===========================================================================================
'//						index.asp
'//-------------------------------------------------------------------------------------------
'//		description		: 此頁面係由自動產生因此不用納入版本
'//-------------------------------------------------------------------------------------------
Response.CacheControl = "no-cache" 
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="robots" content="noindex">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
	<title>國外團體旅遊：東北亞旅遊行程 - 超悠</title>
	<!--產出時間:2018/12/4 下午 11:02:01 / WEB:summittour-->
	<link href='/eweb_summittour/css/reset.css' rel='stylesheet' type='text/css'>
	<link href='/eweb_summittour/css/autopage.css' rel='stylesheet' type='text/css'>
	<script languege="javascript" src="/script/jquery.js"></script>
	<script languege="javascript" src="/eweb_summittour/JS/autopage.js"></script>
</head>
<body>
	<div class="metropolis">
		<div class="title_box" >
			<div class="left_box" id="DAREA00004">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>東南亞</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=BKIMH5A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000620/000646/00011771.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=BKIMH5A" target="_top">沙巴沙很大5日~海洋公園逍遙遊、生態雨林長鼻猴、精油SPA</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190104A' target='_top'>1/4</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190106A' target='_top'>1/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190111A' target='_top'>1/11</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190113A' target='_top'>1/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190118A' target='_top'>1/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190120A' target='_top'>1/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190125A' target='_top'>1/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190127A' target='_top'>1/27</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=BKIMH5A" target="_top">...more</a></div>
				<div class="product_price"><span>$19,300起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBBR1805B" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00015885.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBBR1805B" target="_top">宿霧雙島合一：南方夢幻海域一島一酒店五日遊</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181219A' target='_top'>12/19</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CEBBR1805B" target="_top">...more</a></div>
				<div class="product_price"><span>$28,500起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
		</div>
	</div>
</body>
</html>

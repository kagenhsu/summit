﻿<%
'//===========================================================================================
'//						index.asp
'//-------------------------------------------------------------------------------------------
'//		description		: 此頁面係由自動產生因此不用納入版本
'//-------------------------------------------------------------------------------------------
Response.CacheControl = "no-cache" 
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="robots" content="noindex">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
	<title>國外團體旅遊：巴淡島旅遊行程 - 超悠</title>
	<!--產出時間:2017/7/25 下午 11:00:52 / WEB:summittour-->
	<link href='/eweb_summittour/css/reset.css' rel='stylesheet' type='text/css'>
	<link href='/eweb_summittour/css/autopage.css' rel='stylesheet' type='text/css'>
	<script languege="javascript" src="/script/jquery.js"></script>
	<script languege="javascript" src="/eweb_summittour/JS/autopage.js"></script>
</head>
<body>
	<div class="metropolis">
		<div class="title_box" >
			<div class="left_box" id="SAREA00029">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>巴淡島</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"></div>
				<div class="product_name"><a href="/eWeb/GO/L_GO_Type.asp?iSUB_CD=GO&STYLE_TP=2&TWFG=N&iMGRUP_CD=BKIMHPT5J" target="_top">飛去遊沙巴</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=BKIMHPT5J&GRUP_CD=BKIMHPT7802J&SUB_CD=GO&JOIN_TP=' target='_top'>8/2</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=BKIMHPT5J&GRUP_CD=BKIMHPT7805J&SUB_CD=GO&JOIN_TP=' target='_top'>8/5</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=BKIMHPT5J&GRUP_CD=BKIMHPT7806J&SUB_CD=GO&JOIN_TP=' target='_top'>8/6</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=BKIMHPT5J&GRUP_CD=BKIMHPT7809J&SUB_CD=GO&JOIN_TP=' target='_top'>8/9</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=BKIMHPT5J&GRUP_CD=BKIMHPT7811J&SUB_CD=GO&JOIN_TP=' target='_top'>8/11</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=BKIMHPT5J&GRUP_CD=BKIMHPT7813J&SUB_CD=GO&JOIN_TP=' target='_top'>8/13</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=BKIMHPT5J&GRUP_CD=BKIMHPT7816J&SUB_CD=GO&JOIN_TP=' target='_top'>8/16</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=BKIMHPT5J&GRUP_CD=BKIMHPT7820J&SUB_CD=GO&JOIN_TP=' target='_top'>8/20</a>
					<a href="/eWeb/GO/L_GO_Type.asp?iSUB_CD=GO&STYLE_TP=2&TWFG=N&iMGRUP_CD=BKIMHPT5J" target="_top">...more</a></div>
				<div class="product_price"><span>$19,500起</span></div>
			</div>
    		<div class="product_box">
				<div class="product_img"></div>
				<div class="product_name"><a href="/eWeb/GO/L_GO_Type.asp?iSUB_CD=GO&STYLE_TP=2&TWFG=N&iMGRUP_CD=BKIODPT5A" target="_top">樂情沙巴五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=BKIODPT5A&GRUP_CD=BKIODP7103A&SUB_CD=GO&JOIN_TP=' target='_top'>10/3</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=BKIODPT5A&GRUP_CD=BKIODP7107A&SUB_CD=GO&JOIN_TP=' target='_top'>10/7</a>
					<a href="/eWeb/GO/L_GO_Type.asp?iSUB_CD=GO&STYLE_TP=2&TWFG=N&iMGRUP_CD=BKIODPT5A" target="_top">...more</a></div>
				<div class="product_price"><span>$22,500起</span></div>
			</div>
    		<div class="product_box">
				<div class="product_img"></div>
				<div class="product_name"><a href="/eWeb/GO/L_GO_Type.asp?iSUB_CD=GO&STYLE_TP=2&TWFG=N&iMGRUP_CD=BKIODPT5B" target="_top">愛戀沙巴5日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=BKIODPT5B&GRUP_CD=BKIODP7902B&SUB_CD=GO&JOIN_TP=' target='_top'>9/2</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=BKIODPT5B&GRUP_CD=BKIODP7905B&SUB_CD=GO&JOIN_TP=' target='_top'>9/5</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=BKIODPT5B&GRUP_CD=BKIODP7912B&SUB_CD=GO&JOIN_TP=' target='_top'>9/12</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=BKIODPT5B&GRUP_CD=BKIODP7916B&SUB_CD=GO&JOIN_TP=' target='_top'>9/16</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=BKIODPT5B&GRUP_CD=BKIODP7919B&SUB_CD=GO&JOIN_TP=' target='_top'>9/19</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=BKIODPT5B&GRUP_CD=BKIODP7926B&SUB_CD=GO&JOIN_TP=' target='_top'>9/26</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=BKIODPT5B&GRUP_CD=BKIODP7930B&SUB_CD=GO&JOIN_TP=' target='_top'>9/30</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=BKIODPT5B&GRUP_CD=BKIODP71010B&SUB_CD=GO&JOIN_TP=' target='_top'>10/10</a>
					<a href="/eWeb/GO/L_GO_Type.asp?iSUB_CD=GO&STYLE_TP=2&TWFG=N&iMGRUP_CD=BKIODPT5B" target="_top">...more</a></div>
				<div class="product_price"><span>$23,800起</span></div>
			</div>
    		<div class="product_box">
				<div class="product_img"></div>
				<div class="product_name"><a href="/eWeb/GO/L_GO_Type.asp?iSUB_CD=GO&STYLE_TP=2&TWFG=N&iMGRUP_CD=BKIODPT5D" target="_top">醉美沙巴五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=BKIODPT5D&GRUP_CD=BKIODPT7824D&SUB_CD=GO&JOIN_TP=' target='_top'>8/24</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=BKIODPT5D&GRUP_CD=BKIODP7103D&SUB_CD=GO&JOIN_TP=' target='_top'>10/3</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=BKIODPT5D&GRUP_CD=BKIODP7107D&SUB_CD=GO&JOIN_TP=' target='_top'>10/7</a>
					<a href="/eWeb/GO/L_GO_Type.asp?iSUB_CD=GO&STYLE_TP=2&TWFG=N&iMGRUP_CD=BKIODPT5D" target="_top">...more</a></div>
				<div class="product_price"><span>$23,000起</span></div>
			</div>
    		<div class="product_box">
				<div class="product_img"></div>
				<div class="product_name"><a href="/eWeb/GO/L_GO_Type.asp?iSUB_CD=GO&STYLE_TP=2&TWFG=N&iMGRUP_CD=BKIODPT5F" target="_top">牽手沙巴五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=BKIODPT5F&GRUP_CD=BKIODPT7824F&SUB_CD=GO&JOIN_TP=' target='_top'>8/24</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=BKIODPT5F&GRUP_CD=BKIODP7902F&SUB_CD=GO&JOIN_TP=' target='_top'>9/2</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=BKIODPT5F&GRUP_CD=BKIODP7905F&SUB_CD=GO&JOIN_TP=' target='_top'>9/5</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=BKIODPT5F&GRUP_CD=BKIODP7912F&SUB_CD=GO&JOIN_TP=' target='_top'>9/12</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=BKIODPT5F&GRUP_CD=BKIODP7916F&SUB_CD=GO&JOIN_TP=' target='_top'>9/16</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=BKIODPT5F&GRUP_CD=BKIODP7919F&SUB_CD=GO&JOIN_TP=' target='_top'>9/19</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=BKIODPT5F&GRUP_CD=BKIODP7926F&SUB_CD=GO&JOIN_TP=' target='_top'>9/26</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=BKIODPT5F&GRUP_CD=BKIODP7930F&SUB_CD=GO&JOIN_TP=' target='_top'>9/30</a>
					<a href="/eWeb/GO/L_GO_Type.asp?iSUB_CD=GO&STYLE_TP=2&TWFG=N&iMGRUP_CD=BKIODPT5F" target="_top">...more</a></div>
				<div class="product_price"><span>$16,900起</span></div>
			</div>
		</div>
	</div>
</body>
</html>

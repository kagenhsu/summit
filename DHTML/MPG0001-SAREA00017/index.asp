﻿<%
'//===========================================================================================
'//						index.asp
'//-------------------------------------------------------------------------------------------
'//		description		: 此頁面係由自動產生因此不用納入版本
'//-------------------------------------------------------------------------------------------
Response.CacheControl = "no-cache" 
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="robots" content="noindex">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
	<title>國外團體旅遊：絲路、甘肅旅遊行程 - 超悠</title>
	<!--產出時間:2017/7/25 下午 11:00:51 / WEB:summittour-->
	<link href='/eweb_summittour/css/reset.css' rel='stylesheet' type='text/css'>
	<link href='/eweb_summittour/css/autopage.css' rel='stylesheet' type='text/css'>
	<script languege="javascript" src="/script/jquery.js"></script>
	<script languege="javascript" src="/eweb_summittour/JS/autopage.js"></script>
</head>
<body>
	<div class="metropolis">
		<div class="title_box" >
			<div class="left_box" id="SAREA00017">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>絲路、甘肅</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"></div>
				<div class="product_name"><a href="/eWeb/GO/L_GO_Type.asp?iSUB_CD=GO&STYLE_TP=2&TWFG=N&iMGRUP_CD=URC12-1" target="_top">東方航空─北疆禾木村那拉提巴音布魯克博斯騰湖12日</a></div>
				<div class="departure_date">
					出發日期：
					<s><a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=URC12-1&GRUP_CD=URCMUP7810A&SUB_CD=GO&JOIN_TP=' target='_top'>8/10</a></s> , 
			<s><a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=URC12-1&GRUP_CD=URCMUP7817A&SUB_CD=GO&JOIN_TP=' target='_top'>8/17</a></s> , 
			<s><a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=URC12-1&GRUP_CD=URCMUP7O12A&SUB_CD=GO&JOIN_TP=' target='_top'>10/12</a></s>
					<a href="/eWeb/GO/L_GO_Type.asp?iSUB_CD=GO&STYLE_TP=2&TWFG=N&iMGRUP_CD=URC12-1" target="_top">...more</a></div>
				<div class="product_price"><span>$54,500起</span></div>
			</div>
    		<div class="product_box">
				<div class="product_img"></div>
				<div class="product_name"><a href="/eWeb/GO/L_GO_Type.asp?iSUB_CD=GO&STYLE_TP=2&TWFG=N&iMGRUP_CD=URC1201" target="_top">游遍中國─北疆可可托禾木村巴音布魯克天鵝湖庫爾勒12天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=URC1201&GRUP_CD=URC12T7909A&SUB_CD=GO&JOIN_TP=' target='_top'>9/9</a>
					<a href="/eWeb/GO/L_GO_Type.asp?iSUB_CD=GO&STYLE_TP=2&TWFG=N&iMGRUP_CD=URC1201" target="_top">...more</a></div>
				<div class="product_price"><span>$68,900起</span></div>
			</div>
		</div>
	</div>
</body>
</html>

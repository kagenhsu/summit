﻿<%
'//===========================================================================================
'//						index.asp
'//-------------------------------------------------------------------------------------------
'//		description		: 此頁面係由自動產生因此不用納入版本
'//-------------------------------------------------------------------------------------------
Response.CacheControl = "no-cache" 
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="robots" content="noindex">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
	<title>國外團體旅遊：海南島旅遊行程 - 超悠</title>
	<!--產出時間:2017/7/25 下午 11:00:51 / WEB:summittour-->
	<link href='/eweb_summittour/css/reset.css' rel='stylesheet' type='text/css'>
	<link href='/eweb_summittour/css/autopage.css' rel='stylesheet' type='text/css'>
	<script languege="javascript" src="/script/jquery.js"></script>
	<script languege="javascript" src="/eweb_summittour/JS/autopage.js"></script>
</head>
<body>
	<div class="metropolis">
		<div class="title_box" >
			<div class="left_box" id="SAREA00014">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>海南島</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/eWeb/GO/L_GO_Type.asp?iSUB_CD=GO&STYLE_TP=2&TWFG=N&iMGRUP_CD=HAKGSPR4A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000085/000547/00007711.JPG" /></a></div>
				<div class="product_name"><a href="/eWeb/GO/L_GO_Type.asp?iSUB_CD=GO&STYLE_TP=2&TWFG=N&iMGRUP_CD=HAKGSPR4A" target="_top">傳奇海南─海口興隆熱帶植物花園定安熱帶飛禽世界超值四日</a></div>
 				<div class="product_description">買一送一，稅外3000/人</div> 
				<div class="departure_date">
					出發日期：
					<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=HAKGSPR4A&GRUP_CD=HAKGSP47824D&SUB_CD=GO&JOIN_TP=' target='_top'>8/24</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=HAKGSPR4A&GRUP_CD=HAKGSP47831D&SUB_CD=GO&JOIN_TP=' target='_top'>8/31</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=HAKGSPR4A&GRUP_CD=HAKGSP47907D&SUB_CD=GO&JOIN_TP=' target='_top'>9/7</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=HAKGSPR4A&GRUP_CD=HAKGSP47914D&SUB_CD=GO&JOIN_TP=' target='_top'>9/14</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=HAKGSPR4A&GRUP_CD=HAKGSP47921D&SUB_CD=GO&JOIN_TP=' target='_top'>9/21</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=HAKGSPR4A&GRUP_CD=HAKGSP47928D&SUB_CD=GO&JOIN_TP=' target='_top'>9/28</a>
					<a href="/eWeb/GO/L_GO_Type.asp?iSUB_CD=GO&STYLE_TP=2&TWFG=N&iMGRUP_CD=HAKGSPR4A" target="_top">...more</a></div>
				<div class="product_price"><span>$12,900起</span></div>
   				<div class="product_offer"><p><span style="color:#FFFFFF;"><span style="background-color:#FF8C00;">台中出發</span></span></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/eWeb/GO/L_GO_Type.asp?iSUB_CD=GO&STYLE_TP=2&TWFG=N&iMGRUP_CD=HAKGSPR5C" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000085/000547/00007704.JPG" /></a></div>
				<div class="product_name"><a href="/eWeb/GO/L_GO_Type.asp?iSUB_CD=GO&STYLE_TP=2&TWFG=N&iMGRUP_CD=HAKGSPR5C" target="_top">傳奇海南─海口保亭七仙嶺森林公園陵水椰田古寨石梅灣尊爵五天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=HAKGSPR5C&GRUP_CD=HAKGSP87813D&SUB_CD=GO&JOIN_TP=' target='_top'>8/13</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=HAKGSPR5C&GRUP_CD=HAKGSP87820D&SUB_CD=GO&JOIN_TP=' target='_top'>8/20</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=HAKGSPR5C&GRUP_CD=HAKGSP87827D&SUB_CD=GO&JOIN_TP=' target='_top'>8/27</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=HAKGSPR5C&GRUP_CD=HAKGSP87903D&SUB_CD=GO&JOIN_TP=' target='_top'>9/3</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=HAKGSPR5C&GRUP_CD=HAKGSP87910D&SUB_CD=GO&JOIN_TP=' target='_top'>9/10</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=HAKGSPR5C&GRUP_CD=HAKGSP87917D&SUB_CD=GO&JOIN_TP=' target='_top'>9/17</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=HAKGSPR5C&GRUP_CD=HAKGSP87924D&SUB_CD=GO&JOIN_TP=' target='_top'>9/24</a>
					<a href="/eWeb/GO/L_GO_Type.asp?iSUB_CD=GO&STYLE_TP=2&TWFG=N&iMGRUP_CD=HAKGSPR5C" target="_top">...more</a></div>
				<div class="product_price"><span>$17,900起</span></div>
   				<div class="product_offer"><p><span style="color:#FFFFFF;"><span style="font-family:微軟正黑體,sans-serif;"><span style="font-size:12.0pt;"><span style="background-color:#FF8C00;">台中出發</span></span></span></span></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"></div>
				<div class="product_name"><a href="/eWeb/GO/L_GO_Type.asp?iSUB_CD=GO&STYLE_TP=2&TWFG=N&iMGRUP_CD=HAKGSPR5F" target="_top">傳奇海南─海口澄邁紅樹灣濕地三亞鳳凰嶺雙秀尊爵五日</a></div>
 				<div class="product_description">稅外$3000/人</div> 
				<div class="departure_date">
					出發日期：
					<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=HAKGSPR5F&GRUP_CD=HAKGSP57730C&SUB_CD=GO&JOIN_TP=' target='_top'>7/30</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=HAKGSPR5F&GRUP_CD=HAKGSP57813C&SUB_CD=GO&JOIN_TP=' target='_top'>8/13</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=HAKGSPR5F&GRUP_CD=HAKGSP57820C&SUB_CD=GO&JOIN_TP=' target='_top'>8/20</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=HAKGSPR5F&GRUP_CD=HAKGSP57827C&SUB_CD=GO&JOIN_TP=' target='_top'>8/27</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=HAKGSPR5F&GRUP_CD=HAKGSP57903B&SUB_CD=GO&JOIN_TP=' target='_top'>9/3</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=HAKGSPR5F&GRUP_CD=HAKGSP57910B&SUB_CD=GO&JOIN_TP=' target='_top'>9/10</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=HAKGSPR5F&GRUP_CD=HAKGSP57917B&SUB_CD=GO&JOIN_TP=' target='_top'>9/17</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=HAKGSPR5F&GRUP_CD=HAKGSP57924B&SUB_CD=GO&JOIN_TP=' target='_top'>9/24</a>
					<a href="/eWeb/GO/L_GO_Type.asp?iSUB_CD=GO&STYLE_TP=2&TWFG=N&iMGRUP_CD=HAKGSPR5F" target="_top">...more</a></div>
				<div class="product_price"><span>$14,900起</span></div>
			</div>
    		<div class="product_box">
				<div class="product_img"></div>
				<div class="product_name"><a href="/eWeb/GO/L_GO_Type.asp?iSUB_CD=GO&STYLE_TP=2&TWFG=N&iMGRUP_CD=HAKGSPR5G" target="_top">傳奇海南-海口澄邁逍遙四日</a></div>
 				<div class="product_description">買一送一，稅外$3000/人</div> 
				<div class="departure_date">
					出發日期：
					<s><a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=HAKGSPR5G&GRUP_CD=HAKGSP47810C&SUB_CD=GO&JOIN_TP=' target='_top'>8/10</a></s> , 
			<s><a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=HAKGSPR5G&GRUP_CD=HAKGSP47817C&SUB_CD=GO&JOIN_TP=' target='_top'>8/17</a></s> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=HAKGSPR5G&GRUP_CD=HAKGSP47824C&SUB_CD=GO&JOIN_TP=' target='_top'>8/24</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=HAKGSPR5G&GRUP_CD=HAKGSP47831C&SUB_CD=GO&JOIN_TP=' target='_top'>8/31</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=HAKGSPR5G&GRUP_CD=HAKGSP47907A&SUB_CD=GO&JOIN_TP=' target='_top'>9/7</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=HAKGSPR5G&GRUP_CD=HAKGSP47914A&SUB_CD=GO&JOIN_TP=' target='_top'>9/14</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=HAKGSPR5G&GRUP_CD=HAKGSP47921A&SUB_CD=GO&JOIN_TP=' target='_top'>9/21</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=HAKGSPR5G&GRUP_CD=HAKGSP47928A&SUB_CD=GO&JOIN_TP=' target='_top'>9/28</a>
					<a href="/eWeb/GO/L_GO_Type.asp?iSUB_CD=GO&STYLE_TP=2&TWFG=N&iMGRUP_CD=HAKGSPR5G" target="_top">...more</a></div>
				<div class="product_price"><span>$11,999起</span></div>
			</div>
		</div>
	</div>
</body>
</html>

﻿<%
'//===========================================================================================
'//						index.asp
'//-------------------------------------------------------------------------------------------
'//		description		: 此頁面係由自動產生因此不用納入版本
'//-------------------------------------------------------------------------------------------
Response.CacheControl = "no-cache" 
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="robots" content="noindex">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
	<title>國外團體旅遊：江南、杭州上海旅遊行程 - 超悠</title>
	<!--產出時間:2017/7/7 下午 01:52:27 / WEB:summittour-->
	<link href='/eweb_summittour/css/reset.css' rel='stylesheet' type='text/css'>
	<link href='/eweb_summittour/css/autopage.css' rel='stylesheet' type='text/css'>
	<script languege="javascript" src="/script/jquery.js"></script>
	<script languege="javascript" src="/eweb_summittour/JS/autopage.js"></script>
</head>
<body>
	<div class="metropolis">
		<!--Box_headLeft true -->
		<div class="title_box" >
			<div class="left_box" id="AUTO0029">
				<img src="/eweb_summittour/images/metropolis_title_icon_01.png"/>
				<h1>台中出發-港澳大陸</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/eWeb/GO/L_GO_Type.asp?iSUB_CD=GO&STYLE_TP=2&TWFG=N&iMGRUP_CD=NGB0601" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000084/000158/00003738.JPG" /></a></div>
				<div class="product_name"><a href="/eWeb/GO/L_GO_Type.asp?iSUB_CD=GO&STYLE_TP=2&TWFG=N&iMGRUP_CD=NGB0601" target="_top">華信假期─寧波杭州新江南新昌大佛寺穿岩十九峰烏鎮六日</a></div>
 				<div class="product_description">全程無購物</div> 
				<div class="departure_date">
					出發日期：
					<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=NGB0601&GRUP_CD=NGBAEP67721A&SUB_CD=GO&JOIN_TP=' target='_top'>7/21</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=NGB0601&GRUP_CD=NGBAEP67728A&SUB_CD=GO&JOIN_TP=' target='_top'>7/28</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=NGB0601&GRUP_CD=NGBAEP67804A&SUB_CD=GO&JOIN_TP=' target='_top'>8/4</a> , 
			<s><a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=NGB0601&GRUP_CD=NGBAEP67811A&SUB_CD=GO&JOIN_TP=' target='_top'>8/11</a></s> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=NGB0601&GRUP_CD=NGBAEP67818A&SUB_CD=GO&JOIN_TP=' target='_top'>8/18</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=NGB0601&GRUP_CD=NGBAEP67825A&SUB_CD=GO&JOIN_TP=' target='_top'>8/25</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=NGB0601&GRUP_CD=NGBAEP67901A&SUB_CD=GO&JOIN_TP=' target='_top'>9/1</a> , 
			<a href='/eWeb/GO/V_GO_Detail.asp?MGRUP_CD=NGB0601&GRUP_CD=NGBAEP67903A&SUB_CD=GO&JOIN_TP=' target='_top'>9/3</a>
					<a href="/eWeb/GO/L_GO_Type.asp?iSUB_CD=GO&STYLE_TP=2&TWFG=N&iMGRUP_CD=NGB0601" target="_top">...more</a></div>
				<div class="product_price"><span>$14,300起</span></div>
   				<div class="product_offer"><p><span style="color: rgb(255, 255, 255); font-family: Arial, 微軟正黑體, 新細明體-ExtB; font-size: 14px; background-color: rgb(255, 140, 0);">台中出發</span></p></div>
			</div>
		</div>
	</div>
</body>
</html>

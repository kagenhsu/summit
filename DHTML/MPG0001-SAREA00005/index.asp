﻿<%
'//===========================================================================================
'//						index.asp
'//-------------------------------------------------------------------------------------------
'//		description		: 此頁面係由自動產生因此不用納入版本
'//-------------------------------------------------------------------------------------------
Response.CacheControl = "no-cache" 
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="robots" content="noindex">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
	<title>國外團體旅遊：山東(濟南、青島、威海、煙台、徐州）旅遊行程 - 超悠</title>
	<!--產出時間:2018/12/4 下午 11:01:18 / WEB:summittour-->
	<link href='/eweb_summittour/css/reset.css' rel='stylesheet' type='text/css'>
	<link href='/eweb_summittour/css/autopage.css' rel='stylesheet' type='text/css'>
	<script languege="javascript" src="/script/jquery.js"></script>
	<script languege="javascript" src="/eweb_summittour/JS/autopage.js"></script>
</head>
<body>
	<div class="metropolis">
		<div class="title_box" >
			<div class="left_box" id="SAREA00005">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>山東(濟南、青島、威海、煙台、徐州）</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=TAO0803" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000073/000982/00018167.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=TAO0803" target="_top">游遍中國─歐陸青島天鵝湖童話屋泉城泰山祈福8日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=TAOCIT89206A' target='_top'>2/6</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=TAO0803" target="_top">...more</a></div>
				<div class="product_price"><span>$32,900起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=TAOCIT8A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000073/000982/00011962.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=TAOCIT8A" target="_top">華信假期─山東青島琴島之眼濟南泰山封禪秀臺兒莊八天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=TAOCIP89109A' target='_top'>1/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=TAOCIP89130A' target='_top'>1/30</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=TAOCIP89227A' target='_top'>2/27</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=TAOCIP89313A' target='_top'>3/13</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=TAOCIT8A" target="_top">...more</a></div>
				<div class="product_price"><span>$25,800起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /></p></div>
			</div>
		</div>
	</div>
</body>
</html>

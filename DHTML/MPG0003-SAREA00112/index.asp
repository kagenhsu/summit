﻿<%
'//===========================================================================================
'//						index.asp
'//-------------------------------------------------------------------------------------------
'//		description		: 此頁面係由自動產生因此不用納入版本
'//-------------------------------------------------------------------------------------------
Response.CacheControl = "no-cache" 
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="robots" content="noindex">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
	<title>國外團體旅遊：沖繩(石垣、宮古、與論、琉球、久米島)旅遊行程 - 超悠</title>
	<!--產出時間:2018/12/4 下午 11:01:26 / WEB:summittour-->
	<link href='/eweb_summittour/css/reset.css' rel='stylesheet' type='text/css'>
	<link href='/eweb_summittour/css/autopage.css' rel='stylesheet' type='text/css'>
	<script languege="javascript" src="/script/jquery.js"></script>
	<script languege="javascript" src="/eweb_summittour/JS/autopage.js"></script>
</head>
<body>
	<div class="metropolis">
		<div class="title_box" >
			<div class="left_box" id="SAREA00112">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>沖繩(石垣、宮古、與論、琉球、久米島)</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=OKAITP4B" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000567/000600/00016600.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=OKAITP4B" target="_top">沖繩兜兜風～恐龍森林公園、海洋博、萬座毛、玉泉洞四日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=OKAIT181219A' target='_top'>12/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OKAIT181222A' target='_top'>12/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OKAIT181224A' target='_top'>12/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OKAIT181226A' target='_top'>12/26</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OKAIT181231A' target='_top'>12/31</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OKAIT190105A' target='_top'>1/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OKAIT190107B' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OKAIT190109B' target='_top'>1/9</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=OKAITP4B" target="_top">...more</a></div>
				<div class="product_price"><span>$18,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=OKAITP4C" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000567/000600/00017349.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=OKAITP4C" target="_top">享戀超值沖繩~腳踏車漫遊、海洋博、首里城、瀨長島四日【超級促銷團】</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=OKAIT181219B' target='_top'>12/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OKAIT190107A' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OKAIT190109A' target='_top'>1/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OKAIT190114A' target='_top'>1/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OKAIT190116A' target='_top'>1/16</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=OKAITP4C" target="_top">...more</a></div>
				<div class="product_price"><span>$13,999起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
		</div>
	</div>
</body>
</html>

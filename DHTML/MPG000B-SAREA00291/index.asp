﻿<%
'//===========================================================================================
'//						index.asp
'//-------------------------------------------------------------------------------------------
'//		description		: 此頁面係由自動產生因此不用納入版本
'//-------------------------------------------------------------------------------------------
Response.CacheControl = "no-cache" 
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="robots" content="noindex">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
	<title>國外團體旅遊：緬甸(仰光)旅遊行程 - 超悠</title>
	<!--產出時間:2018/12/4 下午 11:01:44 / WEB:summittour-->
	<link href='/eweb_summittour/css/reset.css' rel='stylesheet' type='text/css'>
	<link href='/eweb_summittour/css/autopage.css' rel='stylesheet' type='text/css'>
	<script languege="javascript" src="/script/jquery.js"></script>
	<script languege="javascript" src="/eweb_summittour/JS/autopage.js"></script>
</head>
<body>
	<div class="metropolis">
		<div class="title_box" >
			<div class="left_box" id="SAREA00291">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>緬甸(仰光)</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=RGNCIP5A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000626/000676/00008260.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=RGNCIP5A" target="_top">緬甸黃金傳奇-仰光風情五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=RGNCI181218D' target='_top'>12/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=RGNCI181220D' target='_top'>12/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=RGNCI181222D' target='_top'>12/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=RGNCI181223E' target='_top'>12/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=RGNCI181225D' target='_top'>12/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=RGNCI181227C' target='_top'>12/27</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=RGNCI181229D' target='_top'>12/29</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=RGNCI181230E' target='_top'>12/30</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=RGNCIP5A" target="_top">...more</a></div>
				<div class="product_price"><span>$28,800起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=RGNCIP6A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000626/000676/00008261.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=RGNCIP6A" target="_top">緬甸盛情邀約-仰光+風動石六日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=RGNCI181218E' target='_top'>12/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=RGNCI181220E' target='_top'>12/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=RGNCI181221A' target='_top'>12/21</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=RGNCI181222E' target='_top'>12/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=RGNCI181223F' target='_top'>12/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=RGNCI181224C' target='_top'>12/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=RGNCI181225E' target='_top'>12/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=RGNCI181226C' target='_top'>12/26</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=RGNCIP6A" target="_top">...more</a></div>
				<div class="product_price"><span>$32,800起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=RGNCIP7A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000626/000676/00008264.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=RGNCIP7A" target="_top">緬甸行腳漫遊七日(仰光+內比都+曼德勒+浦甘)-拉車</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=RGNCI181218F' target='_top'>12/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=RGNCI181220F' target='_top'>12/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=RGNCI181222F' target='_top'>12/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=RGNCI181223G' target='_top'>12/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=RGNCI181225F' target='_top'>12/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=RGNCI181227D' target='_top'>12/27</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=RGNCI181229F' target='_top'>12/29</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=RGNCI181230G' target='_top'>12/30</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=RGNCIP7A" target="_top">...more</a></div>
				<div class="product_price"><span>$35,500起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=RGNCIP8A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000626/000676/00008265.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=RGNCIP8A" target="_top">緬甸縱橫全覽八日(仰光+浦甘+曼德勒+東芝)-四段機</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=RGNCI181219A' target='_top'>12/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=RGNCI181221B' target='_top'>12/21</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=RGNCI181223H' target='_top'>12/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=RGNCI181224D' target='_top'>12/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=RGNCI181226D' target='_top'>12/26</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=RGNCI181228D' target='_top'>12/28</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=RGNCI181230H' target='_top'>12/30</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=RGNCI181231E' target='_top'>12/31</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=RGNCIP8A" target="_top">...more</a></div>
				<div class="product_price"><span>$47,500起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /></p></div>
			</div>
		</div>
	</div>
</body>
</html>

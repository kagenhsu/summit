﻿<%
'//===========================================================================================
'//						index.asp
'//-------------------------------------------------------------------------------------------
'//		description		: 此頁面係由自動產生因此不用納入版本
'//-------------------------------------------------------------------------------------------
Response.CacheControl = "no-cache" 
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="robots" content="noindex">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
	<title>國外團體旅遊：東馬來西亞(沙巴、納閩、沙勞越)、汶萊旅遊行程 - 超悠</title>
	<!--產出時間:2018/12/4 下午 11:01:22 / WEB:summittour-->
	<link href='/eweb_summittour/css/reset.css' rel='stylesheet' type='text/css'>
	<link href='/eweb_summittour/css/autopage.css' rel='stylesheet' type='text/css'>
	<script languege="javascript" src="/script/jquery.js"></script>
	<script languege="javascript" src="/eweb_summittour/JS/autopage.js"></script>
</head>
<body>
	<div class="metropolis">
		<div class="title_box" >
			<div class="left_box" id="SAREA00060">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>東馬來西亞(沙巴、納閩、沙勞越)、汶萊</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=BKIMH05E" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000620/000646/00008439.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=BKIMH05E" target="_top">東南亞馬爾地夫~沙巴淘夢島5日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190102A' target='_top'>1/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190107A' target='_top'>1/7</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190109A' target='_top'>1/9</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190114A' target='_top'>1/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190116A' target='_top'>1/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190121A' target='_top'>1/21</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190123A' target='_top'>1/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190128A' target='_top'>1/28</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=BKIMH05E" target="_top">...more</a></div>
				<div class="product_price"><span>$16,800起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=BKIMH5A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000620/000646/00011771.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=BKIMH5A" target="_top">沙巴沙很大5日~海洋公園逍遙遊、生態雨林長鼻猴、精油SPA</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190104A' target='_top'>1/4</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190106A' target='_top'>1/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190111A' target='_top'>1/11</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190113A' target='_top'>1/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190118A' target='_top'>1/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190120A' target='_top'>1/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190125A' target='_top'>1/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190127A' target='_top'>1/27</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=BKIMH5A" target="_top">...more</a></div>
				<div class="product_price"><span>$19,300起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=BKIMH5B" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000620/000646/00014171.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=BKIMH5B" target="_top">情定沙巴生態雨林、泛舟、長鼻猴、精油SPA五日遊</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190101A' target='_top'>1/1</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190105A' target='_top'>1/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190108A' target='_top'>1/8</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190112A' target='_top'>1/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190115A' target='_top'>1/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190119A' target='_top'>1/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190122A' target='_top'>1/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190126A' target='_top'>1/26</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=BKIMH5B" target="_top">...more</a></div>
				<div class="product_price"><span>$23,500起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=BKIMH5C" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000620/000646/00014239.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=BKIMH5C" target="_top">探索沙巴、龍尾灣、瑪莉島逍遙5日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190103A' target='_top'>1/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190110A' target='_top'>1/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190117A' target='_top'>1/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190124A' target='_top'>1/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190131A' target='_top'>1/31</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190214A' target='_top'>2/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190221A' target='_top'>2/21</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190228A' target='_top'>2/28</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=BKIMH5C" target="_top">...more</a></div>
				<div class="product_price"><span>$20,800起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=BKIMHP05A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000620/000646/00017439.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=BKIMHP05A" target="_top">2019新春沙巴_麥哲倫海洋奇緣5日【豪華五星絲綢灣麥哲倫飯店+龍尾灣海角樂園七合一+淘夢島全日遊+長鼻猴&螢河遊蹤】</a></div>
				<div class="departure_date">
					出發日期：
					<s><a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190203A' target='_top'>2/3</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190205A' target='_top'>2/5</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=BKIMHP05A" target="_top">...more</a></div>
				<div class="product_price"><span>$55,100起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img alt="" height="21" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png" width="61" /><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011383.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=BKIMHP05B" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000620/000646/00008438.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=BKIMHP05B" target="_top">2019新春沙巴_海洋樂翻天4+1日【泛舟&高空滑索+婆羅洲文化村+淘夢島全日遊+長鼻猴&螢河遊蹤+飯店晚宴&開運撈生】</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190206A' target='_top'>2/6</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=BKIMHP05B" target="_top">...more</a></div>
				<div class="product_price"><span>$41,100起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img alt="" height="21" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png" width="61" /><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011383.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=BKIMHP5A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000620/000646/00010266.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=BKIMHP5A" target="_top">響亮璀璨沙巴五日(馬來西亞航空)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190107B' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190114B' target='_top'>1/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190121B' target='_top'>1/21</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190128B' target='_top'>1/28</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190211B' target='_top'>2/11</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190218B' target='_top'>2/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190225B' target='_top'>2/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190304B' target='_top'>3/4</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=BKIMHP5A" target="_top">...more</a></div>
				<div class="product_price"><span>$18,300起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
		</div>
	</div>
</body>
</html>

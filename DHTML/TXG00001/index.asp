﻿<%
'//===========================================================================================
'//						index.asp
'//-------------------------------------------------------------------------------------------
'//		description		: 此頁面係由自動產生因此不用納入版本
'//-------------------------------------------------------------------------------------------
Response.CacheControl = "no-cache" 
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="robots" content="noindex">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
	<title>國外團體旅遊：國內離島旅遊行程 - 超悠</title>
	<!--產出時間:2018/12/4 下午 11:01:49 / WEB:summittour-->
	<link href='/eweb_summittour/css/reset.css' rel='stylesheet' type='text/css'>
	<link href='/eweb_summittour/css/autopage.css' rel='stylesheet' type='text/css'>
	<script languege="javascript" src="/script/jquery.js"></script>
	<script languege="javascript" src="/eweb_summittour/JS/autopage.js"></script>
</head>
<body>
	<div class="metropolis">
		<div class="title_box" >
			<div class="left_box" id="DAREA00001">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>港澳大陸</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=HGH0502" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000258/000995/00017924.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=HGH0502" target="_top">華信假期─杭州海寧上海楓涇古鎮尊貴五天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=HGHAEP59206A' target='_top'>2/6</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=HGH0502" target="_top">...more</a></div>
				<div class="product_price"><span>$29,000起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=HGHAEPR5A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000258/000995/00008171.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=HGHAEPR5A" target="_top">華信假期-黃山奇景千島湖西遞村杭州西湖五日</a></div>
				<div class="departure_date">
					出發日期：
					<s><a href='/EW/GO/GroupDetail.asp?prodCd=HGHAEP59109A' target='_top'>1/9</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=HGHAEPR5A" target="_top">...more</a></div>
				<div class="product_price"><span>$20,500起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.PNG	" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=MFM0402" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000087/001006/00008187.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=MFM0402" target="_top">游遍中國~澳珠長隆海洋王國歡樂四日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=MFM04181209A' target='_top'>12/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=MFM04190106A' target='_top'>1/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=MFM04190113A' target='_top'>1/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=MFM04190210A' target='_top'>2/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=MFM04190217A' target='_top'>2/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=MFM04190303A' target='_top'>3/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=MFM04190310A' target='_top'>3/17</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=MFM0402" target="_top">...more</a></div>
				<div class="product_price"><span>$17,500起</span></div>
   				<div class="product_offer"><p><span style="color:#FFFFFF;"><span style="background-color:#FF8C00;">台中出發</span></span></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=NKGAET8A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000258/000995/00015205.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=NKGAET8A" target="_top">東方神起─南京揚州鹽城徐州句容尊爵逍遙八天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=NKGMUP89202A' target='_top'>2/2</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=NKGAET8A" target="_top">...more</a></div>
				<div class="product_price"><span>$29,999起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=NKGCIT8A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000258/000995/00015205.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=NKGCIT8A" target="_top">東方神起─南京無錫嘉興紹興橫店義烏烏鎮海寧句容超值八天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=NKGMUP88D22B' target='_top'>12/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NKGMUP89105B' target='_top'>1/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NKGMUP89119B' target='_top'>1/19</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=NKGMUP89302B' target='_top'>3/2</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=NKGCIT8A" target="_top">...more</a></div>
				<div class="product_price"><span>$19,900起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=NKGMUT8B" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000258/000995/00015205.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=NKGMUT8B" target="_top">東方航空─南京揚州瘦西湖鹽城丹頂鶴徐州句容尊爵八天</a></div>
				<div class="departure_date">
					出發日期：
					<s><a href='/EW/GO/GroupDetail.asp?prodCd=NKGMUP88D22A' target='_top'>12/22</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NKGMUP89112A' target='_top'>1/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NKGMUP89223A' target='_top'>2/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NKGMUP89309A' target='_top'>3/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NKGMUP89316A' target='_top'>3/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NKGMUP89323A' target='_top'>3/23</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=NKGMUT8B" target="_top">...more</a></div>
				<div class="product_price"><span>$22,000起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.PNG	" width="60" /><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008861.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR4A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000085/000547/00014888.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR4A" target="_top">海南傳奇~三亞Club Med玫瑰谷海棠廣場椰田古寨精彩四天</a></div>
				<div class="departure_date">
					出發日期：
					<s><a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP48D23C' target='_top'>12/23</a></s> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP48D30C' target='_top'>12/30</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49106C' target='_top'>1/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49113C' target='_top'>1/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49120C' target='_top'>1/20</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49127C' target='_top'>1/27</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49217C' target='_top'>2/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49224C' target='_top'>2/24</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=SYX3UPR4A" target="_top">...more</a></div>
				<div class="product_price"><span>$23,500起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.PNG	" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR4C" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000085/000547/00010269.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR4C" target="_top">海南傳奇─三亞臨春嶺森林公園陵水超值四天(買一送一)</a></div>
 				<div class="product_description">稅外$2500/人.買一送一</div> 
				<div class="departure_date">
					出發日期：
					<s><a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP48D23S' target='_top'>12/23</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=SYX3UPR4C" target="_top">...more</a></div>
				<div class="product_price"><span>$4,900起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR4D" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000085/000547/00017947.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR4D" target="_top">海南傳奇─玩翻三亞水上活動浪漫風帆精彩搖滾之旅四天</a></div>
 				<div class="product_description">送網卡</div> 
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49106R' target='_top'>1/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49113R' target='_top'>1/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49120R' target='_top'>1/20</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49127R' target='_top'>1/27</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49217R' target='_top'>2/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49224R' target='_top'>2/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49303R' target='_top'>3/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49310R' target='_top'>3/10</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=SYX3UPR4D" target="_top">...more</a></div>
				<div class="product_price"><span>$14,999起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR4E" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000085/000547/00017978.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR4E" target="_top">海南傳奇─三亞全程五星希爾頓興隆泡湯趣四日(稅外2500)</a></div>
 				<div class="product_description">稅外$2500/人</div> 
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49106H' target='_top'>1/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49113H' target='_top'>1/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49120H' target='_top'>1/20</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49127H' target='_top'>1/27</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49217H' target='_top'>2/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49224H' target='_top'>2/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49303H' target='_top'>3/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49310H' target='_top'>3/10</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=SYX3UPR4E" target="_top">...more</a></div>
				<div class="product_price"><span>$6,900起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR5B" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000085/000547/00014852.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR5B" target="_top">海南傳奇─三亞海口馮小剛電影公社興隆陵水低調奢華五天</a></div>
				<div class="departure_date">
					出發日期：
					<s><a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59206A' target='_top'>2/6</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=SYX3UPR5B" target="_top">...more</a></div>
				<div class="product_price"><span>$29,500起</span></div>
   				<div class="product_offer"><p><span style="color:#FFFFFF;"><span style="background-color:#FF8C00;">台中出發</span></span></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR5C" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000085/000547/00014852.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR5C" target="_top">海南傳奇─三亞陵水特選優值五天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59130A' target='_top'>1/30</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=SYX3UPR5C" target="_top">...more</a></div>
				<div class="product_price"><span>$19,800起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR5D" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000085/000547/00017929.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR5D" target="_top">海南傳奇─三亞Club Med陵水奢華五天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59102C' target='_top'>1/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59109C' target='_top'>1/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59116C' target='_top'>1/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59130C' target='_top'>1/30</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59213C' target='_top'>2/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59220C' target='_top'>2/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59306C' target='_top'>3/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59313C' target='_top'>3/13</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=SYX3UPR5D" target="_top">...more</a></div>
				<div class="product_price"><span>$28,300起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR5E" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000085/000547/00017949.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR5E" target="_top">海南傳奇─玩翻三亞大東海浪漫風帆萬寧石梅灣五日</a></div>
 				<div class="product_description">送網卡</div> 
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59102R' target='_top'>1/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59109R' target='_top'>1/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59116R' target='_top'>1/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59130R' target='_top'>1/30</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59213R' target='_top'>2/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59220R' target='_top'>2/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59306R' target='_top'>3/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59313R' target='_top'>3/13</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=SYX3UPR5E" target="_top">...more</a></div>
				<div class="product_price"><span>$19,999起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR5F" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000085/000547/00017977.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR5F" target="_top">海南傳奇─三亞全程五星希爾頓興隆泡湯趣五天(稅外2500)</a></div>
 				<div class="product_description">稅外$2500/人</div> 
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP58D19H' target='_top'>12/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59102H' target='_top'>1/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59109H' target='_top'>1/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59116H' target='_top'>1/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59130H' target='_top'>1/30</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59213H' target='_top'>2/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59220H' target='_top'>2/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59306H' target='_top'>3/6</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=SYX3UPR5F" target="_top">...more</a></div>
				<div class="product_price"><span>$8,400起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYXJDPR4A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000085/000547/00017789.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYXJDPR4A" target="_top">海南傳奇─三亞鳳凰嶺陵水椰田古寨特選優值四天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49203A' target='_top'>2/3</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=SYXJDPR4A" target="_top">...more</a></div>
				<div class="product_price"><span>$19,900起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.PNG	" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYXJDPR4B" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000085/000547/00014851.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYXJDPR4B" target="_top">海南傳奇─三亞蘭花大世界南天生態植物園鳳凰嶺精選優值四天</a></div>
				<div class="departure_date">
					出發日期：
					<s><a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP48D23A' target='_top'>12/23</a></s> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP48D30A' target='_top'>12/30</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49106A' target='_top'>1/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49113A' target='_top'>1/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49120A' target='_top'>1/20</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49127A' target='_top'>1/27</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49217A' target='_top'>2/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49224A' target='_top'>2/24</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=SYXJDPR4B" target="_top">...more</a></div>
				<div class="product_price"><span>$11,999起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.PNG	" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYXJDPR5A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000085/000547/00014879.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYXJDPR5A" target="_top">海南傳奇─三亞保亭陵水椰田古寨石梅灣奢華饗宴五天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP58D19A' target='_top'>12/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP58D26A' target='_top'>12/26</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59102A' target='_top'>1/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59109A' target='_top'>1/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59116A' target='_top'>1/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59123A' target='_top'>1/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59213A' target='_top'>2/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59220A' target='_top'>2/20</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=SYXJDPR5A" target="_top">...more</a></div>
				<div class="product_price"><span>$21,000起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.PNG	" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYXJDPR5B" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000085/000547/00008345.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYXJDPR5B" target="_top">海南傳奇─三亞Club Med宋城千古情陵水奢華五天</a></div>
				<div class="departure_date">
					出發日期：
					<s><a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59206B' target='_top'>2/6</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=SYXJDPR5B" target="_top">...more</a></div>
				<div class="product_price"><span>$40,500起</span></div>
   				<div class="product_offer"><p><span style="color:#FFFFFF;"><span style="background-color:#FF8C00;">台中出發</span></span></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=WUXAEPR5A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000258/000995/00017367.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=WUXAEPR5A" target="_top">華信假期─無錫泰州鹽城蘇北精華尊貴五天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=WUXAEP59107A' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=WUXAEP59218A' target='_top'>2/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=WUXAEP59304A' target='_top'>3/4</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=WUXAEP59318A' target='_top'>3/18</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=WUXAEPR5A" target="_top">...more</a></div>
				<div class="product_price"><span>$18,000起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.png" width="60" /></p></div>
			</div>
		</div>
		<div class="title_box" >
			<div class="left_box" id="DAREA00016">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>中南半島</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=HANVJP5A" target="_top"><img src="/eWeb_summittour/IMGDB/001123/001126/001362/00016057.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=HANVJP5A" target="_top">【台中出發】特選越捷～悠遊河內雙龍灣5日</a></div>
 				<div class="product_description">贈送當地精美禮物</div> 
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=HANVJ181219A' target='_top'>12/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=HANVJ181222A' target='_top'>12/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=HANVJ181224A' target='_top'>12/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=HANVJ181226A' target='_top'>12/26</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=HANVJP5A" target="_top">...more</a></div>
				<div class="product_price"><span>$15,900起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.png" width="60" /></p></div>
			</div>
		</div>
	</div>
</body>
</html>

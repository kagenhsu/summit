﻿<%
'//===========================================================================================
'//						index.asp
'//-------------------------------------------------------------------------------------------
'//		description		: 此頁面係由自動產生因此不用納入版本
'//-------------------------------------------------------------------------------------------
Response.CacheControl = "no-cache" 
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="robots" content="noindex">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
	<title>國外團體旅遊：海南(海口、三亞)旅遊行程 - 超悠</title>
	<!--產出時間:2018/12/4 下午 11:03:39 / WEB:summittour-->
	<link href='/eweb_summittour/css/reset.css' rel='stylesheet' type='text/css'>
	<link href='/eweb_summittour/css/autopage.css' rel='stylesheet' type='text/css'>
	<script languege="javascript" src="/script/jquery.js"></script>
	<script languege="javascript" src="/eweb_summittour/JS/autopage.js"></script>
</head>
<body>
	<div class="metropolis">
		<div class="title_box" >
			<div class="left_box" id="SAREA00041">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>海南(海口、三亞)</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR4A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000085/000547/00014888.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR4A" target="_top">海南傳奇~三亞Club Med玫瑰谷海棠廣場椰田古寨精彩四天</a></div>
				<div class="departure_date">
					出發日期：
					<s><a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP48D23C' target='_top'>12/23</a></s> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP48D30C' target='_top'>12/30</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49106C' target='_top'>1/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49113C' target='_top'>1/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49120C' target='_top'>1/20</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49127C' target='_top'>1/27</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49217C' target='_top'>2/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49224C' target='_top'>2/24</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=SYX3UPR4A" target="_top">...more</a></div>
				<div class="product_price"><span>$20,500起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.PNG	" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR4C" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000085/000547/00010269.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR4C" target="_top">海南傳奇─三亞臨春嶺森林公園陵水超值四天(買一送一)</a></div>
 				<div class="product_description">稅外$2500/人.買一送一</div> 
				<div class="departure_date">
					出發日期：
					<s><a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP48D23S' target='_top'>12/23</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=SYX3UPR4C" target="_top">...more</a></div>
				<div class="product_price"><span>$2,900起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR4D" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000085/000547/00017947.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR4D" target="_top">海南傳奇─玩翻三亞水上活動浪漫風帆精彩搖滾之旅四天</a></div>
 				<div class="product_description">送網卡</div> 
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49106R' target='_top'>1/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49113R' target='_top'>1/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49120R' target='_top'>1/20</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49127R' target='_top'>1/27</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49217R' target='_top'>2/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49224R' target='_top'>2/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49303R' target='_top'>3/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49310R' target='_top'>3/10</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=SYX3UPR4D" target="_top">...more</a></div>
				<div class="product_price"><span>$12,000起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR4E" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000085/000547/00017978.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR4E" target="_top">海南傳奇─三亞全程五星希爾頓興隆泡湯趣四日(稅外2500)</a></div>
 				<div class="product_description">稅外$2500/人</div> 
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49106H' target='_top'>1/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49113H' target='_top'>1/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49120H' target='_top'>1/20</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49127H' target='_top'>1/27</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49217H' target='_top'>2/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49224H' target='_top'>2/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49303H' target='_top'>3/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49310H' target='_top'>3/10</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=SYX3UPR4E" target="_top">...more</a></div>
				<div class="product_price"><span>$4,400起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR5B" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000085/000547/00014852.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR5B" target="_top">海南傳奇─三亞海口馮小剛電影公社興隆陵水低調奢華五天</a></div>
				<div class="departure_date">
					出發日期：
					<s><a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59206A' target='_top'>2/6</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=SYX3UPR5B" target="_top">...more</a></div>
				<div class="product_price"><span>$25,500起</span></div>
   				<div class="product_offer"><p><span style="color:#FFFFFF;"><span style="background-color:#FF8C00;">台中出發</span></span></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR5C" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000085/000547/00014852.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR5C" target="_top">海南傳奇─三亞陵水特選優值五天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59130A' target='_top'>1/30</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=SYX3UPR5C" target="_top">...more</a></div>
				<div class="product_price"><span>$16,800起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR5D" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000085/000547/00017929.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR5D" target="_top">海南傳奇─三亞Club Med陵水奢華五天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59102C' target='_top'>1/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59109C' target='_top'>1/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59116C' target='_top'>1/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59130C' target='_top'>1/30</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59213C' target='_top'>2/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59220C' target='_top'>2/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59306C' target='_top'>3/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59313C' target='_top'>3/13</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=SYX3UPR5D" target="_top">...more</a></div>
				<div class="product_price"><span>$25,300起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR5E" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000085/000547/00017949.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR5E" target="_top">海南傳奇─玩翻三亞大東海浪漫風帆萬寧石梅灣五日</a></div>
 				<div class="product_description">送網卡</div> 
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59102R' target='_top'>1/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59109R' target='_top'>1/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59116R' target='_top'>1/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59130R' target='_top'>1/30</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59213R' target='_top'>2/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59220R' target='_top'>2/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59306R' target='_top'>3/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59313R' target='_top'>3/13</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=SYX3UPR5E" target="_top">...more</a></div>
				<div class="product_price"><span>$17,000起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR5F" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000085/000547/00017977.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR5F" target="_top">海南傳奇─三亞全程五星希爾頓興隆泡湯趣五天(稅外2500)</a></div>
 				<div class="product_description">稅外$2500/人</div> 
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP58D19H' target='_top'>12/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59102H' target='_top'>1/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59109H' target='_top'>1/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59116H' target='_top'>1/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59130H' target='_top'>1/30</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59213H' target='_top'>2/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59220H' target='_top'>2/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59306H' target='_top'>3/6</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=SYX3UPR5F" target="_top">...more</a></div>
				<div class="product_price"><span>$5,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYXJDPR4A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000085/000547/00017789.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYXJDPR4A" target="_top">海南傳奇─三亞鳳凰嶺陵水椰田古寨特選優值四天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49203A' target='_top'>2/3</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=SYXJDPR4A" target="_top">...more</a></div>
				<div class="product_price"><span>$17,000起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.PNG	" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYXJDPR4B" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000085/000547/00014851.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYXJDPR4B" target="_top">海南傳奇─三亞蘭花大世界南天生態植物園鳳凰嶺精選優值四天</a></div>
				<div class="departure_date">
					出發日期：
					<s><a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP48D23A' target='_top'>12/23</a></s> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP48D30A' target='_top'>12/30</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49106A' target='_top'>1/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49113A' target='_top'>1/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49120A' target='_top'>1/20</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49127A' target='_top'>1/27</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49217A' target='_top'>2/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49224A' target='_top'>2/24</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=SYXJDPR4B" target="_top">...more</a></div>
				<div class="product_price"><span>$9,999起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.PNG	" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYXJDPR5A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000085/000547/00014879.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYXJDPR5A" target="_top">海南傳奇─三亞保亭陵水椰田古寨石梅灣奢華饗宴五天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP58D19A' target='_top'>12/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP58D26A' target='_top'>12/26</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59102A' target='_top'>1/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59109A' target='_top'>1/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59116A' target='_top'>1/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59123A' target='_top'>1/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59213A' target='_top'>2/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59220A' target='_top'>2/20</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=SYXJDPR5A" target="_top">...more</a></div>
				<div class="product_price"><span>$18,000起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.PNG	" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYXJDPR5B" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000085/000547/00008345.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYXJDPR5B" target="_top">海南傳奇─三亞Club Med宋城千古情陵水奢華五天</a></div>
				<div class="departure_date">
					出發日期：
					<s><a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59206B' target='_top'>2/6</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=SYXJDPR5B" target="_top">...more</a></div>
				<div class="product_price"><span>$37,500起</span></div>
   				<div class="product_offer"><p><span style="color:#FFFFFF;"><span style="background-color:#FF8C00;">台中出發</span></span></p></div>
			</div>
		</div>
	</div>
</body>
</html>

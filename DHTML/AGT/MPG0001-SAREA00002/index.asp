﻿<%
'//===========================================================================================
'//						index.asp
'//-------------------------------------------------------------------------------------------
'//		description		: 此頁面係由自動產生因此不用納入版本
'//-------------------------------------------------------------------------------------------
Response.CacheControl = "no-cache" 
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="robots" content="noindex">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
	<title>國外團體旅遊：九寨溝、稻城亞丁旅遊行程 - 超悠</title>
	<!--產出時間:2018/12/4 下午 11:03:37 / WEB:summittour-->
	<link href='/eweb_summittour/css/reset.css' rel='stylesheet' type='text/css'>
	<link href='/eweb_summittour/css/autopage.css' rel='stylesheet' type='text/css'>
	<script languege="javascript" src="/script/jquery.js"></script>
	<script languege="javascript" src="/eweb_summittour/JS/autopage.js"></script>
</head>
<body>
	<div class="metropolis">
		<div class="title_box" >
			<div class="left_box" id="SAREA00002">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>九寨溝、稻城亞丁</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTU08TP" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000079/000549/00012385.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTU08TP" target="_top">華信假期─冰川奇景~海螺溝、四姑娘山八天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCIP88D18A' target='_top'>12/18</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CTU08TP" target="_top">...more</a></div>
				<div class="product_price"><span>$21,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTUCA08" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000079/001180/00017746.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTUCA08" target="_top">五星希爾頓四姑娘山新都橋木格錯8日(無購物、無自費)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190103A' target='_top'>1/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190115A' target='_top'>1/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190122A' target='_top'>1/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190212A' target='_top'>2/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190223A' target='_top'>2/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190302A' target='_top'>3/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190312A' target='_top'>3/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190319A' target='_top'>3/19</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CTUCA08" target="_top">...more</a></div>
				<div class="product_price"><span>$19,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTUCA08B" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000079/001180/00011696.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTUCA08B" target="_top">五星希爾頓四姑娘山蒸汽小車羅城古鎮8日(無購物、無自費)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190103B' target='_top'>1/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190115B' target='_top'>1/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190122B' target='_top'>1/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190212B' target='_top'>2/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190223B' target='_top'>2/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190302B' target='_top'>3/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190312B' target='_top'>3/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190319B' target='_top'>3/19</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CTUCA08B" target="_top">...more</a></div>
				<div class="product_price"><span>$17,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTUCAPT8A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000079/000549/00007741.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTUCAPT8A" target="_top">稻城.亞丁.新都橋.丹巴.四姑娘山八日</a></div>
 				<div class="product_description">贈送紅景天口服液一盒</div> 
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA181217A' target='_top'>12/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA181231A' target='_top'>12/31</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190107A' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190312C' target='_top'>3/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190323A' target='_top'>3/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190326A' target='_top'>3/26</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CTUCAPT8A" target="_top">...more</a></div>
				<div class="product_price"><span>$22,900起</span></div>
   				<div class="product_offer"><p><img alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" style="height: 22px; width: 60px" /><img height="21" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011521.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTUCAPT8B" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000079/000549/00007728.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTUCAPT8B" target="_top">四姑娘山、甲居藏寨、新都橋、海螺溝8日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA181218A' target='_top'>12/18</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA181225A' target='_top'>12/25</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190101A' target='_top'>1/1</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190115C' target='_top'>1/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190129A' target='_top'>1/29</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CTUCAPT8B" target="_top">...more</a></div>
				<div class="product_price"><span>$19,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
		</div>
	</div>
</body>
</html>

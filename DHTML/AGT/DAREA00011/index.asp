﻿<%
'//===========================================================================================
'//						index.asp
'//-------------------------------------------------------------------------------------------
'//		description		: 此頁面係由自動產生因此不用納入版本
'//-------------------------------------------------------------------------------------------
Response.CacheControl = "no-cache" 
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="robots" content="noindex">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
	<title>國外團體旅遊：紐澳旅遊行程 - 超悠</title>
	<!--產出時間:2018/12/4 下午 11:03:27 / WEB:summittour-->
	<link href='/eweb_summittour/css/reset.css' rel='stylesheet' type='text/css'>
	<link href='/eweb_summittour/css/autopage.css' rel='stylesheet' type='text/css'>
	<script languege="javascript" src="/script/jquery.js"></script>
	<script languege="javascript" src="/eweb_summittour/JS/autopage.js"></script>
</head>
<body>
	<div class="metropolis">
		<div class="title_box" >
			<div class="left_box" id="SAREA00237">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>紐西蘭南北島</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=AKLNZP10A" target="_top"><img src="/eWeb_summittour/IMGDB/000576/000759/000924/00016954.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=AKLNZP10A" target="_top">紐西蘭南北島湖山星光10日~米佛峽灣、皇后鎮、阿爾卑斯高山</a></div>
 				<div class="product_description">★紐航直飛特惠價★</div> 
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=AKLNZ190103A' target='_top'>1/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=AKLNZ190117A' target='_top'>1/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=AKLNZ190214A' target='_top'>2/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=AKLNZ190228A' target='_top'>2/28</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=AKLNZ190314A' target='_top'>3/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=AKLNZ190328A' target='_top'>3/28</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=AKLNZP10A" target="_top">...more</a></div>
				<div class="product_price"><span>$97,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=AKLNZP11A" target="_top"><img src="/eWeb_summittour/IMGDB/000576/000759/000924/00016971.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=AKLNZP11A" target="_top">紐西蘭南北島經典全覽11日~高山、湖泊、天空與自然生態饗宴</a></div>
 				<div class="product_description">★早鳥優惠第二人減4000元★</div> 
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=AKLNZ190105B' target='_top'>1/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=AKLNZ190119B' target='_top'>1/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=AKLNZ190124A' target='_top'>1/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=AKLNZ190129A' target='_top'>1/29</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=AKLNZ190202A' target='_top'>2/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=AKLNZ190216B' target='_top'>2/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=AKLNZ190223A' target='_top'>2/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=AKLNZ190302B' target='_top'>3/2</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=AKLNZP11A" target="_top">...more</a></div>
				<div class="product_price"><span>$123,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=AKLNZP15A" target="_top"><img src="/eWeb_summittour/IMGDB/000576/000759/000924/00016990.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=AKLNZP15A" target="_top">紐西蘭南北島~縱貫山水純淨連線經典全覽十五日</a></div>
 				<div class="product_description">★早鳥優惠第二人減6000元★</div> 
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=AKLNZ190105A' target='_top'>1/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=AKLNZ190119A' target='_top'>1/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=AKLNZ190216A' target='_top'>2/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=AKLNZ190302A' target='_top'>3/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=AKLNZ190316A' target='_top'>3/16</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=AKLNZP15A" target="_top">...more</a></div>
				<div class="product_price"><span>$168,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
		</div>
	</div>
</body>
</html>

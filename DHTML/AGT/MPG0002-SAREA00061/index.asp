﻿<%
'//===========================================================================================
'//						index.asp
'//-------------------------------------------------------------------------------------------
'//		description		: 此頁面係由自動產生因此不用納入版本
'//-------------------------------------------------------------------------------------------
Response.CacheControl = "no-cache" 
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="robots" content="noindex">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
	<title>國外團體旅遊：西馬來西亞(吉隆坡、檳城)、新加坡旅遊行程 - 超悠</title>
	<!--產出時間:2018/12/4 下午 11:03:40 / WEB:summittour-->
	<link href='/eweb_summittour/css/reset.css' rel='stylesheet' type='text/css'>
	<link href='/eweb_summittour/css/autopage.css' rel='stylesheet' type='text/css'>
	<script languege="javascript" src="/script/jquery.js"></script>
	<script languege="javascript" src="/eweb_summittour/JS/autopage.js"></script>
</head>
<body>
	<div class="metropolis">
		<div class="title_box" >
			<div class="left_box" id="SAREA00061">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>西馬來西亞(吉隆坡、檳城)、新加坡</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=KULCIP05C" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000619/000641/00014437.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=KULCIP05C" target="_top">《五大保證》旗艦馬來西亞～全程五星酒店、大紅花海上VILLA、熱石SPA療程、英式下午茶5日(無購物)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=KULCI181222A' target='_top'>12/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULCI190104A' target='_top'>1/4</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULCI190107A' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULCI190109A' target='_top'>1/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULCI190111A' target='_top'>1/11</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULCI190114A' target='_top'>1/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULCI190116A' target='_top'>1/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULCI190118A' target='_top'>1/18</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=KULCIP05C" target="_top">...more</a></div>
				<div class="product_price"><span>$25,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=KULD7P05B" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000619/000641/00017545.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=KULD7P05B" target="_top">《春節預購減3千》翻轉馬來西亞～大紅花海上VILLA(一房一泳池)、馬六甲遊蹤5日(升等五星酒店1晚ｘ飯店自助晚餐)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=KULD7190201A' target='_top'>2/1</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULD7190204A' target='_top'>2/4</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULD7190206A' target='_top'>2/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULD7190207A' target='_top'>2/7</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=KULD7P05B" target="_top">...more</a></div>
				<div class="product_price"><span>$28,900起</span></div>
   				<div class="product_offer"><p><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=KULODP05C" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000619/000641/00017450.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=KULODP05C" target="_top">【馬印假期】鍾愛馬來-怡保山城、雙威溫泉樂園、五星悅榕庄五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190107A' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190110A' target='_top'>1/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190114A' target='_top'>1/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190117A' target='_top'>1/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190121A' target='_top'>1/21</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190124A' target='_top'>1/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190218A' target='_top'>2/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190225A' target='_top'>2/25</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=KULODP05C" target="_top">...more</a></div>
				<div class="product_price"><span>$14,900起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008844.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=KULODP05D" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000619/000641/00018038.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=KULODP05D" target="_top">【馬印假期】愛去馬來-馬六甲、法國村、雲頂小資五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190107B' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190110B' target='_top'>1/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190114B' target='_top'>1/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190117B' target='_top'>1/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190121B' target='_top'>1/21</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190124B' target='_top'>1/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190218B' target='_top'>2/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190225B' target='_top'>2/25</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=KULODP05D" target="_top">...more</a></div>
				<div class="product_price"><span>$12,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=KULODP05E" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000619/000641/00018047.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=KULODP05E" target="_top">【馬印假期】最愛馬來-AVANI棕櫚渡假村海上VILLA、馬六甲古城五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190107C' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190110C' target='_top'>1/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190114C' target='_top'>1/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190117C' target='_top'>1/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190121C' target='_top'>1/21</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190124C' target='_top'>1/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190218C' target='_top'>2/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190225C' target='_top'>2/25</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=KULODP05E" target="_top">...more</a></div>
				<div class="product_price"><span>$14,800起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=KULODP06B" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000619/000641/00014411.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=KULODP06B" target="_top">【豬年迎馬新】璀璨馬新大紅花海上Villa.環球影城.樂高樂園.漫步雲端.濱海灣無購物 6 日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190202A' target='_top'>2/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190204A' target='_top'>2/4</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190207A' target='_top'>2/7</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=KULODP06B" target="_top">...more</a></div>
				<div class="product_price"><span>$39,900起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011383.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PENCIP05B" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000619/000641/00017550.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PENCIP05B" target="_top">《經典》檳城蘭卡威～三離島之旅(孕婦島濕米島水晶島)、蟲鳴大地、魚吻足SPA5日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PENCI181217A' target='_top'>12/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PENCI181219A' target='_top'>12/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PENCI181221A' target='_top'>12/21</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PENCI181223A' target='_top'>12/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PENCI181224A' target='_top'>12/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PENCI181225A' target='_top'>12/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PENCI181230A' target='_top'>12/30</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PENCI181231A' target='_top'>12/31</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=PENCIP05B" target="_top">...more</a></div>
				<div class="product_price"><span>$23,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img alt="" height="21" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png" width="61" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SINBRP04A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000619/000641/00017580.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SINBRP04A" target="_top">甜蜜遊獅城～五星聖淘沙、環球影城、海洋生物園、金沙濱海灣四日(含稅)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=SINBR181214A' target='_top'>12/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SINBR181216A' target='_top'>12/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SINBR181218A' target='_top'>12/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SINBR181219A' target='_top'>12/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SINBR181220B' target='_top'>12/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SINBR181221A' target='_top'>12/21</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SINBR181222A' target='_top'>12/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SINBR181223B' target='_top'>12/23</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=SINBRP04A" target="_top">...more</a></div>
				<div class="product_price"><span>$24,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SINBRP05A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000619/000641/00014437.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SINBRP05A" target="_top">經典新馬大紅花海上VILLA五日～精油熱石SPA‧三輪車‧KYTOWER玻璃走廊‧3D藝術博物館‧高塔‧金沙(無購物)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=SINBR181220A' target='_top'>12/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SINBR181223A' target='_top'>12/23</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=SINBR181229A' target='_top'>12/29</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SINBR190103A' target='_top'>1/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SINBR190108A' target='_top'>1/8</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SINBR190110A' target='_top'>1/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SINBR190112A' target='_top'>1/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SINBR190115A' target='_top'>1/15</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=SINBRP05A" target="_top">...more</a></div>
				<div class="product_price"><span>$22,999起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img alt="" height="21" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png" width="61" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SINTRP05A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000619/000641/00017489.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SINTRP05A" target="_top">【豬年迎新春~2/6保證出團】新馬雙國獨家卡通星球.環球影城.樂高樂園.柔佛螢河遊.阿凡達燈光秀5日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=SINTR190206A' target='_top'>2/6</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=SINTRP05A" target="_top">...more</a></div>
				<div class="product_price"><span>$36,999起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img alt="" height="21" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png" width="61" /><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011383.png"/></p></div>
			</div>
		</div>
	</div>
</body>
</html>

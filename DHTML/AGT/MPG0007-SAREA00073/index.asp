﻿<%
'//===========================================================================================
'//						index.asp
'//-------------------------------------------------------------------------------------------
'//		description		: 此頁面係由自動產生因此不用納入版本
'//-------------------------------------------------------------------------------------------
Response.CacheControl = "no-cache" 
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="robots" content="noindex">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
	<title>國外團體旅遊：溫哥華旅遊行程 - 超悠</title>
	<!--產出時間:2018/12/4 下午 11:03:55 / WEB:summittour-->
	<link href='/eweb_summittour/css/reset.css' rel='stylesheet' type='text/css'>
	<link href='/eweb_summittour/css/autopage.css' rel='stylesheet' type='text/css'>
	<script languege="javascript" src="/script/jquery.js"></script>
	<script languege="javascript" src="/eweb_summittour/JS/autopage.js"></script>
</head>
<body>
	<div class="metropolis">
		<div class="title_box" >
			<div class="left_box" id="SAREA00073">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>溫哥華</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=YVRACP08B" target="_top"><img src="/eWeb_summittour/IMGDB/000577/000770/001214/00017600.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=YVRACP08B" target="_top">溫哥華、洛磯山脈8日～雙飛+雙城堡酒店</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=YVRAC190112A' target='_top'>1/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=YVRAC190216A' target='_top'>2/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=YVRAC190309A' target='_top'>3/9</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=YVRACP08B" target="_top">...more</a></div>
				<div class="product_price"><span>$56,800起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img alt="" height="21" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png" width="61" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=YVRACP08C" target="_top"><img src="/eWeb_summittour/IMGDB/000577/000770/001214/00017720.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=YVRACP08C" target="_top">【探索極光】冬季加拿大黃刀鎮8日~3晚黃刀鎮賞極光、狗拉雪橇</a></div>
 				<div class="product_description">★兩人同行第二人減6000元★</div> 
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=YVRAC190119A' target='_top'>1/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=YVRAC190223A' target='_top'>2/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=YVRAC190316A' target='_top'>3/16</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=YVRACP08C" target="_top">...more</a></div>
				<div class="product_price"><span>$77,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
		</div>
	</div>
</body>
</html>

﻿<%
'//===========================================================================================
'//						index.asp
'//-------------------------------------------------------------------------------------------
'//		description		: 此頁面係由自動產生因此不用納入版本
'//-------------------------------------------------------------------------------------------
Response.CacheControl = "no-cache" 
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="robots" content="noindex">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
	<title>國外團體旅遊：重慶、武漢、長江三峽旅遊行程 - 超悠</title>
	<!--產出時間:2018/12/4 下午 11:03:38 / WEB:summittour-->
	<link href='/eweb_summittour/css/reset.css' rel='stylesheet' type='text/css'>
	<link href='/eweb_summittour/css/autopage.css' rel='stylesheet' type='text/css'>
	<script languege="javascript" src="/script/jquery.js"></script>
	<script languege="javascript" src="/eweb_summittour/JS/autopage.js"></script>
</head>
<body>
	<div class="metropolis">
		<div class="title_box" >
			<div class="left_box" id="SAREA00057">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>重慶、武漢、長江三峽</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=WUHCIP8B" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000081/000997/00011822.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=WUHCIP8B" target="_top">華信假期─天下第一仙山武當山兩壩一峽８日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=WUHCIP89124A' target='_top'>1/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=WUHCIP89131A' target='_top'>1/31</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=WUHCIP89214A' target='_top'>2/14</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=WUHCIP89314A' target='_top'>3/14</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=WUHCIP8B" target="_top">...more</a></div>
				<div class="product_price"><span>$26,800起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=WUHCIT8C" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000081/000997/00010002.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=WUHCIT8C" target="_top">華信假期─四季浪歌瀑布絕壁龍洞恩施大峽谷三峽人家八天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=WUHCIP89124B' target='_top'>1/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=WUHCIP89131B' target='_top'>1/31</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=WUHCIP89214B' target='_top'>2/14</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=WUHCIP89314B' target='_top'>3/14</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=WUHCIT8C" target="_top">...more</a></div>
				<div class="product_price"><span>$24,800起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
		</div>
	</div>
</body>
</html>

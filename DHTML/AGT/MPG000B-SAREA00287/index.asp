﻿<%
'//===========================================================================================
'//						index.asp
'//-------------------------------------------------------------------------------------------
'//		description		: 此頁面係由自動產生因此不用納入版本
'//-------------------------------------------------------------------------------------------
Response.CacheControl = "no-cache" 
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="robots" content="noindex">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
	<title>國外團體旅遊：越南(河內、峴港、胡志明、富國島)旅遊行程 - 超悠</title>
	<!--產出時間:2018/12/4 下午 11:03:57 / WEB:summittour-->
	<link href='/eweb_summittour/css/reset.css' rel='stylesheet' type='text/css'>
	<link href='/eweb_summittour/css/autopage.css' rel='stylesheet' type='text/css'>
	<script languege="javascript" src="/script/jquery.js"></script>
	<script languege="javascript" src="/eweb_summittour/JS/autopage.js"></script>
</head>
<body>
	<div class="metropolis">
		<div class="title_box" >
			<div class="left_box" id="SAREA00287">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>越南(河內、峴港、胡志明、富國島)</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=DADFEP5C" target="_top"><img src="/eWeb_summittour/IMGDB/001123/001126/001362/00017149.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=DADFEP5C" target="_top">亙越安南風華再峴、最美海灣中越峴港五日(四星超值)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=DADFE181217A' target='_top'>12/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=DADFE181221A' target='_top'>12/21</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=DADFE181225A' target='_top'>12/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=DADFE190102A' target='_top'>1/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=DADFE190106A' target='_top'>1/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=DADFE190110A' target='_top'>1/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=DADFE190114A' target='_top'>1/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=DADFE190118A' target='_top'>1/18</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=DADFEP5C" target="_top">...more</a></div>
				<div class="product_price"><span>$19,000起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img alt="" height="21" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011521.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=DADFEP5D" target="_top"><img src="/eWeb_summittour/IMGDB/001123/001126/001362/00016104.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=DADFEP5D" target="_top">峴港原來可以醬玩~樂遊峴港、纜車樂園渡假五日(豪華)</a></div>
 				<div class="product_description">贈送越南傳統斗笠乙頂</div> 
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=DADFE181217B' target='_top'>12/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=DADFE181221B' target='_top'>12/21</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=DADFE190102B' target='_top'>1/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=DADFE190106B' target='_top'>1/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=DADFE190110B' target='_top'>1/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=DADFE190114B' target='_top'>1/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=DADFE190118B' target='_top'>1/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=DADFE190122B' target='_top'>1/22</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=DADFEP5D" target="_top">...more</a></div>
				<div class="product_price"><span>$25,500起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img alt="" height="21" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011521.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=DADFEP5E" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000625/000671/00017090.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=DADFEP5E" target="_top">亙越安南風華再峴、最美海灣中越峴港五日(四星超值)【過年加班機】</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=DADFE190205A' target='_top'>2/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=DADFE190206B' target='_top'>2/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=DADFE190207D' target='_top'>2/7</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=DADFEP5E" target="_top">...more</a></div>
				<div class="product_price"><span>$34,900起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=DADFEP5F" target="_top"><img src="/eWeb_summittour/IMGDB/001123/001126/001362/00017151.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=DADFEP5F" target="_top">峴港原來可以醬玩、樂遊峴港漫遊會安、纜車樂園美食五日【過年加班機】</a></div>
 				<div class="product_description">贈送越南傳統斗笠乙頂</div> 
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=DADFE190205B' target='_top'>2/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=DADFE190206A' target='_top'>2/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=DADFE190207C' target='_top'>2/7</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=DADFEP5F" target="_top">...more</a></div>
				<div class="product_price"><span>$40,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=HANCIP5F" target="_top"><img src="/eWeb_summittour/IMGDB/001123/001126/001362/00016057.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=HANCIP5F" target="_top">【中華航空】北越雙龍灣(下龍+陸龍)超值五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=HANCI181219B' target='_top'>12/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=HANCI181226B' target='_top'>12/26</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=HANCI181227B' target='_top'>12/27</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=HANCI190104B' target='_top'>1/4</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=HANCI190106B' target='_top'>1/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=HANCI190111B' target='_top'>1/11</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=HANCI190113B' target='_top'>1/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=HANCI190118B' target='_top'>1/18</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=HANCIP5F" target="_top">...more</a></div>
				<div class="product_price"><span>$17,500起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=HANCIP5G" target="_top"><img src="/eWeb_summittour/IMGDB/001123/001126/001362/00017152.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=HANCIP5G" target="_top">【中華航空】尊爵北越~雙龍傳奇陸龍灣+下龍灣五日4+5星(含三項-按摩、電瓶車、下午茶)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=HANCI181219A' target='_top'>12/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=HANCI181226A' target='_top'>12/26</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=HANCI181227A' target='_top'>12/27</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=HANCI190104A' target='_top'>1/4</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=HANCI190106A' target='_top'>1/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=HANCI190111A' target='_top'>1/11</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=HANCI190113A' target='_top'>1/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=HANCI190118A' target='_top'>1/18</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=HANCIP5G" target="_top">...more</a></div>
				<div class="product_price"><span>$21,500起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=HANCIP5H" target="_top"><img src="/eWeb_summittour/IMGDB/001123/001126/001362/00017155.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=HANCIP5H" target="_top">【中華航空】北越雙龍灣美人魚號VILLA．太陽樂園五日【無購物】</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=HANCI181219C' target='_top'>12/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=HANCI181226C' target='_top'>12/26</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=HANCI190104C' target='_top'>1/4</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=HANCI190106C' target='_top'>1/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=HANCI190111C' target='_top'>1/11</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=HANCI190113C' target='_top'>1/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=HANCI190118C' target='_top'>1/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=HANCI190215C' target='_top'>2/15</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=HANCIP5H" target="_top">...more</a></div>
				<div class="product_price"><span>$24,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=HANCIP5I" target="_top">【中華航空】北越雙龍灣天堂號移動VILLA、太陽樂園、JW萬豪尊爵饗宴五日【無購物】</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=HANCI181219D' target='_top'>12/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=HANCI181226D' target='_top'>12/26</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=HANCI181227D' target='_top'>12/27</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=HANCI190104D' target='_top'>1/4</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=HANCI190106D' target='_top'>1/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=HANCI190111D' target='_top'>1/11</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=HANCI190113D' target='_top'>1/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=HANCI190118D' target='_top'>1/18</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=HANCIP5I" target="_top">...more</a></div>
				<div class="product_price"><span>$32,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=HANVJP5A" target="_top"><img src="/eWeb_summittour/IMGDB/001123/001126/001362/00016057.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=HANVJP5A" target="_top">【台中出發】特選越捷～悠遊河內雙龍灣5日</a></div>
 				<div class="product_description">贈送當地精美禮物</div> 
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=HANVJ181219A' target='_top'>12/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=HANVJ181222A' target='_top'>12/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=HANVJ181224A' target='_top'>12/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=HANVJ181226A' target='_top'>12/26</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=HANVJP5A" target="_top">...more</a></div>
				<div class="product_price"><span>$13,900起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PQC05" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000625/000671/00008399.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PQC05" target="_top">南越海上珍珠富國島+胡志明五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PQC05181220A' target='_top'>12/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PQC05181227A' target='_top'>12/27</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=PQC05" target="_top">...more</a></div>
				<div class="product_price"><span>$27,900起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /></p></div>
			</div>
		</div>
	</div>
</body>
</html>

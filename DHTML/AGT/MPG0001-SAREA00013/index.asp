﻿<%
'//===========================================================================================
'//						index.asp
'//-------------------------------------------------------------------------------------------
'//		description		: 此頁面係由自動產生因此不用納入版本
'//-------------------------------------------------------------------------------------------
Response.CacheControl = "no-cache" 
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="robots" content="noindex">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
	<title>國外團體旅遊：山西(太原)、內蒙(呼和浩特)、銀川旅遊行程 - 超悠</title>
	<!--產出時間:2018/12/4 下午 11:03:37 / WEB:summittour-->
	<link href='/eweb_summittour/css/reset.css' rel='stylesheet' type='text/css'>
	<link href='/eweb_summittour/css/autopage.css' rel='stylesheet' type='text/css'>
	<script languege="javascript" src="/script/jquery.js"></script>
	<script languege="javascript" src="/eweb_summittour/JS/autopage.js"></script>
</head>
<body>
	<div class="metropolis">
		<div class="title_box" >
			<div class="left_box" id="SAREA00013">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>山西(太原)、內蒙(呼和浩特)、銀川</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=INCFEPT8A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000074/000985/00014927.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=INCFEPT8A" target="_top">游遍中國─寧夏沙坡頭108塔東方泰姬瑪哈陵溫泉8日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=INCMUT89122A' target='_top'>1/22</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=INCMUT89226A' target='_top'>2/26</a></s> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=INCMUT89326A' target='_top'>3/26</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=INCFEPT8A" target="_top">...more</a></div>
				<div class="product_price"><span>$18,500起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=INCMUPT8A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000074/000985/00014927.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=INCMUPT8A" target="_top">游遍中國─寧夏沙坡頭108塔東方泰姬瑪哈陵沙湖8日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=INCMUT89427A' target='_top'>4/27</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=INCMUT89525A' target='_top'>5/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=INCMUT89622A' target='_top'>6/22</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=INCMUPT8A" target="_top">...more</a></div>
				<div class="product_price"><span>$21,500起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=INCMUT8A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000074/000985/00017865.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=INCMUT8A" target="_top">游遍中國─塞上寧夏金沙島黃河石林西夏王朝八日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=INCMUT89427B' target='_top'>4/27</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=INCMUT89525B' target='_top'>5/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=INCMUT89622B' target='_top'>6/22</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=INCMUT8A" target="_top">...more</a></div>
				<div class="product_price"><span>$24,500起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /></p></div>
			</div>
		</div>
	</div>
</body>
</html>

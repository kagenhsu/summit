﻿<%
'//===========================================================================================
'//						index.asp
'//-------------------------------------------------------------------------------------------
'//		description		: 此頁面係由自動產生因此不用納入版本
'//-------------------------------------------------------------------------------------------
Response.CacheControl = "no-cache" 
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="robots" content="noindex">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
	<title>國外團體旅遊：旅遊行程 - 超悠</title>
	<!--產出時間:2018/12/4 下午 11:04:00 / WEB:summittour-->
	<link href='/eweb_summittour/css/reset.css' rel='stylesheet' type='text/css'>
	<link href='/eweb_summittour/css/autopage.css' rel='stylesheet' type='text/css'>
	<script languege="javascript" src="/script/jquery.js"></script>
	<script languege="javascript" src="/eweb_summittour/JS/autopage.js"></script>
</head>
<body>
	<div class="metropolis">
		<div class="title_box" >
			<div class="left_box" id="DAREA00001">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>港澳大陸</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTUCAPT8A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000079/000549/00007741.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTUCAPT8A" target="_top">稻城.亞丁.新都橋.丹巴.四姑娘山八日</a></div>
 				<div class="product_description">贈送紅景天口服液一盒</div> 
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA181217A' target='_top'>12/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA181231A' target='_top'>12/31</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190107A' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190312C' target='_top'>3/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190323A' target='_top'>3/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190326A' target='_top'>3/26</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CTUCAPT8A" target="_top">...more</a></div>
				<div class="product_price"><span>$22,900起</span></div>
   				<div class="product_offer"><p><img alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" style="height: 22px; width: 60px" /><img height="21" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011521.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=INCFEPT8A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000074/000985/00014927.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=INCFEPT8A" target="_top">游遍中國─寧夏沙坡頭108塔東方泰姬瑪哈陵溫泉8日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=INCMUT89122A' target='_top'>1/22</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=INCMUT89226A' target='_top'>2/26</a></s> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=INCMUT89326A' target='_top'>3/26</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=INCFEPT8A" target="_top">...more</a></div>
				<div class="product_price"><span>$18,500起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=URCMFPT12A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000065/001160/00013773.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=URCMFPT12A" target="_top">南疆風情.帕米爾高原.卡拉庫力湖.溫宿大峽谷12天(廈門)</a></div>
 				<div class="product_description">贈送每人一頂維吾爾小花帽</div> 
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=URCMF181217A' target='_top'>12/17</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=URCMFPT12A" target="_top">...more</a></div>
				<div class="product_price"><span>$43,900起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img height="21" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011521.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR5E" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000085/000547/00017949.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR5E" target="_top">海南傳奇─玩翻三亞大東海浪漫風帆萬寧石梅灣五日</a></div>
 				<div class="product_description">送網卡</div> 
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59102R' target='_top'>1/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59109R' target='_top'>1/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59116R' target='_top'>1/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59130R' target='_top'>1/30</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59213R' target='_top'>2/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59220R' target='_top'>2/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59306R' target='_top'>3/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59313R' target='_top'>3/13</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=SYX3UPR5E" target="_top">...more</a></div>
				<div class="product_price"><span>$17,000起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.png"/></p></div>
			</div>
		</div>
		<div class="title_box" >
			<div class="left_box" id="DAREA00014">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>美加</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=LAXMIAHXP10A" target="_top"><img src="/eWeb_summittour/IMGDB/000577/001055/001058/00012986.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=LAXMIAHXP10A" target="_top">神祕古巴華麗的冒險10天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=LAXMI190111B' target='_top'>1/11</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXMI190215B' target='_top'>2/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXMI190315B' target='_top'>3/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXMI190412B' target='_top'>4/12</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=LAXMIAHXP10A" target="_top">...more</a></div>
				<div class="product_price"><span>$94,900起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.PNG	" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=LAXMIAHXP12A" target="_top"><img src="/eWeb_summittour/IMGDB/000577/001055/001058/00015018.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=LAXMIAHXP12A" target="_top">情迷醉人烏托邦~古巴深度12天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=LAXMI190111A' target='_top'>1/11</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXMI190215A' target='_top'>2/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXMI190315A' target='_top'>3/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXMI190412A' target='_top'>4/12</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=LAXMIAHXP12A" target="_top">...more</a></div>
				<div class="product_price"><span>$132,900起</span></div>
   				<div class="product_offer"><p><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00015107.png"/><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.PNG	" width="60" /></p></div>
			</div>
		</div>
		<div class="title_box" >
			<div class="left_box" id="DAREA00016">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>中南半島</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=REPQDP5A" target="_top"><img src="/eWeb_summittour/IMGDB/001123/001125/001140/00009240.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=REPQDP5A" target="_top">JC經典吳哥～藝術深度之旅五日</a></div>
 				<div class="product_description">吳哥窟紀念T恤乙件等好禮</div> 
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=REPQD181222A' target='_top'>12/22</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=REPQD181229A' target='_top'>12/29</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=REPQD190105A' target='_top'>1/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=REPQD190112A' target='_top'>1/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=REPQD190119A' target='_top'>1/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=REPQD190126A' target='_top'>1/26</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=REPQD190202A' target='_top'>2/2</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=REPQD190205A' target='_top'>2/5</a></s>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=REPQDP5A" target="_top">...more</a></div>
				<div class="product_price"><span>$21,500起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=REPQDP5B" target="_top"><img src="/eWeb_summittour/IMGDB/001123/001125/001140/00008884.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=REPQDP5B" target="_top">JC暢遊吳哥～跨越時代之旅五日</a></div>
 				<div class="product_description">吳哥窟紀念T恤乙件等好禮</div> 
				<div class="departure_date">
					出發日期：
					<s><a href='/EW/GO/GroupDetail.asp?prodCd=REPQD181222B' target='_top'>12/22</a></s> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=REPQD181229B' target='_top'>12/29</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=REPQD190105B' target='_top'>1/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=REPQD190112B' target='_top'>1/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=REPQD190119B' target='_top'>1/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=REPQD190126B' target='_top'>1/26</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=REPQD190202B' target='_top'>2/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=REPQD190205B' target='_top'>2/5</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=REPQDP5B" target="_top">...more</a></div>
				<div class="product_price"><span>$24,500起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008848.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=HANVJP5A" target="_top"><img src="/eWeb_summittour/IMGDB/001123/001126/001362/00016057.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=HANVJP5A" target="_top">【台中出發】特選越捷～悠遊河內雙龍灣5日</a></div>
 				<div class="product_description">贈送當地精美禮物</div> 
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=HANVJ181219A' target='_top'>12/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=HANVJ181222A' target='_top'>12/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=HANVJ181224A' target='_top'>12/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=HANVJ181226A' target='_top'>12/26</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=HANVJP5A" target="_top">...more</a></div>
				<div class="product_price"><span>$13,900起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.png" width="60" /></p></div>
			</div>
		</div>
	</div>
</body>
</html>

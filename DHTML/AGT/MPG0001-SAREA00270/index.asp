﻿<%
'//===========================================================================================
'//						index.asp
'//-------------------------------------------------------------------------------------------
'//		description		: 此頁面係由自動產生因此不用納入版本
'//-------------------------------------------------------------------------------------------
Response.CacheControl = "no-cache" 
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="robots" content="noindex">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
	<title>國外團體旅遊：南北疆、絲路旅遊行程 - 超悠</title>
	<!--產出時間:2018/12/4 下午 11:03:38 / WEB:summittour-->
	<link href='/eweb_summittour/css/reset.css' rel='stylesheet' type='text/css'>
	<link href='/eweb_summittour/css/autopage.css' rel='stylesheet' type='text/css'>
	<script languege="javascript" src="/script/jquery.js"></script>
	<script languege="javascript" src="/eweb_summittour/JS/autopage.js"></script>
</head>
<body>
	<div class="metropolis">
		<div class="title_box" >
			<div class="left_box" id="SAREA00270">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>南北疆、絲路</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=URCMFPT12A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000065/001160/00013773.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=URCMFPT12A" target="_top">南疆風情.帕米爾高原.卡拉庫力湖.溫宿大峽谷12天(廈門)</a></div>
 				<div class="product_description">贈送每人一頂維吾爾小花帽</div> 
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=URCMF181217A' target='_top'>12/17</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=URCMFPT12A" target="_top">...more</a></div>
				<div class="product_price"><span>$43,900起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img height="21" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011521.png"/></p></div>
			</div>
		</div>
	</div>
</body>
</html>

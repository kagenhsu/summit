﻿<%
'//===========================================================================================
'//						index.asp
'//-------------------------------------------------------------------------------------------
'//		description		: 此頁面係由自動產生因此不用納入版本
'//-------------------------------------------------------------------------------------------
Response.CacheControl = "no-cache" 
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="robots" content="noindex">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
	<title>國外團體旅遊：鄭州、西安旅遊行程 - 超悠</title>
	<!--產出時間:2018/12/4 下午 11:03:40 / WEB:summittour-->
	<link href='/eweb_summittour/css/reset.css' rel='stylesheet' type='text/css'>
	<link href='/eweb_summittour/css/autopage.css' rel='stylesheet' type='text/css'>
	<script languege="javascript" src="/script/jquery.js"></script>
	<script languege="javascript" src="/eweb_summittour/JS/autopage.js"></script>
</head>
<body>
	<div class="metropolis">
		<div class="title_box" >
			<div class="left_box" id="SAREA00007">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>鄭州、西安</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CGOAEPR8B" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000076/000987/00010411.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CGOAEPR8B" target="_top">華信假期─賀新年龍門石窟雲臺山萬仙山郭亮村八日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CGOAEP89206A' target='_top'>2/6</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CGOAEPR8B" target="_top">...more</a></div>
				<div class="product_price"><span>$29,800起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CGOAEPT8A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000076/000987/00010411.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CGOAEPT8A" target="_top">華信假期─太行大峽谷龍門石窟雲台山頂級八日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CGOAEP88D28A' target='_top'>12/28</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CGOAEPT8A" target="_top">...more</a></div>
				<div class="product_price"><span>$28,800起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CGOAEPT8D" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000076/000987/00016483.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CGOAEPT8D" target="_top">華信假期─鄭州之旅登封少林寺龍門石窟雲臺山萬仙山郭亮村八日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CGOAEP88D28B' target='_top'>12/28</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=CGOAEP89102B' target='_top'>1/2</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CGOAEP89116B' target='_top'>1/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CGOAEP89220B' target='_top'>2/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CGOAEP89313B' target='_top'>3/13</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CGOAEPT8D" target="_top">...more</a></div>
				<div class="product_price"><span>$20,800起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /></p></div>
			</div>
		</div>
	</div>
</body>
</html>

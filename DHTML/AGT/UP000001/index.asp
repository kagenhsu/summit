﻿<%
'//===========================================================================================
'//						index.asp
'//-------------------------------------------------------------------------------------------
'//		description		: 此頁面係由自動產生因此不用納入版本
'//-------------------------------------------------------------------------------------------
Response.CacheControl = "no-cache" 
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="robots" content="noindex">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
	<title>國外團體旅遊：中南半島旅遊行程 - 超悠</title>
	<!--產出時間:2018/12/4 下午 11:04:08 / WEB:summittour-->
	<link href='/eweb_summittour/css/reset.css' rel='stylesheet' type='text/css'>
	<link href='/eweb_summittour/css/autopage.css' rel='stylesheet' type='text/css'>
	<script languege="javascript" src="/script/jquery.js"></script>
	<script languege="javascript" src="/eweb_summittour/JS/autopage.js"></script>
</head>
<body>
	<div class="metropolis">
		<div class="title_box" >
			<div class="left_box" id="DAREA00001">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>港澳大陸</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CSXAEPT8C" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000080/000993/00016859.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CSXAEPT8C" target="_top">華信假期─湘西風光張家界鳳凰天空玻璃橋豪華8天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CSXAEP88D20A' target='_top'>12/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CSXAEP89103A' target='_top'>1/3</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=CSXAEP89110A' target='_top'>1/10</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CSXAEP89117A' target='_top'>1/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CSXAEP89228A' target='_top'>2/28</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=CSXAEP89307A' target='_top'>3/7</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CSXAEPT8C" target="_top">...more</a></div>
				<div class="product_price"><span>$17,800起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTUCA08" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000079/001180/00017746.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTUCA08" target="_top">五星希爾頓四姑娘山新都橋木格錯8日(無購物、無自費)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190103A' target='_top'>1/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190115A' target='_top'>1/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190122A' target='_top'>1/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190212A' target='_top'>2/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190223A' target='_top'>2/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190302A' target='_top'>3/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190312A' target='_top'>3/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190319A' target='_top'>3/19</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CTUCA08" target="_top">...more</a></div>
				<div class="product_price"><span>$19,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTUCA08B" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000079/001180/00011696.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTUCA08B" target="_top">五星希爾頓四姑娘山蒸汽小車羅城古鎮8日(無購物、無自費)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190103B' target='_top'>1/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190115B' target='_top'>1/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190122B' target='_top'>1/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190212B' target='_top'>2/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190223B' target='_top'>2/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190302B' target='_top'>3/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190312B' target='_top'>3/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190319B' target='_top'>3/19</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CTUCA08B" target="_top">...more</a></div>
				<div class="product_price"><span>$17,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTUCAPT8B" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000079/000549/00007728.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTUCAPT8B" target="_top">四姑娘山、甲居藏寨、新都橋、海螺溝8日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA181218A' target='_top'>12/18</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA181225A' target='_top'>12/25</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190101A' target='_top'>1/1</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190115C' target='_top'>1/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190129A' target='_top'>1/29</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CTUCAPT8B" target="_top">...more</a></div>
				<div class="product_price"><span>$19,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=INCMUPT8A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000074/000985/00014927.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=INCMUPT8A" target="_top">游遍中國─寧夏沙坡頭108塔東方泰姬瑪哈陵沙湖8日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=INCMUT89427A' target='_top'>4/27</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=INCMUT89525A' target='_top'>5/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=INCMUT89622A' target='_top'>6/22</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=INCMUPT8A" target="_top">...more</a></div>
				<div class="product_price"><span>$21,500起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=INCMUT8A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000074/000985/00017865.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=INCMUT8A" target="_top">游遍中國─塞上寧夏金沙島黃河石林西夏王朝八日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=INCMUT89427B' target='_top'>4/27</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=INCMUT89525B' target='_top'>5/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=INCMUT89622B' target='_top'>6/22</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=INCMUT8A" target="_top">...more</a></div>
				<div class="product_price"><span>$24,500起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=XMNMFPT5A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000086/001004/00011358.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=XMNMFPT5A" target="_top">《遨遊天廈》廈門武夷山雙飛、閩南傳奇秀五日之旅</a></div>
 				<div class="product_description">贈送每人每天1瓶礦泉水</div> 
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=XMNMF190105A' target='_top'>1/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=XMNMF190112A' target='_top'>1/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=XMNMF190223A' target='_top'>2/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=XMNMF190302A' target='_top'>3/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=XMNMF190309A' target='_top'>3/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=XMNMF190316A' target='_top'>3/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=XMNMF190323A' target='_top'>3/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=XMNMF190330A' target='_top'>3/30</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=XMNMFPT5A" target="_top">...more</a></div>
				<div class="product_price"><span>$15,900起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img height="21" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011521.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=NKGCIT8A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000258/000995/00015205.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=NKGCIT8A" target="_top">東方神起─南京無錫嘉興紹興橫店義烏烏鎮海寧句容超值八天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=NKGMUP88D22B' target='_top'>12/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NKGMUP89105B' target='_top'>1/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NKGMUP89119B' target='_top'>1/19</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=NKGMUP89302B' target='_top'>3/2</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=NKGCIT8A" target="_top">...more</a></div>
				<div class="product_price"><span>$16,900起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=NKGMUT8B" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000258/000995/00015205.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=NKGMUT8B" target="_top">東方航空─南京揚州瘦西湖鹽城丹頂鶴徐州句容尊爵八天</a></div>
				<div class="departure_date">
					出發日期：
					<s><a href='/EW/GO/GroupDetail.asp?prodCd=NKGMUP88D22A' target='_top'>12/22</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NKGMUP89112A' target='_top'>1/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NKGMUP89223A' target='_top'>2/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NKGMUP89309A' target='_top'>3/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NKGMUP89316A' target='_top'>3/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NKGMUP89323A' target='_top'>3/23</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=NKGMUT8B" target="_top">...more</a></div>
				<div class="product_price"><span>$20,000起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.PNG	" width="60" /><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008861.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYXJDPR4B" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000085/000547/00014851.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYXJDPR4B" target="_top">海南傳奇─三亞蘭花大世界南天生態植物園鳳凰嶺精選優值四天</a></div>
				<div class="departure_date">
					出發日期：
					<s><a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP48D23A' target='_top'>12/23</a></s> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP48D30A' target='_top'>12/30</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49106A' target='_top'>1/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49113A' target='_top'>1/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49120A' target='_top'>1/20</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49127A' target='_top'>1/27</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49217A' target='_top'>2/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49224A' target='_top'>2/24</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=SYXJDPR4B" target="_top">...more</a></div>
				<div class="product_price"><span>$9,999起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.PNG	" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYXJDPR5A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000085/000547/00014879.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYXJDPR5A" target="_top">海南傳奇─三亞保亭陵水椰田古寨石梅灣奢華饗宴五天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP58D19A' target='_top'>12/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP58D26A' target='_top'>12/26</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59102A' target='_top'>1/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59109A' target='_top'>1/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59116A' target='_top'>1/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59123A' target='_top'>1/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59213A' target='_top'>2/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59220A' target='_top'>2/20</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=SYXJDPR5A" target="_top">...more</a></div>
				<div class="product_price"><span>$18,000起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.PNG	" width="60" /></p></div>
			</div>
		</div>
		<div class="title_box" >
			<div class="left_box" id="DAREA00002">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>東北亞</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=OKAITP4C" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000567/000600/00017349.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=OKAITP4C" target="_top">享戀超值沖繩~腳踏車漫遊、海洋博、首里城、瀨長島四日【超級促銷團】</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=OKAIT181219B' target='_top'>12/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OKAIT190107A' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OKAIT190109A' target='_top'>1/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OKAIT190114A' target='_top'>1/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OKAIT190116A' target='_top'>1/16</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=OKAITP4C" target="_top">...more</a></div>
				<div class="product_price"><span>$11,999起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
		</div>
		<div class="title_box" >
			<div class="left_box" id="DAREA00013">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>促銷行程</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PEKCA05A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000077/001149/00017792.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PEKCA05A" target="_top">皇朝古都北京司馬台長城古北水鎮頤和園五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PEKCA190102B' target='_top'>1/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PEKCA190109B' target='_top'>1/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PEKCA190116B' target='_top'>1/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PEKCA190123B' target='_top'>1/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PEKCA190213B' target='_top'>2/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PEKCA190220B' target='_top'>2/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PEKCA190306B' target='_top'>3/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PEKCA190313B' target='_top'>3/13</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=PEKCA05A" target="_top">...more</a></div>
				<div class="product_price"><span>$18,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PEKCA08A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000077/001149/00013297.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PEKCA08A" target="_top">北京四大文化遺產超值五日(無購物)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PEKCA190102A' target='_top'>1/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PEKCA190109A' target='_top'>1/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PEKCA190116A' target='_top'>1/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PEKCA190123A' target='_top'>1/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PEKCA190213A' target='_top'>2/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PEKCA190220A' target='_top'>2/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PEKCA190306A' target='_top'>3/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PEKCA190313A' target='_top'>3/13</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=PEKCA08A" target="_top">...more</a></div>
				<div class="product_price"><span>$14,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=WNZCA05" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000258/001154/00017856.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=WNZCA05" target="_top">溫州雁蕩山、神仙居、古堰畫鄉、土豪宴五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=WNZCA190105A' target='_top'>1/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=WNZCA190110A' target='_top'>1/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=WNZCA190112A' target='_top'>1/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=WNZCA190117A' target='_top'>1/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=WNZCA190119A' target='_top'>1/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=WNZCA190124A' target='_top'>1/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=WNZCA190126A' target='_top'>1/26</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=WNZCA190214A' target='_top'>2/14</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=WNZCA05" target="_top">...more</a></div>
				<div class="product_price"><span>$16,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
		</div>
	</div>
</body>
</html>

﻿<%
'//===========================================================================================
'//						index.asp
'//-------------------------------------------------------------------------------------------
'//		description		: 此頁面係由自動產生因此不用納入版本
'//-------------------------------------------------------------------------------------------
Response.CacheControl = "no-cache" 
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="robots" content="noindex">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
	<title>國外團體旅遊：旅遊行程 - 超悠</title>
	<!--產出時間:2018/12/4 下午 11:04:18 / WEB:summittour-->
	<link href='/eweb_summittour/css/reset.css' rel='stylesheet' type='text/css'>
	<link href='/eweb_summittour/css/autopage.css' rel='stylesheet' type='text/css'>
	<script languege="javascript" src="/script/jquery.js"></script>
	<script languege="javascript" src="/eweb_summittour/JS/autopage.js"></script>
</head>
<body>
	<div class="metropolis">
		<div class="title_box" >
			<div class="left_box" id="DAREA00001">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>港澳大陸</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CGOAEPR8B" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000076/000987/00010411.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CGOAEPR8B" target="_top">華信假期─賀新年龍門石窟雲臺山萬仙山郭亮村八日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CGOAEP89206A' target='_top'>2/6</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CGOAEPR8B" target="_top">...more</a></div>
				<div class="product_price"><span>$29,800起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CGOAEPT8A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000076/000987/00010411.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CGOAEPT8A" target="_top">華信假期─太行大峽谷龍門石窟雲台山頂級八日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CGOAEP88D28A' target='_top'>12/28</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CGOAEPT8A" target="_top">...more</a></div>
				<div class="product_price"><span>$28,800起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CGOAEPT8D" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000076/000987/00016483.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CGOAEPT8D" target="_top">華信假期─鄭州之旅登封少林寺龍門石窟雲臺山萬仙山郭亮村八日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CGOAEP88D28B' target='_top'>12/28</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=CGOAEP89102B' target='_top'>1/2</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CGOAEP89116B' target='_top'>1/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CGOAEP89220B' target='_top'>2/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CGOAEP89313B' target='_top'>3/13</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CGOAEPT8D" target="_top">...more</a></div>
				<div class="product_price"><span>$20,800起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CSXAEPT8A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000080/000993/00018270.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CSXAEPT8A" target="_top">華信假期─湘江風光張家界鳳凰天空玻璃橋尊爵8日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CSXAEP89321A' target='_top'>3/21</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CSXAEPT8A" target="_top">...more</a></div>
				<div class="product_price"><span>$24,800起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CSXAEPT8B" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000080/000993/00008166.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CSXAEPT8B" target="_top">華信假期─湘媚風光張家界鳳凰天空玻璃橋豪華8天</a></div>
				<div class="departure_date">
					出發日期：
					<s><a href='/EW/GO/GroupDetail.asp?prodCd=CSXAEP89207A' target='_top'>2/7</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CSXAEPT8B" target="_top">...more</a></div>
				<div class="product_price"><span>$25,800起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CSXAEPT8C" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000080/000993/00016859.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CSXAEPT8C" target="_top">華信假期─湘西風光張家界鳳凰天空玻璃橋豪華8天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CSXAEP88D20A' target='_top'>12/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CSXAEP89103A' target='_top'>1/3</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=CSXAEP89110A' target='_top'>1/10</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CSXAEP89117A' target='_top'>1/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CSXAEP89228A' target='_top'>2/28</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=CSXAEP89307A' target='_top'>3/7</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CSXAEPT8C" target="_top">...more</a></div>
				<div class="product_price"><span>$17,800起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTU08TP" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000079/000549/00012385.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTU08TP" target="_top">華信假期─冰川奇景~海螺溝、四姑娘山八天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCIP88D18A' target='_top'>12/18</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CTU08TP" target="_top">...more</a></div>
				<div class="product_price"><span>$21,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTUCA08" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000079/001180/00017746.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTUCA08" target="_top">五星希爾頓四姑娘山新都橋木格錯8日(無購物、無自費)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190103A' target='_top'>1/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190115A' target='_top'>1/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190122A' target='_top'>1/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190212A' target='_top'>2/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190223A' target='_top'>2/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190302A' target='_top'>3/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190312A' target='_top'>3/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190319A' target='_top'>3/19</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CTUCA08" target="_top">...more</a></div>
				<div class="product_price"><span>$19,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTUCA08B" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000079/001180/00011696.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTUCA08B" target="_top">五星希爾頓四姑娘山蒸汽小車羅城古鎮8日(無購物、無自費)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190103B' target='_top'>1/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190115B' target='_top'>1/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190122B' target='_top'>1/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190212B' target='_top'>2/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190223B' target='_top'>2/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190302B' target='_top'>3/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190312B' target='_top'>3/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190319B' target='_top'>3/19</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CTUCA08B" target="_top">...more</a></div>
				<div class="product_price"><span>$17,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTUCAPT8A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000079/000549/00007741.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTUCAPT8A" target="_top">稻城.亞丁.新都橋.丹巴.四姑娘山八日</a></div>
 				<div class="product_description">贈送紅景天口服液一盒</div> 
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA181217A' target='_top'>12/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA181231A' target='_top'>12/31</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190107A' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190312C' target='_top'>3/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190323A' target='_top'>3/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190326A' target='_top'>3/26</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CTUCAPT8A" target="_top">...more</a></div>
				<div class="product_price"><span>$22,900起</span></div>
   				<div class="product_offer"><p><img alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" style="height: 22px; width: 60px" /><img height="21" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011521.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTUCAPT8B" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000079/000549/00007728.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTUCAPT8B" target="_top">四姑娘山、甲居藏寨、新都橋、海螺溝8日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA181218A' target='_top'>12/18</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA181225A' target='_top'>12/25</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190101A' target='_top'>1/1</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190115C' target='_top'>1/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTUCA190129A' target='_top'>1/29</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CTUCAPT8B" target="_top">...more</a></div>
				<div class="product_price"><span>$19,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=INCFEPT8A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000074/000985/00014927.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=INCFEPT8A" target="_top">游遍中國─寧夏沙坡頭108塔東方泰姬瑪哈陵溫泉8日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=INCMUT89122A' target='_top'>1/22</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=INCMUT89226A' target='_top'>2/26</a></s> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=INCMUT89326A' target='_top'>3/26</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=INCFEPT8A" target="_top">...more</a></div>
				<div class="product_price"><span>$18,500起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=INCMUPT8A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000074/000985/00014927.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=INCMUPT8A" target="_top">游遍中國─寧夏沙坡頭108塔東方泰姬瑪哈陵沙湖8日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=INCMUT89427A' target='_top'>4/27</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=INCMUT89525A' target='_top'>5/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=INCMUT89622A' target='_top'>6/22</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=INCMUPT8A" target="_top">...more</a></div>
				<div class="product_price"><span>$21,500起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=INCMUT8A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000074/000985/00017865.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=INCMUT8A" target="_top">游遍中國─塞上寧夏金沙島黃河石林西夏王朝八日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=INCMUT89427B' target='_top'>4/27</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=INCMUT89525B' target='_top'>5/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=INCMUT89622B' target='_top'>6/22</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=INCMUT8A" target="_top">...more</a></div>
				<div class="product_price"><span>$24,500起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=TAO0803" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000073/000982/00018167.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=TAO0803" target="_top">游遍中國─歐陸青島天鵝湖童話屋泉城泰山祈福8日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=TAOCIT89206A' target='_top'>2/6</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=TAO0803" target="_top">...more</a></div>
				<div class="product_price"><span>$29,900起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=TAOCIT8A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000073/000982/00011962.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=TAOCIT8A" target="_top">華信假期─山東青島琴島之眼濟南泰山封禪秀臺兒莊八天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=TAOCIP89109A' target='_top'>1/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=TAOCIP89130A' target='_top'>1/30</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=TAOCIP89227A' target='_top'>2/27</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=TAOCIP89313A' target='_top'>3/13</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=TAOCIT8A" target="_top">...more</a></div>
				<div class="product_price"><span>$22,800起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=URCMFPT12A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000065/001160/00013773.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=URCMFPT12A" target="_top">南疆風情.帕米爾高原.卡拉庫力湖.溫宿大峽谷12天(廈門)</a></div>
 				<div class="product_description">贈送每人一頂維吾爾小花帽</div> 
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=URCMF181217A' target='_top'>12/17</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=URCMFPT12A" target="_top">...more</a></div>
				<div class="product_price"><span>$43,900起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img height="21" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011521.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=WUHCIP8B" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000081/000997/00011822.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=WUHCIP8B" target="_top">華信假期─天下第一仙山武當山兩壩一峽８日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=WUHCIP89124A' target='_top'>1/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=WUHCIP89131A' target='_top'>1/31</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=WUHCIP89214A' target='_top'>2/14</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=WUHCIP89314A' target='_top'>3/14</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=WUHCIP8B" target="_top">...more</a></div>
				<div class="product_price"><span>$26,800起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=WUHCIT8C" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000081/000997/00010002.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=WUHCIT8C" target="_top">華信假期─四季浪歌瀑布絕壁龍洞恩施大峽谷三峽人家八天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=WUHCIP89124B' target='_top'>1/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=WUHCIP89131B' target='_top'>1/31</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=WUHCIP89214B' target='_top'>2/14</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=WUHCIP89314B' target='_top'>3/14</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=WUHCIT8C" target="_top">...more</a></div>
				<div class="product_price"><span>$24,800起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=XMN04" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000086/001155/00017747.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=XMN04" target="_top">廈門靈玲馬戲城平和土樓4日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=XMN04190228A' target='_top'>2/28</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=XMN04" target="_top">...more</a></div>
				<div class="product_price"><span>$15,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=XMNMFPT5A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000086/001004/00011358.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=XMNMFPT5A" target="_top">《遨遊天廈》廈門武夷山雙飛、閩南傳奇秀五日之旅</a></div>
 				<div class="product_description">贈送每人每天1瓶礦泉水</div> 
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=XMNMF190105A' target='_top'>1/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=XMNMF190112A' target='_top'>1/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=XMNMF190223A' target='_top'>2/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=XMNMF190302A' target='_top'>3/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=XMNMF190309A' target='_top'>3/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=XMNMF190316A' target='_top'>3/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=XMNMF190323A' target='_top'>3/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=XMNMF190330A' target='_top'>3/30</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=XMNMFPT5A" target="_top">...more</a></div>
				<div class="product_price"><span>$15,900起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img height="21" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011521.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=HGH0502" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000258/000995/00017924.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=HGH0502" target="_top">華信假期─杭州海寧上海楓涇古鎮尊貴五天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=HGHAEP59206A' target='_top'>2/6</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=HGH0502" target="_top">...more</a></div>
				<div class="product_price"><span>$26,000起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=HGHAEPR5A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000258/000995/00008171.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=HGHAEPR5A" target="_top">華信假期-黃山奇景千島湖西遞村杭州西湖五日</a></div>
				<div class="departure_date">
					出發日期：
					<s><a href='/EW/GO/GroupDetail.asp?prodCd=HGHAEP59109A' target='_top'>1/9</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=HGHAEPR5A" target="_top">...more</a></div>
				<div class="product_price"><span>$18,500起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.PNG	" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=MFM0402" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000087/001006/00008187.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=MFM0402" target="_top">游遍中國~澳珠長隆海洋王國歡樂四日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=MFM04181209A' target='_top'>12/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=MFM04190106A' target='_top'>1/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=MFM04190113A' target='_top'>1/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=MFM04190210A' target='_top'>2/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=MFM04190217A' target='_top'>2/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=MFM04190303A' target='_top'>3/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=MFM04190310A' target='_top'>3/17</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=MFM0402" target="_top">...more</a></div>
				<div class="product_price"><span>$15,500起</span></div>
   				<div class="product_offer"><p><span style="color:#FFFFFF;"><span style="background-color:#FF8C00;">台中出發</span></span></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=NKGAET8A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000258/000995/00015205.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=NKGAET8A" target="_top">東方神起─南京揚州鹽城徐州句容尊爵逍遙八天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=NKGMUP89202A' target='_top'>2/2</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=NKGAET8A" target="_top">...more</a></div>
				<div class="product_price"><span>$27,000起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=NKGCIT8A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000258/000995/00015205.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=NKGCIT8A" target="_top">東方神起─南京無錫嘉興紹興橫店義烏烏鎮海寧句容超值八天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=NKGMUP88D22B' target='_top'>12/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NKGMUP89105B' target='_top'>1/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NKGMUP89119B' target='_top'>1/19</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=NKGMUP89302B' target='_top'>3/2</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=NKGCIT8A" target="_top">...more</a></div>
				<div class="product_price"><span>$16,900起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=NKGMUT8B" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000258/000995/00015205.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=NKGMUT8B" target="_top">東方航空─南京揚州瘦西湖鹽城丹頂鶴徐州句容尊爵八天</a></div>
				<div class="departure_date">
					出發日期：
					<s><a href='/EW/GO/GroupDetail.asp?prodCd=NKGMUP88D22A' target='_top'>12/22</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NKGMUP89112A' target='_top'>1/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NKGMUP89223A' target='_top'>2/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NKGMUP89309A' target='_top'>3/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NKGMUP89316A' target='_top'>3/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NKGMUP89323A' target='_top'>3/23</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=NKGMUT8B" target="_top">...more</a></div>
				<div class="product_price"><span>$20,000起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.PNG	" width="60" /><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008861.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR4A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000085/000547/00014888.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR4A" target="_top">海南傳奇~三亞Club Med玫瑰谷海棠廣場椰田古寨精彩四天</a></div>
				<div class="departure_date">
					出發日期：
					<s><a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP48D23C' target='_top'>12/23</a></s> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP48D30C' target='_top'>12/30</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49106C' target='_top'>1/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49113C' target='_top'>1/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49120C' target='_top'>1/20</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49127C' target='_top'>1/27</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49217C' target='_top'>2/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49224C' target='_top'>2/24</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=SYX3UPR4A" target="_top">...more</a></div>
				<div class="product_price"><span>$20,500起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.PNG	" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR4C" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000085/000547/00010269.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR4C" target="_top">海南傳奇─三亞臨春嶺森林公園陵水超值四天(買一送一)</a></div>
 				<div class="product_description">稅外$2500/人.買一送一</div> 
				<div class="departure_date">
					出發日期：
					<s><a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP48D23S' target='_top'>12/23</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=SYX3UPR4C" target="_top">...more</a></div>
				<div class="product_price"><span>$2,900起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR4D" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000085/000547/00017947.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR4D" target="_top">海南傳奇─玩翻三亞水上活動浪漫風帆精彩搖滾之旅四天</a></div>
 				<div class="product_description">送網卡</div> 
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49106R' target='_top'>1/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49113R' target='_top'>1/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49120R' target='_top'>1/20</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49127R' target='_top'>1/27</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49217R' target='_top'>2/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49224R' target='_top'>2/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49303R' target='_top'>3/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49310R' target='_top'>3/10</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=SYX3UPR4D" target="_top">...more</a></div>
				<div class="product_price"><span>$12,000起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR4E" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000085/000547/00017978.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR4E" target="_top">海南傳奇─三亞全程五星希爾頓興隆泡湯趣四日(稅外2500)</a></div>
 				<div class="product_description">稅外$2500/人</div> 
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49106H' target='_top'>1/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49113H' target='_top'>1/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49120H' target='_top'>1/20</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49127H' target='_top'>1/27</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49217H' target='_top'>2/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49224H' target='_top'>2/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49303H' target='_top'>3/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49310H' target='_top'>3/10</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=SYX3UPR4E" target="_top">...more</a></div>
				<div class="product_price"><span>$4,400起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR5B" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000085/000547/00014852.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR5B" target="_top">海南傳奇─三亞海口馮小剛電影公社興隆陵水低調奢華五天</a></div>
				<div class="departure_date">
					出發日期：
					<s><a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59206A' target='_top'>2/6</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=SYX3UPR5B" target="_top">...more</a></div>
				<div class="product_price"><span>$25,500起</span></div>
   				<div class="product_offer"><p><span style="color:#FFFFFF;"><span style="background-color:#FF8C00;">台中出發</span></span></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR5C" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000085/000547/00014852.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR5C" target="_top">海南傳奇─三亞陵水特選優值五天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59130A' target='_top'>1/30</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=SYX3UPR5C" target="_top">...more</a></div>
				<div class="product_price"><span>$16,800起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR5D" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000085/000547/00017929.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR5D" target="_top">海南傳奇─三亞Club Med陵水奢華五天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59102C' target='_top'>1/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59109C' target='_top'>1/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59116C' target='_top'>1/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59130C' target='_top'>1/30</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59213C' target='_top'>2/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59220C' target='_top'>2/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59306C' target='_top'>3/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59313C' target='_top'>3/13</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=SYX3UPR5D" target="_top">...more</a></div>
				<div class="product_price"><span>$25,300起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR5E" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000085/000547/00017949.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR5E" target="_top">海南傳奇─玩翻三亞大東海浪漫風帆萬寧石梅灣五日</a></div>
 				<div class="product_description">送網卡</div> 
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59102R' target='_top'>1/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59109R' target='_top'>1/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59116R' target='_top'>1/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59130R' target='_top'>1/30</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59213R' target='_top'>2/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59220R' target='_top'>2/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59306R' target='_top'>3/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59313R' target='_top'>3/13</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=SYX3UPR5E" target="_top">...more</a></div>
				<div class="product_price"><span>$17,000起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR5F" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000085/000547/00017977.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR5F" target="_top">海南傳奇─三亞全程五星希爾頓興隆泡湯趣五天(稅外2500)</a></div>
 				<div class="product_description">稅外$2500/人</div> 
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP58D19H' target='_top'>12/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59102H' target='_top'>1/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59109H' target='_top'>1/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59116H' target='_top'>1/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59130H' target='_top'>1/30</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59213H' target='_top'>2/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59220H' target='_top'>2/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59306H' target='_top'>3/6</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=SYX3UPR5F" target="_top">...more</a></div>
				<div class="product_price"><span>$5,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYXJDPR4A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000085/000547/00017789.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYXJDPR4A" target="_top">海南傳奇─三亞鳳凰嶺陵水椰田古寨特選優值四天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49203A' target='_top'>2/3</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=SYXJDPR4A" target="_top">...more</a></div>
				<div class="product_price"><span>$17,000起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.PNG	" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYXJDPR4B" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000085/000547/00014851.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYXJDPR4B" target="_top">海南傳奇─三亞蘭花大世界南天生態植物園鳳凰嶺精選優值四天</a></div>
				<div class="departure_date">
					出發日期：
					<s><a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP48D23A' target='_top'>12/23</a></s> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP48D30A' target='_top'>12/30</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49106A' target='_top'>1/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49113A' target='_top'>1/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49120A' target='_top'>1/20</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49127A' target='_top'>1/27</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49217A' target='_top'>2/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49224A' target='_top'>2/24</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=SYXJDPR4B" target="_top">...more</a></div>
				<div class="product_price"><span>$9,999起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.PNG	" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYXJDPR5A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000085/000547/00014879.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYXJDPR5A" target="_top">海南傳奇─三亞保亭陵水椰田古寨石梅灣奢華饗宴五天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP58D19A' target='_top'>12/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP58D26A' target='_top'>12/26</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59102A' target='_top'>1/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59109A' target='_top'>1/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59116A' target='_top'>1/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59123A' target='_top'>1/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59213A' target='_top'>2/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59220A' target='_top'>2/20</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=SYXJDPR5A" target="_top">...more</a></div>
				<div class="product_price"><span>$18,000起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.PNG	" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYXJDPR5B" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000085/000547/00008345.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYXJDPR5B" target="_top">海南傳奇─三亞Club Med宋城千古情陵水奢華五天</a></div>
				<div class="departure_date">
					出發日期：
					<s><a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59206B' target='_top'>2/6</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=SYXJDPR5B" target="_top">...more</a></div>
				<div class="product_price"><span>$37,500起</span></div>
   				<div class="product_offer"><p><span style="color:#FFFFFF;"><span style="background-color:#FF8C00;">台中出發</span></span></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=WUXAEPR5A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000258/000995/00017367.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=WUXAEPR5A" target="_top">華信假期─無錫泰州鹽城蘇北精華尊貴五天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=WUXAEP59107A' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=WUXAEP59218A' target='_top'>2/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=WUXAEP59304A' target='_top'>3/4</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=WUXAEP59318A' target='_top'>3/18</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=WUXAEPR5A" target="_top">...more</a></div>
				<div class="product_price"><span>$16,000起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=WUXAEPR5B" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000258/000995/00011499.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=WUXAEPR5B" target="_top">游遍中國─無錫杭州嘉興上海五天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=WUXAER59204A' target='_top'>2/4</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=WUXAEPR5B" target="_top">...more</a></div>
				<div class="product_price"><span>$23,500起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.png" width="60" /><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011383.png"/></p></div>
			</div>
		</div>
		<div class="title_box" >
			<div class="left_box" id="DAREA00002">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>東北亞</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CJUITP04A" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001093/001096/00017374.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CJUITP04A" target="_top">PAK輕鬆遊三多島濟州4日漢拏山國家公園〮城山日出峰〮</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CJUIT181219A' target='_top'>12/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CJUIT181225A' target='_top'>12/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CJUIT181226A' target='_top'>12/26</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CJUIT190101A' target='_top'>1/1</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CJUIT190102A' target='_top'>1/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CJUIT190108A' target='_top'>1/8</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CJUIT190109A' target='_top'>1/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CJUIT190115A' target='_top'>1/15</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=CJUITP04A" target="_top">...more</a></div>
				<div class="product_price"><span>$11,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CJUITP05A" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001093/001096/00017371.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CJUITP05A" target="_top">PAK輕鬆遊三多島濟州5日3D奧妙藝術博物館。</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CJUIT181214A' target='_top'>12/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CJUIT181215A' target='_top'>12/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CJUIT181221A' target='_top'>12/21</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CJUIT181222A' target='_top'>12/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CJUIT190112A' target='_top'>1/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CJUIT190125A' target='_top'>1/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CJUIT190126A' target='_top'>1/26</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CJUITP05A" target="_top">...more</a></div>
				<div class="product_price"><span>$15,500起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTSCIP5D" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000557/000559/00007942.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTSCIP5D" target="_top">【浪漫北海道】百萬夜景 小丑漢堡 浪漫小樽物語 五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CTSCI181220A' target='_top'>12/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTSCI190106A' target='_top'>1/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTSCI190110A' target='_top'>1/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTSCI190113A' target='_top'>1/13</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CTSCIP5D" target="_top">...more</a></div>
				<div class="product_price"><span>$27,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTSCIP5E" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000557/000559/00010753.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTSCIP5E" target="_top">【銀色北海道】流冰奇航 雪上活動 螃蟹美食五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CTSCI190120A' target='_top'>1/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTSCI190124A' target='_top'>1/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTSCI190127A' target='_top'>1/27</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTSCI190131A' target='_top'>1/31</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTSCI190214A' target='_top'>2/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTSCI190217A' target='_top'>2/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTSCI190221A' target='_top'>2/21</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTSCI190224A' target='_top'>2/24</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CTSCIP5E" target="_top">...more</a></div>
				<div class="product_price"><span>$35,800起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTSCIP5F" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000557/000559/00017783.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTSCIP5F" target="_top">【新春北海道】流冰奇航 雪上活動 溫泉饗宴五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CTSCI190205A' target='_top'>2/5</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CTSCIP5F" target="_top">...more</a></div>
				<div class="product_price"><span>$60,800起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011383.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTSMMP5A" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000557/000559/00015057.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTSMMP5A" target="_top">【悠閒時光】北海道雪國祭典~道東阿寒湖、層雲峽、破冰船、玩雪四合一、三溫泉5日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CTSMM190206A' target='_top'>2/6</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CTSMMP5A" target="_top">...more</a></div>
				<div class="product_price"><span>$56,800起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011383.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTSMMP6A" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000557/000559/00017850.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTSMMP6A" target="_top">【悠閒時光】北海道雪國祭典~道東阿寒湖、層雲峽、破冰船、玩雪四合一、三溫泉6日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CTSMM190203A' target='_top'>2/3</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CTSMMP6A" target="_top">...more</a></div>
				<div class="product_price"><span>$56,800起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img alt="" height="21" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011383.png" width="61" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=FUKBRP05A" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000569/000603/00015059.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=FUKBRP05A" target="_top">輕遊九州鐵道雙溫泉五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=FUKBR181223A' target='_top'>12/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=FUKBR190107A' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=FUKBR190129A' target='_top'>1/29</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=FUKBR190215A' target='_top'>2/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=FUKBR190218A' target='_top'>2/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=FUKBR190222A' target='_top'>2/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=FUKBR190304A' target='_top'>3/4</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=FUKBR190318A' target='_top'>3/18</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=FUKBRP05A" target="_top">...more</a></div>
				<div class="product_price"><span>$24,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=FUKCIP5B" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000569/000603/00017346.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=FUKCIP5B" target="_top">【戲雪九州】旅人電車 和服體驗 長崎纜車五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=FUKCI181218A' target='_top'>12/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=FUKCI181225A' target='_top'>12/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=FUKCI190103A' target='_top'>1/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=FUKCI190108A' target='_top'>1/8</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=FUKCI190110A' target='_top'>1/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=FUKCI190115A' target='_top'>1/15</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=FUKCIP5B" target="_top">...more</a></div>
				<div class="product_price"><span>$25,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=FUKCIP5C" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000569/000603/00015775.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=FUKCIP5C" target="_top">【FUN寒假】豪斯登堡 旅人電車 和服體驗 五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=FUKCI190118A' target='_top'>1/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=FUKCI190122A' target='_top'>1/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=FUKCI190125A' target='_top'>1/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=FUKCI190129A' target='_top'>1/29</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=FUKCIP5C" target="_top">...more</a></div>
				<div class="product_price"><span>$31,800起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=HKDBRP05B" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000557/001166/00015776.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=HKDBRP05B" target="_top">北海道雪在燒5日(百萬夜景.北極星觀景列車.雪樂園.摩天輪.雙纜車.三大螃蟹)-函館/千歲</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=HKDBR181214A' target='_top'>12/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=HKDBR181217A' target='_top'>12/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=HKDBR181221A' target='_top'>12/21</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=HKDBRP05B" target="_top">...more</a></div>
				<div class="product_price"><span>$29,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=ICNBRP5A" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001093/001096/00008417.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=ICNBRP5A" target="_top">BR-韓國滑雪樂悠悠5日(歡樂滑雪.4D藝術館.樂天世界.樂天水族館.通仁市場.幻多奇秀)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR181219A' target='_top'>12/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR181230A' target='_top'>12/30</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR190104A' target='_top'>1/4</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR190110A' target='_top'>1/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR190117A' target='_top'>1/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR190210A' target='_top'>2/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR190211A' target='_top'>2/11</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR190215A' target='_top'>2/15</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=ICNBRP5A" target="_top">...more</a></div>
				<div class="product_price"><span>$12,000起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=ICNBRP5B" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001093/001096/00017573.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=ICNBRP5B" target="_top">BR-京畿SUPER SKI銀雪韓國5日(歡樂滑雪.光明洞窟.愛寶樂園.格雷萬蠟像館.拌飯秀)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR181221A' target='_top'>12/21</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR181225A' target='_top'>12/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR190101A' target='_top'>1/1</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR190102A' target='_top'>1/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR190108A' target='_top'>1/8</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR190109A' target='_top'>1/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR190115A' target='_top'>1/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR190116A' target='_top'>1/16</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=ICNBRP5B" target="_top">...more</a></div>
				<div class="product_price"><span>$12,500起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=ICNKEP5B" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001093/001096/00017412.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=ICNKEP5B" target="_top">KE-【莎朗嘿呦KOREA】特選冰釣~長腳蟹餐.鮑魚一隻雞.三大活動溜冰滑雪冰釣.空中花園.汗蒸幕韓國5日(午去晚回)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=ICNKE190114B' target='_top'>1/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNKE190119B' target='_top'>1/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNKE190121B' target='_top'>1/21</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=ICNKEP5B" target="_top">...more</a></div>
				<div class="product_price"><span>$16,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=ICNKEP5C" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001093/001096/00016375.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=ICNKEP5C" target="_top">KE-【莎朗嘿呦KOREA】季節冰釣~長腳蟹餐.鮑魚一隻雞.三大活動溜冰滑雪冰釣.東大門.汗蒸幕韓國5日(晚去晚回)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=ICNKE190121A' target='_top'>1/21</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=ICNKE190206A' target='_top'>2/6</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=ICNKEP5C" target="_top">...more</a></div>
				<div class="product_price"><span>$15,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=ICNTGP05A" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001093/001096/00017445.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=ICNTGP05A" target="_top">【泰航假期】冬遊Running Man浪漫雪景纜車冰釣韓國5日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=ICNTG190103A' target='_top'>1/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNTG190109A' target='_top'>1/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNTG190110A' target='_top'>1/10</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=ICNTG190116A' target='_top'>1/16</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNTG190201A' target='_top'>2/1</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNTG190202A' target='_top'>2/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNTG190203A' target='_top'>2/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNTG190205A' target='_top'>2/5</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=ICNTGP05A" target="_top">...more</a></div>
				<div class="product_price"><span>$14,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=ICNTGP05B" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001093/001096/00017430.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=ICNTGP05B" target="_top">【泰航假期】旗艦韓國饕客米其林一星餐玩雪冰釣5日遊</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=ICNTG190118A' target='_top'>1/18</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=ICNTG190122B' target='_top'>1/22</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNTG190125A' target='_top'>1/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNTG190204A' target='_top'>2/4</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNTG190206B' target='_top'>2/6</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=ICNTG190210B' target='_top'>2/10</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNTG190213B' target='_top'>2/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNTG190215B' target='_top'>2/15</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=ICNTGP05B" target="_top">...more</a></div>
				<div class="product_price"><span>$29,400起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=KIXCXP5A" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000564/000583/00008253.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=KIXCXP5A" target="_top">【悠閒時光】日本南紀豪華三溫泉美食版5日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=KIXCX190110A' target='_top'>1/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KIXCX190117A' target='_top'>1/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KIXCX190221A' target='_top'>2/21</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KIXCX190302A' target='_top'>3/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KIXCX190309A' target='_top'>3/9</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=KIXCXP5A" target="_top">...more</a></div>
				<div class="product_price"><span>$29,800起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=KIXCXP6A" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000564/000583/00017863.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=KIXCXP6A" target="_top">【悠閒時光】大阪海之京都天橋立~三方五湖彩虹路(搭乘纜車).美山合掌村.歡樂環球影城.溫泉6日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=KIXCX190126A' target='_top'>1/26</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KIXCX190130A' target='_top'>1/30</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KIXCX190208A' target='_top'>2/8</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KIXCX190209A' target='_top'>2/9</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=KIXCXP6A" target="_top">...more</a></div>
				<div class="product_price"><span>$37,800起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=KIXCXP6B" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000564/000583/00017875.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=KIXCXP6B" target="_top">【悠閒時光】四國淡路夢之紀行~小豆島‧松山城公園‧祖谷溪葛藤橋‧鳴門漩渦‧道後溫泉六日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=KIXCX190111A' target='_top'>1/11</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KIXCX190116A' target='_top'>1/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KIXCX190302B' target='_top'>3/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KIXCX190309B' target='_top'>3/9</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=KIXCXP6B" target="_top">...more</a></div>
				<div class="product_price"><span>$39,800起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=NGOCIP5D" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000565/000590/00017337.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=NGOCIP5D" target="_top">【戲雪名古屋】穗高纜車 童話合掌村 玩雪去 五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=NGOCI190104A' target='_top'>1/4</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NGOCI190106A' target='_top'>1/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NGOCI190111A' target='_top'>1/11</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NGOCI190113A' target='_top'>1/13</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=NGOCIP5D" target="_top">...more</a></div>
				<div class="product_price"><span>$26,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=NGOCIP5E" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000565/000590/00015906.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=NGOCIP5E" target="_top">【FUN寒假】樂高樂園 童話合掌村 玩雪趣 五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=NGOCI190116A' target='_top'>1/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NGOCI190123A' target='_top'>1/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NGOCI190130A' target='_top'>1/30</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=NGOCIP5E" target="_top">...more</a></div>
				<div class="product_price"><span>$33,800起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=NGOCXP5A" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000565/000590/00017337.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=NGOCXP5A" target="_top">【悠閒時光】雪白北陸童話村~白川鄉合掌村、絕景星空、兼六園、戲雪樂、三溫泉5日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=NGOCX190129A' target='_top'>1/29</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=NGOCXP5A" target="_top">...more</a></div>
				<div class="product_price"><span>$33,800起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=NRTCIP5B" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000565/000590/00017804.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=NRTCIP5B" target="_top">【浪漫花語】繽紛舞曲~足立藤花 粉蝶花海 粉紅芝櫻五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=NRTCI190416A' target='_top'>4/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NRTCI190418A' target='_top'>4/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NRTCI190420A' target='_top'>4/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NRTCI190422A' target='_top'>4/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NRTCI190511A' target='_top'>5/11</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NRTCI190513A' target='_top'>5/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NRTCI190515A' target='_top'>5/15</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=NRTCIP5B" target="_top">...more</a></div>
				<div class="product_price"><span>$33,900起</span></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=NRTCIP6A" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000565/000590/00014218.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=NRTCIP6A" target="_top">【雪壁絕景~立山黑部】童話合掌村 藤花伴芝櫻六日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=NRTCI190415A' target='_top'>4/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NRTCI190417A' target='_top'>4/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NRTCI190419A' target='_top'>4/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NRTCI190421A' target='_top'>4/21</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NRTCI190510A' target='_top'>5/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NRTCI190512A' target='_top'>5/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NRTCI190514A' target='_top'>5/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NRTCI190516A' target='_top'>5/16</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=NRTCIP6A" target="_top">...more</a></div>
				<div class="product_price"><span>$36,800起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=NRTCXP6A" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000566/000593/00017823.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=NRTCXP6A" target="_top">【悠閒時光】日本東北奇幻樹冰~藏王樹冰纜車‧秋田內陸鐵道‧松島遊船.三溫泉6日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=NRTCX190127A' target='_top'>1/27</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=NRTCXP6A" target="_top">...more</a></div>
				<div class="product_price"><span>$42,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=NRTCXP7A" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000566/000593/00017822.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=NRTCXP7A" target="_top">【悠閒時光】日本東北奇幻樹冰~星野渡假村‧藏王樹冰纜車‧秋田內陸鐵道‧松島遊船.溫泉7日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=NRTCX190130A' target='_top'>1/30</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=NRTCXP7A" target="_top">...more</a></div>
				<div class="product_price"><span>$55,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=OKAITP4B" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000567/000600/00016600.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=OKAITP4B" target="_top">沖繩兜兜風～恐龍森林公園、海洋博、萬座毛、玉泉洞四日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=OKAIT181219A' target='_top'>12/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OKAIT181222A' target='_top'>12/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OKAIT181224A' target='_top'>12/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OKAIT181226A' target='_top'>12/26</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OKAIT181231A' target='_top'>12/31</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OKAIT190105A' target='_top'>1/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OKAIT190107B' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OKAIT190109B' target='_top'>1/9</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=OKAITP4B" target="_top">...more</a></div>
				<div class="product_price"><span>$16,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=OKAITP4C" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000567/000600/00017349.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=OKAITP4C" target="_top">享戀超值沖繩~腳踏車漫遊、海洋博、首里城、瀨長島四日【超級促銷團】</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=OKAIT181219B' target='_top'>12/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OKAIT190107A' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OKAIT190109A' target='_top'>1/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OKAIT190114A' target='_top'>1/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OKAIT190116A' target='_top'>1/16</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=OKAITP4C" target="_top">...more</a></div>
				<div class="product_price"><span>$11,999起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=OSABRP05A" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000564/000583/00018160.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=OSABRP05A" target="_top">浪漫京阪神樂園5日(舞子步道.清水寺.嵐山竹林步道.環球影城.神戶港灣散策)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=OSABR181219A' target='_top'>12/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OSABR181226A' target='_top'>12/26</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OSABR181231A' target='_top'>12/31</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=OSABRP05A" target="_top">...more</a></div>
				<div class="product_price"><span>$30,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=OSABRP05B" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000564/000583/00018166.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=OSABRP05B" target="_top">浪漫京阪神戲雪5日(兩晚五星.六甲山夜景.戲雪.壽司DIY.嵐山渡月橋.環球影城.和服體驗)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=OSABR190101A' target='_top'>1/1</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OSABR190105A' target='_top'>1/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OSABR190108A' target='_top'>1/8</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OSABR190112A' target='_top'>1/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OSABR190216A' target='_top'>2/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OSABR190219A' target='_top'>2/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OSABR190223A' target='_top'>2/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OSABR190319A' target='_top'>3/19</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=OSABRP05B" target="_top">...more</a></div>
				<div class="product_price"><span>$32,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=OSABRP06A" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000564/000583/00018190.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=OSABRP06A" target="_top">山陰山陽6日(鳥取砂丘.鬼太郎通路.水木茂記念館.柯南的故鄉.伊根舟屋.伊根湾海鷗號遊覧船.天橋立)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=OSABR190106A' target='_top'>1/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OSABR190113A' target='_top'>1/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OSABR190210A' target='_top'>2/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OSABR190217A' target='_top'>2/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OSABR190303A' target='_top'>3/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OSABR190310A' target='_top'>3/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OSABR190317A' target='_top'>3/17</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=OSABRP06A" target="_top">...more</a></div>
				<div class="product_price"><span>$35,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSBXP04A" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001394/001399/00017989.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSBXP04A" target="_top">【哇!釜山】鐵道自行車.天空步道.汗蒸幕特色美食四日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190105G' target='_top'>1/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190112G' target='_top'>1/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190119G' target='_top'>1/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190126G' target='_top'>1/26</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190128E' target='_top'>1/28</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190216G' target='_top'>2/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190309G' target='_top'>3/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190316H' target='_top'>3/16</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=PUSBXP04A" target="_top">...more</a></div>
				<div class="product_price"><span>$8,999起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSBXP04B" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001394/001399/00018010.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSBXP04B" target="_top">【哇!韓國】釜山、汗蒸幕、滑雪包雪具、八色烤肉四日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190105B' target='_top'>1/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190112B' target='_top'>1/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190119B' target='_top'>1/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190126B' target='_top'>1/26</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190216B' target='_top'>2/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190309B' target='_top'>3/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190316C' target='_top'>3/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190323B' target='_top'>3/23</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=PUSBXP04B" target="_top">...more</a></div>
				<div class="product_price"><span>$8,499起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSBXP05A" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001394/001399/00018021.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSBXP05A" target="_top">【哇!韓國】釜山雙天空步道、汗蒸幕、包雪具、體驗樂園、甘川洞、樂天超市五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX181208A' target='_top'>12/8</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX181215A' target='_top'>12/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX181222A' target='_top'>12/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190105C' target='_top'>1/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190107A' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190112C' target='_top'>1/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190114A' target='_top'>1/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190118A' target='_top'>1/18</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=PUSBXP05A" target="_top">...more</a></div>
				<div class="product_price"><span>$9,888起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSBXP05C" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001394/001399/00018029.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSBXP05C" target="_top">【哇!韓國】韓國霸氣首釜愛寶樂園海鮮吃到飽五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190105D' target='_top'>1/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190107B' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190112D' target='_top'>1/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190114B' target='_top'>1/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190119D' target='_top'>1/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190121B' target='_top'>1/21</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190126D' target='_top'>1/26</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190128B' target='_top'>1/28</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=PUSBXP05C" target="_top">...more</a></div>
				<div class="product_price"><span>$10,380起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSBXP05D" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001093/001096/00016032.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSBXP05D" target="_top">【哇!韓國。釜山】韓國全程五星樂天世界滑雪包雪具海鮮吃到飽看星月搖滾秀五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190105F' target='_top'>1/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190107D' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190112F' target='_top'>1/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190114D' target='_top'>1/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190119F' target='_top'>1/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190121D' target='_top'>1/21</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190123B' target='_top'>1/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190126F' target='_top'>1/26</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=PUSBXP05D" target="_top">...more</a></div>
				<div class="product_price"><span>$11,888起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSBXP05E" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001394/001403/00018082.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSBXP05E" target="_top">【哇!釜山】釜山雙天空步道、汗蒸幕、鐵道自行車、紅酒洞窟、甘川洞、樂天超市五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190105E' target='_top'>1/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190107C' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190112E' target='_top'>1/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190114C' target='_top'>1/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190119E' target='_top'>1/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190121C' target='_top'>1/21</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190126E' target='_top'>1/26</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190128C' target='_top'>1/28</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=PUSBXP05E" target="_top">...more</a></div>
				<div class="product_price"><span>$8,800起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSBXP05F" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001394/001403/00018082.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSBXP05F" target="_top">霸氣釜山雙天空步道汗蒸幕雙BUFFET五日-2/28連休</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190227A' target='_top'>2/27</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190228A' target='_top'>2/28</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190301A' target='_top'>3/1</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=PUSBXP05F" target="_top">...more</a></div>
				<div class="product_price"><span>$13,800起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSBXP05G" target="_top">春節版~【哇!韓國。釜山】韓國玩滑雪(包雪具)住滑雪渡假村水世界廚師秀五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190202A' target='_top'>2/2</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190207A' target='_top'>2/7</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=PUSBXP05G" target="_top">...more</a></div>
				<div class="product_price"><span>$17,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSBXP05H" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001394/001403/00018082.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSBXP05H" target="_top">春節版~【哇!韓國。釜山】韓國全程五星樂天世界滑雪包雪具海鮮吃到飽看星月搖滾秀五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190203B' target='_top'>2/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190204A' target='_top'>2/4</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=PUSBXP05H" target="_top">...more</a></div>
				<div class="product_price"><span>$22,800起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSBXP05I" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001394/001403/00018082.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSBXP05I" target="_top">春節版【哇!韓國】釜山雙天空步道、汗蒸幕、包雪具、體驗樂園、甘川洞、樂天超市五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190203A' target='_top'>2/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190208A' target='_top'>2/8</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=PUSBXP05I" target="_top">...more</a></div>
				<div class="product_price"><span>$13,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSITP5C" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001093/001096/00016648.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSITP5C" target="_top">IT-釜山滑雪趣~伊甸園滑雪、積木村、大陵苑天馬塚美食優質版五日【保證不上攝影、全程住宿五花酒店】</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PUSIT181226A' target='_top'>12/26</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSIT190102A' target='_top'>1/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSIT190104A' target='_top'>1/4</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSIT190107A' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSIT190109A' target='_top'>1/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSIT190114A' target='_top'>1/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSIT190116A' target='_top'>1/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSIT190118A' target='_top'>1/18</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=PUSITP5C" target="_top">...more</a></div>
				<div class="product_price"><span>$12,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSITP5E" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001394/001399/00018406.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSITP5E" target="_top">IT-虎釜鎮海賞櫻去~愛來水族館、金海腳踏車、葡萄酒洞窟、炸雞宵夜餐美食五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PUSIT190325A' target='_top'>3/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSIT190327A' target='_top'>3/27</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSIT190329A' target='_top'>3/29</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=PUSITP5E" target="_top">...more</a></div>
				<div class="product_price"><span>$13,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSITP5F" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001093/001096/00017383.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSITP5F" target="_top">IT-釜山超值滑雪趣~伊甸園滑雪、美人魚秀、特麗愛3D奧妙藝術館、積木村美食五日【保證不上攝影】</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PUSIT181224B' target='_top'>12/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSIT181231B' target='_top'>12/31</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=PUSITP5F" target="_top">...more</a></div>
				<div class="product_price"><span>$8,999起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSKEP5A" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001093/001096/00009485.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSKEP5A" target="_top">KE-釜山滑雪去~伊甸園滑雪、美人魚秀、積木村美食五日【保證不上攝影】</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PUSKE181222A' target='_top'>12/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSKE181223A' target='_top'>12/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSKE181230A' target='_top'>12/30</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSKE190105A' target='_top'>1/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSKE190106A' target='_top'>1/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSKE190112A' target='_top'>1/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSKE190113A' target='_top'>1/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSKE190119A' target='_top'>1/19</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=PUSKEP5A" target="_top">...more</a></div>
				<div class="product_price"><span>$11,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSKEP5B" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001093/001096/00017500.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSKEP5B" target="_top">KE-釜邱雙城記~天空步道、愛來水族館(美人魚表演秀)、KAKAO FRIENDS旗艦店五天【保證不上攝影師】</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PUSKE190302A' target='_top'>3/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSKE190303A' target='_top'>3/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSKE190309A' target='_top'>3/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSKE190310A' target='_top'>3/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSKE190316A' target='_top'>3/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSKE190317A' target='_top'>3/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSKE190323A' target='_top'>3/23</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=PUSKEP5B" target="_top">...more</a></div>
				<div class="product_price"><span>$11,500起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSKEP5C" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001394/001399/00018431.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSKEP5C" target="_top">KE-大韓釜山鎮海賞櫻去~愛來水族館、特麗愛3D奧妙藝術館、積木村、西面鬧區美食五日【保證升等兩晚五花酒店+不派攝影師】</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PUSKE190324A' target='_top'>3/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSKE190327A' target='_top'>3/27</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSKE190328A' target='_top'>3/28</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=PUSKEP5C" target="_top">...more</a></div>
				<div class="product_price"><span>$12,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SDJBRP05A" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000566/000593/00018226.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SDJBRP05A" target="_top">東北樹冰銀雪物語5日(藏王冰怪.景觀鐵道.銀山溫泉.松島遊船.採果樂)-仙台/仙台</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=SDJBR190102A' target='_top'>1/2</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=SDJBR190109A' target='_top'>1/9</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SDJBR190113A' target='_top'>1/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SDJBR190120A' target='_top'>1/20</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=SDJBR190123A' target='_top'>1/23</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SDJBR190130A' target='_top'>1/30</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=SDJBR190213A' target='_top'>2/13</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=SDJBRP05A" target="_top">...more</a></div>
				<div class="product_price"><span>$36,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=TYOBRP05A" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000563/000573/00008130.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=TYOBRP05A" target="_top">東京迪士尼雙樂園 箱根海盜船 登上晴空塔5日(鬼太郎茶屋.富士山纜車.天空町商店街)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=TYOBR190103A' target='_top'>1/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=TYOBR190107A' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=TYOBR190110A' target='_top'>1/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=TYOBR190117A' target='_top'>1/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=TYOBR190211A' target='_top'>2/11</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=TYOBR190214A' target='_top'>2/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=TYOBR190218A' target='_top'>2/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=TYOBR190221A' target='_top'>2/21</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=TYOBRP05A" target="_top">...more</a></div>
				<div class="product_price"><span>$27,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=TYOBRP05B" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000563/000573/00008130.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=TYOBRP05B" target="_top">冬季戲雪趣~東京迪士尼 箱根周遊5日(箱根登山電車+斜面電車+空中纜車+海盜船)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=TYOBR181216A' target='_top'>12/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=TYOBR181225A' target='_top'>12/25</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=TYOBRP05B" target="_top">...more</a></div>
				<div class="product_price"><span>$28,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSITP5B" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001093/001096/00016347.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSITP5B" target="_top">IT-釜山玩巨濟島~積木村、大陵苑天馬塚、饗宴美食宵夜餐五日【升等兩晚五花酒店】</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PUSIT190301A' target='_top'>3/1</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSIT190304A' target='_top'>3/4</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSIT190306A' target='_top'>3/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSIT190311A' target='_top'>3/11</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSIT190313A' target='_top'>3/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSIT190315A' target='_top'>3/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSIT190318A' target='_top'>3/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSIT190320A' target='_top'>3/20</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=PUSITP5B" target="_top">...more</a></div>
				<div class="product_price"><span>$10,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=ICNBRP5C" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001093/001096/00017605.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=ICNBRP5C" target="_top">BR-【台中出發】 韓國超值5天(滑雪體驗．HERO塗鴉秀．鐵路自行車．石鍋拌飯DIY)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR181223A' target='_top'>12/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR190106A' target='_top'>1/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR190113A' target='_top'>1/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR190120A' target='_top'>1/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR190127A' target='_top'>1/27</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR190217B' target='_top'>2/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR190224B' target='_top'>2/24</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=ICNBRP5C" target="_top">...more</a></div>
				<div class="product_price"><span>$11,900起</span></div>
   				<div class="product_offer"><p> </p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=TYOAEP5B" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000563/000573/00016598.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=TYOAEP5B" target="_top">【遇見日本】小東北～鬼怒川 大內宿古街 那須高原五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=TYOAE181226A' target='_top'>12/26</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=TYOAEP5B" target="_top">...more</a></div>
				<div class="product_price"><span>$27,900起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.png" width="60" /><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008861.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=TYOAEP5C" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000563/000573/00017357.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=TYOAEP5C" target="_top">【遇見日本】冬季遊日本～山形藏王樹冰、那須滑雪五日遊</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=TYOAE190103A' target='_top'>1/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=TYOAE190112A' target='_top'>1/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=TYOAE190113A' target='_top'>1/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=TYOAE190116A' target='_top'>1/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=TYOAE190118A' target='_top'>1/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=TYOAE190130A' target='_top'>1/30</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=TYOAE190202A' target='_top'>2/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=TYOAE190206A' target='_top'>2/6</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=TYOAEP5C" target="_top">...more</a></div>
				<div class="product_price"><span>$31,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.png"/></p></div>
			</div>
		</div>
		<div class="title_box" >
			<div class="left_box" id="DAREA00004">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>東南亞</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=BKIMH05E" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000620/000646/00008439.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=BKIMH05E" target="_top">東南亞馬爾地夫~沙巴淘夢島5日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190102A' target='_top'>1/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190107A' target='_top'>1/7</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190109A' target='_top'>1/9</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190114A' target='_top'>1/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190116A' target='_top'>1/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190121A' target='_top'>1/21</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190123A' target='_top'>1/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190128A' target='_top'>1/28</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=BKIMH05E" target="_top">...more</a></div>
				<div class="product_price"><span>$14,800起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=BKIMH5A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000620/000646/00011771.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=BKIMH5A" target="_top">沙巴沙很大5日~海洋公園逍遙遊、生態雨林長鼻猴、精油SPA</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190104A' target='_top'>1/4</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190106A' target='_top'>1/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190111A' target='_top'>1/11</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190113A' target='_top'>1/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190118A' target='_top'>1/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190120A' target='_top'>1/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190125A' target='_top'>1/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190127A' target='_top'>1/27</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=BKIMH5A" target="_top">...more</a></div>
				<div class="product_price"><span>$17,300起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=BKIMH5B" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000620/000646/00014171.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=BKIMH5B" target="_top">情定沙巴生態雨林、泛舟、長鼻猴、精油SPA五日遊</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190101A' target='_top'>1/1</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190105A' target='_top'>1/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190108A' target='_top'>1/8</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190112A' target='_top'>1/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190115A' target='_top'>1/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190119A' target='_top'>1/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190122A' target='_top'>1/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190126A' target='_top'>1/26</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=BKIMH5B" target="_top">...more</a></div>
				<div class="product_price"><span>$21,500起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=BKIMH5C" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000620/000646/00014239.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=BKIMH5C" target="_top">探索沙巴、龍尾灣、瑪莉島逍遙5日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190103A' target='_top'>1/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190110A' target='_top'>1/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190117A' target='_top'>1/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190124A' target='_top'>1/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190131A' target='_top'>1/31</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190214A' target='_top'>2/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190221A' target='_top'>2/21</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190228A' target='_top'>2/28</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=BKIMH5C" target="_top">...more</a></div>
				<div class="product_price"><span>$18,800起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=BKIMHP05A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000620/000646/00017439.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=BKIMHP05A" target="_top">2019新春沙巴_麥哲倫海洋奇緣5日【豪華五星絲綢灣麥哲倫飯店+龍尾灣海角樂園七合一+淘夢島全日遊+長鼻猴&螢河遊蹤】</a></div>
				<div class="departure_date">
					出發日期：
					<s><a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190203A' target='_top'>2/3</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190205A' target='_top'>2/5</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=BKIMHP05A" target="_top">...more</a></div>
				<div class="product_price"><span>$50,100起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img alt="" height="21" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png" width="61" /><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011383.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=BKIMHP05B" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000620/000646/00008438.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=BKIMHP05B" target="_top">2019新春沙巴_海洋樂翻天4+1日【泛舟&高空滑索+婆羅洲文化村+淘夢島全日遊+長鼻猴&螢河遊蹤+飯店晚宴&開運撈生】</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190206A' target='_top'>2/6</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=BKIMHP05B" target="_top">...more</a></div>
				<div class="product_price"><span>$38,100起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img alt="" height="21" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png" width="61" /><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011383.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=BKIMHP5A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000620/000646/00010266.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=BKIMHP5A" target="_top">響亮璀璨沙巴五日(馬來西亞航空)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190107B' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190114B' target='_top'>1/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190121B' target='_top'>1/21</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190128B' target='_top'>1/28</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190211B' target='_top'>2/11</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190218B' target='_top'>2/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190225B' target='_top'>2/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190304B' target='_top'>3/4</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=BKIMHP5A" target="_top">...more</a></div>
				<div class="product_price"><span>$16,300起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=BKKCIP5A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000617/000631/00017293.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=BKKCIP5A" target="_top">【泰輕鬆】曼谷芭達雅、四方水上市場、時尚千萬遊艇、冰雪世界、海鮮燒烤+帝王蟹吃到飽5日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=BKKCI190111A' target='_top'>1/11</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKKCI190118A' target='_top'>1/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKKCI190215A' target='_top'>2/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKKCI190222A' target='_top'>2/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKKCI190308A' target='_top'>3/8</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKKCI190315A' target='_top'>3/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKKCI190322A' target='_top'>3/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKKCI190327A' target='_top'>3/27</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=BKKCIP5A" target="_top">...more</a></div>
				<div class="product_price"><span>$20,300起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=BKKCIP5B" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000617/000631/00017310.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=BKKCIP5B" target="_top">【泰國樂】曼谷五星(保證入住一房一廳)+Pool Villa 2晚+海豚灣+夢幻世界+雙按摩5日(無購物)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=BKKCI181218A' target='_top'>12/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKKCI181220A' target='_top'>12/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKKCI190108A' target='_top'>1/8</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKKCI190110A' target='_top'>1/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKKCI190115A' target='_top'>1/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKKCI190117A' target='_top'>1/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKKCI190212A' target='_top'>2/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKKCI190214A' target='_top'>2/14</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=BKKCIP5B" target="_top">...more</a></div>
				<div class="product_price"><span>$25,600起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=BKKCIP6A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000617/000631/00017300.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=BKKCIP6A" target="_top">【泰風情】曼谷芭達雅、暹邏水陸雙主題樂園、機器人餐廳、水流蝦燒烤吃到飽6日(無購物)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=BKKCI190106A' target='_top'>1/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKKCI190107A' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKKCI190113A' target='_top'>1/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKKCI190114A' target='_top'>1/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKKCI190210A' target='_top'>2/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKKCI190211A' target='_top'>2/11</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKKCI190217A' target='_top'>2/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKKCI190218A' target='_top'>2/18</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=BKKCIP6A" target="_top">...more</a></div>
				<div class="product_price"><span>$24,300起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEB05A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00017052.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEB05A" target="_top">衝一波宿霧吧！奇幻島、二日自由活動、自選飯店5日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEB05190102A' target='_top'>1/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEB05190105A' target='_top'>1/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEB05190109A' target='_top'>1/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEB05190112A' target='_top'>1/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEB05190116A' target='_top'>1/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEB05190119A' target='_top'>1/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEB05190123A' target='_top'>1/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEB05190126A' target='_top'>1/26</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=CEB05A" target="_top">...more</a></div>
				<div class="product_price"><span>$7,500起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEB05B" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00015892.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEB05B" target="_top">虎航版～衝一波宿霧吧！奇幻島、二日自由活動、自選飯店5+1日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEB05190101A' target='_top'>1/1</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEB05190108A' target='_top'>1/8</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEB05190115A' target='_top'>1/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEB05190122A' target='_top'>1/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEB05190129A' target='_top'>1/29</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEB05190212A' target='_top'>2/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEB05190219A' target='_top'>2/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEB05190305A' target='_top'>3/5</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=CEB05B" target="_top">...more</a></div>
				<div class="product_price"><span>$7,500起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBBR0502" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00010562.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBBR0502" target="_top">鯨菲昔比-歐斯陸與鯨鯊共舞精緻五日【豪華版】</a></div>
				<div class="departure_date">
					出發日期：
					<s><a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181214A' target='_top'>12/14</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181217A' target='_top'>12/17</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CEBBR0502" target="_top">...more</a></div>
				<div class="product_price"><span>$29,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBBR1805B" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00015885.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBBR1805B" target="_top">宿霧雙島合一：南方夢幻海域一島一酒店五日遊</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181219A' target='_top'>12/19</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CEBBR1805B" target="_top">...more</a></div>
				<div class="product_price"><span>$26,500起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBBR1805C" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00015892.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBBR1805C" target="_top">宿霧雙島合一：享樂生活一島一酒店五日遊</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181215C' target='_top'>12/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181216B' target='_top'>12/16</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CEBBR1805C" target="_top">...more</a></div>
				<div class="product_price"><span>$25,500起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBBR1805D" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00009949.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBBR1805D" target="_top">宿霧小蜜月~兩天自由行五日遊</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181218B' target='_top'>12/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181220A' target='_top'>12/20</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CEBBR1805D" target="_top">...more</a></div>
				<div class="product_price"><span>$19,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBBRP05B" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00015892.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBBRP05B" target="_top">2人成行～新鯨豔宿霧5日精采版</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181219B' target='_top'>12/19</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181222A' target='_top'>12/22</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR190123A' target='_top'>1/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR190227A' target='_top'>2/27</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CEBBRP05B" target="_top">...more</a></div>
				<div class="product_price"><span>$27,500起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBBRP05C" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00012586.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBBRP05C" target="_top">春節限定～新鯨奇無比宿霧5日特惠版</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR190201A' target='_top'>2/1</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR190203A' target='_top'>2/3</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CEBBRP05C" target="_top">...more</a></div>
				<div class="product_price"><span>$35,500起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011383.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBBRP05D" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00017330.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBBRP05D" target="_top">春節限定～新鯨豔宿霧5日繽紛版(長榮加班包機)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR190202A' target='_top'>2/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR190206A' target='_top'>2/6</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CEBBRP05D" target="_top">...more</a></div>
				<div class="product_price"><span>$40,500起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img alt="" height="21" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png" width="61" /><img alt="" height="21" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011383.png" width="61" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBBRP05E" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00009949.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBBRP05E" target="_top">雙宿雙菲~宿霧薄荷島雙島五日繽紛版</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181215D' target='_top'>12/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181217B' target='_top'>12/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181220C' target='_top'>12/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181223A' target='_top'>12/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181224A' target='_top'>12/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181225A' target='_top'>12/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181226A' target='_top'>12/26</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR190306A' target='_top'>3/6</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CEBBRP05E" target="_top">...more</a></div>
				<div class="product_price"><span>$25,500起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBBRP05F" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00017678.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBBRP05F" target="_top">新鯨奇無比宿霧5日特惠版</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181215E' target='_top'>12/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181217C' target='_top'>12/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181220D' target='_top'>12/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181223B' target='_top'>12/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181224B' target='_top'>12/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181225B' target='_top'>12/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181226B' target='_top'>12/26</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR190123B' target='_top'>1/23</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=CEBBRP05F" target="_top">...more</a></div>
				<div class="product_price"><span>$23,500起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBITP05A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00009968.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBITP05A" target="_top">虎虎生風~宿霧資生堂跳島4+1日【輕鬆版】</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190124A' target='_top'>1/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190126A' target='_top'>1/26</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190131A' target='_top'>1/31</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190202A' target='_top'>2/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190207A' target='_top'>2/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190209A' target='_top'>2/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190214A' target='_top'>2/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190216A' target='_top'>2/16</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=CEBITP05A" target="_top">...more</a></div>
				<div class="product_price"><span>$12,500起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img alt="" height="21" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png" width="61" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBITP05B" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00017678.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBITP05B" target="_top">宿霧薄荷島資生堂雙跳島4+1日【超值版】</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190124B' target='_top'>1/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190126B' target='_top'>1/26</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190131B' target='_top'>1/31</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190202B' target='_top'>2/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190207B' target='_top'>2/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190209B' target='_top'>2/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190214B' target='_top'>2/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190216B' target='_top'>2/16</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=CEBITP05B" target="_top">...more</a></div>
				<div class="product_price"><span>$13,500起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBITP05C" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00012586.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBITP05C" target="_top">虎航宿霧薄荷島與鯨共舞全覽4+1日【全覽版】</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190124C' target='_top'>1/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190126C' target='_top'>1/26</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190131C' target='_top'>1/31</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190202C' target='_top'>2/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190207C' target='_top'>2/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190209C' target='_top'>2/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190214C' target='_top'>2/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190216C' target='_top'>2/16</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=CEBITP05C" target="_top">...more</a></div>
				<div class="product_price"><span>$19,500起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBITP05D" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00010562.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBITP05D" target="_top">0119聖嬰節限定～宿霧薄荷島資生堂雙跳島4+1日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190119A' target='_top'>1/19</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CEBITP05D" target="_top">...more</a></div>
				<div class="product_price"><span>$15,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBITP06A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00009949.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBITP06A" target="_top">虎虎生風~宿霧資生堂跳島5+1日【輕鬆版】</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190120A' target='_top'>1/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190122A' target='_top'>1/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190127A' target='_top'>1/27</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190129A' target='_top'>1/29</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190203A' target='_top'>2/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190205A' target='_top'>2/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190210A' target='_top'>2/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190212A' target='_top'>2/12</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=CEBITP06A" target="_top">...more</a></div>
				<div class="product_price"><span>$13,500起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBITP06B" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00012376.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBITP06B" target="_top">虎航宿霧薄荷島與鯨鯊共游5+1日【超值版】</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190120B' target='_top'>1/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190122B' target='_top'>1/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190127B' target='_top'>1/27</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190129B' target='_top'>1/29</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190203B' target='_top'>2/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190205B' target='_top'>2/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190210B' target='_top'>2/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190212B' target='_top'>2/12</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=CEBITP06B" target="_top">...more</a></div>
				<div class="product_price"><span>$19,500起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBITP06C" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00015904.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBITP06C" target="_top">虎航宿霧薄荷島與鯨共舞全覽5+1日【全覽版】</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190120C' target='_top'>1/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190122C' target='_top'>1/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190127C' target='_top'>1/27</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190129C' target='_top'>1/29</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190203C' target='_top'>2/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190210C' target='_top'>2/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190212C' target='_top'>2/12</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=CEBITP06C" target="_top">...more</a></div>
				<div class="product_price"><span>$23,500起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBZ2P04A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00017046.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBZ2P04A" target="_top">宿霧玩很大玩家4日遊特別版早去晚回(亞航)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEBZ2190228A' target='_top'>2/28</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CEBZ2P04A" target="_top">...more</a></div>
				<div class="product_price"><span>$21,500起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011377.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBZ2P05A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00012586.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBZ2P05A" target="_top">宿霧薄荷島海濱渡假五日精采版(保證入住薄荷島海濱渡假村)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEBZ2190227A' target='_top'>2/27</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CEBZ2P05A" target="_top">...more</a></div>
				<div class="product_price"><span>$28,500起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img alt="" height="21" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png" width="61" /><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008845.png" width="60" /><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011377.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBZ2P05B" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00012376.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBZ2P05B" target="_top">宿霧玩很大~歐斯陸與鯨鯊共舞玩家五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEBZ2190227B' target='_top'>2/27</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CEBZ2P05B" target="_top">...more</a></div>
				<div class="product_price"><span>$25,500起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img alt="" height="21" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png" width="61" /><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008845.png" width="60" /><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011377.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBZ2P06A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00017056.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBZ2P06A" target="_top">宿霧薄荷島精采六日(早去早回)</a></div>
				<div class="departure_date">
					出發日期：
					<s><a href='/EW/GO/GroupDetail.asp?prodCd=CEBZ2181227C' target='_top'>12/27</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CEBZ2P06A" target="_top">...more</a></div>
				<div class="product_price"><span>$34,900起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011381.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBZ2P06B" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00010562.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBZ2P06B" target="_top">宿霧玩很大~歐斯陸與鯨鯊共舞玩家6日(早去早回)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEBZ2181227D' target='_top'>12/27</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CEBZ2P06B" target="_top">...more</a></div>
				<div class="product_price"><span>$28,500起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CNXBRP5A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000617/000631/00017958.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CNXBRP5A" target="_top">BR【泰北玩家】清邁、清萊美麗境界5日(五星一房一廳+小木屋.長頸族.黑廟.白廟.金三角.寮國島.泰式按摩)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CNXBR190101A' target='_top'>1/1</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CNXBR190102A' target='_top'>1/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CNXBR190103A' target='_top'>1/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CNXBR190106A' target='_top'>1/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CNXBR190107A' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CNXBR190109A' target='_top'>1/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CNXBR190110A' target='_top'>1/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CNXBR190115A' target='_top'>1/15</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=CNXBRP5A" target="_top">...more</a></div>
				<div class="product_price"><span>$21,000起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CNXBRP5B" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000617/000631/00017981.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CNXBRP5B" target="_top">BR【泰北玩家】清邁、清萊美麗境界5日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CNXBR181224A' target='_top'>12/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CNXBR181225A' target='_top'>12/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CNXBR181227A' target='_top'>12/27</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CNXBR181231A' target='_top'>12/31</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CNXBRP5B" target="_top">...more</a></div>
				<div class="product_price"><span>$22,500起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CNXFDP5A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000617/000631/00018084.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CNXFDP5A" target="_top">FD【亞航假期】泰北雙城遊記叢林騎象超值5+1日遊</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CNXFD190105A' target='_top'>1/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CNXFD190112A' target='_top'>1/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CNXFD190119A' target='_top'>1/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CNXFD190123A' target='_top'>1/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CNXFD190302A' target='_top'>3/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CNXFD190306A' target='_top'>3/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CNXFD190309A' target='_top'>3/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CNXFD190313A' target='_top'>3/13</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=CNXFDP5A" target="_top">...more</a></div>
				<div class="product_price"><span>$16,200起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CNXFDP5B" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000617/000631/00018100.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CNXFDP5B" target="_top">FD【亞航假期】清邁泰好玩叢林騎象帕耀湖大象便便紙DIY5+1日遊</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CNXFD190105B' target='_top'>1/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CNXFD190112B' target='_top'>1/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CNXFD190119B' target='_top'>1/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CNXFD190123B' target='_top'>1/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CNXFD190302B' target='_top'>3/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CNXFD190306B' target='_top'>3/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CNXFD190309B' target='_top'>3/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CNXFD190313B' target='_top'>3/13</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=CNXFDP5B" target="_top">...more</a></div>
				<div class="product_price"><span>$18,000起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CNXFDP6A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000617/000631/00018116.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CNXFDP6A" target="_top">FD【亞航假期】泰北玩透透南邦帕耀湖蘭納古城6+1日遊</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CNXFD190117A' target='_top'>1/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CNXFD190124A' target='_top'>1/24</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CNXFDP6A" target="_top">...more</a></div>
				<div class="product_price"><span>$19,000起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=KLO5JP05A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00016897.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=KLO5JP05A" target="_top">最美長灘島回來了~風帆、香蕉船、沙灘按摩、環島遊、悠閒五日【直飛】</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=KLO5J190126A' target='_top'>1/26</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KLO5J190131A' target='_top'>1/31</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KLO5J190205A' target='_top'>2/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KLO5J190210A' target='_top'>2/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KLO5J190215A' target='_top'>2/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KLO5J190220A' target='_top'>2/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KLO5J190225A' target='_top'>2/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KLO5J190302A' target='_top'>3/2</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=KLO5JP05A" target="_top">...more</a></div>
				<div class="product_price"><span>$19,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=KULCIP05C" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000619/000641/00014437.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=KULCIP05C" target="_top">《五大保證》旗艦馬來西亞～全程五星酒店、大紅花海上VILLA、熱石SPA療程、英式下午茶5日(無購物)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=KULCI181222A' target='_top'>12/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULCI190104A' target='_top'>1/4</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULCI190107A' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULCI190109A' target='_top'>1/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULCI190111A' target='_top'>1/11</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULCI190114A' target='_top'>1/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULCI190116A' target='_top'>1/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULCI190118A' target='_top'>1/18</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=KULCIP05C" target="_top">...more</a></div>
				<div class="product_price"><span>$25,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=KULD7P05B" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000619/000641/00017545.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=KULD7P05B" target="_top">《春節預購減3千》翻轉馬來西亞～大紅花海上VILLA(一房一泳池)、馬六甲遊蹤5日(升等五星酒店1晚ｘ飯店自助晚餐)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=KULD7190201A' target='_top'>2/1</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULD7190204A' target='_top'>2/4</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULD7190206A' target='_top'>2/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULD7190207A' target='_top'>2/7</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=KULD7P05B" target="_top">...more</a></div>
				<div class="product_price"><span>$28,900起</span></div>
   				<div class="product_offer"><p><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=KULODP05C" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000619/000641/00017450.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=KULODP05C" target="_top">【馬印假期】鍾愛馬來-怡保山城、雙威溫泉樂園、五星悅榕庄五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190107A' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190110A' target='_top'>1/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190114A' target='_top'>1/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190117A' target='_top'>1/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190121A' target='_top'>1/21</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190124A' target='_top'>1/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190218A' target='_top'>2/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190225A' target='_top'>2/25</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=KULODP05C" target="_top">...more</a></div>
				<div class="product_price"><span>$14,900起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008844.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=KULODP05D" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000619/000641/00018038.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=KULODP05D" target="_top">【馬印假期】愛去馬來-馬六甲、法國村、雲頂小資五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190107B' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190110B' target='_top'>1/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190114B' target='_top'>1/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190117B' target='_top'>1/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190121B' target='_top'>1/21</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190124B' target='_top'>1/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190218B' target='_top'>2/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190225B' target='_top'>2/25</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=KULODP05D" target="_top">...more</a></div>
				<div class="product_price"><span>$12,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=KULODP05E" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000619/000641/00018047.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=KULODP05E" target="_top">【馬印假期】最愛馬來-AVANI棕櫚渡假村海上VILLA、馬六甲古城五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190107C' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190110C' target='_top'>1/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190114C' target='_top'>1/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190117C' target='_top'>1/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190121C' target='_top'>1/21</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190124C' target='_top'>1/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190218C' target='_top'>2/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190225C' target='_top'>2/25</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=KULODP05E" target="_top">...more</a></div>
				<div class="product_price"><span>$14,800起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=KULODP06B" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000619/000641/00014411.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=KULODP06B" target="_top">【豬年迎馬新】璀璨馬新大紅花海上Villa.環球影城.樂高樂園.漫步雲端.濱海灣無購物 6 日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190202A' target='_top'>2/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190204A' target='_top'>2/4</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190207A' target='_top'>2/7</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=KULODP06B" target="_top">...more</a></div>
				<div class="product_price"><span>$39,900起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011383.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=MNLCIP05A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00017322.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=MNLCIP05A" target="_top">【主題旅遊．愛冒險．全包式】菲律賓．EL_NIDO愛妮島．APULIT阿普莉渡假村5日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=MNLCI181214A' target='_top'>12/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=MNLCI181215A' target='_top'>12/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=MNLCI181216A' target='_top'>12/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=MNLCI181217A' target='_top'>12/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=MNLCI181218A' target='_top'>12/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=MNLCI181219A' target='_top'>12/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=MNLCI190101A' target='_top'>1/1</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=MNLCI190102A' target='_top'>1/2</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=MNLCIP05A" target="_top">...more</a></div>
				<div class="product_price"><span>$55,500起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=MPH5JP05A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00016897.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=MPH5JP05A" target="_top">春節限定5J轉機～HIGH翻長灘島悠閒五日</a></div>
 				<div class="product_description">春節旅遊~10人以上加派領隊</div> 
				<div class="departure_date">
					出發日期：
					<s><a href='/EW/GO/GroupDetail.asp?prodCd=MPH5J190202A' target='_top'>2/2</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=MPH5J190204A' target='_top'>2/4</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=MPH5J190206A' target='_top'>2/6</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=MPH5JP05A" target="_top">...more</a></div>
				<div class="product_price"><span>$22,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PENCIP05B" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000619/000641/00017550.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PENCIP05B" target="_top">《經典》檳城蘭卡威～三離島之旅(孕婦島濕米島水晶島)、蟲鳴大地、魚吻足SPA5日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PENCI181217A' target='_top'>12/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PENCI181219A' target='_top'>12/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PENCI181221A' target='_top'>12/21</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PENCI181223A' target='_top'>12/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PENCI181224A' target='_top'>12/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PENCI181225A' target='_top'>12/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PENCI181230A' target='_top'>12/30</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PENCI181231A' target='_top'>12/31</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=PENCIP05B" target="_top">...more</a></div>
				<div class="product_price"><span>$23,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img alt="" height="21" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png" width="61" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PPS5JP05A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00015348.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PPS5JP05A" target="_top">春節限定～愛上巴拉望一島一飯店跳島渡假5日(保證入住一島一飯店)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PPS5J190202B' target='_top'>2/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PPS5J190203B' target='_top'>2/3</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=PPS5JP05A" target="_top">...more</a></div>
				<div class="product_price"><span>$30,500起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PPS5JP05B" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00016479.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PPS5JP05B" target="_top">228限定～愛上巴拉望一島一飯店跳島渡假5日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PPS5J190227A' target='_top'>2/27</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PPS5J190228A' target='_top'>2/28</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=PPS5JP05B" target="_top">...more</a></div>
				<div class="product_price"><span>$28,500起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PPS5JP05C" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00016479.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PPS5JP05C" target="_top">春節限定5J轉機～玩瘋了巴拉望超級玩家、地底河流、本田灣五天</a></div>
 				<div class="product_description">春節旅遊~10人以上加派領隊</div> 
				<div class="departure_date">
					出發日期：
					<s><a href='/EW/GO/GroupDetail.asp?prodCd=PPS5J190202A' target='_top'>2/2</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PPS5J190203A' target='_top'>2/3</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=PPS5J190204A' target='_top'>2/4</a></s> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=PPS5J190205A' target='_top'>2/5</a></s> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=PPS5J190206A' target='_top'>2/6</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=PPS5JP05C" target="_top">...more</a></div>
				<div class="product_price"><span>$25,800起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011383.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PPSFEP05A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00015348.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PPSFEP05A" target="_top">2人成行～新看見巴拉望渡假5日(保證入住一島一飯店)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PPSFE190119A' target='_top'>1/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PPSFE190123A' target='_top'>1/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PPSFE190127A' target='_top'>1/27</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PPSFE190212A' target='_top'>2/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PPSFE190216A' target='_top'>2/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PPSFE190220A' target='_top'>2/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PPSFE190224A' target='_top'>2/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PPSFE190228A' target='_top'>2/28</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=PPSFEP05A" target="_top">...more</a></div>
				<div class="product_price"><span>$26,500起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PPSFEP05C" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00015904.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PPSFEP05C" target="_top">春節限定～2人成行新看見巴拉望渡假5日(保證入住一島一飯店)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PPSFE190131A' target='_top'>1/31</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PPSFE190204A' target='_top'>2/4</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=PPSFE190208A' target='_top'>2/8</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=PPSFEP05C" target="_top">...more</a></div>
				<div class="product_price"><span>$26,500起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SINBRP04A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000619/000641/00017580.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SINBRP04A" target="_top">甜蜜遊獅城～五星聖淘沙、環球影城、海洋生物園、金沙濱海灣四日(含稅)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=SINBR181214A' target='_top'>12/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SINBR181216A' target='_top'>12/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SINBR181218A' target='_top'>12/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SINBR181219A' target='_top'>12/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SINBR181220B' target='_top'>12/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SINBR181221A' target='_top'>12/21</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SINBR181222A' target='_top'>12/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SINBR181223B' target='_top'>12/23</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=SINBRP04A" target="_top">...more</a></div>
				<div class="product_price"><span>$24,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SINBRP05A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000619/000641/00014437.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SINBRP05A" target="_top">經典新馬大紅花海上VILLA五日～精油熱石SPA‧三輪車‧KYTOWER玻璃走廊‧3D藝術博物館‧高塔‧金沙(無購物)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=SINBR181220A' target='_top'>12/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SINBR181223A' target='_top'>12/23</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=SINBR181229A' target='_top'>12/29</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SINBR190103A' target='_top'>1/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SINBR190108A' target='_top'>1/8</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SINBR190110A' target='_top'>1/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SINBR190112A' target='_top'>1/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SINBR190115A' target='_top'>1/15</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=SINBRP05A" target="_top">...more</a></div>
				<div class="product_price"><span>$22,999起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img alt="" height="21" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png" width="61" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SINTRP05A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000619/000641/00017489.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SINTRP05A" target="_top">【豬年迎新春~2/6保證出團】新馬雙國獨家卡通星球.環球影城.樂高樂園.柔佛螢河遊.阿凡達燈光秀5日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=SINTR190206A' target='_top'>2/6</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=SINTRP05A" target="_top">...more</a></div>
				<div class="product_price"><span>$36,999起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img alt="" height="21" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png" width="61" /><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011383.png"/></p></div>
			</div>
		</div>
		<div class="title_box" >
			<div class="left_box" id="DAREA00010">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>歐洲</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SVOSUP10A" target="_top"><img src="/eWeb_summittour/IMGDB/000432/001367/001370/00016939.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SVOSUP10A" target="_top">俄羅斯雙都、莫曼斯克四晚極光圈十日</a></div>
 				<div class="product_description">★早鳥優惠2000元★</div> 
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=SVOSU181222A' target='_top'>12/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SVOSU181229A' target='_top'>12/29</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SVOSU190105A' target='_top'>1/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SVOSU190112A' target='_top'>1/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SVOSU190118A' target='_top'>1/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SVOSU190126A' target='_top'>1/26</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SVOSU190202A' target='_top'>2/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SVOSU190208A' target='_top'>2/8</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=SVOSUP10A" target="_top">...more</a></div>
				<div class="product_price"><span>$105,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
		</div>
		<div class="title_box" >
			<div class="left_box" id="DAREA00011">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>紐澳</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=AKLNZP10A" target="_top"><img src="/eWeb_summittour/IMGDB/000576/000759/000924/00016954.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=AKLNZP10A" target="_top">紐西蘭南北島湖山星光10日~米佛峽灣、皇后鎮、阿爾卑斯高山</a></div>
 				<div class="product_description">★紐航直飛特惠價★</div> 
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=AKLNZ190103A' target='_top'>1/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=AKLNZ190117A' target='_top'>1/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=AKLNZ190214A' target='_top'>2/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=AKLNZ190228A' target='_top'>2/28</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=AKLNZ190314A' target='_top'>3/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=AKLNZ190328A' target='_top'>3/28</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=AKLNZP10A" target="_top">...more</a></div>
				<div class="product_price"><span>$97,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=AKLNZP11A" target="_top"><img src="/eWeb_summittour/IMGDB/000576/000759/000924/00016971.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=AKLNZP11A" target="_top">紐西蘭南北島經典全覽11日~高山、湖泊、天空與自然生態饗宴</a></div>
 				<div class="product_description">★早鳥優惠第二人減4000元★</div> 
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=AKLNZ190105B' target='_top'>1/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=AKLNZ190119B' target='_top'>1/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=AKLNZ190124A' target='_top'>1/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=AKLNZ190129A' target='_top'>1/29</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=AKLNZ190202A' target='_top'>2/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=AKLNZ190216B' target='_top'>2/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=AKLNZ190223A' target='_top'>2/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=AKLNZ190302B' target='_top'>3/2</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=AKLNZP11A" target="_top">...more</a></div>
				<div class="product_price"><span>$123,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=AKLNZP15A" target="_top"><img src="/eWeb_summittour/IMGDB/000576/000759/000924/00016990.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=AKLNZP15A" target="_top">紐西蘭南北島~縱貫山水純淨連線經典全覽十五日</a></div>
 				<div class="product_description">★早鳥優惠第二人減6000元★</div> 
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=AKLNZ190105A' target='_top'>1/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=AKLNZ190119A' target='_top'>1/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=AKLNZ190216A' target='_top'>2/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=AKLNZ190302A' target='_top'>3/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=AKLNZ190316A' target='_top'>3/16</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=AKLNZP15A" target="_top">...more</a></div>
				<div class="product_price"><span>$168,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
		</div>
		<div class="title_box" >
			<div class="left_box" id="DAREA00014">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>美加</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=FAIBRP08A" target="_top">阿拉斯加璀璨極光、珍娜溫泉8天</a></div>
 				<div class="product_description">保證兩晚入住極光星球屋</div> 
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=FAIBR190114A' target='_top'>1/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=FAIBR190407A' target='_top'>4/7</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=FAIBRP08A" target="_top">...more</a></div>
				<div class="product_price"><span>$89,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=HAVCAP13A" target="_top"><img src="/eWeb_summittour/IMGDB/000577/001055/001058/00008316.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=HAVCAP13A" target="_top">國航出任務～古巴大冒險～發現新北京13天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=HAVCA190110A' target='_top'>1/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=HAVCA190411A' target='_top'>4/11</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=HAVCAP13A" target="_top">...more</a></div>
				<div class="product_price"><span>$121,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=LAXBRP11B" target="_top"><img src="/eWeb_summittour/IMGDB/000577/001055/001058/00017899.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=LAXBRP11B" target="_top">情迷醉人烏托邦～古巴深度11天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=LAXBR190111A' target='_top'>1/11</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXBR190215A' target='_top'>2/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXBR190315A' target='_top'>3/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXBR190412A' target='_top'>4/12</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=LAXBRP11B" target="_top">...more</a></div>
				<div class="product_price"><span>$121,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=LAXHXP08A" target="_top"><img src="/eWeb_summittour/IMGDB/000577/000764/000949/00013080.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=LAXHXP08A" target="_top">美西雙國家公園～大峽谷+錫安、羚羊峽谷+馬蹄灣8天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190106A' target='_top'>1/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190107C' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190120A' target='_top'>1/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190121A' target='_top'>1/21</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190217A' target='_top'>2/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190218C' target='_top'>2/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190224A' target='_top'>2/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190225C' target='_top'>2/25</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=LAXHXP08A" target="_top">...more</a></div>
				<div class="product_price"><span>$40,900起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008861.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=LAXHXP08B" target="_top"><img src="/eWeb_summittour/IMGDB/000577/000764/000949/00015383.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=LAXHXP08B" target="_top">美西四城~大峽谷.洛杉磯.拉斯維加斯.拉芙琳.聖地牙哥空軍一號8天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190101B' target='_top'>1/1</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190107B' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190109B' target='_top'>1/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190212B' target='_top'>2/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190218B' target='_top'>2/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190220B' target='_top'>2/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190225A' target='_top'>2/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190227C' target='_top'>2/27</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=LAXHXP08B" target="_top">...more</a></div>
				<div class="product_price"><span>$33,900起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=LAXHXP08D" target="_top"><img src="/eWeb_summittour/IMGDB/000577/000764/000949/00013080.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=LAXHXP08D" target="_top">美西三城~壯麗奇景大峽谷國家公園.洛杉磯.拉斯維加斯.拉芙琳.環球影城8天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190101A' target='_top'>1/1</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190107A' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190109A' target='_top'>1/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190212A' target='_top'>2/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190218A' target='_top'>2/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190220A' target='_top'>2/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190225B' target='_top'>2/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190227B' target='_top'>2/27</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=LAXHXP08D" target="_top">...more</a></div>
				<div class="product_price"><span>$36,900起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=LAXHXP08E" target="_top"><img src="/eWeb_summittour/IMGDB/000577/000764/000949/00010380.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=LAXHXP08E" target="_top">期間限定～洛杉磯、拉斯維加斯、聖地牙哥海景火車、藍瓶咖啡8天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190115A' target='_top'>1/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190304C' target='_top'>3/4</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=LAXHXP08E" target="_top">...more</a></div>
				<div class="product_price"><span>$26,888起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008846.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=LAXHXP10B" target="_top"><img src="/eWeb_summittour/IMGDB/000577/000764/000949/00014533.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=LAXHXP10B" target="_top">4321走一回!!四大國家公園~羚羊峽谷+馬蹄灣10天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190102A' target='_top'>1/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190116A' target='_top'>1/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190213A' target='_top'>2/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190227A' target='_top'>2/27</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190313A' target='_top'>3/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190327A' target='_top'>3/27</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=LAXHXP10B" target="_top">...more</a></div>
				<div class="product_price"><span>$51,900起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=LAXHXP14A" target="_top"><img src="/eWeb_summittour/IMGDB/000577/000764/000949/00014610.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=LAXHXP14A" target="_top">LUCKY7!!七大國家公園~羚羊峽谷+馬蹄灣14天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190513A' target='_top'>5/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190527A' target='_top'>5/27</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190603A' target='_top'>6/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190610A' target='_top'>6/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190624A' target='_top'>6/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190708A' target='_top'>7/8</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190722A' target='_top'>7/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190805A' target='_top'>8/5</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=LAXHXP14A" target="_top">...more</a></div>
				<div class="product_price"><span>$94,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=LAXMIAHXP10A" target="_top"><img src="/eWeb_summittour/IMGDB/000577/001055/001058/00012986.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=LAXMIAHXP10A" target="_top">神祕古巴華麗的冒險10天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=LAXMI190111B' target='_top'>1/11</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXMI190215B' target='_top'>2/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXMI190315B' target='_top'>3/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXMI190412B' target='_top'>4/12</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=LAXMIAHXP10A" target="_top">...more</a></div>
				<div class="product_price"><span>$94,900起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.PNG	" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=LAXMIAHXP12A" target="_top"><img src="/eWeb_summittour/IMGDB/000577/001055/001058/00015018.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=LAXMIAHXP12A" target="_top">情迷醉人烏托邦~古巴深度12天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=LAXMI190111A' target='_top'>1/11</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXMI190215A' target='_top'>2/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXMI190315A' target='_top'>3/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXMI190412A' target='_top'>4/12</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=LAXMIAHXP12A" target="_top">...more</a></div>
				<div class="product_price"><span>$132,900起</span></div>
   				<div class="product_offer"><p><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00015107.png"/><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.PNG	" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SFOHXP08B" target="_top"><img src="/eWeb_summittour/IMGDB/000577/000765/001230/00015117.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SFOHXP08B" target="_top">漫步北加州8天-入住舊金山市中心2晚  納帕品酒.太浩湖美景.沙加緬度老城.賭城雷諾.APPLE PARK</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=SFOHX190102B' target='_top'>1/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOHX190116B' target='_top'>1/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOHX190213B' target='_top'>2/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOHX190227B' target='_top'>2/27</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOHX190313B' target='_top'>3/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOHX190327B' target='_top'>3/27</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=SFOHXP08B" target="_top">...more</a></div>
				<div class="product_price"><span>$44,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008851.png"/><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SFOHXP08C" target="_top"><img src="/eWeb_summittour/IMGDB/000577/000765/001230/00013060.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SFOHXP08C" target="_top">遇見北加州8天-舊金山經典市區全覽(缪爾紅木公園.蘇莎利托.蝴蝶鎮.17哩.APPLE PARK)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=SFOHX190102A' target='_top'>1/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOHX190116A' target='_top'>1/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOHX190213A' target='_top'>2/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOHX190227A' target='_top'>2/27</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOHX190313A' target='_top'>3/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOHX190327A' target='_top'>3/27</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=SFOHXP08C" target="_top">...more</a></div>
				<div class="product_price"><span>$42,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008851.png"/><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SFOLAXHXP10A" target="_top"><img src="/eWeb_summittour/IMGDB/000577/000764/000949/00013077.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SFOLAXHXP10A" target="_top">愛玩美西五城~17哩海岸.西峽谷.環球影城10天SFO進</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=SFOLA190101A' target='_top'>1/1</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOLA190107A' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOLA190212A' target='_top'>2/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOLA190218A' target='_top'>2/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOLA190225A' target='_top'>2/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOLA190311A' target='_top'>3/11</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOLA190325A' target='_top'>3/25</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=SFOLAXHXP10A" target="_top">...more</a></div>
				<div class="product_price"><span>$50,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SFOLAXHXP10B" target="_top"><img src="/eWeb_summittour/IMGDB/000577/000764/000949/00015177.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SFOLAXHXP10B" target="_top">享玩美西五城~大峽谷.17哩海岸.環球影城.空軍一號10天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=SFOLA190101B' target='_top'>1/1</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOLA190107B' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOLA190212B' target='_top'>2/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOLA190218B' target='_top'>2/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOLA190225B' target='_top'>2/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOLA190311B' target='_top'>3/11</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOLA190325B' target='_top'>3/25</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=SFOLAXHXP10B" target="_top">...more</a></div>
				<div class="product_price"><span>$47,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=YVRACP08B" target="_top"><img src="/eWeb_summittour/IMGDB/000577/000770/001214/00017600.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=YVRACP08B" target="_top">溫哥華、洛磯山脈8日～雙飛+雙城堡酒店</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=YVRAC190112A' target='_top'>1/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=YVRAC190216A' target='_top'>2/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=YVRAC190309A' target='_top'>3/9</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=YVRACP08B" target="_top">...more</a></div>
				<div class="product_price"><span>$56,800起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img alt="" height="21" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png" width="61" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=YVRACP08C" target="_top"><img src="/eWeb_summittour/IMGDB/000577/000770/001214/00017720.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=YVRACP08C" target="_top">【探索極光】冬季加拿大黃刀鎮8日~3晚黃刀鎮賞極光、狗拉雪橇</a></div>
 				<div class="product_description">★兩人同行第二人減6000元★</div> 
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=YVRAC190119A' target='_top'>1/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=YVRAC190223A' target='_top'>2/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=YVRAC190316A' target='_top'>3/16</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=YVRACP08C" target="_top">...more</a></div>
				<div class="product_price"><span>$77,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=YVRHXP12A" target="_top"><img src="/eWeb_summittour/IMGDB/000577/000768/001193/00012761.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=YVRHXP12A" target="_top">阿拉斯加極光、洛磯山脈雪景、奇幻之旅12天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=YVRHX190220A' target='_top'>2/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=YVRHX190225A' target='_top'>2/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=YVRHX190313A' target='_top'>3/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=YVRHX190327A' target='_top'>3/27</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=YVRHXP12A" target="_top">...more</a></div>
				<div class="product_price"><span>$103,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
		</div>
		<div class="title_box" >
			<div class="left_box" id="DAREA00015">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>海島</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PALCI04A" target="_top"><img src="/eWeb_summittour/IMGDB/000578/000733/000960/00008083.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PALCI04A" target="_top">帛琉水世界星光夜釣4日-豪華版</a></div>
 				<div class="product_description">贈帛琉限定HALO HALO剉冰</div> 
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PALCI181219A' target='_top'>12/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PALCI190109A' target='_top'>1/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PALCI190116A' target='_top'>1/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PALCI190130A' target='_top'>1/30</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=PALCI04A" target="_top">...more</a></div>
				<div class="product_price"><span>$26,800起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.PNG	" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PALCI04B" target="_top"><img src="/eWeb_summittour/IMGDB/000578/000733/000960/00008083.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PALCI04B" target="_top">帛琉醉愛水世界4日-經典版</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PALCI181219B' target='_top'>12/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PALCI190109B' target='_top'>1/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PALCI190116B' target='_top'>1/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PALCI190130B' target='_top'>1/30</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=PALCI04B" target="_top">...more</a></div>
				<div class="product_price"><span>$25,300起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PALCI05A" target="_top"><img src="/eWeb_summittour/IMGDB/000578/000733/000960/00008078.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PALCI05A" target="_top">帛琉水世界星光夜釣5日-經典版</a></div>
 				<div class="product_description">贈帛琉限定HALO HALO剉冰</div> 
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PALCI181215A' target='_top'>12/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PALCI190105A' target='_top'>1/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PALCI190112A' target='_top'>1/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PALCI190126A' target='_top'>1/26</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=PALCI05A" target="_top">...more</a></div>
				<div class="product_price"><span>$35,500起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.PNG	" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PALCI05B" target="_top"><img src="/eWeb_summittour/IMGDB/000578/000733/000960/00008081.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PALCI05B" target="_top">帛琉北島叢林探險5日-豪華版</a></div>
 				<div class="product_description">贈帛琉限定HALO HALO剉冰</div> 
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PALCI181215B' target='_top'>12/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PALCI190105B' target='_top'>1/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PALCI190112B' target='_top'>1/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PALCI190126B' target='_top'>1/26</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=PALCI05B" target="_top">...more</a></div>
				<div class="product_price"><span>$36,500起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.PNG	" width="60" /></p></div>
			</div>
		</div>
	</div>
</body>
</html>

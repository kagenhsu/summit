﻿<%
'//===========================================================================================
'//						index.asp
'//-------------------------------------------------------------------------------------------
'//		description		: 此頁面係由自動產生因此不用納入版本
'//-------------------------------------------------------------------------------------------
Response.CacheControl = "no-cache" 
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="robots" content="noindex">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
	<title>國外團體旅遊：促銷行程旅遊行程 - 超悠</title>
	<!--產出時間:2018/12/4 下午 11:04:09 / WEB:summittour-->
	<link href='/eweb_summittour/css/reset.css' rel='stylesheet' type='text/css'>
	<link href='/eweb_summittour/css/autopage.css' rel='stylesheet' type='text/css'>
	<script languege="javascript" src="/script/jquery.js"></script>
	<script languege="javascript" src="/eweb_summittour/JS/autopage.js"></script>
</head>
<body>
	<div class="metropolis">
		<div class="title_box" >
			<div class="left_box" id="DAREA00001">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>港澳大陸</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CGOAEPR8B" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000076/000987/00010411.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CGOAEPR8B" target="_top">華信假期─賀新年龍門石窟雲臺山萬仙山郭亮村八日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CGOAEP89206A' target='_top'>2/6</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CGOAEPR8B" target="_top">...more</a></div>
				<div class="product_price"><span>$29,800起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CSXAEPT8B" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000080/000993/00008166.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CSXAEPT8B" target="_top">華信假期─湘媚風光張家界鳳凰天空玻璃橋豪華8天</a></div>
				<div class="departure_date">
					出發日期：
					<s><a href='/EW/GO/GroupDetail.asp?prodCd=CSXAEP89207A' target='_top'>2/7</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CSXAEPT8B" target="_top">...more</a></div>
				<div class="product_price"><span>$25,800起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=TAO0803" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000073/000982/00018167.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=TAO0803" target="_top">游遍中國─歐陸青島天鵝湖童話屋泉城泰山祈福8日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=TAOCIT89206A' target='_top'>2/6</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=TAO0803" target="_top">...more</a></div>
				<div class="product_price"><span>$29,900起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=HGH0502" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000258/000995/00017924.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=HGH0502" target="_top">華信假期─杭州海寧上海楓涇古鎮尊貴五天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=HGHAEP59206A' target='_top'>2/6</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=HGH0502" target="_top">...more</a></div>
				<div class="product_price"><span>$26,000起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=NKGAET8A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000258/000995/00015205.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=NKGAET8A" target="_top">東方神起─南京揚州鹽城徐州句容尊爵逍遙八天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=NKGMUP89202A' target='_top'>2/2</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=NKGAET8A" target="_top">...more</a></div>
				<div class="product_price"><span>$27,000起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR5B" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000085/000547/00014852.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR5B" target="_top">海南傳奇─三亞海口馮小剛電影公社興隆陵水低調奢華五天</a></div>
				<div class="departure_date">
					出發日期：
					<s><a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59206A' target='_top'>2/6</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=SYX3UPR5B" target="_top">...more</a></div>
				<div class="product_price"><span>$25,500起</span></div>
   				<div class="product_offer"><p><span style="color:#FFFFFF;"><span style="background-color:#FF8C00;">台中出發</span></span></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR5C" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000085/000547/00014852.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYX3UPR5C" target="_top">海南傳奇─三亞陵水特選優值五天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59130A' target='_top'>1/30</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=SYX3UPR5C" target="_top">...more</a></div>
				<div class="product_price"><span>$16,800起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYXJDPR4A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000085/000547/00017789.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYXJDPR4A" target="_top">海南傳奇─三亞鳳凰嶺陵水椰田古寨特選優值四天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP49203A' target='_top'>2/3</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=SYXJDPR4A" target="_top">...more</a></div>
				<div class="product_price"><span>$17,000起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.PNG	" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYXJDPR5B" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000085/000547/00008345.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SYXJDPR5B" target="_top">海南傳奇─三亞Club Med宋城千古情陵水奢華五天</a></div>
				<div class="departure_date">
					出發日期：
					<s><a href='/EW/GO/GroupDetail.asp?prodCd=SYX3UP59206B' target='_top'>2/6</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=SYXJDPR5B" target="_top">...more</a></div>
				<div class="product_price"><span>$37,500起</span></div>
   				<div class="product_offer"><p><span style="color:#FFFFFF;"><span style="background-color:#FF8C00;">台中出發</span></span></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=WUXAEPR5B" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000258/000995/00011499.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=WUXAEPR5B" target="_top">游遍中國─無錫杭州嘉興上海五天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=WUXAER59204A' target='_top'>2/4</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=WUXAEPR5B" target="_top">...more</a></div>
				<div class="product_price"><span>$23,500起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.png" width="60" /><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011383.png"/></p></div>
			</div>
		</div>
		<div class="title_box" >
			<div class="left_box" id="DAREA00002">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>東北亞</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTSMMP5A" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000557/000559/00015057.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTSMMP5A" target="_top">【悠閒時光】北海道雪國祭典~道東阿寒湖、層雲峽、破冰船、玩雪四合一、三溫泉5日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CTSMM190206A' target='_top'>2/6</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CTSMMP5A" target="_top">...more</a></div>
				<div class="product_price"><span>$56,800起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011383.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTSMMP6A" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000557/000559/00017850.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTSMMP6A" target="_top">【悠閒時光】北海道雪國祭典~道東阿寒湖、層雲峽、破冰船、玩雪四合一、三溫泉6日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CTSMM190203A' target='_top'>2/3</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CTSMMP6A" target="_top">...more</a></div>
				<div class="product_price"><span>$56,800起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img alt="" height="21" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011383.png" width="61" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=TYOAEP5C" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000563/000573/00017357.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=TYOAEP5C" target="_top">【遇見日本】冬季遊日本～山形藏王樹冰、那須滑雪五日遊</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=TYOAE190103A' target='_top'>1/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=TYOAE190112A' target='_top'>1/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=TYOAE190113A' target='_top'>1/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=TYOAE190116A' target='_top'>1/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=TYOAE190118A' target='_top'>1/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=TYOAE190130A' target='_top'>1/30</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=TYOAE190202A' target='_top'>2/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=TYOAE190206A' target='_top'>2/6</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=TYOAEP5C" target="_top">...more</a></div>
				<div class="product_price"><span>$31,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.png"/></p></div>
			</div>
		</div>
		<div class="title_box" >
			<div class="left_box" id="DAREA00004">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>東南亞</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=BKIMHP05A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000620/000646/00017439.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=BKIMHP05A" target="_top">2019新春沙巴_麥哲倫海洋奇緣5日【豪華五星絲綢灣麥哲倫飯店+龍尾灣海角樂園七合一+淘夢島全日遊+長鼻猴&螢河遊蹤】</a></div>
				<div class="departure_date">
					出發日期：
					<s><a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190203A' target='_top'>2/3</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190205A' target='_top'>2/5</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=BKIMHP05A" target="_top">...more</a></div>
				<div class="product_price"><span>$50,100起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img alt="" height="21" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png" width="61" /><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011383.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=BKIMHP05B" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000620/000646/00008438.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=BKIMHP05B" target="_top">2019新春沙巴_海洋樂翻天4+1日【泛舟&高空滑索+婆羅洲文化村+淘夢島全日遊+長鼻猴&螢河遊蹤+飯店晚宴&開運撈生】</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=BKIMH190206A' target='_top'>2/6</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=BKIMHP05B" target="_top">...more</a></div>
				<div class="product_price"><span>$38,100起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img alt="" height="21" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png" width="61" /><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011383.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBBRP05C" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00012586.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBBRP05C" target="_top">春節限定～新鯨奇無比宿霧5日特惠版</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR190201A' target='_top'>2/1</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR190203A' target='_top'>2/3</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CEBBRP05C" target="_top">...more</a></div>
				<div class="product_price"><span>$35,500起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011383.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBBRP05D" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00017330.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBBRP05D" target="_top">春節限定～新鯨豔宿霧5日繽紛版(長榮加班包機)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR190202A' target='_top'>2/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR190206A' target='_top'>2/6</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CEBBRP05D" target="_top">...more</a></div>
				<div class="product_price"><span>$40,500起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img alt="" height="21" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png" width="61" /><img alt="" height="21" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011383.png" width="61" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=KULODP06B" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000619/000641/00014411.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=KULODP06B" target="_top">【豬年迎馬新】璀璨馬新大紅花海上Villa.環球影城.樂高樂園.漫步雲端.濱海灣無購物 6 日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190202A' target='_top'>2/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190204A' target='_top'>2/4</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KULOD190207A' target='_top'>2/7</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=KULODP06B" target="_top">...more</a></div>
				<div class="product_price"><span>$39,900起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011383.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=MPH5JP05A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00016897.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=MPH5JP05A" target="_top">春節限定5J轉機～HIGH翻長灘島悠閒五日</a></div>
 				<div class="product_description">春節旅遊~10人以上加派領隊</div> 
				<div class="departure_date">
					出發日期：
					<s><a href='/EW/GO/GroupDetail.asp?prodCd=MPH5J190202A' target='_top'>2/2</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=MPH5J190204A' target='_top'>2/4</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=MPH5J190206A' target='_top'>2/6</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=MPH5JP05A" target="_top">...more</a></div>
				<div class="product_price"><span>$22,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PPS5JP05C" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00016479.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PPS5JP05C" target="_top">春節限定5J轉機～玩瘋了巴拉望超級玩家、地底河流、本田灣五天</a></div>
 				<div class="product_description">春節旅遊~10人以上加派領隊</div> 
				<div class="departure_date">
					出發日期：
					<s><a href='/EW/GO/GroupDetail.asp?prodCd=PPS5J190202A' target='_top'>2/2</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PPS5J190203A' target='_top'>2/3</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=PPS5J190204A' target='_top'>2/4</a></s> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=PPS5J190205A' target='_top'>2/5</a></s> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=PPS5J190206A' target='_top'>2/6</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=PPS5JP05C" target="_top">...more</a></div>
				<div class="product_price"><span>$25,800起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011383.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PPSFEP05C" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00015904.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PPSFEP05C" target="_top">春節限定～2人成行新看見巴拉望渡假5日(保證入住一島一飯店)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PPSFE190131A' target='_top'>1/31</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PPSFE190204A' target='_top'>2/4</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=PPSFE190208A' target='_top'>2/8</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=PPSFEP05C" target="_top">...more</a></div>
				<div class="product_price"><span>$26,500起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SINTRP05A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000619/000641/00017489.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SINTRP05A" target="_top">【豬年迎新春~2/6保證出團】新馬雙國獨家卡通星球.環球影城.樂高樂園.柔佛螢河遊.阿凡達燈光秀5日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=SINTR190206A' target='_top'>2/6</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=SINTRP05A" target="_top">...more</a></div>
				<div class="product_price"><span>$36,999起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img alt="" height="21" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png" width="61" /><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011383.png"/></p></div>
			</div>
		</div>
		<div class="title_box" >
			<div class="left_box" id="DAREA00017">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>東亞</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=ULNCXOMP06B" target="_top"><img src="/eWeb_summittour/IMGDB/001263/001380/001383/00017092.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=ULNCXOMP06B" target="_top">【春節】探索銀白大地~冬季蒙古國6日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=ULNCX190203A' target='_top'>2/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ULNCX190205A' target='_top'>2/5</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=ULNCXOMP06B" target="_top">...more</a></div>
				<div class="product_price"><span>$60,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
		</div>
	</div>
</body>
</html>

﻿<%
'//===========================================================================================
'//						index.asp
'//-------------------------------------------------------------------------------------------
'//		description		: 此頁面係由自動產生因此不用納入版本
'//-------------------------------------------------------------------------------------------
Response.CacheControl = "no-cache" 
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="robots" content="noindex">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
	<title>國外團體旅遊：韓國(首爾)旅遊行程 - 超悠</title>
	<!--產出時間:2018/12/4 下午 11:03:46 / WEB:summittour-->
	<link href='/eweb_summittour/css/reset.css' rel='stylesheet' type='text/css'>
	<link href='/eweb_summittour/css/autopage.css' rel='stylesheet' type='text/css'>
	<script languege="javascript" src="/script/jquery.js"></script>
	<script languege="javascript" src="/eweb_summittour/JS/autopage.js"></script>
</head>
<body>
	<div class="metropolis">
		<div class="title_box" >
			<div class="left_box" id="SAREA00303">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>韓國(首爾)</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=ICNBRP5A" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001093/001096/00008417.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=ICNBRP5A" target="_top">BR-韓國滑雪樂悠悠5日(歡樂滑雪.4D藝術館.樂天世界.樂天水族館.通仁市場.幻多奇秀)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR181219A' target='_top'>12/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR181230A' target='_top'>12/30</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR190104A' target='_top'>1/4</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR190110A' target='_top'>1/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR190117A' target='_top'>1/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR190210A' target='_top'>2/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR190211A' target='_top'>2/11</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR190215A' target='_top'>2/15</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=ICNBRP5A" target="_top">...more</a></div>
				<div class="product_price"><span>$12,000起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=ICNBRP5B" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001093/001096/00017573.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=ICNBRP5B" target="_top">BR-京畿SUPER SKI銀雪韓國5日(歡樂滑雪.光明洞窟.愛寶樂園.格雷萬蠟像館.拌飯秀)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR181221A' target='_top'>12/21</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR181225A' target='_top'>12/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR190101A' target='_top'>1/1</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR190102A' target='_top'>1/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR190108A' target='_top'>1/8</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR190109A' target='_top'>1/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR190115A' target='_top'>1/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR190116A' target='_top'>1/16</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=ICNBRP5B" target="_top">...more</a></div>
				<div class="product_price"><span>$12,500起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=ICNBRP5C" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001093/001096/00017605.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=ICNBRP5C" target="_top">BR-【台中出發】 韓國超值5天(滑雪體驗．HERO塗鴉秀．鐵路自行車．石鍋拌飯DIY)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR181223A' target='_top'>12/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR190106A' target='_top'>1/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR190113A' target='_top'>1/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR190120A' target='_top'>1/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR190127A' target='_top'>1/27</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR190217B' target='_top'>2/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR190224B' target='_top'>2/24</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=ICNBRP5C" target="_top">...more</a></div>
				<div class="product_price"><span>$11,900起</span></div>
   				<div class="product_offer"><p> </p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=ICNKEP5B" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001093/001096/00017412.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=ICNKEP5B" target="_top">KE-【莎朗嘿呦KOREA】特選冰釣~長腳蟹餐.鮑魚一隻雞.三大活動溜冰滑雪冰釣.空中花園.汗蒸幕韓國5日(午去晚回)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=ICNKE190114B' target='_top'>1/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNKE190119B' target='_top'>1/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNKE190121B' target='_top'>1/21</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=ICNKEP5B" target="_top">...more</a></div>
				<div class="product_price"><span>$16,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=ICNKEP5C" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001093/001096/00016375.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=ICNKEP5C" target="_top">KE-【莎朗嘿呦KOREA】季節冰釣~長腳蟹餐.鮑魚一隻雞.三大活動溜冰滑雪冰釣.東大門.汗蒸幕韓國5日(晚去晚回)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=ICNKE190121A' target='_top'>1/21</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=ICNKE190206A' target='_top'>2/6</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=ICNKEP5C" target="_top">...more</a></div>
				<div class="product_price"><span>$15,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=ICNTGP05B" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001093/001096/00017430.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=ICNTGP05B" target="_top">【泰航假期】旗艦韓國饕客米其林一星餐玩雪冰釣5日遊</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=ICNTG190118A' target='_top'>1/18</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=ICNTG190122B' target='_top'>1/22</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNTG190125A' target='_top'>1/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNTG190204A' target='_top'>2/4</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNTG190206B' target='_top'>2/6</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=ICNTG190210B' target='_top'>2/10</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNTG190213B' target='_top'>2/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNTG190215B' target='_top'>2/15</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=ICNTGP05B" target="_top">...more</a></div>
				<div class="product_price"><span>$29,400起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
		</div>
	</div>
</body>
</html>

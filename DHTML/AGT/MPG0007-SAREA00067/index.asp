﻿<%
'//===========================================================================================
'//						index.asp
'//-------------------------------------------------------------------------------------------
'//		description		: 此頁面係由自動產生因此不用納入版本
'//-------------------------------------------------------------------------------------------
Response.CacheControl = "no-cache" 
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="robots" content="noindex">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
	<title>國外團體旅遊：舊金山旅遊行程 - 超悠</title>
	<!--產出時間:2018/12/4 下午 11:03:55 / WEB:summittour-->
	<link href='/eweb_summittour/css/reset.css' rel='stylesheet' type='text/css'>
	<link href='/eweb_summittour/css/autopage.css' rel='stylesheet' type='text/css'>
	<script languege="javascript" src="/script/jquery.js"></script>
	<script languege="javascript" src="/eweb_summittour/JS/autopage.js"></script>
</head>
<body>
	<div class="metropolis">
		<div class="title_box" >
			<div class="left_box" id="SAREA00067">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>舊金山</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SFOHXP08B" target="_top"><img src="/eWeb_summittour/IMGDB/000577/000765/001230/00015117.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SFOHXP08B" target="_top">漫步北加州8天-入住舊金山市中心2晚  納帕品酒.太浩湖美景.沙加緬度老城.賭城雷諾.APPLE PARK</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=SFOHX190102B' target='_top'>1/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOHX190116B' target='_top'>1/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOHX190213B' target='_top'>2/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOHX190227B' target='_top'>2/27</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOHX190313B' target='_top'>3/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOHX190327B' target='_top'>3/27</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=SFOHXP08B" target="_top">...more</a></div>
				<div class="product_price"><span>$44,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008851.png"/><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SFOHXP08C" target="_top"><img src="/eWeb_summittour/IMGDB/000577/000765/001230/00013060.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SFOHXP08C" target="_top">遇見北加州8天-舊金山經典市區全覽(缪爾紅木公園.蘇莎利托.蝴蝶鎮.17哩.APPLE PARK)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=SFOHX190102A' target='_top'>1/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOHX190116A' target='_top'>1/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOHX190213A' target='_top'>2/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOHX190227A' target='_top'>2/27</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOHX190313A' target='_top'>3/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SFOHX190327A' target='_top'>3/27</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=SFOHXP08C" target="_top">...more</a></div>
				<div class="product_price"><span>$42,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008851.png"/><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /></p></div>
			</div>
		</div>
	</div>
</body>
</html>

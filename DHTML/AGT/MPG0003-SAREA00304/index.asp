﻿<%
'//===========================================================================================
'//						index.asp
'//-------------------------------------------------------------------------------------------
'//		description		: 此頁面係由自動產生因此不用納入版本
'//-------------------------------------------------------------------------------------------
Response.CacheControl = "no-cache" 
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="robots" content="noindex">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
	<title>國外團體旅遊：韓國(釜山、大邱、慶州)旅遊行程 - 超悠</title>
	<!--產出時間:2018/12/4 下午 11:03:46 / WEB:summittour-->
	<link href='/eweb_summittour/css/reset.css' rel='stylesheet' type='text/css'>
	<link href='/eweb_summittour/css/autopage.css' rel='stylesheet' type='text/css'>
	<script languege="javascript" src="/script/jquery.js"></script>
	<script languege="javascript" src="/eweb_summittour/JS/autopage.js"></script>
</head>
<body>
	<div class="metropolis">
		<div class="title_box" >
			<div class="left_box" id="SAREA00304">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>韓國(釜山、大邱、慶州)</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSBXP04A" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001394/001399/00017989.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSBXP04A" target="_top">【哇!釜山】鐵道自行車.天空步道.汗蒸幕特色美食四日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190105G' target='_top'>1/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190112G' target='_top'>1/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190119G' target='_top'>1/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190126G' target='_top'>1/26</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190128E' target='_top'>1/28</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190216G' target='_top'>2/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190309G' target='_top'>3/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190316H' target='_top'>3/16</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=PUSBXP04A" target="_top">...more</a></div>
				<div class="product_price"><span>$8,999起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSBXP04B" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001394/001399/00018010.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSBXP04B" target="_top">【哇!韓國】釜山、汗蒸幕、滑雪包雪具、八色烤肉四日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190105B' target='_top'>1/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190112B' target='_top'>1/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190119B' target='_top'>1/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190126B' target='_top'>1/26</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190216B' target='_top'>2/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190309B' target='_top'>3/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190316C' target='_top'>3/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190323B' target='_top'>3/23</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=PUSBXP04B" target="_top">...more</a></div>
				<div class="product_price"><span>$8,499起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSBXP05A" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001394/001399/00018021.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSBXP05A" target="_top">【哇!韓國】釜山雙天空步道、汗蒸幕、包雪具、體驗樂園、甘川洞、樂天超市五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX181208A' target='_top'>12/8</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX181215A' target='_top'>12/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX181222A' target='_top'>12/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190105C' target='_top'>1/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190107A' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190112C' target='_top'>1/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190114A' target='_top'>1/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190118A' target='_top'>1/18</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=PUSBXP05A" target="_top">...more</a></div>
				<div class="product_price"><span>$9,888起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSBXP05C" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001394/001399/00018029.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSBXP05C" target="_top">【哇!韓國】韓國霸氣首釜愛寶樂園海鮮吃到飽五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190105D' target='_top'>1/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190107B' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190112D' target='_top'>1/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190114B' target='_top'>1/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190119D' target='_top'>1/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190121B' target='_top'>1/21</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190126D' target='_top'>1/26</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190128B' target='_top'>1/28</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=PUSBXP05C" target="_top">...more</a></div>
				<div class="product_price"><span>$10,380起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSBXP05D" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001093/001096/00016032.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSBXP05D" target="_top">【哇!韓國。釜山】韓國全程五星樂天世界滑雪包雪具海鮮吃到飽看星月搖滾秀五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190105F' target='_top'>1/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190107D' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190112F' target='_top'>1/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190114D' target='_top'>1/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190119F' target='_top'>1/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190121D' target='_top'>1/21</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190123B' target='_top'>1/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190126F' target='_top'>1/26</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=PUSBXP05D" target="_top">...more</a></div>
				<div class="product_price"><span>$11,888起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSBXP05E" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001394/001403/00018082.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSBXP05E" target="_top">【哇!釜山】釜山雙天空步道、汗蒸幕、鐵道自行車、紅酒洞窟、甘川洞、樂天超市五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190105E' target='_top'>1/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190107C' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190112E' target='_top'>1/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190114C' target='_top'>1/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190119E' target='_top'>1/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190121C' target='_top'>1/21</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190126E' target='_top'>1/26</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190128C' target='_top'>1/28</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=PUSBXP05E" target="_top">...more</a></div>
				<div class="product_price"><span>$8,800起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSBXP05F" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001394/001403/00018082.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSBXP05F" target="_top">霸氣釜山雙天空步道汗蒸幕雙BUFFET五日-2/28連休</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190227A' target='_top'>2/27</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190228A' target='_top'>2/28</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190301A' target='_top'>3/1</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=PUSBXP05F" target="_top">...more</a></div>
				<div class="product_price"><span>$13,800起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSBXP05G" target="_top">春節版~【哇!韓國。釜山】韓國玩滑雪(包雪具)住滑雪渡假村水世界廚師秀五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190202A' target='_top'>2/2</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190207A' target='_top'>2/7</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=PUSBXP05G" target="_top">...more</a></div>
				<div class="product_price"><span>$17,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSBXP05H" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001394/001403/00018082.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSBXP05H" target="_top">春節版~【哇!韓國。釜山】韓國全程五星樂天世界滑雪包雪具海鮮吃到飽看星月搖滾秀五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190203B' target='_top'>2/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190204A' target='_top'>2/4</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=PUSBXP05H" target="_top">...more</a></div>
				<div class="product_price"><span>$22,800起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSBXP05I" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001394/001403/00018082.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSBXP05I" target="_top">春節版【哇!韓國】釜山雙天空步道、汗蒸幕、包雪具、體驗樂園、甘川洞、樂天超市五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190203A' target='_top'>2/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190208A' target='_top'>2/8</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=PUSBXP05I" target="_top">...more</a></div>
				<div class="product_price"><span>$13,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSITP5B" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001093/001096/00016347.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSITP5B" target="_top">IT-釜山玩巨濟島~積木村、大陵苑天馬塚、饗宴美食宵夜餐五日【升等兩晚五花酒店】</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PUSIT190301A' target='_top'>3/1</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSIT190304A' target='_top'>3/4</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSIT190306A' target='_top'>3/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSIT190311A' target='_top'>3/11</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSIT190313A' target='_top'>3/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSIT190315A' target='_top'>3/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSIT190318A' target='_top'>3/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSIT190320A' target='_top'>3/20</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=PUSITP5B" target="_top">...more</a></div>
				<div class="product_price"><span>$10,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSITP5C" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001093/001096/00016648.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSITP5C" target="_top">IT-釜山滑雪趣~伊甸園滑雪、積木村、大陵苑天馬塚美食優質版五日【保證不上攝影、全程住宿五花酒店】</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PUSIT181226A' target='_top'>12/26</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSIT190102A' target='_top'>1/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSIT190104A' target='_top'>1/4</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSIT190107A' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSIT190109A' target='_top'>1/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSIT190114A' target='_top'>1/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSIT190116A' target='_top'>1/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSIT190118A' target='_top'>1/18</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=PUSITP5C" target="_top">...more</a></div>
				<div class="product_price"><span>$12,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSITP5E" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001394/001399/00018406.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSITP5E" target="_top">IT-虎釜鎮海賞櫻去~愛來水族館、金海腳踏車、葡萄酒洞窟、炸雞宵夜餐美食五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PUSIT190325A' target='_top'>3/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSIT190327A' target='_top'>3/27</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSIT190329A' target='_top'>3/29</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=PUSITP5E" target="_top">...more</a></div>
				<div class="product_price"><span>$13,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSITP5F" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001093/001096/00017383.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSITP5F" target="_top">IT-釜山超值滑雪趣~伊甸園滑雪、美人魚秀、特麗愛3D奧妙藝術館、積木村美食五日【保證不上攝影】</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PUSIT181224B' target='_top'>12/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSIT181231B' target='_top'>12/31</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=PUSITP5F" target="_top">...more</a></div>
				<div class="product_price"><span>$8,999起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSKEP5A" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001093/001096/00009485.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSKEP5A" target="_top">KE-釜山滑雪去~伊甸園滑雪、美人魚秀、積木村美食五日【保證不上攝影】</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PUSKE181222A' target='_top'>12/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSKE181223A' target='_top'>12/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSKE181230A' target='_top'>12/30</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSKE190105A' target='_top'>1/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSKE190106A' target='_top'>1/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSKE190112A' target='_top'>1/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSKE190113A' target='_top'>1/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSKE190119A' target='_top'>1/19</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=PUSKEP5A" target="_top">...more</a></div>
				<div class="product_price"><span>$11,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSKEP5B" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001093/001096/00017500.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSKEP5B" target="_top">KE-釜邱雙城記~天空步道、愛來水族館(美人魚表演秀)、KAKAO FRIENDS旗艦店五天【保證不上攝影師】</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PUSKE190302A' target='_top'>3/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSKE190303A' target='_top'>3/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSKE190309A' target='_top'>3/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSKE190310A' target='_top'>3/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSKE190316A' target='_top'>3/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSKE190317A' target='_top'>3/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSKE190323A' target='_top'>3/23</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=PUSKEP5B" target="_top">...more</a></div>
				<div class="product_price"><span>$11,500起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSKEP5C" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001394/001399/00018431.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSKEP5C" target="_top">KE-大韓釜山鎮海賞櫻去~愛來水族館、特麗愛3D奧妙藝術館、積木村、西面鬧區美食五日【保證升等兩晚五花酒店+不派攝影師】</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PUSKE190324A' target='_top'>3/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSKE190327A' target='_top'>3/27</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSKE190328A' target='_top'>3/28</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=PUSKEP5C" target="_top">...more</a></div>
				<div class="product_price"><span>$12,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
		</div>
	</div>
</body>
</html>

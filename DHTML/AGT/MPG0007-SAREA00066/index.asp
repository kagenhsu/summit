﻿<%
'//===========================================================================================
'//						index.asp
'//-------------------------------------------------------------------------------------------
'//		description		: 此頁面係由自動產生因此不用納入版本
'//-------------------------------------------------------------------------------------------
Response.CacheControl = "no-cache" 
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="robots" content="noindex">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
	<title>國外團體旅遊：洛杉磯旅遊行程 - 超悠</title>
	<!--產出時間:2018/12/4 下午 11:03:54 / WEB:summittour-->
	<link href='/eweb_summittour/css/reset.css' rel='stylesheet' type='text/css'>
	<link href='/eweb_summittour/css/autopage.css' rel='stylesheet' type='text/css'>
	<script languege="javascript" src="/script/jquery.js"></script>
	<script languege="javascript" src="/eweb_summittour/JS/autopage.js"></script>
</head>
<body>
	<div class="metropolis">
		<div class="title_box" >
			<div class="left_box" id="SAREA00066">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>洛杉磯</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=LAXHXP08A" target="_top"><img src="/eWeb_summittour/IMGDB/000577/000764/000949/00013080.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=LAXHXP08A" target="_top">美西雙國家公園～大峽谷+錫安、羚羊峽谷+馬蹄灣8天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190106A' target='_top'>1/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190107C' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190120A' target='_top'>1/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190121A' target='_top'>1/21</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190217A' target='_top'>2/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190218C' target='_top'>2/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190224A' target='_top'>2/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190225C' target='_top'>2/25</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=LAXHXP08A" target="_top">...more</a></div>
				<div class="product_price"><span>$40,900起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008861.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=LAXHXP08B" target="_top"><img src="/eWeb_summittour/IMGDB/000577/000764/000949/00015383.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=LAXHXP08B" target="_top">美西四城~大峽谷.洛杉磯.拉斯維加斯.拉芙琳.聖地牙哥空軍一號8天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190101B' target='_top'>1/1</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190107B' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190109B' target='_top'>1/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190212B' target='_top'>2/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190218B' target='_top'>2/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190220B' target='_top'>2/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190225A' target='_top'>2/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190227C' target='_top'>2/27</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=LAXHXP08B" target="_top">...more</a></div>
				<div class="product_price"><span>$33,900起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=LAXHXP08D" target="_top"><img src="/eWeb_summittour/IMGDB/000577/000764/000949/00013080.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=LAXHXP08D" target="_top">美西三城~壯麗奇景大峽谷國家公園.洛杉磯.拉斯維加斯.拉芙琳.環球影城8天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190101A' target='_top'>1/1</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190107A' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190109A' target='_top'>1/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190212A' target='_top'>2/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190218A' target='_top'>2/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190220A' target='_top'>2/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190225B' target='_top'>2/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190227B' target='_top'>2/27</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=LAXHXP08D" target="_top">...more</a></div>
				<div class="product_price"><span>$36,900起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=LAXHXP08E" target="_top"><img src="/eWeb_summittour/IMGDB/000577/000764/000949/00010380.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=LAXHXP08E" target="_top">期間限定～洛杉磯、拉斯維加斯、聖地牙哥海景火車、藍瓶咖啡8天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190115A' target='_top'>1/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190304C' target='_top'>3/4</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=LAXHXP08E" target="_top">...more</a></div>
				<div class="product_price"><span>$26,888起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008846.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=LAXHXP10B" target="_top"><img src="/eWeb_summittour/IMGDB/000577/000764/000949/00014533.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=LAXHXP10B" target="_top">4321走一回!!四大國家公園~羚羊峽谷+馬蹄灣10天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190102A' target='_top'>1/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190116A' target='_top'>1/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190213A' target='_top'>2/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190227A' target='_top'>2/27</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190313A' target='_top'>3/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190327A' target='_top'>3/27</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=LAXHXP10B" target="_top">...more</a></div>
				<div class="product_price"><span>$51,900起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=LAXHXP14A" target="_top"><img src="/eWeb_summittour/IMGDB/000577/000764/000949/00014610.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=LAXHXP14A" target="_top">LUCKY7!!七大國家公園~羚羊峽谷+馬蹄灣14天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190513A' target='_top'>5/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190527A' target='_top'>5/27</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190603A' target='_top'>6/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190610A' target='_top'>6/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190624A' target='_top'>6/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190708A' target='_top'>7/8</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190722A' target='_top'>7/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=LAXHX190805A' target='_top'>8/5</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=LAXHXP14A" target="_top">...more</a></div>
				<div class="product_price"><span>$94,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
		</div>
	</div>
</body>
</html>

﻿<%
'//===========================================================================================
'//						index.asp
'//-------------------------------------------------------------------------------------------
'//		description		: 此頁面係由自動產生因此不用納入版本
'//-------------------------------------------------------------------------------------------
Response.CacheControl = "no-cache" 
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="robots" content="noindex">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
	<title>國外團體旅遊：菲律賓(長灘島、宿霧、巴拉望、科隆)旅遊行程 - 超悠</title>
	<!--產出時間:2018/12/4 下午 11:03:42 / WEB:summittour-->
	<link href='/eweb_summittour/css/reset.css' rel='stylesheet' type='text/css'>
	<link href='/eweb_summittour/css/autopage.css' rel='stylesheet' type='text/css'>
	<script languege="javascript" src="/script/jquery.js"></script>
	<script languege="javascript" src="/eweb_summittour/JS/autopage.js"></script>
</head>
<body>
	<div class="metropolis">
		<div class="title_box" >
			<div class="left_box" id="SAREA00031">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>菲律賓(長灘島、宿霧、巴拉望、科隆)</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEB05A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00017052.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEB05A" target="_top">衝一波宿霧吧！奇幻島、二日自由活動、自選飯店5日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEB05190102A' target='_top'>1/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEB05190105A' target='_top'>1/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEB05190109A' target='_top'>1/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEB05190112A' target='_top'>1/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEB05190116A' target='_top'>1/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEB05190119A' target='_top'>1/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEB05190123A' target='_top'>1/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEB05190126A' target='_top'>1/26</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=CEB05A" target="_top">...more</a></div>
				<div class="product_price"><span>$7,500起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEB05B" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00015892.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEB05B" target="_top">虎航版～衝一波宿霧吧！奇幻島、二日自由活動、自選飯店5+1日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEB05190101A' target='_top'>1/1</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEB05190108A' target='_top'>1/8</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEB05190115A' target='_top'>1/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEB05190122A' target='_top'>1/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEB05190129A' target='_top'>1/29</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEB05190212A' target='_top'>2/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEB05190219A' target='_top'>2/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEB05190305A' target='_top'>3/5</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=CEB05B" target="_top">...more</a></div>
				<div class="product_price"><span>$7,500起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBBR0502" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00010562.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBBR0502" target="_top">鯨菲昔比-歐斯陸與鯨鯊共舞精緻五日【豪華版】</a></div>
				<div class="departure_date">
					出發日期：
					<s><a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181214A' target='_top'>12/14</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181217A' target='_top'>12/17</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CEBBR0502" target="_top">...more</a></div>
				<div class="product_price"><span>$29,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBBR1805B" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00015885.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBBR1805B" target="_top">宿霧雙島合一：南方夢幻海域一島一酒店五日遊</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181219A' target='_top'>12/19</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CEBBR1805B" target="_top">...more</a></div>
				<div class="product_price"><span>$26,500起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBBR1805C" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00015892.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBBR1805C" target="_top">宿霧雙島合一：享樂生活一島一酒店五日遊</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181215C' target='_top'>12/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181216B' target='_top'>12/16</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CEBBR1805C" target="_top">...more</a></div>
				<div class="product_price"><span>$25,500起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBBR1805D" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00009949.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBBR1805D" target="_top">宿霧小蜜月~兩天自由行五日遊</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181218B' target='_top'>12/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181220A' target='_top'>12/20</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CEBBR1805D" target="_top">...more</a></div>
				<div class="product_price"><span>$19,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBBRP05B" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00015892.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBBRP05B" target="_top">2人成行～新鯨豔宿霧5日精采版</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181219B' target='_top'>12/19</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181222A' target='_top'>12/22</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR190123A' target='_top'>1/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR190227A' target='_top'>2/27</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CEBBRP05B" target="_top">...more</a></div>
				<div class="product_price"><span>$27,500起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBBRP05C" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00012586.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBBRP05C" target="_top">春節限定～新鯨奇無比宿霧5日特惠版</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR190201A' target='_top'>2/1</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR190203A' target='_top'>2/3</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CEBBRP05C" target="_top">...more</a></div>
				<div class="product_price"><span>$35,500起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011383.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBBRP05D" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00017330.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBBRP05D" target="_top">春節限定～新鯨豔宿霧5日繽紛版(長榮加班包機)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR190202A' target='_top'>2/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR190206A' target='_top'>2/6</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CEBBRP05D" target="_top">...more</a></div>
				<div class="product_price"><span>$40,500起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img alt="" height="21" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png" width="61" /><img alt="" height="21" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011383.png" width="61" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBBRP05E" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00009949.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBBRP05E" target="_top">雙宿雙菲~宿霧薄荷島雙島五日繽紛版</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181215D' target='_top'>12/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181217B' target='_top'>12/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181220C' target='_top'>12/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181223A' target='_top'>12/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181224A' target='_top'>12/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181225A' target='_top'>12/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181226A' target='_top'>12/26</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR190306A' target='_top'>3/6</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CEBBRP05E" target="_top">...more</a></div>
				<div class="product_price"><span>$25,500起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBBRP05F" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00017678.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBBRP05F" target="_top">新鯨奇無比宿霧5日特惠版</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181215E' target='_top'>12/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181217C' target='_top'>12/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181220D' target='_top'>12/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181223B' target='_top'>12/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181224B' target='_top'>12/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181225B' target='_top'>12/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR181226B' target='_top'>12/26</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBBR190123B' target='_top'>1/23</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=CEBBRP05F" target="_top">...more</a></div>
				<div class="product_price"><span>$23,500起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBITP05A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00009968.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBITP05A" target="_top">虎虎生風~宿霧資生堂跳島4+1日【輕鬆版】</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190124A' target='_top'>1/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190126A' target='_top'>1/26</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190131A' target='_top'>1/31</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190202A' target='_top'>2/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190207A' target='_top'>2/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190209A' target='_top'>2/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190214A' target='_top'>2/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190216A' target='_top'>2/16</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=CEBITP05A" target="_top">...more</a></div>
				<div class="product_price"><span>$12,500起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img alt="" height="21" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png" width="61" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBITP05B" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00017678.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBITP05B" target="_top">宿霧薄荷島資生堂雙跳島4+1日【超值版】</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190124B' target='_top'>1/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190126B' target='_top'>1/26</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190131B' target='_top'>1/31</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190202B' target='_top'>2/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190207B' target='_top'>2/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190209B' target='_top'>2/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190214B' target='_top'>2/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190216B' target='_top'>2/16</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=CEBITP05B" target="_top">...more</a></div>
				<div class="product_price"><span>$13,500起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBITP05C" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00012586.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBITP05C" target="_top">虎航宿霧薄荷島與鯨共舞全覽4+1日【全覽版】</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190124C' target='_top'>1/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190126C' target='_top'>1/26</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190131C' target='_top'>1/31</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190202C' target='_top'>2/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190207C' target='_top'>2/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190209C' target='_top'>2/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190214C' target='_top'>2/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190216C' target='_top'>2/16</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=CEBITP05C" target="_top">...more</a></div>
				<div class="product_price"><span>$19,500起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBITP05D" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00010562.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBITP05D" target="_top">0119聖嬰節限定～宿霧薄荷島資生堂雙跳島4+1日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190119A' target='_top'>1/19</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CEBITP05D" target="_top">...more</a></div>
				<div class="product_price"><span>$15,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBITP06A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00009949.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBITP06A" target="_top">虎虎生風~宿霧資生堂跳島5+1日【輕鬆版】</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190120A' target='_top'>1/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190122A' target='_top'>1/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190127A' target='_top'>1/27</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190129A' target='_top'>1/29</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190203A' target='_top'>2/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190205A' target='_top'>2/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190210A' target='_top'>2/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190212A' target='_top'>2/12</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=CEBITP06A" target="_top">...more</a></div>
				<div class="product_price"><span>$13,500起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBITP06B" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00012376.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBITP06B" target="_top">虎航宿霧薄荷島與鯨鯊共游5+1日【超值版】</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190120B' target='_top'>1/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190122B' target='_top'>1/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190127B' target='_top'>1/27</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190129B' target='_top'>1/29</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190203B' target='_top'>2/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190205B' target='_top'>2/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190210B' target='_top'>2/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190212B' target='_top'>2/12</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=CEBITP06B" target="_top">...more</a></div>
				<div class="product_price"><span>$19,500起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBITP06C" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00015904.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBITP06C" target="_top">虎航宿霧薄荷島與鯨共舞全覽5+1日【全覽版】</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190120C' target='_top'>1/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190122C' target='_top'>1/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190127C' target='_top'>1/27</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190129C' target='_top'>1/29</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190203C' target='_top'>2/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190210C' target='_top'>2/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CEBIT190212C' target='_top'>2/12</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=CEBITP06C" target="_top">...more</a></div>
				<div class="product_price"><span>$23,500起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBZ2P04A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00017046.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBZ2P04A" target="_top">宿霧玩很大玩家4日遊特別版早去晚回(亞航)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEBZ2190228A' target='_top'>2/28</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CEBZ2P04A" target="_top">...more</a></div>
				<div class="product_price"><span>$21,500起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011377.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBZ2P05A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00012586.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBZ2P05A" target="_top">宿霧薄荷島海濱渡假五日精采版(保證入住薄荷島海濱渡假村)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEBZ2190227A' target='_top'>2/27</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CEBZ2P05A" target="_top">...more</a></div>
				<div class="product_price"><span>$28,500起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img alt="" height="21" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png" width="61" /><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008845.png" width="60" /><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011377.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBZ2P05B" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00012376.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBZ2P05B" target="_top">宿霧玩很大~歐斯陸與鯨鯊共舞玩家五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEBZ2190227B' target='_top'>2/27</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CEBZ2P05B" target="_top">...more</a></div>
				<div class="product_price"><span>$25,500起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img alt="" height="21" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png" width="61" /><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008845.png" width="60" /><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011377.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBZ2P06A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00017056.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBZ2P06A" target="_top">宿霧薄荷島精采六日(早去早回)</a></div>
				<div class="departure_date">
					出發日期：
					<s><a href='/EW/GO/GroupDetail.asp?prodCd=CEBZ2181227C' target='_top'>12/27</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CEBZ2P06A" target="_top">...more</a></div>
				<div class="product_price"><span>$34,900起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011381.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBZ2P06B" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00010562.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CEBZ2P06B" target="_top">宿霧玩很大~歐斯陸與鯨鯊共舞玩家6日(早去早回)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CEBZ2181227D' target='_top'>12/27</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CEBZ2P06B" target="_top">...more</a></div>
				<div class="product_price"><span>$28,500起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=KLO5JP05A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00016897.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=KLO5JP05A" target="_top">最美長灘島回來了~風帆、香蕉船、沙灘按摩、環島遊、悠閒五日【直飛】</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=KLO5J190126A' target='_top'>1/26</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KLO5J190131A' target='_top'>1/31</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KLO5J190205A' target='_top'>2/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KLO5J190210A' target='_top'>2/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KLO5J190215A' target='_top'>2/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KLO5J190220A' target='_top'>2/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KLO5J190225A' target='_top'>2/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KLO5J190302A' target='_top'>3/2</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=KLO5JP05A" target="_top">...more</a></div>
				<div class="product_price"><span>$19,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=MNLCIP05A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00017322.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=MNLCIP05A" target="_top">【主題旅遊．愛冒險．全包式】菲律賓．EL_NIDO愛妮島．APULIT阿普莉渡假村5日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=MNLCI181214A' target='_top'>12/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=MNLCI181215A' target='_top'>12/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=MNLCI181216A' target='_top'>12/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=MNLCI181217A' target='_top'>12/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=MNLCI181218A' target='_top'>12/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=MNLCI181219A' target='_top'>12/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=MNLCI190101A' target='_top'>1/1</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=MNLCI190102A' target='_top'>1/2</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=MNLCIP05A" target="_top">...more</a></div>
				<div class="product_price"><span>$55,500起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=MPH5JP05A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00016897.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=MPH5JP05A" target="_top">春節限定5J轉機～HIGH翻長灘島悠閒五日</a></div>
 				<div class="product_description">春節旅遊~10人以上加派領隊</div> 
				<div class="departure_date">
					出發日期：
					<s><a href='/EW/GO/GroupDetail.asp?prodCd=MPH5J190202A' target='_top'>2/2</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=MPH5J190204A' target='_top'>2/4</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=MPH5J190206A' target='_top'>2/6</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=MPH5JP05A" target="_top">...more</a></div>
				<div class="product_price"><span>$22,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PPS5JP05A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00015348.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PPS5JP05A" target="_top">春節限定～愛上巴拉望一島一飯店跳島渡假5日(保證入住一島一飯店)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PPS5J190202B' target='_top'>2/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PPS5J190203B' target='_top'>2/3</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=PPS5JP05A" target="_top">...more</a></div>
				<div class="product_price"><span>$30,500起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PPS5JP05B" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00016479.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PPS5JP05B" target="_top">228限定～愛上巴拉望一島一飯店跳島渡假5日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PPS5J190227A' target='_top'>2/27</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PPS5J190228A' target='_top'>2/28</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=PPS5JP05B" target="_top">...more</a></div>
				<div class="product_price"><span>$28,500起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PPS5JP05C" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00016479.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PPS5JP05C" target="_top">春節限定5J轉機～玩瘋了巴拉望超級玩家、地底河流、本田灣五天</a></div>
 				<div class="product_description">春節旅遊~10人以上加派領隊</div> 
				<div class="departure_date">
					出發日期：
					<s><a href='/EW/GO/GroupDetail.asp?prodCd=PPS5J190202A' target='_top'>2/2</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PPS5J190203A' target='_top'>2/3</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=PPS5J190204A' target='_top'>2/4</a></s> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=PPS5J190205A' target='_top'>2/5</a></s> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=PPS5J190206A' target='_top'>2/6</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=PPS5JP05C" target="_top">...more</a></div>
				<div class="product_price"><span>$25,800起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011383.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PPSFEP05A" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00015348.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PPSFEP05A" target="_top">2人成行～新看見巴拉望渡假5日(保證入住一島一飯店)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PPSFE190119A' target='_top'>1/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PPSFE190123A' target='_top'>1/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PPSFE190127A' target='_top'>1/27</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PPSFE190212A' target='_top'>2/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PPSFE190216A' target='_top'>2/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PPSFE190220A' target='_top'>2/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PPSFE190224A' target='_top'>2/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PPSFE190228A' target='_top'>2/28</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=PPSFEP05A" target="_top">...more</a></div>
				<div class="product_price"><span>$26,500起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PPSFEP05C" target="_top"><img src="/eWeb_summittour/IMGDB/000098/000628/000686/00015904.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PPSFEP05C" target="_top">春節限定～2人成行新看見巴拉望渡假5日(保證入住一島一飯店)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PPSFE190131A' target='_top'>1/31</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PPSFE190204A' target='_top'>2/4</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=PPSFE190208A' target='_top'>2/8</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=PPSFEP05C" target="_top">...more</a></div>
				<div class="product_price"><span>$26,500起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00017443.png"/></p></div>
			</div>
		</div>
	</div>
</body>
</html>

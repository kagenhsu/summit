﻿<%
'//===========================================================================================
'//						index.asp
'//-------------------------------------------------------------------------------------------
'//		description		: 此頁面係由自動產生因此不用納入版本
'//-------------------------------------------------------------------------------------------
Response.CacheControl = "no-cache" 
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="robots" content="noindex">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
	<title>國外團體旅遊：江南、黃山、江西(南昌)旅遊行程 - 超悠</title>
	<!--產出時間:2018/12/4 下午 11:03:38 / WEB:summittour-->
	<link href='/eweb_summittour/css/reset.css' rel='stylesheet' type='text/css'>
	<link href='/eweb_summittour/css/autopage.css' rel='stylesheet' type='text/css'>
	<script languege="javascript" src="/script/jquery.js"></script>
	<script languege="javascript" src="/eweb_summittour/JS/autopage.js"></script>
</head>
<body>
	<div class="metropolis">
		<div class="title_box" >
			<div class="left_box" id="SAREA00040">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>江南、黃山、江西(南昌)</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=HGH0502" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000258/000995/00017924.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=HGH0502" target="_top">華信假期─杭州海寧上海楓涇古鎮尊貴五天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=HGHAEP59206A' target='_top'>2/6</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=HGH0502" target="_top">...more</a></div>
				<div class="product_price"><span>$26,000起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=HGHAEPR5A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000258/000995/00008171.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=HGHAEPR5A" target="_top">華信假期-黃山奇景千島湖西遞村杭州西湖五日</a></div>
				<div class="departure_date">
					出發日期：
					<s><a href='/EW/GO/GroupDetail.asp?prodCd=HGHAEP59109A' target='_top'>1/9</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=HGHAEPR5A" target="_top">...more</a></div>
				<div class="product_price"><span>$18,500起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.PNG	" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=NKGAET8A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000258/000995/00015205.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=NKGAET8A" target="_top">東方神起─南京揚州鹽城徐州句容尊爵逍遙八天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=NKGMUP89202A' target='_top'>2/2</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=NKGAET8A" target="_top">...more</a></div>
				<div class="product_price"><span>$27,000起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=NKGCIT8A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000258/000995/00015205.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=NKGCIT8A" target="_top">東方神起─南京無錫嘉興紹興橫店義烏烏鎮海寧句容超值八天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=NKGMUP88D22B' target='_top'>12/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NKGMUP89105B' target='_top'>1/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NKGMUP89119B' target='_top'>1/19</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=NKGMUP89302B' target='_top'>3/2</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=NKGCIT8A" target="_top">...more</a></div>
				<div class="product_price"><span>$16,900起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=NKGMUT8B" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000258/000995/00015205.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=NKGMUT8B" target="_top">東方航空─南京揚州瘦西湖鹽城丹頂鶴徐州句容尊爵八天</a></div>
				<div class="departure_date">
					出發日期：
					<s><a href='/EW/GO/GroupDetail.asp?prodCd=NKGMUP88D22A' target='_top'>12/22</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NKGMUP89112A' target='_top'>1/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NKGMUP89223A' target='_top'>2/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NKGMUP89309A' target='_top'>3/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NKGMUP89316A' target='_top'>3/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NKGMUP89323A' target='_top'>3/23</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=NKGMUT8B" target="_top">...more</a></div>
				<div class="product_price"><span>$20,000起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.PNG	" width="60" /><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008861.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=WUXAEPR5A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000258/000995/00017367.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=WUXAEPR5A" target="_top">華信假期─無錫泰州鹽城蘇北精華尊貴五天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=WUXAEP59107A' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=WUXAEP59218A' target='_top'>2/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=WUXAEP59304A' target='_top'>3/4</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=WUXAEP59318A' target='_top'>3/18</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=WUXAEPR5A" target="_top">...more</a></div>
				<div class="product_price"><span>$16,000起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=WUXAEPR5B" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000258/000995/00011499.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=WUXAEPR5B" target="_top">游遍中國─無錫杭州嘉興上海五天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=WUXAER59204A' target='_top'>2/4</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=WUXAEPR5B" target="_top">...more</a></div>
				<div class="product_price"><span>$23,500起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.png" width="60" /><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011383.png"/></p></div>
			</div>
		</div>
	</div>
</body>
</html>

﻿<%
'//===========================================================================================
'//						index.asp
'//-------------------------------------------------------------------------------------------
'//		description		: 此頁面係由自動產生因此不用納入版本
'//-------------------------------------------------------------------------------------------
Response.CacheControl = "no-cache" 
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="robots" content="noindex">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
	<title>國外團體旅遊：東北(花卷、仙台、青森、秋田、新潟)旅遊行程 - 超悠</title>
	<!--產出時間:2018/12/4 下午 11:03:45 / WEB:summittour-->
	<link href='/eweb_summittour/css/reset.css' rel='stylesheet' type='text/css'>
	<link href='/eweb_summittour/css/autopage.css' rel='stylesheet' type='text/css'>
	<script languege="javascript" src="/script/jquery.js"></script>
	<script languege="javascript" src="/eweb_summittour/JS/autopage.js"></script>
</head>
<body>
	<div class="metropolis">
		<div class="title_box" >
			<div class="left_box" id="SAREA00104">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>東北(花卷、仙台、青森、秋田、新潟)</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=NRTCXP6A" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000566/000593/00017823.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=NRTCXP6A" target="_top">【悠閒時光】日本東北奇幻樹冰~藏王樹冰纜車‧秋田內陸鐵道‧松島遊船.三溫泉6日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=NRTCX190127A' target='_top'>1/27</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=NRTCXP6A" target="_top">...more</a></div>
				<div class="product_price"><span>$42,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=NRTCXP7A" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000566/000593/00017822.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=NRTCXP7A" target="_top">【悠閒時光】日本東北奇幻樹冰~星野渡假村‧藏王樹冰纜車‧秋田內陸鐵道‧松島遊船.溫泉7日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=NRTCX190130A' target='_top'>1/30</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=NRTCXP7A" target="_top">...more</a></div>
				<div class="product_price"><span>$55,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SDJBRP05A" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000566/000593/00018226.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SDJBRP05A" target="_top">東北樹冰銀雪物語5日(藏王冰怪.景觀鐵道.銀山溫泉.松島遊船.採果樂)-仙台/仙台</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=SDJBR190102A' target='_top'>1/2</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=SDJBR190109A' target='_top'>1/9</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SDJBR190113A' target='_top'>1/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SDJBR190120A' target='_top'>1/20</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=SDJBR190123A' target='_top'>1/23</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SDJBR190130A' target='_top'>1/30</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=SDJBR190213A' target='_top'>2/13</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=SDJBRP05A" target="_top">...more</a></div>
				<div class="product_price"><span>$36,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
		</div>
	</div>
</body>
</html>

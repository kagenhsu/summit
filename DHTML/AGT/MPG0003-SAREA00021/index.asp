﻿<%
'//===========================================================================================
'//						index.asp
'//-------------------------------------------------------------------------------------------
'//		description		: 此頁面係由自動產生因此不用納入版本
'//-------------------------------------------------------------------------------------------
Response.CacheControl = "no-cache" 
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="robots" content="noindex">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
	<title>國外團體旅遊：東京(迪士尼、箱根、輕井澤)旅遊行程 - 超悠</title>
	<!--產出時間:2018/12/4 下午 11:03:45 / WEB:summittour-->
	<link href='/eweb_summittour/css/reset.css' rel='stylesheet' type='text/css'>
	<link href='/eweb_summittour/css/autopage.css' rel='stylesheet' type='text/css'>
	<script languege="javascript" src="/script/jquery.js"></script>
	<script languege="javascript" src="/eweb_summittour/JS/autopage.js"></script>
</head>
<body>
	<div class="metropolis">
		<div class="title_box" >
			<div class="left_box" id="SAREA00021">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>東京(迪士尼、箱根、輕井澤)</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=TYOAEP5B" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000563/000573/00016598.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=TYOAEP5B" target="_top">【遇見日本】小東北～鬼怒川 大內宿古街 那須高原五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=TYOAE181226A' target='_top'>12/26</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=TYOAEP5B" target="_top">...more</a></div>
				<div class="product_price"><span>$27,900起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.png" width="60" /><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008861.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=TYOAEP5C" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000563/000573/00017357.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=TYOAEP5C" target="_top">【遇見日本】冬季遊日本～山形藏王樹冰、那須滑雪五日遊</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=TYOAE190103A' target='_top'>1/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=TYOAE190112A' target='_top'>1/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=TYOAE190113A' target='_top'>1/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=TYOAE190116A' target='_top'>1/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=TYOAE190118A' target='_top'>1/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=TYOAE190130A' target='_top'>1/30</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=TYOAE190202A' target='_top'>2/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=TYOAE190206A' target='_top'>2/6</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=TYOAEP5C" target="_top">...more</a></div>
				<div class="product_price"><span>$31,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=TYOBRP05A" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000563/000573/00008130.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=TYOBRP05A" target="_top">東京迪士尼雙樂園 箱根海盜船 登上晴空塔5日(鬼太郎茶屋.富士山纜車.天空町商店街)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=TYOBR190103A' target='_top'>1/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=TYOBR190107A' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=TYOBR190110A' target='_top'>1/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=TYOBR190117A' target='_top'>1/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=TYOBR190211A' target='_top'>2/11</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=TYOBR190214A' target='_top'>2/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=TYOBR190218A' target='_top'>2/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=TYOBR190221A' target='_top'>2/21</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=TYOBRP05A" target="_top">...more</a></div>
				<div class="product_price"><span>$27,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=TYOBRP05B" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000563/000573/00008130.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=TYOBRP05B" target="_top">冬季戲雪趣~東京迪士尼 箱根周遊5日(箱根登山電車+斜面電車+空中纜車+海盜船)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=TYOBR181216A' target='_top'>12/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=TYOBR181225A' target='_top'>12/25</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=TYOBRP05B" target="_top">...more</a></div>
				<div class="product_price"><span>$28,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
		</div>
	</div>
</body>
</html>

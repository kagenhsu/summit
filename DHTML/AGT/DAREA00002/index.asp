﻿<%
'//===========================================================================================
'//						index.asp
'//-------------------------------------------------------------------------------------------
'//		description		: 此頁面係由自動產生因此不用納入版本
'//-------------------------------------------------------------------------------------------
Response.CacheControl = "no-cache" 
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="robots" content="noindex">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
	<title>國外團體旅遊：東北亞旅遊行程 - 超悠</title>
	<!--產出時間:2018/12/4 下午 11:03:07 / WEB:summittour-->
	<link href='/eweb_summittour/css/reset.css' rel='stylesheet' type='text/css'>
	<link href='/eweb_summittour/css/autopage.css' rel='stylesheet' type='text/css'>
	<script languege="javascript" src="/script/jquery.js"></script>
	<script languege="javascript" src="/eweb_summittour/JS/autopage.js"></script>
</head>
<body>
	<div class="metropolis">
		<div class="title_box" >
			<div class="left_box" id="SAREA00023">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>九州(大分、福岡、宮崎、鹿兒島、熊本)</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=FUKBRP05A" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000569/000603/00015059.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=FUKBRP05A" target="_top">輕遊九州鐵道雙溫泉五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=FUKBR181223A' target='_top'>12/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=FUKBR190107A' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=FUKBR190129A' target='_top'>1/29</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=FUKBR190215A' target='_top'>2/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=FUKBR190218A' target='_top'>2/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=FUKBR190222A' target='_top'>2/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=FUKBR190304A' target='_top'>3/4</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=FUKBR190318A' target='_top'>3/18</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=FUKBRP05A" target="_top">...more</a></div>
				<div class="product_price"><span>$24,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=FUKCIP5B" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000569/000603/00017346.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=FUKCIP5B" target="_top">【戲雪九州】旅人電車 和服體驗 長崎纜車五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=FUKCI181218A' target='_top'>12/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=FUKCI181225A' target='_top'>12/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=FUKCI190103A' target='_top'>1/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=FUKCI190108A' target='_top'>1/8</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=FUKCI190110A' target='_top'>1/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=FUKCI190115A' target='_top'>1/15</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=FUKCIP5B" target="_top">...more</a></div>
				<div class="product_price"><span>$25,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=FUKCIP5C" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000569/000603/00015775.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=FUKCIP5C" target="_top">【FUN寒假】豪斯登堡 旅人電車 和服體驗 五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=FUKCI190118A' target='_top'>1/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=FUKCI190122A' target='_top'>1/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=FUKCI190125A' target='_top'>1/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=FUKCI190129A' target='_top'>1/29</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=FUKCIP5C" target="_top">...more</a></div>
				<div class="product_price"><span>$31,800起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
		</div>
		<div class="title_box" >
			<div class="left_box" id="SAREA00020">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>北海道(札幌、函館、旭川)</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTSCIP5D" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000557/000559/00007942.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTSCIP5D" target="_top">【浪漫北海道】百萬夜景 小丑漢堡 浪漫小樽物語 五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CTSCI181220A' target='_top'>12/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTSCI190106A' target='_top'>1/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTSCI190110A' target='_top'>1/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTSCI190113A' target='_top'>1/13</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CTSCIP5D" target="_top">...more</a></div>
				<div class="product_price"><span>$27,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTSCIP5E" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000557/000559/00010753.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTSCIP5E" target="_top">【銀色北海道】流冰奇航 雪上活動 螃蟹美食五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CTSCI190120A' target='_top'>1/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTSCI190124A' target='_top'>1/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTSCI190127A' target='_top'>1/27</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTSCI190131A' target='_top'>1/31</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTSCI190214A' target='_top'>2/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTSCI190217A' target='_top'>2/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTSCI190221A' target='_top'>2/21</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CTSCI190224A' target='_top'>2/24</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CTSCIP5E" target="_top">...more</a></div>
				<div class="product_price"><span>$35,800起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTSCIP5F" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000557/000559/00017783.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTSCIP5F" target="_top">【新春北海道】流冰奇航 雪上活動 溫泉饗宴五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CTSCI190205A' target='_top'>2/5</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CTSCIP5F" target="_top">...more</a></div>
				<div class="product_price"><span>$60,800起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011383.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTSMMP5A" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000557/000559/00015057.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTSMMP5A" target="_top">【悠閒時光】北海道雪國祭典~道東阿寒湖、層雲峽、破冰船、玩雪四合一、三溫泉5日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CTSMM190206A' target='_top'>2/6</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CTSMMP5A" target="_top">...more</a></div>
				<div class="product_price"><span>$56,800起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img height="21" width="61" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011383.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTSMMP6A" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000557/000559/00017850.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CTSMMP6A" target="_top">【悠閒時光】北海道雪國祭典~道東阿寒湖、層雲峽、破冰船、玩雪四合一、三溫泉6日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CTSMM190203A' target='_top'>2/3</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CTSMMP6A" target="_top">...more</a></div>
				<div class="product_price"><span>$56,800起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /><img alt="" height="21" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00011383.png" width="61" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=HKDBRP05B" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000557/001166/00015776.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=HKDBRP05B" target="_top">北海道雪在燒5日(百萬夜景.北極星觀景列車.雪樂園.摩天輪.雙纜車.三大螃蟹)-函館/千歲</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=HKDBR181214A' target='_top'>12/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=HKDBR181217A' target='_top'>12/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=HKDBR181221A' target='_top'>12/21</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=HKDBRP05B" target="_top">...more</a></div>
				<div class="product_price"><span>$29,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
		</div>
		<div class="title_box" >
			<div class="left_box" id="SAREA00022">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>北陸黑部立山(名古屋、富山、小松、靜岡)</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=NGOCIP5D" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000565/000590/00017337.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=NGOCIP5D" target="_top">【戲雪名古屋】穗高纜車 童話合掌村 玩雪去 五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=NGOCI190104A' target='_top'>1/4</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NGOCI190106A' target='_top'>1/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NGOCI190111A' target='_top'>1/11</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NGOCI190113A' target='_top'>1/13</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=NGOCIP5D" target="_top">...more</a></div>
				<div class="product_price"><span>$26,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=NGOCIP5E" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000565/000590/00015906.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=NGOCIP5E" target="_top">【FUN寒假】樂高樂園 童話合掌村 玩雪趣 五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=NGOCI190116A' target='_top'>1/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NGOCI190123A' target='_top'>1/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NGOCI190130A' target='_top'>1/30</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=NGOCIP5E" target="_top">...more</a></div>
				<div class="product_price"><span>$33,800起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=NGOCXP5A" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000565/000590/00017337.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=NGOCXP5A" target="_top">【悠閒時光】雪白北陸童話村~白川鄉合掌村、絕景星空、兼六園、戲雪樂、三溫泉5日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=NGOCX190129A' target='_top'>1/29</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=NGOCXP5A" target="_top">...more</a></div>
				<div class="product_price"><span>$33,800起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=NRTCIP5B" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000565/000590/00017804.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=NRTCIP5B" target="_top">【浪漫花語】繽紛舞曲~足立藤花 粉蝶花海 粉紅芝櫻五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=NRTCI190416A' target='_top'>4/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NRTCI190418A' target='_top'>4/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NRTCI190420A' target='_top'>4/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NRTCI190422A' target='_top'>4/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NRTCI190511A' target='_top'>5/11</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NRTCI190513A' target='_top'>5/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NRTCI190515A' target='_top'>5/15</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=NRTCIP5B" target="_top">...more</a></div>
				<div class="product_price"><span>$33,900起</span></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=NRTCIP6A" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000565/000590/00014218.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=NRTCIP6A" target="_top">【雪壁絕景~立山黑部】童話合掌村 藤花伴芝櫻六日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=NRTCI190415A' target='_top'>4/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NRTCI190417A' target='_top'>4/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NRTCI190419A' target='_top'>4/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NRTCI190421A' target='_top'>4/21</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NRTCI190510A' target='_top'>5/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NRTCI190512A' target='_top'>5/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NRTCI190514A' target='_top'>5/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=NRTCI190516A' target='_top'>5/16</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=NRTCIP6A" target="_top">...more</a></div>
				<div class="product_price"><span>$36,800起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
		</div>
		<div class="title_box" >
			<div class="left_box" id="SAREA00112">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>沖繩(石垣、宮古、與論、琉球、久米島)</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=OKAITP4B" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000567/000600/00016600.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=OKAITP4B" target="_top">沖繩兜兜風～恐龍森林公園、海洋博、萬座毛、玉泉洞四日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=OKAIT181219A' target='_top'>12/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OKAIT181222A' target='_top'>12/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OKAIT181224A' target='_top'>12/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OKAIT181226A' target='_top'>12/26</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OKAIT181231A' target='_top'>12/31</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OKAIT190105A' target='_top'>1/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OKAIT190107B' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OKAIT190109B' target='_top'>1/9</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=OKAITP4B" target="_top">...more</a></div>
				<div class="product_price"><span>$16,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=OKAITP4C" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000567/000600/00017349.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=OKAITP4C" target="_top">享戀超值沖繩~腳踏車漫遊、海洋博、首里城、瀨長島四日【超級促銷團】</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=OKAIT181219B' target='_top'>12/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OKAIT190107A' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OKAIT190109A' target='_top'>1/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OKAIT190114A' target='_top'>1/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OKAIT190116A' target='_top'>1/16</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=OKAITP4C" target="_top">...more</a></div>
				<div class="product_price"><span>$11,999起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
		</div>
		<div class="title_box" >
			<div class="left_box" id="SAREA00104">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>東北(花卷、仙台、青森、秋田、新潟)</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=NRTCXP6A" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000566/000593/00017823.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=NRTCXP6A" target="_top">【悠閒時光】日本東北奇幻樹冰~藏王樹冰纜車‧秋田內陸鐵道‧松島遊船.三溫泉6日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=NRTCX190127A' target='_top'>1/27</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=NRTCXP6A" target="_top">...more</a></div>
				<div class="product_price"><span>$42,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=NRTCXP7A" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000566/000593/00017822.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=NRTCXP7A" target="_top">【悠閒時光】日本東北奇幻樹冰~星野渡假村‧藏王樹冰纜車‧秋田內陸鐵道‧松島遊船.溫泉7日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=NRTCX190130A' target='_top'>1/30</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=NRTCXP7A" target="_top">...more</a></div>
				<div class="product_price"><span>$55,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=SDJBRP05A" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000566/000593/00018226.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=SDJBRP05A" target="_top">東北樹冰銀雪物語5日(藏王冰怪.景觀鐵道.銀山溫泉.松島遊船.採果樂)-仙台/仙台</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=SDJBR190102A' target='_top'>1/2</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=SDJBR190109A' target='_top'>1/9</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SDJBR190113A' target='_top'>1/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SDJBR190120A' target='_top'>1/20</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=SDJBR190123A' target='_top'>1/23</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=SDJBR190130A' target='_top'>1/30</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=SDJBR190213A' target='_top'>2/13</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=SDJBRP05A" target="_top">...more</a></div>
				<div class="product_price"><span>$36,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
		</div>
		<div class="title_box" >
			<div class="left_box" id="SAREA00021">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>東京(迪士尼、箱根、輕井澤)</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=TYOAEP5B" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000563/000573/00016598.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=TYOAEP5B" target="_top">【遇見日本】小東北～鬼怒川 大內宿古街 那須高原五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=TYOAE181226A' target='_top'>12/26</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=TYOAEP5B" target="_top">...more</a></div>
				<div class="product_price"><span>$27,900起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.png" width="60" /><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008861.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=TYOAEP5C" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000563/000573/00017357.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=TYOAEP5C" target="_top">【遇見日本】冬季遊日本～山形藏王樹冰、那須滑雪五日遊</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=TYOAE190103A' target='_top'>1/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=TYOAE190112A' target='_top'>1/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=TYOAE190113A' target='_top'>1/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=TYOAE190116A' target='_top'>1/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=TYOAE190118A' target='_top'>1/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=TYOAE190130A' target='_top'>1/30</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=TYOAE190202A' target='_top'>2/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=TYOAE190206A' target='_top'>2/6</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=TYOAEP5C" target="_top">...more</a></div>
				<div class="product_price"><span>$31,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008853.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=TYOBRP05A" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000563/000573/00008130.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=TYOBRP05A" target="_top">東京迪士尼雙樂園 箱根海盜船 登上晴空塔5日(鬼太郎茶屋.富士山纜車.天空町商店街)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=TYOBR190103A' target='_top'>1/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=TYOBR190107A' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=TYOBR190110A' target='_top'>1/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=TYOBR190117A' target='_top'>1/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=TYOBR190211A' target='_top'>2/11</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=TYOBR190214A' target='_top'>2/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=TYOBR190218A' target='_top'>2/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=TYOBR190221A' target='_top'>2/21</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=TYOBRP05A" target="_top">...more</a></div>
				<div class="product_price"><span>$27,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=TYOBRP05B" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000563/000573/00008130.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=TYOBRP05B" target="_top">冬季戲雪趣~東京迪士尼 箱根周遊5日(箱根登山電車+斜面電車+空中纜車+海盜船)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=TYOBR181216A' target='_top'>12/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=TYOBR181225A' target='_top'>12/25</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=TYOBRP05B" target="_top">...more</a></div>
				<div class="product_price"><span>$28,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
		</div>
		<div class="title_box" >
			<div class="left_box" id="SAREA00303">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>韓國(首爾)</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=ICNBRP5A" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001093/001096/00008417.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=ICNBRP5A" target="_top">BR-韓國滑雪樂悠悠5日(歡樂滑雪.4D藝術館.樂天世界.樂天水族館.通仁市場.幻多奇秀)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR181219A' target='_top'>12/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR181230A' target='_top'>12/30</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR190104A' target='_top'>1/4</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR190110A' target='_top'>1/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR190117A' target='_top'>1/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR190210A' target='_top'>2/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR190211A' target='_top'>2/11</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR190215A' target='_top'>2/15</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=ICNBRP5A" target="_top">...more</a></div>
				<div class="product_price"><span>$12,000起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=ICNBRP5B" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001093/001096/00017573.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=ICNBRP5B" target="_top">BR-京畿SUPER SKI銀雪韓國5日(歡樂滑雪.光明洞窟.愛寶樂園.格雷萬蠟像館.拌飯秀)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR181221A' target='_top'>12/21</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR181225A' target='_top'>12/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR190101A' target='_top'>1/1</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR190102A' target='_top'>1/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR190108A' target='_top'>1/8</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR190109A' target='_top'>1/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR190115A' target='_top'>1/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR190116A' target='_top'>1/16</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=ICNBRP5B" target="_top">...more</a></div>
				<div class="product_price"><span>$12,500起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=ICNBRP5C" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001093/001096/00017605.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=ICNBRP5C" target="_top">BR-【台中出發】 韓國超值5天(滑雪體驗．HERO塗鴉秀．鐵路自行車．石鍋拌飯DIY)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR181223A' target='_top'>12/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR190106A' target='_top'>1/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR190113A' target='_top'>1/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR190120A' target='_top'>1/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR190127A' target='_top'>1/27</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR190217B' target='_top'>2/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNBR190224B' target='_top'>2/24</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=ICNBRP5C" target="_top">...more</a></div>
				<div class="product_price"><span>$11,900起</span></div>
   				<div class="product_offer"><p> </p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=ICNKEP5B" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001093/001096/00017412.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=ICNKEP5B" target="_top">KE-【莎朗嘿呦KOREA】特選冰釣~長腳蟹餐.鮑魚一隻雞.三大活動溜冰滑雪冰釣.空中花園.汗蒸幕韓國5日(午去晚回)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=ICNKE190114B' target='_top'>1/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNKE190119B' target='_top'>1/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNKE190121B' target='_top'>1/21</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=ICNKEP5B" target="_top">...more</a></div>
				<div class="product_price"><span>$16,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=ICNKEP5C" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001093/001096/00016375.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=ICNKEP5C" target="_top">KE-【莎朗嘿呦KOREA】季節冰釣~長腳蟹餐.鮑魚一隻雞.三大活動溜冰滑雪冰釣.東大門.汗蒸幕韓國5日(晚去晚回)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=ICNKE190121A' target='_top'>1/21</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=ICNKE190206A' target='_top'>2/6</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=ICNKEP5C" target="_top">...more</a></div>
				<div class="product_price"><span>$15,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=ICNTGP05B" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001093/001096/00017430.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=ICNTGP05B" target="_top">【泰航假期】旗艦韓國饕客米其林一星餐玩雪冰釣5日遊</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=ICNTG190118A' target='_top'>1/18</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=ICNTG190122B' target='_top'>1/22</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNTG190125A' target='_top'>1/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNTG190204A' target='_top'>2/4</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNTG190206B' target='_top'>2/6</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=ICNTG190210B' target='_top'>2/10</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNTG190213B' target='_top'>2/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNTG190215B' target='_top'>2/15</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=ICNTGP05B" target="_top">...more</a></div>
				<div class="product_price"><span>$29,400起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
		</div>
		<div class="title_box" >
			<div class="left_box" id="SAREA00048">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>韓國(首爾.釜山.濟州島)</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=ICNTGP05A" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001093/001096/00017445.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=ICNTGP05A" target="_top">【泰航假期】冬遊Running Man浪漫雪景纜車冰釣韓國5日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=ICNTG190103A' target='_top'>1/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNTG190109A' target='_top'>1/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNTG190110A' target='_top'>1/10</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=ICNTG190116A' target='_top'>1/16</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNTG190201A' target='_top'>2/1</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNTG190202A' target='_top'>2/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNTG190203A' target='_top'>2/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ICNTG190205A' target='_top'>2/5</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=ICNTGP05A" target="_top">...more</a></div>
				<div class="product_price"><span>$14,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
		</div>
		<div class="title_box" >
			<div class="left_box" id="SAREA00304">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>韓國(釜山、大邱、慶州)</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSBXP04A" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001394/001399/00017989.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSBXP04A" target="_top">【哇!釜山】鐵道自行車.天空步道.汗蒸幕特色美食四日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190105G' target='_top'>1/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190112G' target='_top'>1/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190119G' target='_top'>1/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190126G' target='_top'>1/26</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190128E' target='_top'>1/28</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190216G' target='_top'>2/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190309G' target='_top'>3/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190316H' target='_top'>3/16</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=PUSBXP04A" target="_top">...more</a></div>
				<div class="product_price"><span>$8,999起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSBXP04B" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001394/001399/00018010.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSBXP04B" target="_top">【哇!韓國】釜山、汗蒸幕、滑雪包雪具、八色烤肉四日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190105B' target='_top'>1/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190112B' target='_top'>1/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190119B' target='_top'>1/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190126B' target='_top'>1/26</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190216B' target='_top'>2/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190309B' target='_top'>3/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190316C' target='_top'>3/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190323B' target='_top'>3/23</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=PUSBXP04B" target="_top">...more</a></div>
				<div class="product_price"><span>$8,499起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSBXP05A" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001394/001399/00018021.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSBXP05A" target="_top">【哇!韓國】釜山雙天空步道、汗蒸幕、包雪具、體驗樂園、甘川洞、樂天超市五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX181208A' target='_top'>12/8</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX181215A' target='_top'>12/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX181222A' target='_top'>12/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190105C' target='_top'>1/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190107A' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190112C' target='_top'>1/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190114A' target='_top'>1/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190118A' target='_top'>1/18</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=PUSBXP05A" target="_top">...more</a></div>
				<div class="product_price"><span>$9,888起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSBXP05C" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001394/001399/00018029.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSBXP05C" target="_top">【哇!韓國】韓國霸氣首釜愛寶樂園海鮮吃到飽五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190105D' target='_top'>1/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190107B' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190112D' target='_top'>1/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190114B' target='_top'>1/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190119D' target='_top'>1/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190121B' target='_top'>1/21</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190126D' target='_top'>1/26</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190128B' target='_top'>1/28</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=PUSBXP05C" target="_top">...more</a></div>
				<div class="product_price"><span>$10,380起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSBXP05D" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001093/001096/00016032.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSBXP05D" target="_top">【哇!韓國。釜山】韓國全程五星樂天世界滑雪包雪具海鮮吃到飽看星月搖滾秀五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190105F' target='_top'>1/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190107D' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190112F' target='_top'>1/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190114D' target='_top'>1/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190119F' target='_top'>1/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190121D' target='_top'>1/21</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190123B' target='_top'>1/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190126F' target='_top'>1/26</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=PUSBXP05D" target="_top">...more</a></div>
				<div class="product_price"><span>$11,888起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSBXP05E" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001394/001403/00018082.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSBXP05E" target="_top">【哇!釜山】釜山雙天空步道、汗蒸幕、鐵道自行車、紅酒洞窟、甘川洞、樂天超市五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190105E' target='_top'>1/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190107C' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190112E' target='_top'>1/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190114C' target='_top'>1/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190119E' target='_top'>1/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190121C' target='_top'>1/21</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190126E' target='_top'>1/26</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190128C' target='_top'>1/28</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=PUSBXP05E" target="_top">...more</a></div>
				<div class="product_price"><span>$8,800起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSBXP05F" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001394/001403/00018082.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSBXP05F" target="_top">霸氣釜山雙天空步道汗蒸幕雙BUFFET五日-2/28連休</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190227A' target='_top'>2/27</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190228A' target='_top'>2/28</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190301A' target='_top'>3/1</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=PUSBXP05F" target="_top">...more</a></div>
				<div class="product_price"><span>$13,800起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSBXP05G" target="_top">春節版~【哇!韓國。釜山】韓國玩滑雪(包雪具)住滑雪渡假村水世界廚師秀五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190202A' target='_top'>2/2</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190207A' target='_top'>2/7</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=PUSBXP05G" target="_top">...more</a></div>
				<div class="product_price"><span>$17,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSBXP05H" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001394/001403/00018082.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSBXP05H" target="_top">春節版~【哇!韓國。釜山】韓國全程五星樂天世界滑雪包雪具海鮮吃到飽看星月搖滾秀五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190203B' target='_top'>2/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190204A' target='_top'>2/4</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=PUSBXP05H" target="_top">...more</a></div>
				<div class="product_price"><span>$22,800起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSBXP05I" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001394/001403/00018082.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSBXP05I" target="_top">春節版【哇!韓國】釜山雙天空步道、汗蒸幕、包雪具、體驗樂園、甘川洞、樂天超市五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190203A' target='_top'>2/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSBX190208A' target='_top'>2/8</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=PUSBXP05I" target="_top">...more</a></div>
				<div class="product_price"><span>$13,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSITP5B" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001093/001096/00016347.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSITP5B" target="_top">IT-釜山玩巨濟島~積木村、大陵苑天馬塚、饗宴美食宵夜餐五日【升等兩晚五花酒店】</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PUSIT190301A' target='_top'>3/1</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSIT190304A' target='_top'>3/4</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSIT190306A' target='_top'>3/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSIT190311A' target='_top'>3/11</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSIT190313A' target='_top'>3/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSIT190315A' target='_top'>3/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSIT190318A' target='_top'>3/18</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSIT190320A' target='_top'>3/20</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=PUSITP5B" target="_top">...more</a></div>
				<div class="product_price"><span>$10,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSITP5C" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001093/001096/00016648.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSITP5C" target="_top">IT-釜山滑雪趣~伊甸園滑雪、積木村、大陵苑天馬塚美食優質版五日【保證不上攝影、全程住宿五花酒店】</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PUSIT181226A' target='_top'>12/26</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSIT190102A' target='_top'>1/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSIT190104A' target='_top'>1/4</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSIT190107A' target='_top'>1/7</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSIT190109A' target='_top'>1/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSIT190114A' target='_top'>1/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSIT190116A' target='_top'>1/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSIT190118A' target='_top'>1/18</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=PUSITP5C" target="_top">...more</a></div>
				<div class="product_price"><span>$12,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSITP5E" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001394/001399/00018406.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSITP5E" target="_top">IT-虎釜鎮海賞櫻去~愛來水族館、金海腳踏車、葡萄酒洞窟、炸雞宵夜餐美食五日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PUSIT190325A' target='_top'>3/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSIT190327A' target='_top'>3/27</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSIT190329A' target='_top'>3/29</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=PUSITP5E" target="_top">...more</a></div>
				<div class="product_price"><span>$13,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSITP5F" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001093/001096/00017383.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSITP5F" target="_top">IT-釜山超值滑雪趣~伊甸園滑雪、美人魚秀、特麗愛3D奧妙藝術館、積木村美食五日【保證不上攝影】</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PUSIT181224B' target='_top'>12/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSIT181231B' target='_top'>12/31</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=PUSITP5F" target="_top">...more</a></div>
				<div class="product_price"><span>$8,999起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSKEP5A" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001093/001096/00009485.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSKEP5A" target="_top">KE-釜山滑雪去~伊甸園滑雪、美人魚秀、積木村美食五日【保證不上攝影】</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PUSKE181222A' target='_top'>12/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSKE181223A' target='_top'>12/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSKE181230A' target='_top'>12/30</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSKE190105A' target='_top'>1/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSKE190106A' target='_top'>1/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSKE190112A' target='_top'>1/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSKE190113A' target='_top'>1/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSKE190119A' target='_top'>1/19</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=PUSKEP5A" target="_top">...more</a></div>
				<div class="product_price"><span>$11,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSKEP5B" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001093/001096/00017500.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSKEP5B" target="_top">KE-釜邱雙城記~天空步道、愛來水族館(美人魚表演秀)、KAKAO FRIENDS旗艦店五天【保證不上攝影師】</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PUSKE190302A' target='_top'>3/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSKE190303A' target='_top'>3/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSKE190309A' target='_top'>3/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSKE190310A' target='_top'>3/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSKE190316A' target='_top'>3/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSKE190317A' target='_top'>3/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSKE190323A' target='_top'>3/23</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=PUSKEP5B" target="_top">...more</a></div>
				<div class="product_price"><span>$11,500起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSKEP5C" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001394/001399/00018431.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=PUSKEP5C" target="_top">KE-大韓釜山鎮海賞櫻去~愛來水族館、特麗愛3D奧妙藝術館、積木村、西面鬧區美食五日【保證升等兩晚五花酒店+不派攝影師】</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=PUSKE190324A' target='_top'>3/24</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSKE190327A' target='_top'>3/27</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=PUSKE190328A' target='_top'>3/28</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=PUSKEP5C" target="_top">...more</a></div>
				<div class="product_price"><span>$12,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
		</div>
		<div class="title_box" >
			<div class="left_box" id="SAREA00305">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>韓國(濟州島)</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CJUITP04A" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001093/001096/00017374.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CJUITP04A" target="_top">PAK輕鬆遊三多島濟州4日漢拏山國家公園〮城山日出峰〮</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CJUIT181219A' target='_top'>12/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CJUIT181225A' target='_top'>12/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CJUIT181226A' target='_top'>12/26</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CJUIT190101A' target='_top'>1/1</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CJUIT190102A' target='_top'>1/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CJUIT190108A' target='_top'>1/8</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CJUIT190109A' target='_top'>1/9</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CJUIT190115A' target='_top'>1/15</a>
					<a style ='' href="/EW/GO/GroupList.asp?mGrupCd=CJUITP04A" target="_top">...more</a></div>
				<div class="product_price"><span>$11,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CJUITP05A" target="_top"><img src="/eWeb_summittour/IMGDB/000182/001093/001096/00017371.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CJUITP05A" target="_top">PAK輕鬆遊三多島濟州5日3D奧妙藝術博物館。</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CJUIT181214A' target='_top'>12/14</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CJUIT181215A' target='_top'>12/15</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CJUIT181221A' target='_top'>12/21</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CJUIT181222A' target='_top'>12/22</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CJUIT190112A' target='_top'>1/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CJUIT190125A' target='_top'>1/25</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CJUIT190126A' target='_top'>1/26</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CJUITP05A" target="_top">...more</a></div>
				<div class="product_price"><span>$15,500起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
		</div>
		<div class="title_box" >
			<div class="left_box" id="SAREA00003">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>關西(大阪、京都、神戶、奈良、南紀)</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=KIXCXP5A" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000564/000583/00008253.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=KIXCXP5A" target="_top">【悠閒時光】日本南紀豪華三溫泉美食版5日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=KIXCX190110A' target='_top'>1/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KIXCX190117A' target='_top'>1/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KIXCX190221A' target='_top'>2/21</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KIXCX190302A' target='_top'>3/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KIXCX190309A' target='_top'>3/9</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=KIXCXP5A" target="_top">...more</a></div>
				<div class="product_price"><span>$29,800起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=KIXCXP6A" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000564/000583/00017863.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=KIXCXP6A" target="_top">【悠閒時光】大阪海之京都天橋立~三方五湖彩虹路(搭乘纜車).美山合掌村.歡樂環球影城.溫泉6日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=KIXCX190126A' target='_top'>1/26</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KIXCX190130A' target='_top'>1/30</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KIXCX190208A' target='_top'>2/8</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KIXCX190209A' target='_top'>2/9</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=KIXCXP6A" target="_top">...more</a></div>
				<div class="product_price"><span>$37,800起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=KIXCXP6B" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000564/000583/00017875.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=KIXCXP6B" target="_top">【悠閒時光】四國淡路夢之紀行~小豆島‧松山城公園‧祖谷溪葛藤橋‧鳴門漩渦‧道後溫泉六日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=KIXCX190111A' target='_top'>1/11</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KIXCX190116A' target='_top'>1/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KIXCX190302B' target='_top'>3/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KIXCX190309B' target='_top'>3/9</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=KIXCXP6B" target="_top">...more</a></div>
				<div class="product_price"><span>$39,800起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=OSABRP05A" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000564/000583/00018160.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=OSABRP05A" target="_top">浪漫京阪神樂園5日(舞子步道.清水寺.嵐山竹林步道.環球影城.神戶港灣散策)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=OSABR181219A' target='_top'>12/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OSABR181226A' target='_top'>12/26</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OSABR181231A' target='_top'>12/31</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=OSABRP05A" target="_top">...more</a></div>
				<div class="product_price"><span>$30,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=OSABRP05B" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000564/000583/00018166.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=OSABRP05B" target="_top">浪漫京阪神戲雪5日(兩晚五星.六甲山夜景.戲雪.壽司DIY.嵐山渡月橋.環球影城.和服體驗)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=OSABR190101A' target='_top'>1/1</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OSABR190105A' target='_top'>1/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OSABR190108A' target='_top'>1/8</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OSABR190112A' target='_top'>1/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OSABR190216A' target='_top'>2/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OSABR190219A' target='_top'>2/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OSABR190223A' target='_top'>2/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OSABR190319A' target='_top'>3/19</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=OSABRP05B" target="_top">...more</a></div>
				<div class="product_price"><span>$32,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=OSABRP06A" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000564/000583/00018190.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=OSABRP06A" target="_top">山陰山陽6日(鳥取砂丘.鬼太郎通路.水木茂記念館.柯南的故鄉.伊根舟屋.伊根湾海鷗號遊覧船.天橋立)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=OSABR190106A' target='_top'>1/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OSABR190113A' target='_top'>1/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OSABR190210A' target='_top'>2/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OSABR190217A' target='_top'>2/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OSABR190303A' target='_top'>3/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OSABR190310A' target='_top'>3/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OSABR190317A' target='_top'>3/17</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=OSABRP06A" target="_top">...more</a></div>
				<div class="product_price"><span>$35,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
		</div>
	</div>
</body>
</html>

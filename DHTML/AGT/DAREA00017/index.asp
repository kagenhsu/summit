﻿<%
'//===========================================================================================
'//						index.asp
'//-------------------------------------------------------------------------------------------
'//		description		: 此頁面係由自動產生因此不用納入版本
'//-------------------------------------------------------------------------------------------
Response.CacheControl = "no-cache" 
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="robots" content="noindex">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
	<title>國外團體旅遊：東亞旅遊行程 - 超悠</title>
	<!--產出時間:2018/12/4 下午 11:03:14 / WEB:summittour-->
	<link href='/eweb_summittour/css/reset.css' rel='stylesheet' type='text/css'>
	<link href='/eweb_summittour/css/autopage.css' rel='stylesheet' type='text/css'>
	<script languege="javascript" src="/script/jquery.js"></script>
	<script languege="javascript" src="/eweb_summittour/JS/autopage.js"></script>
</head>
<body>
	<div class="metropolis">
		<div class="title_box" >
			<div class="left_box" id="SAREA00296">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>蒙古國+西伯利亞</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=ULNCXOMP06A" target="_top"><img src="/eWeb_summittour/IMGDB/001263/001380/001383/00017093.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=ULNCXOMP06A" target="_top">探索銀白大地~冬雪蒙古國6日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=ULNCX181220A' target='_top'>12/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ULNCX181227A' target='_top'>12/27</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ULNCX190110A' target='_top'>1/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ULNCX190124A' target='_top'>1/24</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=ULNCXOMP06A" target="_top">...more</a></div>
				<div class="product_price"><span>$55,900起</span></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=ULNCXOMP06B" target="_top"><img src="/eWeb_summittour/IMGDB/001263/001380/001383/00017092.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=ULNCXOMP06B" target="_top">【春節】探索銀白大地~冬季蒙古國6日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=ULNCX190203A' target='_top'>2/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=ULNCX190205A' target='_top'>2/5</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=ULNCXOMP06B" target="_top">...more</a></div>
				<div class="product_price"><span>$60,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
		</div>
	</div>
</body>
</html>

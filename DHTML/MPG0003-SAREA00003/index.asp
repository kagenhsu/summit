﻿<%
'//===========================================================================================
'//						index.asp
'//-------------------------------------------------------------------------------------------
'//		description		: 此頁面係由自動產生因此不用納入版本
'//-------------------------------------------------------------------------------------------
Response.CacheControl = "no-cache" 
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="robots" content="noindex">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
	<title>國外團體旅遊：關西(大阪、京都、神戶、奈良、南紀)旅遊行程 - 超悠</title>
	<!--產出時間:2018/12/4 下午 11:01:29 / WEB:summittour-->
	<link href='/eweb_summittour/css/reset.css' rel='stylesheet' type='text/css'>
	<link href='/eweb_summittour/css/autopage.css' rel='stylesheet' type='text/css'>
	<script languege="javascript" src="/script/jquery.js"></script>
	<script languege="javascript" src="/eweb_summittour/JS/autopage.js"></script>
</head>
<body>
	<div class="metropolis">
		<div class="title_box" >
			<div class="left_box" id="SAREA00003">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>關西(大阪、京都、神戶、奈良、南紀)</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=KIXCXP5A" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000564/000583/00008253.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=KIXCXP5A" target="_top">【悠閒時光】日本南紀豪華三溫泉美食版5日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=KIXCX190110A' target='_top'>1/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KIXCX190117A' target='_top'>1/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KIXCX190221A' target='_top'>2/21</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KIXCX190302A' target='_top'>3/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KIXCX190309A' target='_top'>3/9</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=KIXCXP5A" target="_top">...more</a></div>
				<div class="product_price"><span>$32,800起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=KIXCXP6A" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000564/000583/00017863.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=KIXCXP6A" target="_top">【悠閒時光】大阪海之京都天橋立~三方五湖彩虹路(搭乘纜車).美山合掌村.歡樂環球影城.溫泉6日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=KIXCX190126A' target='_top'>1/26</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KIXCX190130A' target='_top'>1/30</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KIXCX190208A' target='_top'>2/8</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KIXCX190209A' target='_top'>2/9</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=KIXCXP6A" target="_top">...more</a></div>
				<div class="product_price"><span>$40,800起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=KIXCXP6B" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000564/000583/00017875.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=KIXCXP6B" target="_top">【悠閒時光】四國淡路夢之紀行~小豆島‧松山城公園‧祖谷溪葛藤橋‧鳴門漩渦‧道後溫泉六日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=KIXCX190111A' target='_top'>1/11</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KIXCX190116A' target='_top'>1/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KIXCX190302B' target='_top'>3/2</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=KIXCX190309B' target='_top'>3/9</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=KIXCXP6B" target="_top">...more</a></div>
				<div class="product_price"><span>$42,800起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=OSABRP05A" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000564/000583/00018160.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=OSABRP05A" target="_top">浪漫京阪神樂園5日(舞子步道.清水寺.嵐山竹林步道.環球影城.神戶港灣散策)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=OSABR181219A' target='_top'>12/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OSABR181226A' target='_top'>12/26</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OSABR181231A' target='_top'>12/31</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=OSABRP05A" target="_top">...more</a></div>
				<div class="product_price"><span>$33,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=OSABRP05B" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000564/000583/00018166.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=OSABRP05B" target="_top">浪漫京阪神戲雪5日(兩晚五星.六甲山夜景.戲雪.壽司DIY.嵐山渡月橋.環球影城.和服體驗)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=OSABR190101A' target='_top'>1/1</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OSABR190105A' target='_top'>1/5</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OSABR190108A' target='_top'>1/8</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OSABR190112A' target='_top'>1/12</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OSABR190216A' target='_top'>2/16</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OSABR190219A' target='_top'>2/19</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OSABR190223A' target='_top'>2/23</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OSABR190319A' target='_top'>3/19</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=OSABRP05B" target="_top">...more</a></div>
				<div class="product_price"><span>$35,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=OSABRP06A" target="_top"><img src="/eWeb_summittour/IMGDB/000182/000564/000583/00018190.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=OSABRP06A" target="_top">山陰山陽6日(鳥取砂丘.鬼太郎通路.水木茂記念館.柯南的故鄉.伊根舟屋.伊根湾海鷗號遊覧船.天橋立)</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=OSABR190106A' target='_top'>1/6</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OSABR190113A' target='_top'>1/13</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OSABR190210A' target='_top'>2/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OSABR190217A' target='_top'>2/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OSABR190303A' target='_top'>3/3</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OSABR190310A' target='_top'>3/10</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=OSABR190317A' target='_top'>3/17</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=OSABRP06A" target="_top">...more</a></div>
				<div class="product_price"><span>$38,900起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
		</div>
	</div>
</body>
</html>

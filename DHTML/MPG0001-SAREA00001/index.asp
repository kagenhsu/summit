﻿<%
'//===========================================================================================
'//						index.asp
'//-------------------------------------------------------------------------------------------
'//		description		: 此頁面係由自動產生因此不用納入版本
'//-------------------------------------------------------------------------------------------
Response.CacheControl = "no-cache" 
%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="robots" content="noindex">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
	<title>國外團體旅遊：長沙、張家界旅遊行程 - 超悠</title>
	<!--產出時間:2018/12/4 下午 11:01:19 / WEB:summittour-->
	<link href='/eweb_summittour/css/reset.css' rel='stylesheet' type='text/css'>
	<link href='/eweb_summittour/css/autopage.css' rel='stylesheet' type='text/css'>
	<script languege="javascript" src="/script/jquery.js"></script>
	<script languege="javascript" src="/eweb_summittour/JS/autopage.js"></script>
</head>
<body>
	<div class="metropolis">
		<div class="title_box" >
			<div class="left_box" id="SAREA00001">
				<img src="/eweb_summittour/images/metropolis_title_icon_00.png"/>
				<h1>長沙、張家界</h1>
			</div>
		</div>
		<div class="product_all"> 
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CSXAEPT8A" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000080/000993/00018270.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CSXAEPT8A" target="_top">華信假期─湘江風光張家界鳳凰天空玻璃橋尊爵8日</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CSXAEP89321A' target='_top'>3/21</a>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CSXAEPT8A" target="_top">...more</a></div>
				<div class="product_price"><span>$26,800起</span></div>
   				<div class="product_offer"><p><img alt="" height="22" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png" width="60" /></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CSXAEPT8B" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000080/000993/00008166.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CSXAEPT8B" target="_top">華信假期─湘媚風光張家界鳳凰天空玻璃橋豪華8天</a></div>
				<div class="departure_date">
					出發日期：
					<s><a href='/EW/GO/GroupDetail.asp?prodCd=CSXAEP89207A' target='_top'>2/7</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CSXAEPT8B" target="_top">...more</a></div>
				<div class="product_price"><span>$28,800起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
    		<div class="product_box">
				<div class="product_img"><a href="/EW/GO/MGroupDetail.asp?prodCd=CSXAEPT8C" target="_top"><img src="/eWeb_summittour/IMGDB/000060/000080/000993/00016859.JPG" /></a></div>
				<div class="product_name"><a href="/EW/GO/MGroupDetail.asp?prodCd=CSXAEPT8C" target="_top">華信假期─湘西風光張家界鳳凰天空玻璃橋豪華8天</a></div>
				<div class="departure_date">
					出發日期：
					<a href='/EW/GO/GroupDetail.asp?prodCd=CSXAEP88D20A' target='_top'>12/20</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CSXAEP89103A' target='_top'>1/3</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=CSXAEP89110A' target='_top'>1/10</a></s> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CSXAEP89117A' target='_top'>1/17</a> , 
			<a href='/EW/GO/GroupDetail.asp?prodCd=CSXAEP89228A' target='_top'>2/28</a> , 
			<s><a href='/EW/GO/GroupDetail.asp?prodCd=CSXAEP89307A' target='_top'>3/7</a></s>
					<a style ='display:none' href="/EW/GO/GroupList.asp?mGrupCd=CSXAEPT8C" target="_top">...more</a></div>
				<div class="product_price"><span>$19,800起</span></div>
   				<div class="product_offer"><p><img height="22" width="60" alt="" src="http://www.summittour.com.tw/eWeb_summittour/IMGDB/000000/001137/00008852.png"/></p></div>
			</div>
		</div>
	</div>
</body>
</html>
